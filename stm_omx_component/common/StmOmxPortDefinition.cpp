/*
 * Copyright 2013 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file   StmOmxPortDefinition.cpp
 * @author STMicroelectronics
 */

#include "StmOmxPortDefinition.hpp"
#include "StmOmxIComponent.hpp"

#include "string.h"

namespace {

template<typename T>
const T& std_max(const T& a, const T& b)
{ return (a > b) ? a : b; }

}

namespace stm {

//=============================================================================
// class BufferReq implementation
//=============================================================================

BufferReq::BufferReq()
    :m_nBufferCountActual(0),
     m_nBufferCountMin(0),
     m_nBufferSizeMin(0),
     m_bBuffersContiguous(OMX_FALSE),
     m_nBufferAlignment(0)
{}

BufferReq::BufferReq(OMX_U32 aBufferCountActual,
                     OMX_U32 aBufferCountMin,
                     OMX_U32 aBufferSizeMin,
                     OMX_BOOL aContiguous,
                     OMX_U32 aBufferAlignment)
    :m_nBufferCountActual(aBufferCountActual),
     m_nBufferCountMin(aBufferCountMin),
     m_nBufferSizeMin(aBufferSizeMin),
     m_bBuffersContiguous(aContiguous),
     m_nBufferAlignment(aBufferAlignment)
{}

BufferReq::BufferReq(const OMX_PARAM_PORTDEFINITIONTYPE& portDef)
    :m_nBufferCountActual(portDef.nBufferCountActual),
     m_nBufferCountMin(portDef.nBufferCountMin),
     m_nBufferSizeMin(portDef.nBufferSize),
     m_bBuffersContiguous(portDef.bBuffersContiguous),
     m_nBufferAlignment(portDef.nBufferAlignment)
{}

BufferReq::BufferReq(const BufferReq& req)
    :m_nBufferCountActual(req.countActual()),
     m_nBufferCountMin(req.countMin()),
     m_nBufferSizeMin(req.sizeMin()),
     m_bBuffersContiguous(req.isContiguous()),
     m_nBufferAlignment(req.alignment())
{}

BufferReq::~BufferReq()
{}

BufferReq&
BufferReq::operator=(const BufferReq& req)
{
    if (this == &req) {     // Same object?
        return *this;       // Yes, so skip assignment, and just return *this.
    }

    m_nBufferCountActual = req.countActual();
    m_nBufferCountMin = req.countMin();
    m_nBufferSizeMin = req.sizeMin();
    m_bBuffersContiguous = req.isContiguous();
    m_nBufferAlignment = req.alignment();
    return *this;
}

void
BufferReq::updatePortDef(OMX_PARAM_PORTDEFINITIONTYPE& portDef) const
{
    portDef.nBufferCountActual = this->countActual() ;
    portDef.nBufferCountMin = this->countMin() ;
    portDef.nBufferSize = this->sizeMin() ;
    portDef.bBuffersContiguous = this->isContiguous() ;
    portDef.nBufferAlignment = this->alignment() ;
}

BufferReq
BufferReq::max(const BufferReq& a, const BufferReq& b)
{
    OMX_U32   maxCountActual = std_max(a.countActual(), b.countActual());
    OMX_U32   maxCountMin = std_max(a.countMin(), b.countMin());
    OMX_U32   maxBufferSizeMin = std_max(a.sizeMin(), b.sizeMin());
    OMX_BOOL  maxBuffersContiguous = (a.isContiguous() || b.isContiguous())? OMX_TRUE: OMX_FALSE;
    OMX_U32   maxBufferAlignment = std_max(a.alignment(), b.alignment());
    return BufferReq(maxCountActual, maxCountMin, maxBufferSizeMin,
                     maxBuffersContiguous, maxBufferAlignment);
}

//=============================================================================
// class PortDefinition implementation
//=============================================================================

PortDefinition::PortDefinition(unsigned int nIdx, OMX_DIRTYPE eDir)
    :IOmxPortDefinition()
{
    memset(&m_sPortDef, 0, sizeof(OMX_PARAM_PORTDEFINITIONTYPE));
    m_sPortDef.nSize = sizeof(OMX_PARAM_PORTDEFINITIONTYPE);
    omxIlSpecVersion(&(m_sPortDef.nVersion));

    m_sPortDef.nPortIndex = nIdx;
    m_sPortDef.eDir = eDir;
    m_sPortDef.bEnabled = OMX_TRUE;
    m_sPortDef.bPopulated = OMX_FALSE;
    m_sPortDef.nBufferCountActual = 0;
    m_sPortDef.nBufferCountMin = 0;
    m_sPortDef.nBufferSize = 0;
    m_sPortDef.bBuffersContiguous = OMX_FALSE;
    m_sPortDef.nBufferAlignment = 0;
    m_sPortDef.eDomain = OMX_PortDomainMax;
}

PortDefinition::PortDefinition(unsigned int nIdx, OMX_DIRTYPE eDir,
                               const BufferReq& req)
    :IOmxPortDefinition()
{
    memset(&m_sPortDef, 0, sizeof(OMX_PARAM_PORTDEFINITIONTYPE));
    m_sPortDef.nSize = sizeof(OMX_PARAM_PORTDEFINITIONTYPE);
    omxIlSpecVersion(&(m_sPortDef.nVersion));

    m_sPortDef.nPortIndex = nIdx;
    m_sPortDef.eDir = eDir;
    m_sPortDef.bEnabled = OMX_TRUE;
    m_sPortDef.bPopulated = OMX_FALSE;
    m_sPortDef.nBufferCountActual = req.countActual();
    m_sPortDef.nBufferCountMin = req.countMin();
    m_sPortDef.nBufferSize = req.sizeMin();
    m_sPortDef.bBuffersContiguous = req.isContiguous();
    m_sPortDef.nBufferAlignment = req.alignment();
    m_sPortDef.eDomain = OMX_PortDomainMax;
}

PortDefinition::PortDefinition(const PortDefinition& p)
    :IOmxPortDefinition()
{
    m_sPortDef = p.getParamPortDef();
}

PortDefinition::~PortDefinition()
{}

unsigned int
PortDefinition::index() const
{ return m_sPortDef.nPortIndex; }

bool
PortDefinition::isEnabled() const
{ return m_sPortDef.bEnabled; }

bool
PortDefinition::isPopulated() const
{ return m_sPortDef.bPopulated; }

OMX_PORTDOMAINTYPE
PortDefinition::domain() const
{ return m_sPortDef.eDomain; }

OMX_DIRTYPE
PortDefinition::direction() const
{ return m_sPortDef.eDir; }

unsigned int
PortDefinition::bufferCountActual() const
{ return m_sPortDef.nBufferCountActual; }

unsigned int
PortDefinition::bufferCountMin() const
{ return m_sPortDef.nBufferCountMin; }

unsigned int
PortDefinition::bufferSize() const
{ return m_sPortDef.nBufferSize; }

bool
PortDefinition::isBuffersContiguous() const
{ return m_sPortDef.bBuffersContiguous; }

unsigned int
PortDefinition::bufferAlignment() const
{ return m_sPortDef.nBufferAlignment; }

void
PortDefinition::bufferCountActual(unsigned int aCount)
{ m_sPortDef.nBufferCountActual = aCount; }

void
PortDefinition::bufferCountMin(unsigned int aCount)
{ m_sPortDef.nBufferCountMin = aCount; }

void
PortDefinition::bufferSize(unsigned int aSize)
{ m_sPortDef.nBufferSize = aSize; }

void
PortDefinition::buffersContiguous(bool aCont)
{ m_sPortDef.bBuffersContiguous = (aCont? OMX_TRUE: OMX_FALSE); }

void
PortDefinition::bufferAlignment(unsigned int aAlign)
{ m_sPortDef.nBufferAlignment = aAlign; }

void
PortDefinition::enabled()
{ m_sPortDef.bEnabled = OMX_TRUE; }

void
PortDefinition::disabled()
{ m_sPortDef.bEnabled = OMX_FALSE; }

void
PortDefinition::populated()
{ m_sPortDef.bPopulated = OMX_TRUE; }

void
PortDefinition::unpopulated()
{ m_sPortDef.bPopulated = OMX_FALSE; }

BufferReq
PortDefinition::bufferReq() const
{
    return BufferReq((OMX_U32)this->bufferCountActual(),
                     (OMX_U32)this->bufferCountMin(),
                     (OMX_U32)this->bufferSize(),
                     (this->isBuffersContiguous()? OMX_TRUE: OMX_FALSE),
                     (OMX_U32)this->bufferAlignment());
}

OMX_PARAM_PORTDEFINITIONTYPE
PortDefinition::getParamPortDef() const
{ return m_sPortDef; }

void
PortDefinition::setParamPortDef(OMX_PARAM_PORTDEFINITIONTYPE* pType)
{
    // Only update non read-only field
    this->bufferCountActual(pType->nBufferCountActual);
}

} // eof namespace stm
