/*
 * Copyright 2013 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file   OtherPortDefinition.cpp
 * @author STMicroelectronics
 */

#include "OtherPortDefinition.hpp"

#if HAVE_ASSERT_H
#include <assert.h>
#endif

#if HAVE_DBGTASSERT_H
// Re-direct assert on DBGT_ASSERT macro
#define assert(expr)  DBGT_ASSERT(expr)
#endif

#include <linux/dbgt.h>


namespace stm {

OtherPortDefinition::OtherPortDefinition(unsigned int nIdx, OMX_DIRTYPE eDir)
    :PortDefinition(nIdx, eDir)
{
    m_sPortDef.eDomain = OMX_PortDomainOther;
    m_sPortDef.format.other.eFormat = OMX_OTHER_FormatTime;
}

OtherPortDefinition::~OtherPortDefinition()
{}

OtherPortDefinition::OtherPortDefinition(unsigned int nIdx, OMX_DIRTYPE eDir,
                                         const BufferReq& aReq)
    :PortDefinition(nIdx, eDir, aReq)
{
    m_sPortDef.eDomain = OMX_PortDomainOther;
    m_sPortDef.format.other.eFormat = OMX_OTHER_FormatTime;
}

OtherPortDefinition::OtherPortDefinition(const OtherPortDefinition& vp)
    :PortDefinition(vp)
{
    m_sPortDef.eDomain = OMX_PortDomainOther;
    m_sPortDef.format.other.eFormat = vp.format();
}

OMX_OTHER_FORMATTYPE
OtherPortDefinition::format() const
{ return m_sPortDef.format.other.eFormat;}

void
OtherPortDefinition::format(OMX_OTHER_FORMATTYPE aFmt)
{ m_sPortDef.format.other.eFormat = aFmt; }

void
OtherPortDefinition::setParamPortDef(OMX_PARAM_PORTDEFINITIONTYPE* pType)
{
    PortDefinition::setParamPortDef(pType);

    assert(pType->eDomain == OMX_PortDomainOther);
    this->format(pType->format.other.eFormat);
}

} // eof namespace stm
