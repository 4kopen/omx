/*
 * Copyright 2013 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file   Mutex.hpp
 * @author STMicroelectronics
 */


#ifndef _STM_MUTEX_HPP_
#define _STM_MUTEX_HPP_

// Implementation of mutex based on pthread
#include <pthread.h>


namespace stm {

class IMutex;
class Mutex;
class ScopedLock;
class Condition;

/**
 * Declare the interface of a Mutex object
 */
class IMutex
{
public:
    /** Destructor */
    virtual ~IMutex()
    {}

    /** Lock the mutex. */
    virtual
    int lock() = 0;

    /** Release the mutex. */
    virtual
    int release() = 0;

protected:
    /** Constructor. Make protected so that it can not
        be created on its own */
    IMutex()
    {}

    /** Copy constructor. No implemented */
    IMutex(const IMutex&);
};

class Mutex: public IMutex
{
public:
    /** Constructor. */
    Mutex();

    /** Destructor. */
    virtual ~Mutex();

    /** @brief Lock the mutex.
     * This method attempts to lock the mutex. If the mutex is already locked,
     * the calling thread is blocked until the mutex becomes available.
     *
     * @return returns 0 if success.
     */
    virtual
    int lock();

    /** @brief Release the mutex.
     * This method attempts to release the mutex. Attempting to unlock the
     * mutex if it is not locked results in undefined behavior.
     *
     * @return returns 0 if success.
     */
    virtual
    int release();

private:
    friend class Condition;
    pthread_mutex_t* mutex_t();

private:
    pthread_mutex_t m_tMutex;
};

inline pthread_mutex_t* Mutex::mutex_t()
{ return &m_tMutex; }

class ScopedLock
{
public:
    /** Constructor. */
    ScopedLock(IMutex& mtx);

    /** Destructor */
    virtual ~ScopedLock();

private:
    IMutex& m_rMutex;
};


class Condition
{
public:
    /** Constructor. */
    Condition();

    /** Destructor */
    virtual ~Condition();

    /** @brief Signal condition.
     * This method unblock threads blocked on a condition variable.
     *
     * @return returns 0 if success.
     */
    virtual
    int signal();

    /** @brief Block on a condition variable.
     * This method blocks the calling thread, waiting for the condition to be
     * signaled.
     *
     * @param mut the mutex associated with the condition variable
     * @return returns 0 if success.
     */
    virtual
    int wait(Mutex& mut);

private:
    pthread_cond_t          m_cond;
};


} // eof ns stm

#endif // ifndef _STM_MUTEX_HPP__
