/*
 * Copyright 2013 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file   Role.hpp
 * @author STMicroelectronics
 */


#ifndef _STM_ROLE_HPP_
#define _STM_ROLE_HPP_

#include <string.h>

namespace stm {

class Role
{
public:
    /** Max length for component role */
    static const unsigned int MAX_LENGTH = 128;
    static const unsigned int MAX_ROLES = 16;

public:
    /** @group Constructors and Destructors
     * @{
     */
    /** Default constructor. */
    Role();

    /** Constructor. */
    Role(unsigned int nNb, const char** aStrArray);

    /** Destructor. */
    virtual ~Role();

    /** 2nd phase constructor. Valid only if not already initialized */
    void construct(unsigned int nNb, const char** aStrArray);

    /** Add a new role */
    void add(const char* pRole);
    /** @}*/

    /** Return the number of defined role */
    virtual unsigned int size() const;

    /** Return index of the active role */
    virtual unsigned int active() const;

    /**
     * @brief Set index of the active role .
     * May assert if aActiveIndex is greater than or equal to the vector size
     */
    virtual const char* active(unsigned int aActiveIndex);

    /**
     * @brief Return role within the given index
     * May assert if aIndex is greater than or equal to the vector size
     */
    virtual const char* at(unsigned int aIndex) const;

protected:
    /** Copy constructor not implemented */
    Role(const Role& );

private:
    /** Index of the active role */
    unsigned int  m_active;

    /** vector of string containing supported roles */
    unsigned int  m_nRoleSz;
    char*         m_roles[MAX_ROLES];
};


} // eof ns stm

#endif // ifndef _STM_ROLE_HPP__
