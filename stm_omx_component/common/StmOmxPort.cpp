/*
 * Copyright 2013 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file   StmOmxPort.cpp
 * @author STMicroelectronics
 */

#include "StmOmxIComponent.hpp"
#include "StmOmxPort.hpp"
#include "StmOmxVendorExt.hpp"
#include "OMX_debug.h"
#include <string.h>
#if HAVE_ASSERT_H
#include <assert.h>
#endif

#if HAVE_DBGTASSERT_H
// Re-direct assert on DBGT_ASSERT macro
#define assert(expr)  OMX_DBGT_ASSERT(expr)
#endif

#define DBGT_PREFIX "OMXP"
#define DBGT_LAYER  0
#include <linux/dbgt.h>

#include <unistd.h> // for usleep


namespace stm {


//=============================================================================
// class OmxPort implementation
//=============================================================================


//=====================================================================================
// STOMXComponent::STOMXPort
//  OMX port constructor
//======================================================================================

OmxPort::OmxPort()
    :IOmxPort(),
    mIsLoggingEnabled(false),
     m_nBuffersCount(0),
     m_bTunnel(false),
    fifos(),
    current(NULL),
    currentMutex()
{
    m_sTunnelSetup.nTunnelFlags = 0;
    m_sTunnelSetup.eSupplier = OMX_BufferSupplyUnspecified;
}

OMX_ERRORTYPE OmxPort::create()
{
    pthread_mutex_init(&currentMutex, NULL);
    return OMX_ErrorNone;
}
//=====================================================================================
// STOMXComponent::OmxPort
//  OMX port desstructor
//======================================================================================

OmxPort::~OmxPort()
{}

void OmxPort::incrementNbBuffers()
{
    m_nBuffersCount++;
    if (this->definition().bufferCountActual() <= this->nbBuffers()) {
        this->definition().populated();
    }
}

void OmxPort::decrementNbBuffers()
{
    m_nBuffersCount--;
    if (nbBuffers() == 0) {
        this->definition().unpopulated();
    }
}

//=============================================================================
// OmxComponent::name
//=============================================================================

const char* OmxPort::name() const
{ return m_strName; }

//==========================================================================
// OmxPort::push
// push an OMX command or a buffer to the fifo
//==========================================================================
void OmxPort::push(OMX_BUFFERHEADERTYPE* pBufferHdr)
{
    OMX_DBGT_PROLOG();
    fifos.push(pBufferHdr);
    OMX_DBGT_EPILOG();
}

//==========================================================================
// OmxPort::popAndLockCurrent
// pop an OMX command or a buffer from the fifo and lock it
//==========================================================================
OMX_BUFFERHEADERTYPE* OmxPort::popAndLockCurrent()
{
    OMX_DBGT_PROLOG();
    OMX_DBGT_CHECK(NULL == current);
    fifos.lock();

    OMX_BUFFERHEADERTYPE* buf = fifos.popLocked();
    pthread_mutex_lock( &currentMutex );
    current = buf;
    fifos.unlock();
    OMX_DBGT_EPILOG();
    return current;
}

//==========================================================================
// OmxPort::pop
// pop an OMX command or a buffer from the fifo
//==========================================================================
OMX_BUFFERHEADERTYPE* OmxPort::pop()
{
    OMX_DBGT_PROLOG();
    fifos.lock();

    OMX_BUFFERHEADERTYPE* buf = fifos.popLocked();

    fifos.unlock();
    OMX_DBGT_EPILOG();
    return buf;
}

//==========================================================================
// OmxPort::relockCurrent
// lock the current buffer
//==========================================================================
OMX_BUFFERHEADERTYPE* OmxPort::relockCurrent()
{
    OMX_DBGT_PROLOG();
    pthread_mutex_lock( &currentMutex );
    OMX_DBGT_EPILOG();
    return current;
}

//==========================================================================
// OmxPort::freeAndUnlockCurrent
// free and unlock the current buffer
//==========================================================================
void OmxPort::freeAndUnlockCurrent()
{
    OMX_DBGT_PROLOG();
    current = NULL;
    pthread_mutex_unlock( &currentMutex );
    OMX_DBGT_EPILOG();
}

//==========================================================================
// OmxPort::unlockCurrent
// free the current buffer
//==========================================================================
void OmxPort::unlockCurrent()
{
    OMX_DBGT_PROLOG();
    pthread_mutex_unlock( &currentMutex );
    OMX_DBGT_EPILOG();
}


OMX_ERRORTYPE
OmxPort::tunnelRequest(OMX_HANDLETYPE hTunneledComp,
                       OMX_U32 nTunneledPort,
                       OMX_TUNNELSETUPTYPE* pTunnelSetup)
{
    OMX_DBGT_PROLOG();
    if (hTunneledComp == NULL && pTunnelSetup == 0) {
        // Remove tunneling request
        m_bTunnel = false;
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }

    assert(hTunneledComp);
    assert(pTunnelSetup);

    if (this->isOutput()) {
        // Port shall indicate supplier preference.
        // Nothing to do
        pTunnelSetup->eSupplier = OMX_BufferSupplyUnspecified;
        m_bTunnel = true;
    } else {
        // The input port is responsible to check that it is compatible
        // with the tunneled output port
        // Check if connected component is supporting LinuxDVB extension.
        OMX_ERRORTYPE eError = OMX_ErrorNone;
        OMX_INDEXTYPE nExtIndex;
        if (this->definition().domain() == OMX_PortDomainAudio) {
            // Check Audio device is supported
            eError = OMX_GetExtensionIndex(hTunneledComp,
                                           (OMX_STRING)OMX_StmDvbAudioDeviceInfoExt,
                                           &nExtIndex);
        } else if (this->definition().domain() == OMX_PortDomainVideo) {
            // Check Video device is supported
           eError = OMX_GetExtensionIndex(hTunneledComp,
                                           (OMX_STRING)OMX_StmDvbVideoDeviceInfoExt,
                                           &nExtIndex);
        } else if (this->definition().domain() == OMX_PortDomainOther) {
            // Check either Video either Audio
            eError = OMX_GetExtensionIndex(hTunneledComp,
                                           (OMX_STRING)OMX_StmDvbAudioDeviceInfoExt,
                                           &nExtIndex);
           if (eError != OMX_ErrorNone) {
               eError = OMX_GetExtensionIndex(hTunneledComp,
                                              (OMX_STRING)OMX_StmDvbVideoDeviceInfoExt,
                                              &nExtIndex);
           }
        }

        // Check result
        if (eError != OMX_ErrorNone) {
            OMX_DBGT_ERROR("Tunneling request with non-ST component");
            OMX_DBGT_EPILOG("Tunnel request failed");
            return eError;
        }
        m_bTunnel = true;
    }

    OMX_DBGT_EPILOG();
    return OMX_ErrorNone;
}

OMX_BUFFERHEADERTYPE*
OmxPort::allocateBufferHeader(OMX_PTR pAppPrivate,
                                OMX_U32 nSizeBytes) const
{
    OMX_BUFFERHEADERTYPE* pBufferHdr = (OMX_BUFFERHEADERTYPE*)malloc(sizeof(OMX_BUFFERHEADERTYPE)) ;
    if (!pBufferHdr) {
        OMX_DBGT_ERROR("Can not allocate buffer header");
        return 0;
    }

    memset(pBufferHdr, 0, sizeof(OMX_BUFFERHEADERTYPE));
    pBufferHdr->nSize = sizeof(OMX_BUFFERHEADERTYPE);
    stm::omxIlSpecVersion(&(pBufferHdr->nVersion));
    ASSERT_STRUCT_TYPE(pBufferHdr, OMX_BUFFERHEADERTYPE);
    pBufferHdr->pBuffer              = 0;
    pBufferHdr->nAllocLen            = nSizeBytes;
    pBufferHdr->nFilledLen           = 0;
    pBufferHdr->nOffset              = 0;
    pBufferHdr->pAppPrivate          = pAppPrivate;
    pBufferHdr->pPlatformPrivate     = 0;
    pBufferHdr->hMarkTargetComponent = 0;
    pBufferHdr->pMarkData            = 0;
    pBufferHdr->nTickCount           = 0;
    pBufferHdr->nTimeStamp           = 0;
    pBufferHdr->nFlags               = 0;
    pBufferHdr->pOutputPortPrivate   = 0;
    pBufferHdr->pInputPortPrivate    = 0;

    if (this->isInput()) {
        pBufferHdr->nInputPortIndex = this->index();
        pBufferHdr->nOutputPortIndex = 0xFFFFFFFE;
    } else {
        pBufferHdr->nInputPortIndex = 0xFFFFFFFE;
        pBufferHdr->nOutputPortIndex = this->index();
    }
    return pBufferHdr;
}

void
OmxPort::freeBufferHeader(OMX_BUFFERHEADERTYPE* pBufferHdr) const
{
    if (!pBufferHdr) {
        OMX_DBGT_ERROR("Can not free invalid buffer header");
    }
    free(pBufferHdr);
}

OMX_ERRORTYPE
OmxPort::useBuffer(OMX_BUFFERHEADERTYPE** ppBufferHdr,
                     OMX_PTR pAppPrivate,
                     OMX_U32 nSizeBytes,
                     OMX_U8* pBuffer)
{
    // Code provided as default implementation
    // should be overloaded in inherit port class
    OMX_DBGT_PROLOG("Using buffer %p on port %u", pBuffer, this->index());

    // Invalid case in proprietary tunneling
    if (this->isTunneled()) {
        return OMX_ErrorIncorrectStateOperation;
    }

    // Allocate a buffer header
    *ppBufferHdr = 0;
    OMX_BUFFERHEADERTYPE* pBufferHdr = allocateBufferHeader(pAppPrivate,
                                                            nSizeBytes);
    OMX_DBGT_CHECK_RETURN(0 != pBufferHdr, OMX_ErrorInsufficientResources);

    pBufferHdr->pBuffer = pBuffer;
    // Incremented buffer counter. It also updates populating status
    this->incrementNbBuffers();

    *ppBufferHdr = pBufferHdr;
    OMX_DBGT_EPILOG();
    return OMX_ErrorNone;
}

OMX_ERRORTYPE
OmxPort::allocateBuffer(OMX_BUFFERHEADERTYPE** ppBufferHdr,
                          OMX_PTR pAppPrivate,
                          OMX_U32 nSizeBytes)
{
    // Code provided as default implementation
    // should be overloaded in inherit port class

    OMX_DBGT_PROLOG("Allocating new buffer on port %u", this->index());
    if (nSizeBytes < this->definition().bufferSize()) {
        return OMX_ErrorBadParameter;
    }

    // Invalid case in proprietary tunneling
    if (this->isTunneled()) {
        return OMX_ErrorIncorrectStateOperation;
    }

    // Allocate buffer header
    *ppBufferHdr = 0;
    OMX_BUFFERHEADERTYPE* pBufferHdr = allocateBufferHeader(pAppPrivate,
                                                            nSizeBytes);
    OMX_DBGT_CHECK_RETURN(0 != pBufferHdr, OMX_ErrorInsufficientResources);

    // Allocate a buffer
    OMX_U8* pBuf = (OMX_U8*)malloc(nSizeBytes);
    OMX_DBGT_CHECK_RETURN(0 != pBuf, OMX_ErrorInsufficientResources);

    pBufferHdr->pBuffer = pBuf;
    pBufferHdr->pPlatformPrivate = (void*)1;
    // Incremented buffer counter. It also updates populating status
    this->incrementNbBuffers();

    *ppBufferHdr = pBufferHdr;
    OMX_DBGT_EPILOG("Port %u has allocated %u buffers",
                this->index(), this->nbBuffers());
    return OMX_ErrorNone;
}

OMX_ERRORTYPE
OmxPort::freeBuffer(OMX_BUFFERHEADERTYPE* pBufferHdr)
{
    // Code provided as default implementation
    // should be overloaded in inherit port class

    // Invalid case in proprietary tunneling
    assert(!isTunneled());

    OMX_DBGT_PROLOG();
    // Decrement buffer counter. It also updates populating status
    this->decrementNbBuffers();

    if (pBufferHdr->pPlatformPrivate) {
        free(pBufferHdr->pBuffer);
        pBufferHdr->pBuffer = 0;
    }
    freeBufferHeader(pBufferHdr);
    OMX_DBGT_EPILOG();
    return OMX_ErrorNone;
}

// TODO: prefer const variable
#define OMX_TIMEOUT 20000         // Timeout value in microsecond
#define OMX_MAX_TIMEOUTS 50       // Count of Maximum number of times the component can time out


OMX_ERRORTYPE
OmxPort::disable()
{
    // Check if already disable
    if (this->definition().isEnabled() == false)
        return OMX_ErrorNone;


    if (this->isTunneled()) {
        // Nothing to free
        this->definition().disabled();
        return OMX_ErrorNone;
    }

    // Wait for all buffers to be freed
    for (unsigned int nTimeout = 0x0; nTimeout <= OMX_MAX_TIMEOUTS; nTimeout++)
    {
        if (!this->isPopulated()) {
            // Change port enable status
            this->definition().disabled();
            return OMX_ErrorNone;
        }
        usleep(OMX_TIMEOUT);
    }
    return OMX_ErrorPortUnresponsiveDuringDeallocation;
}

OMX_ERRORTYPE OmxPort::enable()
{
    // Check if already enable
    if (this->definition().isEnabled() == true)
        return OMX_ErrorNone;

    if (this->isTunneled()) {
        // No need to wait on port to be populated
        this->definition().enabled();
        return OMX_ErrorNone;
    }

    // Change port enable status
    this->definition().enabled();

    // Wait for port to be populated
    for (unsigned int nTimeout = 0x0; nTimeout <= OMX_MAX_TIMEOUTS; nTimeout++)
    {
        if ((this->component().omxState() == OMX_StateLoaded) ||
            this->isPopulated()) {
            return OMX_ErrorNone;
        }
        usleep(OMX_TIMEOUT);
    }
    return OMX_ErrorPortUnresponsiveDuringAllocation;
}

OMX_ERRORTYPE OmxPort::flush()
{ return OMX_ErrorNone; }

OMX_ERRORTYPE OmxPort::pause()
{ return OMX_ErrorNone; }

OMX_ERRORTYPE OmxPort::resume()
{ return OMX_ErrorNone; }

} // eof namespace stm
