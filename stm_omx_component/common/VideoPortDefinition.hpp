/*
 * Copyright 2013 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file   VideoPortDefinition.hpp
 * @author STMicroelectronics
 */

#ifndef _STM_VIDEOPORTDEFINITION_HPP_
#define _STM_VIDEOPORTDEFINITION_HPP_

#include "StmOmxPortDefinition.hpp"
#include <OMX_VideoExt.h>

namespace stm {

/**
 * Class VideoPortDefinition contains video port accessor.
 */
class VideoPortDefinition: public PortDefinition
{
public:
    /**@name Constructors and Destructors */
    //@{
    /** Constructor. */
    VideoPortDefinition(unsigned int nIdx, OMX_DIRTYPE eDir);

    VideoPortDefinition(unsigned int nIdx, OMX_DIRTYPE eDir,
                        const BufferReq& req);

    /** Copy constructor. */
    VideoPortDefinition(const VideoPortDefinition& vp);

    /** Destructor. */
    virtual ~VideoPortDefinition();
    //@}

    /**@name Video property getters */
    //@{
    virtual const char*
    contentType() const;

    virtual unsigned int
    xFrameRate() const;

    virtual unsigned int
    frameWidth() const;

    virtual unsigned int
    frameHeight() const;

    virtual int
    stride() const;

    virtual unsigned int
    sliceHeight() const;

    virtual unsigned int
    bitRate() const;

    virtual unsigned int
    frameRate() const;

    virtual bool
    flagErrorConcealment() const;

    virtual OMX_VIDEO_CODINGTYPE
    compressionFormat() const;

    virtual OMX_COLOR_FORMATTYPE
    colorFormat() const;
    //@}

    /**@name Video property setters */
    //@{
    virtual void
    contentType(const char* mimeType);

    virtual void
    frameWidth(unsigned int aWidth);

    virtual void
    frameHeight(unsigned int aHeight);

    virtual void
    stride(int aStride);

    virtual void
    sliceHeight(unsigned int aSliceHeight);

    virtual void
    xFrameRate(unsigned int );

    virtual void
    bitRate(unsigned int aBitRate);

    virtual void
    frameRate(unsigned int aFrameRate);

    virtual void
    flagErrorConcealment(bool aERC);

    virtual void
    compressionFormat(OMX_VIDEO_CODINGTYPE aFmt);

    virtual void
    compressionFormat(OMX_VIDEO_CODINGEXTTYPE aFmt);

    virtual void
    mimeType(const char * cMime);

    virtual void
    colorFormat(OMX_COLOR_FORMATTYPE aFmt);
    //@}

    virtual void
    setParamPortDef(OMX_PARAM_PORTDEFINITIONTYPE* pType);

private:
    static const unsigned int kMimeTypeSz = 32;
    char                      m_strMIMEType[kMimeTypeSz];
};


} // eof namespace stm

#endif // _STM_VIDEOPORTDEFINITION_HPP_
