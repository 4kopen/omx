/*
 * Copyright 2013 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file   IDvbDevice.hpp
 * @author STMicroelectronics
 */

#ifndef _STM_IDVBDEVICE_HPP_
#define _STM_IDVBDEVICE_HPP_

#include <stdint.h>

namespace stm {

/**
 * Interface class for linux dvb devices.
 */
class IDvbDevice
{
public:
    typedef enum  {
        EState_Stop  = 0,
        EState_Start,
        EState_Pause,
    } EState;

    /** Destructor. */
    virtual ~IDvbDevice()
    {}

public:
    /**
     * Returns the device file descriptor id.
     * Return -1 if not opened
     */
    virtual int id() const = 0;

    /**
     * Returns the device name.
     */
    virtual const char* name() const = 0;

    /**
     * Opens the dvb device named in constructor.
     * Also configure the device source, stream type, etc...
     */
    virtual int create() = 0;
    /** Close the dvb instance */
    virtual int destroy() = 0;
    /** Check if device is opened or not */
    virtual bool isOpen() const = 0;

    /** Moves device in play state. */
    virtual int start() const = 0;
    /** Moves device in stop state. */
    virtual int stop() const = 0;
    /** Stopping device execution (if hw is true) and change state. */
    virtual int pause(bool hw) const = 0;
    /** Resuming device execution (if hw is true) and change state. */
    virtual int resume(bool hw) const = 0;
    /** Used to protect write from pause */
    virtual void lock() const = 0;
    /** Used to protect write from pause */
    virtual void unlock() const = 0;
    /** Used to well manage the clear/flush during pause */
    virtual void resetflags() const = 0;


    /** Return the current media time */
    virtual uint64_t getTime() const = 0;

    /** Return the current media frequency */
    virtual uint32_t getSamplingRate() const = 0;

    /** Writes data to this device. */
    virtual int write(const void* data_ptr, unsigned int len) const = 0;
    /**
     * Waits for any buffered samples to be played by the device.
     * If parameter bEoS is true then end of stream is reached and no more
     * data will be written in device.
     */
    virtual int flush(bool bEoS = false) const = 0;

    /**
     * Discards buffers already sent to device. Flush by discarding already
     * sent buffers
     */
    virtual int clear() const = 0;

    virtual EState state() const = 0;

    virtual void setEncoding(int nEncoding){};

    virtual void setSync(bool bSyncEnable){};
    virtual void setDecodeMode(uint32_t nDecodeMode){};

    virtual void setResolution(int nWidth, int nHeight){};

    virtual void set10Bit(bool bEnable10Bit){};

    /** Check whether device is started or in pause */
    bool isStarted() const;
    /** Check whether device is started or not */
    bool isStopped() const;
   /** Check whether device is started or not */
    bool isPaused() const;

    virtual int UseBuffer(unsigned long virtualAddr, unsigned long bufferSize,
                  int width, int height, int color,
                  unsigned long userId) const = 0;
    virtual unsigned long FlushBuffer() const = 0;

    virtual int FillThisBuffer(unsigned long userId) const = 0;
    virtual int FillBufferDone(unsigned long & userId, uint64_t & TS, bool & eos) const = 0;
    virtual int QueueBuffer (void* BufferData, int &nBytesSize, uint64_t &ts) const = 0;
    virtual int v4l2Start() const {return 0;};
    virtual int v4l2Stop() const {return 0;};
protected:
    /** Hidden default constructor, make protected so cannot ever create
        one on its own. */
    IDvbDevice()
    {}

    /** Copy constructor not implemented */
    IDvbDevice(const IDvbDevice& st);
};

inline bool IDvbDevice::isStarted() const
{ return this->state() != EState_Stop; }

inline bool IDvbDevice::isStopped() const
{ return this->state() == EState_Stop; }

inline bool IDvbDevice::isPaused() const
{ return this->state() == EState_Pause; }

} // eof namespace stm

#endif
