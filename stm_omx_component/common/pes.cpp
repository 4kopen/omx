/*
 * Copyright 2011 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *
 * File   ::    pes.cpp
 * Author ::    Jean-Philippe FASSINO (jean-philippe.fassino@st.com)
 *
 *
 */

#include <pes.hpp>
typedef struct BitPacker_s
{
    unsigned char*      Ptr;                                    /* write pointer */
    unsigned int        BitBuffer;                              /* bitreader shifter */
    int                 Remaining;                              /* number of remaining in the shifter */
#ifdef DEBUG_PUTBITS
    int                 debug;
#endif /* DEBUG_PUTBITS */
} BitPacker_t;

/* Define the structure of the private data for the adpcm PES packets.
 * This is the structure that is expected by the Streaming Engine.*/
struct AdpcmPrivateDate_s
{
    unsigned char           StreamId;
    unsigned int            Frequency;
    unsigned char           NbOfCoefficients;
    unsigned char           NbOfChannels;
    unsigned short          NbOfSamplesPerBlock;
    unsigned char           LastPacket;
} __attribute__ ((packed));

typedef AdpcmPrivateDate_s adpcmPrivateDate_t;

//=====================================================================================
// PutBits
//=====================================================================================
void PutBits(BitPacker_t * ld, unsigned int code, unsigned int length)
{
    unsigned int bit_buf;
    int bit_left;

    bit_buf = ld->BitBuffer;
    bit_left = ld->Remaining;

#ifdef DEBUG_PUTBITS
    if (ld->debug)
        PLAY_TRACE("code = %d, length = %d, bit_buf = 0x%x, bit_left = %d\n", code, length, bit_buf, bit_left);
#endif /* DEBUG_PUTBITS */

    if ((int)length < bit_left)
    {
        /* fits into current buffer */
        bit_buf = (bit_buf << length) | code;
        bit_left -= length;
    }
    else
    {
        /* doesn't fit */
        bit_buf <<= bit_left;
        bit_buf |= code >> (length - bit_left);
        ld->Ptr[0] = (char)(bit_buf >> 24);
        ld->Ptr[1] = (char)(bit_buf >> 16);
        ld->Ptr[2] = (char)(bit_buf >> 8);
        ld->Ptr[3] = (char)bit_buf;
        ld->Ptr   += 4;
        length    -= bit_left;
        bit_buf    = code & ((1 << length) - 1);
        bit_left   = 32 - length;
    }

#ifdef DEBUG_PUTBITS
    if (ld->debug)
        PLAY_TRACE("bit_left = %d, bit_buf = 0x%x\n", bit_left, bit_buf);
#endif /* DEBUG_PUTBITS */

    /* writeback */
    ld->BitBuffer = bit_buf;
    ld->Remaining = bit_left;
}


//=====================================================================================
// FlushBits
//=====================================================================================
void FlushBits(BitPacker_t * ld)
{
    ld->BitBuffer <<= ld->Remaining;
    while (ld->Remaining < 32)
    {
#ifdef DEBUG_PUTBITS
        if (ld->debug)
            PLAY_TRACE("flushing 0x%2.2x\n", ld->BitBuffer >> 24);
#endif /* DEBUG_PUTBITS */
        *ld->Ptr++ = ld->BitBuffer >> 24;
        ld->BitBuffer <<= 8;
        ld->Remaining += 8;
    }
    ld->Remaining = 32;
    ld->BitBuffer = 0;
}

//=====================================================================================
// InsertPesHeader
//=====================================================================================
int InsertPesHeader (unsigned char *data, int size, unsigned char stream_id, unsigned long long int pts, int pic_start_code)
{

/*
 * * PESHeader Structure

byte 0   |   byte 1   |   byte 2   |   byte 3   |   byte 4   |   byte 5
00000000   00000000     00000001
        Start code                 | Stream ID  |  PES packet length


byte 6
7 6     5 4          3           2          1           0
1 0     PES          PES         data       copyright   original
        scrambling   priority    alignment                or
        control                  indicator               copy

byte 7
7 6        5        4          3            2             1        0
PTS DTS    ESCR    ES rate     DSM trick    additional    PES      PES
flags      flag    flag        mode flag    copy          flag     extension
                                            info flag              flag

byte 8
PES header data length

PTS DTS flags -- Presentation Time Stamp / Decode Time Stamp
00 = no PTS or DTS data present, 01 is forbidden
if set to 10 the following data is appended to the header data field:

7 6 5 4    3 2 1       0    7 6 5 4 3 2 1 0 7 6 5 4 3 2 1    0    7 6 5 4 3 2 1 0 7 6 5 4 3 2 1    0
0 0 1 0    PTS 32..30  1    PTS 29..15                       1    PTS 14..00                       1

*/
    /*
     * Below is a representation of individual bytes
     * in the pes header
     */
   unsigned char pes[]   = {
                                0x00,  0x00,   0x01,                 // bytes 1,2,3 : Start Code
                                0x00,                                // byte 4 : Stream ID
                                0x00,  0x00,                         // byte 5,6 : PES packet Length
                                0x80,                                // byte 7
                                0x00,                                // byte 8
                                0x00,                                // byte 9 : PES header data length
                                0x00,  0x00,  0x00, 0x00, 0x00,      // bytes 10-14 : PTS info
                                0x00,  0x00,  0x00, 0x00, 0x00       // bytes 14-19
                            };
    //For tracking Pes Header Length, incremented when a byte sequence is completed
    unsigned char* pesPtr = pes;
    //For calculating the total number of bytes written
    int bytes_filled;
    /* Adding 3 for various indicators, flags & PES_header_data_length
     * 5 for PTS, 5 for pic start code
     */
    int pes_packet_length = size + 3 + (pts != INVALID_PTS_VALUE ? 5:0) +
                            (pic_start_code ? 5 : 0);

    int j=0;
    char ch;
    int i=0;

    //Byte 1,2,3 are fixed
    pesPtr += 3;

    //Filling Byte 4
    *(pesPtr++) = (stream_id & 0xFF);

    //Filling byte 5,6 (higher bits of length in lower bytes)
    if (stream_id == MPEG_VIDEO_PES_START_CODE || stream_id == H264_VIDEO_PES_START_CODE ||
            stream_id == VC1_VIDEO_PES_START_CODE || stream_id == H263_VIDEO_PES_START_CODE)
    {
        if (pes_packet_length > 65535)
        {
            pes_packet_length = 0;
        }
    }
    //Filling Byte 5, contains higher bits of pes_packet_length
    *(pesPtr++) = ((pes_packet_length>>8) & 0xFF);

    //Filling Byte 6, contains lower bits of pes_packet_length
    *(pesPtr++) = (pes_packet_length & 0xFF);

    //Byte 7 is fixed
    pesPtr++;

    //Filling bit 7 of byte 8, rest of the bits are fixed
    if(pts != INVALID_PTS_VALUE)
    {
        *(pesPtr++) = 0x80;
    }

    //Filling bits 0,2 of byte 9, rest of the bits are fixed
    if(pts != INVALID_PTS_VALUE)
    {
        *(pesPtr++) = 0x5;
    }

    //9 bytes are filled in any case
    bytes_filled = 9;

    //Filling bytes 10-14
    if(pts != INVALID_PTS_VALUE)
    {
        //Filling bit 0 of byte 10
        *(pesPtr) |= 0x01;
        //Filling bits 1,2,3 of byte 10
        *(pesPtr) |= ((pts>>29 & 0x0E));

        //Filling bit 5 of byte 10
        *(pesPtr++) |= ((0x01)<<5);

        //Filling bits 0-7 of byte 11
        *(pesPtr++) |= ((pts>>22) & 0xFF);

        //Filling bit 0 of byte 12
        *(pesPtr) |= 0x01;
        //Filling bits 1-7 of byte 12
        *(pesPtr++) |= ((pts>>14 & 0xFE));

        //Filling bits 0-7 of byte 13
        *(pesPtr++) |= ((pts>>7) & 0xFF);

        //Filling bit 0 of byte 14
        *(pesPtr) |= 0x01;
        //Filling bits 1-7 of byte 14
        *(pesPtr++) |= (pts<<1 & 0xFE);

        bytes_filled +=5;
    }

    //Filling bytes 15-19
    if(pic_start_code)
    {
        //Byte 15,16 fixed
        pesPtr +=2;

        //Filling bit 0 of byte 17
        *(pesPtr++) = 0x01;                         // Start Code

        //Filling bit 0-7  of byte 18
        *(pesPtr++) = (pic_start_code & 0xFF);      // 00, for picture start

        /* Filling byte 19, For any extra information (like in mpeg4p2, the
         * pic_start_code)
         */
        *(pesPtr++) = ((pic_start_code>>8) & 0xFF);

        bytes_filled +=5;
    }

    memcpy(data,pes,bytes_filled);

    return bytes_filled;

}


//=====================================================================================
// InsertVideoPrivateDataHeaderForAudio
//=====================================================================================
int InsertVideoPrivateDataHeaderForAudio(unsigned char *data, int payload_size)
{
    BitPacker_t ld2 = {data, 0, 32};
    int         HeaderLength;
    int         i;

    PutBits (&ld2, PES_PRIVATE_DATA_FLAG, 8);
    PutBits (&ld2, payload_size & 0xff, 8);
    PutBits (&ld2, (payload_size >> 8) & 0xff, 8);
    PutBits (&ld2, (payload_size >> 16) & 0xff, 8);

    for (i = 4; i < (PES_PRIVATE_DATA_LENGTH+1); i++)
         PutBits (&ld2, 0, 8);

    FlushBits (&ld2);

    HeaderLength        = PES_PRIVATE_DATA_LENGTH+1;

    return HeaderLength;
}

#define PCM_LITTLE_ENDIAN                               0x00
#define PCM_BIG_ENDIAN                                  0x01
#define WAV_FORMAT_HEADER_CHANNEL_COUNT_OFFSET          10
#define WAV_FORMAT_HEADER_CHANNEL_COUNT_SIZE            2
#define WAV_FORMAT_HEADER_SAMPLE_RATE_SIZE              4
#define WAV_FORMAT_HEADER_BYTES_PER_SEC_SIZE            4
#define WAV_FORMAT_HEADER_BLOCK_ALIGN_SIZE              2
#define WAV_FORMAT_HEADER_SAMPLE_SIZE_SIZE              2
/*
 *  This sequence creates a WAV style header so that the player can set up details of
 *  Sampling rate etc.  This cannot be done as a standard lpcm because most quicktime pcm
 *  files (e.g. from digital still camera) use a sample rate which is not supported by lpcm.
 */
int InsertWavHeader (unsigned char *Data, unsigned int Channels, unsigned int BitsPerSample,
                     unsigned int SampleRate, unsigned int DataEndianness)
{
    unsigned char*      DataPtr                 = Data;
    unsigned int        BlockAlign              = (BitsPerSample / 8) * Channels;
    unsigned int        AverageBytesPerSecond   = SampleRate * BlockAlign;

    const unsigned char WavFormatHeader[]   = {
                                               0x66,    0x6d,   0x74,   0x20,       /* "fmt " */
                                               0x16,    0x00,   0x00,   0x00,       /* Data size */
                                               0x01,    0x00,                       /* Compression code (pcm)*/
                                               0x00,    0x00,                       /* No. channels */
                                               0x00,    0x00,   0x00,   0x00,       /* Sample rate */
                                               0x00,    0x00,   0x00,   0x00,       /* Average bytes per second */
                                               0x00,    0x00,                       /* Block align */
                                               0x00,    0x00,                       /* Sample size */
                                              };

    //LOGE ("Inject Channels               %d\n",  Channels);
    //LOGE ("Inject BitsPerSample          %d\n",  BitsPerSample);
    //LOGE ("Inject SampleRate             %d\n",  SampleRate);
    //LOGE ("Inject BlockAlign             %d\n",  BlockAlign);
    //LOGE ("Inject AverageBytesPerSecond  %d\n",  AverageBytesPerSecond);

    memcpy (DataPtr, WavFormatHeader, sizeof(WavFormatHeader));

    if (DataEndianness == PCM_BIG_ENDIAN)
        memcpy (DataPtr, " tmf", 4);         /* Convert "fmt " to " tmf" to indicate big-endian data */

    DataPtr            += WAV_FORMAT_HEADER_CHANNEL_COUNT_OFFSET;
    *DataPtr++          =  Channels & 0xff;
    *DataPtr++          = (Channels >> 8) & 0xff;
    *DataPtr++          =  SampleRate & 0xff;
    *DataPtr++          = (SampleRate >> 8) & 0xff;
    *DataPtr++          = (SampleRate >> 16) & 0xff;
    *DataPtr++          = (SampleRate >> 24) & 0xff;
    *DataPtr++          =  AverageBytesPerSecond & 0xff;
    *DataPtr++          = (AverageBytesPerSecond >> 8) & 0xff;
    *DataPtr++          = (AverageBytesPerSecond >> 16) & 0xff;
    *DataPtr++          = (AverageBytesPerSecond >> 24) & 0xff;
    *DataPtr++          =  BlockAlign & 0xff;
    *DataPtr++          = (BlockAlign >> 8) & 0xff;
    *DataPtr++          =  BitsPerSample & 0xff;
    *DataPtr++          = (BitsPerSample >> 8) & 0xff;

    return DataPtr - Data;
}

/*
 *  This sequence adds the private data needed for ADPCM injection into the SE
 */

int InsertAdpcmPvtData(unsigned char *DataPtr, unsigned char Channels, unsigned int SampleRate,
                       unsigned int SamplesPerBlock, unsigned char NumberOfCoefficients)
{
    adpcmPrivateDate_t privateData;
    unsigned int pvtDataSize = sizeof(privateData);

    memset (&privateData, 0, pvtDataSize);
    privateData.StreamId = 0xA5;
    privateData.Frequency = SampleRate;
    privateData.NbOfChannels = Channels;
    privateData.NbOfCoefficients = NumberOfCoefficients; // Should be always 2 for IMA_ADPCM
    privateData.NbOfSamplesPerBlock = SamplesPerBlock;

    memcpy (DataPtr, &privateData, pvtDataSize);
    return pvtDataSize;
}
