/*
 * Copyright 2013 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file   StmOmxVendorExt.hpp
 * @author STMicroelectronics
 */

#ifndef _STM_OMXVENDOREXT_HPP_
#define _STM_OMXVENDOREXT_HPP_

#include "IDvbDevice.hpp"
#include "OMX_Index.h"

// ST Vendor extension definition
extern "C" {

#define OMX_StmDvbVideoDeviceInfo      (OMX_IndexVendorStartUnused + 0xd22e00)
#define OMX_StmDvbAudioDeviceInfo      (OMX_IndexVendorStartUnused + 0xd22e01)
#define OMX_StmDvbVideoDeviceInfoExt   "OMX.stm.dvb.index.videoDeviceInfo"
#define OMX_StmDvbAudioDeviceInfoExt   "OMX.stm.dvb.index.audioDeviceInfo"

typedef struct {
    OMX_U32 nSize;
    OMX_VERSIONTYPE nVersion;
    stm::IDvbDevice**  ppDevice;
    OMX_HANDLETYPE     hTunnelComp;
} OMX_GetStmLinuxDvbDeviceInfoParams;

}

#define INIT_DVB_DEVICE_INFO(_i_)                                       \
    memset((_i_), 0, sizeof(OMX_GetStmLinuxDvbDeviceInfoParams));       \
    (_i_)->nSize = sizeof(OMX_GetStmLinuxDvbDeviceInfoParams);          \
    stm::omxIlSpecVersion(&((_i_)->nVersion));                          \
    (_i_)->ppDevice = 0


inline void copyDeviceInfo(OMX_GetStmLinuxDvbDeviceInfoParams& to,
                           OMX_GetStmLinuxDvbDeviceInfoParams& from)
{
    to.ppDevice = from.ppDevice;
}

#endif
