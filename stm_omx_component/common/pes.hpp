/*
 * Copyright 2011 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * File	  ::	pes.h	
 *
 */
#include <string.h>

#define INVALID_PTS_VALUE                       0x200000000ull
#define MAX_PTS_VALUE                           (INVALID_PTS_VALUE-1)
#define PTS_TO_US(x)                            (((x) * 1000)/90)
#define MAX_TS_US                               PTS_TO_US(MAX_PTS_VALUE)
#define MAX_PES_PACKET_DATA_SIZE                0xffff
#define MAX_PES_PACKET_SIZE                     (MAX_PES_PACKET_DATA_SIZE - 64)

#define MPEG_AUDIO_PES_START_CODE               0xc0
#define AAC_AUDIO_PES_START_CODE                0xcf
#define PRIVATE_STREAM_1_PES_START_CODE         0xbd
#define PES_MAX_HEADER_SIZE                     64
#define PES_MIN_HEADER_SIZE                     9
#define MPEG_VIDEO_PES_START_CODE               0xe0
#define H264_VIDEO_PES_START_CODE               0xe2
#define PES_START_CODE_RESERVED_4               0xfd
#define PES_START_CODE_RESERVED_5               0xfe
#define VC1_VIDEO_PES_START_CODE                PES_START_CODE_RESERVED_4
#define H263_VIDEO_PES_START_CODE               PES_START_CODE_RESERVED_5


#define PES_LENGTH_BYTE_0                               5
#define PES_LENGTH_BYTE_1                               4
#define PES_FLAGS_BYTE                                  7
#define PES_EXTENSION_DATA_PRESENT                      0x01
#define PES_PRIVATE_DATA_LENGTH                         8
#define PES_EXTENSION_FLAG_BYTE                         7
#define PES_EXTENSION_FLAG                              0x01
#define PES_HEADER_DATA_LENGTH_BYTE                     8
#define PES_PRIVATE_DATA_FLAG                           0x80

// windows WAVE file Encoding TAGS
#define WAVE_FORMAT_DVI_ADPCM   0x0011 /* Intel DVI ADPCM */


int InsertPesHeader (unsigned char *data, int size, unsigned char stream_id, unsigned long long int pts, int pic_start_code);
int InsertVideoPrivateDataHeaderForAudio(unsigned char *data, int payload_size);
int InsertWavHeader (unsigned char *Data, unsigned int Channels, unsigned int BitsPerSample,
                     unsigned int SampleRate, unsigned int DataEndianness);
int InsertAdpcmPvtData(unsigned char *DataPtr, unsigned char Channels, unsigned int SampleRate,
                       unsigned int SamplesPerBlock, unsigned char NumberOfCoefficients);
