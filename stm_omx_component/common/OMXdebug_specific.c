/*
 * Copyright 2011 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *
 * File   ::    OMXdebug_specific.c
 * Author ::    
 *
 *
 */

#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <pthread.h>
#include <assert.h>
#include <sys/select.h>
#include <sys/inotify.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/cdefs.h>
#include <sys/prctl.h>
#include <unistd.h>
#include <stdlib.h>

#include "OMX_debug.h"
#define DBGT_PREFIX "OMX"
#define DBGT_LAYER  0
#include <linux/dbgt.h>

static int dumpBuffer;
static int traceActivity;
static unsigned long OMX_dump_size;

static char *OMX_dump_output_buffer;
static size_t OMX_dump_output_buffer_size;
static char *OMX_dump_input_buffer;
static size_t OMX_dump_input_buffer_size;

static char OMX_dump_name[OMX_MAX_STRINGNAME_SIZE]; /* Name of component's buffers to dump */
static char OMX_dump_path[OMX_MAX_STRINGNAME_SIZE];
static char OMX_trace_name[OMX_MAX_STRINGNAME_SIZE];

static OMX_COMPONENTTYPE *OMX_dump_handle;
static int OMX_dump_dir; /* 0=dump input, 1=dump output, 2=dump input&output */
static int OMX_dump_hex; /* 0=binary(default), 1=hexa */
static int OMX_dump_idx;
static int OMX_dump_enabled;
static int mDataDumpEnabled;
static unsigned long mCurrentDataDumpSize;
#ifndef ANDROID
#define ANDROID_LOG_DEBUG 0
#endif

#ifdef ANDROID
int OMX_wrapper_log_print

(int prio, const char *tag, char *buffer, size_t size, const char *fmt, ...)
{
    va_list ap;
    (void) buffer;
    (void) size;
    va_start(ap, fmt);
    int ret = __android_log_vprint(prio, tag, fmt, ap);
    va_end(ap);
    return ret;
}
#endif

int OMX_debug_sprintf
(int prio, const char *tag, char *buffer, size_t size, const char *fmt, ...)
{
    va_list ap;
    (void) prio;
    (void) tag;
    va_start(ap, fmt);
    int ret = vsnprintf(buffer, size, fmt, ap);
    va_end(ap);
    return ret;
}

static pthread_mutex_t GetParameter_str_mutex = PTHREAD_MUTEX_INITIALIZER;

// GetParameter wrapper
void  OMXDebug_GetParameter(OMX_HANDLETYPE hComponent,
                            OMX_INDEXTYPE  nIndex,
                            OMX_PTR        pComponentParameterStructure,
                            const char*    name)
{
    static char param_str[OMXDEBUG_MAX_DEBUG_BUFFER];

    pthread_mutex_lock(&GetParameter_str_mutex);

    strOMX_INDEXTYPE_struct(
     nIndex,
     pComponentParameterStructure,
     OMX_DEBUG_LOG_LEVEL,
     ANDROID_LOG_DEBUG,
     LOG_TAG,
     NULL,
     param_str,
     OMXDEBUG_MAX_DEBUG_BUFFER);
    DBGT_PDEBUG("%s GetParameter:\n%s", name, param_str);

    pthread_mutex_unlock(&GetParameter_str_mutex);
}

static pthread_mutex_t SetParameter_str_mutex = PTHREAD_MUTEX_INITIALIZER;

// SetParameter wrapper
void OMXDebug_SetParameter(OMX_HANDLETYPE hComponent,
                           OMX_INDEXTYPE  nIndex,
                           OMX_PTR        pComponentParameterStructure,
                           const char*    name)
{
    static char param_str[OMXDEBUG_MAX_DEBUG_BUFFER];

    pthread_mutex_lock(&SetParameter_str_mutex);

    strOMX_INDEXTYPE_struct(
     nIndex,
     pComponentParameterStructure,
     OMX_DEBUG_LOG_LEVEL,
     ANDROID_LOG_DEBUG,
     LOG_TAG,
     NULL,
     param_str,
     OMXDEBUG_MAX_DEBUG_BUFFER);
    if(nIndex == (OMX_U32)OMX_IndexParamVideoPortFormat)
       DBGT_PINFO("%s SetParameter()\n%s", name, param_str);
    else
       DBGT_PDEBUG("%s SetParameter()\n%s", name, param_str);

    pthread_mutex_unlock(&SetParameter_str_mutex);
}

static pthread_mutex_t GetConfig_str_mutex = PTHREAD_MUTEX_INITIALIZER;

// GetConfig wrapper
void OMXDebug_GetConfig(OMX_HANDLETYPE hComponent,
                        OMX_INDEXTYPE  nIndex,
                        OMX_PTR        pComponentConfigStructure,
                        const char*    name)
{
    static char config_str[OMXDEBUG_MAX_DEBUG_BUFFER];

    pthread_mutex_lock(&GetConfig_str_mutex);

    strOMX_INDEXTYPE_struct(
     nIndex,
     pComponentConfigStructure,
     OMX_DEBUG_LOG_LEVEL,
     ANDROID_LOG_DEBUG,
     LOG_TAG,
     NULL,
     config_str,
     OMXDEBUG_MAX_DEBUG_BUFFER);
    DBGT_PDEBUG("%s GetConfig:\n%s", name, config_str);

    pthread_mutex_unlock(&GetConfig_str_mutex);
}

static pthread_mutex_t SetConfig_str_mutex = PTHREAD_MUTEX_INITIALIZER;

// SetConfig wrapper
void OMXDebug_SetConfig(OMX_HANDLETYPE hComponent,
                        OMX_INDEXTYPE  nIndex,
                        OMX_PTR        pComponentConfigStructure,
                        const char*    name)
{
    static char config_str[OMXDEBUG_MAX_DEBUG_BUFFER];

    pthread_mutex_lock(&SetConfig_str_mutex);

    strOMX_INDEXTYPE_struct(
     nIndex,
     pComponentConfigStructure,
     OMX_DEBUG_LOG_LEVEL,
     ANDROID_LOG_DEBUG,
     LOG_TAG,
     NULL,
     config_str,
     OMXDEBUG_MAX_DEBUG_BUFFER);
    DBGT_PDEBUG("%s SetConfig:\n%s", name, config_str);

    pthread_mutex_unlock(&SetConfig_str_mutex);
}

static pthread_mutex_t GetComponentVersion_str_mutex = PTHREAD_MUTEX_INITIALIZER;

// GetComponentVersion wrapper
void OMXDebug_GetComponentVersion(OMX_HANDLETYPE   hComponent,
                                  OMX_STRING       pComponentName,
                                  OMX_VERSIONTYPE* pComponentVersion,
                                  OMX_VERSIONTYPE* pSpecVersion,
                                  OMX_UUIDTYPE*    pComponentUUID,
                                  const char*      name)
{
    pthread_mutex_lock(&GetComponentVersion_str_mutex);

    static char CV_str[OMXDEBUG_MAX_DEBUG_BUFFER];
    static char SV_str[OMXDEBUG_MAX_DEBUG_BUFFER];
    static char CUUID_str[OMXDEBUG_MAX_DEBUG_BUFFER];

    strOMX_VERSIONTYPE(pComponentVersion, CV_str, OMXDEBUG_MAX_DEBUG_BUFFER);
    strOMX_VERSIONTYPE(pSpecVersion, SV_str, OMXDEBUG_MAX_DEBUG_BUFFER);
    strOMX_UUIDTYPE(pComponentUUID, CUUID_str, OMXDEBUG_MAX_DEBUG_BUFFER);

    DBGT_PDEBUG("%s GetComponentVersion, ComponentName=%s, ComponentVersion=%s, SpecVersion=%s, ComponentUUID=%s",
     name,
     pComponentName,
     CV_str,
     SV_str,
     CUUID_str);

    pthread_mutex_unlock(&GetComponentVersion_str_mutex);
}

static pthread_mutex_t SendCommand_str_mutex = PTHREAD_MUTEX_INITIALIZER;

// SendCommand wrapper
void OMXDebug_SendCommand(OMX_HANDLETYPE  hComponent,
                          OMX_COMMANDTYPE Cmd,
                          OMX_U32         nParam,
                          OMX_PTR         pCmdData,
                          const char*     name)
{
    static char sendcommand_str[OMXDEBUG_MAX_DEBUG_BUFFER];

    pthread_mutex_lock(&SendCommand_str_mutex);

    strOMX_COMMAND(Cmd, nParam, sendcommand_str, OMXDEBUG_MAX_DEBUG_BUFFER);
    DBGT_PDEBUG("%s SendCommand: %s", name, sendcommand_str);

    pthread_mutex_unlock(&SendCommand_str_mutex);

}

static pthread_mutex_t UseBuffer_str_mutex = PTHREAD_MUTEX_INITIALIZER;

void OMXDebug_UseBuffer(OMX_HANDLETYPE         hComponent,
                        OMX_BUFFERHEADERTYPE** ppBufferHdr,
                        OMX_U32                nPortIndex,
                        OMX_PTR                pAppPrivate,
                        OMX_U32                nbBuffers,
                        OMX_U32                nSizeBytes,
                        OMX_U8*                pBuffer,
                        const char*            name)
{
    pthread_mutex_lock(&UseBuffer_str_mutex);

    DBGT_PTRACE("%s Used Buffer %d: 0x%08x on %s port length %d",
                name,
                (int)nbBuffers,
                (int)pBuffer,
                (int)nPortIndex == 0 ? "Input" : "Output",
                (int)nSizeBytes);

    pthread_mutex_unlock(&UseBuffer_str_mutex);
}

static pthread_mutex_t AllocateBuffer_str_mutex = PTHREAD_MUTEX_INITIALIZER;

// AllocateBuffer wrapper
void OMXDebug_AllocateBuffer(OMX_HANDLETYPE         hComponent,
                             OMX_BUFFERHEADERTYPE** ppBufferHdr,
                             OMX_U32                nPortIndex,
                             OMX_PTR                pAppPrivate,
                             OMX_U32                nbBuffers,
                             OMX_U32                nSizeBytes,
                             const char*            name)
{
    pthread_mutex_lock(&AllocateBuffer_str_mutex);

    DBGT_PTRACE("%s Allocated Buffer %d: 0x%08x on %s port length %d",
                name,
                (int)nbBuffers,
                (int)(*ppBufferHdr)->pBuffer,
                (int) nPortIndex == 0 ? "input" : "output", (int) nSizeBytes);

    pthread_mutex_unlock(&AllocateBuffer_str_mutex);
}

void OMXDebug_FreeBuffer(OMX_HANDLETYPE        hComponent,
                         OMX_U32               nPortIndex,
                         OMX_BUFFERHEADERTYPE* pBuffer,
                         OMX_U32               nbBuffers,
                         const char*           name)
{
    DBGT_PTRACE("%s Free Buffer %d: 0x%08x, on %s Port",
                name,
                (int)nbBuffers,
                (int)pBuffer->pBuffer,
                (int)nPortIndex == 0 ? "Input" : "Output");
}

static pthread_mutex_t EmptyThisBuffer_str_mutex = PTHREAD_MUTEX_INITIALIZER;

// EmptyThisBuffer wrapper
void OMXDebug_EmptyThisBuffer(OMX_HANDLETYPE        hComponent,
                              OMX_BUFFERHEADERTYPE* pBuffer,
                              const char*           name)
{
    pthread_mutex_lock(&EmptyThisBuffer_str_mutex);

    DBGT_PTRACE("%s EmptyThisBuffer: 0x%08x, (length %d), timestamp %lld us (%.2f secs)",
               name,
               (unsigned int)pBuffer->pBuffer,
               (int)pBuffer->nFilledLen,
               pBuffer->nTimeStamp,
               (float)pBuffer->nTimeStamp / 1E6 );

    pthread_mutex_unlock(&EmptyThisBuffer_str_mutex);

    if ((OMX_dump_enabled) && (OMX_dump_dir != 1) && (dumpBuffer)) {
        if ( OMX_dump_hex) {
            DBGT_PINFO("%s dump input port in hexadecimal (OMXDebug_EmptyThisBuffer):\n%s",
                        name,
                        strOMX_BUFFERDATA(
                        pBuffer,
                        &OMX_dump_input_buffer,
                        &OMX_dump_input_buffer_size,
                        OMX_dump_size,
                        OMX_BUFFERDATA_NB_END_BYTES,
                        OMX_BUFFERDATA_NB_BYTES_PER_LINES));
        } else if (!OMX_dump_hex && ((unsigned int)OMX_dump_idx == pBuffer->nInputPortIndex)) {
            dumpData(pBuffer,
                     "IN",
                     OMX_dump_path,
                     OMX_dump_idx,
                     OMX_dump_size,
                     OMX_dump_name,
                     hComponent);
        }
    }
}

static pthread_mutex_t FillThisBuffer_str_mutex = PTHREAD_MUTEX_INITIALIZER;

// FillThisBuffer wrapper
void OMXDebug_FillThisBuffer(OMX_HANDLETYPE        hComponent,
                             OMX_BUFFERHEADERTYPE* pBuffer,
                             const char*           name)
{
    pthread_mutex_lock(&FillThisBuffer_str_mutex);
    DBGT_PTRACE("%s FillThisBuffer: (0x%08x)", name, (unsigned int) pBuffer->pBuffer);
    pthread_mutex_unlock(&FillThisBuffer_str_mutex);
}


static pthread_mutex_t ComponentTunnelRequest_str_mutex = PTHREAD_MUTEX_INITIALIZER;

// ComponentTunnelRequest wrapper
void OMXDebug_ComponentTunnelRequest(OMX_HANDLETYPE       hComponent,
                                     OMX_U32              nPort,
                                     OMX_HANDLETYPE       hTunneledComp,
                                     OMX_U32              nTunneledPort,
                                     OMX_TUNNELSETUPTYPE* pTunnelSetup,
                                     const char*          name)
{
    static char tunnelsetup_str[OMXDEBUG_MAX_DEBUG_BUFFER];

    pthread_mutex_lock(&ComponentTunnelRequest_str_mutex);

    strOMX_TUNNELSETUPTYPE(pTunnelSetup, tunnelsetup_str, OMXDEBUG_MAX_DEBUG_BUFFER);
    DBGT_PDEBUG("%s TunnelRequest:%s", name, tunnelsetup_str);

    pthread_mutex_unlock(&ComponentTunnelRequest_str_mutex);
}

void OMXDebug_ComponentRoleEnum(OMX_HANDLETYPE hComponent,
                                OMX_U8 *       cRole,
                                OMX_U32        nIndex,
                                const char*    name)
{
    DBGT_PDEBUG("%s ComponentRoleEnum  nIndex=%d", name, (int) nIndex);
}


void OMXDebug_GetExtensionIndex(OMX_HANDLETYPE hComponent,
                                OMX_STRING     cParameterName,
                                OMX_INDEXTYPE* pIndexType,
                                const char*    name)
{
    DBGT_PDEBUG("%s GetExtensionIndex cParameterName=%s", name, cParameterName);
}

void OMXDebug_onStateChange(OMX_STATETYPE newState,
                            const char*   name)
{
  DBGT_PTRACE("%s onStateChange %s",
	      name, OMX_TYPE_TO_STR(OMX_STATETYPE, newState));

    switch (newState) {
        case OMX_StateIdle: {
            DBGT_PTRACE("%s Now Idle.", name);
        } break;

        case OMX_StateExecuting: {
            DBGT_PTRACE("%s Now Executing.", name);
        } break;

        case OMX_StateLoaded: {
            DBGT_PTRACE("%s Now Loaded.", name);
        } break;

        case OMX_StatePause: {
            DBGT_PTRACE("%s Now Pause.", name);
        } break;

        case OMX_StateInvalid: {
            DBGT_WARNING("%s StateInvalid.", name);
        } break;

        case OMX_StateWaitForResources: {
            DBGT_WARNING("%s WaitForRessources.", name);
        } break;

        default: { 
           DBGT_WARNING("%s should not be here.", name);
        }  break;
    }
}

/********************************************************************************
 * Wrapper funtions being called by client to then call OMXDebug code
 ********************************************************************************/

// SetCallbacks wrapper
void OMXDebug_SetCallbacks(OMX_HANDLETYPE hComponent,
                           OMX_PTR        pAppData)
{
}

OMX_ERRORTYPE OMXDebug_EmptyBufferDone(OMX_HANDLETYPE        hComponent,
                                       OMX_PTR               pAppData,
                                       OMX_BUFFERHEADERTYPE* pBuffer,
                                       const char*           name)
{
    OMX_ERRORTYPE  eError = OMX_ErrorNone;

    DBGT_PTRACE("%s EMPTY_BUFFER_DONE:(Buffer: 0x%08x)", name, (unsigned int) pBuffer->pBuffer);

    return eError;
}

OMX_ERRORTYPE OMXDebug_FillBufferDone(OMX_HANDLETYPE        hComponent,
                                      OMX_PTR               pAppData,
                                      OMX_BUFFERHEADERTYPE* pBuffer,
                                      const char*           name)
{
    OMX_ERRORTYPE  eError = OMX_ErrorNone;

    DBGT_PTRACE("%s FILL_BUFFER_DONE:(Buffer: 0x%08x size : %d length: %d flags: 0x%08x timestamp: %lld us (%.2f secs)",
                    name,
                    (unsigned int) pBuffer->pBuffer,
                    (int)pBuffer->nAllocLen,
                    (int)pBuffer->nFilledLen,
                    (unsigned int)pBuffer->nFlags,
                    pBuffer->nTimeStamp,
                    (float)pBuffer->nTimeStamp / 1E6);

    if (OMX_dump_enabled && OMX_dump_dir && dumpBuffer) {
        if (OMX_dump_hex) {
            DBGT_PINFO("%s dump output port in hexadecimal (OMXDebug_FillBufferDone):\n%s",
                        name,
                        strOMX_BUFFERDATA(
                        pBuffer,
                        &OMX_dump_output_buffer,
                        &OMX_dump_output_buffer_size,
                        OMX_dump_size,
                        OMX_BUFFERDATA_NB_END_BYTES,
                        OMX_BUFFERDATA_NB_BYTES_PER_LINES));
        } else if (!OMX_dump_hex && (unsigned int)OMX_dump_idx == pBuffer->nOutputPortIndex) {
                     dumpData(pBuffer,
                     "OUT",
                     OMX_dump_path,
                     OMX_dump_idx,
                     OMX_dump_size,
                     OMX_dump_name,
                     hComponent);
        }
    }
    return eError;
}

OMX_ERRORTYPE  OMXDebug_EventHandler(OMX_HANDLETYPE   hComponent,
                                     OMX_PTR       pAppData,
                                     OMX_EVENTTYPE eEvent,
                                     OMX_U32       nData1,
                                     OMX_U32       nData2,
                                     OMX_PTR       pEventData,
                                     const char*          name)
{
    OMX_ERRORTYPE  eError = OMX_ErrorNone;
    static char EventHandler_message_buffer[OMXDEBUG_MAX_DEBUG_BUFFER];

    strOMX_EVENTHANDLER(eEvent,
                        nData1,
                        nData2,
                        pEventData,
                        EventHandler_message_buffer,
                        OMXDEBUG_MAX_DEBUG_BUFFER);

    DBGT_PDEBUG("%s ON_EVENT_HANDLER:%s", name, EventHandler_message_buffer);

    return eError;
}


char * strOMX_VERSIONTYPE(OMX_VERSIONTYPE *version, char *buffer, size_t size)
{
    if (version) {
        snprintf(buffer, size,"\n"
                 "              + nVersion     =0x%08x\n"
                 "              + nVersionMajor=%d\n"
                 "              + nVersionMinor=%d\n"
                 "              + nRevision    =%d\n"
                 "              + nStep        =%d",
                 (unsigned int) version->nVersion,
                 (int) version->s.nVersionMajor,
                 (int) version->s.nVersionMinor,
                 (int) version->s.nRevision,
                 (int) version->s.nStep);
    } else {
        snprintf(buffer, size, "\n"
                 "              + nVersion = NULL");
    }
    return buffer;
}

char * strOMX_UUIDTYPE(OMX_UUIDTYPE *pComponentUUID, char *buffer, size_t size)
{
    char bufferCUUID[128*2+5];
    int nb_char = 0;
    if(pComponentUUID) {
        bufferCUUID[0] = '\0';
        char * tmpi = bufferCUUID;
        int index = 0;
        while(index < 128) {
            if((index != 0) && (index%32 == 0))
            {
                *(tmpi++) = '\0';
            }
            sprintf(tmpi, "%02x", (unsigned int) pComponentUUID[index++]);
            tmpi += 2;
        }
        *(tmpi++) = '\0';

#define BUFFER buffer + nb_char, ( size - nb_char > 0 ? size - nb_char : 0)

        nb_char += snprintf(BUFFER, "\n");
        nb_char += snprintf(BUFFER, "              + ComponentUUID[ 0...31]=%s\n", bufferCUUID);
        nb_char += snprintf(BUFFER, "              + ComponentUUID[32...63]=%s\n", bufferCUUID+64+1);
        nb_char += snprintf(BUFFER, "              + ComponentUUID[64...95]=%s\n", bufferCUUID+128+2);
        nb_char += snprintf(BUFFER, "              + ComponentUUID[96..127]=%s\n", bufferCUUID+192+3);
    } else {
        snprintf(buffer, size, "\n              + ComponentUUID = NULL");
    }
    return buffer;
}

char * strOMX_EVENTHANDLER(OMX_EVENTTYPE eEvent,
                           OMX_U32 nData1,
                           OMX_U32 nData2,
                           OMX_PTR pEventData,
                           char *buffer, size_t size)
{
#define SEPARATOR_STR " : "
#define DESCR_BUFFER_SIZE 1024

    char event_str[128];
    char extra_str[128];
    char descr_str[DESCR_BUFFER_SIZE];

    switch(eEvent) {
    case OMX_EventCmdComplete:
        strOMX_COMMAND((OMX_COMMANDTYPE) nData1, nData2, descr_str, DESCR_BUFFER_SIZE);
        break;
    case OMX_EventError:
        extra_str[0] = ' ';
        extra_str[1] = '\0';
        break;
    case OMX_EventPortSettingsChanged:
    case OMX_EventBufferFlag:
        sprintf(extra_str, " - port %d", (int) nData1);
        break;
    default:
        extra_str[0] = '\0';
    }

    snprintf(buffer, size, "%s%s%s", strOMX_EVENTTYPE(eEvent, event_str, 128), extra_str,
             (eEvent == OMX_EventCmdComplete ? descr_str :
              (eEvent == OMX_EventError ?
               strOMX_ERRORTYPE((OMX_ERRORTYPE) nData1, descr_str, DESCR_BUFFER_SIZE) : "")));
    return buffer;
}

char * strOMX_TUNNELSETUPTYPE(OMX_TUNNELSETUPTYPE* pTunnelSetup, char *buffer, size_t size)
{
    char supplier_str[128];
    if(pTunnelSetup) {
        snprintf(buffer, size,
                "TunnelSetup.nTunnelFlags=0x%08x TunnelSetup.eSupplier=%s",
                (unsigned int) pTunnelSetup->nTunnelFlags,
                strOMX_BUFFERSUPPLIERTYPE(pTunnelSetup->eSupplier, supplier_str, 128));
    } else {
        snprintf(buffer, size, "TunnelSetup = NULL");
    }
    return buffer;
}

char * strOMX_BUFFERHEADERTYPE(OMX_BUFFERHEADERTYPE* pBuffer, char *buffer, size_t size)
{
    int nb_char = 0;
    buffer[0] = '\0';

#define BUFFER buffer + nb_char, ( size - nb_char > 0 ? size - nb_char : 0)

    if (pBuffer) {
        if (pBuffer->pBuffer) {
            nb_char += snprintf(BUFFER,
                 "         |- pBuffer              : %p\n", pBuffer->pBuffer);
        }
 
        if ((pBuffer->nAllocLen) && (pBuffer->nAllocLen != 0)) {
            nb_char += snprintf(BUFFER,
                 "         |- nAllocLen            : %u\n", pBuffer->nAllocLen);
        }

        if ((pBuffer->nFilledLen) && (pBuffer->nFilledLen != 0)) {
            nb_char += snprintf(BUFFER,
                 "         |- nFilledLen           : %u\n", pBuffer->nFilledLen);
        }

        if ((pBuffer->nOffset) && (pBuffer->nOffset != 0)){
            nb_char += snprintf(BUFFER,
                 "         |- nOffset              : %u\n",
                 pBuffer->nOffset);
        }

        if (pBuffer->pAppPrivate) {

            nb_char += snprintf(BUFFER,
                 "         |- pAppPrivate          : 0x%08x\n",
                 (unsigned int) pBuffer->pAppPrivate);
        }

        if ((pBuffer->pPlatformPrivate) && (pBuffer->pPlatformPrivate != 0)) {
            nb_char += snprintf(BUFFER,
                 "         |- pPlatformPrivate     : 0x%08x\n",
                 (unsigned int) pBuffer->pPlatformPrivate);
        }

        if (pBuffer->hMarkTargetComponent) {
            nb_char += snprintf(BUFFER,
                 "         |- hMarkTargetComponent : 0x%08x\n",
                 (unsigned int) pBuffer->hMarkTargetComponent);
        }

        if(pBuffer->pMarkData) {
            nb_char += snprintf(BUFFER,
                 "         |- pMarkData            : 0x%08x\n",
                 (unsigned int) pBuffer->pMarkData);
        }

        if(pBuffer->nTickCount) {
            nb_char += snprintf(BUFFER,
                 "         |- nTickCount           : %u\n",
                 pBuffer->nTickCount);
        }

        if((pBuffer->nTimeStamp) && (pBuffer->nTimeStamp =! 0)) {
            nb_char += snprintf(BUFFER,
                 "         |- nTimeStamp           : %lld\n", pBuffer->nTimeStamp);
        }

        nb_char += snprintf(BUFFER, "         |- nFlags               :");

        if (pBuffer->nFlags & OMX_BUFFERFLAG_EOS) {
            nb_char += snprintf(BUFFER, " EOS |");
        }
        if (pBuffer->nFlags & OMX_BUFFERFLAG_STARTTIME) {
            nb_char += snprintf(BUFFER, " STARTTIME |");
        }
        if (pBuffer->nFlags & OMX_BUFFERFLAG_DECODEONLY) {
            nb_char += snprintf(BUFFER, " DECODEONLY |");
        }
        if (pBuffer->nFlags & OMX_BUFFERFLAG_DATACORRUPT) {
            nb_char += snprintf(BUFFER, " DATACORRUPT |");
        }
        if (pBuffer->nFlags & OMX_BUFFERFLAG_ENDOFFRAME) {
            nb_char += snprintf(BUFFER, " ENDOFFRAME |");
        }
        if (pBuffer->nFlags & OMX_BUFFERFLAG_SYNCFRAME) {
            nb_char += snprintf(BUFFER, " SYNCFRAME |");
        }
        if (pBuffer->nFlags & OMX_BUFFERFLAG_EXTRADATA) {
            nb_char += snprintf(BUFFER, " EXTRADATA |");
        }
        if (pBuffer->nFlags & OMX_BUFFERFLAG_CODECCONFIG) {
            nb_char += snprintf(BUFFER, " CODECONFIG |");
        }
        nb_char += snprintf(BUFFER,
                 " (0x%08x)\n"
                 "         |- nInputPortIndex      : %u\n",
                 (unsigned int) pBuffer->nFlags,
                 pBuffer->nInputPortIndex);
    } else {
        snprintf(buffer, size, "         |- NULL");
    }

    return buffer;
}

#define OMX_BUFFERDATA_STR_BYTE_FORMAT "%02x "
#define OMX_BUFFERDATA_STR_BYTE_LENGTH 3
#define OMX_BUFFERDATA_STR_SEPARATOR_FORMAT "\n [...]\n"
#define OMX_BUFFERDATA_STR_SEPARATOR_LENGTH 8

char * strOMX_BUFFERDATA(OMX_BUFFERHEADERTYPE* pBuffer,
                         char **buffer, size_t *size,
                         unsigned int nb_bytes_to_dump,
                         unsigned int nb_end_bytes,
                         unsigned int nb_bytes_per_lines)
{
    unsigned int i = 0;
    unsigned int n;
    OMX_U8* current = NULL;
    char* str = NULL;
    int dump_end = 1;
    unsigned int needed_buffer_size;

    if (nb_bytes_to_dump < 2*nb_end_bytes) {
        nb_bytes_to_dump = 2*nb_end_bytes;
    }

    needed_buffer_size = (nb_bytes_to_dump * OMX_BUFFERDATA_STR_BYTE_LENGTH) +
        OMX_BUFFERDATA_STR_SEPARATOR_LENGTH;

    if(*size < needed_buffer_size) {
        *size = needed_buffer_size;
        if(*buffer) {
            free(*buffer);
        }
        *buffer = (char *) malloc(needed_buffer_size);
    }

    if (pBuffer->nFilledLen == 0) {
        *buffer[0] = '\0';
        return *buffer;
    }

    n = nb_bytes_to_dump;

    if (pBuffer->nFilledLen <= n) {
        n = pBuffer->nFilledLen - 1;
        dump_end = 0;
    } else if (pBuffer->nFilledLen <= (n + nb_end_bytes)) {
        n = pBuffer->nFilledLen;
        dump_end = 0;
    } else {
        n -= nb_end_bytes;
        dump_end = 1;
    }

    current = (OMX_U8*)&(pBuffer->pBuffer[pBuffer->nOffset]);
    str = *buffer;

    unsigned int nb_bytes_for_line = 0;
    for (i = 0 ; i < n ; i++) {
        sprintf(str, OMX_BUFFERDATA_STR_BYTE_FORMAT, (*current));
        current++;
        str += OMX_BUFFERDATA_STR_BYTE_LENGTH;
        nb_bytes_for_line++;
        if (nb_bytes_for_line == nb_bytes_per_lines) {
            *(str-1) = '\n';
            nb_bytes_for_line = 0;
        }
    }

    if(dump_end) {
        *(str-1) = ' '; // Since BUFFERDATA_STR_SEPARATOR start with '\n' force space before
        sprintf(str, OMX_BUFFERDATA_STR_SEPARATOR_FORMAT);
        str += OMX_BUFFERDATA_STR_SEPARATOR_LENGTH;

        current = (OMX_U8*)&(pBuffer->pBuffer[pBuffer->nFilledLen]) - nb_end_bytes;
        for(i = 0 ; i < nb_end_bytes; i++) {
            sprintf(str, OMX_BUFFERDATA_STR_BYTE_FORMAT, (*current));
            current++;
            str += OMX_BUFFERDATA_STR_BYTE_LENGTH;
        }
    }
    return *buffer;
}

char * strOMX_COMMAND(OMX_COMMANDTYPE cmd, OMX_U32 param, char *buffer, size_t size)
{
    char arg_str[128];
    char command_str[128];

    arg_str[0] = '\0';

    switch(cmd) {
    case OMX_CommandFlush:
    case OMX_CommandPortDisable:
    case OMX_CommandPortEnable:
    case OMX_CommandMarkBuffer:
        sprintf(arg_str, "port %d", (int) param);
        break;
    default:
        // Nothing specific
        break;
    }
    snprintf(buffer, size, "%s - %s",
             strOMX_COMMANDTYPE(cmd, command_str, 128),
             (cmd == OMX_CommandStateSet ?
              strOMX_STATETYPE((OMX_STATETYPE) param, arg_str, 128) :
              arg_str));
    return buffer;
}

void dumpData(OMX_BUFFERHEADERTYPE* pBuffer,
              char *portType,
              char *path,
              int portIndex,
              unsigned long target_size,
              const char *comp_name,
              OMX_HANDLETYPE hComponent)
{   int written = 0;
    char str[OMX_MAX_STRINGNAME_SIZE];
    struct stat st;
    FILE *fd;
    sprintf(str, "%s%s_%s_%d_0x%08x.bin", path, comp_name, portType, (int)portIndex, (int)hComponent);

    int ret = stat(str, &st);
    if (ret) {
        mDataDumpEnabled = 1;
        mCurrentDataDumpSize = 0;
        DBGT_PINFO("%s OMX binary buffer dump started on port index %d of %s port of %s,output in file %s (dumpdata)",
                   comp_name,
                   OMX_dump_idx,
                   OMX_dump_dir ? "output" : "input",
                   OMX_dump_name,
                   str);
    }
    if (!mDataDumpEnabled) {
        return;
    }
    if (mCurrentDataDumpSize >= target_size && target_size != 0) {
        DBGT_PINFO("%s OMX binary buffer dump stopped after %d bytes (%d target size reached), remove %s file to restart dump (dumpdata).",
                   comp_name,
                   (int)mCurrentDataDumpSize,
                   (int)target_size,
                   str);
        mDataDumpEnabled = 0;
        return;
    }
    fd = fopen(str,"ab");
    if (!fd) {
        DBGT_ERROR(": Unable to open %s file.Data Dump discarded",str);
        OMX_dump_enabled = 0;
        return;
    }

    written = fwrite(pBuffer->pBuffer,1,pBuffer->nFilledLen,fd);
    mCurrentDataDumpSize += written;
    if (written != (int)pBuffer->nFilledLen) {
        DBGT_PINFO("%s OMX binary buffer dump stopped after %d bytes (no space left on device), remove %s file to restart dump (dumpdata).",
                   comp_name,
                   (int)mCurrentDataDumpSize,
                   str);
        mDataDumpEnabled = 0;
    }
    fclose(fd);
}


/** Update variables impacting data dump execution from environment properties */
void OMXDebug_Init(OMX_COMPONENTTYPE *handle, const char* name)
{
    {
        GET_PROPERTY(OMX_DUMP_NAME_PROPERTY_NAME, value, "");
        strncpy(OMX_dump_name, value, OMX_MAX_STRINGNAME_SIZE);
        OMX_dump_handle = handle;
    }

    if (strlen(OMX_dump_name)) {
        OMX_dump_enabled = 0;
        {   GET_PROPERTY(OMX_DUMP_SIZE_PROPERTY_NAME, value, "0");
            OMX_dump_size = strtoul(value, NULL, 16);
            DBGT_PINFO("%s OMX_dump_size %d (OMXDebug_Init)", name, (int)OMX_dump_size);
        }
        {
            GET_PROPERTY(OMX_DUMP_FORMAT_PROPERTY_NAME, value, "0");
            OMX_dump_hex = atoi(value);
            DBGT_PINFO("%s OMX_dump_hex %d (OMXDebug_Init)", name, OMX_dump_hex);
        }
        {
            GET_PROPERTY(OMX_DUMP_OUTPUT_PROPERTY_NAME, value, "0");
            OMX_dump_dir = atoi(value);
            DBGT_PINFO("%s OMX_dump_dir %d (OMXDebug_Init)", name, OMX_dump_dir);
        }
        {
            GET_PROPERTY(OMX_DUMP_INDEX_PROPERTY_NAME, value, "0");
            OMX_dump_idx = atoi(value);
            DBGT_PINFO("%s OMX_dump_idx %d (OMXDebug_Init)", name, OMX_dump_idx);
        }
        {
            if(!OMX_dump_hex)
            {
                GET_PROPERTY(OMX_DUMP_PATH_PROPERTY_NAME, value, "/data/dump/");
                strncpy(OMX_dump_path, value, OMX_MAX_STRINGNAME_SIZE);
                DBGT_PINFO("%s OMX_dump_path %s (OMXDebug_Init)", name, OMX_dump_path);
                if (mkdir(OMX_dump_path, S_IRWXU | S_IRWXG | S_IRWXO) != 0) {
                    if (errno != EEXIST) {
                        DBGT_ERROR(": mkdir of ' %s ' failed (err=%s), discarding data dump",
                                   OMX_dump_path,
                                   strerror(errno));
                        goto nodatadump;
                    }
                }
            }
        }

        OMX_dump_enabled = 1;
        if (OMX_dump_hex) {
            if ((OMX_dump_handle == handle) && (strcmp(OMX_dump_name, name) == 0)) {
                DBGT_PINFO("%s OMX buffer dump in hexa format enabled on %s of %s for %d bytes dumped per buffer, output in logcat (OMXDebug_Init)",
                           OMX_dump_dir ? "output" : "input",
                           name,
                           OMX_dump_name,
                           (int)OMX_dump_size ? (int)OMX_dump_size :OMX_BUFFERDATA_NB_END_BYTES*2);
            }
        } else {
            char str[64] ;
            if ((OMX_dump_handle == handle) && (strcmp(OMX_dump_name, name) == 0)) {
                sprintf(str, "%s%s_%s_%d_0x%08x.bin", OMX_dump_path, OMX_dump_name, OMX_dump_dir ?"OUT":"IN", OMX_dump_idx, (int)handle);
                DBGT_PINFO("%s OMX binary buffer dump enabled on port index %d of %s port of %s ,%s%d%s,output in file %s (OMXDebug_Init)",
                           name,
                           OMX_dump_idx,
                           OMX_dump_dir ? "output" : "input",
                           OMX_dump_name,
                           (unsigned int)OMX_dump_size ? "dump will stop after " : "",
                           (unsigned int)OMX_dump_size,
                           OMX_dump_size ? " bytes " : "",
                           str);
            }
        }
    }
    nodatadump:
    {
        GET_PROPERTY(OMX_DUMP_NAME_PROPERTY_NAME, value, "");
        strncpy(OMX_trace_name, value, OMX_MAX_STRINGNAME_SIZE);
    }
    if (OMX_dump_handle == handle) {
        if (OMX_dump_name[0] == '\0') {
            dumpBuffer = 1;
        } else {
            if (strcmp(OMX_dump_name, name)) {
                dumpBuffer = 0;
            } else {
                dumpBuffer = 1;
            }
        }
        if (OMX_trace_name[0] == '\0') {
            traceActivity = 1;
        } else {
           if (strcmp(OMX_trace_name, name)) {
                traceActivity = 0;
            } else {
                traceActivity = 0;
            }
        }
    }
}

OMX_ERRORTYPE OMXDebug_DeInit()
{
    if(OMX_dump_input_buffer)
        free(OMX_dump_input_buffer);

    if(OMX_dump_output_buffer)
        free(OMX_dump_output_buffer);

    return OMX_ErrorNone;
}
