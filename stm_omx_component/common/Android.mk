LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)

LOCAL_SRC_FILES:= \
	OMXdebug.cpp \
	OMXdebug_specific.c \
	pes.cpp \
	Role.cpp \
	Mutex.cpp \
	StmOmxPortDefinition.cpp  \
	VideoPortDefinition.cpp  \
	AudioPortDefinition.cpp  \
	OtherPortDefinition.cpp  \
	StmOmxPort.cpp  \
	ClockPort.cpp  \
	StmOmxComponent.cpp  \
	StmOmxComponentWrapper.cpp \
	link.cpp

# Includes for ICS
LOCAL_C_INCLUDES+= \
	$(TOP)/frameworks/base/include/media/stagefright/openmax \
	$(TOP)/frameworks/base/include/media/stagefright \
	$(TOP)/frameworks/native/include/media/hardware \
	$(TOP)/vendor/stm/module/stlinuxtv/linux/include

# Includes for JB
LOCAL_C_INCLUDES+= \
	$(TOP)/frameworks/native/include/media/openmax

# Common includes
LOCAL_C_INCLUDES+= \
	$(TOP)/vendor/stm/hardware/omx/include/ \
	$(TOP)/vendor/stm/hardware/header/usr/include \
	$(TOP)/vendor/stm/module/stmfb/linux/kernel/	\
	$(TOP)/vendor/stm/module/omxse \
	$(LOCAL_PATH)/inc/ 

LOCAL_SHARED_LIBRARIES := \
        libcutils \
        liblog 

LOCAL_CPPFLAGS += -DANDROID
LOCAL_CFLAGS   += -DDBGT_CONFIG_DEBUG -DDBGT_CONFIG_AUTOVAR -DDBGT_TAG=\"OMX\ \ \ \" -Werror
LOCAL_CFLAGS   += -Wno-error=switch
LOCAL_CFLAGS   += -Wno-unused-parameter
LOCAL_CFLAGS   += -DHAVE_DBGTASSERT_H
LOCAL_MODULE:= libSTMOMXCommon
LOCAL_MODULE_TAGS := optional

include $(BUILD_SHARED_LIBRARY)




# include $(CLEAR_VARS)

# LOCAL_SRC_FILES:= \
# 	src/OMXdebug.cpp \
# 	src/OMXdebug_specific.c \
# 	src/STOMXPort.cpp \
# 	src/STOMXComponent.cpp \
# 	src/STOMXComponentAsync.cpp \
# 	src/StmOmxComponentWrapper.cpp \
# 	src/pes.cpp	\
# 	src/perfmeter.cpp \
# 	src/v4lvideo.cpp src/v4laudio.cpp\
# 	src/dvbvideo.cpp src/dvbaudio.cpp

# # Includes for ICS
# LOCAL_C_INCLUDES+= \
# 	$(TOP)/frameworks/base/include/media/stagefright/openmax \
# 	$(TOP)/frameworks/base/include/media/stagefright \
# 	$(TOP)/frameworks/native/include/media/hardware

# # Includes for JB
# LOCAL_C_INCLUDES+= \
# 	$(TOP)/frameworks/native/include/media/openmax

# # Common includes
# LOCAL_C_INCLUDES+= \
# 	$(TOP)/vendor/stm/hardware/omx/include/ \
# 	$(TOP)/vendor/stm/hardware/header/usr/include \
# 	$(TOP)/vendor/stm/module/stmfb/linux/kernel/	\
# 	$(TOP)/vendor/stm/module/omxse \
# 	$(TOP)/vendor/stm/module/stlinuxtv/linux/include \
# 	$(LOCAL_PATH)/inc/ 

# LOCAL_SHARED_LIBRARIES := \
#         libcutils \
#         liblog 

# LOCAL_CPPFLAGS += -DANDROID
# LOCAL_CFLAGS   += -DDBGT_CONFIG_DEBUG -DDBGT_CONFIG_AUTOVAR -DDBGT_TAG=\"OMX\" -Werror
# LOCAL_CFLAGS   += -DHAVE_DBGTASSERT_H
# LOCAL_MODULE:= libSTMOMXCommon
# LOCAL_MODULE_TAGS := optional

# include $(BUILD_SHARED_LIBRARY)
