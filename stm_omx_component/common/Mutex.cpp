/*
 * Copyright 2013 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file   Mutex.cpp
 * @author STMicroelectronics
 */

#include "Mutex.hpp"
#include "StmOmxComponent.hpp"
#include "OMX_debug.h"

#if HAVE_ASSERT_H
#include <assert.h>
#endif

#if HAVE_DBGTASSERT_H
// Re-direct assert on DBGT_ASSERT macro
#define assert(expr)  DBGT_ASSERT(expr)
#endif

#include <linux/dbgt.h>

namespace stm {

Mutex::Mutex()
    : IMutex()
{
    pthread_mutex_init(&m_tMutex, 0);
}

Mutex::~Mutex()
{
    pthread_mutex_destroy(&m_tMutex);
}

int Mutex::lock()
{
    int err = pthread_mutex_lock(&m_tMutex);
    if (err != 0) {
        DBGT_CRITICAL("locking mutex returned error =%d", err);
    }
    return err;
}

int Mutex::release()
{
    int err = pthread_mutex_unlock(&m_tMutex);

    if (err != 0) {
        DBGT_CRITICAL("releasing mutex returned error =%d", err);
    }
    return err;
}

ScopedLock::ScopedLock(IMutex& mtx)
    : m_rMutex(mtx)
{
    m_rMutex.lock();
}

ScopedLock::~ScopedLock()
{
    m_rMutex.release();
}


Condition::Condition()
{
    pthread_cond_init(&m_cond, 0);
}

Condition::~Condition()
{
    pthread_cond_destroy(&m_cond);
}

int Condition::signal()
{
    return pthread_cond_broadcast(&m_cond);
}

int Condition::wait(Mutex& mut)
{
    return pthread_cond_wait(&m_cond, mut.mutex_t());
}

} // eof namespace stm
