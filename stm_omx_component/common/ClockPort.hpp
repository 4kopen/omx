/*
 * Copyright 2013 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file   ClockPort.hpp
 * @author STMicroelectronics
 */

#ifndef _STM_CLOCKPORT_HPP_
#define _STM_CLOCKPORT_HPP_

#include "StmOmxPort.hpp"
#include "StmOmxComponent.hpp"
#include "OtherPortDefinition.hpp"


namespace stm {

class OmxClockPort: public OmxPort
{
public:
    OmxClockPort(unsigned int nIdx, OMX_DIRTYPE eDir,
                 const OmxComponent& owner);
    virtual ~OmxClockPort();

    virtual OMX_ERRORTYPE create();
    virtual void destroy();

    virtual const OmxComponent& component() const;
    virtual const OtherPortDefinition& definition() const;

    virtual OMX_ERRORTYPE
    getParameter(OMX_INDEXTYPE nIndex, OMX_PTR pParamStruct);

    virtual OMX_ERRORTYPE
    setParameter(OMX_INDEXTYPE nIndex, OMX_PTR pParamStruct);

    virtual OMX_ERRORTYPE
    emptyThisBuffer(OMX_BUFFERHEADERTYPE* pBuffer);

    virtual OMX_ERRORTYPE
    fillThisBuffer(OMX_BUFFERHEADERTYPE* pBuffer);

protected:
    virtual OtherPortDefinition& definition();

private:
    const OmxComponent&         m_owner;
    OtherPortDefinition         m_PortDefinition;
};

} // eof namespace stm

#endif // _STM_CLOCKPORT_HPP_
