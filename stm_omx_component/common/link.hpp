/*
 * Copyright 2013 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file   link.hpp
 * @author kausik maiti(kausik.maiti@st.com)
 */

#ifndef _ST_OMX_LINK_H
#define _ST_OMX_LINK_H
#include <OMX_debug.h>
#include <string.h>
struct timeMapping_s
{
    uint64_t nTS;
    uint64_t nNewTS;
    struct timeMapping_s * next_p;
};


struct timeMapping_s *insertNode(struct timeMapping_s * front, uint64_t nTS, uint64_t nNewTS);

struct timeMapping_s *deleteNode(struct timeMapping_s * front, uint64_t nTS);

void destroyList(struct timeMapping_s * front);

#endif //_ST_OMX_LINK_H
