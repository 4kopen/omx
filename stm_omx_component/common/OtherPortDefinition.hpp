/*
 * Copyright 2013 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file   OtherPortDefinition.hpp
 * @author STMicroelectronics
 */

#ifndef _STM_OTHERPORTDEFINITION_HPP_
#define _STM_OTHERPORTDEFINITION_HPP_

#include "StmOmxPortDefinition.hpp"

namespace stm {

/**
 * Class OtherPortDefinition contains other port accessor.
 */
class OtherPortDefinition: public PortDefinition
{
public:
    /**@name Constructors and Destructors */
    //@{
    /** Constructor. */
    OtherPortDefinition(unsigned int nIdx, OMX_DIRTYPE eDir);

    OtherPortDefinition(unsigned int nIdx, OMX_DIRTYPE eDir,
                        const BufferReq& req);

    /** Copy constructor. */
    OtherPortDefinition(const OtherPortDefinition& vp);

    /** Destructor. */
    virtual
    ~OtherPortDefinition();
    //@}

    /**@name Other property getters */
    //@{
    virtual OMX_OTHER_FORMATTYPE
    format() const;
    //@}

    /**@name Video property setters */
    //@{
    virtual void
    format(OMX_OTHER_FORMATTYPE aFmt);
    //@}

    virtual void
    setParamPortDef(OMX_PARAM_PORTDEFINITIONTYPE* pType);
};

} // eof namespace stm

#endif // _STM_OTHERPORTDEFINITION_HPP_
