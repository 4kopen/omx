/*
 * Copyright 2013 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file   StmOmxComponentWrapper.h
 * @brief  Declaration of the global component interface methods used
 *         to fill OMX_COMPONENTTYPE structure
 * @author STMicroelectronics
 */


#ifndef _STM_OMXCOMPONENTWRAPPER_H_
#define _STM_OMXCOMPONENTWRAPPER_H_


#include "OMX_Core.h"
#include "OMX_Component.h"


#ifdef __cplusplus
extern "C" {
#endif

/**
 * This method sets the component interface functions.
 * here one could use the nVersion of the OMX_COMPONENTTYPE
 * to set these functions specifially for OpenMAX 1.0 version.
 */
void
STOMX_SetComponentTypeFunctions(OMX_COMPONENTTYPE* pComp);


OMX_ERRORTYPE
STOMX_SetCallbacks(OMX_HANDLETYPE hComponent,
                   OMX_CALLBACKTYPE* pCallbacks,
                   OMX_PTR pAppData);

OMX_ERRORTYPE
STOMX_GetComponentVersion(OMX_HANDLETYPE hComponent,
                          OMX_STRING cComponentName,
                          OMX_VERSIONTYPE* pComponentVersion,
                          OMX_VERSIONTYPE* pSpecVersion,
                          OMX_UUIDTYPE* pComponentUUID);

OMX_ERRORTYPE
STOMX_SendCommand(OMX_HANDLETYPE hComponent,
                  OMX_COMMANDTYPE Cmd,
                  OMX_U32 nParam1,
                  OMX_PTR pCmdData);

OMX_ERRORTYPE
STOMX_GetParameter(OMX_HANDLETYPE hComponent,
                   OMX_INDEXTYPE nParamIndex,
                   OMX_PTR ComponentParameterStructure);

OMX_ERRORTYPE
STOMX_SetParameter(OMX_HANDLETYPE hComponent,
                   OMX_INDEXTYPE nParamIndex,
                   OMX_PTR ComponentParameterStructure);

OMX_ERRORTYPE
STOMX_GetConfig(OMX_HANDLETYPE hComponent,
                OMX_INDEXTYPE nConfigIndex,
                OMX_PTR ComponentConfigStructure);

OMX_ERRORTYPE
STOMX_SetConfig(OMX_HANDLETYPE hComponent,
                OMX_INDEXTYPE nConfigIndex,
                OMX_PTR ComponentConfigStructure);

OMX_ERRORTYPE
STOMX_GetExtensionIndex(OMX_HANDLETYPE hComponent,
                        OMX_STRING cParameterName,
                        OMX_INDEXTYPE * pIndexType);

OMX_ERRORTYPE
STOMX_GetState(OMX_HANDLETYPE hComponent,
               OMX_STATETYPE* pState);

OMX_ERRORTYPE
STOMX_ComponentTunnelRequest(OMX_HANDLETYPE hComp,
                             OMX_U32 nPort,
                             OMX_HANDLETYPE hTunneledComp,
                             OMX_U32 nTunneledPort,
                             OMX_TUNNELSETUPTYPE* pTunnelSetup);

OMX_ERRORTYPE
STOMX_UseBuffer(OMX_HANDLETYPE hComponent,
                OMX_BUFFERHEADERTYPE** ppBufferHdr,
                OMX_U32 nPortIndex,
                OMX_PTR pAppPrivate,
                OMX_U32 nSizeBytes,
                OMX_U8* pBuffer);

OMX_ERRORTYPE
STOMX_AllocateBuffer(OMX_HANDLETYPE hComponent,
                     OMX_BUFFERHEADERTYPE** ppBufferHdr,
                     OMX_U32 nPortIndex,
                     OMX_PTR pAppPrivate,
                     OMX_U32 nSizeBytes);

OMX_ERRORTYPE
STOMX_FreeBuffer(OMX_HANDLETYPE hComponent,
                 OMX_U32 nPortIndex,
                 OMX_BUFFERHEADERTYPE* pBufferHdr);

OMX_ERRORTYPE
STOMX_EmptyThisBuffer(OMX_HANDLETYPE hComponent,
                      OMX_BUFFERHEADERTYPE* pBufferHdr);

OMX_ERRORTYPE
STOMX_FillThisBuffer(OMX_HANDLETYPE hComponent,
                     OMX_BUFFERHEADERTYPE* pBufferHdr);

OMX_ERRORTYPE
STOMX_UseEGLImage(OMX_HANDLETYPE hComponent,
                  OMX_BUFFERHEADERTYPE** ppBufferHdr,
                  OMX_U32 nPortIndex,
                  OMX_PTR pAppPrivate,
                  void* eglImage);


OMX_ERRORTYPE
STOMX_ComponentDeInit(OMX_HANDLETYPE hComponent);

OMX_ERRORTYPE
STOMX_componentRoleEnum(OMX_HANDLETYPE hComponent,
                        OMX_U8* cRole,
                        OMX_U32 nIndex);

#ifdef __cplusplus
} // eof extern C declaration
#endif

#endif // ifndef _STM_OMXCOMPONENTWRAPPER_H_
