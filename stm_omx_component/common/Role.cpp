/*
 * Copyright 2013 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file   Role.hpp
 * @author STMicroelectronics
 */

#include "Role.hpp"


#if HAVE_ASSERT_H
#include <assert.h>
#endif

#if HAVE_DBGTASSERT_H
// Re-direct assert on DBGT_ASSERT macro
#define assert(expr)  DBGT_ASSERT(expr)
#endif

#include <linux/dbgt.h>

namespace stm {

Role::Role()
    :m_active(0),
     m_nRoleSz(0)
{
    memset(m_roles, 0 ,sizeof(m_roles));
}

Role::Role(unsigned int nNb, const char** aStrArray)
    :m_active(0),
     m_nRoleSz(0)
{
    this->construct(nNb, aStrArray);
}

Role::~Role()
{
    for (unsigned int i = 0; i < this->size(); i++) {
        delete[] m_roles[i];
        m_roles[i] = 0;
    }
    m_nRoleSz = 0;
}

unsigned int Role::size() const
{ return this->m_nRoleSz; }

void Role::construct(unsigned int nNb, const char** aStrArray)
{
    if (!nNb) {
        return;
    }

    assert(nNb < Role::MAX_ROLES);
    assert((this->size() == 0) && "Roles are already initialized");
    for (unsigned int i = 0; i < nNb; i++) {
        this->add(aStrArray[i]);
    }
    m_nRoleSz = nNb;
}

void Role::add(const char* pRole)
{
    unsigned nNewIdx = this->size();
    assert(nNewIdx < (Role::MAX_ROLES - 1));
    m_roles[nNewIdx] = new char[Role::MAX_LENGTH - 1];
    strncpy(m_roles[nNewIdx], pRole, Role::MAX_LENGTH - 1);
    m_nRoleSz++;
}

unsigned int Role::active() const
{
    assert((this->size() != 0) && "No role initialized");
    return m_active;
}

const char* Role::active(unsigned int aActiveIndex)
{
    assert(aActiveIndex < this->size());
    const char* ret = m_roles[aActiveIndex];

    // Change the current role
    m_active = aActiveIndex;
    return ret;
}

const char* Role::at(unsigned int aIndex) const
{
    assert(aIndex < this->size());
    return m_roles[aIndex];
}

} // eof namespace stm
