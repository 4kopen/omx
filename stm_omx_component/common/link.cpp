/*
 * Copyright 2013 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file   link.cpp
 * @author kausik maiti(kausik.maiti@st.com)
 */
#include <link.hpp>
#include <stdio.h>
#include <stdlib.h>

struct timeMapping_s *insertNode(struct timeMapping_s * front, uint64_t nTS, uint64_t nNewTS)
{
    struct timeMapping_s *newNode = (struct timeMapping_s *)malloc(sizeof(struct timeMapping_s));
    struct timeMapping_s *ptr = front;
    if(newNode == NULL)
    {
        return NULL;
    }
    newNode->nTS = nTS;
    newNode->nNewTS = nNewTS;
    newNode->next_p = NULL;
    //go to the last node
    if(ptr)
    {
        while(ptr && ptr->next_p)
        {
            ptr = ptr->next_p;
        }
        ptr->next_p = newNode;
    }
    else
    {
        front = newNode;
    }
    return front;
}

struct timeMapping_s *deleteNode(struct timeMapping_s * front, uint64_t nTS)
{
    struct timeMapping_s *node= front;
    struct timeMapping_s *bNode;

    if(front == NULL)
        return front;

    if(front->nTS == nTS)
    {
       node = front;
       front = front->next_p;
       free(node);
       return front;
    }

    for(node = front->next_p, bNode = front; node!=NULL; node = node->next_p, bNode = bNode->next_p)
    {
        if(node->nTS == nTS)
        {
            bNode->next_p = node->next_p;
            free(node);
            return(front);
        }
    }
    return (front);
}

void destroyList(struct timeMapping_s * front)
{
    while(front)
    {
        struct timeMapping_s *node= front;
        front = node->next_p;
        free (node);
    }
}
