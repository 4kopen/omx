/*
 * Copyright 2013 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file   StmOmxPortDefinition.hpp
 * @author STMicroelectronics
 */

#ifndef _STM_OMXPORTDEFINITION_HPP_
#define _STM_OMXPORTDEFINITION_HPP_

#include "StmOmxIPortDefinition.hpp"


namespace stm {

/**
 * Port OMX requirements intrinsic to each OpenMAX port.
 * These requirements are negotiated during port tunneling
 */
class BufferReq
{
public:
    /** @name Constructors and Destructor */
    //@{
    /** Default constructor */
    BufferReq();

    BufferReq(OMX_U32 aBufferCountActual,
              OMX_U32 aBufferCountMin,
              OMX_U32 aBufferSizeMin,
              OMX_BOOL aContiguous,
              OMX_U32 aBufferAlignment);

    /** Constructor from OpenMAX PORTDEFINITIONTYPE structure */
    BufferReq(const OMX_PARAM_PORTDEFINITIONTYPE& portDef);

    /** Copy constructor */
    BufferReq(const BufferReq& req);

    /** Destructor */
    ~BufferReq();


public:
    /** Get  number of buffers that are required on this port before
        it is populated */
    OMX_U32
    countActual() const;

    /** Get the minimum number of buffers this port requires */
    OMX_U32
    countMin() const;

    /** Get the minimum size, in bytes, for buffers to be
        used for this port */
    OMX_U32
    sizeMin() const;

    /** Check if this port requires each buffer to be in
        contiguous memory */
    OMX_BOOL
    isContiguous() const;

    /** Return in bytes,  the alignment the port requires for each
        of its buffer */
    OMX_U32
    alignment() const;

    /** Assignement operator */
    BufferReq&
    operator=(const BufferReq& req);

    /** Method to update a PORTDEFINITIONTYPE structure with
        buffer requirement instance */
    void
    updatePortDef(OMX_PARAM_PORTDEFINITIONTYPE& portDef) const;

    /** Global method to get max of two buffer requirements */
    static BufferReq
    max(const BufferReq& a, const BufferReq& b);


private:
    /** The number of buffers that are required on this port before
        it is populated */
    OMX_U32   m_nBufferCountActual;

    /** The minimum number of buffers this port requires */
    OMX_U32   m_nBufferCountMin;

    /** Minimum size, in bytes, for buffers to be used for this port */
    OMX_U32   m_nBufferSizeMin;

    /** Boolean field that indicates this port requires each buffer
        to be in contiguous memory */
    OMX_BOOL  m_bBuffersContiguous;

    /** Specifies in bytes, the alignment the port requires for each
        of its buffer */
    OMX_U32   m_nBufferAlignment;
};

inline OMX_U32
BufferReq::countActual() const
{ return m_nBufferCountActual; }

inline OMX_U32
BufferReq::countMin() const
{ return m_nBufferCountMin; }

inline OMX_U32
BufferReq::sizeMin() const
{ return m_nBufferSizeMin; }

inline OMX_BOOL
BufferReq::isContiguous() const
{ return m_bBuffersContiguous; }

inline OMX_U32
BufferReq::alignment() const
{ return m_nBufferAlignment; }


/**
 * Class PortDefinition specifies general port information common to all
 * domain (video, image, audio). Members of that class characterize
 * each port of the component: index, direction, buffer requirements ...
 */
class PortDefinition: public IOmxPortDefinition
{
public:
    /**@name Constructors and Destructors */
    //@{
    /** Constructor. */
    PortDefinition(unsigned int nIdx, OMX_DIRTYPE eDir);

    PortDefinition(unsigned int nIdx, OMX_DIRTYPE eDir, const BufferReq& req);

    PortDefinition(const PortDefinition& p);

    /** Destructor. */
    virtual ~PortDefinition();
    //@}

    /**@name Property getters */
    //@{
    virtual unsigned int index() const;

    virtual bool isEnabled() const;

    virtual bool isPopulated() const;

    virtual OMX_PORTDOMAINTYPE domain() const;

    virtual OMX_DIRTYPE direction() const;

    virtual unsigned int
    bufferCountActual() const;

    virtual unsigned int
    bufferCountMin() const;

    virtual unsigned int
    bufferSize() const;

    virtual bool
    isBuffersContiguous() const;

    virtual unsigned int
    bufferAlignment() const;
    //@}

    /**@name Property setters */
    //@{
    void
    bufferCountActual(unsigned int aCount);

    void
    bufferCountMin(unsigned int aCount);

    void
    bufferSize(unsigned int aSize);

    void
    buffersContiguous(bool aCont);

    void
    bufferAlignment(unsigned int aAlign);

    virtual void
    enabled();

    virtual void
    disabled();

    /** Turn internal port state to populated */
    virtual void
    populated();

    /** Turn internal port state to not populated */
    virtual void
    unpopulated();
    //@}

    /** Return buffer requirements of that port */
    BufferReq bufferReq() const;

    /**
	 * Populate OMX structure OMX_PARAM_PORTDEFINITIONTYPE
	 * Called by OMX getParameter calls
	 */
    OMX_PARAM_PORTDEFINITIONTYPE
    getParamPortDef() const;

    /**
	 * Update some read/write port definition settings with OMX structure
     * OMX_PARAM_PORTDEFINITIONTYPE.
     * Called by OMX setParameter calls
	 */
    void
    setParamPortDef(OMX_PARAM_PORTDEFINITIONTYPE* pType);

protected:
    OMX_PARAM_PORTDEFINITIONTYPE  m_sPortDef;
};

} // eof namespace stm

#endif // _STM_OMXPORTDEFINITION_HPP_
