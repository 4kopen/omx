/*
 * Copyright 2013 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file   VideoPortDefinition.cpp
 * @author STMicroelectronics
 */

#include "VideoPortDefinition.hpp"
#include "OMX_debug.h"

#if HAVE_ASSERT_H
#include <assert.h>
#endif

#if HAVE_DBGTASSERT_H
// Re-direct assert on DBGT_ASSERT macro
#define assert(expr)  DBGT_ASSERT(expr)
#endif

#include <linux/dbgt.h>

#include <string.h>


namespace stm {

//=============================================================================
// VideoPortDefinition implementation
//=============================================================================

VideoPortDefinition::VideoPortDefinition(unsigned int nIdx, OMX_DIRTYPE eDir)
    :PortDefinition(nIdx, eDir)
{
    m_sPortDef.eDomain = OMX_PortDomainVideo;
    m_sPortDef.format.video.nFrameWidth = 0;
    m_sPortDef.format.video.nFrameHeight = 0;
    m_sPortDef.format.video.nStride = 0;
    m_sPortDef.format.video.nSliceHeight = 0;
    m_sPortDef.format.video.nBitrate = 0;
    m_sPortDef.format.video.xFramerate = 0;
    m_sPortDef.format.video.bFlagErrorConcealment = OMX_FALSE;
    m_sPortDef.format.video.eCompressionFormat = OMX_VIDEO_CodingUnused;
    m_sPortDef.format.video.eColorFormat = OMX_COLOR_FormatUnused;
    m_strMIMEType[0] = '\0';
    m_sPortDef.format.video.cMIMEType = m_strMIMEType;
}

VideoPortDefinition::~VideoPortDefinition()
{}

VideoPortDefinition::VideoPortDefinition(unsigned int nIdx, OMX_DIRTYPE eDir,
                                         const BufferReq& aReq)
    :PortDefinition(nIdx, eDir, aReq)
{
    m_sPortDef.eDomain = OMX_PortDomainVideo;
    m_sPortDef.format.video.nFrameWidth = 0;
    m_sPortDef.format.video.nFrameHeight = 0;
    m_sPortDef.format.video.nStride = 0;
    m_sPortDef.format.video.nSliceHeight = 0;
    m_sPortDef.format.video.nBitrate = 0;
    m_sPortDef.format.video.xFramerate = 0;
    m_sPortDef.format.video.bFlagErrorConcealment = OMX_FALSE;
    m_sPortDef.format.video.eCompressionFormat = OMX_VIDEO_CodingUnused;
    m_sPortDef.format.video.eColorFormat = OMX_COLOR_FormatUnused;
    m_strMIMEType[0] = '\0';
    m_sPortDef.format.video.cMIMEType = m_strMIMEType;
}

VideoPortDefinition::VideoPortDefinition(const VideoPortDefinition& vp)
    :PortDefinition(vp)
{
    m_sPortDef.eDomain = OMX_PortDomainVideo;
    m_sPortDef.format.video.nFrameWidth = vp.frameWidth();
    m_sPortDef.format.video.nFrameHeight = vp.frameHeight();
    m_sPortDef.format.video.nStride = vp.stride();
    m_sPortDef.format.video.nSliceHeight = vp.sliceHeight();
    m_sPortDef.format.video.nBitrate = vp.bitRate();
    m_sPortDef.format.video.xFramerate = vp.frameRate();
    m_sPortDef.format.video.bFlagErrorConcealment =
        (vp.flagErrorConcealment()? OMX_TRUE: OMX_FALSE);
    m_sPortDef.format.video.eCompressionFormat = vp.compressionFormat();
    m_sPortDef.format.video.eColorFormat = vp.colorFormat();
    //check the string length before copy
    assert(strlen(vp.contentType()) <= kMimeTypeSz);
    strcpy(m_strMIMEType, vp.contentType());
    m_sPortDef.format.video.cMIMEType = m_strMIMEType;
}

const char*
VideoPortDefinition::contentType() const
{ return m_strMIMEType;}

unsigned int
VideoPortDefinition::frameWidth() const
{ return m_sPortDef.format.video.nFrameWidth;}

unsigned int
VideoPortDefinition::xFrameRate() const
{ return m_sPortDef.format.video.xFramerate;}


unsigned int
VideoPortDefinition::frameHeight() const
{ return m_sPortDef.format.video.nFrameHeight;}

int
VideoPortDefinition::stride() const
{ return m_sPortDef.format.video.nStride;}

unsigned int
VideoPortDefinition::sliceHeight() const
{ return m_sPortDef.format.video.nSliceHeight;}

unsigned int
VideoPortDefinition::bitRate() const
{ return m_sPortDef.format.video.nBitrate;}

unsigned int
VideoPortDefinition::frameRate() const
{ return m_sPortDef.format.video.xFramerate;}

bool
VideoPortDefinition::flagErrorConcealment() const
{ return m_sPortDef.format.video.bFlagErrorConcealment;}

OMX_VIDEO_CODINGTYPE
VideoPortDefinition::compressionFormat() const
{ return m_sPortDef.format.video.eCompressionFormat;}

OMX_COLOR_FORMATTYPE
VideoPortDefinition::colorFormat() const
{ return m_sPortDef.format.video.eColorFormat;}

void
VideoPortDefinition::mimeType(const char * cMime)
{
    m_sPortDef.format.video.cMIMEType = (char *)cMime;
}

void
VideoPortDefinition::contentType(const char* mimeType)
{
    //check the string length before copy
    assert(strlen(mimeType) <= kMimeTypeSz);
    strcpy(m_strMIMEType, mimeType);
}

void
VideoPortDefinition::frameWidth(unsigned int aWidth)
{ m_sPortDef.format.video.nFrameWidth = aWidth; }

void
VideoPortDefinition::frameHeight(unsigned int aHeight)
{ m_sPortDef.format.video.nFrameHeight = aHeight; }

void
VideoPortDefinition::stride(int aStride)
{ m_sPortDef.format.video.nStride = aStride; }

void
VideoPortDefinition::sliceHeight(unsigned int aSliceHeight)
{ m_sPortDef.format.video.nSliceHeight = aSliceHeight; }

void
VideoPortDefinition::bitRate(unsigned int aBitRate)
{ m_sPortDef.format.video.nBitrate = aBitRate; }

void
VideoPortDefinition::frameRate(unsigned int aFrameRate)
{ m_sPortDef.format.video.xFramerate = aFrameRate; }

void
VideoPortDefinition::flagErrorConcealment(bool aERC)
{ m_sPortDef.format.video.bFlagErrorConcealment = (aERC? OMX_TRUE: OMX_FALSE); }

void
VideoPortDefinition::compressionFormat(OMX_VIDEO_CODINGTYPE aFmt)
{
    m_sPortDef.format.video.eCompressionFormat = aFmt;
    m_sPortDef.format.video.eColorFormat = OMX_COLOR_FormatUnused;

    switch(aFmt)
    {
    case OMX_VIDEO_CodingAVC:
        strcpy(m_strMIMEType, "video/avc"); break;
    case OMX_VIDEO_CodingMPEG4:
        strcpy(m_strMIMEType, "video/mp4"); break;
    case OMX_VIDEO_CodingH263:
        strcpy(m_strMIMEType, "video/h263"); break;
    case OMX_VIDEO_CodingMPEG2:
        strcpy(m_strMIMEType, "video/mpeg"); break;
    case OMX_VIDEO_CodingVC1:
        strcpy(m_strMIMEType, "video/vc1"); break;
    case OMX_VIDEO_CodingWMV:
        strcpy(m_strMIMEType, "video/x-ms-wmv"); break;
    case OMX_VIDEO_CodingFLV1:
        strcpy(m_strMIMEType, "video/x-flv"); break;
    case OMX_VIDEO_CodingMJPEG:
        strcpy(m_strMIMEType, "video/mjpeg"); break;
    case OMX_VIDEO_CodingVP8:
        strcpy(m_strMIMEType, "video/vp8"); break;
    case OMX_VIDEO_CodingVP9:
        strcpy(m_strMIMEType, "video/vp9"); break;
    case OMX_VIDEO_CodingHEVC:
        strcpy(m_strMIMEType, "video/hevc"); break;
    default:
        // Now Handle OMX_VIDEO_CODINGEXTTYPE
        compressionFormat((OMX_VIDEO_CODINGEXTTYPE) aFmt);
    }
}

void
VideoPortDefinition::compressionFormat(OMX_VIDEO_CODINGEXTTYPE aFmt)
{
    switch(aFmt)
    {
    case OMX_VIDEO_CodingSorenson:
        strcpy(m_strMIMEType, "video/x-flv"); break;
    case OMX_VIDEO_CodingVPX:
        strcpy(m_strMIMEType, "video/vp8"); break;
    case OMX_VIDEO_CodingTHEORA:
        strcpy(m_strMIMEType, "video/theora"); break;
    case OMX_VIDEO_CodingCAVS:
        strcpy(m_strMIMEType, "video/avs"); break;
    default:
        DBGT_WARNING("Video coding format mime type not supported (%d)",
                     aFmt);
        strcpy(m_strMIMEType, "video/undef");
    }
}

void
VideoPortDefinition::colorFormat(OMX_COLOR_FORMATTYPE aFmt)
{
    m_sPortDef.format.video.eColorFormat = aFmt;
    m_sPortDef.format.video.eCompressionFormat = OMX_VIDEO_CodingUnused;

    switch(aFmt)
    {
    case OMX_COLOR_FormatYUV420SemiPlanar:
        strcpy(m_strMIMEType, "video/nv12"); break;
    case OMX_COLOR_Format32bitBGRA8888:
        strcpy(m_strMIMEType, "video/BGRA8888"); break;
    case OMX_COLOR_Format16bitRGB565:
        strcpy(m_strMIMEType, "video/RGB565"); break;
    case OMX_STM_COLOR_FormatYVU420SemiPlanarNV21:
        strcpy(m_strMIMEType, "video/nv21"); break;
    default:
        DBGT_WARNING("eColorFormat mime type not supported (%d)",
                     aFmt);
        strcpy(m_strMIMEType, "video/undef");
    }
}

void
VideoPortDefinition::xFrameRate(unsigned int frameRate)
{
    m_sPortDef.format.video.xFramerate = frameRate;
}

void
VideoPortDefinition::setParamPortDef(OMX_PARAM_PORTDEFINITIONTYPE* pType)
{
    PortDefinition::setParamPortDef(pType);

    assert(pType->eDomain == OMX_PortDomainVideo);
    if (pType->format.video.cMIMEType) {
        this->contentType(pType->format.video.cMIMEType);
    } else {
        this->contentType("");
    }

    this->frameWidth(pType->format.video.nFrameWidth);
    this->frameHeight(pType->format.video.nFrameHeight);
    this->stride(pType->format.video. nStride);
    this->sliceHeight(pType->format.video.nSliceHeight);
    this->bitRate(pType->format.video.nBitrate);
    this->frameRate(pType->format.video.xFramerate);
    this->flagErrorConcealment(pType->format.video.bFlagErrorConcealment);

    assert(pType->format.video.eCompressionFormat != OMX_VIDEO_CodingUnused ||
           pType->format.video.eColorFormat != OMX_COLOR_FormatUnused);

    if (pType->format.video.eCompressionFormat != OMX_VIDEO_CodingUnused) {
        assert(pType->format.video.eColorFormat == OMX_COLOR_FormatUnused);
        this->compressionFormat(pType->format.video.eCompressionFormat);
    }

    if (pType->format.video.eColorFormat != OMX_COLOR_FormatUnused) {
        assert(pType->format.video.eCompressionFormat == OMX_VIDEO_CodingUnused);
        this->colorFormat(pType->format.video.eColorFormat);
    }

    // Unused members
    //pType->format.video.pNativeRender
    //pType->format.video.pNativeWindow
}

} // eof namespace stm
