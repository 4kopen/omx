/*
 * Copyright 2011 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file   StmOmxComponentWrapper.cpp
 * @brief  Implementation of the global component interface methods used
 *         in OMX_COMPONENTTYPE structure
 * @author STMicroelectronics
 */


#include "StmOmxIComponent.hpp"
#include "StmOmxIPortDefinition.hpp"
#include "StmOmxIPort.hpp"

#include "OMX_debug.h"

#if HAVE_ASSERT_H
#include <assert.h>
#endif

#if HAVE_DBGTASSERT_H
// Re-direct assert on DBGT_ASSERT macro
#define assert(expr)  DBGT_ASSERT(expr)
#endif

#define DBGT_PREFIX "OMXW"
#define DBGT_LAYER  0
#include <linux/dbgt.h>

#if HAVE_STRING_H
#include <string.h>
#endif

// Check parameter is non Null
#define CHECK_PARAM_NON_NULL(param)            \
    if (param == 0) {                          \
        DBGT_ERROR("null parameter " # param); \
        DBGT_EPILOG("bad parameter");          \
        return OMX_ErrorBadParameter;          \
    }

// Check component is not in invalid state
#define CHECK_VALID_STATE(pComp)                               \
    if (pComp->omxState() == OMX_StateInvalid) {               \
        DBGT_ERROR("Component in state OMX_StateInvalid");     \
        DBGT_EPILOG("Incorrect state operation");              \
        return OMX_ErrorIncorrectStateOperation;               \
    }

// Check returned OMX error, and display message if different from ErrorNone
#define CHECK_OMX_ERROR(eError)                               \
    if (eError != OMX_ErrorNone) {                            \
        DBGT_ERROR("returned with error %s(0x%x)",            \
                   OMX_TYPE_TO_STR(OMX_ERRORTYPE, eError),    \
                   (unsigned int)eError);                     \
    }


#define GET_COMP(_comp) \
    ((stm::IOmxComponent*)(((OMX_COMPONENTTYPE*)_comp)->pComponentPrivate))

#define GET_CST_COMP(_comp) \
    ((const stm::IOmxComponent*)(((OMX_COMPONENTTYPE*)_comp)->pComponentPrivate))

#ifdef __cplusplus
extern "C" {
#endif

//=============================================================================
// STOMX_SetCallbacks
//=============================================================================
OMX_ERRORTYPE STOMX_SetCallbacks(OMX_HANDLETYPE hComponent,
                                 OMX_CALLBACKTYPE* pCallbacks,
                                 OMX_PTR pAppData)
{
    DBGT_PROLOG();
    CHECK_PARAM_NON_NULL(hComponent);
    CHECK_PARAM_NON_NULL(pCallbacks);

    stm::IOmxComponent* pComp = GET_COMP(hComponent);
    assert(pComp);

    DBGT_PTRACE("%s Set Callbacks :(callbacks: 0x%08x)",
                pComp->name(), (unsigned int)pCallbacks);

    OMX_ERRORTYPE eError = pComp->setCallbacks(pCallbacks, pAppData);
    CHECK_OMX_ERROR(eError);
    DBGT_EPILOG();
    return  eError;
}

//=============================================================================
// STOMX_GetComponentVersion
//=============================================================================
OMX_ERRORTYPE STOMX_GetComponentVersion(OMX_HANDLETYPE hComponent,
                                        OMX_STRING cComponentName,
                                        OMX_VERSIONTYPE* pComponentVersion,
                                        OMX_VERSIONTYPE* pSpecVersion,
                                        OMX_UUIDTYPE* pComponentUUID)
{
    DBGT_PROLOG();
    CHECK_PARAM_NON_NULL(hComponent);
    CHECK_PARAM_NON_NULL(cComponentName);
    CHECK_PARAM_NON_NULL(pComponentVersion);
    CHECK_PARAM_NON_NULL(pSpecVersion);
    CHECK_PARAM_NON_NULL(pComponentUUID);

    stm::IOmxComponent* pComp = GET_COMP(hComponent);
    assert(pComp);

    strncpy(cComponentName, pComp->name(), OMX_MAX_STRINGNAME_SIZE - 1);

    OMX_ERRORTYPE eError = pComp->getComponentVersion(pComponentVersion,
                                                      pSpecVersion,
                                                      pComponentUUID);

    OMXDebug_GetComponentVersion(hComponent,
                                 cComponentName,
                                 pComponentVersion,
                                 pSpecVersion,
                                 pComponentUUID,
                                 pComp->name());
    CHECK_OMX_ERROR(eError);
    DBGT_EPILOG();
    return  eError;
}

//=============================================================================
// STOMX_SendCommand
//=============================================================================
OMX_ERRORTYPE STOMX_SendCommand(OMX_HANDLETYPE hComponent,
                                OMX_COMMANDTYPE aCmd,
                                OMX_U32 nParam,
                                OMX_PTR pCmdData)
{
    DBGT_PROLOG();
    CHECK_PARAM_NON_NULL(hComponent);

    stm::IOmxComponent* pComp = GET_COMP(hComponent);
    assert(pComp);
    CHECK_VALID_STATE(pComp);

    // Check port index in case of port command type
    if (aCmd == OMX_CommandPortDisable ||
        aCmd == OMX_CommandPortEnable ||
        aCmd == OMX_CommandFlush) {
        if ((nParam != OMX_ALL) &&
            (nParam >= pComp->portNb())) {
            DBGT_ERROR("Invalid port index %u", nParam);
            DBGT_EPILOG("Bad port index");
            return OMX_ErrorBadPortIndex;
        }
    }

    OMXDebug_SendCommand(hComponent,
                         aCmd,
                         nParam,
                         pCmdData,
                         pComp->name());

    OMX_ERRORTYPE eError =  pComp->sendCommand(aCmd, nParam, pCmdData);
    CHECK_OMX_ERROR(eError);
    DBGT_EPILOG();
    return  eError;
}

//=============================================================================
// STOMX_GetParameter
// allows to get OMX comonent parameters
//=============================================================================
OMX_ERRORTYPE STOMX_GetParameter(OMX_HANDLETYPE hComponent,
                                 OMX_INDEXTYPE nParamIndex,
                                 OMX_PTR pParamStruct)
{
    DBGT_PROLOG();
    CHECK_PARAM_NON_NULL(hComponent);
    CHECK_PARAM_NON_NULL(pParamStruct);

    stm::IOmxComponent* pComp = GET_COMP(hComponent);
    assert(pComp);
    CHECK_VALID_STATE(pComp);

    OMX_ERRORTYPE eError = pComp->getParameter(nParamIndex, pParamStruct);
    OMXDebug_GetParameter(hComponent,
                          nParamIndex,
                          pParamStruct,
                          pComp->name());
    CHECK_OMX_ERROR(eError);
    DBGT_EPILOG();
    return  eError;
}

//=============================================================================
// STOMX_SetParameter
// allows to set OMX component parameters
//=============================================================================
OMX_ERRORTYPE STOMX_SetParameter(OMX_HANDLETYPE hComponent,
                                 OMX_INDEXTYPE nParamIndex,
                                 OMX_PTR pParamStruct)
{
    DBGT_PROLOG();
    CHECK_PARAM_NON_NULL(hComponent);
    CHECK_PARAM_NON_NULL(pParamStruct);

    stm::IOmxComponent* pComp = GET_COMP(hComponent);
    assert(pComp);
    CHECK_VALID_STATE(pComp);

    OMXDebug_SetParameter(hComponent,
                          nParamIndex,
                          pParamStruct,
                          pComp->name());

    OMX_ERRORTYPE eError = pComp->setParameter(nParamIndex, pParamStruct);
    CHECK_OMX_ERROR(eError);
    DBGT_EPILOG();
    return  eError;
}

//=============================================================================
// STOMX_GetConfig
//=============================================================================
OMX_ERRORTYPE STOMX_GetConfig(OMX_HANDLETYPE hComponent,
                              OMX_INDEXTYPE nConfigIndex,
                              OMX_PTR pConfigStruct)
{
    DBGT_PROLOG();
    CHECK_PARAM_NON_NULL(hComponent);
    CHECK_PARAM_NON_NULL(pConfigStruct);

    stm::IOmxComponent* pComp = GET_COMP(hComponent);
    assert(pComp);
    CHECK_VALID_STATE(pComp);

    OMX_ERRORTYPE eError = pComp->getConfig(nConfigIndex, pConfigStruct);

    OMXDebug_GetConfig(hComponent,
                       nConfigIndex,
                       pConfigStruct,
                       pComp->name());
    CHECK_OMX_ERROR(eError);
    DBGT_EPILOG();
    return  eError;
}

//=============================================================================
// STOMX_SetConfig
//=============================================================================
OMX_ERRORTYPE STOMX_SetConfig(OMX_HANDLETYPE hComponent,
                              OMX_INDEXTYPE nConfigIndex,
                              OMX_PTR pConfigStruct)
{
    DBGT_PROLOG();
    CHECK_PARAM_NON_NULL(hComponent);
    CHECK_PARAM_NON_NULL(pConfigStruct);

    stm::IOmxComponent* pComp = GET_COMP(hComponent);
    assert(pComp);
    CHECK_VALID_STATE(pComp);

    OMXDebug_SetConfig(hComponent,
                       nConfigIndex,
                       pConfigStruct,
                       pComp->name());

    OMX_ERRORTYPE eError = pComp->setConfig(nConfigIndex, pConfigStruct);
    CHECK_OMX_ERROR(eError);
    DBGT_EPILOG();
    return  eError;
}

//=============================================================================
// STOMX_GetExtensionIndex
//=============================================================================
OMX_ERRORTYPE STOMX_GetExtensionIndex(OMX_HANDLETYPE hComponent,
                                      OMX_STRING cParameterName,
                                      OMX_INDEXTYPE* pIndexType)
{
    DBGT_PROLOG();
    CHECK_PARAM_NON_NULL(hComponent);
    CHECK_PARAM_NON_NULL(cParameterName);
    CHECK_PARAM_NON_NULL(pIndexType);

    stm::IOmxComponent* pComp = GET_COMP(hComponent);
    assert(pComp);

    DBGT_PDEBUG("%s GetExtensionIndex cParameterName=%s",
                pComp->name(), cParameterName);

    OMX_ERRORTYPE eError = pComp->getExtensionIndex(cParameterName,
                                                    pIndexType);
    CHECK_OMX_ERROR(eError);
    DBGT_EPILOG();
    return eError;
}

//=============================================================================
// STOMX_GetState
//=============================================================================
OMX_ERRORTYPE STOMX_GetState(OMX_HANDLETYPE hComponent,
                             OMX_STATETYPE* pState)
{
    DBGT_PROLOG();
    CHECK_PARAM_NON_NULL(hComponent);
    CHECK_PARAM_NON_NULL(pState);

    stm::IOmxComponent* pComp = GET_COMP(hComponent);
    assert(pComp);

    *pState = pComp->omxState();

    DBGT_PTRACE("%s Get State: %s",
                pComp->name(),
                OMX_TYPE_TO_STR(OMX_STATETYPE, *pState));

    DBGT_EPILOG();
    return OMX_ErrorNone;
}

//=============================================================================
// STOMX_ComponentTunnelRequest
//=============================================================================
OMX_ERRORTYPE STOMX_ComponentTunnelRequest(OMX_HANDLETYPE hComponent,
                                           OMX_U32 nPort,
                                           OMX_HANDLETYPE hTunneledComp,
                                           OMX_U32 nTunneledPort,
                                           OMX_TUNNELSETUPTYPE* pTunnelSetup)
{
    DBGT_PROLOG();
    CHECK_PARAM_NON_NULL(hComponent);
    CHECK_PARAM_NON_NULL(hTunneledComp);
    CHECK_PARAM_NON_NULL(pTunnelSetup);

    stm::IOmxComponent* pComp = GET_COMP(hComponent);
    assert(pComp);

    // Check if component state is loaded
    if (pComp->omxState() != OMX_StateLoaded) {
        DBGT_ERROR("Component can not setup TunnelRequest in Loaded state");
        DBGT_EPILOG("Incorrect state operation");
        return OMX_ErrorIncorrectStateOperation;
    }

    DBGT_PTRACE("%s TunnelRequest: on %s port",
                pComp->name(),
                (int) nPort == 0 ? "input" : "output");

    OMX_ERRORTYPE eError = pComp->componentTunnelRequest(nPort,
                                                         hTunneledComp,
                                                         nTunneledPort,
                                                         pTunnelSetup);

    CHECK_OMX_ERROR(eError);
    DBGT_EPILOG();
    return eError;
}

//=============================================================================
// STOMX_UseBuffer
//=============================================================================
OMX_ERRORTYPE STOMX_UseBuffer(OMX_HANDLETYPE hComponent,
                              OMX_BUFFERHEADERTYPE** ppBufferHdr,
                              OMX_U32 nPortIndex,
                              OMX_PTR pAppPrivate,
                              OMX_U32 nSizeBytes,
                              OMX_U8* pBuffer)
{
    DBGT_PROLOG();
    CHECK_PARAM_NON_NULL(hComponent);
    CHECK_PARAM_NON_NULL(ppBufferHdr);
    CHECK_PARAM_NON_NULL(pAppPrivate);
    CHECK_PARAM_NON_NULL(pBuffer);

    stm::IOmxComponent* pComp = GET_COMP(hComponent);
    assert(pComp);

    // Check port index
    if (nPortIndex >= pComp->portNb()) {
        DBGT_ERROR("Invalid port index %u", nPortIndex);
        DBGT_EPILOG("Bad port index");
        return OMX_ErrorBadPortIndex;
    }

    // Check port status and parameters
    const stm::IOmxPort& rPort = GET_CST_COMP(hComponent)->getPort(nPortIndex);
    if (nSizeBytes < rPort.definition().bufferSize()) {
        DBGT_ERROR("Different port allocation size");
        DBGT_EPILOG("Bad allocation size");
        return OMX_ErrorBadParameter;
    }
    if (rPort.definition().isPopulated()) {
        DBGT_ERROR("Port is already populated");
        DBGT_EPILOG("Invalid allocation request");
        return OMX_ErrorBadParameter;
    }

    DBGT_PTRACE("%s Use Buffer: 0x%08x on %s port length %d",
                pComp->name(),
                (int)pBuffer,
                (int)nPortIndex == 0 ? "Input" : "Output",
                (int)nSizeBytes);

    OMX_ERRORTYPE eError = pComp->useBuffer(ppBufferHdr,
                                            nPortIndex,
                                            pAppPrivate,
                                            nSizeBytes,
                                            pBuffer);
    CHECK_OMX_ERROR(eError);
    DBGT_EPILOG();
    return eError;
}

//=============================================================================
// STOMX_AllocateBuffer
//=============================================================================
OMX_ERRORTYPE STOMX_AllocateBuffer(OMX_HANDLETYPE hComponent,
                                   OMX_BUFFERHEADERTYPE** ppBufferHdr,
                                   OMX_U32 nPortIndex,
                                   OMX_PTR pAppPrivate,
                                   OMX_U32 nSizeBytes)
{
    DBGT_PROLOG();
    CHECK_PARAM_NON_NULL(hComponent);
    CHECK_PARAM_NON_NULL(ppBufferHdr);

    stm::IOmxComponent* pComp = GET_COMP(hComponent);
    assert(pComp);

    // Check port index
    if (nPortIndex >= pComp->portNb()) {
        DBGT_ERROR("Invalid port index %u", nPortIndex);
        DBGT_EPILOG("Bad port index");
        return OMX_ErrorBadPortIndex;
    }

    // Check port status and parameters
    const stm::IOmxPort& rPort = GET_CST_COMP(hComponent)->getPort(nPortIndex);
    if (!rPort.definition().isEnabled()) {
        DBGT_ERROR("Port is disabled");
        DBGT_EPILOG("Incorrect state operation");
        return OMX_ErrorIncorrectStateOperation;
    }
    if (nSizeBytes < rPort.definition().bufferSize()) {
#ifdef ANDROID
        DBGT_ERROR("Different port allocation size");
        DBGT_EPILOG("Bad allocation size");
        return OMX_ErrorBadParameter;
#else
        DBGT_PINFO("Different port allocation size");
        nSizeBytes = rPort.definition().bufferSize();
#endif
    }
    if (rPort.definition().isPopulated()) {
        DBGT_ERROR("Port is already populated");
        DBGT_EPILOG("Invalid allocation request");
        return OMX_ErrorBadParameter;
    }

    OMX_ERRORTYPE eError = pComp->allocateBuffer(ppBufferHdr,
                                                 nPortIndex,
                                                 pAppPrivate,
                                                 nSizeBytes);
    DBGT_PTRACE("%s Allocate Buffer: 0x%08x on %s port length %d",
                pComp->name(),
                (int)(*ppBufferHdr)->pBuffer,
                (int) nPortIndex == 0 ? "input" : "output",
                (int) nSizeBytes);

    CHECK_OMX_ERROR(eError);
    DBGT_EPILOG();
    return eError;
}

//=============================================================================
// STOMX_FreeBuffer
//=============================================================================
OMX_ERRORTYPE STOMX_FreeBuffer(OMX_HANDLETYPE hComponent,
                               OMX_U32 nPortIndex,
                               OMX_BUFFERHEADERTYPE* pBufferHdr)
{
    DBGT_PROLOG();
    CHECK_PARAM_NON_NULL(hComponent);
    CHECK_PARAM_NON_NULL(pBufferHdr);

    stm::IOmxComponent* pComp = GET_COMP(hComponent);
    assert(pComp);

    // Check port index
    if (nPortIndex >= pComp->portNb()) {
        DBGT_ERROR("Invalid port index %u", nPortIndex);
        DBGT_EPILOG("Bad port index");
        return OMX_ErrorBadPortIndex;
    }

    DBGT_PTRACE("%s Free Buffer: 0x%08x, on %s Port",
                pComp->name(),
                (int)pBufferHdr->pBuffer,
                (int)nPortIndex == 0 ? "Input" : "Output");

    OMX_ERRORTYPE eError = pComp->freeBuffer(nPortIndex,
                                             pBufferHdr);
    CHECK_OMX_ERROR(eError);
    DBGT_EPILOG();
    return eError;
}

//=============================================================================
// STOMX_EmptyThisBuffer
//=============================================================================
OMX_ERRORTYPE STOMX_EmptyThisBuffer(OMX_HANDLETYPE hComponent,
                                    OMX_BUFFERHEADERTYPE* pBufferHdr)
{
    DBGT_PROLOG();
    CHECK_PARAM_NON_NULL(hComponent);
    CHECK_PARAM_NON_NULL(pBufferHdr);

    stm::IOmxComponent* pComp = GET_COMP(hComponent);
    assert(pComp);

    // Check if component state is executing or pause
    if ((pComp->omxState() != OMX_StateExecuting) &&
        (pComp->omxState() != OMX_StatePause)) {
        DBGT_ERROR("Component can not transfer buffers in that state");
        DBGT_EPILOG("Incorrect state operation");
        return OMX_ErrorIncorrectStateOperation;
    }

    // Check port index
    if (pBufferHdr->nInputPortIndex >= pComp->portNb()) {
        DBGT_ERROR("Invalid port index %u", pBufferHdr->nInputPortIndex);
        DBGT_EPILOG("Bad port index");
        return OMX_ErrorBadPortIndex;
    }

    // Check port status and parameters
    const stm::IOmxPort& rPort =
        GET_CST_COMP(hComponent)->getPort(pBufferHdr->nInputPortIndex);
    if (!rPort.definition().isEnabled()) {
        DBGT_ERROR("Port is disabled");
        DBGT_EPILOG("Incorrect state operation");
        return OMX_ErrorIncorrectStateOperation;
    }
    if (OMX_DirInput != rPort.definition().direction()) {
        DBGT_ERROR("Port is not an input port");
        DBGT_EPILOG("Invalid operation on port");
        return OMX_ErrorIncorrectStateOperation;
    }

    OMXDebug_EmptyThisBuffer(hComponent, pBufferHdr, pComp->name());

    OMX_ERRORTYPE eError = pComp->emptyThisBuffer(pBufferHdr);
    CHECK_OMX_ERROR(eError);
    DBGT_EPILOG();
    return eError;
}

//=============================================================================
// STOMX_FillThisBuffer
//=============================================================================
OMX_ERRORTYPE STOMX_FillThisBuffer(OMX_HANDLETYPE hComponent,
                                   OMX_BUFFERHEADERTYPE* pBufferHdr)
{
    DBGT_PROLOG();
    CHECK_PARAM_NON_NULL(hComponent);
    CHECK_PARAM_NON_NULL(pBufferHdr);

    stm::IOmxComponent* pComp = GET_COMP(hComponent);
    assert(pComp);

    // Check if component state is executing or pause
    if ((pComp->omxState() != OMX_StateExecuting) &&
        (pComp->omxState() != OMX_StatePause)) {
        DBGT_ERROR("Component can not transfer buffers in that state");
        DBGT_EPILOG("Incorrect state operation");
        return OMX_ErrorIncorrectStateOperation;
    }

    // Check port index
    if (pBufferHdr->nOutputPortIndex >= pComp->portNb()) {
        DBGT_ERROR("Invalid port index %u", pBufferHdr->nInputPortIndex);
        DBGT_EPILOG("Bad port index");
        return OMX_ErrorBadPortIndex;
    }

    // Check port status and parameters
    const stm::IOmxPort& rPort =
        GET_CST_COMP(hComponent)->getPort(pBufferHdr->nOutputPortIndex);
    if (!rPort.definition().isEnabled()) {
        DBGT_ERROR("Port is disabled");
        DBGT_EPILOG("Incorrect state operation");
        return OMX_ErrorIncorrectStateOperation;
    }
    if (OMX_DirOutput != rPort.definition().direction()) {
        DBGT_ERROR("Port is not an output port");
        DBGT_EPILOG("Invalid operation on port");
        return OMX_ErrorIncorrectStateOperation;
    }

    OMXDebug_FillThisBuffer(hComponent, pBufferHdr, pComp->name());

    OMX_ERRORTYPE eError = pComp->fillThisBuffer(pBufferHdr);
    CHECK_OMX_ERROR(eError);
    DBGT_EPILOG();
    return eError;
}

//=============================================================================
// STOMX_UseEGLImage
//=============================================================================
OMX_ERRORTYPE STOMX_UseEGLImage(OMX_HANDLETYPE hComponent,
                                OMX_BUFFERHEADERTYPE** ppBufferHdr,
                                OMX_U32 nPortIndex,
                                OMX_PTR pAppPrivate,
                                void* eglImage)
{
    DBGT_PROLOG();
    CHECK_PARAM_NON_NULL(hComponent);
    CHECK_PARAM_NON_NULL(ppBufferHdr);
    CHECK_PARAM_NON_NULL(eglImage);

    stm::IOmxComponent* pComp = GET_COMP(hComponent);
    assert(pComp);

    OMX_ERRORTYPE eError =  pComp->useEGLImage(ppBufferHdr,
                                               nPortIndex,
                                               pAppPrivate,
                                               eglImage);
    CHECK_OMX_ERROR(eError);
    DBGT_EPILOG();
    return eError;
}

//=============================================================================
// STOMX_ComponentDeInit
//=============================================================================
OMX_ERRORTYPE STOMX_ComponentDeInit(OMX_HANDLETYPE hComponent)
{
    DBGT_PROLOG();
    CHECK_PARAM_NON_NULL(hComponent);

    stm::IOmxComponent* pComp = GET_COMP(hComponent);
    assert(pComp);

    DBGT_PINFO("%s Handle=%p (STOMXComponent::STOMX_ComponentDeInit)",
               pComp->name(), hComponent);

    // Call deinit method of component
    pComp->destroy();

    // Call component destructor
    delete pComp;

    OMXDebug_DeInit();

    DBGT_EPILOG();
    return OMX_ErrorNone;
}

//=============================================================================
// STOMX_ComponentRoleEnum
//=============================================================================
OMX_ERRORTYPE STOMX_ComponentRoleEnum(OMX_HANDLETYPE hComponent,
                                      OMX_U8* cRole,
                                      OMX_U32 nIndex)
{
    DBGT_PROLOG();
    CHECK_PARAM_NON_NULL(hComponent);
    CHECK_PARAM_NON_NULL(cRole);

    stm::IOmxComponent* pComp = GET_COMP(hComponent);
    assert(pComp);

    DBGT_PDEBUG("%s ComponentRoleEnum  nIndex=%d",
                pComp->name(), (int) nIndex);

    OMX_ERRORTYPE eError =  pComp->componentRoleEnum(cRole, nIndex);
    CHECK_OMX_ERROR(eError);
    DBGT_EPILOG();
    return eError;
}

//=============================================================================
// STOMX_SetComponentTypeFunctions
//=============================================================================
void STOMX_SetComponentTypeFunctions(OMX_COMPONENTTYPE* pComp)
{
    assert(pComp);

    pComp->GetComponentVersion    = STOMX_GetComponentVersion;
    pComp->SendCommand            = STOMX_SendCommand;
    pComp->GetParameter           = STOMX_GetParameter;
    pComp->SetParameter           = STOMX_SetParameter;
    pComp->SetCallbacks           = STOMX_SetCallbacks;
    pComp->GetConfig              = STOMX_GetConfig;
    pComp->SetConfig              = STOMX_SetConfig;
    pComp->GetExtensionIndex      = STOMX_GetExtensionIndex;
    pComp->GetState               = STOMX_GetState;
    pComp->ComponentTunnelRequest = STOMX_ComponentTunnelRequest;
    pComp->UseBuffer              = STOMX_UseBuffer;
    pComp->AllocateBuffer         = STOMX_AllocateBuffer;
    pComp->FreeBuffer             = STOMX_FreeBuffer;
    pComp->EmptyThisBuffer        = STOMX_EmptyThisBuffer;
    pComp->FillThisBuffer         = STOMX_FillThisBuffer;
    pComp->ComponentDeInit        = STOMX_ComponentDeInit;
    pComp->UseEGLImage            = STOMX_UseEGLImage;
    pComp->ComponentRoleEnum      = STOMX_ComponentRoleEnum;
}

#ifdef __cplusplus
} // eof extern C declaration
#endif
