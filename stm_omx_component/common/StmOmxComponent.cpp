/*
 * Copyright 2011 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *
 * File   ::    STOMXComponent.cpp
 * Author ::    Jean-Philippe FASSINO (jean-philippe.fassino@st.com)
 *
 *
 */

#include "StmOmxComponent.hpp"
#include "StmOmxVendorExt.hpp"
#include "StmOmxComponentWrapper.hpp"

#include "OMX_Index.h"
#include "OMX_IndexExt.h"
#include "OMX_CoreExt.h"
#include "OMX_Audio.h"
#include "OMX_AudioExt.h"
#include "OMX_VideoExt.h"

#include "OMX_debug.h"
#include <string.h>

#if HAVE_ASSERT_H
#include <assert.h>
#endif

#if HAVE_DBGTASSERT_H
// Re-direct assert on DBGT_ASSERT macro
#define assert(expr)  OMX_DBGT_ASSERT(expr)
#endif

#define DBGT_DECLARE_AUTOVAR
#define DBGT_PREFIX "OMXC"
#define DBGT_LAYER  0
#include <linux/dbgt.h>

#include <stdio.h> // For sprintf
#include <unistd.h> // for usleep

// TODO: prefer const variable
#define OMX_TIMEOUT 20000         // Timeout value in microsecond
#define OMX_MAX_TIMEOUTS 50       // Count of Maximum number of times the component can time out


// Check returned OMX error, and display message if different from ErrorNone
#define CHECK_OMX_ERROR(eError)                               \
    if (eError != OMX_ErrorNone) {                            \
        OMX_DBGT_ERROR("%s returned with error %s(0x%x)",         \
                   name(),                                    \
                   OMX_TYPE_TO_STR(OMX_ERRORTYPE, eError),    \
                   (unsigned int)eError);                     \
    }


namespace stm {

static int ComponentInstanceNumber = 0;

//=============================================================================
// OmxComponent::ComponentInit
// Static  component factory
//=============================================================================

OMX_ERRORTYPE OmxComponent::ComponentInit(OmxComponent* self)
{
    DBGT_PROLOG();

    DBGT_PINFO("%s (OmxComponent::ComponentInit)", self->name());

    DBGT_CHECK_RETURN(NULL != self, OMX_ErrorInsufficientResources);
    OMX_ERRORTYPE eError = self->create();
    if (eError != OMX_ErrorNone) {
       DBGT_ERROR("%s ComponentInit returned with error %s(0x%x)",
                   self->name(),
                   OMX_TYPE_TO_STR(OMX_ERRORTYPE, eError),
                   (unsigned int)eError);
        self->destroy();
        delete self;
    }
    DBGT_EPILOG();
    return eError;
}

//=============================================================================
// OmxComponent::OmxComponent
//  OMX component constructor
//=============================================================================
OmxComponent::OmxComponent(const char* _name, OMX_HANDLETYPE hComponent,
                           unsigned int _PortNumber)
    :CmdThread(0),
     mIsLoggingEnabled(false),
     m_nState(OMX_StateLoaded),
     pClientCallbacks(NULL),
     pAppData(NULL),
     hSelf(hComponent),
     m_nPortNumber(_PortNumber),
     m_nGroupPrio(0),
     m_nGroupID(0),
     IsIdleStateCompleting(false),
     m_mutexCmd(),
     m_condIdle()
{
    OMX_COMPONENTTYPE* pComp = (OMX_COMPONENTTYPE*)hComponent;
    DBGT_TRACE_INIT(omx);
    OMXDebug_Init((OMX_COMPONENTTYPE *)hComponent, (char *)_name);
    sprintf(m_strName, "[%s]:(%d)", _name, ComponentInstanceNumber++);
    OMXDEBUG_UPDATE_AV_TRACE_SETTINGS(m_strName);
    pComp->pComponentPrivate = (OMX_PTR)this;

    // Wrapper global method providing OMX static method pointers
    STOMX_SetComponentTypeFunctions(pComp);
    OMX_CONF_INIT_STRUCT_PTR(&sPortParam, OMX_PORT_PARAM_TYPE);
    sPortParam.nPorts = _PortNumber;
    sPortParam.nStartPortNumber = 0x0;
}

//=============================================================================
// OmxComponent::~OmxComponent
//  OMX component destructor
//=============================================================================

OmxComponent::~OmxComponent()
{
    ComponentInstanceNumber--;
}

//==========================================================================
// OmxComponent::create
// resource allocation of OMX component
//==========================================================================

OMX_ERRORTYPE OmxComponent::create()
{
    OMX_ERRORTYPE eError;

    OMX_DBGT_PROLOG();

    // Create internal thread
    OMX_DBGT_CHECK_RETURN(! pthread_create(&CmdThread, NULL, CmdThreadEntry, this),
                      OMX_ErrorInsufficientResources);

    OMX_DBGT_EPILOG();
    return OMX_ErrorNone;
}

//==========================================================================
// OmxComponent::destroy
// OMX component resource deallocation
//==========================================================================

void OmxComponent::destroy()
{
    OMX_ERRORTYPE eError;

    OMX_DBGT_PROLOG();

    // Put the command and data in the pipe
    Cmds.push(STOMXCommand());

    // Wait for thread to exit so we can get the status into "error"
    if(CmdThread != 0)
        pthread_join(CmdThread, (void**)&eError);

    OMX_DBGT_EPILOG();
}

OMX_ERRORTYPE OmxComponent::pause()
{
    // Default implementation simply forward request to all port
    OMX_ERRORTYPE eError = OMX_ErrorNone;
    for (unsigned int i = 0; i < this->portNb(); i++) {
        eError = this->getPort(i).pause();
        if (eError != OMX_ErrorNone) {
            OMX_DBGT_ERROR("Error when pausing port %u", i);
            return eError;
        }
    }

    return OMX_ErrorNone;
}

OMX_ERRORTYPE OmxComponent::resume()
{
    // Default implementation only forward request to all port
    OMX_ERRORTYPE eError = OMX_ErrorNone;
    for (unsigned int i = 0; i < this->portNb(); i++) {
        eError = this->getPort(i).resume();
        if (eError != OMX_ErrorNone) {
            OMX_DBGT_ERROR("Error when pausing port %u", i);
            return eError;
        }
    }

    return OMX_ErrorNone;
}

OMX_ERRORTYPE OmxComponent::flush(unsigned int nPortIdx)
{
    // Default implementation only forward request to right port
    assert(nPortIdx < this->portNb());
    return this->getPort(nPortIdx).flush();
}

//=============================================================================
// OmxComponent::GetComponentVersion
//=============================================================================
OMX_ERRORTYPE
OmxComponent::getComponentVersion(OMX_VERSIONTYPE* pComponentVersion,
                                    OMX_VERSIONTYPE* pSpecVersion,
                                    OMX_UUIDTYPE* pComponentUUID)
{
    OMX_DBGT_PROLOG();

    // Fill same as OpenMAX IL version today.
    pComponentVersion->s.nVersionMajor = stm::VERSION_MAJOR;
    pComponentVersion->s.nVersionMinor = stm::VERSION_MINOR;
    pComponentVersion->s.nRevision     = stm::VERSION_REVISION;
    pComponentVersion->s.nStep         = stm::VERSION_STEP;

    // this is the OpenMAX IL version. Has nothing to do with component
    // version.
    stm::omxIlSpecVersion(pSpecVersion);

    // TODO fill the pComponentUUID

    OMX_DBGT_EPILOG();
    return OMX_ErrorNone;
}

//=============================================================================
// OmxComponent::name
//=============================================================================

const char* OmxComponent::name() const
{ return m_strName; }

//=============================================================================
// OmxComponent::handle
//=============================================================================

OMX_HANDLETYPE OmxComponent::handle() const
{ return hSelf; }

//=============================================================================
// OmxComponent::portNb
//=============================================================================

unsigned int OmxComponent::portNb() const
{ return m_nPortNumber; }

//=============================================================================
// OmxComponent::omxState
//=============================================================================

OMX_STATETYPE OmxComponent::omxState() const
{ return m_nState; }

//=============================================================================
// OmxComponent::isPortsPopulated
//=============================================================================

OMX_BOOL OmxComponent::isPortsPopulated() const
{
    OMX_DBGT_PROLOG();

    for (unsigned int i = 0; i < this->portNb(); i++) {
        if (this->getPort(i).isTunneled()) {
            // No check if proprietary tunneled
            continue;
        }
        if (this->getPort(i).definition().bufferCountActual() == 0) {
            OMX_DBGT_EPILOG("%s: port %u uses no buffer", this->name(), i);
            continue;
        } else if (!this->getPort(i).isPopulated()) {
            OMX_DBGT_EPILOG("%s port %u is not populated", this->name(), i);
            return OMX_FALSE;
        }
    }
    OMX_DBGT_EPILOG("%s: all ports populated", this->name());
    return OMX_TRUE;
}

//=============================================================================
// OmxComponent::isPortsUnpopulated
//=============================================================================

OMX_BOOL OmxComponent::isPortsUnpopulated() const
{
    OMX_DBGT_PROLOG();

    for (unsigned int i = 0; i < this->portNb(); i++) {
        if (this->getPort(i).isTunneled()) {
            // No check if proprietary tunneled
            continue;
        }
        if (this->getPort(i).isPopulated()) {
            OMX_DBGT_EPILOG("%s port %u is populated", this->name(), i);
            return OMX_FALSE;
        }
    }
    OMX_DBGT_EPILOG("%s: all ports unpopulated", this->name());
    return OMX_TRUE;
}

//=============================================================================
// OmxComponent::notifyEventHandler
//=============================================================================

void OmxComponent::notifyEventHandler(OMX_U32 eEvent,
                                        OMX_U32 nData1,
                                        OMX_U32 nData2,
                                        OMX_PTR pEventData) const
{
    OMX_DBGT_PROLOG();
    if (!pClientCallbacks) {
        OMX_DBGT_ERROR("No client callback defined.");
        return;
    }
    pClientCallbacks->EventHandler(hSelf, pAppData,
                                   (OMX_EVENTTYPE)eEvent, nData1, nData2,
                                   pEventData);
    OMXDebug_EventHandler(hSelf, pAppData,
                          (OMX_EVENTTYPE)eEvent, nData1, nData2,
                          pEventData, name());
    OMX_DBGT_EPILOG();
}

void OmxComponent::notifyEmptyBufferDone(OMX_BUFFERHEADERTYPE* pBuffer) const
{
    OMX_DBGT_PROLOG();
    if (!pClientCallbacks) {
        OMX_DBGT_ERROR("No client callback defined.");
        return;
    }
    pClientCallbacks->EmptyBufferDone(hSelf, pAppData, pBuffer);
    OMXDebug_EmptyBufferDone(hSelf, pAppData, pBuffer, name());
    OMX_DBGT_EPILOG();
}

void OmxComponent::notifyFillBufferDone(OMX_BUFFERHEADERTYPE* pBuffer) const
{
    OMX_DBGT_PROLOG();
    if (!pClientCallbacks) {
        OMX_DBGT_ERROR("No client callback defined.");
        return;
    }
    pClientCallbacks->FillBufferDone(hSelf, pAppData, pBuffer);

    if(OMXdebug_flagAfterEos == 0) {
        if ((pBuffer != NULL) && (pBuffer->pBuffer != NULL))
            OMXDebug_FillBufferDone(hSelf, pAppData, pBuffer, name());
    }
    OMX_DBGT_EPILOG();
}

//=============================================================================
// OmxComponent::setCallBacks
//=============================================================================

OMX_ERRORTYPE OmxComponent::setCallbacks(OMX_CALLBACKTYPE* pCallbacks,
                                           OMX_PTR pAppData)
{
    OMX_DBGT_PROLOG();

    this->pClientCallbacks = pCallbacks;
    this->pAppData = pAppData;

    OMX_DBGT_EPILOG();
    return OMX_ErrorNone;
}

template <typename T>
OMX_ERRORTYPE OmxComponent::getPortParam(OMX_INDEXTYPE  nParamIndex,
                                         OMX_PTR        pParamStruct)
{
    ASSERT_STRUCT_TYPE(pParamStruct, T);
    T* param = (T*)pParamStruct;
    if (param->nPortIndex >= this->portNb()) {
        OMX_DBGT_ERROR("Invalid port index %u", (unsigned int)param->nPortIndex);
        return OMX_ErrorBadPortIndex;
    } else {
        return this->getPort(param->nPortIndex).getParameter(nParamIndex,
                                                             pParamStruct);
    }
}

template <typename T>
OMX_ERRORTYPE OmxComponent::setPortParam(OMX_INDEXTYPE  nParamIndex,
                                         OMX_PTR        pParamStruct)
{
    ASSERT_STRUCT_TYPE(pParamStruct, T);
    T* param = (T*)pParamStruct;
    if (param->nPortIndex >= this->portNb()) {
        OMX_DBGT_ERROR("Invalid port index %u", (unsigned int)param->nPortIndex);
        return OMX_ErrorBadPortIndex;
    } else {
        return this->getPort(param->nPortIndex).setParameter(nParamIndex,
                                                             pParamStruct);
    }
}

//=============================================================================
// OmxComponent::getParameter
// allows to get OMX component parameters
//=============================================================================
OMX_ERRORTYPE OmxComponent::getParameter(OMX_INDEXTYPE nParamIndex,
                                         OMX_PTR       pParamStruct)
{
    OMX_ERRORTYPE eError = OMX_ErrorNone;

    OMX_DBGT_PROLOG("OmxComponent::getParameter index=0x%x",
                (unsigned int)nParamIndex);
    switch ((int)nParamIndex)
    {
    case OMX_IndexParamAudioInit:
    case OMX_IndexParamImageInit:
    case OMX_IndexParamVideoInit:
    case OMX_IndexParamOtherInit: {
        ASSERT_STRUCT_TYPE(pParamStruct, OMX_PORT_PARAM_TYPE);
        // Default OMX_PORT_PARAM_TYPE structure return is
        // no port, null start number
        // inherit component shall overload for existing port
        OMX_PORT_PARAM_TYPE* param = (OMX_PORT_PARAM_TYPE*)pParamStruct;
        assert(param);
        param->nPorts = 0;
        param->nStartPortNumber = 0;
        eError = OMX_ErrorNone;
    }; break;

    case OMX_IndexParamVideoBitrate:{
        eError = getPortParam<OMX_VIDEO_PARAM_BITRATETYPE>(nParamIndex,
                                                            pParamStruct);
    }; break;

    case OMX_IndexParamPortDefinition: {
        eError = getPortParam<OMX_PARAM_PORTDEFINITIONTYPE>(nParamIndex,
                                                            pParamStruct);
    }; break;

    case OMX_IndexParamOtherPortFormat: {
        eError = getPortParam<OMX_OTHER_PARAM_PORTFORMATTYPE>(nParamIndex,
                                                              pParamStruct);
    }; break;

    case OMX_IndexParamAudioPortFormat: {
        eError = getPortParam<OMX_AUDIO_PARAM_PORTFORMATTYPE>(nParamIndex,
                                                              pParamStruct);
    }; break;

    case OMX_IndexParamImagePortFormat: {
        eError = getPortParam<OMX_IMAGE_PARAM_PORTFORMATTYPE>(nParamIndex,
                                                              pParamStruct);
    }; break;

    case OMX_IndexParamVideoPortFormat: {
        eError = getPortParam<OMX_VIDEO_PARAM_PORTFORMATTYPE>(nParamIndex,
                                                              pParamStruct);
    }; break;

    case OMX_getAndroidNativeBufferUsage: {
        eError = getPortParam<OMX_GetAndroidNativeBufferUsageParams>(nParamIndex,
                                                                     pParamStruct);
    }; break;

    case OMX_IndexParamAudioAac: {
        eError = getPortParam<OMX_AUDIO_PARAM_AACPROFILETYPE>(nParamIndex,
                                                              pParamStruct);
    }; break;

    case OMX_IndexParamAudioMp3: {
        eError = getPortParam<OMX_AUDIO_PARAM_MP3TYPE>(nParamIndex,
                                                       pParamStruct);
    }; break;

    case OMX_IndexParamAudioVorbis: {
        eError = getPortParam<OMX_AUDIO_PARAM_VORBISTYPE>(nParamIndex,
                                                          pParamStruct);
    }; break;

    case OMX_IndexParamAudioWma: {
        eError = getPortParam<OMX_AUDIO_PARAM_WMATYPE>(nParamIndex,
                                                       pParamStruct);
    }; break;

    case OMX_IndexParamAudioAc3: {
        eError = getPortParam<OMX_AUDIO_PARAM_AC3TYPE>(nParamIndex,
                                                       pParamStruct);
    }; break;

    case OMX_IndexParamAudioDts: {
        eError = getPortParam<OMX_AUDIO_PARAM_DTSTYPE>(nParamIndex,
                                                       pParamStruct);
    }; break;

    case OMX_IndexParamAudioAmr: {
        eError = getPortParam<OMX_AUDIO_PARAM_AMRTYPE>(nParamIndex,
                                                       pParamStruct);
    }; break;

    case OMX_IndexParamAudioAdpcm: {
        eError = getPortParam<OMX_AUDIO_PARAM_ADPCMTYPE>(nParamIndex,
                                                       pParamStruct);
    }; break;

    case OMX_IndexParamAudioPcm: {
        eError = getPortParam<OMX_AUDIO_PARAM_PCMMODETYPE>(nParamIndex,
                                                           pParamStruct);
    }; break;

    case OMX_IndexParamVideoAvc: {
        eError = getPortParam<OMX_VIDEO_PARAM_AVCTYPE>(nParamIndex,
                                                           pParamStruct);
    }; break;

    case OMX_IndexParamVideoProfileLevelQuerySupported: {
        eError = getPortParam<OMX_VIDEO_PARAM_PROFILELEVELTYPE>(nParamIndex,
                                                           pParamStruct);
    }; break;

    case OMX_IndexParamVideoIntraRefresh:{
        eError = getPortParam<OMX_VIDEO_PARAM_INTRAREFRESHTYPE>(nParamIndex,
                                                            pParamStruct);
    }; break;

    case OMX_IndexParam10BitProfile:{
        eError = getPortParam<OMX_VIDEO_PARAM_10BITPROFILETYPE>(nParamIndex,
                                                            pParamStruct);
    }; break;

    case OMX_IndexExtParamPrepareForAdaptivePlayback:{
        eError = getPortParam<OMX_VIDEO_PARAM_PREPARE_FOR_ADAPTIVE_PLAYBACK>(nParamIndex,
                                                            pParamStruct);
    }; break;

    default: {
        OMX_DBGT_ERROR("GetParameter(%x) => OMX_ErrorUnsupportedIndex", nParamIndex);
        eError = OMX_ErrorUnsupportedIndex;
    }; break;
    }

    CHECK_OMX_ERROR(eError);
    OMX_DBGT_EPILOG();
    return eError;
}


//=============================================================================
// OmxComponent::setParameter
// allows to set OMX component parameters
//=============================================================================

OMX_ERRORTYPE OmxComponent::setParameter(OMX_INDEXTYPE       nParamIndex,
                                         OMX_PTR             pParamStruct)
{
    OMX_ERRORTYPE eError = OMX_ErrorNone;

    OMX_DBGT_PROLOG();
    switch ((int)nParamIndex)
    {
    case OMX_IndexParamAudioInit:
    case OMX_IndexParamImageInit:
    case OMX_IndexParamVideoInit:
    case OMX_IndexParamOtherInit: {
        // Should not be able to set OMX_IndexParamXxxxInit.
        ASSERT_STRUCT_TYPE(pParamStruct, OMX_PORT_PARAM_TYPE);
        eError = OMX_ErrorUnsupportedSetting;
    }; break;

    case OMX_IndexParamPriorityMgmt: {
        ASSERT_STRUCT_TYPE(pParamStruct, OMX_PRIORITYMGMTTYPE);
        OMX_PRIORITYMGMTTYPE* param= (OMX_PRIORITYMGMTTYPE*)pParamStruct;
        this->setPriority(param->nGroupID, param->nGroupPriority);
        eError = OMX_ErrorNone;
    }; break;

    case OMX_IndexParamMediaContainer: {
        // TODO may be passed to SDK2 or used by decoder components
        DBGT_WARNING("OMX_IndexParamMediaContainer dummy implementation");
        eError = OMX_ErrorNone;
    }; break;

    case OMX_IndexParamResourceInfo: {
        ASSERT_STRUCT_TYPE(pParamStruct, OMX_RESOURCE_INFO);
        OMX_RESOURCE_INFO* param= (OMX_RESOURCE_INFO*)pParamStruct;
        // TODO use resource info returned by RM
        eError = OMX_ErrorNone;
    }; break;

    case OMX_IndexParamPortDefinition: {
        eError = setPortParam<OMX_PARAM_PORTDEFINITIONTYPE>(nParamIndex,
                                                            pParamStruct);
    }; break;

    case OMX_IndexParamOtherPortFormat: {
        eError = setPortParam<OMX_OTHER_PARAM_PORTFORMATTYPE>(nParamIndex,
                                                              pParamStruct);
    }; break;

    case OMX_IndexParamAudioPortFormat: {
        eError = setPortParam<OMX_AUDIO_PARAM_PORTFORMATTYPE>(nParamIndex,
                                                              pParamStruct);
    }; break;

    case OMX_IndexParamImagePortFormat: {
        eError = setPortParam<OMX_IMAGE_PARAM_PORTFORMATTYPE>(nParamIndex,
                                                              pParamStruct);
    }; break;

    case OMX_IndexParamVideoPortFormat: {
        eError = setPortParam<OMX_VIDEO_PARAM_PORTFORMATTYPE>(nParamIndex,
                                                              pParamStruct);
    }; break;

    case OMX_enableAndroidNativeBuffers: {
        eError = setPortParam<OMX_EnableAndroidNativeBuffersParams>(nParamIndex,
                                                                    pParamStruct);
    }; break;

    case OMX_IndexParamStandardComponentRole: {
        ASSERT_STRUCT_TYPE(pParamStruct, OMX_PARAM_COMPONENTROLETYPE);
        OMX_PARAM_COMPONENTROLETYPE* param = (OMX_PARAM_COMPONENTROLETYPE*)pParamStruct;
        eError = OMX_ErrorNotImplemented;
    }; break;

    case OMX_IndexParamAudioAac: {
        eError = setPortParam<OMX_AUDIO_PARAM_AACPROFILETYPE>(nParamIndex,
                                                              pParamStruct);
    }; break;

    case OMX_IndexParamAudioMp3: {
        eError = setPortParam<OMX_AUDIO_PARAM_MP3TYPE>(nParamIndex,
                                                       pParamStruct);
    }; break;

    case OMX_IndexParamAudioVorbis: {
        eError = setPortParam<OMX_AUDIO_PARAM_VORBISTYPE>(nParamIndex,
                                                          pParamStruct);
    }; break;

    case OMX_IndexParamAudioWma: {
        eError = setPortParam<OMX_AUDIO_PARAM_WMATYPE>(nParamIndex,
                                                       pParamStruct);
    }; break;

    case OMX_IndexParamAudioAc3: {
        eError = setPortParam<OMX_AUDIO_PARAM_AC3TYPE>(nParamIndex,
                                                       pParamStruct);
    }; break;

    case OMX_IndexParamAudioDts: {
        eError = setPortParam<OMX_AUDIO_PARAM_DTSTYPE>(nParamIndex,
                                                       pParamStruct);
    }; break;

    case OMX_IndexParamAudioAmr: {
        eError = setPortParam<OMX_AUDIO_PARAM_AMRTYPE>(nParamIndex,
                                                       pParamStruct);
    }; break;

    case OMX_IndexParamAudioAdpcm: {
        eError = setPortParam<OMX_AUDIO_PARAM_ADPCMTYPE>(nParamIndex,
                                                       pParamStruct);
    }; break;

    case OMX_IndexParamAudioPcm: {
        eError = setPortParam<OMX_AUDIO_PARAM_PCMMODETYPE>(nParamIndex,
                                                           pParamStruct);
    }; break;

    case OMX_IndexParamLowLatencyMode: {
        // TODO nothing implemented. No support in SDK2
        DBGT_WARNING("OMX_IndexParamLowLatencyMode dummy implementation");
        eError = OMX_ErrorNone;
    }; break;

    case OMX_IndexParamVideoAvc: {
        eError = setPortParam<OMX_VIDEO_PARAM_AVCTYPE>(nParamIndex,
                                                           pParamStruct);
    }; break;

    case OMX_IndexParamVideoProfileLevelQuerySupported: {
        eError = setPortParam<OMX_VIDEO_PARAM_PROFILELEVELTYPE>(nParamIndex,
                                                           pParamStruct);
    }; break;

    case OMX_IndexParamVideoBitrate:{
        eError = setPortParam<OMX_VIDEO_PARAM_BITRATETYPE>(nParamIndex,
                                                            pParamStruct);
    }; break;

    case OMX_IndexParamVideoIntraRefresh:{
        eError = setPortParam<OMX_VIDEO_PARAM_INTRAREFRESHTYPE>(nParamIndex,
                                                            pParamStruct);
    }; break;

    case OMX_IndexParam10BitProfile:{
        eError = setPortParam<OMX_VIDEO_PARAM_10BITPROFILETYPE>(nParamIndex,
                                                            pParamStruct);
    }; break;

    case OMX_IndexExtParamPrepareForAdaptivePlayback:{
        eError = setPortParam<OMX_VIDEO_PARAM_PREPARE_FOR_ADAPTIVE_PLAYBACK>(nParamIndex,
                                                            pParamStruct);
    }; break;

    default:
        eError = OMX_ErrorUnsupportedIndex;
        break;
    }

    CHECK_OMX_ERROR(eError);
    OMX_DBGT_EPILOG();
    return eError;
}


//=============================================================================
// OmxComponent::getConfig
// no configuration
//=============================================================================
OMX_ERRORTYPE OmxComponent::getConfig(OMX_INDEXTYPE nConfigIndex,
                                      OMX_PTR       pConfigStruct)
{
    OMX_ERRORTYPE eError = OMX_ErrorNone;

    OMX_DBGT_PROLOG();
    switch ((int)nConfigIndex)
    {
    case OMX_IndexConfigCommonOutputCrop: {
        eError = getPortParam<OMX_CONFIG_RECTTYPE>(nConfigIndex,
                                                   pConfigStruct);
    }; break;

    default:
        eError = OMX_ErrorUnsupportedIndex;
        break;
    }

    CHECK_OMX_ERROR(eError);
    OMX_DBGT_EPILOG();
    return eError;
}

//=============================================================================
// OmxComponent::setConfig
// no configuration
//=============================================================================
OMX_ERRORTYPE OmxComponent::setConfig(OMX_INDEXTYPE       nConfigIndex,
                                        OMX_PTR             pConfigStruct)
{
    OMX_DBGT_ERROR("SetConfig(%d) => OMX_ErrorUnsupportedIndex", nConfigIndex);
    return OMX_ErrorUnsupportedIndex;
}


//=============================================================================
// OmxComponent::getExtensionIndex
//=============================================================================
OMX_ERRORTYPE OmxComponent::getExtensionIndex(OMX_STRING      cParameterName,
                                              OMX_INDEXTYPE*  pIndexType)
{
    OMX_DBGT_PROLOG();
    if (strcmp(cParameterName,
               "OMX.google.android.index.enableAndroidNativeBuffers") == 0) {
        *pIndexType = (OMX_INDEXTYPE)OMX_enableAndroidNativeBuffers;
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }

    if (strcmp(cParameterName,
               "OMX.google.android.index.getAndroidNativeBufferUsage") == 0) {
        *pIndexType = (OMX_INDEXTYPE)OMX_getAndroidNativeBufferUsage;
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }

    if (strcmp(cParameterName,
               "OMX.google.android.index.useAndroidNativeBuffer2") == 0) {
        // This is call just a dummy for android to support
        // backward compatibility
        *pIndexType = (OMX_INDEXTYPE)OMX_useAndroidNativeBuffer2;
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }

    OMX_DBGT_EPILOG("Unsupported extension");
    return OMX_ErrorUnsupportedIndex;
}

//=============================================================================
// OmxComponent::useBuffer
//=============================================================================
OMX_ERRORTYPE OmxComponent::useBuffer(OMX_BUFFERHEADERTYPE**  ppBufferHdr,
                                        OMX_U32                 nPortIndex,
                                        OMX_PTR                 pAppPrivate,
                                        OMX_U32                 nSizeBytes,
                                        OMX_U8*                 pBuffer)
{
    OMX_DBGT_PROLOG();
    OMX_ERRORTYPE eError = getPort(nPortIndex).useBuffer(ppBufferHdr,
                                                          pAppPrivate,
                                                          nSizeBytes,
                                                          pBuffer);
    CHECK_OMX_ERROR(eError);
    OMX_DBGT_EPILOG();
    return eError;
}

//=============================================================================
// OmxComponent::allocateBuffer
//=============================================================================
OMX_ERRORTYPE OmxComponent::allocateBuffer(OMX_BUFFERHEADERTYPE** ppBufferHdr,
                                             OMX_U32                nPortIndex,
                                             OMX_PTR                pAppPrivate,
                                             OMX_U32                nSizeBytes)
{
    OMX_DBGT_PROLOG("Component allocate buffer on port %u with size %u"
                ,nPortIndex, nSizeBytes);

    OMX_ERRORTYPE eError = getPort(nPortIndex).allocateBuffer(ppBufferHdr,
                                                              pAppPrivate,
                                                              nSizeBytes);
    CHECK_OMX_ERROR(eError);
    OMX_DBGT_EPILOG();
    return eError;
}


//=============================================================================
// OmxComponent::freeBuffer
//=============================================================================
OMX_ERRORTYPE OmxComponent::freeBuffer(OMX_U32               nPortIndex,
                                         OMX_BUFFERHEADERTYPE* pBufferHdr)
{
    OMX_DBGT_PROLOG();
    OMX_STATETYPE currentState = this->omxState();

    if ((currentState == OMX_StateExecuting) ||
        (currentState == OMX_StatePause)) {
        if (IsIdleStateCompleting)
        {
            OMX_DBGT_PINFO("Blocking freeBuffer before Idle state is set");
            m_condIdle.wait(m_mutexCmd);
            OMX_DBGT_PINFO("Unblocking freeBuffer");
        }
        else {
            // No check here as we can come here in two cases
            // 1. We have already moved to idle state
            // 2. Port already issued OMX_CommandPortDisable
            OMX_DBGT_PINFO("Freeing buffer");
        }
    }

    OMX_ERRORTYPE eError = getPort(nPortIndex).freeBuffer(pBufferHdr);
    CHECK_OMX_ERROR(eError);
    OMX_DBGT_EPILOG();
    return eError;
}

//=============================================================================
// OmxComponent::useEGLImage
//=============================================================================
OMX_ERRORTYPE OmxComponent::useEGLImage(OMX_BUFFERHEADERTYPE** ppBufferHdr,
                                          OMX_U32 nPortIndex,
                                          OMX_PTR pAppPrivate,
                                          void* eglImage)
{
    OMX_DBGT_ERROR("useEGLImage => OMX_ErrorNotImplemented");
    return OMX_ErrorNotImplemented;
}

//=============================================================================
// OmxComponent::componentRoleEnum
//=============================================================================
OMX_ERRORTYPE OmxComponent::componentRoleEnum(OMX_U8* cRole,
                                                OMX_U32 nIndex)
{
    OMX_DBGT_ERROR("componentRoleEnum => OMX_ErrorNotImplemented");
    return OMX_ErrorNotImplemented;
}

//=============================================================================
// OmxComponent::componentTunnelRequest
//=============================================================================
OMX_ERRORTYPE
OmxComponent::componentTunnelRequest(OMX_U32 nPortIndex,
                                     OMX_HANDLETYPE hTunneledComp,
                                     OMX_U32 nTunneledPort,
                                     OMX_TUNNELSETUPTYPE* pSetup)
{
    OMX_DBGT_PROLOG("%s componentTunnelRequest on port %u",
                name(), (unsigned int)nPortIndex);
    OMX_ERRORTYPE eError = getPort(nPortIndex).tunnelRequest(hTunneledComp,
                                                             nTunneledPort,
                                                             pSetup);
    CHECK_OMX_ERROR(eError);
    OMX_DBGT_EPILOG();
    return eError;
}



//==========================================================================
// OmxComponent::sendCommand
// management of OMX command sent by player
//==========================================================================

OMX_ERRORTYPE OmxComponent::sendCommand(OMX_COMMANDTYPE     Cmd,
                                               OMX_U32             nParam1,
                                               OMX_PTR             pCmdData)
{
    STOMX_AsyncCommand pusherCmd = STOMX_PushCmdNewCommand;

    OMX_DBGT_PROLOG();
    switch (Cmd){
    case OMX_CommandStateSet:
        OMX_DBGT_PDEBUG("%s::sendCommand(OMX_CommandStateSet, %s)", this->name(),
                    OMX_TYPE_TO_STR(OMX_STATETYPE, (OMX_STATETYPE)nParam1));
        Cmds.push(STOMXCommand(Cmd, nParam1));
        if ((OMX_STATETYPE)nParam1 == OMX_StateIdle) IsIdleStateCompleting = true;
        break;
    case OMX_CommandFlush:
        OMX_DBGT_PDEBUG("%s::sendCommand(OMX_CommandFlush, %d)", this->name(), (int)nParam1);
        goto OMX_PARAM_AS_PORT;
    case OMX_CommandPortDisable:
        OMX_DBGT_PDEBUG("%s::sendCommand(OMX_CommandPortDisable, %d)", this->name(), (int)nParam1);
        goto OMX_PARAM_AS_PORT;
    case OMX_CommandPortEnable:
        OMX_DBGT_PDEBUG("%s::sendCommand(OMX_CommandPortEnable, %d)", this->name(), (int)nParam1);
        goto OMX_PARAM_AS_PORT;
    case OMX_CommandMarkBuffer:
        OMX_DBGT_WARNING("%s::sendCommand(OMX_CommandMarkBuffer)", this->name());
        OMX_DBGT_CHECK_RETURN(NULL != pCmdData, OMX_ErrorBadParameter);
        // In case of MarkBuf, the pCmdData parameter is used to carry the data.
        // In other cases, the nParam1 parameter carries the data.
#if 0
        write(CmdPipe[1], &pusherCmd, sizeof(pusherCmd));
        write(CmdPipe[1], &Cmd, sizeof(Cmd));
        write(CmdPipe[1], &pCmdData, sizeof(OMX_PTR));
#endif
        break;
    default:
        OMX_DBGT_CHECK_RETURN(0, OMX_ErrorBadParameter);
    }
    OMX_DBGT_EPILOG();
    return OMX_ErrorNone;
OMX_PARAM_AS_PORT:
    OMX_DBGT_CHECK_RETURN(nParam1 < this->portNb() || nParam1 == OMX_ALL, OMX_ErrorBadPortIndex);

    /*
     * In order to simply thread code, we generate command for each port instead of OMX_ALL
     */
    if(nParam1 == OMX_ALL) {
        for(nParam1 = 0; nParam1 < this->portNb(); nParam1++)
            Cmds.push(STOMXCommand(Cmd, nParam1));
    } else
        Cmds.push(STOMXCommand(Cmd, nParam1));

    OMX_DBGT_EPILOG();
    return OMX_ErrorNone;
}


//=================================================================================
// OmxComponent::emptyThisBuffer
// Called by player to send a encoded frame buffer
// This buffer is pushed via a queue to the PusherThread in order to be sent to SE
//=================================================================================

OMX_ERRORTYPE OmxComponent::emptyThisBuffer(OMX_BUFFERHEADERTYPE* pBufferHdr)
{
    OMX_DBGT_PROLOG();

    // Delegate job to the right port
    OmxPort& cPort = this->getPort(pBufferHdr->nInputPortIndex);
    assert(!cPort.isTunneled());

    OMX_ERRORTYPE eError = cPort.emptyThisBuffer(pBufferHdr);
    OMX_DBGT_EPILOG();
    return eError;
}

//=================================================================================
// OmxComponent::fillThisBuffer
// Called by player to send an empty buffer to be filled with a decoded frame
// This buffer is send to omxse
//=================================================================================

OMX_ERRORTYPE OmxComponent::fillThisBuffer(OMX_BUFFERHEADERTYPE* pBufferHdr)
{
    // Delegate job to the right port
    OmxPort& cPort = this->getPort(pBufferHdr->nOutputPortIndex);
    assert(!cPort.isTunneled());

    OMX_ERRORTYPE eError = cPort.fillThisBuffer(pBufferHdr);
    OMX_DBGT_EPILOG();
    return eError;
}

//=================================================================================
// OmxComponent::CmdThreadEntry
// Entry function of the OMX Cmd management thread
//=================================================================================

void* OmxComponent::CmdThreadEntry(void* pThreadData)
{
    DBGT_PROLOG();

    OMX_ERRORTYPE  eError;

    OmxComponent* self = (OmxComponent*)pThreadData;

    eError = self->CmdThreadRunner();

    DBGT_EPILOG();
    return (void *)eError;
}

//=================================================================================
// Error management functions of OMX command handle
// LowLevelError  - IncorrectTransition - UnsupportedSetting 
// LostResources - InsufficientResources
//=================================================================================

OMX_ERRORTYPE OmxComponent::LowLevelError(OMX_ERRORTYPE eError)
{
    OMX_DBGT_ERROR("  ... OMX_ErrorIncorrectStateTransition");
    this->notifyEventHandler(OMX_EventError, eError, 0 , NULL);
    return eError;
}

OMX_ERRORTYPE OmxComponent::IncorrectTransition()
{
    OMX_DBGT_ERROR("  ... OMX_ErrorIncorrectStateTransition");
    this->notifyEventHandler(OMX_EventError, OMX_ErrorIncorrectStateTransition, 0 , NULL);
    return OMX_ErrorIncorrectStateTransition;
}


OMX_ERRORTYPE OmxComponent::UnsupportedSetting()
{
    OMX_DBGT_ERROR("  ... OMX_ErrorUnsupportedSetting");
    this->notifyEventHandler(OMX_EventError, OMX_ErrorUnsupportedSetting, 0 , NULL);
    return OMX_ErrorUnsupportedSetting;
}

OMX_ERRORTYPE OmxComponent::InsufficientResources()
{
    OMX_DBGT_ERROR("  ... OMX_ErrorInsufficientResources");
    this->notifyEventHandler(OMX_EventError, OMX_ErrorInsufficientResources, 0 , NULL);
    return OMX_ErrorInsufficientResources;
}


OMX_ERRORTYPE OmxComponent::LostResources()
{
    OMX_DBGT_ERROR("  ... OMX_ErrorResourcesLost");
    this->notifyEventHandler(OMX_EventError, OMX_ErrorResourcesLost, 0 , NULL);
    return OMX_ErrorResourcesLost;
}


//=================================================================================
// OmxComponent::ValidStateTransition
//  Validation of a new OMX state setting
//=================================================================================

OMX_ERRORTYPE OmxComponent::ValidStateTransition(OMX_STATETYPE newState)
{
    OMX_DBGT_PROLOG();
    this->omxState(newState);

    if (this->omxState() == OMX_StateIdle)
    {
        IsIdleStateCompleting = false;
        m_condIdle.signal();
    }

    this->notifyEventHandler(OMX_EventCmdComplete, OMX_CommandStateSet,
                             this->omxState(), NULL);
    OMX_DBGT_EPILOG();
    return OMX_ErrorNone;
}


//=================================================================================
// OmxComponent::WaitingForPortPopulated
//  Wait for the achievelent of the populate OMX port transition
//=================================================================================

OMX_ERRORTYPE OmxComponent::WaitingForPortPopulated(OMX_STATETYPE newState)
{
    OMX_DBGT_PROLOG("Waiting for ports populated");
    for(OMX_U32 nTimeout = 0x0; nTimeout <= OMX_MAX_TIMEOUTS; nTimeout++)
    {
       // Ports have to be populated before transition completes
       if (isPortsPopulated()) {
           OMX_DBGT_EPILOG("Ports populated");
           return ValidStateTransition(newState);
       }
       usleep(OMX_TIMEOUT);
    }

    OMX_DBGT_ERROR("Timeout waiting on port population");
    OMX_DBGT_EPILOG();
    return InsufficientResources();
}


//=================================================================================
// OmxComponent::WaitingForPortUnpopulated
//  Wait for the achievelent of the unpopoulate OMX port transition
//=================================================================================

OMX_ERRORTYPE OmxComponent::WaitingForPortUnpopulated(OMX_STATETYPE newState)
{
    for(OMX_U32 nTimeout = 0x0; nTimeout <= OMX_MAX_TIMEOUTS; nTimeout++)
    {
       // Transition happens only when the ports are unpopulated
        if (isPortsUnpopulated()) {
           return ValidStateTransition(newState);
       }
       usleep(OMX_TIMEOUT);
    }

    return LostResources();
}

//=================================================================================
// OmxComponent::SetStateInvalid
//  Invalid set OMX state management
//=================================================================================

OMX_ERRORTYPE OmxComponent::SetStateInvalid()
{
    OMXDebug_onStateChange(OMX_StateInvalid, this->name());
    this->notifyEventHandler(OMX_EventError, OMX_ErrorInvalidState, 0 , NULL);
    return IncorrectTransition();
}


//=================================================================================
// OmxComponent::SetStateLoaded
//  Loaded set OMX state management
//=================================================================================

OMX_ERRORTYPE OmxComponent::SetStateLoaded()
{
    OMX_ERRORTYPE eError = OMX_ErrorNone;
    OMX_STATETYPE currentState = this->omxState();
    OMX_DBGT_PROLOG();

    OMXDebug_onStateChange(OMX_StateLoaded, this->name());
    if (currentState == OMX_StateIdle ||
        currentState == OMX_StateWaitForResources)
    {
        eError = deConfigure();
        if(eError != OMX_ErrorNone)
        {
            eError = LowLevelError(eError);
        }
        else
        {
            eError = WaitingForPortUnpopulated(OMX_StateLoaded);
        }
    }
    else
    {
            eError = IncorrectTransition();
    }

    OMX_DBGT_EPILOG();
    return eError;
}

//=================================================================================
// OmxComponent::SetStateIdle
//  Idle set OMX state management
//=================================================================================

OMX_ERRORTYPE OmxComponent::SetStateIdle()
{
    OMX_ERRORTYPE eError = OMX_ErrorNone;

    OMX_DBGT_PROLOG();

    OMXDebug_onStateChange(OMX_StateIdle, this->name());
    switch(this->omxState())
    {
        case  OMX_StateLoaded:
        case  OMX_StateWaitForResources:
             eError = configure();
             if(eError != OMX_ErrorNone)
             {
                 eError = UnsupportedSetting();
             }
             break;

        case OMX_StateExecuting:
        case OMX_StatePause:
            stop();
            // Flush all ports
            for (unsigned int i = 0; i < this->portNb(); i++) {
                this->flush(i);
            }
            break;

        default:
            eError = IncorrectTransition();
    }

    if (eError == OMX_ErrorNone)
    {
       eError = WaitingForPortPopulated(OMX_StateIdle);
    }

    OMX_DBGT_EPILOG();
    return eError;
}

//=================================================================================
// OmxComponent::SetStateExecuting
//  Executing set OMX state management
//=================================================================================

OMX_ERRORTYPE OmxComponent::SetStateExecuting()
{
    OMX_ERRORTYPE eError = OMX_ErrorNone;

    OMXDebug_onStateChange(OMX_StateExecuting, this->name());
    // Transition can only happen from pause or idle state
    switch(this->omxState())
    {
    case  OMX_StateIdle: {
        eError = start();
        if(eError != OMX_ErrorNone)
        {
            eError = LowLevelError(eError);
        }
        else
        {
            eError = ValidStateTransition(OMX_StateExecuting);
        }
    }; break;

    case  OMX_StatePause: {
        eError = resume();
        if(eError != OMX_ErrorNone)
        {
            eError = LowLevelError(eError);
        }
        else
        {
            eError = ValidStateTransition(OMX_StateExecuting);
        }
    }; break;

    default: {
        eError = IncorrectTransition();
    }; break;
    }
    return eError;
}

//=================================================================================
// OmxComponent::SetStatePause
//  Pause set OMX state management
//=================================================================================
OMX_ERRORTYPE OmxComponent::SetStatePause()
{
    OMX_ERRORTYPE eError = OMX_ErrorNone;
    OMX_STATETYPE currentState = this->omxState();
    OMXDebug_onStateChange(OMX_StatePause, this->name());

    // Transition can only happen from idle or executing state
    if (currentState == OMX_StateIdle ||
        currentState == OMX_StateExecuting)
    {
        eError = pause();
        if(eError != OMX_ErrorNone)
        {
            eError = LowLevelError(eError);
        }
        else
        {
            eError = ValidStateTransition(OMX_StatePause);
        }
    }
    else
    {
        eError = IncorrectTransition();
    }

    return eError;
}

//=================================================================================
// OmxComponent::SetStateWaitForResources
//  WaitForResources set OMX state management
//=================================================================================
OMX_ERRORTYPE OmxComponent::SetStateWaitForResources()
{
    OMX_ERRORTYPE eError = OMX_ErrorNone;

    OMXDebug_onStateChange(OMX_StateWaitForResources, this->name());
    if (this->omxState() == OMX_StateLoaded)
    {
        eError = ValidStateTransition(OMX_StateWaitForResources);
    }
    else
    {
        eError = IncorrectTransition();
    }

    return eError;
}


//=================================================================================
// OmxComponent::CommandStateSet
//  Management of 'set state' OMX command
//=================================================================================

OMX_ERRORTYPE OmxComponent::CommandStateSet(OMX_STATETYPE newState)
{
    OMX_ERRORTYPE eError = OMX_ErrorNone;

    OMX_DBGT_PROLOG();

    // If the parameter states a transition to the same state
    //   raise a same state transition error.
    if (this->omxState() == newState)
    {
#ifdef ANDROID
        this->notifyEventHandler(OMX_EventError, OMX_ErrorSameState, 0 , NULL);
#else
        this->notifyEventHandler(OMX_EventCmdComplete, OMX_CommandStateSet, this->omxState() , NULL);
#endif
    }
    else
    {
        // transitions/callbacks made based on state transition table
        // cmddata contains the target state
        switch (newState) {
            case OMX_StateInvalid:
                eError = SetStateInvalid();
                break;

            case OMX_StateLoaded:
                eError = SetStateLoaded();
                break;

            case OMX_StateIdle:
                eError = SetStateIdle();
                break;

            case OMX_StateExecuting:
                eError = SetStateExecuting();
                break;

            case OMX_StatePause:
                eError = SetStatePause();
                break;

            case OMX_StateWaitForResources:
                eError = SetStateWaitForResources();
                break;

            default:
                OMXDebug_onStateChange(newState, this->name());
                break;
        }
    }

    OMX_DBGT_EPILOG();
    return OMX_ErrorNone;
}

//=============================================================================
// OmxComponent::CommandPortDisable
//  Management of 'port disable'  OMX command
//=============================================================================

OMX_ERRORTYPE OmxComponent::CommandPortDisable(STOMXCommand cmd)
{
    OMX_DBGT_PROLOG("%s Disable port %u", this->name(),
                (unsigned int)cmd.u.omxcmd.data);

    OmxPort& cPort = this->getPort(cmd.u.omxcmd.data);
    OMX_ERRORTYPE eError = cPort.disable();

    if (eError == OMX_ErrorNone) {
        // Return cmdcomplete event if port disabled
        this->notifyEventHandler(OMX_EventCmdComplete, OMX_CommandPortDisable,
                                 cmd.u.omxcmd.data, NULL);
    } else {
        OMX_DBGT_ERROR("Failed to disable port %u", (unsigned int)cmd.u.omxcmd.data);
        this->notifyEventHandler(OMX_EventError,
                                 eError, 0 , NULL);
    }
    OMX_DBGT_EPILOG();
    return eError;
}


//=============================================================================
// OmxComponent::CommandPortEnable
//  Management of 'port enable'  OMX command
//=============================================================================

OMX_ERRORTYPE OmxComponent::CommandPortEnable(STOMXCommand cmd)
{
    // Enable port(s): cmddata contains the port index
    OMX_DBGT_PROLOG("%s Enable port %u", this->name(),
                (unsigned int)cmd.u.omxcmd.data);

    OmxPort& cPort = this->getPort(cmd.u.omxcmd.data);
    OMX_ERRORTYPE eError = cPort.enable();

    if (eError == OMX_ErrorNone) {
        // Return cmdcomplete event, if port enabled
        this->notifyEventHandler(OMX_EventCmdComplete,
                                 OMX_CommandPortEnable, cmd.u.omxcmd.data, NULL);
    } else {
        OMX_DBGT_ERROR("Failed to enable port %u", (unsigned int)cmd.u.omxcmd.data);
        this->notifyEventHandler(OMX_EventError,
                                 eError, 0 , NULL);
    }
    OMX_DBGT_EPILOG();
    return eError;
}


//=============================================================================
// OmxComponent::CommandFlush
//  Management of 'flush'  OMX command
//=============================================================================

OMX_ERRORTYPE OmxComponent::CommandFlush(STOMXCommand cmd)
{
    // Flush port(s): cmddata contains the port index
    OMX_DBGT_PROLOG("%s Flush port %u",
                 this->name(), (unsigned int)cmd.u.omxcmd.data);

    OMX_ERRORTYPE eError = this->flush(cmd.u.omxcmd.data);
    if (eError == OMX_ErrorNone) {
        this->notifyEventHandler(OMX_EventCmdComplete, OMX_CommandFlush,
                                 cmd.u.omxcmd.data, NULL);
    } else {
        OMX_DBGT_ERROR("Failed to flush port %u", (unsigned int)cmd.u.omxcmd.data);
        this->notifyEventHandler(OMX_EventError,
                                 eError, 0 , NULL);
    }
    OMX_DBGT_EPILOG();
    return eError;
}

//=============================================================================
// OmxComponent::CmdThreadRunner
//  core function of OMX command thread management
//=============================================================================

OMX_ERRORTYPE OmxComponent::CmdThreadRunner()
{
    OMX_ERRORTYPE eError = OMX_ErrorNone;

    OMX_DBGT_PROLOG();
    OMX_DBGT_PINFO("%s Command thread running (OmxComponent::CmdThreadRunner)", this->name());

    // infinite loop of thread until OMX component destroy
    while(OMX_TRUE)
    {
        // wait for OMX command sent by player
        STOMXCommand cmd;
        Cmds.waitAndPop(cmd);

        switch(cmd.command)
        {
            case STOMX_PushCmdNewCommand:
            {
                // OMX command reveived to manage
                switch(cmd.u.omxcmd.cmd)
                {

                    case OMX_CommandStateSet:
                    {
                        eError = CommandStateSet((OMX_STATETYPE)(cmd.u.omxcmd.data));
                        break;
                    }

                    case OMX_CommandPortDisable:
                    {
                        // Disable port(s): cmddata contains the port index
                        eError = CommandPortDisable(cmd);
                        break;
                    }

                    case OMX_CommandPortEnable:
                    {
                        // Enable port(s): cmddata contains the port index
                        eError = CommandPortEnable(cmd);
                        break;
                    }

                    case OMX_CommandFlush:
                    {
                        // Flush port(s): cmddata contains the port index
                        eError = CommandFlush(cmd);
                        break;
                    }

                    default:
                    {
                        OMX_DBGT_ERROR("Command unknown (%d)...", (unsigned int)cmd.u.omxcmd.cmd);
                        break;
                    }
                }
            };
            break;

            case STOMX_PushCmdBuffer:
                // Do nothing, it will be handle according state, this command is just here to wake up thread
                break;

            case STOMX_PushCmdExit:
                goto EXIT;

            default:
                OMX_DBGT_ERROR("Bad incomming command %d", cmd.command);
                goto EXIT;
        }

        if(this->omxState() == OMX_StateExecuting)
            execute();
    }


// end of OMX component session: thread is destroyed
EXIT:
    OMX_DBGT_PINFO("%s Command thread exit (OmxComponent::CmdThreadRunner)", this->name());
    OMX_DBGT_EPILOG();
    return OMX_ErrorNone;
}





} // eof namespace stm
