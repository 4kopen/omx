/*
 * Copyright 2013 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file   StmIOmxPort.hpp
 * @author STMicroelectronics
 */

#ifndef _STM_OMXFIFO_HPP_
#define _STM_OMXFIFO_HPP_

#include <pthread.h>
#include <time.h>
#include <errno.h>

namespace stm {

template <class T, int N>
class OmxFifo
{
public:
    OmxFifo();
    virtual ~OmxFifo();

    unsigned int size() const;
    int numberOfElement() { return elements; }
    bool empty() const;

    void push(const T& t);
    bool tryPop(T& t);
    bool waitAndPop(T& t);
    T popLocked();

    // TODO: move to protected. Shall not be used externally
    void lock() const { pthread_mutex_lock( &mutex ); }
    void unlock() const { pthread_mutex_unlock( &mutex ); }

private:
    T fifos[N];
    int readPos, writePos;
    int elements;
    mutable pthread_mutex_t mutex;
    pthread_cond_t cond;
};

template <class T, int N>
OmxFifo<T, N>::OmxFifo()
{
    elements = 0;
    readPos = writePos = 0;
    pthread_mutex_init(&mutex, 0);
    pthread_cond_init(&cond, 0);
}

template <class T, int N>
OmxFifo<T, N>::~OmxFifo()
{
    pthread_mutex_destroy(&mutex);
    pthread_cond_destroy(&cond);
}

template <class T, int N>
unsigned int OmxFifo<T, N>::size() const
{
    this->lock();
    unsigned int nElem = elements;
    this->unlock();
    return nElem;
}

template <class T, int N>
bool OmxFifo<T, N>::empty() const
{
    this->lock();
    bool ret = (elements == 0);
    this->unlock();
    return ret;
}

template <class T, int N>
void OmxFifo<T, N>::push(const T& t)
{
    this->lock();
    const bool bWasEmpty = (elements == 0);

    while (elements >= N) {
        // Fifo is full;
        pthread_cond_wait(&cond, &mutex);
    }
    fifos[writePos] = t;
    if(++writePos >= N)
        writePos = 0;
    elements++;
    this->unlock();

    if (bWasEmpty) {
        pthread_cond_broadcast(&cond);
    }
}

template <class T, int N>
bool OmxFifo<T, N>::tryPop(T& t)
{
    if(elements > 0)
    {
        t = fifos[readPos];
        if (++readPos >= N)
            readPos = 0;
        if(elements-- == N) // Signal fifo no more full
            pthread_cond_signal( &cond );
        return true;
    }

    return false;
}

template <class T, int N>
T OmxFifo<T, N>::popLocked()
{
    while(elements == 0)
        pthread_cond_wait( &cond, &mutex );

    T t=0;
    bool err = tryPop(t);

    return t;
}

template <class T, int N>
bool OmxFifo<T, N>::waitAndPop(T& poppedValue)
{
    // TODO add timeout to enable error recovery
    this->lock();
    while (elements == 0) {
        pthread_cond_wait(&cond, &mutex);
    }

    const bool bWasFull = (elements == N);
    poppedValue = fifos[readPos];
    if (++readPos >= N)
        readPos = 0;
    elements--;
    this->unlock();

    if (bWasFull) {
        pthread_cond_broadcast(&cond);
    }
    return true;
}

} // eof namespace stm

#endif // _STM_OMXFIFO_HPP_
