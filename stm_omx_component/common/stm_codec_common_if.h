/*
 * Copyright 2011 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *
 * File   ::    stm_codec_common_if.h
 * Author ::    Pradeep Kumar SAH (pradeep.sah@st.com)
 *
 *
 */

#ifndef	_stm_codec_common_if_h_
#define _stm_codec_common_if_h_

/* codec return level of return value */
typedef enum {
    RETURN_STATUS_OK,
    RETURN_STATUS_WARNING,
    RETURN_STATUS_ERROR,
    RETURN_STATUS_FATAL_ERROR
} RETURN_STATUS_LEVEL_T;

/* values returned by initialization functions */
typedef enum
{
    INIT_OK,
    INIT_WARNING_NO_ESRAM,
    INIT_FAILURE_MALLOC,
    INIT_FAILURE_CONFIGURE,
    INIT_BAD_MEMORY_PRESET,
    INIT_INVALID_VALUE,
    INIT_ERROR = INIT_FAILURE_MALLOC 
} CODEC_INIT_T;

/* List of supported audio modules */
typedef enum {
    UNKNOWN,
    MP3,         /* ISO compliant */
    MP3_25,      /* ISO + fhg extension */
    AAC_ADTS,
    AAC_ADIF,   
    AAC_RAW,     /* No header or syncword */
    AAC_ENC,     /* MPEG2 */
    ADPCM,       /* speech codecs */
    RIFF,        /* WAVE files */
    BYPASS,
    WMA,
    PAC,
    AC3,
    MPEG2,
    MPEG2_MC,
    MPEG2_DAB,
    DTS,
    PCM,
    SBC,         /* BlueTooth audio codec */
    AMR,
    AMRWB,
    GSMFR,
    MIDIST,
    G723_1,
    REALAUDIO8,
    G711,
    G729,
    QCELP,
    G722,
    EVRC,
    AWP,
    G726,
    DEC_AAC,
    EAAC,
    ENC_G723_1,
    DEC_G723_1,
    ILBC,
    ENC_MP3,
    STMPEG,
    DEC_WMA,
    OGGVORBIS,
    GSMHR,
    DDPLUS,
    FLAC,
    MODE_LAST_IN_LIST
} AUDIO_MODE_T;

/* Sample structure */
/* audio module has to fill all fields */
typedef struct {
    int sample_freq;                    // sample frequency (one defined into audiotables.h)
    int sample_size;                    // sample size (16 or 24) 
    int headroom;			            // number of guard bits
    int chans_nb;                       // number of output channels
    int block_len;                      // number of sample in the current frame  
    int *buf_add;	 
    int samples_interleaved;            // decoder status//remove for clean purpose
    int out_pid;

}SAMPLE_STRUCT_T;

/****************************************************************/
/* Typedef used to locate the position of a bit in RAM,			*/
/* - buf_add  : address of the begining of the buffer           */
/* - buf_end  : address of the end of the buffer                */
/* - pos_ptr  : current address (to read or write)              */
/* - buf_size : size of the buffer in WORD                      */
/* - nbit_available : number of bits available in the current   */
/*   word pointed by pos_ptr									*/
/* - word_size : (16/24) DSP running mode 						*/
/****************************************************************/
typedef struct BS_STRUCT_T
{
	unsigned int *buf_add;
	unsigned int *buf_end;
	unsigned int *pos_ptr;
	int buf_size;
	int nbit_available;
	int word_size;
}BS_STRUCT_T;


/* Bit stream structure */
/* audio module has to fill real_size_frame_in_bit (encoder mode) */
typedef struct {
	int real_size_frame_in_bit;		// number of significant bits (in case of varying frame size)
	BS_STRUCT_T bits_struct;		// set of pointers to read/write bit stream from/to FE fifos
									// (defined into bitstream_lib_proto.h)
}STREAM_STRUCT_T;

/* Audio Module (AM) state structure */
typedef struct {
	/* status flags */
	int first_time;			// set to 1 by UFE, reset by AM in case of specific first task 
	int output_enable;		// set to 1 by AM to indicate when output enable (decoder only to allow copy into fifo)
	int remaining_blocks;	// set to 0 by AM to indicate when all input buffer has been processed (decoder only)
	int status;				// set to error status level
	AUDIO_MODE_T mode; 		// store value found by codec_sync() since last codec_init. Written by UFE, read by AM 
    int bfi;                // if non zero, it means that the current frame is not correct (only updated with SHM)
	int eof;				// Set to TRUE by Codec when reaches end of process
	int sync_lost;			// set to TRUE by FE when synchronization is lost, reset to 0 by codec when decoding next frame
	int in_pid;             // WMA Input Packet Id
	int emit_buffers;       // Set to False if AM does not require the processed buffers to be emitted out at the o/p port.
	
}STATE_STRUCT_T;


/* FE entry point structure */
typedef struct {

	SAMPLE_STRUCT_T	sample_struct; 	// struct used as input for the encoders 
									// and as output for the decoders 
	STREAM_STRUCT_T stream_struct;	// struct used as output for the encoders 
									// and as input for the decoders 
	STATE_STRUCT_T codec_state; 	// audio module state structure

	unsigned int last_input_data;	// Flag to inform codec that no more new data are available
	
	void * codec_local_struct;			// audio module static memory 
	void * codec_config_local_struct;	// Optionnal field: codec may want to duplicate service_zone for its own processing.
	void * codec_info_local_struct;		// codec extra info for the api (bit-rate, modes...)

}CODEC_INTERFACE_T;

#endif /*_stm_codec_common_if_h_*/
