/*
 * Copyright 2013 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file   StmOmxPort.hpp
 * @author STMicroelectronics
 */

#ifndef _STM_OMXPORT_HPP_
#define _STM_OMXPORT_HPP_

#include "StmOmxIPort.hpp"
#include "StmOmxPortDefinition.hpp"
#include "StmOmxFifo.hpp"

#define MAX_BUFFER_IN_PORT 10

namespace stm {

/**
 * Class OmxPort implement general behavior common to all domain
 * (video, image, audio).
 */

class OmxPort: public IOmxPort
{
public:
    OmxPort();
    virtual ~OmxPort();

    /** Return port definition and buffer requirements */
    virtual const PortDefinition& definition() const = 0;

    virtual OMX_ERRORTYPE create();
    virtual void destroy() = 0;

    /** Return port index */
    unsigned int
    index() const;

    /** Check if port direction is input */
    bool isInput() const;

    /** Check if port direction is output */
    bool isOutput() const;

    /** Check if port is tunneled with another component */
    bool isTunneled() const;

    /** Return whether the port is populated with all of its buffers or not.
     * A disabled port is always unpopulated
     */
    bool isPopulated() const;

    virtual const char* name() const;

public:
    virtual OMX_ERRORTYPE
    tunnelRequest(OMX_HANDLETYPE hTunneledComp,
                  OMX_U32 nTunneledPort,
                  OMX_TUNNELSETUPTYPE* pTunnelSetup);

    virtual OMX_ERRORTYPE
    useBuffer(OMX_BUFFERHEADERTYPE** ppBufferHdr,
              OMX_PTR pAppPrivate,
              OMX_U32 nSizeBytes,
              OMX_U8* pBuffer);

    virtual OMX_ERRORTYPE
    allocateBuffer(OMX_BUFFERHEADERTYPE** ppBufferHdr,
                   OMX_PTR pAppPrivate,
                   OMX_U32 nSizeBytes);

    virtual OMX_ERRORTYPE
    freeBuffer(OMX_BUFFERHEADERTYPE* pBufferHdr);

    virtual OMX_ERRORTYPE disable();
    virtual OMX_ERRORTYPE enable();
    virtual OMX_ERRORTYPE flush();
    virtual OMX_ERRORTYPE pause();
    virtual OMX_ERRORTYPE resume();

    /** Return the number of buffer currently populating port */
    unsigned int nbBuffers() const;

protected:
    /** Return port definition and buffer requirements */
    virtual PortDefinition& definition() = 0;

    /** Increment the number of buffer populating port */
    void incrementNbBuffers();
    /** Decrement the number of buffer populating port */
    void decrementNbBuffers();

    OMX_BUFFERHEADERTYPE*
    allocateBufferHeader(OMX_PTR pAppPrivate,
                         OMX_U32 nSizeBytes) const;

    void
    freeBufferHeader(OMX_BUFFERHEADERTYPE* pBufferHdr) const;

protected:
    /** Name of the component */
    char m_strName[125];

    /** to get option of selective logging */
    bool mIsLoggingEnabled;
private:
   // Count the number of buffer used/allocated by the port
    unsigned int         m_nBuffersCount;

    // Flag to know if port is tunneled
    bool                 m_bTunnel;
    OMX_TUNNELSETUPTYPE  m_sTunnelSetup;
protected:
    OmxFifo<OMX_BUFFERHEADERTYPE*, MAX_BUFFER_IN_PORT> fifos;

    OMX_BUFFERHEADERTYPE* current;
    pthread_mutex_t currentMutex;
public:

    OmxFifo<OMX_BUFFERHEADERTYPE*, MAX_BUFFER_IN_PORT>* getRawFifos() { return &fifos; }
    int bufferInPipe() { return fifos.numberOfElement(); }

    virtual void push(OMX_BUFFERHEADERTYPE*   pBufferHdr);
    virtual OMX_BUFFERHEADERTYPE* pop();

    OMX_BUFFERHEADERTYPE* popAndLockCurrent();

    OMX_BUFFERHEADERTYPE* relockCurrent();
    void freeAndUnlockCurrent();
    void unlockCurrent();
};

inline unsigned int OmxPort::index() const
{ return this->definition().index();}

inline bool OmxPort::isInput() const
{ return (this->definition().direction() == OMX_DirInput); }

inline bool OmxPort::isOutput() const
{ return (this->definition().direction() == OMX_DirOutput); }

inline bool OmxPort::isTunneled() const
{ return (m_bTunnel == true); }

inline bool OmxPort::isPopulated() const
{
    if (this->definition().isEnabled() == false) {
        // Always false for a disabled port
        return false;
    }
    return this->definition().isPopulated();
}

inline unsigned int OmxPort::nbBuffers() const
{ return m_nBuffersCount; }

} // eof namespace stm

#endif // _STM_OMXPORT_HPP_
