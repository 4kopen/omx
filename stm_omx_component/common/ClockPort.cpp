/*
 * Copyright 2013 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file   ClockPort.cpp
 * @author STMicroelectronics
 */

#include "ClockPort.hpp"
#include "OMX_debug.h"

#include <stdio.h>
#include <string.h>
#if HAVE_ASSERT_H
#include <assert.h>
#endif

#if HAVE_DBGTASSERT_H
// Re-direct assert on DBGT_ASSERT macro
#define assert(expr)  OMX_DBGT_ASSERT(expr)
#endif

#include <linux/dbgt.h>


namespace stm {

OmxClockPort::OmxClockPort(unsigned int nIdx, OMX_DIRTYPE eDir,
                           const OmxComponent& owner)
    :OmxPort(),
     m_owner(owner),
     m_PortDefinition(nIdx, eDir)
{
    // Only format time is supported by clock port
    this->definition().format(OMX_OTHER_FormatTime);
    //check the string length before copy
    assert(strlen(this->component().name()) <= sizeof(m_strName));
    strcpy(m_strName,this->component().name());
    OMXDEBUG_UPDATE_AV_TRACE_SETTINGS(this->component().name());
}

OmxClockPort::~OmxClockPort()
{}

OMX_ERRORTYPE OmxClockPort::create()
{ return OMX_ErrorNone; }

void OmxClockPort::destroy()
{}

const OtherPortDefinition& OmxClockPort::definition() const
{ return m_PortDefinition; }

OtherPortDefinition& OmxClockPort::definition()
{ return m_PortDefinition; }

const OmxComponent& OmxClockPort::component() const
{ return m_owner; }

OMX_ERRORTYPE
OmxClockPort::getParameter(OMX_INDEXTYPE nIndex, OMX_PTR pParamStruct)
{
    OMX_DBGT_PROLOG("OmxClockPort::getParameter");
    switch (nIndex)
    {
    case OMX_IndexParamPortDefinition: {
        // Port definition
        ASSERT_STRUCT_TYPE(pParamStruct, OMX_PARAM_PORTDEFINITIONTYPE);
        OMX_PARAM_PORTDEFINITIONTYPE* param;
        param = (OMX_PARAM_PORTDEFINITIONTYPE*)pParamStruct;
        assert(param->nPortIndex == this->definition().index());
        *param = this->definition().getParamPortDef();
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }

    case OMX_IndexParamOtherPortFormat: {
        // Supported other port format
        ASSERT_STRUCT_TYPE(pParamStruct, OMX_OTHER_PARAM_PORTFORMATTYPE);
        OMX_OTHER_PARAM_PORTFORMATTYPE* param;
        param = (OMX_OTHER_PARAM_PORTFORMATTYPE*)pParamStruct;
        assert(param->nPortIndex == this->definition().index());
        // Only one format supported
        if (param->nIndex == 0) {
            param->eFormat = OMX_OTHER_FormatTime;
            OMX_DBGT_EPILOG();
            return OMX_ErrorNone;
        } else {
            OMX_DBGT_EPILOG("No more supported format");
            return OMX_ErrorNoMore;
        }
    }

    case OMX_IndexParamAudioPortFormat:
    case OMX_IndexParamVideoPortFormat:
    case OMX_IndexParamImagePortFormat: {
        // Unsupported port format
        OMX_DBGT_EPILOG("Unsupported index");
        return OMX_ErrorUnsupportedIndex;
    }

    default: {
        OMX_DBGT_EPILOG("Unsupported index");
        return OMX_ErrorUnsupportedIndex;
    }
    }
}

OMX_ERRORTYPE
OmxClockPort::setParameter(OMX_INDEXTYPE nIndex, OMX_PTR pParamStruct)
{
    OMX_DBGT_PROLOG();
    switch (nIndex)
    {
    case OMX_IndexParamPortDefinition: {
        // Port definition
        ASSERT_STRUCT_TYPE(pParamStruct, OMX_PARAM_PORTDEFINITIONTYPE);
        OMX_PARAM_PORTDEFINITIONTYPE* param;
        param = (OMX_PARAM_PORTDEFINITIONTYPE*)pParamStruct;
        assert(param->nPortIndex == this->definition().index());
        this->definition().setParamPortDef(param);
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }

    case OMX_IndexParamOtherPortFormat: {
        // Set a default port format
        ASSERT_STRUCT_TYPE(pParamStruct, OMX_OTHER_PARAM_PORTFORMATTYPE);
        OMX_OTHER_PARAM_PORTFORMATTYPE* param;
        param = (OMX_OTHER_PARAM_PORTFORMATTYPE*)pParamStruct;
        assert(param->nPortIndex == this->definition().index());
        // Check format is time
        if (param->eFormat == OMX_OTHER_FormatTime) {
            this->definition().format(param->eFormat);
            OMX_DBGT_EPILOG("Set port other format %u", param->eFormat);
            return OMX_ErrorNone;
        }

        // Unsupported other format
        OMX_DBGT_EPILOG("Unsupported other color format");
        return OMX_ErrorUnsupportedSetting;
    }

    case OMX_IndexParamAudioPortFormat:
    case OMX_IndexParamVideoPortFormat:
    case OMX_IndexParamImagePortFormat: {
        // Unsupported port format
        OMX_DBGT_EPILOG("Unsupported index");
        return OMX_ErrorUnsupportedIndex;
    }

    default: {
        OMX_DBGT_EPILOG("Unsupported index");
        return OMX_ErrorUnsupportedIndex;
    }
    }
}

OMX_ERRORTYPE
OmxClockPort::emptyThisBuffer(OMX_BUFFERHEADERTYPE* pBuffer)
{
    assert(0 && "Invalid command on clock port");
    return OMX_ErrorNone;
}

OMX_ERRORTYPE
OmxClockPort::fillThisBuffer(OMX_BUFFERHEADERTYPE* pBuffer)
{
    assert(0 && "Invalid command on clock port");
    return OMX_ErrorNone;
}

} // eof namespace stm
