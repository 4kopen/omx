/*
 * Copyright 2013 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file   AudioPortDefinition.hpp
 * @author STMicroelectronics
 */

#ifndef _STM_AUDIOPORTDEFINITION_HPP_
#define _STM_AUDIOPORTDEFINITION_HPP_

#include "StmOmxPortDefinition.hpp"

namespace stm {

/**
 * Class AudioPortDefinition contains audio port accessor.
 */
class AudioPortDefinition: public PortDefinition
{
public:
    /**@name Constructors and Destructors */
    //@{
    /** Constructor. */
    AudioPortDefinition(unsigned int nIdx, OMX_DIRTYPE eDir);

    AudioPortDefinition(unsigned int nIdx, OMX_DIRTYPE eDir,
                        const BufferReq& req);

    /** Copy constructor. */
    AudioPortDefinition(const AudioPortDefinition& vp);

    /** Destructor. */
    virtual
    ~AudioPortDefinition();
    //@}

    /**@name Audio property getters */
    //@{
    virtual const char*
    contentType() const;

    virtual bool
    flagErrorConcealment() const;

    virtual OMX_AUDIO_CODINGTYPE
    compressionFormat() const;
    //@}

    /**@name Audio property setters */
    //@{
    virtual void
    contentType(const char* mimeType);

    virtual void
    flagErrorConcealment(bool aERC);

    virtual void
    compressionFormat(OMX_AUDIO_CODINGTYPE aFmt);
    //@}

    virtual void
    setParamPortDef(OMX_PARAM_PORTDEFINITIONTYPE* pType);

private:
    static const unsigned int kMimeTypeSz = 32;
    char                      m_strMIMEType[kMimeTypeSz];
};


} // eof namespace stm

#endif // _STM_AUDIOPORTDEFINITION_HPP_
