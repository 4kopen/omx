/*
 * Copyright 2013 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file   StmOmxComponent.hpp
 * @author STMicroelectronics
 */

#ifndef _STM_OMXCOMPONENT_HPP_
#define _STM_OMXCOMPONENT_HPP_

#include "StmOmxIComponent.hpp"
#include "StmOmxPort.hpp"
#include "StmOmxFifo.hpp"
#include "Mutex.hpp"


/*
 * Initializes a data structure using a pointer to the structure.
 * The initialization of OMX structures always sets up the nSize and nVersion fields
 *   of the structure.
 */
#define OMX_CONF_INIT_STRUCT_PTR(_s_, _name_)   \
    memset((_s_), 0x0, sizeof(_name_));         \
    (_s_)->nSize = sizeof(_name_);              \
    stm::omxIlSpecVersion(&((_s_)->nVersion))

// Google Android extension definition
extern "C" {
#define OMX_enableAndroidNativeBuffers      (OMX_IndexVendorStartUnused + 1)
#define OMX_getAndroidNativeBufferUsage     (OMX_IndexVendorStartUnused + 2)
#define OMX_useAndroidNativeBuffer2         (OMX_IndexVendorStartUnused + 3)

typedef struct {
    OMX_U32 nSize;
    OMX_VERSIONTYPE nVersion;
    OMX_U32 nPortIndex;
    OMX_U32 nUsage;
} OMX_GetAndroidNativeBufferUsageParams;

typedef struct {
    OMX_U32 nSize;
    OMX_VERSIONTYPE nVersion;
    OMX_U32 nPortIndex;
    OMX_BOOL enable;
} OMX_EnableAndroidNativeBuffersParams;

}


namespace stm {

typedef enum {
    STOMX_PushCmdNewCommand,
    STOMX_PushCmdBuffer,
    STOMX_PushCmdExit
} STOMX_AsyncCommand;

class STOMXCommand
{
public:
    STOMXCommand():
        command(STOMX_PushCmdExit) {}
    STOMXCommand(OMX_BUFFERHEADERTYPE* _pBufferHdr):
        command(STOMX_PushCmdBuffer)
    {
        u.pBufferHdr = _pBufferHdr;
    }
    STOMXCommand(OMX_COMMANDTYPE _omxCmd):
        command(STOMX_PushCmdNewCommand)
    {
        u.omxcmd.cmd = _omxCmd;
    }
    STOMXCommand(OMX_COMMANDTYPE _omxCmd, OMX_U32 _data):
        command(STOMX_PushCmdNewCommand)
    {
        u.omxcmd.cmd = _omxCmd;
        u.omxcmd.data = _data;
    }

    STOMX_AsyncCommand command;
    union
    {
        struct
        {
            OMX_COMMANDTYPE cmd;
            OMX_U32 data;
        } omxcmd;
        OMX_BUFFERHEADERTYPE* pBufferHdr;
    } u;
};



class OmxComponent: public stm::IOmxComponent
{
public:

    OmxComponent(const char* _name, OMX_HANDLETYPE hComponent,
                 unsigned int _PortNumber);
    virtual ~OmxComponent();

    /** 2nd step construct to allocate component resources */
    virtual OMX_ERRORTYPE create() = 0;
    /** Component resource deallocation */
    virtual void destroy() = 0;

    /* A kind of factory */
    static OMX_ERRORTYPE ComponentInit(OmxComponent* self);


public:
    virtual const char* name() const;

    virtual OMX_HANDLETYPE handle() const;

    virtual unsigned int portNb() const;

    virtual const OmxPort& getPort(unsigned int nIdx) const = 0;

    virtual OMX_STATETYPE omxState() const;

    virtual
    OMX_ERRORTYPE getComponentVersion(OMX_VERSIONTYPE* pComponentVersion,
                                      OMX_VERSIONTYPE* pSpecVersion,
                                      OMX_UUIDTYPE* pComponentUUID);

    virtual
    OMX_ERRORTYPE sendCommand(OMX_COMMANDTYPE Cmd,
                              OMX_U32 nParam,
                              OMX_PTR pCmdData);

    virtual
    OMX_ERRORTYPE getParameter(OMX_INDEXTYPE nIndex,
                               OMX_PTR pComponentParameterStructure);

    virtual
    OMX_ERRORTYPE setParameter(OMX_INDEXTYPE nIndex,
                               OMX_PTR pComponentParameterStructure);

    virtual
    OMX_ERRORTYPE getConfig(OMX_INDEXTYPE nIndex,
                            OMX_PTR pComponentConfigStructure);

    virtual
    OMX_ERRORTYPE setConfig(OMX_INDEXTYPE nIndex,
                            OMX_PTR pComponentConfigStructure);

    virtual
    OMX_ERRORTYPE getExtensionIndex(OMX_STRING cParameterName,
                                    OMX_INDEXTYPE* pIndexType);

    virtual
    OMX_ERRORTYPE componentTunnelRequest(OMX_U32 nPort,
                                         OMX_HANDLETYPE hTunneledComp,
                                         OMX_U32 nTunneledPort,
                                         OMX_TUNNELSETUPTYPE* pSetup);

    virtual
    OMX_ERRORTYPE useBuffer(OMX_BUFFERHEADERTYPE** ppBufferHdr,
                            OMX_U32 nPortIndex,
                            OMX_PTR pAppPrivate,
                            OMX_U32 nSizeBytes,
                            OMX_U8* pBuffer);

    virtual
    OMX_ERRORTYPE allocateBuffer(OMX_BUFFERHEADERTYPE** ppBufferHdr,
                                 OMX_U32 nPortIndex,
                                 OMX_PTR pAppPrivate,
                                 OMX_U32 nSizeBytes);

    virtual
    OMX_ERRORTYPE freeBuffer(OMX_U32 nPortIndex,
                             OMX_BUFFERHEADERTYPE* pBuffer);

    virtual
    OMX_ERRORTYPE emptyThisBuffer(OMX_BUFFERHEADERTYPE* pBuffer);

    virtual
    OMX_ERRORTYPE fillThisBuffer(OMX_BUFFERHEADERTYPE* pBuffer);

    virtual
    OMX_ERRORTYPE setCallbacks(OMX_CALLBACKTYPE* pCallbacks,
                               OMX_PTR pAppData);

    virtual
    OMX_ERRORTYPE useEGLImage(OMX_BUFFERHEADERTYPE** ppBufferHdr,
                              OMX_U32 nPortIndex,
                              OMX_PTR pAppPrivate,
                              void* eglImage);

    virtual
    OMX_ERRORTYPE componentRoleEnum(OMX_U8* cRole,
                                    OMX_U32 nIndex);


protected:
    virtual OmxPort& getPort(unsigned int nIdx) = 0;

    template <typename T>
    OMX_ERRORTYPE getPortParam(OMX_INDEXTYPE  nParamIndex,
                               OMX_PTR        pParamStruct);
    template <typename T>
    OMX_ERRORTYPE setPortParam(OMX_INDEXTYPE  nParamIndex,
                               OMX_PTR        pParamStruct);

    OMX_BOOL isPortsPopulated() const;
    OMX_BOOL isPortsUnpopulated() const;

    virtual void notifyEventHandler(OMX_U32 eEvent,
                                    OMX_U32 nData1,
                                    OMX_U32 nData2,
                                    OMX_PTR pEventData) const;

    virtual void notifyEmptyBufferDone(OMX_BUFFERHEADERTYPE* pBuffer) const;

    virtual void notifyFillBufferDone(OMX_BUFFERHEADERTYPE* pBuffer) const;

    void omxState(OMX_STATETYPE newState);

    void setPriority(unsigned int nGrID, unsigned int nGrPrio);

    virtual OMX_ERRORTYPE configure() = 0;
    virtual OMX_ERRORTYPE deConfigure() {return OMX_ErrorNone;};
    virtual OMX_ERRORTYPE start() = 0;
    virtual void stop() = 0;
    virtual void execute() = 0;
    virtual OMX_ERRORTYPE pause();
    virtual OMX_ERRORTYPE resume();
    virtual OMX_ERRORTYPE flush(unsigned int nPortIdx);

private:
    static void* CmdThreadEntry(void* pThreadData);

    // Set new state management
    OMX_ERRORTYPE SetStateInvalid();
    OMX_ERRORTYPE SetStateLoaded();
    OMX_ERRORTYPE SetStateIdle();
    OMX_ERRORTYPE SetStateExecuting();
    OMX_ERRORTYPE SetStatePause();
    OMX_ERRORTYPE SetStateWaitForResources();

    // Error cases on new state management
    OMX_ERRORTYPE LowLevelError(OMX_ERRORTYPE eError);
    OMX_ERRORTYPE IncorrectTransition();
    OMX_ERRORTYPE UnsupportedSetting();
    OMX_ERRORTYPE InsufficientResources();
    OMX_ERRORTYPE LostResources();

    OMX_ERRORTYPE ValidStateTransition(OMX_STATETYPE newState);
    OMX_ERRORTYPE WaitingForPortPopulated(OMX_STATETYPE newState);
    OMX_ERRORTYPE WaitingForPortUnpopulated(OMX_STATETYPE newState);

    // OMX Command management
    OMX_ERRORTYPE CommandStateSet(OMX_STATETYPE newState);
    OMX_ERRORTYPE CommandPortDisable(STOMXCommand cmd);
    OMX_ERRORTYPE CommandPortEnable(STOMXCommand cmd);
    OMX_ERRORTYPE CommandFlush(STOMXCommand cmd);
    OMX_ERRORTYPE CmdThreadRunner();

protected:
    // could be smaller if we remove stop/flush of Command thread
    OmxFifo<STOMXCommand, 1000> Cmds;
    pthread_t CmdThread;

protected:
    /** Name of the component */
    char m_strName[125];

    /** to get option of selective logging */
    bool mIsLoggingEnabled;
private:
    /** The OMX state of the component */
    OMX_STATETYPE m_nState;

    /** The IL Client callbacks structure */
    OMX_CALLBACKTYPE *pClientCallbacks;

    /** Data pointer for application data  */
    OMX_PTR pAppData;

    /** Component OMX handle */
    OMX_HANDLETYPE hSelf;

    OMX_PORT_PARAM_TYPE sPortParam;

    /** Number of port of the component */
    const unsigned int m_nPortNumber;

    /** Component priority info */
    unsigned int m_nGroupPrio;
    unsigned int m_nGroupID;

    bool IsIdleStateCompleting;
    Mutex m_mutexCmd;
    Condition   m_condIdle;
};

inline void
OmxComponent::omxState(OMX_STATETYPE newState)
{ m_nState = newState; }

inline void
OmxComponent::setPriority(unsigned int nGrID, unsigned int nGrPrio)
{
    m_nGroupID = nGrID;
    m_nGroupPrio = nGrPrio;
}


} // eof namespace stm

#endif // _STM_OMXCOMPONENT_HPP_
