/*
 * Copyright 2013 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file   AudioPortDefinition.cpp
 * @author STMicroelectronics
 */

#include "AudioPortDefinition.hpp"

#include "OMX_AudioExt.h"
#include "OMX_debug.h"
#if HAVE_ASSERT_H
#include <assert.h>
#endif

#if HAVE_DBGTASSERT_H
// Re-direct assert on DBGT_ASSERT macro
#define assert(expr)  DBGT_ASSERT(expr)
#endif

#include <linux/dbgt.h>

#include <string.h>


namespace stm {

//=============================================================================
// AudioPortDefinition implementation
//=============================================================================

AudioPortDefinition::AudioPortDefinition(unsigned int nIdx, OMX_DIRTYPE eDir)
    :PortDefinition(nIdx, eDir)
{
    m_sPortDef.eDomain = OMX_PortDomainAudio;
    m_sPortDef.format.audio.pNativeRender = 0;
    m_sPortDef.format.audio.bFlagErrorConcealment = OMX_FALSE;
    m_sPortDef.format.audio.eEncoding = OMX_AUDIO_CodingUnused;
    m_strMIMEType[0] = '\0';
    m_sPortDef.format.audio.cMIMEType = m_strMIMEType;
}

AudioPortDefinition::~AudioPortDefinition()
{}

AudioPortDefinition::AudioPortDefinition(unsigned int nIdx, OMX_DIRTYPE eDir,
                                         const BufferReq& aReq)
    :PortDefinition(nIdx, eDir, aReq)
{
    m_sPortDef.eDomain = OMX_PortDomainAudio;
    m_sPortDef.format.audio.pNativeRender = 0;
    m_sPortDef.format.audio.bFlagErrorConcealment = OMX_FALSE;
    m_sPortDef.format.audio.eEncoding = OMX_AUDIO_CodingUnused;
    m_strMIMEType[0] = '\0';
    m_sPortDef.format.audio.cMIMEType = m_strMIMEType;
}

AudioPortDefinition::AudioPortDefinition(const AudioPortDefinition& ap)
    :PortDefinition(ap)
{
    m_sPortDef.eDomain = OMX_PortDomainAudio;
    m_sPortDef.format.audio.pNativeRender = 0;
    m_sPortDef.format.audio.bFlagErrorConcealment =
        (ap.flagErrorConcealment()? OMX_TRUE: OMX_FALSE);
    m_sPortDef.format.audio.eEncoding = ap.compressionFormat();
    //check the string length before copy
    assert(strlen(ap.contentType()) <= sizeof(m_strMIMEType));
    strcpy(m_strMIMEType, ap.contentType());
    m_sPortDef.format.audio.cMIMEType = m_strMIMEType;
}

const char*
AudioPortDefinition::contentType() const
{ return m_strMIMEType;}

bool
AudioPortDefinition::flagErrorConcealment() const
{ return m_sPortDef.format.audio.bFlagErrorConcealment;}

OMX_AUDIO_CODINGTYPE
AudioPortDefinition::compressionFormat() const
{ return m_sPortDef.format.audio.eEncoding;}

void
AudioPortDefinition::contentType(const char* mimeType)
{
    //check the string length before copy
    assert(strlen(mimeType) <= sizeof(m_strMIMEType));
    strcpy(m_strMIMEType, mimeType);
}

void
AudioPortDefinition::flagErrorConcealment(bool aERC)
{ m_sPortDef.format.audio.bFlagErrorConcealment = (aERC? OMX_TRUE: OMX_FALSE); }

void
AudioPortDefinition::compressionFormat(OMX_AUDIO_CODINGTYPE aFmt)
{
    m_sPortDef.format.audio.eEncoding = aFmt;
    switch((int)aFmt)
    {
    case OMX_AUDIO_CodingAAC:
        strcpy(m_strMIMEType, "audio/aac"); break;
    case OMX_AUDIO_CodingMP3:
        strcpy(m_strMIMEType, "audio/mpeg"); break;
    case OMX_AUDIO_CodingVORBIS:
        strcpy(m_strMIMEType, "audio/vorbis"); break;
    case OMX_AUDIO_CodingWMA:
        strcpy(m_strMIMEType, "audio/x-ms-wma"); break;
    case OMX_AUDIO_CodingAC3:
        strcpy(m_strMIMEType, "audio/ac3"); break;
    case OMX_AUDIO_CodingDTS:
        strcpy(m_strMIMEType, "audio/dts"); break;
    case OMX_AUDIO_CodingAMR:
        strcpy(m_strMIMEType, "audio/amrnb"); break;
    case OMX_AUDIO_CodingADPCM:
        // Let us not do anything here as we depend on the set MIMEType
        // strcpy(m_strMIMEType, "audio/adpcm");
        break;
    case OMX_AUDIO_CodingPCM:
        strcpy(m_strMIMEType, "audio/raw"); break;
    default:
        DBGT_WARNING("Audio coding format mime type not supported (%d)",
                     aFmt);
        strcpy(m_strMIMEType, "audio/undef");
    }
}

void
AudioPortDefinition::setParamPortDef(OMX_PARAM_PORTDEFINITIONTYPE* pType)
{
    PortDefinition::setParamPortDef(pType);
    assert(pType->eDomain == OMX_PortDomainAudio);
    this->flagErrorConcealment(pType->format.audio.bFlagErrorConcealment);
    assert(pType->format.audio.eEncoding != OMX_AUDIO_CodingUnused);
    this->compressionFormat(pType->format.audio.eEncoding);

    // Store the actual MIMEType sent
    this->contentType(pType->format.audio.cMIMEType);
    // Unused members
    //pType->format.audio.pNativeRender
}

} // eof namespace stm
