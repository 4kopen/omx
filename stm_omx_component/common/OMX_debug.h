/*
 * Copyright 2011 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Lines 138 to 235 in this file are direct prototype implementation of
 * OpenMAX methods as described in OpenMAX-IL API spec 1.0 and 1.2
 * available at
 * https://www.khronos.org/registry/omxil/
 *
 * File   ::    OMX_debug.h
 * Author ::    
 *
 *
 */

#ifndef _OMX_DEBUG_H_
#define _OMX_DEBUG_H_

#include <sys/cdefs.h>
#include <sys/types.h>

#include <OMX_Audio.h>
#include <OMX_Component.h>
#include <OMX_ContentPipe.h>
#include <OMX_Core.h>
//#include <OMX_CoreExt.h>
#include <OMX_Image.h>
#include <OMX_Index.h>
//#include <OMX_IndexExt.h>
#include <OMX_IVCommon.h>
#include <OMX_Other.h>
#include <OMX_Types.h>
#include <OMX_Video.h>
#include <OMX_debug.h>

__BEGIN_DECLS

/**
 * Wrappers for simpler generated code of strOMX_INDEXTYPE_struct function, allowing selection of
 * dump mean (Android log or snprintf). Those wrappers are needed as Android log cannot exceed 1024
 * characters.
 * LOG_BUF_SIZE defined to 1024 in system/core/liblog/logd_write.c
 */
#ifdef ANDROID
int OMX_wrapper_log_print
(int prio, const char *tag, char *buffer, size_t size, const char *fmt, ...);
#else
#define ALOGE(...) {printf("AE/" LOG_TAG ": " __VA_ARGS__);printf("\n");}
#define ALOGI(...) {printf("AI/" LOG_TAG ": " __VA_ARGS__);printf("\n");}
#define ALOGW(...) {printf("AW/" LOG_TAG ": " __VA_ARGS__);printf("\n");}
#endif
int OMX_debug_sprintf
(int prio, const char *tag, char *buffer, size_t size, const char *fmt, ...);

static int (*OMX_debug_log)
(int prio, const char *tag, char *buffer, size_t size, const char *fmt, ...);

#define OMXDEBUG_MAX_DEBUG_BUFFER 2048
#define OMX_DEBUG_LOG_LEVEL         1

#define OMX_DUMP_SIZE_PROPERTY_NAME   "debug.omx.dump.size"
#define OMX_DUMP_FORMAT_PROPERTY_NAME "debug.omx.dump.hex"
#define OMX_DUMP_OUTPUT_PROPERTY_NAME "debug.omx.dump.dir"
#define OMX_DUMP_INDEX_PROPERTY_NAME  "debug.omx.dump.idx"
#define OMX_DUMP_NAME_PROPERTY_NAME   "debug.omx.dump.name"
#define OMX_DUMP_PATH_PROPERTY_NAME   "debug.omx.dump.path"
#define OMX_DUMP_FORMAT_PROPERTY_NAME "debug.omx.dump.hex"

/* Data dump variables and function */
#define OMX_BUFFERDATA_NB_END_BYTES 16
#define OMX_BUFFERDATA_NB_BYTES_PER_LINES 16

#define OMX_enableAndroidNativeBuffers      (OMX_IndexVendorStartUnused + 1)
#define OMX_getAndroidNativeBufferUsage     (OMX_IndexVendorStartUnused + 2)
#define OMX_useAndroidNativeBuffer2         (OMX_IndexVendorStartUnused + 3)

#define OMX_STM_COLOR_FormatYVU420SemiPlanarNV21 OMX_QCOM_COLOR_FormatYVU420SemiPlanar

/********************************************************************************
 * Structure to hold and OMX CallBack from OMX components to client
 ********************************************************************************/
static int OMXdebug_flagAfterEos;
/** OMX IL wrapper call backs registered to the component */
typedef enum {
    OMX_EVENTHANDLER,
    OMX_EMPTYBUFFERDONE,
    OMX_FILLBUFFERDONE,
    CB_thread_exit,
} CbMessageType;

typedef struct {
    CbMessageType type;
    OMX_COMPONENTTYPE *hComponent;
    union {
        struct {
            OMX_PTR pAppData;
            OMX_EVENTTYPE eEvent;
            OMX_U32 nData1;
            OMX_U32 nData2;
            OMX_PTR pEventData;
        } OnEventDone;
        struct {
            OMX_PTR pAppData;
            OMX_BUFFERHEADERTYPE* pBuffer;
        } OnBufferDone;
    } args;
} CB_Message;

void sprintCBMessage(char *buffer, CB_Message *message, size_t size);


/**********************************************************************************/

/*
* The OMX components interfaces the OMX IL client will call
* The wrapper will return the following functions as component entry point
*/
/** Dump buffer's data in binary format on this component */
void  dumpData(OMX_BUFFERHEADERTYPE* pBuffer,
               char *portType,
               char *path,
               int portIndex,
               unsigned long target_size,
               const char *comp_name,
               OMX_HANDLETYPE hComponent);

void  OMXDebug_Init(OMX_COMPONENTTYPE *handle, const char* name);

OMX_ERRORTYPE OMXDebug_DeInit(void);

void  OMXDebug_GetParameter(OMX_HANDLETYPE hComponent,
                            OMX_INDEXTYPE  nIndex,
                            OMX_PTR        pComponentParameterStructure,
                            const char*    name);

void  OMXDebug_SetParameter(OMX_HANDLETYPE hComponent,
                            OMX_INDEXTYPE  nIndex,
                            OMX_PTR        pComponentParameterStructure,
                            const char*    name);

void OMXDebug_GetConfig (OMX_HANDLETYPE hComponent,
                         OMX_INDEXTYPE  nIndex,
                         OMX_PTR        pComponentConfigStructure,
                         const char*    name);

void OMXDebug_SetConfig (OMX_HANDLETYPE hComponent,
                         OMX_INDEXTYPE  nIndex,
                         OMX_PTR        pComponentConfigStructure,
                         const char*    name);

void OMXDebug_GetComponentVersion (OMX_HANDLETYPE   hComponent,
                                   OMX_STRING       pComponentName,
                                   OMX_VERSIONTYPE* pComponentVersion,
                                   OMX_VERSIONTYPE* pSpecVersion,
                                   OMX_UUIDTYPE*    pComponentUUID,
                                   const char*      name);

void OMXDebug_SendCommand (OMX_HANDLETYPE  hComponent,
                           OMX_COMMANDTYPE Cmd,
                           OMX_U32         nParam,
                           OMX_PTR         pCmdData,
                           const char*     name);

void OMXDebug_UseBuffer(OMX_HANDLETYPE         hComponent,
                        OMX_BUFFERHEADERTYPE** ppBufferHdr,
                        OMX_U32                nPortIndex,
                        OMX_PTR                pAppPrivate,
                        OMX_U32                nbBuffers,
                        OMX_U32                nSizeBytes,
                        OMX_U8*                pBuffer,
                        const char*            name);

void OMXDebug_AllocateBuffer(OMX_HANDLETYPE         hComponent,
                             OMX_BUFFERHEADERTYPE** ppBufferHdr,
                             OMX_U32                nPortIndex,
                             OMX_PTR                pAppPrivate,
                             OMX_U32                nbBuffers,
                             OMX_U32                nSizeBytes,
                             const char*            name);

void OMXDebug_FreeBuffer(OMX_HANDLETYPE        hComponent,
                         OMX_U32               nPortIndex,
                         OMX_BUFFERHEADERTYPE* pBuffer,
                         OMX_U32               nbBuffers,
                         const char*           name);

void OMXDebug_EmptyThisBuffer(OMX_HANDLETYPE        hComponent,
                              OMX_BUFFERHEADERTYPE* pBuffer,
                              const char*           name);

void OMXDebug_FillThisBuffer(OMX_HANDLETYPE        hComponent,
                             OMX_BUFFERHEADERTYPE* pBuffer,
                             const char*           name);

void OMXDebug_GetExtensionIndex(OMX_HANDLETYPE hComponent,
                                OMX_STRING     cParameterName,
                                OMX_INDEXTYPE* pIndexType,
                                const char*    name);

void OMXDebug_ComponentTunnelRequest(OMX_HANDLETYPE       hComponent,
                                     OMX_U32              nPort,
                                     OMX_HANDLETYPE       hTunneledComp,
                                     OMX_U32              nTunneledPort,
                                     OMX_TUNNELSETUPTYPE* pTunnelSetup,
                                     const char*          name);

OMX_ERRORTYPE OMXDebug_EmptyBufferDone(OMX_HANDLETYPE          hComponent,
                                       OMX_PTR                 pAppData,
                                       OMX_BUFFERHEADERTYPE*   pBuffer,
                                       const char*             name);

OMX_ERRORTYPE OMXDebug_FillBufferDone(OMX_HANDLETYPE          hComponent,
                                      OMX_PTR                 pAppData,
                                      OMX_BUFFERHEADERTYPE*   pBuffer,
                                      const char*             name);

OMX_ERRORTYPE  OMXDebug_EventHandler(OMX_HANDLETYPE hComponent,
                                     OMX_PTR        pAppData,
                                     OMX_EVENTTYPE  eEvent,
                                     OMX_U32        nData1,
                                     OMX_U32        nData2,
                                     OMX_PTR        pEventData,
                                     const char*           name);

void OMXDebug_onStateChange(OMX_STATETYPE newState,
                            const char*   name);


/** Macro for getting OMX enums without need for a buffer. example of use:
 *   LOG("err=%s", OMX_TYPE_TO_STR(OMX_ERRORTYPE, err)); */
#define OMX_TYPE_TO_STR(type, value) str##type(value, NULL, 0)

/********************************************************************************
 * Hand written functions
 ********************************************************************************/
/**
 * Return string describing version number
 * @param version pointer to the version to dump
 * @param buffer to dump into
 * @param size in bytes of provided buffer
 */
char * strOMX_VERSIONTYPE(OMX_VERSIONTYPE *version, char *buffer, size_t size);

/**
 * Return string describing OMX very long unique identifier
 * @param pComponentUUID pointer to the component UUID
 * @param buffer to dump into
 * @param size in bytes of provided buffer
 */
char * strOMX_UUIDTYPE(OMX_UUIDTYPE *pComponentUUID, char *buffer, size_t size);

/**
 * Return string describing an OMX event
 * @param eEvent the event
 * @param nData1 will be the OMX_ERRORTYPE for an error event and will
 * be an OMX_COMMANDTYPE for a command complete event and
 * OMX_INDEXTYPE for a OMX_PortSettingsChanged event.
 * @param nData2 will hold further information related to the
 * event. Can be OMX_STATETYPE for a OMX_CommandStateSet command or
 * port index for a OMX_PortSettingsChanged event. Default value is 0
 * if not used.
 * @param pEventData Pointer to additional event-specific data (see
 * spec for meaning).
 * @param buffer to dump into
 * @param size in bytes of provided buffer
 */
char * strOMX_EVENTHANDLER(OMX_EVENTTYPE eEvent,
                           OMX_U32 nData1,
                           OMX_U32 nData2,
                           OMX_PTR pEventData,
                           char *buffer, size_t size);

/**
 * Return string describing an OMX tunnel setup request
 * @param pTunnelSetup the OMX tunnel setup request
 * @param buffer used by function
 * @param size in bytes of provided buffer
 */
char * strOMX_TUNNELSETUPTYPE(OMX_TUNNELSETUPTYPE* pTunnelSetup, char *buffer, size_t size);

/**
 * Return string describing an OMX buffer header
 * @param pBuffer the pointer to the OMX buffer header
 * @param buffer used by function
 * @param size in bytes of provided buffer
 */
char * strOMX_BUFFERHEADERTYPE(OMX_BUFFERHEADERTYPE* pBuffer, char *buffer, size_t size);


/**
 * Return a string describing an OMX buffer content
 * @param pBuffer the pointer to the OMX buffer header
 * @param buffer pointer to buffer the function will allocate/free based on the
 * requested dump size
 * @param size pointer to size in bytes of the buffer needed to perform the dump
 * @param nb_bytes_to_dump number of bytes to dump.
 * @param nb_end_bytes number of trailing bytes to dump
 * @param nb_bytes_per_lines number of bytes to display before emitting a CR
 */
char * strOMX_BUFFERDATA(OMX_BUFFERHEADERTYPE* pBuffer,
                         char **buffer, size_t *size,
                         unsigned int nb_bytes_to_dump,
                         unsigned int nb_end_bytes,
                         unsigned int nb_bytes_per_lines);

/**
 * Return string describing an OMX send command operation
 * @param cmd the OMX command
 * @param param the argument to the OMX command
 * @param buffer used by function
 * @param size in bytes of provided buffer
 */
char * strOMX_COMMAND(OMX_COMMANDTYPE cmd, OMX_U32 param, char *buffer, size_t size);

/********************************************************************************
 * Below functions are generated by GenerateOMXdebug script
 ********************************************************************************/

/**Dump the OMX type referred by val in a human readable form and return the corresponding string
 * @param val the desired type or index
 * @param buffer used by function in case val is not known
 * @param size in bytes of provided buffer
 * @note buffer argument is returned only in case val is not known otherwise buffer will be
 * unchanged and a predefined string will be returned. This is more efficient than performing a
 * string copy all the time.
 */

char * strOMX_ERRORTYPE(OMX_ERRORTYPE val, char *buffer, size_t size);
char * strOMX_AUDIO_CHANNELTYPE(OMX_AUDIO_CHANNELTYPE val, char *buffer, size_t size);
char * strOMX_VIDEO_CODINGTYPE(OMX_VIDEO_CODINGTYPE val, char *buffer, size_t size);
char * strOMX_AUDIO_WMAPROFILETYPE(OMX_AUDIO_WMAPROFILETYPE val, char *buffer, size_t size);
char * strOMX_TIME_SEEKMODETYPE(OMX_TIME_SEEKMODETYPE val, char *buffer, size_t size);
char * strOMX_METERINGTYPE(OMX_METERINGTYPE val, char *buffer, size_t size);
char * strOMX_IMAGE_QUANTIZATIONTABLETYPE(OMX_IMAGE_QUANTIZATIONTABLETYPE val, char *buffer, size_t size);
char * strOMX_AUDIO_AACSTREAMFORMATTYPE(OMX_AUDIO_AACSTREAMFORMATTYPE val, char *buffer, size_t size);
char * strOMX_VIDEO_MPEG4PROFILETYPE(OMX_VIDEO_MPEG4PROFILETYPE val, char *buffer, size_t size);
char * strOMX_FOCUSSTATUSTYPE(OMX_FOCUSSTATUSTYPE val, char *buffer, size_t size);
char * strOMX_METADATASEARCHMODETYPE(OMX_METADATASEARCHMODETYPE val, char *buffer, size_t size);
char * strOMX_TRANSITIONEFFECTTYPE(OMX_TRANSITIONEFFECTTYPE val, char *buffer, size_t size);
char * strOMX_SUSPENSIONPOLICYTYPE(OMX_SUSPENSIONPOLICYTYPE val, char *buffer, size_t size);
char * strOMX_VIDEO_WMVFORMATTYPE(OMX_VIDEO_WMVFORMATTYPE val, char *buffer, size_t size);
char * strOMX_METADATACHARSETTYPE(OMX_METADATACHARSETTYPE val, char *buffer, size_t size);
char * strOMX_EXPOSURECONTROLTYPE(OMX_EXPOSURECONTROLTYPE val, char *buffer, size_t size);
char * strOMX_VIDEO_MOTIONVECTORTYPE(OMX_VIDEO_MOTIONVECTORTYPE val, char *buffer, size_t size);
char * strOMX_IMAGE_HUFFMANTABLETYPE(OMX_IMAGE_HUFFMANTABLETYPE val, char *buffer, size_t size);
char * strOMX_AUDIO_ECHOCANTYPE(OMX_AUDIO_ECHOCANTYPE val, char *buffer, size_t size);
char * strOMX_OTHER_FORMATTYPE(OMX_OTHER_FORMATTYPE val, char *buffer, size_t size);
char * strOMX_VIDEO_INTRAREFRESHTYPE(OMX_VIDEO_INTRAREFRESHTYPE val, char *buffer, size_t size);
char * strOMX_BUFFERSUPPLIERTYPE(OMX_BUFFERSUPPLIERTYPE val, char *buffer, size_t size);
char * strOMX_AUDIO_MIDIPLAYBACKSTATETYPE(OMX_AUDIO_MIDIPLAYBACKSTATETYPE val, char *buffer, size_t size);
char * strOMX_IMAGEFILTERTYPE(OMX_IMAGEFILTERTYPE val, char *buffer, size_t size);
char * strOMX_AUDIO_MIDISOUNDBANKLAYOUTTYPE(OMX_AUDIO_MIDISOUNDBANKLAYOUTTYPE val, char *buffer, size_t size);
char * strOMX_PORTDOMAINTYPE(OMX_PORTDOMAINTYPE val, char *buffer, size_t size);
char * strOMX_EXTRADATATYPE(OMX_EXTRADATATYPE val, char *buffer, size_t size);
char * strOMX_AUDIO_RAFORMATTYPE(OMX_AUDIO_RAFORMATTYPE val, char *buffer, size_t size);
char * strOMX_AUDIO_MIDIFORMATTYPE(OMX_AUDIO_MIDIFORMATTYPE val, char *buffer, size_t size);
char * strOMX_MIRRORTYPE(OMX_MIRRORTYPE val, char *buffer, size_t size);
char * strOMX_ENDIANTYPE(OMX_ENDIANTYPE val, char *buffer, size_t size);
char * strOMX_AUDIO_AMRDTXMODETYPE(OMX_AUDIO_AMRDTXMODETYPE val, char *buffer, size_t size);
char * strOMX_DIRTYPE(OMX_DIRTYPE val, char *buffer, size_t size);
char * strOMX_WHITEBALCONTROLTYPE(OMX_WHITEBALCONTROLTYPE val, char *buffer, size_t size);
char * strOMX_IMAGE_FLASHCONTROLTYPE(OMX_IMAGE_FLASHCONTROLTYPE val, char *buffer, size_t size);
char * strOMX_AUDIO_MP3STREAMFORMATTYPE(OMX_AUDIO_MP3STREAMFORMATTYPE val, char *buffer, size_t size);
char * strOMX_DITHERTYPE(OMX_DITHERTYPE val, char *buffer, size_t size);
char * strOMX_SUSPENSIONTYPE(OMX_SUSPENSIONTYPE val, char *buffer, size_t size);
char * strOMX_AUDIO_G729TYPE(OMX_AUDIO_G729TYPE val, char *buffer, size_t size);
char * strOMX_IMAGE_CODINGTYPE(OMX_IMAGE_CODINGTYPE val, char *buffer, size_t size);
//char * strOMX_EVENTEXTTYPE(OMX_EVENTEXTTYPE val, char *buffer, size_t size);
char * strOMX_IMAGE_FOCUSCONTROLTYPE(OMX_IMAGE_FOCUSCONTROLTYPE val, char *buffer, size_t size);
char * strOMX_INDEXTYPE(OMX_INDEXTYPE val, char *buffer, size_t size);
char * strOMX_VIDEO_AVCLOOPFILTERTYPE(OMX_VIDEO_AVCLOOPFILTERTYPE val, char *buffer, size_t size);
char * strOMX_AUDIO_AMRFRAMEFORMATTYPE(OMX_AUDIO_AMRFRAMEFORMATTYPE val, char *buffer, size_t size);
char * strOMX_VIDEO_RVFORMATTYPE(OMX_VIDEO_RVFORMATTYPE val, char *buffer, size_t size);
char * strOMX_AUDIO_CHANNELMODETYPE(OMX_AUDIO_CHANNELMODETYPE val, char *buffer, size_t size);
char * strOMX_VIDEO_MPEG2PROFILETYPE(OMX_VIDEO_MPEG2PROFILETYPE val, char *buffer, size_t size);
char * strOMX_VIDEO_AVCPROFILETYPE(OMX_VIDEO_AVCPROFILETYPE val, char *buffer, size_t size);
char * strOMX_VIDEO_H263PROFILETYPE(OMX_VIDEO_H263PROFILETYPE val, char *buffer, size_t size);
char * strOMX_VIDEO_CONTROLRATETYPE(OMX_VIDEO_CONTROLRATETYPE val, char *buffer, size_t size);
char * strOMX_COLORBLENDTYPE(OMX_COLORBLENDTYPE val, char *buffer, size_t size);
char * strOMX_AUDIO_CODINGTYPE(OMX_AUDIO_CODINGTYPE val, char *buffer, size_t size);
char * strOMX_DATAUNITENCAPSULATIONTYPE(OMX_DATAUNITENCAPSULATIONTYPE val, char *buffer, size_t size);
char * strOMX_VIDEO_MPEG2LEVELTYPE(OMX_VIDEO_MPEG2LEVELTYPE val, char *buffer, size_t size);
char * strOMX_AUDIO_SBCALLOCMETHODTYPE(OMX_AUDIO_SBCALLOCMETHODTYPE val, char *buffer, size_t size);
char * strOMX_AUDIO_WMAFORMATTYPE(OMX_AUDIO_WMAFORMATTYPE val, char *buffer, size_t size);
char * strOMX_VIDEO_MPEG4LEVELTYPE(OMX_VIDEO_MPEG4LEVELTYPE val, char *buffer, size_t size);
char * strOMX_AUDIO_CDMARATETYPE(OMX_AUDIO_CDMARATETYPE val, char *buffer, size_t size);
char * strOMX_AUDIO_MIDISOUNDBANKTYPE(OMX_AUDIO_MIDISOUNDBANKTYPE val, char *buffer, size_t size);
char * strOMX_AUDIO_AMRBANDMODETYPE(OMX_AUDIO_AMRBANDMODETYPE val, char *buffer, size_t size);
char * strOMX_COLOR_FORMATTYPE(OMX_COLOR_FORMATTYPE val, char *buffer, size_t size);
char * strOMX_AUDIO_G726MODE(OMX_AUDIO_G726MODE val, char *buffer, size_t size);
char * strOMX_AUDIO_PCMMODETYPE(OMX_AUDIO_PCMMODETYPE val, char *buffer, size_t size);
char * strOMX_DATAUNITTYPE(OMX_DATAUNITTYPE val, char *buffer, size_t size);
char * strOMX_METADATASCOPETYPE(OMX_METADATASCOPETYPE val, char *buffer, size_t size);
char * strOMX_NUMERICALDATATYPE(OMX_NUMERICALDATATYPE val, char *buffer, size_t size);
char * strOMX_VIDEO_PICTURETYPE(OMX_VIDEO_PICTURETYPE val, char *buffer, size_t size);
char * strOMX_VIDEO_H263LEVELTYPE(OMX_VIDEO_H263LEVELTYPE val, char *buffer, size_t size);
char * strOMX_VIDEO_AVCLEVELTYPE(OMX_VIDEO_AVCLEVELTYPE val, char *buffer, size_t size);
char * strOMX_AUDIO_STEREOWIDENINGTYPE(OMX_AUDIO_STEREOWIDENINGTYPE val, char *buffer, size_t size);
char * strOMX_AUDIO_AACPROFILETYPE(OMX_AUDIO_AACPROFILETYPE val, char *buffer, size_t size);
char * strOMX_VIDEO_AVCSLICEMODETYPE(OMX_VIDEO_AVCSLICEMODETYPE val, char *buffer, size_t size);
char * strOMX_BOOL(OMX_BOOL val, char *buffer, size_t size);
char * strOMX_AUDIO_G723RATE(OMX_AUDIO_G723RATE val, char *buffer, size_t size);


/**
 * Return string describing an OMX state
 * @param val the OMX state
 * @param buffer used by function in case val is not known
 * @param size in bytes of provided buffer
 * @note buffer argument is returned only in case val is not known otherwise buffer will be
 * unchanged and a predefined string will be returned. This is more efficient than performing a
 * string copy all the time.
 */
char * strOMX_STATETYPE(OMX_STATETYPE val, char *buffer, size_t size);

/**
 * Return string describing an OMX command
 * @param val the OMX command
 * @param buffer used by function in case val is not known
 * @param size in bytes of provided buffer
 * @note buffer argument is returned only in case val is not known otherwise buffer will be
 * unchanged and a predefined string will be returned. This is more efficient than performing a
 * string copy all the time.
 */
char * strOMX_COMMANDTYPE(OMX_COMMANDTYPE val, char *buffer, size_t size);

/**
 * Return string describing an OMX event
 * @param val the OMX event
 * @param buffer used by function in case val is not known
 * @param size in bytes of provided buffer
 * @note buffer argument is returned only in case val is not known otherwise buffer will be
 * unchanged and a predefined string will be returned. This is more efficient than performing a
 * string copy all the time.
 */
char * strOMX_EVENTTYPE(OMX_EVENTTYPE val, char *buffer, size_t size);

/**
 * Dump in a human readable form an OMX configuration or parameter structure index.
 * @param val the OMX configuration or parameter structure index.
 * @param buffer used by function in case val is not known
 * @param size in bytes of provided buffer
 * @note buffer argument is returned only in case val is not known otherwise buffer will be
 * unchanged and a predefined string will be returned. This is more efficient than performing a
 * string copy all the time.
 */
char * strOMX_INDEXTYPE(OMX_INDEXTYPE val, char *buffer, size_t size);

/**
 * Dump in a human readable form an OMX configuration or parameter structure.
 * @param index the structure index provided in ptr.
 * @param ptr the pointer to the strucuture to dump.
 * @param level if level is 1 then only index name is dumped, if > 1 then all structure fields are
 * dumped.
 * @param prio is the android log priority to be used in case a tag is provided.
 * @param tag if not NULL and ANDROID is defined then Android log using prio priority will be
 * used to dump the structure content. If NULL or ANDROID not defined then content will be dumped to
 * the provided buffer.
 * @param prefix if not NULL then used to prefix all lines generated in this function
 * @param buffer the buffer in which to dump
 * @param size the buffer size
 */
void strOMX_INDEXTYPE_struct(OMX_U32 index,
                             OMX_PTR ptr,
                             int level,
                             int prio,
                             const char *tag,
                             const char *prefix,
                             char *buffer,
                             size_t size);


#define OMX_DEBUG_ADEC_TRACE_PROPERTY "debug.adec.trace"
#define OMX_DEBUG_AENC_TRACE_PROPERTY "debug.aenc.trace"
#define OMX_DEBUG_VDEC_TRACE_PROPERTY "debug.vdec.trace"
#define OMX_DEBUG_VENC_TRACE_PROPERTY "debug.venc.trace"

#define OMXDEBUG_UPDATE_AV_TRACE_SETTINGS(compName) \
    do { \
        long unsigned int propValue = 0; \
        if (0 == strncmp(compName, "[OMX.STM.Audio.D", 16) || \
            0 == strncmp(compName, "COM.ST.Audio.D", 14)) { \
            GET_PROPERTY(OMX_DEBUG_ADEC_TRACE_PROPERTY, value, "0"); \
            propValue = strtoul(value, NULL, 16); \
        } else if (0 == strncmp(compName, "[OMX.STM.Audio.E", 16) || \
            0 == strncmp(compName, "COM.ST.Audio.E", 14)) { \
            GET_PROPERTY(OMX_DEBUG_AENC_TRACE_PROPERTY, value, "0"); \
            propValue = strtoul(value, NULL, 16); \
        } else if (0 == strncmp(compName, "[OMX.STM.Audio.R", 16) || \
            0 == strncmp(compName, "COM.ST.Audio.R", 14)) { \
            GET_PROPERTY(OMX_DEBUG_AENC_TRACE_PROPERTY, value, "0"); \
            propValue = strtoul(value, NULL, 16); \
        } else if (0 == strncmp(compName, "[OMX.STM.Video.D", 16) || \
            0 == strncmp(compName, "COM.ST.Video.D", 14)) { \
            GET_PROPERTY(OMX_DEBUG_VDEC_TRACE_PROPERTY, value, "0"); \
            propValue = strtoul(value, NULL, 16); \
        } else if (0 == strncmp(compName, "[OMX.STM.Video.E", 16) || \
            0 == strncmp(compName, "COM.ST.Video.E", 14)) { \
            GET_PROPERTY(OMX_DEBUG_VENC_TRACE_PROPERTY, value, "0"); \
            propValue = strtoul(value, NULL, 16); \
        } else if (0 == strncmp(compName, "[OMX.STM.Video.R", 16) || \
            0 == strncmp(compName, "COM.ST.Video.R", 14)) { \
            GET_PROPERTY(OMX_DEBUG_VENC_TRACE_PROPERTY, value, "0"); \
            propValue = strtoul(value, NULL, 16); \
        } else if (0 == strncmp(compName, "[OMX.STM.Clock", 14) || \
            0 == strncmp(compName, "COM.ST.Clock", 12)) { \
            GET_PROPERTY(OMX_DEBUG_VENC_TRACE_PROPERTY, value, "0"); \
            propValue = strtoul(value, NULL, 16); \
        } else { \
            ALOGI("Unknown Component %s", compName); \
        } \
        mIsLoggingEnabled = (propValue != 0); \
    } while (0)


#define OMX_DBGT_ASSERT(condition, args...) \
    do { \
      if (!(condition)) { \
        ALOGE("!!! %s" DBGT_INDENT(DBGT_LAYER)" ""%s" \
              "assertion !(" #condition ") ""failed at %s, %s:%d", \
              DBGT_PREFIX, name(), __PRETTY_FUNCTION__, __FILE__, __LINE__); \
        LOG_ALWAYS_FATAL_IF(!(condition), ## args); \
      } \
    } while (0)

#define OMX_DBGT_CRITICAL(fmt, args...) \
    ALOGE( "!! %s" DBGT_INDENT(DBGT_LAYER)"%s "" %s " \
          fmt " %s:%d", DBGT_PREFIX, name(), __PRETTY_FUNCTION__, \
          ## args, __FILE__, __LINE__)

#define OMX_DBGT_ERROR(fmt, args...) \
    ALOGE( "! %s" DBGT_INDENT(DBGT_LAYER)" %s " "%s" \
         fmt, DBGT_PREFIX, name(), __PRETTY_FUNCTION__, ## args)

#define OMX_DBGT_WARNING(fmt, args...) \
    ALOGW( "? %s" DBGT_INDENT(DBGT_LAYER)"%s""%s " \
         fmt, DBGT_PREFIX, name(), __PRETTY_FUNCTION__, ## args)

#ifndef DBGT_CONFIG_DEBUG

#define OMX_DBGT_PINFO(...)
#define OMX_DBGT_PTRACE(...)
#define OMX_DBGT_PDEBUG(...)
#define OMX_DBGT_PROLOG(...)
#define OMX_DBGT_EPILOG(...)
#define OMX_DBGT_CHECK_RETURN(C, V) do {\
    if (!(C)) { \
        OMX_DBGT_ERROR("failed(" #C ") => " #V); \
        return V; } \
    } while(0)
#define OMX_DBGT_CHECK_EXIT(C, V) do {\
    if (!(C)) { \
        eError = V;\
        OMX_DBGT_ERROR("failed(" #C ") => " #V); \
        goto EXIT; } \
    } while(0)
#define OMX_DBGT_CHECK_EXIT(C, V) do {\
    if (!(C)) { \
        eError = V;\
        OMX_DBGT_ERROR("failed(" #C ") => " #V); \
        goto EXIT; } \
    } while(0)
#define OMX_DBGT_CHECK(C) do {\
    if (!(C)) { \
        OMX_DBGT_ASSERT("assert failed(" #C ")"); \
        } \
    } while(0)
#else  /* #ifndef DBGT_CONFIG_DEBUG */

#define OMX_DBGT_PINFO(fmt, args...) \
    do { \
        if (mIsLoggingEnabled) { \
        DBGT_PINFO("%s" fmt, name(), ##args); \
        } \
    } while (0)

#define OMX_DBGT_PTRACE(fmt, args...) \
    do { \
        if (mIsLoggingEnabled) { \
        DBGT_PTRACE("%s" fmt, name(), ##args); \
        } \
    } while (0)

#define OMX_DBGT_PDEBUG(fmt, args...) \
    do { \
        if (mIsLoggingEnabled) { \
        DBGT_PDEBUG("%s" fmt, name(), ##args); \
        } \
    } while (0)

#define OMX_DBGT_PVERBOSE(fmt, args...) \
    do { \
        if (mIsLoggingEnabled) { \
        DBGT_PVERBOSE("%s"fmt, name(), ##args); \
        } \
    } while (0)

#define OMX_DBGT_EPILOG(fmt, args...) \
    do { \
        if (mIsLoggingEnabled) { \
            if (DBGT_VAR & (0x2<<(DBGT_LAYER*4))) { \
            ALOGD( "  %s" DBGT_INDENT(DBGT_LAYER)"%s""< %s " \
                  fmt, DBGT_PREFIX, name(), __FUNCTION__, ## args); \
            } \
        } \
    } while (0)

#define OMX_DBGT_PROLOG(fmt, args...) \
    do { \
        if (mIsLoggingEnabled) { \
            if (DBGT_VAR & (0x2<<(DBGT_LAYER*4))) { \
            ALOGD( "  %s" DBGT_INDENT(DBGT_LAYER)"%s""> %s " \
                  fmt, DBGT_PREFIX, name(), __FUNCTION__, ## args); \
            } \
        } \
    } while (0)


#define OMX_DBGT_CHECK_EXIT(C, V) do {\
    if (!(C)) { \
        eError = V;\
        OMX_DBGT_ERROR("failed(" #C ") => " #V); \
        goto EXIT; } \
    } while(0)

#define OMX_DBGT_CHECK_EXIT_MSG(C, V, M) do {\
    if (!(C)) { \
        eError = V;\
        OMX_DBGT_ERROR("failed(" #C ") => " #V " (%s)", M); \
        goto EXIT; } \
    } while(0)

#define OMX_DBGT_CHECK_RETURN(C, V) do {\
    if (!(C)) { \
        OMX_DBGT_ERROR("failed(" #C ") => " #V); \
        return V; } \
    } while(0)

#define OMX_DBGT_CHECK_RETURN_MSG(C, V, M) do {\
    if (!(C)) { \
        OMX_DBGT_ERROR("failed(" #C ") => " #V " (%s)", M); \
        return V; } \
    } while(0)

#define OMX_DBGT_CHECK(C) do {\
    if (!(C)) { \
        OMX_DBGT_ASSERT("assert failed(" #C ")"); \
        } \
    } while(0)

#endif //ifndef DBGT_CONFIG_DEBUG

__END_DECLS

#endif /* _OMX_DEBUG_H_ */

