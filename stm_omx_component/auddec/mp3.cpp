/*
 * Copyright 2011 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *
 * File   ::    mp3.cpp
 * Author ::    Deepak Pandey (deepak.pandey@st.com)
 *
 *
 */

#include "AudioPesAdapter.hpp"
#include "AudioDecoder.hpp"
#include "OMX_debug.h"

#include "pes.hpp"

#define DBGT_PREFIX "MP3 "
#define DBGT_LAYER  1
#include <linux/dbgt.h>

#define PTS_CORRECTION(x) (((x) * 90000)/1000000)

namespace stm {

Mp3PesAdapter::Mp3PesAdapter(const ADECComponent& dec)
    :AudioPesAdapter(dec)
{}
  
Mp3PesAdapter::~Mp3PesAdapter()
{}

int Mp3PesAdapter::emitCodecConfig(uint8_t* pBuffer,
                                   unsigned int nFilledLen) const
{
    return 0;
}

int Mp3PesAdapter::emitFrame(uint8_t* pBuffer,
                             unsigned int nFilledLen,
                             uint64_t nTimeStamp) const
{
    int packet_length = nFilledLen;
    int err = 0;
    OMX_U32 PesHeaderLength;
    uint64_t nPTS = PTS_CORRECTION(nTimeStamp);

    OMX_DBGT_PROLOG("pBuf=%p, len=%d, nTimeStamp=%llu", pBuffer, nFilledLen,
                nTimeStamp);

    if(nPTS > INVALID_PTS_VALUE)
    {
        //We will anyway overflow. So send no PTS
        nPTS = INVALID_PTS_VALUE;
    }

    memset (PesHeader, 0, PES_MAX_HEADER_SIZE);
    PesHeaderLength = InsertPesHeader (PesHeader, packet_length,
                                       MPEG_AUDIO_PES_START_CODE,
                                       nPTS, 0);

    err  = this->auddec().dvb().write(PesHeader, PesHeaderLength);
    if(err)
    {
       OMX_DBGT_ERROR("%s::MP3EmitFrame->dvb().write failed", name());
       goto error;
    }

    if (nFilledLen > 0)
        err = this->auddec().dvb().write(pBuffer, nFilledLen);
    if(err)
    {
       OMX_DBGT_ERROR("%s::MP3EmitFrame->dvb().write failed", name());
    }

error:
    OMX_DBGT_EPILOG();
    return err;
}

} // eof namespace stm
