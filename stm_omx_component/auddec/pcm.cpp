/*
 * Copyright 2011 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *
 * File   ::    pcm.cpp
 * Author ::    Olivier Douvenot (olivier.douvenot@st.com)
 *
 *
 */

#include "AudioPesAdapter.hpp"
#include "AudioDecoder.hpp"
#include "OMX_debug.h"

#include "pes.hpp"

#define DBGT_PREFIX "PCM "
#define DBGT_LAYER  1
#include <linux/dbgt.h>

#define PTS_CORRECTION(x) (((x) * 90000)/1000000)

namespace stm {

PcmPesAdapter::PcmPesAdapter(const ADECComponent& dec)
    :AudioPesAdapter(dec)
{
}

PcmPesAdapter::~PcmPesAdapter()
{
}

int PcmPesAdapter::emitCodecConfig(uint8_t* pBuffer,
                                   unsigned int nFilledLen) const
{
    uint32_t HeaderLength, metaDataLength;
    int      err = 0;

    OMX_DBGT_PROLOG("pBuf=%p, len=%lu", pBuffer, (unsigned long)nFilledLen);

    memset (PesHeader, 0, PES_MAX_HEADER_SIZE);
    metaDataLength = InsertWavHeader(&PesHeader[PES_MIN_HEADER_SIZE],
                                     this->auddec().inputPort().pcmParams().nChannels,
                                     this->auddec().inputPort().pcmParams().nBitPerSample, 
                                     this->auddec().inputPort().pcmParams().nSamplingRate,
                                     (this->auddec().inputPort().pcmParams().eEndian == OMX_EndianBig)?1:0);

    HeaderLength    = InsertPesHeader (PesHeader, metaDataLength, PRIVATE_STREAM_1_PES_START_CODE, INVALID_PTS_VALUE, 0);

    err  = this->auddec().dvb().write(PesHeader, HeaderLength+metaDataLength);
    if(err)
    {
       OMX_DBGT_ERROR("%s::PCMemitCodecConfig->dvb().write failed", name());
    }
    OMX_DBGT_EPILOG();
    return err;
}

int PcmPesAdapter::emitFrame(uint8_t* pBuffer,
                             unsigned int nFilledLen,
                             uint64_t nTimeStamp) const
{
    int err = 0;
    uint8_t* pOutBuffer = (uint8_t*)this->auddec().outputPort().buffer;
    OMX_U32 PesHeaderLength;
    uint64_t nPTS = PTS_CORRECTION(nTimeStamp);

    OMX_DBGT_PROLOG("pBuf=%p, len=%d, nTimeStamp=%llu", pBuffer, nFilledLen,
                nTimeStamp);

    if(nPTS > INVALID_PTS_VALUE)
    {
        //We will anyway overflow. So send no PTS
        nPTS = INVALID_PTS_VALUE;
    }

    memset (PesHeader, 0, PES_MAX_HEADER_SIZE);
    PesHeaderLength = InsertPesHeader ((unsigned char*)&pOutBuffer[0], nFilledLen, PRIVATE_STREAM_1_PES_START_CODE,
                                       nPTS, 0);

    memcpy((unsigned char*)&pOutBuffer[PesHeaderLength], pBuffer, nFilledLen);

    err = this->auddec().dvb().write((unsigned char*)&pOutBuffer[0], PesHeaderLength+nFilledLen);
    if(err)
    {
       OMX_DBGT_ERROR("%s::PCMEmitFrame->dvb().write pBuffer failed", name());
       goto error;
    }

error:
    OMX_DBGT_EPILOG();
    return err;
}

} // eof namespace stm
