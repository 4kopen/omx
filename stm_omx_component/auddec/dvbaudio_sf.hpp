/*
 * Copyright 2013 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file   dvbaudio_sf.hpp
 * @author STMicroelectronics
 */


#ifndef ST_OMX_DVBAudio_SF_H
#define ST_OMX_DVBAudio_SF_H
#include <stdio.h>
#include <string.h>
#include "IDvbDevice.hpp"
#include "Mutex.hpp"

#ifdef DVB_DEVICE_GRAB
#undef DVB_DEVICE_GRAB
#endif
#ifdef ANDROID
#define DVB_DEVICE_GRAB                 "/dev/dvb0.audio%d"
#else
#define DVB_DEVICE_GRAB                 "/dev/dvb/adapter0/audio%d"
#endif
#define DEVICE_NAME_LENGTH            sizeof(DVB_DEVICE_GRAB)+5

typedef  enum  {
    DVBAudio_SF_CodingAAC = 0,
    DVBAudio_SF_CodingOTHER = 1
} DVBAudio_SF_CODINGTYPE;


class DVBAudio_SF: public stm::IDvbDevice
{
public:
    int grabIdx;

    DVBAudio_SF(const char* strName,
                const char* strName1,
                int nEncoding,
                unsigned int nSampleRate,
                unsigned int nChannels,
                unsigned int nBitPerSample,
                const char *name);

    virtual ~DVBAudio_SF();

public:
    virtual int id() const;
    virtual const char* name() const;
    virtual const char* v4lname() const;


    virtual int create();
    virtual int destroy();
    virtual bool isOpen() const;

    virtual int start() const;
    virtual int stop() const;
    virtual int pause(bool hw) const;
    virtual int resume(bool hw) const;
    virtual void lock() const;
    virtual void unlock() const;
    virtual void resetflags() const;

    virtual uint64_t getTime() const;
    virtual uint32_t getSamplingRate() const{ return m_nSampleRate; };
    virtual int write(const void* data_ptr, unsigned int len) const;
    virtual int flush(bool bEoS = false) const;
    virtual int clear() const;

    virtual stm::IDvbDevice::EState state() const;

    virtual int UseBuffer(unsigned long virtualAddr, unsigned long bufferSize,
                  int width, int height, int color,
                  unsigned long userId) const;
    virtual unsigned long FlushBuffer() const;

    virtual int FillThisBuffer(unsigned long userId) const;
    virtual int FillBufferDone(unsigned long & userId, uint64_t & TS, bool & eos) const;
    int Instantiate();
    int v4lOpen(int grabIdx);
    int v4lConfigure(uint32_t sampleRate, uint32_t channelCount, uint32_t bitsPerSample);
    virtual int QueueBuffer (void* BufferData, int &nBytesSize, uint64_t &ts) const;
    int SetEncoding(int encoding);

private:
    void state(stm::IDvbDevice::EState st) const;
    void setSamplingRate(uint32_t samplingRate){ m_nSampleRate = samplingRate; };
protected:
    bool mIsLoggingEnabled;

private:
    int adev_id;
    int m_nEncoding;
    mutable unsigned int m_nSampleRate;
    unsigned int m_nChannels;
    unsigned int m_nBitPerSample;
    char m_deviceName[DEVICE_NAME_LENGTH];
    char m_deviceNamev4l[DEVICE_NAME_LENGTH];
    int v4l_fd;
    int eos;   // Workaround seek backward after end of playback
    /** Internal state to know if device started or not */
    mutable stm::IDvbDevice::EState m_nState;
    /** Serialize access to dvb commands */
    mutable stm::Mutex      m_mutexCmd;
    /** Condition variable signaled when state has changed */
    mutable stm::Condition  m_condState;

    mutable bool             m_bClearWrite;
    mutable bool             m_bFlushWrite;
};

#endif
