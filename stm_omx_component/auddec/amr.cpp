/*
 * Copyright 2011 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *
 * File   ::    amr.cpp
 * Author ::    Kausik MAITI (kausik.maiti@st.com)
 *
 *
 */
#include "AudioPesAdapter.hpp"
#include "AudioDecoder.hpp"
#include "OMX_debug.h"
#include <stdio.h>

#include "pes.hpp"

#define DBGT_PREFIX "AMR "
#define DBGT_LAYER  1
#include <linux/dbgt.h>

//#define AMR_DEBUG
//#define AMR_LOGBUFFER

#define PTS_CORRECTION(x) (((x) * 90000)/1000000)

static const uint32_t FrameSizeNB[16] = {
        13, 14, 16, 18, 20, 21, 27, 32,
        6, 1, 1, 1, 1, 1, 1, 1
    };

namespace stm {

AmrPesAdapter::AmrPesAdapter(const ADECComponent& dec)
    :AudioPesAdapter(dec)
{}

AmrPesAdapter::~AmrPesAdapter()
{}

int AmrPesAdapter::emitCodecConfig(uint8_t* pBuffer,
                                   unsigned int nFilledLen) const
{
    uint32_t err = 0;
    OMX_DBGT_PROLOG("pBuf=0x%p, len=%lu", pBuffer, (unsigned long)nFilledLen);
    if(this->auddec().outputPort().isTunneled())
    {
        uint32_t metaDataLength, headerLength;
        memset (PesHeader, 0, PES_MAX_HEADER_SIZE);
        metaDataLength = InsertWavHeader(&PesHeader[PES_MIN_HEADER_SIZE],
                        1, 16, 8000, 0x00); // 1 channel, 16 bit, 8KHz, Little Endian
        OMX_DBGT_PROLOG("metadatLength =%d", metaDataLength);
        headerLength = InsertPesHeader (PesHeader, metaDataLength, PRIVATE_STREAM_1_PES_START_CODE, INVALID_PTS_VALUE, 0);

        err = this->auddec().dvb().write(PesHeader, (headerLength + metaDataLength));
        if(err)
        {
           OMX_DBGT_ERROR("%s::AMREmitFrame->dvb().write failed", 
                      this->name());
        }
#ifdef AMR_DEBUG
    {
        uint32_t i = 0;
        char* LogPtr = (char *)malloc ((headerLength + metaDataLength) * 3 + 1);

        OMX_DBGT_PINFO ("%s AMR data header length %d (ADECComponent::AMREmitConfig)",
                    this->name(), headerLength);
        for(i = 0; i < (headerLength + metaDataLength); i++)
        {
            sprintf (LogPtr+3*i, "%.2x ", PesHeader[i]);
        }
        OMX_DBGT_PINFO ("%s %s (ADECComponent::AMREmitConfig)",
                    this->name(), LogPtr);
        free (LogPtr);
    }
#endif
    }
    OMX_DBGT_EPILOG();
    return err;
}


int AmrPesAdapter::emitFrame(uint8_t* pBuffer,
                             unsigned int nFilledLen, uint64_t nTimeStamp) const
{
    uint32_t err = 0, offset = 0, decodedSize = 0;
    uint32_t pesHeaderLength = 0;
    uint32_t decFrameSize = this->auddec().outputPort().NbSamples * 2;
    STREAM_STRUCT_T *streamParams = &this->auddec().swDecInterface.stream_struct;
    SAMPLE_STRUCT_T *sampleParam = &this->auddec().swDecInterface.sample_struct;
    uint8_t * decodedBuffer = (uint8_t *)this->auddec().outputPort().buffer;
    RETURN_STATUS_LEVEL_T returnStatus;

    // temp buffer that is 32 bits alligned. Can be removed later if the decoder supports bitstream
    uint8_t tempBuffer[((nFilledLen + 3) >> 2)<<2];
    uint64_t nPTS = PTS_CORRECTION(nTimeStamp);

    OMX_DBGT_PROLOG("pBuf=0x%p, len=%d, nTimeStamp=%llu", pBuffer, nFilledLen, nTimeStamp);
#ifdef AMR_LOGBUFFER
    {
        FILE *fd;
        fd = fopen("/data/amr_input.amr","ab+");
        if (!fd) {
            OMX_DBGT_ERROR(": Unable to open %s file.Data Dump discarded","amr_input.amr");
        }
        else{
            fwrite(pBuffer,1, nFilledLen,fd);
            fclose(fd);
        }
    }
#endif

    // We may receive multiple frames
    // decode them in loop and return the total in one go.
    do {
        // Decode the first octet of the current frame
        uint8_t * pSrcBuffer = &pBuffer[offset];
        uint32_t mode = pBuffer[offset] >> 3 & 0x0F;

        OMX_DBGT_PINFO("emitFrame: Mode = %d, size = %d", (uint32_t)mode, (uint32_t)FrameSizeNB[mode]);

        // Loop to make the input 32 bit alligned
        {
            int i, loopcount = nFilledLen >> 2;

            for(i = 0; i< loopcount ; i++)
            {
                tempBuffer[i*4]   = pSrcBuffer[i*4+3];
                tempBuffer[i*4+1] = pSrcBuffer[i*4+2];
                tempBuffer[i*4+2] = pSrcBuffer[i*4+1];
                tempBuffer[i*4+3] = pSrcBuffer[i*4];
            }
            // we may have a few bytes left so we reorder mannually
            loopcount = nFilledLen - (loopcount << 2);
            // we have few extra bytes
            while(loopcount)
            {
                tempBuffer[(i+1)*4 - loopcount]   = pSrcBuffer[i*4 + loopcount - 1];
                loopcount--;
            }
        }

        if(nPTS > INVALID_PTS_VALUE)
        {
            //We will anyway overflow. So send no PTS
            nPTS = INVALID_PTS_VALUE;
        }

        // if we are in tunneled mode we inject the data into dvb using PES format
        if(this->auddec().outputPort().isTunneled())
        {
            memset (PesHeader, 0, PES_MAX_HEADER_SIZE);
            pesHeaderLength = InsertPesHeader (PesHeader, (decodedSize + decFrameSize),
                                             PRIVATE_STREAM_1_PES_START_CODE, nPTS, 0);
            OMX_DBGT_PINFO("nDecodedSamples = %d, headerLength = %d", this->auddec().outputPort().NbSamples, pesHeaderLength);
            memcpy((void *)this->auddec().outputPort().buffer, PesHeader, pesHeaderLength);

            if(decodedSize == 0)
            {
                decodedBuffer += pesHeaderLength;
            }
        }

        //Populate the Input buffer params
        streamParams->real_size_frame_in_bit = (FrameSizeNB[mode] << 3); // size in bits
        streamParams->bits_struct.buf_add    = (uint32_t *)tempBuffer; // start address of compressed buffer
        streamParams->bits_struct.buf_end    = (uint32_t *)(tempBuffer + FrameSizeNB[mode]); // end address of compressed buffer
        streamParams->bits_struct.pos_ptr    = (uint32_t *)tempBuffer ; // start address of compressed buffer again
        streamParams->bits_struct.buf_size   = (FrameSizeNB[mode] + 3) >> 2; // size in word32
        streamParams->bits_struct.nbit_available   = 32; // size in bits
        streamParams->bits_struct.word_size        = 32;

        sampleParam->buf_add     = (int32_t *)decodedBuffer;

        amr_decode_setBuffer(&this->auddec().swDecInterface, decodedBuffer);

        // Post for decode now
        returnStatus = amr_decode_frame(&this->auddec().swDecInterface);
        if(returnStatus != RETURN_STATUS_OK)
        {
            OMX_DBGT_ERROR("amr_decode_frame Failed : %d",returnStatus);
        }

#ifdef AMR_LOGBUFFER
        {
            FILE *fd;
            fd = fopen("/data/amr_output.pcm","ab+");
            if (!fd) {
                OMX_DBGT_ERROR(": Unable to open %s file.Data Dump discarded","amr_output.pcm");
            }
            else{
                fwrite(decodedBuffer,1, decFrameSize,fd);
                fclose(fd);
            }
        }
#endif
        decodedBuffer += decFrameSize; // Increment the start add of decoded buffer
        decodedSize += decFrameSize;   // Update the decoded size
        offset += FrameSizeNB[mode];   // Increment the src buffer offset
    }while((offset < nFilledLen) && ((decodedSize + pesHeaderLength) < sizeof(this->auddec().outputPort().buffer)));

    // if we are in tunneled mode we inject the data into dvb
    if(this->auddec().outputPort().isTunneled())
    {

#ifdef AMR_DEBUG
        {
            uint32_t i = 0;
            char* LogPtr = (char *)malloc (pesHeaderLength * 3 + 1);

            OMX_DBGT_PINFO ("%s AMR data header length %d (ADECComponent::AMREmitFrame)",
                        this->name(), pesHeaderLength);
            for(i = 0; i < pesHeaderLength; i++)
            {
                sprintf (LogPtr+3*i, "%.2x ", PesHeader[i]);
            }
            OMX_DBGT_PINFO ("%s %s (ADECComponent::AMREmitFrame)",
                        this->name(), LogPtr);
            free (LogPtr);
        }
#endif
        err |= this->auddec().dvb().write(this->auddec().outputPort().buffer, (pesHeaderLength + decodedSize));
        if(err)
        {
           OMX_DBGT_ERROR("%s::AMREmitFrame->dvb().write failed", this->name());
        }
    }

    OMX_DBGT_EPILOG();
    return err;
}

} // eof namespace stm

