/*
 * Copyright 2013 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file   AudioDecoderPort.cpp
 * @author STMicroelectronics
 */

#include "AudioDecoderPort.hpp"
#include "AudioDecoder.hpp"
#include "AudioPesAdapter.hpp"

#include "OMX_Audio.h"
#include "OMX_AudioExt.h"
#include "OMX_CoreExt.h"
#include "OMX_debug.h"
#include "pes.hpp"

#include <linux/dvb/stm_dvb.h>
#include <linux/dvb/stm_audio.h>

#if HAVE_ASSERT_H
#include <assert.h>
#endif

#if HAVE_DBGTASSERT_H
// Re-direct assert on DBGT_ASSERT macro
#define assert(expr)  OMX_DBGT_ASSERT(expr)
#endif

#define DBGT_PREFIX "ADECP"
#define DBGT_LAYER 0
#include <linux/dbgt.h>

#define PTS_CORRECTION(x) (((x) * 90)/1000)
#define NUM_IN_BUFFERS 2
#define NUM_OUT_BUFFERS 3

namespace stm {

//==========================================================================
// AudDecPort implementation
//==========================================================================

AudDecPort::AudDecPort(unsigned int nIdx, OMX_DIRTYPE aDir,
                       const ADECComponent& owner)
    :OmxPort(),
     m_owner(owner),
     m_PortDefinition(nIdx, aDir),
     m_waitSem(NULL),
     m_triggerSem(NULL)
{
    //check the string length before copy
    assert(strlen(this->auddec().name()) <= sizeof(m_strName));
    strcpy(m_strName,this->auddec().name());
    OMXDEBUG_UPDATE_AV_TRACE_SETTINGS(this->auddec().name());
}

AudDecPort::~AudDecPort()
{}

AudDecInputPort::EThreadState AudDecInputPort::threadState() const
{ return m_nThreadState; }

void AudDecInputPort::threadState(EThreadState st)
{
    m_mutexThread.lock();
    m_nThreadState = st;
    m_mutexThread.release();
    m_condState.signal();
}

const AudioPortDefinition& AudDecPort::definition() const
{ return m_PortDefinition; }

AudioPortDefinition& AudDecPort::definition()
{ return m_PortDefinition; }

const OmxComponent& AudDecPort::component() const
{ return m_owner; }


//==========================================================================
// AudDecOutputPort implementation
//==========================================================================

AudDecOutputPort::AudDecOutputPort(unsigned int nIdx, OMX_DIRTYPE aDir,
                                   const ADECComponent& owner)
    :AudDecPort(nIdx, aDir, owner),
     puller_thread_id(0),
     m_nReceivedFrame(0)
{
    this->definition().bufferCountMin(NUM_OUT_BUFFERS);
    this->definition().bufferCountActual(NUM_OUT_BUFFERS);
    this->codingFormat(OMX_AUDIO_CodingPCM);

    static const unsigned int kDefaultFreqHz = 44100;
    OMX_CONF_INIT_STRUCT_PTR(&m_pcmParams, OMX_AUDIO_PARAM_PCMMODETYPE );
    m_pcmParams.eNumData      = OMX_NumericalDataSigned;
    m_pcmParams.nBitPerSample = 16;
    m_pcmParams.nChannels     = 2;
    m_pcmParams.ePCMMode      = OMX_AUDIO_PCMModeLinear;
    m_pcmParams.nSamplingRate = kDefaultFreqHz;
    m_pcmParams.bInterleaved  = OMX_TRUE;
    m_pcmParams.nPortIndex    = OMX_ST_AUDIO_OUTPUT_PORT;
}

AudDecOutputPort::~AudDecOutputPort()
{}

OMX_ERRORTYPE AudDecOutputPort::create()
{
    OmxPort::create();
    return OMX_ErrorNone;
}

void AudDecOutputPort::ConfigureSWDecoder()
{
    if(this->auddec().SWDecoderEnabled)
    {
        SAMPLE_STRUCT_T *sampleParam = &this->auddec().swDecInterface.sample_struct;

        // Configure the Output PCM Params
        sampleParam->sample_freq = m_pcmParams.nSamplingRate;
        sampleParam->sample_size = m_pcmParams.nBitPerSample;
        sampleParam->chans_nb    = m_pcmParams.nChannels;

        sampleParam->block_len   = NbSamples;
        sampleParam->buf_add     = (int32_t *)buffer;

        amr_decode_setBuffer(&this->auddec().swDecInterface, buffer);
    }
}

int AudDecOutputPort::start()
{
    // Nothing to do on output port if tunneled
    if (this->isTunneled()) {
        puller_thread_id = 0;
        return 0;
    } else {
        return pthread_create(&puller_thread_id, 0,
                              AudDecOutputPort::ThreadEntryPoint, this);
    }
}

int AudDecOutputPort::stop()
{
    // Nothing to do on output port if tunneled
    if (this->isTunneled()) {
        puller_thread_id = 0;
        return 0;
    } else {
        return 0;
    }
}

void AudDecOutputPort::destroy()
{
    OMX_DBGT_PROLOG();
    if (puller_thread_id != 0) {
        this->push(EOSBUF);
        if(this->auddec().SWDecoderEnabled)
        {
            sem_post(m_triggerSem);
        }
        pthread_join(puller_thread_id, 0);
        puller_thread_id = 0;
    }
    if (this->nbBuffers() > 0) {
        OMX_DBGT_ERROR("Allocated buffer on output port not freed");
    }

    m_nReceivedFrame = 0;

    OMX_DBGT_EPILOG();
}

void* AudDecOutputPort::ThreadEntryPoint(void* a_pThis)
{
    AudDecOutputPort* pThis = static_cast<AudDecOutputPort*>(a_pThis);
    pThis->threadFunction();
    return 0;
}

OMX_AUDIO_CODINGTYPE
AudDecOutputPort::supportedCodingFormat(unsigned int nIdx) const
{
    assert(nIdx < this->supportedCodingFormatNb());
    return OMX_AUDIO_CodingPCM;
}

unsigned int
AudDecOutputPort::supportedCodingFormatNb() const
{ return 1; }

bool AudDecOutputPort::isSupportedCodingFormat(OMX_AUDIO_CODINGTYPE aFmt) const
{
    if (aFmt == OMX_AUDIO_CodingPCM)
        return true;
    else
        return false;
}

void AudDecOutputPort::codingFormat(OMX_AUDIO_CODINGTYPE aFmt)
{
    assert(isSupportedCodingFormat(aFmt));
    this->definition().compressionFormat(aFmt);
}

void AudDecOutputPort::updateBufferSize(OMX_AUDIO_CODINGTYPE aFmt)
{
    static const unsigned int kAAC_SAMPLE_NUMBER = 2048;
    static const unsigned int kMP3_SAMPLE_NUMBER = 1152;
    static const unsigned int kVORBIS_SAMPLE_NUMBER = 4096;
    static const unsigned int kWMA_SAMPLE_NUMBER = 4096;
    static const unsigned int kAC3_SAMPLE_NUMBER = 4096;
    static const unsigned int kDTS_SAMPLE_NUMBER = 4096;
    static const unsigned int kAMR_SAMPLE_NUMBER = 160;
    static const unsigned int kADPCM_SAMPLE_NUMBER = 4096;// TBVerified

    unsigned int nSampleNumber = 0;
    switch((int)aFmt)
    {
    case OMX_AUDIO_CodingAAC:
        nSampleNumber = kAAC_SAMPLE_NUMBER; break;
    case OMX_AUDIO_CodingMPEG:
    case OMX_AUDIO_CodingMP3:
        nSampleNumber = kMP3_SAMPLE_NUMBER; break;
    case OMX_AUDIO_CodingVORBIS:
        nSampleNumber = kVORBIS_SAMPLE_NUMBER; break;
    case OMX_AUDIO_CodingWMA:
        nSampleNumber = kWMA_SAMPLE_NUMBER; break;
    case OMX_AUDIO_CodingAC3:
        nSampleNumber = kAC3_SAMPLE_NUMBER; break;
    case OMX_AUDIO_CodingDTS:
        nSampleNumber = kDTS_SAMPLE_NUMBER; break;
    case OMX_AUDIO_CodingAMR:
        nSampleNumber = kAMR_SAMPLE_NUMBER; break;
    case OMX_AUDIO_CodingADPCM:
        nSampleNumber = kADPCM_SAMPLE_NUMBER; break;
    default:
        nSampleNumber = 0; break;
    }

    NbSamples = nSampleNumber;

    unsigned int nNewSz = (nSampleNumber* (m_pcmParams.nChannels *
                                           m_pcmParams.nBitPerSample) / 8);
    this->definition().bufferSize(nNewSz);
}

OMX_ERRORTYPE
AudDecOutputPort::getParameter(OMX_INDEXTYPE nParamIndex, OMX_PTR pParamStruct)
{
    OMX_DBGT_PROLOG("on Audio decoder output port");
    switch ((int)nParamIndex)
    {
    case OMX_IndexParamPortDefinition: {
        // Get port definition
        ASSERT_STRUCT_TYPE(pParamStruct, OMX_PARAM_PORTDEFINITIONTYPE);
        OMX_PARAM_PORTDEFINITIONTYPE* param;
        param = (OMX_PARAM_PORTDEFINITIONTYPE*)pParamStruct;
        assert(param->nPortIndex == this->definition().index());
        OMX_DBGT_PDEBUG("::OMX_IndexParamPortDefinition:");
        *param = this->definition().getParamPortDef();
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }break;

    case OMX_IndexParamAudioPortFormat: {
        // Supported audio port format
        ASSERT_STRUCT_TYPE(pParamStruct, OMX_AUDIO_PARAM_PORTFORMATTYPE);
        OMX_AUDIO_PARAM_PORTFORMATTYPE* param;
        param = (OMX_AUDIO_PARAM_PORTFORMATTYPE*)pParamStruct;
        assert(param->nPortIndex == this->definition().index());
        OMX_DBGT_PDEBUG("::OMX_IndexParamAudioPortFormat:");
        if (param->nIndex < this->supportedCodingFormatNb()) {
            param->eEncoding = this->supportedCodingFormat(param->nIndex);
            OMX_DBGT_EPILOG();
            return OMX_ErrorNone;
        } else {
            OMX_DBGT_EPILOG("No more supported format");
            return OMX_ErrorNoMore;
        }
    }break;

    case OMX_IndexParamAudioAac:
    case OMX_IndexParamAudioMp3:
    case OMX_IndexParamAudioVorbis:
    case OMX_IndexParamAudioWma:
    case OMX_IndexParamAudioAc3:
    case OMX_IndexParamAudioDts:
    case OMX_IndexParamAudioAmr:
    case OMX_IndexParamAudioAdpcm: {
        OMX_DBGT_ERROR("Bad port index");
        OMX_DBGT_EPILOG();
        return OMX_ErrorBadPortIndex;
    }; break;

   case OMX_IndexParamAudioPcm: {
        ASSERT_STRUCT_TYPE(pParamStruct, OMX_AUDIO_PARAM_PCMMODETYPE);
        OMX_AUDIO_PARAM_PCMMODETYPE* param;
        param = (OMX_AUDIO_PARAM_PCMMODETYPE*)pParamStruct;
        assert(param->nPortIndex == this->definition().index());
        OMX_DBGT_PDEBUG("::OMX_IndexParamAudioPcm:");
        *param = m_pcmParams;
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }; break;

    case OMX_IndexParamOtherPortFormat:
    case OMX_IndexParamVideoPortFormat:
    case OMX_IndexParamImagePortFormat:
    default: {
        OMX_DBGT_ERROR("Unsupported index");
        OMX_DBGT_EPILOG("Unsupported index");
        return OMX_ErrorUnsupportedIndex;
    }; break;
    }
}

OMX_ERRORTYPE
AudDecOutputPort::setParameter(OMX_INDEXTYPE nParamIndex, OMX_PTR pParamStruct)
{
    OMX_DBGT_PROLOG("on Audio decoder ouptut port");
    switch ((int)nParamIndex)
    {
    case OMX_IndexParamAudioAac:
    case OMX_IndexParamAudioMp3:
    case OMX_IndexParamAudioVorbis:
    case OMX_IndexParamAudioWma:
    case OMX_IndexParamAudioAc3:
    case OMX_IndexParamAudioDts:
    case OMX_IndexParamAudioAmr: {
       OMX_DBGT_ERROR("Bad port index");
       OMX_DBGT_EPILOG();
       return OMX_ErrorBadPortIndex;
    }; break;

    case OMX_IndexParamAudioPortFormat: {
        // Set audio port format
        ASSERT_STRUCT_TYPE(pParamStruct, OMX_AUDIO_PARAM_PORTFORMATTYPE);
        OMX_AUDIO_PARAM_PORTFORMATTYPE* param;
        param = (OMX_AUDIO_PARAM_PORTFORMATTYPE*)pParamStruct;
        assert(param->nPortIndex == this->definition().index());

        if (!isSupportedCodingFormat(param->eEncoding)) {
            OMX_DBGT_ERROR("::OMX_IndexParamAudioPortFormat:"
                           " eEncoding not supported (%d)",
                           param->eEncoding);
            OMX_DBGT_EPILOG("Unsupported audio coding format");
            return OMX_ErrorUnsupportedSetting;
        }

        OMX_DBGT_PDEBUG("::OMX_IndexParamAudioPortFormat:"
                       " audio format changed from %s (eEncoding=%d(0x%x)) to (eEncoding=%u(0x%x))",
                   this->definition().contentType(),
                   this->definition().compressionFormat(),
                   this->definition().compressionFormat(),
                   param->eEncoding,
                   param->eEncoding);
        // Apply format on port
        this->codingFormat(param->eEncoding);

        OMX_DBGT_EPILOG("Set port audio format %s", this->definition().contentType());
        return OMX_ErrorNone;
    }; break;

    case OMX_IndexParamPortDefinition: {
        // Port definition
        ASSERT_STRUCT_TYPE(pParamStruct, OMX_PARAM_PORTDEFINITIONTYPE);
        OMX_PARAM_PORTDEFINITIONTYPE* param;
        param = (OMX_PARAM_PORTDEFINITIONTYPE*)pParamStruct;
        assert(param->nPortIndex == this->definition().index());

        OMX_DBGT_PDEBUG("::OMX_IndexParamPortDefinition:"
                   " nBufferCountActual %u -> %u,"
                   " nBufferCountMin  %uKB -> %uKB,"
                   " nBufferSize %u -> %u ",
                   this->definition().bufferCountActual(), param->nBufferCountActual,
                   this->definition().bufferCountMin(), param->nBufferCountMin,
                   this->definition().bufferSize(), param->nBufferSize);

        if (this->definition().bufferSize() != param->nBufferSize) {
            OMX_DBGT_PINFO("::OMX_IndexParamPortDefinition:"
                       " changing nBufferSize from %uKB to %uKB ",
                       this->definition().bufferSize()/1024,
                       param->nBufferSize/1024);
            this->definition().bufferSize(param->nBufferSize);
        }

        this->definition().setParamPortDef(param);
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }; break;

    case OMX_IndexParamAudioPcm: {
        ASSERT_STRUCT_TYPE(pParamStruct, OMX_AUDIO_PARAM_PCMMODETYPE);
        OMX_AUDIO_PARAM_PCMMODETYPE* param;
        param = (OMX_AUDIO_PARAM_PCMMODETYPE*)pParamStruct;
        assert(param->nPortIndex == this->definition().index());
        if ((*param).nBitPerSample == 0) {
            // Take default value in this case
            OMX_DBGT_WARNING("::OMX_IndexParamAudioPcm:"
                             " nBitPerSample wrongly set to 0, "
                             "default it to %u for output nBufferSize computation",
                             m_pcmParams.nBitPerSample);
            (*param).nBitPerSample = m_pcmParams.nBitPerSample;
        }

        OMX_DBGT_PDEBUG("::OMX_IndexParamAudioPcm:"
                   " nChannels  %u -> %u,"
                   " nSamplingRate %u -> %u ",
                   m_pcmParams.nChannels, param->nChannels,
                   m_pcmParams.nSamplingRate, param->nSamplingRate);

       m_pcmParams = *param;
       /*TODO Its a workaround solution as there is no requirement for sending
         multi-channel decoded data to the output. The down-mixing is done in SE
       */
       if(m_pcmParams.nChannels > 2)
       {
          OMX_DBGT_PDEBUG("::OMX_IndexParamAudioPcm:"
                     " nChannels forced to 2 from %u",
                     m_pcmParams.nChannels);

          m_pcmParams.nChannels = 2;
       }
       OMX_DBGT_EPILOG();
       return OMX_ErrorNone;
    }; break;

    case OMX_IndexParamOtherPortFormat:
    case OMX_IndexParamVideoPortFormat:
    case OMX_IndexParamImagePortFormat:
    default: {
        OMX_DBGT_ERROR("Unsupported index");
        OMX_DBGT_EPILOG("Unsupported index");
        return OMX_ErrorUnsupportedIndex;
    }; break;
    }
}

OMX_ERRORTYPE
AudDecOutputPort::useBuffer(OMX_BUFFERHEADERTYPE** ppBufferHdr,
                            OMX_PTR pAppPrivate,
                            OMX_U32 nSizeBytes,
                            OMX_U8* pBuffer)
{
     return OmxPort::useBuffer(ppBufferHdr, pAppPrivate,
                              nSizeBytes, pBuffer);
}

OMX_ERRORTYPE
AudDecOutputPort::allocateBuffer(OMX_BUFFERHEADERTYPE** ppBufferHdr,
                                 OMX_PTR pAppPrivate,
                                 OMX_U32 nSizeBytes)
{
    OMX_DBGT_PROLOG("Allocating buffer on audio decoder ouput port");
    OMX_ERRORTYPE eError;

    // Invalid case in proprietary tunneling
    assert(!this->isTunneled());

    // Allocate a buffer header
    *ppBufferHdr = 0;
    OMX_BUFFERHEADERTYPE* pBufferHdr = allocateBufferHeader(pAppPrivate,
                                                            nSizeBytes);
    if (!pBufferHdr) {
        eError = OMX_ErrorInsufficientResources;
        OMX_DBGT_ERROR("Can not allocate buffer header");
        OMX_DBGT_EPILOG("Return error %s", OMX_TYPE_TO_STR(OMX_ERRORTYPE, eError));
        return eError;
    }

    OMX_U8* pBuffer = (OMX_U8*)malloc(nSizeBytes);
    if (!pBuffer) {
        eError = OMX_ErrorInsufficientResources;
        freeBufferHeader(pBufferHdr);
        OMX_DBGT_ERROR("Can not allocate buffer");
        OMX_DBGT_EPILOG("Return error %s", OMX_TYPE_TO_STR(OMX_ERRORTYPE, eError));
        return eError;
    }

    pBufferHdr->pBuffer = pBuffer;
    *ppBufferHdr = pBufferHdr;

    (*ppBufferHdr)->pPlatformPrivate = (void*)pAppPrivate;

    // Increment internal buffer counter.
    this->incrementNbBuffers();

    OMX_DBGT_EPILOG();
    return OMX_ErrorNone;
}

OMX_ERRORTYPE
AudDecOutputPort::freeBuffer(OMX_BUFFERHEADERTYPE* pBufferHdr)
{
    assert(!isTunneled());
    assert(pBufferHdr);
    return OmxPort::freeBuffer(pBufferHdr);
}

OMX_ERRORTYPE
AudDecOutputPort::emptyThisBuffer(OMX_BUFFERHEADERTYPE* pBufferHdr)
{
    assert(0 && "Invalid call on output port");
    return OMX_ErrorNone;
}

OMX_ERRORTYPE
AudDecOutputPort::fillThisBuffer(OMX_BUFFERHEADERTYPE* pBufferHdr)
{
    assert(this->isTunneled() == false);
    OMX_DBGT_PROLOG();
    this->push(pBufferHdr);
    OMX_DBGT_EPILOG();
    return OMX_ErrorNone;
}

OMX_ERRORTYPE
AudDecOutputPort::flush()
{
    OMX_DBGT_PROLOG("Flush audio decoder output port");
    if (isTunneled()) {
        // Proprietary tunneling. Nothing to flush
        DBGT_EPILOG("Port is tunneled.");
        return OMX_ErrorNone;
    }

    OMX_BUFFERHEADERTYPE *buf  = 0;
    this->getRawFifos()->lock();

    while(this->getRawFifos()->tryPop(buf) == true) {
        if (buf != EOSBUF) {
            this->auddec().notifyFillBufferDone(buf);
        }
    }
    this->getRawFifos()->unlock();
    buf = 0;
    buf = this->relockCurrent();
    if(buf != NULL)
    {
        this->auddec().notifyFillBufferDone(buf);
    }
    this->freeAndUnlockCurrent();

    OMX_DBGT_EPILOG();
    return OMX_ErrorNone;
}

void AudDecOutputPort::threadFunction()
{
    OMX_DBGT_PROLOG();

    OMX_DBGT_PINFO("%s::Puller thread running (ADECComponent::PullingRunner)", component().name());
    OMX_BUFFERHEADERTYPE *pOutBufHdr;
    int nBytesSize;
    uint64_t TS;
    uint32_t ret;
    while(true)
    {
        //  Wait for decoded frame from SE
        pOutBufHdr = this->popAndLockCurrent();
        assert(pOutBufHdr != 0);

        if(pOutBufHdr == this->EOSBUF)
        {
            // End Of Stream maker received
            OMX_DBGT_PDEBUG("%s::PullingRunner outputPort->EOSBUF",component().name());
            this->freeAndUnlockCurrent();
            goto EXIT;
        }

        // Unlock buffer in order to allow flush while we are blocked in kernel
        // This could lead to race condition on buffer, but...
        this->unlockCurrent();

        nBytesSize = sizeof(buffer);
        if(this->auddec().SWDecoderEnabled)
        {
            // wait for the decode
            sem_wait(m_waitSem);
            nBytesSize = this->auddec().nDecodedSamples * (m_pcmParams.nChannels * m_pcmParams.nBitPerSample) / 8;
            TS = this->auddec().nTS;
        }
        else
        {
            ret = this->auddec().dvb().QueueBuffer((void*)buffer, nBytesSize, TS);
            // We send an EOF and then return
        }
        OMX_DBGT_PDEBUG("%s::Queue buffer 0x%08x : %d", component().name(), (uint32_t)buffer, nBytesSize);

        uint32_t SamplingRate = this->auddec().dvb().getSamplingRate();
        if(m_pcmParams.nSamplingRate != SamplingRate)
        {
            OMX_DBGT_PDEBUG("%s::SamplingRate change from : %d to %d", component().name(), (uint32_t)m_pcmParams.nSamplingRate, SamplingRate);
            m_pcmParams.nSamplingRate = SamplingRate;
            this->auddec().notifyEventHandler(OMX_EventPortSettingsChanged, OMX_ST_AUDIO_OUTPUT_PORT, OMX_IndexParamPortDefinition, NULL);
        }

        // Relock it to finish sendback
        pOutBufHdr = this->relockCurrent();

        if(pOutBufHdr != NULL)
        {
            if((nBytesSize == 0) || (ret != 0))
            {
                // End Of Stream marker received
                pOutBufHdr->nFlags |= OMX_BUFFERFLAG_EOS;
                pOutBufHdr->nOffset    = 0;
                pOutBufHdr->nFilledLen = 0;
                pOutBufHdr->nTimeStamp = 0;
                this->auddec().notifyFillBufferDone(pOutBufHdr);
                this->auddec().dvb().flush(true);
		        this->auddec().notifyEventHandler(OMX_EventBufferFlag,OMX_ST_AUDIO_OUTPUT_PORT,OMX_BUFFERFLAG_EOS, NULL);

                if(this->auddec().SWDecoderEnabled)
                {
                    //Pusher triggered in EOF
                    sem_post(m_triggerSem);
                }
            }
            else
            {
                int nbSample = nBytesSize / sizeof(buffer[0]) / 8;

                // Do we have the potential correction
                struct timeMapping_s *node;
                struct timeMapping_s *overflowNode;
                this->auddec().m_mutexCtrl.lock();
                node = this->auddec().timeMapping_p;
                overflowNode = this->auddec().overflowTimeMapping_p;
                while(node)
                {
                    uint64_t timeDiff = (node->nTS > TS)?(node->nTS - TS): (TS - node->nTS);
                    if(timeDiff <= 100)
                    {
                        OMX_DBGT_PDEBUG("Potential timemapping: stored: %llu, received: %llu", node->nTS, TS);
                        TS = node->nTS;
                        this->auddec().timeMapping_p = deleteNode(this->auddec().timeMapping_p, node->nTS);
                        break;
                    }
                    node = node->next_p;
                }

                while(overflowNode)
                {
                    if(TS ==  overflowNode->nNewTS)
                    {
                        OMX_DBGT_PDEBUG("timemapping for overflow: stored: %llu, received: %llu", overflowNode->nTS, TS);
                        TS = overflowNode->nTS;
                        this->auddec().overflowTimeMapping_p = deleteNode(this->auddec().overflowTimeMapping_p, overflowNode->nTS);
                        break;
                    }
                    overflowNode = overflowNode->next_p;
                }

                this->auddec().m_mutexCtrl.release();

                pOutBufHdr->nTimeStamp = TS;
                pOutBufHdr->nOffset = 0;

                if(this->auddec().SWDecoderEnabled)
                {
                    pOutBufHdr->nFilledLen = nBytesSize;
                    this->auddec().Convert1x16To2x16Channels(this->auddec().nDecodedSamples,
                                                            (uint16_t*)buffer, (uint16_t*)pOutBufHdr->pBuffer);
                    //Pusher triggered
                    sem_post(m_triggerSem);
                }
                else
                {
                    this->auddec().Convert8x32To2x16Channels(nbSample, buffer, (uint16_t*)pOutBufHdr->pBuffer, m_pcmParams.nChannels);
                    pOutBufHdr->nFilledLen = nbSample *
                        (m_pcmParams.nChannels *
                         m_pcmParams.nBitPerSample) / 8;
                }

                m_nReceivedFrame++;
                DBGT_PDEBUG("%s::FillBufferDone 0x%08x (len=%u, pts=%llu, flags=%x) sendedFrame=%d receivedFrame + bufferinpipe=%d(+%d)",
                        component().name(),
                        (unsigned int)pOutBufHdr->pBuffer,
                        pOutBufHdr->nFilledLen,
                        pOutBufHdr->nTimeStamp,
                        pOutBufHdr->nFlags,
                        this->auddec().inputPort().sendedFrame(),
                        this->receivedFrame(),
                        this->getRawFifos()->size());

                this->auddec().notifyFillBufferDone(pOutBufHdr);
            }
        }
        else
        {
            OMX_DBGT_PDEBUG("%s::pOutBufHdr is NULL. Released by someone", component().name());
            if((nBytesSize == 0) || (ret != 0))
            {
                 OMX_DBGT_PDEBUG("%s::pOutBufHdr is NULL. EOS received", component().name());
                 this->auddec().dvb().flush(true);
                 this->auddec().notifyEventHandler(OMX_EventBufferFlag,OMX_ST_AUDIO_OUTPUT_PORT,OMX_BUFFERFLAG_EOS, NULL);
            }
        }

        this->freeAndUnlockCurrent();
        
        if(ret != 0)
        {
            OMX_DBGT_PDEBUG("%s::PullingRunner outputPort ret = %d",component().name(), ret);
            goto EXIT;
        }
    }

EXIT:
     flush();
     OMX_DBGT_EPILOG();
}


//==========================================================================
// AudDecInputPort implementation
//==========================================================================

const int  AudDecInputPort::m_codingFormats[] = {
    OMX_AUDIO_CodingAAC,
    OMX_AUDIO_CodingPCM,
    OMX_AUDIO_CodingMPEG,
    OMX_AUDIO_CodingMP3,
    OMX_AUDIO_CodingVORBIS,
    OMX_AUDIO_CodingWMA,
    OMX_AUDIO_CodingAC3,
    OMX_AUDIO_CodingDTS,
    OMX_AUDIO_CodingAMR,
    OMX_AUDIO_CodingADPCM
};

OMX_BUFFERHEADERTYPE* AudDecInputPort::EOSBUF =
    (OMX_BUFFERHEADERTYPE*)0xFFFFFFFF;

OMX_BUFFERHEADERTYPE* AudDecOutputPort::EOSBUF =
    (OMX_BUFFERHEADERTYPE*)0xFFFFFFFF;

AudDecInputPort::AudDecInputPort(unsigned int nIdx, OMX_DIRTYPE aDir,
                                 const ADECComponent& owner)
    :AudDecPort(nIdx, aDir, owner),
     pusher_thread_id(0),
     m_nThreadState(EThreadState_Stopped),
     m_mutexThread(),
     m_condState(),
     m_nSendedFrame(0)
{
    this->definition().bufferCountMin(NUM_IN_BUFFERS);
    this->definition().bufferCountActual(NUM_IN_BUFFERS);
    this->codingFormat(OMX_AUDIO_CodingVORBIS);
    // JCT: Temporary hard-coded value (because the OMX component returns
    // a too small value)
    this->definition().bufferSize(262144);

    OMX_CONF_INIT_STRUCT_PTR(&m_aacParams,    OMX_AUDIO_PARAM_AACPROFILETYPE );
    OMX_CONF_INIT_STRUCT_PTR(&m_pcmParams,    OMX_AUDIO_PARAM_PCMMODETYPE );
    OMX_CONF_INIT_STRUCT_PTR(&m_mp3Params,    OMX_AUDIO_PARAM_MP3TYPE );
    OMX_CONF_INIT_STRUCT_PTR(&m_mpaParams,    OMX_AUDIO_PARAM_MPATYPE );
    OMX_CONF_INIT_STRUCT_PTR(&m_vorbisParams, OMX_AUDIO_PARAM_VORBISTYPE );
    OMX_CONF_INIT_STRUCT_PTR(&m_wmaParams,    OMX_AUDIO_PARAM_WMATYPE );
    OMX_CONF_INIT_STRUCT_PTR(&m_ac3Params,    OMX_AUDIO_PARAM_AC3TYPE );
    OMX_CONF_INIT_STRUCT_PTR(&m_dtsParams,    OMX_AUDIO_PARAM_DTSTYPE );
    OMX_CONF_INIT_STRUCT_PTR(&m_amrParams,    OMX_AUDIO_PARAM_AMRTYPE );
    OMX_CONF_INIT_STRUCT_PTR(&m_adpcmParams,  OMX_AUDIO_PARAM_ADPCMTYPE );
    m_amrParams.eAMRFrameFormat = OMX_AUDIO_AMRFrameFormatFSF;
}

AudDecInputPort::~AudDecInputPort()
{}

OMX_ERRORTYPE AudDecInputPort::create()
{
    OmxPort::create();
    return OMX_ErrorNone;
}

void AudDecInputPort::ConfigureSWDecoder()
{
    if(this->definition().compressionFormat() == OMX_AUDIO_CodingAMR)
    {
        STATE_STRUCT_T * codecstate  = &this->auddec().swDecInterface.codec_state;

        //Configure the state structure
        codecstate->first_time    = true;  // set true for first time after that make it false
        codecstate->output_enable = true;  // always true
        codecstate->eof           = false; // set it to false and enable it for the last frame
    }
}

int AudDecInputPort::start()
{

    int ret = pthread_create(&pusher_thread_id, 0,
                          AudDecInputPort::ThreadEntryPoint, this);
    if (ret) {
        OMX_DBGT_ERROR("Can not create video decoder input port thread");
    } else {
        assert(ret == 0);
        this->threadState(EThreadState_Running);
    }
    return ret;
}

int AudDecInputPort::stop()
{
    return 0;
}

void AudDecInputPort::destroy()
{
    OMX_DBGT_PROLOG();
    if (pusher_thread_id != 0) {
        // Resume if was previously paused
        this->resume();
        this->push(EOSBUF);
        if(this->auddec().SWDecoderEnabled)
        {
            sem_post(m_triggerSem);
        }
        pthread_join(pusher_thread_id, 0);
        pusher_thread_id = 0;
    }
    if (this->nbBuffers() > 0) {
        OMX_DBGT_ERROR("Allocated buffer on input port not freed");
    }

    m_nSendedFrame = 0;

    OMX_DBGT_EPILOG();
}

void* AudDecInputPort::ThreadEntryPoint(void* a_pThis)
{
    AudDecInputPort* pThis = static_cast<AudDecInputPort*>(a_pThis);
    pThis->threadFunction();
    return 0;
}

OMX_AUDIO_CODINGTYPE
AudDecInputPort::supportedCodingFormat(unsigned int nIdx) const
{
    assert(nIdx < this->supportedCodingFormatNb());
    return (OMX_AUDIO_CODINGTYPE)m_codingFormats[nIdx];
}

unsigned int
AudDecInputPort::supportedCodingFormatNb() const
{ return (sizeof(m_codingFormats) / sizeof(m_codingFormats[0])); }

bool AudDecInputPort::isSupportedCodingFormat(OMX_AUDIO_CODINGTYPE aFmt) const
{
    bool ret = false;
    for (unsigned int i = 0; i < this->supportedCodingFormatNb(); i++) {
        if (this->supportedCodingFormat(i) == aFmt) {
            ret = true;
            break;
        }
    }
    return ret;
}

void AudDecInputPort::codingFormat(OMX_AUDIO_CODINGTYPE aFmt)
{
    assert(isSupportedCodingFormat(aFmt));
    this->definition().compressionFormat(aFmt);
}

unsigned int
AudDecInputPort::channelNb() const
{
    switch((int)this->definition().compressionFormat())
    {
    case OMX_AUDIO_CodingAAC:
        return m_aacParams.nChannels; break;
    case OMX_AUDIO_CodingPCM:
        return m_pcmParams.nChannels; break;
    case OMX_AUDIO_CodingMPEG:
    case OMX_AUDIO_CodingMP3:
        return m_mp3Params.nChannels; break;
    case OMX_AUDIO_CodingVORBIS:
        return m_vorbisParams.nChannels; break;
    case OMX_AUDIO_CodingWMA:
        return m_wmaParams.nChannels; break;
    case OMX_AUDIO_CodingAC3:
        return m_ac3Params.nChannels; break;
    case OMX_AUDIO_CodingDTS:
        return m_dtsParams.nChannels; break;
    case OMX_AUDIO_CodingAMR:
        return m_amrParams.nChannels; break;
    case OMX_AUDIO_CodingADPCM:
        return m_adpcmParams.nChannels; break;
    default:
        return 0;
    }
}

unsigned int
AudDecInputPort::sampleRate() const
{
    switch((int)this->definition().compressionFormat())
    {
    case OMX_AUDIO_CodingAAC:
        return m_aacParams.nSampleRate; break;
    case OMX_AUDIO_CodingPCM:
        return m_pcmParams.nSamplingRate; break;
    case OMX_AUDIO_CodingMPEG:
    case OMX_AUDIO_CodingMP3:
        return m_mp3Params.nSampleRate; break;
    case OMX_AUDIO_CodingVORBIS:
        return m_vorbisParams.nSampleRate; break;
    case OMX_AUDIO_CodingWMA:
        return m_wmaParams.nSamplingRate; break;
    case OMX_AUDIO_CodingAC3:
        return m_ac3Params.nSampleRate; break;
    case OMX_AUDIO_CodingDTS:
        return m_dtsParams.nSampleRate; break;
    case OMX_AUDIO_CodingAMR:
        return 8000; break; // Fixed at 8KHz
    case OMX_AUDIO_CodingADPCM:
        return m_adpcmParams.nSampleRate; break;
    default:
        return 0;
    }
}

OMX_ERRORTYPE
AudDecInputPort::getParameter(OMX_INDEXTYPE nParamIndex, OMX_PTR pParamStruct)
{
    OMX_DBGT_PROLOG("on Audio decoder input port");
    switch ((int)nParamIndex)
    {
    case OMX_IndexParamPortDefinition: {
        // Get port definition
        ASSERT_STRUCT_TYPE(pParamStruct, OMX_PARAM_PORTDEFINITIONTYPE);
        OMX_PARAM_PORTDEFINITIONTYPE* param;
        param = (OMX_PARAM_PORTDEFINITIONTYPE*)pParamStruct;
        assert(param->nPortIndex == this->definition().index());
        OMX_DBGT_PDEBUG("::OMX_IndexParamPortDefinition:");
        *param = this->definition().getParamPortDef();
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }break;

    case OMX_IndexParamAudioPortFormat: {
        // Supported audio port format
        ASSERT_STRUCT_TYPE(pParamStruct, OMX_AUDIO_PARAM_PORTFORMATTYPE);
        OMX_AUDIO_PARAM_PORTFORMATTYPE* param;
        param = (OMX_AUDIO_PARAM_PORTFORMATTYPE*)pParamStruct;
        assert(param->nPortIndex == this->definition().index());
        OMX_DBGT_PDEBUG("::OMX_IndexParamAudioPortFormat:");
        if (param->nIndex < this->supportedCodingFormatNb()) {
            param->eEncoding = this->supportedCodingFormat(param->nIndex);
            OMX_DBGT_EPILOG();
            return OMX_ErrorNone;
        } else {
            OMX_DBGT_EPILOG("No more supported format");
            return OMX_ErrorNoMore;
        }
    }break;

    case OMX_IndexParamAudioAac: {
        ASSERT_STRUCT_TYPE(pParamStruct, OMX_AUDIO_PARAM_AACPROFILETYPE);
        OMX_AUDIO_PARAM_AACPROFILETYPE* param;
        param = (OMX_AUDIO_PARAM_AACPROFILETYPE*)pParamStruct;
        assert(param->nPortIndex == this->definition().index());
        OMX_DBGT_PDEBUG("::OMX_IndexParamAudioAac:");
        *param = m_aacParams;
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }; break;

    case OMX_IndexParamAudioPcm: {
        ASSERT_STRUCT_TYPE(pParamStruct, OMX_AUDIO_PARAM_PCMMODETYPE);
        OMX_AUDIO_PARAM_PCMMODETYPE* param;
        param = (OMX_AUDIO_PARAM_PCMMODETYPE*)pParamStruct;
        assert(param->nPortIndex == this->definition().index());
        OMX_DBGT_PDEBUG("::OMX_IndexParamAudioPcm:");
        *param = m_pcmParams;
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }; break;

    case OMX_IndexParamAudioMpa: {
        ASSERT_STRUCT_TYPE(pParamStruct, OMX_AUDIO_PARAM_MPATYPE);
        OMX_AUDIO_PARAM_MPATYPE* param;
        param = (OMX_AUDIO_PARAM_MPATYPE*)pParamStruct;
        assert(param->nPortIndex == this->definition().index());
        OMX_DBGT_PDEBUG("::OMX_IndexParamAudioMpa:");
        *param = m_mpaParams;
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }; break;

    case OMX_IndexParamAudioMp3: {
        ASSERT_STRUCT_TYPE(pParamStruct, OMX_AUDIO_PARAM_MP3TYPE);
        OMX_AUDIO_PARAM_MP3TYPE* param;
        param = (OMX_AUDIO_PARAM_MP3TYPE*)pParamStruct;
        assert(param->nPortIndex == this->definition().index());
        OMX_DBGT_PDEBUG("::OMX_IndexParamAudioMp3:");
        *param = m_mp3Params;
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }; break;

    case OMX_IndexParamAudioVorbis: {
        ASSERT_STRUCT_TYPE(pParamStruct, OMX_AUDIO_PARAM_VORBISTYPE);
        OMX_AUDIO_PARAM_VORBISTYPE* param;
        param = (OMX_AUDIO_PARAM_VORBISTYPE*)pParamStruct;
        assert(param->nPortIndex == this->definition().index());
        OMX_DBGT_PDEBUG("::OMX_IndexParamAudioVorbis:");
        *param = m_vorbisParams;
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }; break;

    case OMX_IndexParamAudioWma: {
        ASSERT_STRUCT_TYPE(pParamStruct, OMX_AUDIO_PARAM_WMATYPE);
        OMX_AUDIO_PARAM_WMATYPE* param;
        param = (OMX_AUDIO_PARAM_WMATYPE*)pParamStruct;
        assert(param->nPortIndex == this->definition().index());
        OMX_DBGT_PDEBUG("::OMX_IndexParamAudioWma:");
        *param = m_wmaParams;
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }; break;

    case OMX_IndexParamAudioAc3: {
        ASSERT_STRUCT_TYPE(pParamStruct, OMX_AUDIO_PARAM_AC3TYPE);
        OMX_AUDIO_PARAM_AC3TYPE* param;
        param = (OMX_AUDIO_PARAM_AC3TYPE*)pParamStruct;
        assert(param->nPortIndex == this->definition().index());
        OMX_DBGT_PDEBUG("::OMX_IndexParamAudioAc3:");
        *param = m_ac3Params;
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }; break;

    case OMX_IndexParamAudioDts: {
        ASSERT_STRUCT_TYPE(pParamStruct, OMX_AUDIO_PARAM_DTSTYPE);
        OMX_AUDIO_PARAM_DTSTYPE* param;
        param = (OMX_AUDIO_PARAM_DTSTYPE*)pParamStruct;
        assert(param->nPortIndex == this->definition().index());
        OMX_DBGT_PDEBUG("::OMX_IndexParamAudioDts:");
        *param = m_dtsParams;
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }; break;

    case OMX_IndexParamAudioAmr: {
        ASSERT_STRUCT_TYPE(pParamStruct, OMX_AUDIO_PARAM_AMRTYPE);
        OMX_AUDIO_PARAM_AMRTYPE* param;
        param = (OMX_AUDIO_PARAM_AMRTYPE*)pParamStruct;
        assert(param->nPortIndex == this->definition().index());
        OMX_DBGT_PDEBUG("::OMX_IndexParamAudioAmr:");
        *param = m_amrParams;
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }; break;

    case OMX_IndexParamAudioAdpcm: {
        ASSERT_STRUCT_TYPE(pParamStruct, OMX_AUDIO_PARAM_ADPCMTYPE);
        OMX_AUDIO_PARAM_ADPCMTYPE* param;
        param = (OMX_AUDIO_PARAM_ADPCMTYPE*)pParamStruct;
        assert(param->nPortIndex == this->definition().index());
        OMX_DBGT_PDEBUG("::OMX_IndexParamAudioAdpcm:");
        *param = m_adpcmParams;
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }; break;

    case OMX_IndexParamOtherPortFormat:
    case OMX_IndexParamVideoPortFormat:
    case OMX_IndexParamImagePortFormat:
    default: {
        OMX_DBGT_ERROR("Unsupported index");
        OMX_DBGT_EPILOG("Unsupported index");
        return OMX_ErrorUnsupportedIndex;
    }; break;
    }
}

OMX_ERRORTYPE
AudDecInputPort::setParameter(OMX_INDEXTYPE nParamIndex, OMX_PTR pParamStruct)
{
    OMX_DBGT_PROLOG("on Audio decoder input port");
    switch ((int)nParamIndex)
    {
    case OMX_IndexParamAudioPortFormat: {
        // Set audio port format
        ASSERT_STRUCT_TYPE(pParamStruct, OMX_AUDIO_PARAM_PORTFORMATTYPE);
        OMX_AUDIO_PARAM_PORTFORMATTYPE* param;
        param = (OMX_AUDIO_PARAM_PORTFORMATTYPE*)pParamStruct;
        assert(param->nPortIndex == this->definition().index());

        if (!isSupportedCodingFormat(param->eEncoding)) {
            // Unsupported coding format
            OMX_DBGT_ERROR("%s::OMX_IndexParamAudioPortFormat:"
                           " eEncoding not supported (%d)",
                       component().name(), param->eEncoding);
            OMX_DBGT_EPILOG("Unsupported audio compression format");
            return OMX_ErrorUnsupportedSetting;
        }

        OMX_DBGT_PDEBUG("::OMX_IndexParamAudioPortFormat:)"
                   "coding format from %s (eEncoding=%d(0x%x)) to (eEncoding=%d(0x%x)",
                   this->definition().contentType(),
                   this->definition().compressionFormat(),
                   this->definition().compressionFormat(),
                   param->eEncoding, param->eEncoding);

        // Apply format on port
        this->codingFormat(param->eEncoding);

        OMX_DBGT_EPILOG("Set port coding format %s", this->definition().contentType());
        return OMX_ErrorNone;
    }; break;

    case OMX_IndexParamPortDefinition: {
        // Set port definition
        ASSERT_STRUCT_TYPE(pParamStruct, OMX_PARAM_PORTDEFINITIONTYPE);
        OMX_PARAM_PORTDEFINITIONTYPE* param;
        param = (OMX_PARAM_PORTDEFINITIONTYPE*)pParamStruct;
        assert(param->nPortIndex == this->definition().index());

        OMX_DBGT_PDEBUG("::OMX_IndexParamPortDefinition:"
                   " nBufferCountActual %u -> %u,"
                   " nBufferCountMin  %uKB -> %uKB,"
                   " nBufferSize %u -> %u ",
                   this->definition().bufferCountActual(), param->nBufferCountActual,
                   this->definition().bufferCountMin(), param->nBufferCountMin,
                   this->definition().bufferSize(), param->nBufferSize);

        if (this->definition().bufferSize() != param->nBufferSize) {
            OMX_DBGT_PINFO("::OMX_IndexParamPortDefinition:"
                           " changing nBufferSize from %uKB to %uKB",
                       this->definition().bufferSize()/1024,
                       param->nBufferSize/1024);
            this->definition().bufferSize(param->nBufferSize);
        }
        this->definition().setParamPortDef(param);
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }; break;

    case OMX_IndexParamAudioAac: {
        ASSERT_STRUCT_TYPE(pParamStruct, OMX_AUDIO_PARAM_AACPROFILETYPE);
        OMX_AUDIO_PARAM_AACPROFILETYPE* param;
        param = (OMX_AUDIO_PARAM_AACPROFILETYPE*)pParamStruct;
        assert(param->nPortIndex == this->definition().index());
        m_aacParams = *param;
        this->codingFormat(OMX_AUDIO_CodingAAC);
        OMX_DBGT_PDEBUG("::OMX_IndexParamAudioAac:");
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }; break;

   case OMX_IndexParamAudioMpa: {
       ASSERT_STRUCT_TYPE(pParamStruct, OMX_AUDIO_PARAM_MPATYPE);
       OMX_AUDIO_PARAM_MPATYPE* param;
       param = (OMX_AUDIO_PARAM_MPATYPE*)pParamStruct;
       assert(param->nPortIndex == this->definition().index());
       m_mpaParams = *param;
       this->codingFormat((OMX_AUDIO_CODINGTYPE)OMX_AUDIO_CodingMPEG);
       OMX_DBGT_PDEBUG("::OMX_IndexParamAudioMpa:");
       OMX_DBGT_EPILOG();
       return OMX_ErrorNone;
   }; break;

   case OMX_IndexParamAudioMp3: {
       ASSERT_STRUCT_TYPE(pParamStruct, OMX_AUDIO_PARAM_MP3TYPE);
       OMX_AUDIO_PARAM_MP3TYPE* param;
       param = (OMX_AUDIO_PARAM_MP3TYPE*)pParamStruct;
       assert(param->nPortIndex == this->definition().index());
       m_mp3Params = *param;
       this->codingFormat(OMX_AUDIO_CodingMP3);
       OMX_DBGT_PDEBUG("::OMX_IndexParamAudioMp3:");
       OMX_DBGT_EPILOG();
       return OMX_ErrorNone;
   }; break;

    case OMX_IndexParamAudioVorbis: {
       ASSERT_STRUCT_TYPE(pParamStruct, OMX_AUDIO_PARAM_VORBISTYPE);
       OMX_AUDIO_PARAM_VORBISTYPE* param;
       param = (OMX_AUDIO_PARAM_VORBISTYPE*)pParamStruct;
       assert(param->nPortIndex == this->definition().index());
       m_vorbisParams = *param;
       this->codingFormat(OMX_AUDIO_CodingVORBIS);
       OMX_DBGT_PDEBUG("::OMX_IndexParamAudioVorbis:");
       OMX_DBGT_EPILOG();
       return OMX_ErrorNone;
    }; break;

    case OMX_IndexParamAudioWma: {
       ASSERT_STRUCT_TYPE(pParamStruct, OMX_AUDIO_PARAM_WMATYPE);
       OMX_AUDIO_PARAM_WMATYPE* param;
       param = (OMX_AUDIO_PARAM_WMATYPE*)pParamStruct;
       assert(param->nPortIndex == this->definition().index());
       m_wmaParams = *param;
       this->codingFormat(OMX_AUDIO_CodingWMA);
       OMX_DBGT_PDEBUG("::OMX_IndexParamAudioWma:");
       OMX_DBGT_EPILOG();
       return OMX_ErrorNone;
    }; break;

    case OMX_IndexParamAudioAc3: {
        ASSERT_STRUCT_TYPE(pParamStruct, OMX_AUDIO_PARAM_AC3TYPE);
        OMX_AUDIO_PARAM_AC3TYPE* param;
        param = (OMX_AUDIO_PARAM_AC3TYPE*)pParamStruct;
        assert(param->nPortIndex == this->definition().index());
        m_ac3Params = *param;
        this->codingFormat((OMX_AUDIO_CODINGTYPE)OMX_AUDIO_CodingAC3);
        OMX_DBGT_PDEBUG("::OMX_IndexParamAudioAc3:");
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }; break;

    case OMX_IndexParamAudioDts: {
        ASSERT_STRUCT_TYPE(pParamStruct, OMX_AUDIO_PARAM_DTSTYPE);
        OMX_AUDIO_PARAM_DTSTYPE* param;
        param = (OMX_AUDIO_PARAM_DTSTYPE*)pParamStruct;
        assert(param->nPortIndex == this->definition().index());
        m_dtsParams = *param;
        this->codingFormat((OMX_AUDIO_CODINGTYPE)OMX_AUDIO_CodingDTS);
        OMX_DBGT_PDEBUG("::OMX_IndexParamAudioDts:");
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }; break;

    case OMX_IndexParamAudioAdpcm: {
        ASSERT_STRUCT_TYPE(pParamStruct, OMX_AUDIO_PARAM_ADPCMTYPE);
        OMX_AUDIO_PARAM_ADPCMTYPE* param;
        param = (OMX_AUDIO_PARAM_ADPCMTYPE*)pParamStruct;
        assert(param->nPortIndex == this->definition().index());
        m_adpcmParams = *param;
        this->codingFormat((OMX_AUDIO_CODINGTYPE)OMX_AUDIO_CodingADPCM);
        OMX_DBGT_PDEBUG("::OMX_IndexParamAudioAdpcm:");
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }; break;

    case OMX_IndexParamAudioAmr: {
        ASSERT_STRUCT_TYPE(pParamStruct, OMX_AUDIO_PARAM_AMRTYPE);
        OMX_AUDIO_PARAM_AMRTYPE* param;
        param = (OMX_AUDIO_PARAM_AMRTYPE*)pParamStruct;
        assert(param->nPortIndex == this->definition().index());
        OMX_DBGT_PDEBUG("::OMX_IndexParamAudioAmr:");
        if(param->eAMRFrameFormat != OMX_AUDIO_AMRFrameFormatFSF)
        {
            OMX_DBGT_EPILOG("Unsupported AMR format");
            return OMX_ErrorNotImplemented;
        }
        m_amrParams = *param;
        this->codingFormat((OMX_AUDIO_CODINGTYPE)OMX_AUDIO_CodingAMR);
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }; break;

    case OMX_IndexParamAudioPcm: {
        ASSERT_STRUCT_TYPE(pParamStruct, OMX_AUDIO_PARAM_PCMMODETYPE);
        OMX_AUDIO_PARAM_PCMMODETYPE* param;
        param = (OMX_AUDIO_PARAM_PCMMODETYPE*)pParamStruct;
        assert(param->nPortIndex == this->definition().index());
        m_pcmParams = *param;
        this->codingFormat(OMX_AUDIO_CodingPCM);
        OMX_DBGT_PDEBUG("::OMX_IndexParamAudioPcm:");
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }; break;

    case OMX_IndexParamOtherPortFormat:
    case OMX_IndexParamVideoPortFormat:
    case OMX_IndexParamImagePortFormat:
    default: {
        OMX_DBGT_ERROR("Unsupported index");
        OMX_DBGT_EPILOG("Unsupported index");
        return OMX_ErrorUnsupportedIndex;
    }
    }
}

OMX_ERRORTYPE
AudDecInputPort::useBuffer(OMX_BUFFERHEADERTYPE** ppBufferHdr,
                           OMX_PTR pAppPrivate,
                           OMX_U32 nSizeBytes,
                           OMX_U8* pBuffer)
{
    return OmxPort::useBuffer(ppBufferHdr, pAppPrivate,
                              nSizeBytes, pBuffer);
}

OMX_ERRORTYPE
AudDecInputPort::allocateBuffer(OMX_BUFFERHEADERTYPE** ppBufferHdr,
                                OMX_PTR pAppPrivate,
                                OMX_U32 nSizeBytes)
{
    return OmxPort::allocateBuffer(ppBufferHdr, pAppPrivate, nSizeBytes);
}

OMX_ERRORTYPE
AudDecInputPort::freeBuffer(OMX_BUFFERHEADERTYPE* pBufferHdr)
{
    return OmxPort::freeBuffer(pBufferHdr);
}

OMX_ERRORTYPE
AudDecInputPort::emptyThisBuffer(OMX_BUFFERHEADERTYPE* pBufferHdr)
{
    OMX_DBGT_PROLOG();
    this->push(pBufferHdr);
    OMX_DBGT_EPILOG();
    return OMX_ErrorNone;
}

OMX_ERRORTYPE
AudDecInputPort::fillThisBuffer(OMX_BUFFERHEADERTYPE* pBufferHdr)
{
    assert(0 && "Invalid call on input port");
    return OMX_ErrorNone;
}

OMX_ERRORTYPE AudDecInputPort::flush()
{
    OMX_DBGT_PROLOG("Flush audio decoder input port");
    if (isTunneled()) {
        // Proprietary tunneling. Nothing to flush
        DBGT_EPILOG("Port is tunneled.");
        return OMX_ErrorNone;
    }

    OMX_BUFFERHEADERTYPE *buf  = 0;
    this->getRawFifos()->lock();

    while(this->getRawFifos()->tryPop(buf) == true) {
        this->auddec().notifyEmptyBufferDone(buf);
    }
    this->getRawFifos()->unlock();
    buf = 0;
    buf = this->relockCurrent();
    if(buf != NULL)
    {
        this->auddec().notifyEmptyBufferDone(buf);
    }
    this->freeAndUnlockCurrent();
    this->auddec().dvb().resetflags();

    OMX_DBGT_EPILOG();
    return OMX_ErrorNone;
}

OMX_ERRORTYPE AudDecInputPort::pause()
{
    OMX_DBGT_PROLOG("Pausing audio input port");
    if (this->threadState() == EThreadState_Paused) {
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }

    // Pausing thread
    this->threadState(EThreadState_Paused);
    OMX_DBGT_EPILOG();
    return OMX_ErrorNone;
}

OMX_ERRORTYPE AudDecInputPort::resume()
{
    if (this->threadState() == EThreadState_Running) {
        return OMX_ErrorNone;
    }

    // Resuming thread
    this->threadState(EThreadState_Running);
    return OMX_ErrorNone;
}

int AudDecInputPort::decodeFrame(unsigned int nOmxFlags,
                                 uint8_t* pBuf, unsigned int nLen,
                                 uint64_t nTimeStamp)
{
    int dvbErr = 0;
    static int firstTime = true;

    // to protect dvbaudio::write done in emitCodecConfig and emitFrame from dvbaudio::pause
    this->auddec().dvb().lock();

    if (nOmxFlags & OMX_BUFFERFLAG_CODECCONFIG) {
        OMX_DBGT_PDEBUG("%s::Codec configuration:: Bytes Recvd in Buffer = %u",
                    component().name(), nLen);
        OMX_DBGT_PDEBUG("%s::Emit PES Codec config",
                    component().name());
        dvbErr = this->auddec().pesAdapter()->emitCodecConfig(pBuf, nLen);
        if (dvbErr) {
            DBGT_ERROR("Error (%d) when writing in audio dvb", dvbErr);
            this->auddec().dvb().unlock();
            return dvbErr;
        }
        firstTime = false;
    }
    else
    {
        OMX_DBGT_PDEBUG("%s::Emit PES Frame with %u bytes",
                    component().name(), nLen);
        if(firstTime)
        {
            OMX_DBGT_PDEBUG("%s::Codec configuration:: Bytes Recvd in Buffer = %u",
                        component().name(), nLen);
            OMX_DBGT_PDEBUG("%s::Emit PES Codec config",
                        component().name());
            dvbErr = this->auddec().pesAdapter()->emitCodecConfig(pBuf, nLen);
            if (dvbErr) {
                DBGT_ERROR("Error (%d) when writing in audio dvb", dvbErr);
                this->auddec().dvb().unlock();
                return dvbErr;
            }
            firstTime = false;
        }

        if(!this->auddec().outputPort().isTunneled() && (nTimeStamp != (uint64_t)OMX_INVALID_TIMESTAMP))
        {
            // We may require a mapping due to overflow of time beyond 2^34-1
            if(nTimeStamp >  MAX_TS_US)
            {
                uint64_t  nTime = nTimeStamp % MAX_TS_US;
                OMX_DBGT_PDEBUG("TS overflow : actual: %llu, truncated: %llu", nTimeStamp, nTime);
                this->auddec().m_mutexCtrl.lock();
                this->auddec().overflowTimeMapping_p = insertNode(this->auddec().overflowTimeMapping_p, nTimeStamp, nTime);
                this->auddec().m_mutexCtrl.release();
                nTimeStamp = nTime;
            }

            // We may require a mapping due to quantization loss
            uint64_t  nTime = PTS_CORRECTION(nTimeStamp);
            nTime  = PTS_TO_US(nTime);
            if(nTime != nTimeStamp)
            {
                OMX_DBGT_PDEBUG("Potential TS mod : actual: %llu, returned: %llu", nTimeStamp, nTime);
                this->auddec().m_mutexCtrl.lock();
                this->auddec().timeMapping_p = insertNode(this->auddec().timeMapping_p, nTimeStamp, 0);
                this->auddec().m_mutexCtrl.release();
            }
        }

        dvbErr = this->auddec().pesAdapter()->emitFrame(pBuf, nLen, nTimeStamp);
        if (dvbErr) {
            OMX_DBGT_ERROR("Error (%d) when writing in audio dvb", dvbErr);
            this->auddec().dvb().unlock();
            return dvbErr;
        }
    }

    this->auddec().dvb().unlock();

    m_nSendedFrame++;
    OMX_DBGT_PDEBUG("%s::EmptyBufferDone %p (len=%u, pts=%llu, flags=%x) "
                "sendedframe + bufferinpipe =%d(+%d) receivedframe=%d",
                component().name(),
                pBuf,
                nLen,
                nTimeStamp,
                nOmxFlags,
                this->sendedFrame(),
                this->getRawFifos()->size(),
                this->auddec().outputPort().receivedFrame());
    return 0;
}

void AudDecInputPort::threadFunction()
{
    OMX_DBGT_PROLOG("%s Pusher thread running on input port",
                component().name());

    OMX_BUFFERHEADERTYPE *pInBufHdr = 0;
    OMX_U32 nFlags = 0;
    bool eosDetected = false;

    // Push input buffer according state
    while(true)
    {
        m_mutexThread.lock();
        while (this->threadState() == EThreadState_Paused) {
            OMX_DBGT_PINFO("Pause audio input port thread");
            m_condState.wait(m_mutexThread);
            OMX_DBGT_PINFO("Resuming audio input port thread");
        }
        m_mutexThread.release();

        // Wait for a 'EmptyThisBuffer' from player
        pInBufHdr = this->popAndLockCurrent();
        assert(pInBufHdr != 0);
        eosDetected = false;
        if(pInBufHdr != NULL && pInBufHdr != this->EOSBUF)
        {
            if (pInBufHdr->nFlags & OMX_BUFFERFLAG_EOS)
            {
                eosDetected = true;
                OMX_DBGT_PDEBUG("execute Eos");
            }
            nFlags = pInBufHdr->nFlags;
            // Frame to decode
            if (this->decodeFrame(nFlags,
                        pInBufHdr->pBuffer + pInBufHdr->nOffset,
                        pInBufHdr->nFilledLen,
                        pInBufHdr->nTimeStamp)) {
                OMX_DBGT_ERROR("Error when decoding audio frame");
            }
            else this->auddec().dvb().resetflags();

            if(this->auddec().SWDecoderEnabled && !this->auddec().outputPort().isTunneled())
            {
                this->auddec().nDecodedSamples = (eosDetected)?0:this->auddec().outputPort().NbSamples;
                this->auddec().nTS = pInBufHdr->nTimeStamp;
                // Release the buffer here as we have no more use for it
                this->auddec().notifyEmptyBufferDone(pInBufHdr);
                this->freeAndUnlockCurrent();
                //Puller triggered
	        sem_post(m_triggerSem);
                //Wait for the Puller to consume decoded data
                OMX_DBGT_PTRACE("wait in waitSem");
                sem_wait(m_waitSem);
            }
            else
            {
                this->auddec().notifyEmptyBufferDone(pInBufHdr);
                this->freeAndUnlockCurrent();
            }

            if(eosDetected)
            {
                OMX_DBGT_PDEBUG("%s::execute eosDetected",component().name());
                if(!this->auddec().SWDecoderEnabled || this->auddec().outputPort().isTunneled())
                {
                    this->auddec().dvb().flush(true);
                }

                this->auddec().notifyEventHandler(OMX_EventBufferFlag,
                        this->auddec().inputPort().definition().index(),
                        nFlags, 0);

                if(this->auddec().outputPort().isTunneled())
                {
                    this->auddec().notifyEventHandler(OMX_EventBufferFlag,
                        this->auddec().outputPort().definition().index(),
                        nFlags, 0);
#ifndef ANDROID
                    {
                        OMX_BUFFERHEADERTYPE BufferHdr;
                        OMX_CONF_INIT_STRUCT_PTR(&BufferHdr, OMX_BUFFERHEADERTYPE);
                        BufferHdr.nFlags = OMX_BUFFERFLAG_EOS;
                        if(this->auddec().hTunnelComp)
                            OMX_EmptyThisBuffer(this->auddec().hTunnelComp, &BufferHdr);
                    }
#endif
                }

            }
        }
        else
        {
            this->freeAndUnlockCurrent();
            goto EXIT;
        }
    }
EXIT:
    OMX_DBGT_PINFO("Pusher thread exit (ADECComponent::PusherRunner)");
    flush();
    OMX_DBGT_EPILOG();
}

} // eof namespace stm
