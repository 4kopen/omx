/*
 * Copyright 2011 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *
 * File   ::    wma.cpp
 * Author ::    Damien TOURNUS (damien.tournus@st.com)
 *
 *
 */

#include "AudioPesAdapter.hpp"
#include "AudioDecoder.hpp"
#include "OMX_debug.h"

#include "pes.hpp"

#define DBGT_PREFIX "WMA "
#define DBGT_LAYER  1
#include <linux/dbgt.h>

//#define WMA_DEBUG

#ifdef WMA_DEBUG
#include <stdio.h>
#endif

#define PTS_CORRECTION(x)   (((x)* 90000)/1000000)

namespace stm {

WmaPesAdapter::WmaPesAdapter(const ADECComponent& dec)
    :AudioPesAdapter(dec)
{}
  
WmaPesAdapter::~WmaPesAdapter()
{}

int WmaPesAdapter::emitCodecConfig(uint8_t* pBuffer,
                                   unsigned int nFilledLen) const
{
    int   header_length;
    int err = 0;

    OMX_DBGT_PROLOG();

    memset (PesHeader, 0, PES_MAX_HEADER_SIZE);
    header_length = InsertPesHeader (PesHeader, nFilledLen, MPEG_AUDIO_PES_START_CODE, INVALID_PTS_VALUE, 0);
    err = this->auddec().dvb().write(PesHeader, header_length);
    if(err)
    {
       OMX_DBGT_ERROR("%s::WMAEmitCodecConfig->dvb().write failed",
                  this->name());
       goto error;
    }

#ifdef WMA_DEBUG
    {
        int i = 0;
        char* LogPtr = (char *)malloc (header_length * 3 + 1);

        OMX_DBGT_PINFO("%s WMA data header length %lu (ADECComponent::WMAEmitCodecConfig)",
                   this->name(), (unsigned long)header_length);
        for(i = 0; i < header_length; i++)
        {
            sprintf (LogPtr+3*i, "%.2x ", PesHeader[i]);
        }
        OMX_DBGT_PINFO("%s %s (ADECComponent::WMAEmitCodecConfig)",
                   this->name(), LogPtr);
        free (LogPtr);
    }
#endif

    err = this->auddec().dvb().write(pBuffer, nFilledLen);
    if(err)
    {
       OMX_DBGT_ERROR("%s::WMAEmitCodecConfig->dvb().write failed",
                  this->name());
       goto error;
    }

#ifdef WMA_DEBUG
    {
        int i = 0;
        char* LogPtr = (char *)malloc (nFilledLen * 3 + 1);

        OMX_DBGT_PINFO("%s WMA data length %lu (ADECComponent::WMAEmitCodecConfig)",
                   this->name(), (unsigned long)nFilledLen);
        for(i = 0; i < (int)nFilledLen; i++)
        {
            sprintf (LogPtr+3*i, "%.2x ", pBuffer[i]);
        }
        OMX_DBGT_PINFO("%s %s (ADECComponent::WMAEmitCodecConfig)",
                   this->name(), LogPtr);
        free (LogPtr);
    }
#endif
error:
    OMX_DBGT_EPILOG();
    return err;
}



int WmaPesAdapter::emitFrame(uint8_t* pBuffer,
                             unsigned int nFilledLen, uint64_t nTimeStamp) const
{
    int   header_length;
    int err = 0;
    uint64_t nPTS = PTS_CORRECTION(nTimeStamp);

    OMX_DBGT_PROLOG("pBuf=%p, len=%d, nTimeStamp=%llu", pBuffer, nFilledLen, nTimeStamp);

    if(nPTS > INVALID_PTS_VALUE)
    {
        //We will anyway overflow. So send no PTS
        nPTS = INVALID_PTS_VALUE;
    }

    memset (PesHeader, 0, PES_MAX_HEADER_SIZE);
    header_length = InsertPesHeader (PesHeader, nFilledLen, MPEG_AUDIO_PES_START_CODE, nPTS, 0);
    err = this->auddec().dvb().write(PesHeader, header_length);
    if(err)
    {
       OMX_DBGT_ERROR("%s::WMAEmitFrame->dvb().write failed", this->name());
       goto error;
    }

#ifdef WMA_DEBUG
    {
        int i = 0;
        char* LogPtr = (char *)malloc (header_length * 3 + 1);

        OMX_DBGT_PINFO("%s WMA data header length %lu (ADECComponent::WMAEmitFrame)", name(), (unsigned long)header_length);
        for(i = 0; i < header_length; i++)
        {
            sprintf (LogPtr+3*i, "%.2x ", PesHeader[i]);
        }
        OMX_DBGT_PINFO("%s %s (ADECComponent::WMAEmitFrame)", name(), LogPtr);
        free (LogPtr);
    }
#endif

    if (nFilledLen > 0)
        err = this->auddec().dvb().write(pBuffer, nFilledLen);
    if(err)
    {
       OMX_DBGT_ERROR("%s::WMAEmitFrame->dvb().write failed", name());
       goto error;
    }

#ifdef WMA_DEBUG
    {
        int i = 0;
        char* LogPtr = (char *)malloc (40 * 3 + 1);

        OMX_DBGT_PINFO("%s WMA data length %lu (ADECComponent::WMAEmitFrame)", name(), (unsigned long)nFilledLen);
        for(i = 0; i < 40; i++)
        {
            sprintf (LogPtr+3*i, "%.2x ", pBuffer[i]);
        }
        OMX_DBGT_PINFO ("%s %s (ADECComponent::WMAEmitFrame)", name(), LogPtr);
        free (LogPtr);
    }
#endif

error:
    OMX_DBGT_EPILOG();
    return err;
}

} // eof namespace stm
