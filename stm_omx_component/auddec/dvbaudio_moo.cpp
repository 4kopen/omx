/*
 * Copyright 2013 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file   dvbaudio_moo.cpp
 * @author STMicroelectronics
 */

#include "dvbaudio_moo.hpp"
#include "OMX_debug.h"
#include <sys/ioctl.h>
#include <sys/types.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>

#include <linux/dvb/audio.h>
#include <linux/dvb/stm_dvb.h>
#define ENABLE_TEMPORARY_ENTRY_POINTS
#include <linux/dvb/stm_audio.h>

#if HAVE_ASSERT_H
#include <assert.h>
#endif

#if HAVE_DBGTASSERT_H
// Re-direct assert on DBGT_ASSERT macro
#define assert(expr)  OMX_DBGT_ASSERT(expr)
#endif

#undef  DBGT_TAG
#define DBGT_TAG "DVBAM"
#define DBGT_PREFIX "DVBAM"
#define DBGT_LAYER  1
#define DBGT_VAR mDbgtVarDVBAM
#define DBGT_DECLARE_AUTOVAR
#include <linux/dbgt.h>


#define THREAD_GUARD(name)  stm::ScopedLock name(m_mutexCmd)

DVBAudio_MOO::DVBAudio_MOO(const char* aDeviceName, int nEncoding, const char *cName)
    :mIsLoggingEnabled(false),
     adev_id(-1),
     m_nEncoding(nEncoding),
     enableSync(true),
     m_nState(EState_Stop),
     m_mutexCmd(),
     m_mutexCmd2(),
     m_condState(),
     m_bClocked(true),
     m_bClearWrite(false),
     m_bClearWrite2(false),
     m_bFlushWrite(false)
{
    DBGT_TRACE_INIT(adec); // Reuse ADEC trace property
    //check the string length before copy
    assert(strlen(aDeviceName) <= sizeof(m_deviceName));
    strcpy(m_deviceName, aDeviceName);
    OMXDEBUG_UPDATE_AV_TRACE_SETTINGS(cName);
}

DVBAudio_MOO::~DVBAudio_MOO()
{}

//=====================================================================================
// DVBAudio_MOO::id
//=====================================================================================
int DVBAudio_MOO::id() const
{ return adev_id; }

//=====================================================================================
// DVBAudio_MOO::name
//=====================================================================================
const char* DVBAudio_MOO::name() const
{ return m_deviceName; }

stm::IDvbDevice::EState DVBAudio_MOO::state() const
{ return m_nState; }

void DVBAudio_MOO::state(stm::IDvbDevice::EState st) const
{
    m_nState = st;
    m_condState.signal();
}

//=====================================================================================
// DVBAudio_MOO::setEncoding
//=====================================================================================
void DVBAudio_MOO::setEncoding(int nEncoding)
{
    m_nEncoding = nEncoding;
}

//==============================================================================
// DVBAudio_MOO::setSync
//==============================================================================

void DVBAudio_MOO::setSync(bool bSyncEnable)
{
    enableSync = bSyncEnable;
    if(isOpen())
    {
        if (ioctl (id(), AUDIO_SET_AV_SYNC, (void*)enableSync) != 0)
        {
            OMX_DBGT_ERROR("AUDIO_SET_AV_SYNC ioctl failed (%s)", strerror (errno));
        }
    }
}


//=====================================================================================
// DVBAudio_MOO::create
//=====================================================================================
int DVBAudio_MOO::create()
{
    THREAD_GUARD(guard);
    OMX_DBGT_PROLOG("Creating DVB audio %s device", name());
    if(!isOpen()){
        adev_id = open(name(), O_RDWR);
    }

    if (adev_id < 0){
        OMX_DBGT_ERROR("Unable to open Device %s (%s)", name(), strerror (errno));
        OMX_DBGT_EPILOG("Unable to open device");
        return -1;
    }

    OMX_DBGT_PINFO("'%s'  Configure DVB audio", name());

    // Setting the video device number to 0 for now
    if (ioctl (id(), AUDIO_SET_SYNC_GROUP, (void*)(AUDIO_SYNC_GROUP_VIDEO + 0)) != 0)
    {
        OMX_DBGT_ERROR("AUDIO_SET_SYNC_GROUP ioctl failed (%s)", strerror (errno));
        OMX_DBGT_EPILOG("Unable to configure device");
        return -1;
    }

    if (ioctl (id(), AUDIO_CHANNEL_SELECT, (void*)(AUDIO_STEREO)) != 0)
    {
        OMX_DBGT_ERROR("AUDIO_CHANNEL_SELECT ioctl failed (%s)", strerror (errno));
        OMX_DBGT_EPILOG("Unable to configure device");
        return -1;
    }

    if (ioctl (id(), AUDIO_SET_APPLICATION_TYPE, (void*)(AUDIO_APPLICATION_ISO)) != 0)
    {
        OMX_DBGT_ERROR("AUDIO_SET_APPLICATION_TYPE ioctl failed (%s)", strerror (errno));
        OMX_DBGT_EPILOG("Unable to configure device");
        return -1;
    }

    if (ioctl (id(), AUDIO_SET_PROGRAM_REFERENCE_LEVEL, (void*)(0)) != 0)
    {
        OMX_DBGT_ERROR("AUDIO_SET_PROGRAM_REFERENCE_LEVEL ioctl failed (%s)", strerror (errno));
        OMX_DBGT_EPILOG("Unable to configure device");
        return -1;
    }

    if (ioctl (id(), AUDIO_SELECT_SOURCE, (void*)AUDIO_SOURCE_MEMORY) != 0)
    {
        OMX_DBGT_ERROR("AUDIO_SELECT_SOURCE ioctl failed (%s)", strerror (errno));
        OMX_DBGT_EPILOG("Unable to configure device");
        return -1;
    }

    if (ioctl (id(), AUDIO_SET_STREAMTYPE ,(void*)STREAM_TYPE_PES) != 0)
    {
        OMX_DBGT_ERROR("AUDIO_SET_STREAMTYPE ioctl failed (%s)", strerror (errno));
        OMX_DBGT_EPILOG("Unable to configure device");
        return -1;
    }

    if (ioctl (id(), AUDIO_SET_ENCODING, (void*)m_nEncoding) != 0)
    {
        OMX_DBGT_ERROR("AUDIO_SET_ENCODING ioctl failed (%s)", strerror (errno));
        OMX_DBGT_EPILOG("Unable to configure device");
        return -1;
    }

    // Setting of specifical AAC configuration parameters
    if (m_nEncoding == AUDIO_ENCODING_AAC)
    {
        OMX_DBGT_PDEBUG("Specific AAC configuration parameters");
        audio_mpeg4aac_t audio_mpeg4aac;
        audio_mpeg4aac.aac_profile    = AUDIO_AAC_LC_TS_PROFILE ;
        audio_mpeg4aac.sbr_enable     = false;
        audio_mpeg4aac.sbr_96k_enable = false;
        audio_mpeg4aac.ps_enable      = false;

        if (ioctl (adev_id, AUDIO_SET_AAC_DECODER_CONFIG, (void*)&audio_mpeg4aac) != 0)
        {
            OMX_DBGT_ERROR("DVBAudio_MOO::Instantiate AUDIO_SET_AAC_DECODER_CONFIG ioctl failed (%s)",
                       strerror (errno));
            OMX_DBGT_EPILOG("Unable to start device");
            return -1;
        }
    }

    if (ioctl (id(), AUDIO_SET_AV_SYNC, (void*)enableSync) != 0)
    {
        OMX_DBGT_ERROR("AUDIO_SET_AV_SYNC ioctl failed (%s)", strerror (errno));
        OMX_DBGT_EPILOG("Unable to configure device");
        return -1;
    }

    OMX_DBGT_EPILOG();
    return 0;
}

//=====================================================================================
// DVBAudio_MOO::destroy
//=====================================================================================
int DVBAudio_MOO::destroy()
{
    THREAD_GUARD(guard);
    OMX_DBGT_PROLOG();
    if (close(id()) != 0) {
        OMX_DBGT_ERROR("Audio close failed (%s)", strerror (errno));
        OMX_DBGT_EPILOG("Unable to close device");
        return -1;
    }

    adev_id = -1;
    OMX_DBGT_EPILOG();
    return 0;
}

//=====================================================================================
// DVBAudio_MOO::isOpen
//=====================================================================================
bool DVBAudio_MOO::isOpen() const
{ return (id() != -1); }

//=====================================================================================
// DVBAudio_MOO::start
//=====================================================================================
int DVBAudio_MOO::start() const
{
    struct audio_command command;
    THREAD_GUARD(guard);
    OMX_DBGT_PROLOG("Starting device Audio");
    assert(isOpen());
    if (m_bClocked == false) {
        OMX_DBGT_EPILOG("started in state pause");
        return 0;
    }
    if (this->isStarted()) {
        OMX_DBGT_EPILOG("Already started");
        return 0;
    }

    memset(&command, 0, sizeof(command));
    command.cmd      = AUDIO_CMD_SET_OPTION;
    command.u.option.option = DVB_OPTION_CTRL_REDUCE_COLLATED_DATA;
    command.u.option.value  = (enableSync)?DVB_OPTION_VALUE_DISABLE:DVB_OPTION_VALUE_ENABLE;

    if (ioctl (id(), AUDIO_COMMAND ,&command) != 0)
    {
        OMX_DBGT_ERROR("AUDIO_COMMAND REDUCE_COLLATED_DATA (%s)", strerror (errno));
        OMX_DBGT_EPILOG();
        return -1;
    }

    if (ioctl (id(), AUDIO_PLAY, NULL) != 0) {
        OMX_DBGT_ERROR("AUDIO_PLAY ioctl failed (%s)", strerror (errno));
        OMX_DBGT_EPILOG("Unable to start device");
        return -1;
    }
    this->state(stm::IDvbDevice::EState_Start);
    OMX_DBGT_EPILOG();
    return 0;
}

//=====================================================================================
// DVBAudio_MOO::stop
//=====================================================================================
int DVBAudio_MOO::stop() const
{
    THREAD_GUARD(guard);
    OMX_DBGT_PROLOG();
    assert(isOpen());
    if (this->isStopped()) {
        OMX_DBGT_EPILOG("Already stopped");
        return 0;
    }
    if (ioctl (id(), AUDIO_STOP, NULL) != 0) {
        OMX_DBGT_ERROR("AUDIO_STOP ioctl failed (%s)", strerror (errno));
        OMX_DBGT_EPILOG("Unable to stop device");
        return -1;
    }
    this->state(stm::IDvbDevice::EState_Stop);
    OMX_DBGT_EPILOG();
    return 0;
}

//=====================================================================================
// DVBAudio_MOO::pause
//=====================================================================================
int DVBAudio_MOO::pause(bool hw) const
{
    THREAD_GUARD(guard);
    OMX_DBGT_PROLOG();
    assert(isOpen());
    if (this->isStopped()) {
        if (m_nEncoding == AUDIO_ENCODING_PCM) {
            m_bClocked = false;
            this->state(stm::IDvbDevice::EState_Pause);
        }
        OMX_DBGT_EPILOG();
        return 0;
    }
    if (this->isPaused()) {
        OMX_DBGT_EPILOG("Already paused");
        return 0;
    }
    if (hw)
    if (ioctl(id(), AUDIO_PAUSE, NULL) != 0) {
        OMX_DBGT_ERROR("AUDIO_PAUSE ioctl failed (%s)", strerror (errno));
        OMX_DBGT_EPILOG("Unable to pause device");
        return -1;
    }
    this->state(stm::IDvbDevice::EState_Pause);
    OMX_DBGT_EPILOG();
    return 0;
}

//=====================================================================================
// DVBAudio_MOO::resume
//=====================================================================================
int DVBAudio_MOO::resume(bool hw) const
{
    THREAD_GUARD(guard);
    OMX_DBGT_PROLOG();
    assert(isOpen());
    if (this->isStopped()) {
        OMX_DBGT_WARNING("Invalid state change");
        OMX_DBGT_EPILOG("Can not resume a stopped device");
        return 0;
    }
    if (!this->isPaused()) {
        OMX_DBGT_EPILOG("Already resumed");
        return 0;
    }
    if (hw) {
        if (m_bClocked == false) {
            m_bClocked = true;
            if (ioctl(id(), AUDIO_PLAY, NULL) != 0) {
                OMX_DBGT_ERROR("AUDIO_PLAY ioctl failed (%s)", strerror (errno));
                OMX_DBGT_EPILOG("Unable to start device");
                return -1;
            }
        } else {
            if (ioctl(id(), AUDIO_CONTINUE, NULL) != 0) {
                OMX_DBGT_ERROR("AUDIO_CONTINUE ioctl failed (%s)", strerror (errno));
                OMX_DBGT_EPILOG("Unable to resume device");
                return -1;
            }
        }
    }
    this->state(stm::IDvbDevice::EState_Start);
    OMX_DBGT_EPILOG();
    return 0;
}

//=====================================================================================
// DVBAudio_MOO::lock
//=====================================================================================
void DVBAudio_MOO::lock() const
{
    OMX_DBGT_PROLOG();
    m_mutexCmd2.lock();
}

//=====================================================================================
// DVBAudio_MOO::unlock
//=====================================================================================
void DVBAudio_MOO::unlock() const
{
    OMX_DBGT_PROLOG();
    m_mutexCmd2.release();
}

//=====================================================================================
// DVBAudio_MOO::resetflags
//=====================================================================================
void DVBAudio_MOO::resetflags() const
{
    if (m_bClearWrite || m_bFlushWrite) {
        OMX_DBGT_PROLOG("step 1");

        m_bClearWrite = false;
        m_bClearWrite2 = true;
        m_bFlushWrite = false;

        OMX_DBGT_EPILOG();
    } else if (m_bClearWrite2) {
        OMX_DBGT_PROLOG("step 2");

        m_bClearWrite2 = false;

        OMX_DBGT_EPILOG();
    }
}

//=====================================================================================
// DVBAudio_MOO::write
//=====================================================================================
int DVBAudio_MOO::write(const void* data_ptr, unsigned int len) const
{
    THREAD_GUARD(guard);
    OMX_DBGT_PROLOG("Injecting data in DVB Audio");
    assert(isOpen());

    // Block request if in pause state
    if (m_bClearWrite2 == false)
    while (this->state() == EState_Pause) {
        OMX_DBGT_PINFO("Blocking write on a paused device");
        // Check if device was cleared in the meantime
        if (m_bClearWrite == true || m_bFlushWrite == true) {
            OMX_DBGT_EPILOG("Data cleared");
            return -ECANCELED;
        }
        m_condState.wait(m_mutexCmd);
        // Check if device was cleared in the meantime
        if (m_bClearWrite == true || m_bFlushWrite == true) {
            OMX_DBGT_EPILOG("Data cleared");
            return -ECANCELED;
        }
    }

    if (this->isStopped()) {
        OMX_DBGT_WARNING("Writing on a stopped device");
        OMX_DBGT_EPILOG("Data skipped");
        return -EAGAIN;
    }

    int retval = ::write(id(), data_ptr, (size_t)len);
    if (retval < 0) {
        OMX_DBGT_ERROR("Error when writing in audio device (%s)", strerror(errno));
        OMX_DBGT_EPILOG("Write error");
        return -errno;
    }
    assert(retval >= 0);
    if (len != (unsigned int)retval) {
        OMX_DBGT_ERROR("Error writing Audio content write=%lu != written=%d",
                   (unsigned long)len, retval);
        OMX_DBGT_EPILOG("Write error");
        return -errno;
    }
    OMX_DBGT_EPILOG();
    return 0;
}

//=====================================================================================
// DVBAudio_MOO::flush
//=====================================================================================
int DVBAudio_MOO::flush(bool bEoS) const
{
    THREAD_GUARD(guard);
    OMX_DBGT_PROLOG();
    assert(isOpen());
    if (this->isStopped()) {
        OMX_DBGT_EPILOG("Flushing a stopped device");
        return 0;
    }
    if (bEoS) {
        // Inject EOS discontinuity.
        if (ioctl(id(), AUDIO_DISCONTINUITY, (void*)DVB_DISCONTINUITY_EOS) != 0)
        {
            OMX_DBGT_ERROR("AUDIO_DISCONTINUITY ioctl failed (%s)",
                       strerror (errno));
            OMX_DBGT_EPILOG();
            return -1;
        }
    }
    // Wait till pipeline is flushed
    if (ioctl(id(), AUDIO_FLUSH, NULL) != 0) {
        OMX_DBGT_ERROR("AUDIO_FLUSH ioctl failed (%s)", strerror (errno));
        OMX_DBGT_EPILOG();
        return -1;
    }
    // Trigger state with same value to unblock any write
    m_bFlushWrite = true;
    this->state(this->state());
    OMX_DBGT_EPILOG();
    return 0;
}

//=====================================================================================
// DVBAudio_MOO::clear
//=====================================================================================
int DVBAudio_MOO::clear() const
{
    THREAD_GUARD(guard);
    OMX_DBGT_PROLOG();
    assert(isOpen());
    if (this->isStopped()) {
        OMX_DBGT_EPILOG("Clear on a stopped device");
        return 0;
    }

    if (m_bClocked == true) {
        // Flush the current stream from the pipeline.  This is accomplished
        // by draining the stream with discard and indicating that a
        // discontinuity will occur
        if (ioctl(id(), AUDIO_CLEAR_BUFFER, NULL) != 0) {
            OMX_DBGT_ERROR("AUDIO_CLEAR_BUFFER ioctl failed (%s)", strerror (errno));
            OMX_DBGT_EPILOG();
            return -1;
        }
    }

    // Trigger state with same value to unblock any write
    m_bClearWrite = true;
    this->state(this->state());
    OMX_DBGT_EPILOG();
    return 0;
}

// Todo shall be exported in dvb headers
#define INVALID_TIME    0xfeedfacedeadbeefull

//=====================================================================================
// DVBAudio_MOO::getTime
//=====================================================================================
uint64_t DVBAudio_MOO::getTime() const
{

    OMX_DBGT_PROLOG();
    static uint64_t LastInvalidTime = (uint64_t)-1ll;
    audio_play_info_t AudioPlayInfo;
    assert(isOpen());
    if (ioctl(id(), AUDIO_GET_PLAY_INFO, (void*)&AudioPlayInfo) != 0)
    {
        OMX_DBGT_ERROR("AUDIO_GET_PLAY_INFO ioctl failed (%s)", strerror (errno));
        return 0;
    }
    OMX_DBGT_PDEBUG("Get time info on audio device "
                "sys_time=%llu pres_time=%llu pts=%llu" ,
                AudioPlayInfo.system_time,
                AudioPlayInfo.presentation_time,
                AudioPlayInfo.pts);

    if ((AudioPlayInfo.pts == INVALID_TIME) || (AudioPlayInfo.pts == LastInvalidTime)) {
        OMX_DBGT_EPILOG("Invalid audio media time");
        return 0;
    }

    if (AudioPlayInfo.presentation_time == INVALID_TIME) {
        LastInvalidTime = AudioPlayInfo.pts;
        OMX_DBGT_EPILOG("Invalid audio media time");
        return 0;
    }

    uint64_t audtime = (AudioPlayInfo.pts * 100) / 9; // pts unit is 90 kHz ticks
    OMX_DBGT_EPILOG();
    return audtime;
}

//=====================================================================================
// DVBAudio_MOO::UseBuffer
// //==================================================================================

int DVBAudio_MOO::UseBuffer(unsigned long virtualAddr, unsigned long bufferSize,
                        int width, int height, int color,
                        unsigned long userId) const
{
    assert(0);
    return 0;
}

//=====================================================================================
// DVBAudio_MOO::FillThisBuffer
//=====================================================================================
int DVBAudio_MOO::FillThisBuffer(unsigned long userId) const
{
    assert(0);
    return 0;
}

//=====================================================================================
// DVBAudio_MOO::FillBufferDone
//=====================================================================================
int DVBAudio_MOO::FillBufferDone(unsigned long &userId, uint64_t &TS,
                             bool & eos) const
{
    assert(0);
    return 0;
}

//=====================================================================================
// DVBAudio_MOO::FlushBuffer
//=====================================================================================
unsigned long DVBAudio_MOO::FlushBuffer() const
{
    assert(0);
    return 0;
}

//=====================================================================================
// DVBAudio_MOO::QueueBuffer
//=====================================================================================
int DVBAudio_MOO::QueueBuffer (void* BufferData, int &nBytesSize, uint64_t &ts) const
{
    assert(0);
    return 0;
}


