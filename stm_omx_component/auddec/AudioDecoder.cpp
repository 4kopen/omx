/*
 * Copyright 2011 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *
 * File	  ::	AudioDecoder.cpp
 * Author ::	Jean-Philippe Fassino (jean-philippe.fassino@st.com)
 *
 *
 */


#include "AudioDecoder.hpp"
#include "AudioPesAdapter.hpp"
#include "StmOmxVendorExt.hpp"
#include "OMX_debug.h"
#include "pes.hpp"

#include <linux/dvb/stm_audio.h>
#include <string.h>

#if HAVE_ASSERT_H
#include <assert.h>
#endif

#if HAVE_DBGTASSERT_H
// Re-direct assert on DBGT_ASSERT macro
#define assert(expr)  OMX_DBGT_ASSERT(expr)
#endif

#define DBGT_DECLARE_AUTOVAR
#define DBGT_PREFIX "ADEC"
#define DBGT_LAYER  0
#include <linux/dbgt.h>

namespace stm {

//==========================================
// ADECComponent constructor
//==========================================

static const char kRoleDefault[] = "default";
static const char kRoleMP3[] = "audio_decoder.mp3";
static const char kRoleVORBIS[] = "audio_decoder.vorbis";
static const char kRoleAAC[] = "audio_decoder.aac";
static const char kRolePCM[] = "audio_decoder.pcm";
static const char kRoleWMA[] = "audio_decoder.wma";
static const char kRoleAC3[] = "audio_decoder.ac3";
static const char kRoleDTS[] = "audio_decoder.dts";
static const char kRoleAMR[] = "audio_decoder.amrnb";
static const char kRoleADPCM[] = "audio_decoder.adpcm";
static const char kRoleMPA[] = "audio_decoder.mpa";
#ifdef ANDROID
const char DVB_DEVICE_MIXER[] = "/dev/dvb0.audio0";
#else
const char DVB_DEVICE_MIXER[] = "/dev/dvb/adapter0/audio0";
#endif
#ifndef DIRECT_MIXER
const char V4L_DEVICE[] =       "/dev/video0";
#endif

ADECComponent::ADECComponent(OMX_HANDLETYPE hComponent):
    OmxComponent("OMX.STM.Audio.Decoder", hComponent, OMX_ST_AUDIO_PORT_NUMBER),
    m_dvb(0),
    m_inputPort(OMX_ST_AUDIO_INPUT_PORT, OMX_DirInput, *this),
    m_outputPort(OMX_ST_AUDIO_OUTPUT_PORT, OMX_DirOutput, *this),
    m_componentRole(),
    m_pPesAdapter(0),
    swDecInterface(),
    amrConfig(),
    amrInfo(),
    inputPortSem(),
    outputPortSem(),
    SWDecoderEnabled(false),
    nTS(0),
    nDecodedSamples(0),
    hTunnelComp(NULL),
    timeMapping_p(NULL),
    overflowTimeMapping_p(NULL),
    m_mutexCtrl()
{
    DBGT_TRACE_INIT(adec);
}

ADECComponent::~ADECComponent()
{
    if (m_pPesAdapter != NULL) {
        delete m_pPesAdapter;
        m_pPesAdapter = 0;
    }

    if (m_dvb) {
        delete m_dvb;
        m_dvb = 0;
    }
}

//=====================================================================================
// ADECComponent::flush
//  flush current decoded frame
//======================================================================================
OMX_ERRORTYPE ADECComponent::flush(unsigned int nPortIdx)
{
    OMX_DBGT_PROLOG();
    OMX_DBGT_PDEBUG("ADECComponent::flush on port %d  %s",
                nPortIdx, name());
    if(SWDecoderEnabled)
    {
        swDecInterface.codec_state.first_time = true;
        amr_decode_reset(&swDecInterface);
    }

    if((this->outputPort_().isTunneled() == false) && !SWDecoderEnabled )
    {
        int result = this->dvb().clear();
        if (result)
        {
           OMX_DBGT_ERROR("%s::clear failed", name());
           OMX_DBGT_EPILOG();
           return OMX_ErrorUndefined;
        }
        m_mutexCtrl.lock();
        destroyList(timeMapping_p);
        timeMapping_p = NULL;
        destroyList(overflowTimeMapping_p);
        overflowTimeMapping_p = NULL;
        m_mutexCtrl.release();
    }

    OMX_ERRORTYPE eError = this->getPort(nPortIdx).flush();
    OMX_DBGT_EPILOG();
    return eError;
}

//==========================================================
// ADECComponent::getParameter
// allows to return current parameters to player
//==========================================================
OMX_ERRORTYPE ADECComponent::getParameter(OMX_INDEXTYPE nParamIndex,
                                          OMX_PTR       pParamStruct)
{
    OMX_DBGT_PROLOG("%s getParameter 0x%08x", name(), nParamIndex);
    OMX_DBGT_PROLOG();
    OMX_ERRORTYPE eError = OMX_ErrorNone;

    switch ((int)nParamIndex)
    {
    case OMX_IndexParamAudioInit: {
        ASSERT_STRUCT_TYPE(pParamStruct, OMX_PORT_PARAM_TYPE);
        OMX_PORT_PARAM_TYPE *param = (OMX_PORT_PARAM_TYPE*)pParamStruct;
        assert(param);
        OMX_DBGT_PDEBUG("::OMX_IndexParamAudioInit:");
        param->nPorts = OMX_ST_AUDIO_PORT_NUMBER;
        param->nStartPortNumber = 0;
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }; break;

    }

    // If the component does not support it, we fall back to generic logic.
    eError = OmxComponent::getParameter(nParamIndex,
                                        pParamStruct);
    OMX_DBGT_EPILOG();
    return eError;
}

//==========================================================================
// ADECComponent::setParameter
// allows player to set parameters associated to output port of OMX component
//==========================================================================
OMX_ERRORTYPE ADECComponent::setParameter(OMX_INDEXTYPE nParamIndex,
                                          OMX_PTR       pParamStruct)
{
    OMX_ERRORTYPE eError = OMX_ErrorNone;
    OMX_DBGT_PROLOG();

    switch ((int)nParamIndex)
    {
    case OMX_IndexParamStandardComponentRole: {
        ASSERT_STRUCT_TYPE(pParamStruct, OMX_PARAM_COMPONENTROLETYPE);
        OMX_PARAM_COMPONENTROLETYPE *param = (OMX_PARAM_COMPONENTROLETYPE*)pParamStruct;
        assert(param);
        OMX_DBGT_PDEBUG("::OMX_IndexParamStandardComponentRole:");
        eError = this->setRole((const char*)param->cRole);
        OMX_DBGT_EPILOG();
        return eError;
    }; break;


    case OMX_IndexParamAudioPcm:
    case OMX_IndexParamAudioAac:
    case OMX_IndexParamAudioMp3:
    case OMX_IndexParamAudioVorbis:
    case OMX_IndexParamAudioWma:
    case OMX_IndexParamAudioAc3:
    case OMX_IndexParamAudioDts:
    case OMX_IndexParamAudioAmr:
    case OMX_IndexParamAudioAdpcm: {
        //Generic implementation of setParameter
    }; break;


    default:
        OMX_DBGT_PDEBUG("ADECComponent::get undef parameter (0x%08x)",
                    (unsigned int)nParamIndex);
        break;
    }

    // If the component does not support it, we fall back to generic logic.
    eError = OmxComponent::setParameter(nParamIndex, pParamStruct);

    OMX_DBGT_EPILOG();
    return eError;
}

OMX_ERRORTYPE
ADECComponent::getConfig(OMX_INDEXTYPE nIndex,
                          OMX_PTR pConfStruct)
{
    OMX_DBGT_PROLOG();
    switch ((int)nIndex)
    {
    case OMX_StmDvbAudioDeviceInfo: {
        ASSERT_STRUCT_TYPE(pConfStruct, OMX_GetStmLinuxDvbDeviceInfoParams);
        OMX_GetStmLinuxDvbDeviceInfoParams *param;
        param = (OMX_GetStmLinuxDvbDeviceInfoParams*)pConfStruct;
        assert(param);
        OMX_DBGT_PDEBUG("::OMX_StmDvbAudioDeviceInfo:");
        // Return the dvb component
        param->ppDevice = (stm::IDvbDevice**)&m_dvb;
        // Store the sink component
        hTunnelComp = param->hTunnelComp;
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }; break;
    case OMX_IndexConfigTimeCurrentMediaTime: {
        ASSERT_STRUCT_TYPE(pConfStruct, OMX_TIME_CONFIG_TIMESTAMPTYPE);
        OMX_TIME_CONFIG_TIMESTAMPTYPE *param;
        param = (OMX_TIME_CONFIG_TIMESTAMPTYPE *)pConfStruct;
        assert(param);
        OMX_DBGT_PDEBUG("::OMX_IndexConfigTimeCurrentMediaTime:");
        // Get back the timestamp of the data currently being played
        if(m_dvb && m_dvb->isStarted()) {
             param->nTimestamp = m_dvb->getTime();
        }
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    } break;

    }

    // If the component does not support it, we fall back to generic logic.
    OMX_ERRORTYPE eError = OmxComponent::getConfig(nIndex,
                                                   pConfStruct);
    OMX_DBGT_EPILOG();
    return eError;
}

OMX_ERRORTYPE
ADECComponent::setConfig(OMX_INDEXTYPE nIndex,
                          OMX_PTR pConfStruct)
{
    OMX_DBGT_PROLOG();
    switch ((int)nIndex)
    {
    case OMX_IndexConfigTimePosition: {
        ASSERT_STRUCT_TYPE(pConfStruct, OMX_TIME_CONFIG_TIMESTAMPTYPE);
        OMX_TIME_CONFIG_TIMESTAMPTYPE *param = (OMX_TIME_CONFIG_TIMESTAMPTYPE*)pConfStruct;
        assert(param);
        OMX_DBGT_PDEBUG("::OMX_IndexConfigTimePosition:");
        // Do we need to do anything here?
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }; break;
    case OMX_IndexParamLowLatencyMode: {
        ASSERT_STRUCT_TYPE(pConfStruct, OMX_CONFIG_BOOLEANTYPE);
        OMX_CONFIG_BOOLEANTYPE *param = (OMX_CONFIG_BOOLEANTYPE*)pConfStruct;
        // If set Sync disabled
        OMX_DBGT_PDEBUG("::OMX_IndexParamLowLatencyMode: LowLatency Mode %u", param->bEnabled);
        if(m_dvb) this->dvb().setSync(!param->bEnabled);

        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }; break;
    }

    // If the component does not support it, we fall back to generic logic.
    OMX_ERRORTYPE eError = OmxComponent::setConfig(nIndex,
                                                   pConfStruct);
    OMX_DBGT_EPILOG();
    return eError;
}

OMX_ERRORTYPE ADECComponent::getExtensionIndex(
        OMX_STRING          cParameterName,
        OMX_INDEXTYPE *     pIndexType)
{
    OMX_DBGT_PROLOG();
    if (strcmp(cParameterName,
               OMX_StmDvbAudioDeviceInfoExt) == 0) {
        *pIndexType = (OMX_INDEXTYPE)OMX_StmDvbAudioDeviceInfo;
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }
    else if(strcmp(cParameterName,
            RDK_OMX_CONFIG_LOW_LATENCY)==0)
    {
        *pIndexType = (OMX_INDEXTYPE)OMX_IndexParamLowLatencyMode;
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }

    OMX_ERRORTYPE eError = OmxComponent::getExtensionIndex(cParameterName,
                                                           pIndexType);
    OMX_DBGT_EPILOG();
    return eError;
}

OMX_ERRORTYPE ADECComponent::emptyThisBuffer(OMX_BUFFERHEADERTYPE* pBufferHdr)
{
    OMX_DBGT_PROLOG();
    OMX_ERRORTYPE eError = OmxComponent::emptyThisBuffer(pBufferHdr);
    OMX_DBGT_EPILOG();
    return eError;
}

OMX_ERRORTYPE ADECComponent::fillThisBuffer(OMX_BUFFERHEADERTYPE* pBufferHdr)
{
    OMX_DBGT_PROLOG();
    OMX_ERRORTYPE eError = OmxComponent::fillThisBuffer(pBufferHdr);
    OMX_DBGT_EPILOG();
    return eError;
}



//=================================================================================
// ADECComponent::destroy
// Allows free resources associated to the OMX component
//=================================================================================
void ADECComponent::destroy()
{
    OMX_DBGT_PROLOG();

    if(m_dvb) {
        stop();

        dvb().destroy();
    }
    deallocate();

    // destroy the semaphores
    sem_destroy(&inputPortSem);
    sem_destroy(&outputPortSem);

    OmxComponent::destroy();
    OMX_DBGT_EPILOG();
}

//=================================================================================
// ADECComponent::create
// Allows create resources associated to a new OMX component
//=================================================================================
OMX_ERRORTYPE ADECComponent::create ()
{
    OMX_ERRORTYPE eError = OMX_ErrorNone;

    OMX_DBGT_PROLOG();

    if ((eError = OmxComponent::create()) != OMX_ErrorNone) {
        OMX_DBGT_ERROR("eError = 0x%x", eError);
        OMX_DBGT_EPILOG();
        return eError;
    }

    // Initialize decoder roles
    this->role().add(kRoleAAC);
    this->role().add(kRolePCM);
    this->role().add(kRoleMP3);
    this->role().add(kRoleVORBIS);
    this->role().add(kRoleWMA);
    this->role().add(kRoleAC3);
    this->role().add(kRoleDTS);
    this->role().add(kRoleAMR);
    this->role().add(kRoleADPCM);
    this->role().add(kRoleMPA);

    // Initialize the audio parameters for input port
    if ((eError = m_inputPort.create()) != OMX_ErrorNone) {
        OMX_DBGT_ERROR("Error when creating audio decoder input port");
        OMX_DBGT_EPILOG();
        return eError;
    }

    // Initialize the audio parameters for output port
    if ((eError = m_outputPort.create()) != OMX_ErrorNone) {
            OMX_DBGT_ERROR("Error when creating audio decoder output port");
            OMX_DBGT_EPILOG();
            return eError;
    }

    OMX_DBGT_CHECK_RETURN(! sem_init(&inputPortSem,0,0), OMX_ErrorInsufficientResources);
    OMX_DBGT_CHECK_RETURN(! sem_init(&outputPortSem,0,0), OMX_ErrorInsufficientResources);

    m_inputPort.setWaitSem(&outputPortSem);
    m_inputPort.setTriggerSem(&inputPortSem);

    m_outputPort.setWaitSem(&inputPortSem);
    m_outputPort.setTriggerSem(&outputPortSem);

    // Set default role: currently AAC
    this->setRole(kRoleDefault);

    OMX_DBGT_EPILOG();
    return OMX_ErrorNone;
}

OMX_ERRORTYPE ADECComponent::setRole(const char* pNewRole)
{
    int nRoleIdx = -1;
    if (strcmp(pNewRole, kRoleDefault) == 0) {
        nRoleIdx = 0;
        OMX_DBGT_PDEBUG("Setting role to default %s", this->role().at(nRoleIdx));
    } else {
        // check if proposed role is supported
        for (unsigned int i = 0; i < this->role().size(); i++) {
            if (strcmp(pNewRole, this->role().at(i)) == 0) {
                nRoleIdx = i;
                break;
            }
        }
    }

    if (nRoleIdx == -1) {
        OMX_DBGT_ERROR("Unsupported role %s", pNewRole);
        return OMX_ErrorUnsupportedSetting;
    }

    const char* pActivRole = this->role().active(nRoleIdx);
    if (strcmp(pActivRole, kRoleAAC) == 0) {
        this->inputPort_().codingFormat(OMX_AUDIO_CodingAAC);
        this->outputPort_().updateBufferSize(OMX_AUDIO_CodingAAC);
    } else if (strcmp(pActivRole, kRolePCM) == 0) {
        this->inputPort_().codingFormat(OMX_AUDIO_CodingPCM);
        this->outputPort_().updateBufferSize(OMX_AUDIO_CodingPCM);
    } else if (strcmp(pActivRole, kRoleMP3) == 0) {
        this->inputPort_().codingFormat(OMX_AUDIO_CodingMP3);
        this->outputPort_().updateBufferSize(OMX_AUDIO_CodingMP3);
    } else if (strcmp(pActivRole, kRoleVORBIS) == 0) {
        this->inputPort_().codingFormat(OMX_AUDIO_CodingVORBIS);
        this->outputPort_().updateBufferSize(OMX_AUDIO_CodingVORBIS);
    } else if (strcmp(pActivRole, kRoleWMA) == 0) {
        this->inputPort_().codingFormat(OMX_AUDIO_CodingWMA);
        this->outputPort_().updateBufferSize(OMX_AUDIO_CodingWMA);
    } else if (strcmp(pActivRole, kRoleAC3) == 0) {
        this->inputPort_().codingFormat((OMX_AUDIO_CODINGTYPE)OMX_AUDIO_CodingAC3);
        this->outputPort_().updateBufferSize((OMX_AUDIO_CODINGTYPE)OMX_AUDIO_CodingAC3);
    } else if (strcmp(pActivRole, kRoleDTS) == 0) {
        this->inputPort_().codingFormat((OMX_AUDIO_CODINGTYPE)OMX_AUDIO_CodingDTS);
        this->outputPort_().updateBufferSize((OMX_AUDIO_CODINGTYPE)OMX_AUDIO_CodingDTS);
    } else if (strcmp(pActivRole, kRoleAMR) == 0) {
        this->inputPort_().codingFormat((OMX_AUDIO_CODINGTYPE)OMX_AUDIO_CodingAMR);
        this->outputPort_().updateBufferSize((OMX_AUDIO_CODINGTYPE)OMX_AUDIO_CodingAMR);
    } else if (strcmp(pActivRole, kRoleADPCM) == 0) {
        this->inputPort_().codingFormat(OMX_AUDIO_CodingADPCM);
        this->outputPort_().updateBufferSize(OMX_AUDIO_CodingADPCM);
    } else if (strcmp(pActivRole, kRoleMPA) == 0) {
        this->inputPort_().codingFormat((OMX_AUDIO_CODINGTYPE)OMX_AUDIO_CodingMPEG);
        this->outputPort_().updateBufferSize((OMX_AUDIO_CODINGTYPE)OMX_AUDIO_CodingMPEG);
    } else {
        // TODO don't know which role to use in default case...
        assert(0 && "Role not supported");
    }

    return OMX_ErrorNone;
}

const AudDecPort& ADECComponent::getPort(unsigned int nIdx) const
{
    assert(nIdx < OMX_ST_AUDIO_PORT_NUMBER);
    if (nIdx == 0)
        return m_inputPort;
    else
        return m_outputPort;
}

AudDecPort& ADECComponent::getPort(unsigned int nIdx)
{
    assert(nIdx < OMX_ST_AUDIO_PORT_NUMBER);
    if (nIdx == 0)
        return m_inputPort;
    else
        return m_outputPort;
}

//=====================================================================================
// ADECComponent::Convert1x16To2x16Channels
//  AMR decoder O/P channel number is equal to 1
//  As android works only from PCM 16bit stereo, a conversion from 1 channel 16bits to 2 channel 16 bits is done 
//======================================================================================
void ADECComponent::Convert1x16To2x16Channels(int nbSample, uint16_t* pcmsrc,
                                              uint16_t* pcmdst) const
{
    int i;
    for(i= 0; i < nbSample; i++)
    {
        pcmdst[0] = pcmsrc[0];
        pcmdst[1] = pcmsrc[0];
        pcmdst += 2;
        pcmsrc += 1;
    }
}

//=============================================================================
// ADECComponent::Convert8x32To2x16Channels
//  SE channel number output is equal to channel number input.
//  As android works only from PCM 16bit stereo, a conversion from 8 channel
//  32bits to 2 channel 16 bits is done
//=============================================================================
void ADECComponent::Convert8x32To2x16Channels(int nbSample, uint32_t* pcmsrc,
                                              uint16_t* pcmdst, int nbChannels) const
{
    int i ;
    if (1 == nbChannels)
    {
        for(i = 0; i < nbSample; i++)
        {
           // Keep only the mono channel. It must be properly downmixed to get all channels
           // Data produced is always 8 channel
           pcmdst[0] = (pcmsrc[3] >> 16) ;
           pcmsrc += 8;
           pcmdst += 1;
        }
    }
    else
    {
        for(i = 0; i < nbSample; i++)
        {
           // Keep only the stereo channels. They must be properly downmixed to get all channels
           // Data produced is always 8 channel
           pcmdst[0] = (pcmsrc[0] >> 16) ;
           pcmdst[1] = (pcmsrc[1] >> 16) ;
           pcmsrc += 8;
           pcmdst += 2;
        }
	}
}

//=====================================================================================
// ADECComponent::deConfigure
//  decoder de-configuration
//======================================================================================
OMX_ERRORTYPE ADECComponent::deConfigure()
{
    OMX_DBGT_PROLOG();
    // stop the current playback (if still playing)
    if(m_dvb) stop();

    // cleanup and delete video ports
    m_inputPort.destroy();
    m_outputPort.destroy();

    if (m_pPesAdapter) {
        delete m_pPesAdapter;
        m_pPesAdapter = 0;
    }
    OMX_DBGT_EPILOG();
    return OMX_ErrorNone;
}

//=====================================================================================
// ADECComponent::configure
//  decoder configuration according to codec
//======================================================================================
OMX_ERRORTYPE ADECComponent::configure()
{
    OMX_DBGT_PROLOG();

    OMX_AUDIO_CODINGTYPE cFmt = this->inputPort().definition().compressionFormat();
    int DvbColorFormat = 0;
#ifndef ANDROID
    if(!this->outputPort_().isTunneled())
    {
        OMX_DBGT_WARNING("We do not support NON tunneled mode");
    }

    // we just return back if dvbis already started
    if(m_dvb and this->dvb().isStarted())
    {
        return OMX_ErrorNone;
    }
#endif
    if (cFmt == OMX_AUDIO_CodingAAC)
    {
        OMX_DBGT_PINFO("%s Decode AAC: nSampleRate = %d,  nBitRate = %d, "
                   "nFrameLength = %d (ADECComponent::configure)",
                   name(),
                   this->inputPort().sampleRate(),
                   (int)this->inputPort().aacParams().nBitRate,
                   (int)this->inputPort().aacParams().nFrameLength);
        DvbColorFormat = AUDIO_ENCODING_AAC;
    }
    else if (cFmt == OMX_AUDIO_CodingPCM)
    {
        OMX_DBGT_PINFO("%s Decode PCM: nSampleRate = %u (ADECComponent::configure)",
                   name(),
                   this->inputPort().sampleRate());
        DvbColorFormat = AUDIO_ENCODING_PCM;
    }
    else if (cFmt == OMX_AUDIO_CodingMP3)
    {
        OMX_DBGT_PINFO("%s Decode MP3: nSampleRate = %u (ADECComponent::configure)",
                   name(),
                   this->inputPort().sampleRate());
        DvbColorFormat = AUDIO_ENCODING_MP3;
    }
    else if(cFmt == OMX_AUDIO_CodingVORBIS)
    {
        OMX_DBGT_PINFO("%s Decode VORBIS: nSampleRate = %d, nBitRate = %d (ADECComponent::configure)",
                   name(), this->inputPort().sampleRate(),
                   (int)this->inputPort().vorbisParams().nBitRate);
        DvbColorFormat = AUDIO_ENCODING_VORBIS;
    }
    else if(cFmt == OMX_AUDIO_CodingWMA)
    {
        OMX_DBGT_PINFO("%s Decode WMA: nSamplingRate = %d, nBitRate = %d (ADECComponent::configure)",
                   name(), this->inputPort().sampleRate(),
                   (int)this->inputPort().wmaParams().nBitRate);
        DvbColorFormat = AUDIO_ENCODING_WMA;
    }
    else if(cFmt == (OMX_AUDIO_CODINGTYPE)OMX_AUDIO_CodingAC3)
    {
        OMX_DBGT_PINFO("%s Decode AC3: nSampleRate = %u (ADECComponent::configure)",
                   name(), this->inputPort().sampleRate());
        DvbColorFormat = AUDIO_ENCODING_AC3;
    }
    else if(cFmt == (OMX_AUDIO_CODINGTYPE)OMX_AUDIO_CodingDTS)
    {
        OMX_DBGT_PINFO("%s Decode DTS: nSampleRate = %d (ADECComponent::configure)",
                   name(), this->inputPort().sampleRate());
        DvbColorFormat = AUDIO_ENCODING_DTS;
    }
    else if(cFmt == (OMX_AUDIO_CODINGTYPE)OMX_AUDIO_CodingADPCM)
    {
        OMX_DBGT_PINFO("%s Decode ADPCM: nSampleRate = %d (ADECComponent::configure)",
                   name(), this->inputPort().sampleRate());
        DvbColorFormat = AUDIO_ENCODING_IMA_ADPCM;
        OMX_DBGT_PINFO("%s mimeType", this->inputPort().definition().contentType());
        if(0 == strcmp(this->inputPort().definition().contentType(),"audio/raw-adpcm-ms"))
        {
            OMX_DBGT_PINFO("setting AUDIO_ENCODING_MS_ADPCM");
            DvbColorFormat = AUDIO_ENCODING_MS_ADPCM;
        }
    }
    else if(cFmt == (OMX_AUDIO_CODINGTYPE)OMX_AUDIO_CodingAMR)
    {
        CODEC_INIT_T initStatus;
        OMX_DBGT_PINFO("%s Decode AMR: nSampleRate = %d (ADECComponent::configure)",
                   name(), this->inputPort().sampleRate());

        DvbColorFormat = AUDIO_ENCODING_PRIVATE;

        //configure the SW dec Interface
        swDecInterface.codec_info_local_struct   = &amrInfo;
        swDecInterface.codec_config_local_struct = &amrConfig;
        swDecInterface.codec_state.first_time    = 1;

        amrConfig.concealment_on = AMR_ERROR_CONCEALMENT_OFF;
        amrConfig.noHeader       = 1;
        /* Set Memory preset */
        amrConfig.memory_preset  = 0;

        /* Set payload type */
        amrConfig.Payload_Format = 0x0; // for now else we take from param->eAMRFrameFormat
        amrConfig.Efr_on         = 0;

        initStatus = amr_init_decode_malloc(&swDecInterface);
        if(initStatus != INIT_OK)
        {
            OMX_DBGT_ERROR("OMX_ErrorInsufficientResources : %d", initStatus);
            return OMX_ErrorInsufficientResources;
        }
    }
    else if (cFmt == (OMX_AUDIO_CODINGTYPE)OMX_AUDIO_CodingMPEG)
    {
        OMX_DBGT_PINFO("%s Decode MPA: nSampleRate = %u (ADECComponent::configure)",
                   name(),
                   this->inputPort().sampleRate());
        DvbColorFormat = AUDIO_ENCODING_MPEG2;
    }
    else
    {
        OMX_DBGT_ERROR("Unsupported Audio encoding format (%d)", cFmt);
        OMX_DBGT_EPILOG();
        return OMX_ErrorUnsupportedSetting;
    }
    // Create the port threads
    if (this->inputPort_().start() != 0) {
        return OMX_ErrorInsufficientResources;
    }
    if (this->outputPort_().start() != 0) {
        return OMX_ErrorInsufficientResources;
    }

    if(m_dvb)
    {
        dvb().destroy();
        delete m_dvb;
        m_dvb=0;
    }
#ifndef ANDROID
        OMX_DBGT_PINFO("Creating DVBAudio_MOO object in tunnel mode");
        // As of date AUDIO_ENCODING_PRIVATE is set for AMR only.
        // May need modification if used for other S/W codecs
        m_dvb = new DVBAudio_MOO(DVB_DEVICE_MIXER,
                                 (DvbColorFormat == AUDIO_ENCODING_PRIVATE)?AUDIO_ENCODING_PCM:DvbColorFormat,
                                 this->name());
#else

    if(this->outputPort_().isTunneled()) {
        OMX_DBGT_PINFO("Creating DVBAudio_MOO object in tunnel mode");
        // As of date AUDIO_ENCODING_PRIVATE is set for AMR only.
        // May need modification if used for other S/W codecs
        m_dvb = new DVBAudio_MOO(DVB_DEVICE_MIXER,
                                 (DvbColorFormat == AUDIO_ENCODING_PRIVATE)?AUDIO_ENCODING_PCM:DvbColorFormat,
                                 this->name());
    }
    else {
        OMX_DBGT_PDEBUG("Creating DVBAudio_SF object in grabbing mode");
        m_dvb = new DVBAudio_SF("",
                                V4L_DEVICE,
                                DvbColorFormat,
                                this->outputPort().pcmParams().nSamplingRate,
                                this->outputPort().pcmParams().nChannels,
                                this->outputPort().pcmParams().nBitPerSample,
                                this->name());
    }
#endif
    if (!m_dvb) {
        OMX_DBGT_ERROR("Can not allocate new Audio device");
        OMX_DBGT_EPILOG();
        return OMX_ErrorInsufficientResources;
    }

    m_dvb->setEncoding((DvbColorFormat == AUDIO_ENCODING_PRIVATE)?AUDIO_ENCODING_PCM:DvbColorFormat);

    if (m_dvb->create()) {
        OMX_DBGT_ERROR("Can not create new Audio device");
        OMX_DBGT_EPILOG();
        return OMX_ErrorHardware;
    }

    m_pPesAdapter = AudioPesAdapter::create(DvbColorFormat, *this);
    if (!m_pPesAdapter) {
        OMX_DBGT_ERROR("%s::configure failed", name());
        OMX_DBGT_EPILOG();
        return OMX_ErrorUnsupportedSetting;
    }

    OMX_DBGT_PINFO("%s PCM params: nSamplingRate = %u, nChannels = %u, "
               "nBufferSize = %u, nBitPerSample = %u (ADECComponent::configure)",
               name(),
               this->outputPort().pcmParams().nSamplingRate,
               this->outputPort().pcmParams().nChannels,
               this->outputPort().definition().bufferSize(),
               this->outputPort().pcmParams().nBitPerSample);

    OMX_DBGT_EPILOG();
    return OMX_ErrorNone;
}

OMX_ERRORTYPE ADECComponent::start()
{
    int result = 0;

    OMX_DBGT_PROLOG();
    if(this->inputPort().definition().compressionFormat() == OMX_AUDIO_CodingAMR)
    {
        SAMPLE_STRUCT_T *sampleParam = &swDecInterface.sample_struct;
        STATE_STRUCT_T * codecstate  = &swDecInterface.codec_state;

        SWDecoderEnabled = true;

        memset(sampleParam, 0 , sizeof(SAMPLE_STRUCT_T));
        memset(codecstate, 0  , sizeof(STATE_STRUCT_T));

        //Configure the state structure
        codecstate->first_time    = true;  // set true for first time after that make it false
        codecstate->output_enable = true;  // always true
        codecstate->eof           = false; // set it to false and enable it for the last frame

        // rest will be populated by the ports.
        this->outputPort_().ConfigureSWDecoder();
        this->inputPort_().ConfigureSWDecoder();
    }

    if((this->inputPort().definition().compressionFormat() != OMX_AUDIO_CodingAMR) ||
        this->outputPort().isTunneled())
    {
        if (dvb().start() != 0) {
           OMX_DBGT_ERROR("::dvb.start %s failed (%s)", name(), strerror (errno));
           OMX_DBGT_EPILOG();
           return OMX_ErrorInsufficientResources;
        }
    }

    OMX_DBGT_EPILOG();
    return OMX_ErrorNone;
}

void ADECComponent::stop()
{
    int result = 0;
    OMX_DBGT_PROLOG();

    if(SWDecoderEnabled && !this->outputPort().isTunneled())
    {
        OMX_DBGT_EPILOG();
        return;
    }

    result = dvb().stop();
    if(result) {
        OMX_DBGT_ERROR("%s::stop failed", name());
    }

    OMX_DBGT_EPILOG();
}

 //=====================================================================================
// ADECComponent::deallocate
//  deallocate any S/W decoder allocated
//======================================================================================
void ADECComponent::deallocate()
{
    OMX_DBGT_PROLOG();
    if(SWDecoderEnabled)
    {
        amr_close_decode_malloc(&swDecInterface);
    }
    OMX_DBGT_EPILOG();
}

void ADECComponent::execute()
{
    OMX_DBGT_PROLOG();
    OMX_DBGT_EPILOG();
}

} // eof namespace stm

//=====================================================================================
// OMX_ComponentInit
// "C" interface to initialize a new OMX component: creation of the associated object
//======================================================================================

extern "C" OMX_ERRORTYPE OMX_ComponentInit (OMX_HANDLETYPE hComponent)
{
    return stm::OmxComponent::ComponentInit(new stm::ADECComponent(hComponent));
}

