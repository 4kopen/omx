/*
 * Copyright 2013 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file   AudioPesAdapter.cpp
 * @author STMicroelectronics
 */

#include "AudioPesAdapter.hpp"
#include "AudioDecoder.hpp"
#include "OMX_debug.h"
#include "pes.hpp"

#include <linux/dvb/stm_audio.h>
#include <malloc.h>

#define DBGT_PREFIX "ADEC"
#define DBGT_LAYER  0
#include <linux/dbgt.h>

namespace stm {

AudioPesAdapter::AudioPesAdapter(const ADECComponent& dec)
    :PesHeader(0),
     mIsLoggingEnabled(false),
     m_auddec(dec)
{
    PesHeader = (unsigned char*)malloc(MAX_PES_PACKET_SIZE);
    OMXDEBUG_UPDATE_AV_TRACE_SETTINGS(this->auddec().name());
}

AudioPesAdapter::~AudioPesAdapter()
{
    if (PesHeader) {
        free(PesHeader);
        PesHeader = 0;
    }
}

const char* AudioPesAdapter::name() const
{ return this->auddec().name(); }


AudioPesAdapter* AudioPesAdapter::create(int nCoding, const ADECComponent& aDec)
{
    AudioPesAdapter* adapt = 0;

    switch(nCoding) {
    case AUDIO_ENCODING_AAC: {
        adapt = new AacPesAdapter(aDec);
    }; break;

    case AUDIO_ENCODING_PCM: {
        adapt = new PcmPesAdapter(aDec);
    }; break;

    case AUDIO_ENCODING_MPEG2:
    case AUDIO_ENCODING_MP3: {
        adapt = new Mp3PesAdapter(aDec);
    }; break;
  
    case AUDIO_ENCODING_DTS: {
        adapt = new DtsPesAdapter(aDec);
    }; break;

    case AUDIO_ENCODING_AC3: {
        adapt = new Ac3PesAdapter(aDec);
    }; break;

    case AUDIO_ENCODING_WMA: {
        adapt = new WmaPesAdapter(aDec);
    }; break;

    case AUDIO_ENCODING_VORBIS: {
        adapt = new VorbisPesAdapter(aDec);
    }; break;

    case AUDIO_ENCODING_IMA_ADPCM:
    case AUDIO_ENCODING_MS_ADPCM: {
        adapt = new AdpcmPesAdapter(aDec);
    }; break;

    case AUDIO_ENCODING_PRIVATE: {
        adapt = new AmrPesAdapter(aDec);
    }; break;

    default: {
        DBGT_ERROR("Unsupported Audio format");
        adapt = 0;
    }; break;   
    }
    return adapt;
}

}
