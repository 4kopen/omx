/*
 * Copyright 2011 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *
 * File   ::    v4laudio.hpp
 * Author ::    Jean-Philippe FASSINO (jean-philippe.fassino@st.com)
 *
 *
 */


#ifndef ST_OMX_V4L_AUDIO_H
#define ST_OMX_V4L_AUDIO_H


class V4LAudio {
protected:
    bool mIsLoggingEnabled;

protected:
    int v4l_fd;
    const char* deviceName;
    int eos;   // Workaround seek backward after end of playback

public:
    V4LAudio(const char* _deviceName);

    int Open (int grabIdx);
    void Close();
// PATCH     int Configure();
    int Configure(uint32_t sampleRate, uint32_t channelCount, uint32_t bitsPerSample);
    int Start();
    int32_t flush(bool bEoS = false);
    inline void SetEos(int eosState) { eos = eosState; }; // Workaround seek backward after end of playback
    int Stop();
    int QueueBuffer(void* BufferData, int &nBytesSize, uint64_t &ts);
    const char* name() const;
};

#endif
