/*
 * Copyright 2011 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *
 * File   ::    adpm.cpp
 * Author ::    kausik MAITI (kausik.maiti@st.com)
 *
 *
 */

#include "AudioPesAdapter.hpp"
#include "AudioDecoder.hpp"
#include "OMX_debug.h"

#include "pes.hpp"
#include <stdio.h>

#define DBGT_PREFIX "ADPCM "
#define DBGT_LAYER  1
#include <linux/dbgt.h>

//#define AUD_ADPCM_INPUTBUFFER_DUMP
//#define ADPCM_DEBUG

#define PTS_CORRECTION(x) (((x) * 90000)/1000000)

namespace stm {

AdpcmPesAdapter::AdpcmPesAdapter(const ADECComponent& dec)
    :AudioPesAdapter(dec)
{
    memset(&AdpcmInfo, 0, sizeof(AdpcmInfo));
}

AdpcmPesAdapter::~AdpcmPesAdapter()
{
}

int AdpcmPesAdapter::emitCodecConfig(uint8_t* pBuffer,
                                   unsigned int nFilledLen) const
{
    int      err = 0;
    ADPCMInfo_t *AdpcmInfo_p = (ADPCMInfo_t *)pBuffer;

    OMX_DBGT_PROLOG("pBuf=%p, len=%lu", pBuffer, (unsigned long)nFilledLen);
    OMX_DBGT_ASSERT(nFilledLen <= sizeof(ADPCMInfo_t));

    OMX_DBGT_PINFO("nFormatTag %u, nBlockAlign %u, nSamplesPerBlock %u",
                    AdpcmInfo_p->nFormatTag, AdpcmInfo_p->nBlockAlign, AdpcmInfo_p->nSamplesPerBlock);
    memcpy(&AdpcmInfo, AdpcmInfo_p, nFilledLen);
    OMX_DBGT_EPILOG();
    return err;
}

int AdpcmPesAdapter::emitFrame(uint8_t* pBuffer,
                             unsigned int nFilledLen,
                             uint64_t nTimeStamp) const
{
    int err = 0, metaDataLength, pesHeaderLength;

    uint8_t* pOutBuffer = (uint8_t*)this->auddec().outputPort().buffer;
    OMX_U32 PesHeaderLength;
    uint64_t nPTS = PTS_CORRECTION(nTimeStamp);

    OMX_DBGT_PROLOG("pBuf=%p, len=%d, nTimeStamp=%llu", pBuffer, nFilledLen,
                nTimeStamp);
#ifdef AUD_ADPCM_INPUTBUFFER_DUMP
    {
        FILE *fd;
        fd = fopen("/data/adpcm_input.dat","ab+");
        if (!fd) {
            OMX_DBGT_ERROR(": Unable to open %s file.Data Dump discarded","adpcm_input.dat");
        }
        else{
            fwrite(pBuffer,1, nFilledLen,fd);
            fclose(fd);
        }
    }
#endif

    memset (PesHeader, 0, PES_MAX_HEADER_SIZE);

    uint32_t nSampleRate    = this->auddec().inputPort().adpcmParams().nSampleRate;
    uint32_t nChannels      = this->auddec().inputPort().adpcmParams().nChannels;
    uint32_t nBitsPerSample = this->auddec().inputPort().adpcmParams().nBitsPerSample;
    uint32_t nBlockAlign    = AdpcmInfo.nBlockAlign;
    uint32_t nSamplesPerBlock = AdpcmInfo.nSamplesPerBlock;
    uint32_t nCoefficients    = (AdpcmInfo.nFormatTag == WAVE_FORMAT_DVI_ADPCM)?
                                2:AdpcmInfo.nCoefficients;

    OMX_DBGT_PINFO("nBlockAlign %d, wsamplesPerBlock %d, nCoefficients %u",
                    nBlockAlign,  nSamplesPerBlock, nCoefficients);

    metaDataLength = InsertAdpcmPvtData(&PesHeader[PES_MIN_HEADER_SIZE+5],
                        nChannels, nSampleRate, nSamplesPerBlock, nCoefficients);

    if(nPTS > INVALID_PTS_VALUE)
    {
        //We will anyway overflow. So send no PTS
        nPTS = INVALID_PTS_VALUE;
    }

    pesHeaderLength = InsertPesHeader (PesHeader, (metaDataLength + nFilledLen),
                        PRIVATE_STREAM_1_PES_START_CODE,
                        nPTS, 0);
#ifdef ADPCM_DEBUG
    {
        int i = 0;
        char* LogPtr = (char *)malloc ((metaDataLength+pesHeaderLength) * 3 + 1);

        OMX_DBGT_PINFO ("%s ADPCM data header length %d",
                    this->name(), (pesHeaderLength + metaDataLength));
        for(i = 0; i < (pesHeaderLength + metaDataLength); i++)
        {
            sprintf (LogPtr+3*i, "%.2x ", PesHeader[i]);
        }
        OMX_DBGT_PINFO ("%s %s ",
                    this->name(), LogPtr);
        free (LogPtr);
    }
#endif

    err = this->auddec().dvb().write((unsigned char*)&PesHeader[0], (pesHeaderLength+metaDataLength));
    if(err)
    {
       OMX_DBGT_ERROR("%s::PCMEmitFrame->dvb().write pBuffer failed", name());
       goto error;
    }

    err = this->auddec().dvb().write(pBuffer, nFilledLen);
    if(err)
    {
       OMX_DBGT_ERROR("%s::PCMEmitFrame->dvb().write pBuffer failed", name());
       goto error;
    }

error:
    OMX_DBGT_EPILOG();
    return err;
}

} // eof namespace stm
