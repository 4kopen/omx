/*
 * Copyright 2011 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *
 * File   ::    aac.cpp
 * Author ::    Jean-Philippe FASSINO (jean-philippe.fassino@st.com)
 *
 *
 */

#include "AudioPesAdapter.hpp"
#include "AudioDecoder.hpp"
#include "OMX_debug.h"
#include "pes.hpp"

#if HAVE_ASSERT_H
#include <assert.h>
#endif

#if HAVE_DBGTASSERT_H
// Re-direct assert on DBGT_ASSERT macro
#define assert(expr)  OMX_DBGT_ASSERT(expr)
#endif

#define DBGT_PREFIX "AAC "
#define DBGT_LAYER  1
#include <linux/dbgt.h>

#define PTS_CORRECTION(x) (((x) * 90000)/1000000)

#define AAC_HEADER_LENGTH           7
#define AAC_SAMPLE_RATE_INDEX       13
#define AAC_CHANNELS_INDEX          8

namespace stm {

AacPesAdapter::AacPesAdapter(const ADECComponent& dec)
    :AudioPesAdapter(dec)
{}
  
AacPesAdapter::~AacPesAdapter()
{}

int AacPesAdapter::emitCodecConfig(uint8_t* pBuffer, unsigned int nFilledLen) const
{
    return 0;
}

int AacPesAdapter::emitFrame(uint8_t* pBuffer, unsigned int nFilledLen,
                                uint64_t nTimeStamp) const
{
    int err = -1;
    uint8_t  aac_header[AAC_HEADER_LENGTH]           = { 0xff, 0xf1, 0x00, 0x00, 0x00, 0x1f, 0xfc };
    uint32_t aac_sample_rates[AAC_SAMPLE_RATE_INDEX] = { 96000, 88200, 64000, 48000, 44100, 32000,
                                                         24000, 22050, 16000, 12000, 11025,  8000,  7350 };
    int32_t  profile         = 1;
    int32_t  sample_index    = 0x0F;
    int32_t  channel_count   = this->auddec().inputPort().channelNb();
    int32_t  aac_data_length;
    uint32_t PesHeaderLength;
    int i;
    bool bHeaderInBuffer;
    uint64_t nPTS = PTS_CORRECTION(nTimeStamp);

    for (i = 0; i < AAC_SAMPLE_RATE_INDEX; i++)
    {
        if (this->auddec().inputPort().sampleRate() == aac_sample_rates[i])
        {
            sample_index = i;
            break;
        }
    }

    if (sample_index == AAC_SAMPLE_RATE_INDEX)
    {
        OMX_DBGT_ERROR("%s::AACEmitFrame aacParams.nSampleRate (%u) not supported", name(), this->auddec().inputPort().aacParams().nSampleRate);
        goto ERROR;
    }


    switch (this->auddec().inputPort().aacParams().eAACStreamFormat)
    {
    case OMX_AUDIO_AACStreamFormatMP2ADTS:
    case OMX_AUDIO_AACStreamFormatMP4ADTS:
    case OMX_AUDIO_AACStreamFormatMP4LATM:
        // header is already located in source buffer
        aac_data_length = nFilledLen;
        bHeaderInBuffer=true;
        break;

    case OMX_AUDIO_AACStreamFormatMP4FF:
        // Header needs to be added
        aac_data_length = nFilledLen + AAC_HEADER_LENGTH;
        bHeaderInBuffer=false;
        break;

    case OMX_AUDIO_AACStreamFormatMP4LOAS:
    case OMX_AUDIO_AACStreamFormatADIF:
    case OMX_AUDIO_AACStreamFormatRAW:
        OMX_DBGT_ERROR("%s::AACEmitFrame AACStreamFormat (%d) not supported", name(), this->auddec().inputPort().aacParams().eAACStreamFormat);
        goto ERROR;
        break;

    default:
        OMX_DBGT_ERROR("%s::AACEmitFrame AACStreamFormat (%d) not recognized", name(), this->auddec().inputPort().aacParams().eAACStreamFormat);
        goto ERROR;
        break;
    }

    if(nPTS > INVALID_PTS_VALUE)
    {
        //We will anyway overflow. So send no PTS
        nPTS = INVALID_PTS_VALUE;
    }

    memset (PesHeader, 0, PES_MAX_HEADER_SIZE);
    PesHeaderLength = InsertPesHeader (PesHeader, aac_data_length, MPEG_AUDIO_PES_START_CODE, nPTS, 0);

    OMX_DBGT_PDEBUG("%s::AACEmitFrame (PesHeader)", name()) ;
    err = this->auddec().dvb().write(PesHeader, PesHeaderLength);
    if(err)
    {
       OMX_DBGT_ERROR("%s::AACEmitFrame->dvb.write(PesHeader) failed", name());
       goto ERROR;
    }

    if (nFilledLen == 0) goto ERROR;

    if (! bHeaderInBuffer)
    {
        // Create header
        aac_header[2]  = ((profile & 0x03) << 6)  | (sample_index << 2) | ((channel_count >> 2) & 0x01);
        aac_header[3]  = (channel_count & 0x03) << 6;
        aac_header[3] |= (aac_data_length >> 11) & 0x3;
        aac_header[4]  = (aac_data_length >> 3)  & 0xff;
        aac_header[5] |= (aac_data_length << 5)  & 0xe0;

        OMX_DBGT_PDEBUG("%s::AACEmitFrame (header)", name());
        err = this->auddec().dvb().write(aac_header, AAC_HEADER_LENGTH);
        if (err)
        {
           OMX_DBGT_ERROR("%s::AACEmitFrame->dvb.write(aac_header) failed", name());
           goto ERROR;
        }

        OMX_DBGT_PDEBUG("%s::AACEmitFrame (buffer)", name());
        err = this->auddec().dvb().write(pBuffer, nFilledLen);
        if (err)
        {
           OMX_DBGT_ERROR("%s::AACEmitFrame->dvb.write(buffer) failed", name());
           goto ERROR;
        }
    }
    else
    {
        // Write header part
        OMX_DBGT_PDEBUG("%s::AACEmitFrame (header from buffer)", name());
        err = this->auddec().dvb().write(pBuffer, AAC_HEADER_LENGTH);
        if (err)
        {
           OMX_DBGT_ERROR("%s::AACEmitFrame->dvb.write(header from buffer) failed", name());
           goto ERROR;
        }

        OMX_DBGT_PDEBUG("%s::AACEmitFrame (buffer)", name());
        err = this->auddec().dvb().write(pBuffer + AAC_HEADER_LENGTH, nFilledLen - AAC_HEADER_LENGTH);
        if (err)
        {
           OMX_DBGT_ERROR("%s::AACEmitFrame->dvb.write(buffer) failed", name());
           goto ERROR;
        }
    }


ERROR:
    OMX_DBGT_EPILOG();
    return err;
}

} // eof namespace stm
