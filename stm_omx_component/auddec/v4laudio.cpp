/*
 * Copyright 2011 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *
 * File   ::    v4laudio.cpp
 * Author ::    Jean-Philippe FASSINO (jean-philippe.fassino@st.com)
 *
 */
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <errno.h>
#include <stdio.h> // For file operations
#include "OMX_debug.h"
#include <linux/videodev2.h>
#include <linux/dvb/dvb_v4l2_export.h>
#include <linux/stm/stmedia_export.h>

#include <v4laudio.hpp>

#undef  DBGT_TAG
#define DBGT_TAG "V4LA"
#define DBGT_PREFIX "V4LA"
#define DBGT_LAYER  1
#define DBGT_VAR mDbgtVarV4LA
#define DBGT_DECLARE_AUTOVAR
#include <linux/dbgt.h>

#define MAX_INPUT_BUFFER_NUMBER 10     // number max of input buffers

#define V4L_S_INPUT                     "dvb0.audio%d"


//=====================================================================================
// V4LAudio::V4LAudio
//  Constructor 
//======================================================================================
V4LAudio::V4LAudio(const char* _deviceName):
    mIsLoggingEnabled(false),
    deviceName(_deviceName)
{
    DBGT_TRACE_INIT(adec);// Reuse ADEC trace property
    OMXDEBUG_UPDATE_AV_TRACE_SETTINGS(deviceName);
}

const char* V4LAudio::name() const
{ return deviceName; }

//=====================================================================================
// V4LAudio::Open
//  Open a V4L session: associate with DVB grabIdx  
//======================================================================================
int V4LAudio::Open(int grabIdx)
{
    struct v4l2_input  input;
    char inputEnum[32];

    OMX_DBGT_PROLOG();

    eos = false;
    /* Open the device */

    v4l_fd = open (deviceName, O_RDWR | O_NONBLOCK);


    if(v4l_fd < 0)
    {
        OMX_DBGT_ERROR("Unable to open V4L Device (%s)", strerror(errno));
        OMX_DBGT_EPILOG();
        return -1;
    }

    input.index = 0;
    sprintf(inputEnum, V4L_S_INPUT, grabIdx);

    while (ioctl (v4l_fd, VIDIOC_ENUMAUDIO, &input) == 0)
    {
        if(strcasecmp((char*)input.name, inputEnum) == 0)
        {
            if (ioctl (v4l_fd, VIDIOC_S_AUDIO, &input.index) == 0)
            {
                OMX_DBGT_PINFO("'%s' Grab Audio on V4L2 input '%s' (V4LAudio::Open)", deviceName, inputEnum);
                OMX_DBGT_EPILOG();
                return 0;
            }
        }

        input.index++;
    }

    OMX_DBGT_ERROR("V4L2 INPUT '%s' not found", inputEnum);
    OMX_DBGT_EPILOG();
    return -1;
}


//=====================================================================================
// V4LAudio::Close
//======================================================================================
void V4LAudio::Close()
{
    OMX_DBGT_PROLOG();
    close(v4l_fd);
    OMX_DBGT_EPILOG();
}


//=====================================================================================
// V4LAudio::Configure
// Configuration of V4L session
//======================================================================================
int V4LAudio::Configure(uint32_t sampleRate, uint32_t channelCount, uint32_t bitsPerSample)
{
    struct v4l2_format          Format;
    struct v4l2_buffer          Buffer;
    struct v4l2_requestbuffers  BufferRequest;
    struct v4l2_audio_format    *audio_fmt;

    OMX_DBGT_PROLOG();
    memset (&Format, 0, sizeof (struct v4l2_format));
    Format.type                         = V4L2_BUF_TYPE_AUDIO_CAPTURE;
    Format.fmt.pix.width                = 0;
    Format.fmt.pix.height               = 0;
    Format.fmt.pix.field                = V4L2_FIELD_ANY;
    Format.fmt.pix.pixelformat          = 0;
    Format.fmt.pix.bytesperline         = 0;
    audio_fmt                           = (struct v4l2_audio_format *)&Format.fmt.raw_data;
    audio_fmt->SampleRateHz             = sampleRate;
    audio_fmt->Channelcount             = channelCount;
    audio_fmt->BitsPerSample            = bitsPerSample;

    if (ioctl (v4l_fd, VIDIOC_S_FMT, &Format) < 0)
    {
        OMX_DBGT_ERROR("Set Format (VIDIOC_S_FMT) failed (%s)", strerror (errno));
        OMX_DBGT_EPILOG();
        return -1;
    }

    memset (&BufferRequest, 0, sizeof (struct v4l2_requestbuffers));
    BufferRequest.memory                = V4L2_MEMORY_USERPTR;
    BufferRequest.type                  = V4L2_BUF_TYPE_AUDIO_CAPTURE;
    BufferRequest.count                 = MAX_INPUT_BUFFER_NUMBER;
    if (ioctl (v4l_fd, VIDIOC_REQBUFS, &BufferRequest) < 0)
    {
        OMX_DBGT_ERROR("Buffer Request (VIDIOC_REQBUFS) failed (%s)", strerror (errno));
        OMX_DBGT_EPILOG();
        return -1;
    }

    OMX_DBGT_EPILOG();
    return 0;
}

//=====================================================================================
// V4LAudio::Start
//======================================================================================
int V4LAudio::Start()
{
    struct v4l2_buffer          Buffer;

    OMX_DBGT_PROLOG();
    memset (&Buffer, 0, sizeof (struct v4l2_buffer));
    Buffer.type                         = V4L2_BUF_TYPE_AUDIO_CAPTURE;
    if (ioctl (v4l_fd, VIDIOC_STREAMON, &Buffer.type) < 0)
    {
        OMX_DBGT_ERROR("Switching stream on (VIDIOC_STREAMON) failed (%s)", strerror (errno));
        OMX_DBGT_EPILOG();
        return -1;
    }

    OMX_DBGT_EPILOG();
    return 0;
}

//=====================================================================================
// V4LAudio::Flush
//======================================================================================
int32_t V4LAudio::Flush(bool bEoS)
{
    OMX_DBGT_PROLOG();

    if (Stop() != 0) {
        OMX_DBGT_ERROR("Error trying to stop\n");
        OMX_DBGT_EPILOG();
        return -1;
    }

    // Restore the initial stream state 
    if (Start() != 0) {
        OMX_DBGT_ERROR("Error trying to restart \n");
        OMX_DBGT_EPILOG();
        return -1;
    }

    OMX_DBGT_EPILOG();
    return 0;
}

//=====================================================================================
// V4LAudio::Stop
//======================================================================================
int V4LAudio::Stop()
{
    struct v4l2_buffer          Buffer;

    OMX_DBGT_PROLOG();
    memset (&Buffer, 0, sizeof (struct v4l2_buffer));
    Buffer.type                         = V4L2_BUF_TYPE_AUDIO_CAPTURE;
    if (ioctl (v4l_fd, VIDIOC_STREAMOFF, &Buffer.type) < 0)
    {
        OMX_DBGT_ERROR("Switching stream off (VIDIOC_STREAMOFF) failed (%s)", strerror (errno));
        OMX_DBGT_EPILOG();
        return -1;
    }

    OMX_DBGT_EPILOG();
    return 0;
}


//=====================================================================================
// V4LAudio::QueueBuffer
// send a empty buffer to SE and get back the buffer with a decoded frame
//======================================================================================
int V4LAudio::QueueBuffer (void* BufferData, int &nBytesSize, uint64_t &ts)
{
    struct v4l2_buffer  Buffer;

    OMX_DBGT_PROLOG();

    memset (&Buffer, 0, sizeof(Buffer));
    Buffer.type                 = V4L2_BUF_TYPE_AUDIO_CAPTURE;
    Buffer.memory               = V4L2_MEMORY_USERPTR;
    Buffer.m.userptr            = (unsigned long)BufferData;
    Buffer.field                = V4L2_FIELD_ANY;
    Buffer.length               = nBytesSize;
    if (ioctl (v4l_fd, VIDIOC_QBUF, &Buffer) < 0)
    {
        OMX_DBGT_ERROR("Buffer queue (VIDIOC_QBUF) failed (%s)", strerror (errno));
        OMX_DBGT_EPILOG();
        return -1;
    }

    Buffer.type                 = V4L2_BUF_TYPE_AUDIO_CAPTURE;
    Buffer.memory               = V4L2_MEMORY_USERPTR;
//    OMX_DBGT_PINFO("VIDIOC_DQBUF");

    int ret = ioctl (v4l_fd, VIDIOC_DQBUF, &Buffer);
    int counter = 0;
    int counter_EOS = 0;

// remove counter (300) to exit of the loop for the following case : end of playback (no more decoded buffer received)
// but sometime in this case the pulling thread is not blocked on outputPort->popAndLockCurrent() but on DQBUF
// util the stop closed the fd or a seek backward inject new buffer to decode
    while ((ret == -1) && (errno == EAGAIN)) {

      ret = ioctl (v4l_fd, VIDIOC_DQBUF, &Buffer);
      usleep(10000);
      counter++;
    }

    if (ret < 0)
    {
       if(errno == 19)
       {
          OMX_DBGT_PDEBUG("V4LAudio::QueueBuffer - EOS marker received");
          Buffer.bytesused = 0;
       }
       else
       {
         OMX_DBGT_ERROR("Failed to dequeue buffer (VIDIOC_DQBUF) %d %d (%s)",ret, errno, strerror (errno));
         OMX_DBGT_EPILOG();
         return -1;
       }
    }

    nBytesSize = Buffer.bytesused;
    ts = (uint64_t)Buffer.timestamp.tv_sec * 1000000 + (uint64_t)Buffer.timestamp.tv_usec;

    OMX_DBGT_EPILOG();
    return 0;
}
