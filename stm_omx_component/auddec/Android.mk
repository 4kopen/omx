LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)

LOCAL_SRC_FILES:= \
	AudioDecoderPort.cpp	    \
	AudioDecoder.cpp	    \
	AudioPesAdapter.cpp	    \
	dvbaudio_moo.cpp            \
	dvbaudio_sf.cpp             \
	aac.cpp                     \
	pcm.cpp                     \
	vorbis.cpp                  \
	ac3.cpp                     \
	wma.cpp                     \
	dts.cpp                     \
	mp3.cpp                     \
	amr.cpp                     \
        adpcm.cpp

# Includes for ICS
LOCAL_C_INCLUDES:= \
	$(TOP)/frameworks/base/include/media/stagefright/openmax

# Includes for JB
LOCAL_C_INCLUDES+= \
	$(TOP)/frameworks/native/include/media/openmax

LOCAL_LDFLAGS += \
               -Wl,--no-warn-shared-textrel

# Common includes
LOCAL_C_INCLUDES+= \
	$(TOP)/vendor/stm/hardware/omx/include/ \
	$(TOP)/vendor/stm/hardware/omx/stm_omx_component/common/ \
	$(TOP)/vendor/stm/hardware/omx/stm_omx_component/auddec/inc/ \
	$(TOP)/vendor/stm/hardware/omx/stm_omx_component/aud/ \
	$(TOP)/vendor/stm/hardware/omx/stm_amrnb/include/ \
	$(TOP)/vendor/stm/hardware/header/usr/include \
	$(TOP)/vendor/stm/module/stlinuxtv/linux/include \
	$(TOP)/vendor/stm/module/stlinuxtv

LOCAL_SHARED_LIBRARIES := \
        libdl \
        libOMX_Core \
        libcutils \
        liblog \
        libSTMOMXCommon

LOCAL_STATIC_LIBRARIES := prebuildAMR


LOCAL_CPPFLAGS += -DANDROID
LOCAL_CFLAGS   += -DDBGT_CONFIG_DEBUG -DDBGT_CONFIG_AUTOVAR -DDBGT_TAG=\"ADEC\" -Werror
LOCAL_CFLAGS   += -DHAVE_DBGTASSERT_H
LOCAL_CFLAGS   += -Wno-unused-parameter
LOCAL_MODULE:= libOMX.STM.Audio.Decoder
LOCAL_MODULE_TAGS := optional

include $(BUILD_SHARED_LIBRARY)
