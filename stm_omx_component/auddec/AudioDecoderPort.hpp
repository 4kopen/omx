/*
 * Copyright 2013 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file   AudioDecoderPort.hpp
 * @author STMicroelectronics
 */

#ifndef _STM_AUDIODECODERPORT_HPP_
#define _STM_AUDIODECODERPORT_HPP_

#include "StmOmxPort.hpp"
#include "StmOmxComponent.hpp"
#include "StmOmxVendorExt.hpp"
#include "AudioPortDefinition.hpp"
#include "stm_amr_decoder_if.h"

#include "Mutex.hpp"
#include "semaphore.h"

#include <OMX_Audio.h>
#include <OMX_AudioExt.h>
#include <OMX_IndexExt.h>

#include <pthread.h>


namespace stm {

// Forward declaration
class AudDecPort;
class AudDecOutputPort;
class AudDecInputPort;
class ADECComponent;

class AudDecPort: public OmxPort
{
public:
    /** Constructor */
    AudDecPort(unsigned int nIdx, OMX_DIRTYPE aDir,
               const ADECComponent& owner);

    /** Destructor */
    virtual ~AudDecPort();

    virtual const OmxComponent& component() const;
    virtual const AudioPortDefinition& definition() const;

    virtual void setWaitSem(sem_t  * waitSem);
    virtual void setTriggerSem(sem_t  * waitSem);

    virtual void ConfigureSWDecoder();
protected:
    virtual AudioPortDefinition& definition();

    const ADECComponent& auddec() const;


private:
    const ADECComponent&         m_owner;
    AudioPortDefinition          m_PortDefinition;

protected:
    sem_t                      * m_waitSem;
    sem_t                      * m_triggerSem;
};

inline const ADECComponent& AudDecPort::auddec() const
{ return m_owner; }

inline void AudDecPort::setWaitSem(sem_t  * waitSem)
{ m_waitSem = waitSem; }

inline void AudDecPort::setTriggerSem(sem_t  * triggerSem)
{ m_triggerSem = triggerSem; }

inline void AudDecPort::ConfigureSWDecoder()
{}

//==========================================================================
// AudDecOutputPort declaration
//==========================================================================

class AudDecOutputPort: public AudDecPort
{
public:
    /** Constructor */
    AudDecOutputPort(unsigned int nIdx, OMX_DIRTYPE aDir,
                     const ADECComponent& owner);

    /** Destructor */
    virtual ~AudDecOutputPort();

    virtual OMX_ERRORTYPE create();
    virtual void destroy();
    int start();
    int stop();

public:
    virtual OMX_ERRORTYPE
    getParameter(OMX_INDEXTYPE nIndex, OMX_PTR pParamStruct);

    virtual OMX_ERRORTYPE
    setParameter(OMX_INDEXTYPE nIndex, OMX_PTR pParamStruct);

    virtual OMX_ERRORTYPE
    useBuffer(OMX_BUFFERHEADERTYPE** ppBufferHdr,
              OMX_PTR pAppPrivate,
              OMX_U32 nSizeBytes,
              OMX_U8* pBuffer);

    virtual OMX_ERRORTYPE
    allocateBuffer(OMX_BUFFERHEADERTYPE** ppBufferHdr,
                   OMX_PTR pAppPrivate,
                   OMX_U32 nSizeBytes);

    virtual OMX_ERRORTYPE
    freeBuffer(OMX_BUFFERHEADERTYPE* pBufferHdr);

    virtual OMX_ERRORTYPE
    emptyThisBuffer(OMX_BUFFERHEADERTYPE* pBuffer);

    virtual OMX_ERRORTYPE
    fillThisBuffer(OMX_BUFFERHEADERTYPE* pBuffer);

    virtual OMX_ERRORTYPE flush();

    virtual void ConfigureSWDecoder();

    OMX_AUDIO_CODINGTYPE
    supportedCodingFormat(unsigned int nIdx) const;

    unsigned int
    supportedCodingFormatNb() const;

    bool isSupportedCodingFormat(OMX_AUDIO_CODINGTYPE aFmt) const;

    /** Change current port compression format in definition */
    void codingFormat(OMX_AUDIO_CODINGTYPE aFmt);

    void updateBufferSize(OMX_AUDIO_CODINGTYPE aFmt);

    void sampleRate(unsigned int nRate);

    const OMX_AUDIO_PARAM_PCMMODETYPE& pcmParams() const;

    unsigned int receivedFrame() const;
    static OMX_BUFFERHEADERTYPE* EOSBUF;

    uint32_t buffer[2048 * 8*2];  // 64KB for AAC - 128KB for vorbis
    unsigned int NbSamples;
protected:
    /**
     * This function serves as the entry point to the thread.
     * @param pthis the instance
     */
    static void* ThreadEntryPoint(void* pThis);

    /** The thread main function loop */
    void threadFunction();

private:
    /** Thread Id of the port thread */
    pthread_t puller_thread_id;
    unsigned int m_nReceivedFrame;
    OMX_AUDIO_PARAM_PCMMODETYPE m_pcmParams;
    Mutex           m_mutexThread;
};

inline void AudDecOutputPort::sampleRate(unsigned int nRate)
{ m_pcmParams.nSamplingRate = nRate; }

inline const OMX_AUDIO_PARAM_PCMMODETYPE& AudDecOutputPort::pcmParams() const
{ return m_pcmParams; }

inline unsigned int AudDecOutputPort::receivedFrame() const
{ return m_nReceivedFrame; }

//==========================================================================
// AudDecInputPort declaration
//==========================================================================

class AudDecInputPort: public AudDecPort
{
public:
    /** Constructor */
    AudDecInputPort(unsigned int nIdx, OMX_DIRTYPE aDir,
                    const ADECComponent& owner);

    /** Destructor */
    virtual ~AudDecInputPort();

    virtual OMX_ERRORTYPE create();
    virtual void destroy();
    int start();
    int stop();

public:
    virtual OMX_ERRORTYPE
    getParameter(OMX_INDEXTYPE nIndex, OMX_PTR pParamStruct);

    virtual OMX_ERRORTYPE
    setParameter(OMX_INDEXTYPE nIndex, OMX_PTR pParamStruct);

    virtual OMX_ERRORTYPE
    useBuffer(OMX_BUFFERHEADERTYPE** ppBufferHdr,
              OMX_PTR pAppPrivate,
              OMX_U32 nSizeBytes,
              OMX_U8* pBuffer);

    virtual OMX_ERRORTYPE
    allocateBuffer(OMX_BUFFERHEADERTYPE** ppBufferHdr,
                   OMX_PTR pAppPrivate,
                   OMX_U32 nSizeBytes);

    virtual OMX_ERRORTYPE
    freeBuffer(OMX_BUFFERHEADERTYPE* pBufferHdr);

    virtual OMX_ERRORTYPE
    emptyThisBuffer(OMX_BUFFERHEADERTYPE* pBuffer);

    virtual OMX_ERRORTYPE
    fillThisBuffer(OMX_BUFFERHEADERTYPE* pBuffer);

    virtual OMX_ERRORTYPE flush();
    virtual OMX_ERRORTYPE pause();
    virtual OMX_ERRORTYPE resume();

    OMX_AUDIO_CODINGTYPE
    supportedCodingFormat(unsigned int nIdx) const;

    unsigned int
    supportedCodingFormatNb() const;

    bool isSupportedCodingFormat(OMX_AUDIO_CODINGTYPE aFmt) const;

    /** Change current port compression format in definition */
    void codingFormat(OMX_AUDIO_CODINGTYPE aFmt);

    unsigned int channelNb() const;
    unsigned int sampleRate() const;

    const OMX_AUDIO_PARAM_AACPROFILETYPE&
    aacParams() const;

    const OMX_AUDIO_PARAM_PCMMODETYPE&
    pcmParams() const;

    const OMX_AUDIO_PARAM_MP3TYPE&
    mp3Params() const;

    const OMX_AUDIO_PARAM_MPATYPE&
    mpaParams() const;

    const OMX_AUDIO_PARAM_VORBISTYPE&
    vorbisParams() const;

    const OMX_AUDIO_PARAM_WMATYPE&
    wmaParams() const;

    const OMX_AUDIO_PARAM_AC3TYPE&
    ac3Params() const;

    const OMX_AUDIO_PARAM_DTSTYPE&
    dtsParams() const;

    const OMX_AUDIO_PARAM_AMRTYPE&
    amrParams();

    const OMX_AUDIO_PARAM_ADPCMTYPE&
    adpcmParams() const;

    unsigned int sendedFrame() const;

    virtual void ConfigureSWDecoder();

protected:
    /**
     * This function serves as the entry point to the thread.
     * @param pthis the instance
     */
    static void* ThreadEntryPoint(void* pThis);

    /** The thread main function loop */
    void threadFunction();

private:
    typedef enum  {
        EThreadState_Stopped  = 0,
        EThreadState_Running,
        EThreadState_Paused,
    } EThreadState;

    EThreadState threadState() const;
    void threadState(EThreadState st);

    int decodeFrame(unsigned int nOmxFlags,
                    uint8_t* pBuf, unsigned int nLen,
                    uint64_t nTimeStamp);
private:
    static OMX_BUFFERHEADERTYPE* EOSBUF;
    static const int  m_codingFormats[];

    //Thread Id of the port thread
    pthread_t pusher_thread_id;
    EThreadState    m_nThreadState;
    Mutex           m_mutexThread;
    Condition       m_condState;

    unsigned int    m_nSendedFrame;

    OMX_AUDIO_PARAM_AACPROFILETYPE m_aacParams;
    OMX_AUDIO_PARAM_PCMMODETYPE m_pcmParams;
    OMX_AUDIO_PARAM_MP3TYPE        m_mp3Params;
    OMX_AUDIO_PARAM_MPATYPE        m_mpaParams;
    OMX_AUDIO_PARAM_VORBISTYPE     m_vorbisParams;
    OMX_AUDIO_PARAM_WMATYPE        m_wmaParams;
    OMX_AUDIO_PARAM_AC3TYPE        m_ac3Params;
    OMX_AUDIO_PARAM_DTSTYPE        m_dtsParams;
    OMX_AUDIO_PARAM_AMRTYPE        m_amrParams;
    OMX_AUDIO_PARAM_ADPCMTYPE      m_adpcmParams;
};

inline unsigned int AudDecInputPort::sendedFrame() const
{ return m_nSendedFrame; }

inline const OMX_AUDIO_PARAM_AACPROFILETYPE&
AudDecInputPort::aacParams() const
{ return m_aacParams; }

inline const OMX_AUDIO_PARAM_PCMMODETYPE&
AudDecInputPort::pcmParams() const
{ return m_pcmParams; }

inline const OMX_AUDIO_PARAM_MP3TYPE&
AudDecInputPort::mp3Params() const
{ return m_mp3Params; }

inline const OMX_AUDIO_PARAM_MPATYPE&
AudDecInputPort::mpaParams() const
{ return m_mpaParams; }

inline const OMX_AUDIO_PARAM_VORBISTYPE&
AudDecInputPort::vorbisParams() const
{ return m_vorbisParams; }

inline const OMX_AUDIO_PARAM_WMATYPE&
AudDecInputPort::wmaParams() const
{ return m_wmaParams; }

inline const OMX_AUDIO_PARAM_AC3TYPE&
AudDecInputPort::ac3Params() const
{ return m_ac3Params; }

inline const OMX_AUDIO_PARAM_DTSTYPE&
AudDecInputPort::dtsParams() const
{ return m_dtsParams; }

inline const OMX_AUDIO_PARAM_AMRTYPE&
AudDecInputPort::amrParams()
{ return m_amrParams; }

inline const OMX_AUDIO_PARAM_ADPCMTYPE&
AudDecInputPort::adpcmParams() const
{ return m_adpcmParams; }

} // eof namespace stm

#endif  // _STM_AUDIODECODERPORT_HPP_
