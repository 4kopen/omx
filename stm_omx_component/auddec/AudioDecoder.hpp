/*
 * Copyright 2011 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *
 * File	  ::	AudioDecoder.hpp
 * Author ::	Jean-Philippe Fassino (jean-philippe.fassino@st.com)
 *
 *
 */

#ifndef _STM_AUDIODECODER_HPP_
#define _STM_AUDIODECODER_HPP_

#include <stdio.h>
#include <string.h>
#include "StmOmxComponent.hpp"
#include "AudioDecoderPort.hpp"
#include "Role.hpp"
#include "dvbaudio_moo.hpp"
#include "dvbaudio_sf.hpp"
#include "semaphore.h"
#include "stm_amr_decoder_if.h"
#include "link.hpp"

namespace stm {

//Forward declaration
class AudioPesAdapter;

class ADECComponent: public OmxComponent
{
public:
    ADECComponent(OMX_HANDLETYPE hComponent);
    virtual ~ADECComponent();

    virtual OMX_ERRORTYPE create();
    virtual void destroy();
    virtual const AudDecPort& getPort(unsigned int nIdx) const;

    friend class AudDecInputPort;
    friend class AudDecOutputPort;

    virtual OMX_ERRORTYPE getParameter(
            OMX_INDEXTYPE       nParamIndex,
            OMX_PTR             ComponentParameterStructure);
    virtual OMX_ERRORTYPE setParameter(
            OMX_INDEXTYPE       nParamIndex,
            OMX_PTR             ComponentParameterStructure);
    virtual
    OMX_ERRORTYPE getConfig(OMX_INDEXTYPE nIndex, OMX_PTR pConfStruct);
    virtual
    OMX_ERRORTYPE setConfig(OMX_INDEXTYPE nIndex, OMX_PTR pConfStruct);
    virtual OMX_ERRORTYPE getExtensionIndex(
                  OMX_STRING          cParameterName,
                  OMX_INDEXTYPE *     pIndexType);
    virtual OMX_ERRORTYPE emptyThisBuffer(OMX_BUFFERHEADERTYPE*   pBufferHdr);
    virtual OMX_ERRORTYPE fillThisBuffer(OMX_BUFFERHEADERTYPE*   pBufferHdr);

    const AudDecInputPort& inputPort() const;
    const AudDecOutputPort& outputPort() const;
    const IDvbDevice& dvb() const;

protected:
    virtual AudDecPort& getPort(unsigned int nIdx);

    virtual OMX_ERRORTYPE configure();
    virtual OMX_ERRORTYPE deConfigure();
    virtual OMX_ERRORTYPE start();
    virtual void stop();
    virtual void execute();
    virtual void deallocate();
    virtual OMX_ERRORTYPE flush(unsigned int nPortIdx);

    OMX_ERRORTYPE setRole(const char* pNewRole);

private:
    AudDecInputPort& inputPort_();
    AudDecOutputPort& outputPort_();

    Role& role();
    AudioPesAdapter* pesAdapter() const;
    IDvbDevice& dvb();

private:
    void Convert8x32To2x16Channels(int nbSample,uint32_t* pcmsrc,uint16_t* pcmdst, int nbChannels) const;
    void Convert1x16To2x16Channels(int nbSample,uint16_t* pcmsrc,uint16_t* pcmdst) const;
    IDvbDevice*          m_dvb;
    AudDecInputPort      m_inputPort;
    AudDecOutputPort     m_outputPort;

    Role                 m_componentRole;
    AudioPesAdapter*     m_pPesAdapter;
public:
    mutable CODEC_INTERFACE_T      swDecInterface;
    AMR_DECODER_CONFIG_STRUCT_T    amrConfig;
    AMR_DECODER_INFO_STRUCT_T      amrInfo;
    // Semaphore to sync S/W decoder
    sem_t inputPortSem;
    sem_t outputPortSem;
    bool SWDecoderEnabled;
    mutable uint64_t nTS;
    mutable uint32_t nDecodedSamples;
    OMX_HANDLETYPE   hTunnelComp;
    mutable struct timeMapping_s   *timeMapping_p;
    mutable struct timeMapping_s   *overflowTimeMapping_p;
    mutable Mutex m_mutexCtrl;
};


inline const IDvbDevice& ADECComponent::dvb() const
 { return *m_dvb; }

inline IDvbDevice& ADECComponent::dvb()
 { return *m_dvb; }

inline const AudDecInputPort& ADECComponent::inputPort() const
{ return m_inputPort; }

inline const AudDecOutputPort& ADECComponent::outputPort() const
{ return m_outputPort; }

inline AudDecInputPort& ADECComponent::inputPort_()
{ return m_inputPort; }

inline AudDecOutputPort& ADECComponent::outputPort_()
{ return m_outputPort; }

inline Role&  ADECComponent::role()
{ return m_componentRole; }

inline AudioPesAdapter* ADECComponent::pesAdapter() const
{ return m_pPesAdapter; }

#define OMX_ST_AUDIO_INPUT_PORT 0
#define OMX_ST_AUDIO_OUTPUT_PORT 1
#define OMX_ST_AUDIO_PORT_NUMBER 2


} // eof namespace stm

#endif  // _STM_AUDIODECODER_HPP_
