/*
 * Copyright 2013 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file   dvbaudio_sf.cpp
 * @author STMicroelectronics
 */

#include "dvbaudio_sf.hpp"
#include "OMX_debug.h"
#include <sys/ioctl.h>
#include <sys/types.h>
#include <fcntl.h>
#include <errno.h>
#include <stdio.h> // For file operations
#include <unistd.h>

#include <linux/dvb/audio.h>
#include <linux/dvb/stm_dvb.h>
#define ENABLE_TEMPORARY_ENTRY_POINTS
#include <linux/dvb/stm_audio.h>
#include <linux/videodev2.h>
#include <linux/dvb/dvb_v4l2_export.h>
#include <utils/v4l2_helper.h>
#include <linux/stm/stmedia_export.h>

#if HAVE_ASSERT_H
#include <assert.h>
#endif

#if HAVE_DBGTASSERT_H
// Re-direct assert on DBGT_ASSERT macro
#define assert(expr)  OMX_DBGT_ASSERT(expr)
#endif

#undef  DBGT_TAG
#define DBGT_TAG "DVBAS"
#define DBGT_PREFIX "DVBAS"
#define DBGT_LAYER  1
#define DBGT_VAR mDbgtVarDVBAS
#define DBGT_DECLARE_AUTOVAR
#include <linux/dbgt.h>


#define MIN_DVB_GRAB_DEVICE 3
#define MAX_DVB_GRAB_DEVICE 7

#define MAX_INPUT_BUFFER_NUMBER 10     // number max of input buffers

#define V4L_S_INPUT                     "dvb0.audio%d"

#define V4L2_AUDIO_DRIVER_NAME    "AV Decoder"
#define V4L2_AUDIO_CARD_NAME      "STMicroelectronics"

DVBAudio_SF::DVBAudio_SF(const char* aDeviceName, const char* aDeviceNamev4l, int nEncoding, unsigned int nSampleRate, unsigned int nChannels, unsigned int nBitPerSample, const char *cName)
    :grabIdx(-1),
     mIsLoggingEnabled(false),
     adev_id(-1),
     m_nEncoding(nEncoding),
     m_nSampleRate(nSampleRate),
     m_nChannels(nChannels),
     m_nBitPerSample(nBitPerSample),
     v4l_fd(0),
     eos(0),
     m_nState(EState_Stop),
     m_mutexCmd(),
     m_condState(),
     m_bClearWrite(false),
     m_bFlushWrite(false)
{
    DBGT_TRACE_INIT(adec); // Reuse ADEC trace property
    //check the string length before copy
    assert((strlen(aDeviceName) <= sizeof(m_deviceName)) && (strlen(aDeviceNamev4l) <= sizeof(m_deviceNamev4l))) ;
    strcpy(m_deviceName, aDeviceName);
    strcpy(m_deviceNamev4l, aDeviceNamev4l);
    OMXDEBUG_UPDATE_AV_TRACE_SETTINGS(cName);
}

DVBAudio_SF::~DVBAudio_SF()
{}

int DVBAudio_SF::id() const
{ return adev_id; }

const char* DVBAudio_SF::name() const
{ return m_deviceName; }

const char* DVBAudio_SF::v4lname() const
{ return m_deviceNamev4l; }

stm::IDvbDevice::EState DVBAudio_SF::state() const
{ return m_nState; }

void DVBAudio_SF::state(stm::IDvbDevice::EState st) const
{
    m_nState = st;
    m_condState.signal();
}

//=====================================================================================
// DVBAudio_SF::Instantiate
// Instanciation of SE decoder
//=====================================================================================
 int DVBAudio_SF::Instantiate()
{
    OMX_DBGT_PROLOG("Instantiate DVB audio device");
    if(name()[0] == 0)
    {
        for(grabIdx = MIN_DVB_GRAB_DEVICE; grabIdx <= MAX_DVB_GRAB_DEVICE; grabIdx++)
        {
            sprintf((char *)name(), DVB_DEVICE_GRAB, grabIdx);
            adev_id = open(name(), O_RDWR);
            if(adev_id >= 0)
                break;
        }

        if(adev_id < 0)
        {
            OMX_DBGT_ERROR("No more available device for audio grab (%s)", strerror (errno));
            OMX_DBGT_EPILOG();
            return -1;
        }
    }
    else
    {
        adev_id = open(name(), O_RDWR);

        if(adev_id < 0){
            OMX_DBGT_ERROR("Unable to open Device %s (%s)", name(), strerror (errno));
            OMX_DBGT_EPILOG();
            return -1;
        }
    }

    OMX_DBGT_PINFO("'%s'  Configure DVB audio", name());

    if (ioctl (adev_id, AUDIO_SELECT_SOURCE, (void*)AUDIO_SOURCE_MEMORY) != 0)
    {
        OMX_DBGT_ERROR("AUDIO_SELECT_SOURCE ioctl failed (%s)", strerror (errno));
        OMX_DBGT_EPILOG("Unable to configure device");
        return -1;
    }

    if (ioctl (adev_id, AUDIO_SET_STREAMTYPE ,(void*)STREAM_TYPE_PES) != 0)
    {
        OMX_DBGT_ERROR("AUDIO_SET_STREAMTYPE ioctl failed (%s)", strerror (errno));
        OMX_DBGT_EPILOG("Unable to configure device");
        return -1;
    }

    // From SDK2 05.95 need to request no AVSync to work with v4l2: this avoids using step
    if (ioctl (adev_id, AUDIO_SET_AV_SYNC ,0) != 0)
    {
        OMX_DBGT_ERROR("AUDIO_SET_AV_SYNC ioctl failed (%s)", strerror (errno));
        OMX_DBGT_EPILOG();
        return -1;
    }

    OMX_DBGT_EPILOG();
    return 0;
}

//=====================================================================================
// DVBAudio_SF::SetEncoding
//=====================================================================================
int DVBAudio_SF::SetEncoding(int encoding)
{
    OMX_DBGT_PROLOG();

    OMX_DBGT_PINFO("'%s' (DVBAudio_SF::SetEncoding)", name());

    if (ioctl (adev_id, AUDIO_SET_ENCODING, (void*)encoding) != 0)
    {
        OMX_DBGT_ERROR("AUDIO_SET_ENCODING ioctl failed (%s)", strerror (errno));
        OMX_DBGT_EPILOG("Unable to configure device");
        return -1;
    }

    OMX_DBGT_EPILOG();
    return 0;
}



int DVBAudio_SF::create()
{
    OMX_DBGT_PROLOG("Creating DVB audio device");
    if (Instantiate() != 0) {
        OMX_DBGT_ERROR("DVB AUDIO DEVICE Instanciate failed (%s)", strerror (errno));
        OMX_DBGT_EPILOG("Unable to instanciate dvb device");
        return -1;
    }
    if (v4lOpen(grabIdx) != 0) {
        OMX_DBGT_ERROR("V4L AUDIO DEVICE failed (%s)", strerror (errno));
        OMX_DBGT_EPILOG("Unable to open V4l device");
        return -1;
    }

    if (v4lConfigure(m_nSampleRate, m_nChannels, m_nBitPerSample) != 0) {
        OMX_DBGT_ERROR("V4L AUDIO DEVICE Configure failed (%s)", strerror (errno));
        OMX_DBGT_EPILOG("Unable to configure V4l device");
        return -1;
    }

    if (SetEncoding(m_nEncoding) != 0) {
        OMX_DBGT_ERROR("DVB AUDIO DEVICE SetEncoding failed (%s)", strerror (errno));
        OMX_DBGT_EPILOG("Unable to set encoding type");
        return -1;
    }
    OMX_DBGT_EPILOG("Audio devices (v4l/dvb) created");
    return 0;
}

//=====================================================================================
// DVBAudio_SF::destroy
//=====================================================================================
int DVBAudio_SF::destroy()
{
    OMX_DBGT_PROLOG();
    if (close(id()) != 0) {
        OMX_DBGT_ERROR("Audio close failed (%s)", strerror (errno));
        OMX_DBGT_EPILOG("Unable to close device");
        return -1;
    }

    //close v4l device
    close(v4l_fd);

    adev_id = -1;
    v4l_fd = -1;
    OMX_DBGT_EPILOG();
    return 0;
}

//=====================================================================================
// DVBAudio_SF::isOpen
//=====================================================================================
bool DVBAudio_SF::isOpen() const
{ return (id() != -1); }

int DVBAudio_SF::start() const
{
    OMX_DBGT_PROLOG("Starting device Audio");
    struct v4l2_buffer          Buffer;
    struct audio_command command;
    assert(isOpen());
    if (this->isStarted()) {
        OMX_DBGT_EPILOG("Already started");
        return 0;
    }

    if (ioctl (id(), AUDIO_STREAM_DOWNMIX , m_nChannels) != 0)
    {
        OMX_DBGT_ERROR("AUDIO_STREAM_DOWNMIX (%s)", strerror (errno));
        OMX_DBGT_EPILOG();
        return -1;
    }

    memset(&command, 0, sizeof(command));
    command.cmd      = AUDIO_CMD_SET_OPTION;
    command.u.option.option = DVB_OPTION_CTRL_REDUCE_COLLATED_DATA;
    command.u.option.value  = DVB_OPTION_VALUE_ENABLE;

    if (ioctl (id(), AUDIO_COMMAND ,&command) != 0)
    {
        OMX_DBGT_ERROR("AUDIO_COMMAND REDUCE_COLLATED_DATA (%s)", strerror (errno));
        OMX_DBGT_EPILOG();
        return -1;
    }

    if (ioctl (id(), AUDIO_PLAY, NULL) != 0) {
        OMX_DBGT_ERROR("AUDIO_PLAY ioctl failed (%s)", strerror (errno));
        OMX_DBGT_EPILOG("Unable to start device");
        return -1;
    }

    // Setting of specifical AAC configuration parameters
    if (m_nEncoding == AUDIO_ENCODING_AAC)
    {
        OMX_DBGT_PDEBUG("Specific AAC configuration parameters");
        audio_mpeg4aac_t audio_mpeg4aac;
        audio_mpeg4aac.aac_profile    = AUDIO_AAC_LC_TS_PROFILE ;
        audio_mpeg4aac.sbr_enable     = true;
        audio_mpeg4aac.sbr_96k_enable = false;
        audio_mpeg4aac.ps_enable      = false;

        if (ioctl (id(), AUDIO_SET_AAC_DECODER_CONFIG, (void*)&audio_mpeg4aac) != 0)
        {
            OMX_DBGT_ERROR("DVBAudio_SF::start AUDIO_SET_AAC_DECODER_CONFIG ioctl failed (%s)",
                       strerror (errno));
            OMX_DBGT_EPILOG("Unable to start device");
            return -1;
        }
    }

    //v4l start
    memset (&Buffer, 0, sizeof (struct v4l2_buffer));
    Buffer.type                         = V4L2_BUF_TYPE_AUDIO_CAPTURE;
    if (ioctl (v4l_fd, VIDIOC_STREAMON, &Buffer.type) < 0)
    {
        OMX_DBGT_ERROR("Switching stream on (VIDIOC_STREAMON) failed (%s)", strerror (errno));
        OMX_DBGT_EPILOG();
        return -1;
    }

    this->state(stm::IDvbDevice::EState_Start);

    OMX_DBGT_EPILOG();
    return 0;
}

//=====================================================================================
// DVBAudio_SF::stop
//=====================================================================================
int DVBAudio_SF::stop() const
{
    OMX_DBGT_PROLOG();
    assert(isOpen());

    struct v4l2_buffer          Buffer;
    if (this->isStopped()) {
        OMX_DBGT_EPILOG("Already stopped");
        return 0;
    }
    if (ioctl (id(), AUDIO_STOP, NULL) != 0) {
        OMX_DBGT_ERROR("AUDIO_STOP ioctl failed (%s)", strerror (errno));
        OMX_DBGT_EPILOG("Unable to stop device");
        return -1;
    }

    memset (&Buffer, 0, sizeof (struct v4l2_buffer));
    Buffer.type                         = V4L2_BUF_TYPE_AUDIO_CAPTURE;
    if (ioctl (v4l_fd, VIDIOC_STREAMOFF, &Buffer.type) < 0)
    {
        OMX_DBGT_ERROR("Switching stream off (VIDIOC_STREAMOFF) failed (%s)", strerror (errno));
        OMX_DBGT_EPILOG();
        return -1;
    }

    this->state(stm::IDvbDevice::EState_Stop);

    OMX_DBGT_EPILOG();
    return 0;
}

//=====================================================================================
// DVBAudio_SF::pause
//=====================================================================================
int DVBAudio_SF::pause(bool hw) const
{
    OMX_DBGT_PROLOG();
    assert(isOpen());
    if (this->isStopped()) {
        OMX_DBGT_WARNING("Invalid state change");
        OMX_DBGT_EPILOG("Can not pause a stopped device");
        return 0;
    }
    if (this->isPaused()) {
        OMX_DBGT_EPILOG("Already paused");
        return 0;
    }
    if (hw)
    if (ioctl(id(), AUDIO_PAUSE, NULL) != 0) {
        OMX_DBGT_ERROR("AUDIO_PAUSE ioctl failed (%s)", strerror (errno));
        OMX_DBGT_EPILOG("Unable to pause device");
        return -1;
    }
    this->state(stm::IDvbDevice::EState_Pause);
    OMX_DBGT_EPILOG();
    return 0;
}

//=====================================================================================
// DVBAudio_SF::resume
//=====================================================================================
int DVBAudio_SF::resume(bool hw) const
{
    OMX_DBGT_PROLOG();
    assert(isOpen());
    if (this->isStopped()) {
        OMX_DBGT_WARNING("Invalid state change");
        OMX_DBGT_EPILOG("Can not resume a stopped device");
        return 0;
    }
    if (!this->isPaused()) {
        OMX_DBGT_EPILOG("Already resumed");
        return 0;
    }
    if (hw)
    if (ioctl(id(), AUDIO_PLAY, NULL) != 0) {
        OMX_DBGT_ERROR("AUDIO_CONTINUE ioctl failed (%s)", strerror (errno));
        OMX_DBGT_EPILOG("Unable to resume device");
        return -1;
    }
    this->state(stm::IDvbDevice::EState_Start);
    OMX_DBGT_EPILOG();
    return 0;
}

//=====================================================================================
// DVBAudio_SF::lock
//=====================================================================================
void DVBAudio_SF::lock() const
{
}

//=====================================================================================
// DVBAudio_SF::unlock
//=====================================================================================
void DVBAudio_SF::unlock() const
{
}

//=====================================================================================
// DVBAudio_SF::resetflags
//=====================================================================================
void DVBAudio_SF::resetflags() const
{
    OMX_DBGT_PROLOG();

    m_bClearWrite = false;
    m_bFlushWrite = false;

    OMX_DBGT_EPILOG();
}

//=====================================================================================
// DVBAudio_SF::write
//=====================================================================================
int DVBAudio_SF::write(const void* data_ptr, unsigned int len) const
{
    OMX_DBGT_PROLOG("Injecting data in DVB Audio");
    assert(isOpen());

    // Block request if in pause state
    while (this->state() == EState_Pause) {
        OMX_DBGT_PINFO("Blocking write on a paused device");
        m_condState.wait(m_mutexCmd);
        // Check if device was cleared in the meantime
        if (m_bClearWrite == true || m_bFlushWrite == true) {
           OMX_DBGT_EPILOG("Data cleared");
            return -ECANCELED;
        }
    }

    if (this->isStopped()) {
        OMX_DBGT_WARNING("Writing on a stopped device");
        OMX_DBGT_EPILOG("Data skipped");
        return -EAGAIN;
    }

    int retval = ::write(id(), data_ptr, (size_t)len);
    if (retval < 0) {
        OMX_DBGT_ERROR("Error when writing in audio device (%s)", strerror(errno));
        OMX_DBGT_EPILOG("Write error");
        return -errno;
    }
    assert(retval >= 0);
    if (len != (unsigned int)retval) {
        OMX_DBGT_ERROR("Error writing Audio content write=%lu != written=%d",
                   (unsigned long)len, retval);
        OMX_DBGT_EPILOG("Write error");
        return -errno;
    }
    OMX_DBGT_EPILOG();
    return 0;
}

//=====================================================================================
// DVBAudio_SF::flush
//=====================================================================================
int DVBAudio_SF::flush(bool bEoS) const
{
    OMX_DBGT_PROLOG();
    assert(isOpen());
    if (this->isStopped()) {
        OMX_DBGT_EPILOG("Flushing a stopped device");
        return 0;
    }
    if (bEoS) {
        if (ioctl(id(), AUDIO_DISCONTINUITY, (void*)DVB_DISCONTINUITY_EOS) != 0) {
            OMX_DBGT_ERROR("AUDIO_DISCONTINUITY ioctl failed (%s)", strerror (errno));
            OMX_DBGT_EPILOG();
            return -1;
        }
    } else {
        if (ioctl(id(), AUDIO_FLUSH, NULL) != 0) {
            OMX_DBGT_ERROR("AUDIO_FLUSH ioctl failed (%s)", strerror (errno));
            OMX_DBGT_EPILOG();
            return -1;
        }
    }
    // Trigger state with same value to unblock any write
    m_bFlushWrite = true;
    this->state(this->state());
    OMX_DBGT_EPILOG();
    return 0;
}

//=====================================================================================
// DVBAudio_SF::clear
//=====================================================================================
int DVBAudio_SF::clear() const
{
    OMX_DBGT_PROLOG();
    assert(isOpen());
    if (this->isStopped()) {
        OMX_DBGT_EPILOG("Clear on a stopped device");
        return 0;
    }
    if (ioctl(adev_id, AUDIO_CLEAR_BUFFER, (void*)0) != 0)
     {
         OMX_DBGT_ERROR("AUDIO_CLEAR_BUFFER ioctl failed (%s)", strerror (errno));
         OMX_DBGT_EPILOG();
         return -1;
     }

    // Trigger state with same value to unblock any write
    m_bClearWrite = true;
    this->state(this->state());
     OMX_DBGT_EPILOG();
     return 0;
}

//=====================================================================================
// DVBAudio_SF::getTime
//=====================================================================================
uint64_t DVBAudio_SF::getTime() const
{
    assert(0);
    return 0;
}

//=====================================================================================
// DVBAudio_SF::v4lOpen
//  Open a V4L session: associate with DVB grabIdx
//=====================================================================================
int DVBAudio_SF::v4lOpen(int grabIdx)
{
    struct v4l2_audio audio;
    char inputEnum[32];
    OMX_DBGT_PROLOG();
    memset(&audio,0,sizeof(struct v4l2_audio));
    eos = false;
    /* Open the device */
    v4l_fd = v4l2_open_by_name(V4L2_AUDIO_DRIVER_NAME,V4L2_AUDIO_CARD_NAME, O_RDWR | O_NONBLOCK);

    if(v4l_fd < 0)
    {
        OMX_DBGT_ERROR("Unable to open V4L Device (%s)", strerror(errno));
        OMX_DBGT_EPILOG();
        return -1;
    }

    audio.index = 0;
    sprintf(inputEnum, V4L_S_INPUT, grabIdx);

    while (ioctl (v4l_fd, VIDIOC_ENUMAUDIO, &audio) == 0)
    {
        if(strcasecmp((char*)audio.name, inputEnum) == 0)
        {
            if (ioctl (v4l_fd, VIDIOC_S_AUDIO, &audio.index) == 0)
            {
                OMX_DBGT_PINFO("'%s' Grab Audio on V4L2 input '%s' (V4LAudio::Open)", v4lname(), inputEnum);
                OMX_DBGT_EPILOG();
                return 0;
            }
        }

        audio.index++;
    }

    OMX_DBGT_ERROR("V4L2 INPUT '%s' not found", inputEnum);
    OMX_DBGT_EPILOG();
    return -1;
}

//=====================================================================================
// DVBAudio_SF::v4lConfigure
// Configuration of V4L session
//=====================================================================================
int DVBAudio_SF::v4lConfigure(uint32_t sampleRate, uint32_t channelCount, uint32_t bitsPerSample)
{
    struct v4l2_format          Format;
    struct v4l2_buffer          Buffer;
    struct v4l2_requestbuffers  BufferRequest;
    struct v4l2_audio_format    *audio_fmt;

    OMX_DBGT_PROLOG();
    memset (&Format, 0, sizeof (struct v4l2_format));
    Format.type                         = V4L2_BUF_TYPE_AUDIO_CAPTURE;
    audio_fmt                           = (struct v4l2_audio_format *)&Format.fmt.raw_data;
    audio_fmt->SampleRateHz             = 0;
    audio_fmt->Channelcount             = 0;
    audio_fmt->BitsPerSample            = 0;

    if (ioctl (v4l_fd, VIDIOC_S_FMT, &Format) < 0)
    {
        OMX_DBGT_ERROR("Set Format (VIDIOC_S_FMT) failed (%s)", strerror (errno));
        OMX_DBGT_EPILOG();
        return -1;
    }

    memset (&BufferRequest, 0, sizeof (struct v4l2_requestbuffers));
    BufferRequest.memory                = V4L2_MEMORY_USERPTR;
    BufferRequest.type                  = V4L2_BUF_TYPE_AUDIO_CAPTURE;
    BufferRequest.count                 = MAX_INPUT_BUFFER_NUMBER;
    if (ioctl (v4l_fd, VIDIOC_REQBUFS, &BufferRequest) < 0)
    {
        OMX_DBGT_ERROR("Buffer Request (VIDIOC_REQBUFS) failed (%s)", strerror (errno));
        OMX_DBGT_EPILOG();
        return -1;
    }

    OMX_DBGT_EPILOG();
    return 0;
}


//=====================================================================================
// DVBAudio_SF::QueueBuffer
// send a empty buffer to SE and get back the buffer with a decoded frame
//=====================================================================================
int DVBAudio_SF::QueueBuffer (void* BufferData, int &nBytesSize, uint64_t &ts) const
{
    struct v4l2_buffer  Buffer;
    struct v4l2_audio_uncompressed_metadata audio_uncompressed_metadata;

    OMX_DBGT_PROLOG();

    if(isStarted() == false) {
      OMX_DBGT_ERROR("try to Buffer queue on device stopped");
      OMX_DBGT_EPILOG();
        return -1;
    }

    memset (&Buffer, 0, sizeof(Buffer));
    Buffer.type                 = V4L2_BUF_TYPE_AUDIO_CAPTURE;
    Buffer.memory               = V4L2_MEMORY_USERPTR;
    Buffer.m.userptr            = (unsigned long)BufferData;
    Buffer.field                = V4L2_FIELD_ANY;
    Buffer.length               = nBytesSize;

    if (ioctl (v4l_fd, VIDIOC_QBUF, &Buffer) < 0)
    {
        OMX_DBGT_ERROR("Buffer queue (VIDIOC_QBUF) failed (%s)", strerror (errno));
        OMX_DBGT_EPILOG();
        return -1;
    }

    Buffer.type                 = V4L2_BUF_TYPE_AUDIO_CAPTURE;
    Buffer.memory               = V4L2_MEMORY_USERPTR;
    Buffer.reserved             = (int)&audio_uncompressed_metadata;
    Buffer.bytesused = 0;

    int ret = ioctl (v4l_fd, VIDIOC_DQBUF, &Buffer);
    int counter = 0;
    int counter_EOS = 0;

// remove counter (300) to exit of the loop for the following case : end of playback (no more decoded buffer received)
// but sometime in this case the pulling thread is not blocked on outputPort->popAndLockCurrent() but on DQBUF
// util the stop closed the fd or a seek backward inject new buffer to decode
    while ((ret == -1) && (errno == EAGAIN)) {

      ret = ioctl (v4l_fd, VIDIOC_DQBUF, &Buffer);
      usleep(10000);
      counter++;
    }

    if (ret < 0)
    {
       if(errno == 19)
       {
          OMX_DBGT_PDEBUG("DVBAudio_SF::v4lQueueBuffer - EOS marker received");
          Buffer.bytesused= 0;
          nBytesSize = Buffer.bytesused;
          ts = (uint64_t)Buffer.timestamp.tv_sec * 1000000 + (uint64_t)Buffer.timestamp.tv_usec;
          OMX_DBGT_EPILOG();
          return 0;
       }
       else
       {
         OMX_DBGT_ERROR("Failed to dequeue buffer (VIDIOC_DQBUF) %d %d (%s)",ret, errno, strerror (errno));
         OMX_DBGT_EPILOG();
         return -1;
       }
    }
    if(m_nSampleRate != audio_uncompressed_metadata.sample_rate)
    {
        OMX_DBGT_PDEBUG("Freq set=%d, obtained=%d\n", m_nSampleRate, audio_uncompressed_metadata.sample_rate);
        m_nSampleRate = audio_uncompressed_metadata.sample_rate;
    }

    nBytesSize = Buffer.bytesused;

    ts = (uint64_t)Buffer.timestamp.tv_sec * 1000000 + (uint64_t)Buffer.timestamp.tv_usec;

    OMX_DBGT_EPILOG();
    return 0;
}

//=====================================================================================
// DVBAudio_SF::UseBuffer
//==================================================================================

int DVBAudio_SF::UseBuffer(unsigned long virtualAddr, unsigned long bufferSize,
                        int width, int height, int color,
                        unsigned long userId) const
{
    assert(0);
    return 0;
}

//=====================================================================================
// DVBAudio_SF::FillThisBuffer
//=====================================================================================
int DVBAudio_SF::FillThisBuffer(unsigned long userId) const
{
    assert(0);
    return 0;
}

//=====================================================================================
// DVBAudio_SF::FillBufferDone
//=====================================================================================
int DVBAudio_SF::FillBufferDone(unsigned long &userId, uint64_t &TS,
                             bool & eos) const
{
    assert(0);
    return 0;
}

//=====================================================================================
// DVBAudio_SF::FlushBuffer
//=====================================================================================
unsigned long DVBAudio_SF::FlushBuffer() const
{
    assert(0);
    return 0;
}


