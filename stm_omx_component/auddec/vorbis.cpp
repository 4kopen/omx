/*
 * Copyright 2011 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *
 * File   ::    vorbis.cpp
 * Author ::    Damien TOURNUS (damien.tournus@st.com)
 *
 *
 */

#include "AudioPesAdapter.hpp"
#include "AudioDecoder.hpp"
#include "OMX_debug.h"

#include "pes.hpp"

#define DBGT_PREFIX "VORB"
#define DBGT_LAYER  1
#include <linux/dbgt.h>

//#define VORBIS_DEBUG

#define PTS_CORRECTION(x) (((x) * 90000)/1000000)

// missing comment header config
static unsigned char comment_config[] = 
{
   // Header
   0x00, 0x00, 0x01, 0xBD, 0x00, 0x1C, 0x80, 0x01, 0x09, 0x80, 0x10, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   // Header type : config=3
   0x03,
   // 'vorbis'
   0x76, 0x6F, 0x72, 0x62, 0x69, 0x73,
   // Vendor field lenght
   0x00, 0x00, 0x00, 0x00,
   // User field number
   0x00, 0x00, 0x00, 0x00,
   // framing bit
   0x01
};

namespace stm {

VorbisPesAdapter::VorbisPesAdapter(const ADECComponent& dec)
    :AudioPesAdapter(dec),
     lastTimeStamp(0)
{}
  
VorbisPesAdapter::~VorbisPesAdapter()
{}

int VorbisPesAdapter::emitCodecConfig(uint8_t* pBuffer,
                                      unsigned int nFilledLen) const
{
    uint32_t HeaderLength;
    uint32_t PrivateHeaderLength;
    uint32_t PesLength;
    int err = 0;

    OMX_DBGT_PROLOG();

    memset (PesHeader, 0, PES_MAX_HEADER_SIZE);

    lastTimeStamp = 0;

    HeaderLength                        = InsertPesHeader (PesHeader, nFilledLen, 0xbd, INVALID_PTS_VALUE, 0);
    PrivateHeaderLength                 = InsertVideoPrivateDataHeaderForAudio (&PesHeader[HeaderLength], nFilledLen);

    /* Update PesLength */
    PesLength                               = PesHeader[PES_LENGTH_BYTE_0] + (PesHeader[PES_LENGTH_BYTE_1] << 8);
    PesLength                              += PrivateHeaderLength;
    PesHeader[PES_LENGTH_BYTE_0]            = PesLength & 0xff;
    PesHeader[PES_LENGTH_BYTE_1]            = (PesLength >> 8) & 0xff;
    PesHeader[PES_HEADER_DATA_LENGTH_BYTE] += PrivateHeaderLength;
    PesHeader[PES_FLAGS_BYTE]              |= PES_EXTENSION_DATA_PRESENT;

    HeaderLength                           += PrivateHeaderLength;

    err  = this->auddec().dvb().write(PesHeader, HeaderLength);
    if(err)
    {
       OMX_DBGT_ERROR("%s::VORBISEmitCodecConfig->dvb().write failed", this->name());
       goto error;
    }

#ifdef VORBIS_DEBUG
    OMX_DBGT_PTRACE("EmitCodecConfig header # 1 size : %d ",HeaderLength);
    OMX_DBGT_PTRACE("EmitCodecConfig header data : ");
    for (unsigned int i=0;i<(HeaderLength);i++)
	OMX_DBGT_PTRACE(" 0x%x",(unsigned int)PesHeader[i]);
#endif

    err = this->auddec().dvb().write(pBuffer, nFilledLen);
    if(err)
    {
       OMX_DBGT_ERROR("%s::VORBISEmitCodecConfig->dvb().write failed", this->name());
       goto error;
    }

    // adding missing comment header config
    if(pBuffer[0] == 1)
    {
      err = this->auddec().dvb().write(comment_config, sizeof(comment_config));
    }

    if(err)
    {
       OMX_DBGT_ERROR("%s::VORBISEmitCodecConfig->dvb().write failed", this->name());
    }

error:
    OMX_DBGT_EPILOG();
    return err;
}



int VorbisPesAdapter::emitFrame(uint8_t* pBuffer,
                                unsigned int nFilledLen,
                                uint64_t nTimeStamp) const
{
    uint32_t HeaderLength;
    uint32_t PrivateHeaderLength;
    uint32_t PesLength;
    int err = 0;
    uint64_t nPTS = PTS_CORRECTION(nTimeStamp);

    OMX_DBGT_PROLOG("pBuf=%p, len=%d, nTimeStamp=%llu", pBuffer, nFilledLen, nTimeStamp);

    memset (PesHeader, 0, PES_MAX_HEADER_SIZE);

    if(lastTimeStamp == nTimeStamp)
    {
        // PTS is the same for several frames :
        // so when PTS is the same as previous frame, INVALID_PTS_VALUE value
        // is set to force Streaming Engine to calculate it.
        HeaderLength = InsertPesHeader (PesHeader, nFilledLen, 0xbd, INVALID_PTS_VALUE, 0);
    }
    else
    {
        if(nPTS > INVALID_PTS_VALUE)
        {
	    //We will anyway overflow. So send no PTS
            nPTS = INVALID_PTS_VALUE;
        }
        HeaderLength = InsertPesHeader (PesHeader, nFilledLen, 0xbd, nPTS, 0);
        lastTimeStamp = nTimeStamp;
    }

    PrivateHeaderLength                 = InsertVideoPrivateDataHeaderForAudio (&PesHeader[HeaderLength], nFilledLen);
    /* Update PesLength */
    PesLength                               = PesHeader[PES_LENGTH_BYTE_0] + (PesHeader[PES_LENGTH_BYTE_1] << 8);
    PesLength                              += PrivateHeaderLength;
    PesHeader[PES_LENGTH_BYTE_0]            = PesLength & 0xff;
    PesHeader[PES_LENGTH_BYTE_1]            = (PesLength >> 8) & 0xff;
    PesHeader[PES_HEADER_DATA_LENGTH_BYTE] += PrivateHeaderLength;
    PesHeader[PES_FLAGS_BYTE]              |= PES_EXTENSION_DATA_PRESENT;

    HeaderLength                           += PrivateHeaderLength;

    err  = this->auddec().dvb().write(PesHeader, HeaderLength);
    if(err)
    {
       OMX_DBGT_ERROR("%s::VORBISEmitCodecConfig->dvb().write failed", this->name());
       goto error;
    }

    if (nFilledLen > 0)
        err = this->auddec().dvb().write(pBuffer, nFilledLen);
    if(err)
    {
       OMX_DBGT_ERROR("%s::VORBISEmitCodecConfig->dvb().write failed", this->name());
    }

error:
    OMX_DBGT_EPILOG();
    return err;
}

} // eof namespace stm
