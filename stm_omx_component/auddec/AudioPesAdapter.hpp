/*
 * Copyright 2013 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file   AudioPesAdapter.hpp
 * @author STMicroelectronics
 */

#ifndef _STM_AUDIOPESADAPTER_HPP_
#define _STM_AUDIOPESADAPTER_HPP_

#include <stdint.h>

namespace stm {

// Forward declaration
class ADECComponent;

class AudioPesAdapter
{
public:
    AudioPesAdapter(const ADECComponent& dec);

    virtual ~AudioPesAdapter();

    /**
     * Factory method returning right PesAdapter instance
     * depending on codec type
     */
    static AudioPesAdapter* create(int nCoding,
                                   const ADECComponent& aDec);

public:
    virtual int emitCodecConfig(uint8_t* pBuffer,
                                unsigned int nFilledLen) const = 0;

    virtual int emitFrame(uint8_t* pBuffer, unsigned int nFilledLen,
                          uint64_t nTimeStamp) const = 0;

    const char* name() const;

protected:
    unsigned char*       PesHeader;
    const ADECComponent& auddec() const;

protected:
    bool mIsLoggingEnabled;

private:
    const ADECComponent&  m_auddec;
};

inline const ADECComponent& AudioPesAdapter::auddec() const
{ return m_auddec; }


/**
 * Aac codec
 */
class AacPesAdapter: public AudioPesAdapter
{
public:
    AacPesAdapter(const ADECComponent& dec);

    virtual ~AacPesAdapter();

public:
    virtual int emitCodecConfig(uint8_t* pBuffer,
                                unsigned int nFilledLen) const;

    virtual int emitFrame(uint8_t* pBuffer, unsigned int nFilledLen,
                          uint64_t nTimeStamp) const;
};

/**
 * Pcm codec
 */
class PcmPesAdapter: public AudioPesAdapter
{
public:
    PcmPesAdapter(const ADECComponent& dec);

    virtual ~PcmPesAdapter();

public:
    virtual int emitCodecConfig(uint8_t* pBuffer,
                                unsigned int nFilledLen) const;

    virtual int emitFrame(uint8_t* pBuffer, unsigned int nFilledLen,
                          uint64_t nTimeStamp) const;
};


/**
 * Mp3 codec
 */
class Mp3PesAdapter: public AudioPesAdapter
{
public:
    Mp3PesAdapter(const ADECComponent& dec);

    virtual ~Mp3PesAdapter();

public:
    virtual int emitCodecConfig(uint8_t* pBuffer,
                                unsigned int nFilledLen) const;

    virtual int emitFrame(uint8_t* pBuffer, unsigned int nFilledLen,
                          uint64_t nTimeStamp) const;
};


/**
 * Dts codec
 */
class DtsPesAdapter: public AudioPesAdapter
{
public:
    DtsPesAdapter(const ADECComponent& dec);

    virtual ~DtsPesAdapter();

public:
    virtual int emitCodecConfig(uint8_t* pBuffer,
                                unsigned int nFilledLen) const;

    virtual int emitFrame(uint8_t* pBuffer, unsigned int nFilledLen,
                          uint64_t nTimeStamp) const;
};

/**
 * Ac3 codec
 */
class Ac3PesAdapter: public AudioPesAdapter
{
public:
    Ac3PesAdapter(const ADECComponent& dec);

    virtual ~Ac3PesAdapter();

public:
    virtual int emitCodecConfig(uint8_t* pBuffer,
                                unsigned int nFilledLen) const;

    virtual int emitFrame(uint8_t* pBuffer, unsigned int nFilledLen,
                          uint64_t nTimeStamp) const;
};

/**
 * Wma codec
 */
class WmaPesAdapter: public AudioPesAdapter
{
public:
    WmaPesAdapter(const ADECComponent& dec);

    virtual ~WmaPesAdapter();

public:
    virtual int emitCodecConfig(uint8_t* pBuffer,
                                unsigned int nFilledLen) const;

    virtual int emitFrame(uint8_t* pBuffer, unsigned int nFilledLen,
                          uint64_t nTimeStamp) const;
};

/**
 * Vorbis codec
 */
class VorbisPesAdapter: public AudioPesAdapter
{
public:
    VorbisPesAdapter(const ADECComponent& dec);

    virtual ~VorbisPesAdapter();

public:
    virtual int emitCodecConfig(uint8_t* pBuffer,
                                unsigned int nFilledLen) const;

    virtual int emitFrame(uint8_t* pBuffer, unsigned int nFilledLen,
                          uint64_t nTimeStamp) const;

private:
    mutable uint64_t lastTimeStamp;
};

/**
 * Amr codec
 */
class AmrPesAdapter: public AudioPesAdapter
{
public:
    AmrPesAdapter(const ADECComponent& dec);

    virtual ~AmrPesAdapter();

public:
    virtual int emitCodecConfig(uint8_t* pBuffer,
                                unsigned int nFilledLen) const;

    virtual int emitFrame(uint8_t* pBuffer, unsigned int nFilledLen,
                          uint64_t nTimeStamp) const;
};

/**
 * Adpcm codec
 */
class AdpcmPesAdapter: public AudioPesAdapter
{
// structure as defined in the FFMpeg Extractor
typedef struct ADPCMInfo_s {
    uint32_t    nBlockAlign;
    uint32_t    nSamplesPerBlock;
    uint32_t    nFormatTag;
    uint32_t    nCoefficients;
} ADPCMInfo_t;

  mutable ADPCMInfo_t AdpcmInfo;
public:
    AdpcmPesAdapter(const ADECComponent& dec);

    virtual ~AdpcmPesAdapter();

public:
    virtual int emitCodecConfig(uint8_t* pBuffer,
                                unsigned int nFilledLen) const;

    virtual int emitFrame(uint8_t* pBuffer, unsigned int nFilledLen,
                          uint64_t nTimeStamp) const;
};

} // eof nm stm

#endif
