/*
 * Copyright 2011 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *
 * File   ::    ac3.cpp
 * Author ::    Damien TOURNUS (damien.tournus@st.com)
 *
 *
 */

#include "AudioPesAdapter.hpp"
#include "AudioDecoder.hpp"
#include "OMX_debug.h"

#include "pes.hpp"

#define DBGT_PREFIX "AC3 "
#define DBGT_LAYER  1
#include <linux/dbgt.h>

//#define AC3_DEBUG

#define PTS_CORRECTION(x) (((x) * 90000)/1000000)

namespace stm {

Ac3PesAdapter::Ac3PesAdapter(const ADECComponent& dec)
    :AudioPesAdapter(dec)
{}
  
Ac3PesAdapter::~Ac3PesAdapter()
{}

int Ac3PesAdapter::emitCodecConfig(uint8_t* pBuffer,
                                   unsigned int nFilledLen) const
{
    OMX_DBGT_PROLOG("pBuf=%p, len=%lu", pBuffer, (unsigned long)nFilledLen);

    OMX_DBGT_EPILOG();
    return 0;
}


int Ac3PesAdapter::emitFrame(uint8_t* pBuffer,
                             unsigned int nFilledLen, uint64_t nTimeStamp) const
{
    int   header_length;
    int err = 0;
    uint64_t nPTS = PTS_CORRECTION(nTimeStamp);

    OMX_DBGT_PROLOG("pBuf=%p, len=%d, nTimeStamp=%llu", pBuffer, nFilledLen, nTimeStamp);

    if(nPTS > INVALID_PTS_VALUE)
    {
        //We will anyway overflow. So send no PTS
        nPTS = INVALID_PTS_VALUE;
    }

    memset (PesHeader, 0, PES_MAX_HEADER_SIZE);
    header_length = InsertPesHeader (PesHeader, nFilledLen, PRIVATE_STREAM_1_PES_START_CODE, nPTS, 0);

    err = this->auddec().dvb().write(PesHeader, header_length);
    if(err)
    {
        OMX_DBGT_ERROR("%s::AC3EmitFrame->dvb().write PesHeader failed", this->name());
        goto ERROR;
    }

#ifdef AC3_DEBUG
    {
        int i = 0;
        char* LogPtr = (char *)malloc (header_length * 3 + 1);

        OMX_DBGT_PINFO ("%s AC3 data header length %d (ADECComponent::AC3EmitFrame)",
                    this->name(), header_length);
        for(i = 0; i < header_length; i++)
        {
            sprintf (LogPtr+3*i, "%.2x ", PesHeader[i]);
        }
        OMX_DBGT_PINFO ("%s %s (ADECComponent::AC3EmitFrame)",
                    this->name(), LogPtr);
        free (LogPtr);
    }
#endif

    if (nFilledLen > 0)
        err = this->auddec().dvb().write(pBuffer, nFilledLen);
    if(err)
    {
        OMX_DBGT_ERROR("%s::AC3EmitFrame->dvb().write pBuffer failed", this->name());
        goto ERROR;
    }

#ifdef AC3_DEBUG
    {
        int i = 0;
        char* LogPtr = (char *)malloc (40 * 3 + 1);

        OMX_DBGT_PINFO ("%s AC3 data length %d  (ADECComponent::AC3EmitFrame)",
                    this->name(), nFilledLen);
        for(i = 0; i < 40; i++)
        {
            sprintf (LogPtr+3*i, "%.2x ", pBuffer[i]);
        }
        OMX_DBGT_PINFO ("%s %s (ADECComponent::AC3EmitFrame)",
                    this->name(), LogPtr);
        free (LogPtr);
    }
#endif

ERROR:
    OMX_DBGT_EPILOG();
    return err;
}

} // eof namespace stm
