/*
 * Copyright 2013 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file   dvbaudio_moo.hpp
 * @author STMicroelectronics
 */


#ifndef ST_OMX_DVBAUDIO_H
#define ST_OMX_DVBAUDIO_H
#include <stdio.h>
#include <string.h>
#include "IDvbDevice.hpp"
#include "Mutex.hpp"

#ifdef ANDROID
#define DVB_DEVICE_GRAB                 "/dev/dvb0.audio%d"
#else
#define DVB_DEVICE_GRAB                 "/dev/dvb/adaptor0/audio%d"
#endif
#define DEVICE_NAME_LENGTH            sizeof(DVB_DEVICE_GRAB)+5

typedef  enum  {
    DVBAudio_CodingAAC = 0,
    DVBAudio_CodingOTHER = 1
} DVBAudio_CODINGTYPE;


class DVBAudio_MOO: public stm::IDvbDevice
{
public:
    DVBAudio_MOO(const char* strName, int nEncoding, const char *name);
    virtual ~DVBAudio_MOO();

public:
    virtual int id() const;
    virtual const char* name() const;

    virtual int create();
    virtual int destroy();
    virtual bool isOpen() const;

    virtual int start() const;
    virtual int stop() const;
    virtual int pause(bool hw) const;
    virtual int resume(bool hw) const;
    virtual void lock() const;
    virtual void unlock() const;
    virtual void resetflags() const;
    virtual void setEncoding(int nEncoding);
    virtual void setSync(bool bSyncEnable);

    virtual uint64_t getTime() const;
    virtual uint32_t getSamplingRate() const{return 0;};

    virtual int write(const void* data_ptr, unsigned int len) const;
    virtual int flush(bool bEoS = false) const;
    virtual int clear() const;

    virtual stm::IDvbDevice::EState state() const;

    virtual int QueueBuffer (void* BufferData, int &nBytesSize, uint64_t &ts) const;
    virtual int UseBuffer(unsigned long virtualAddr, unsigned long bufferSize,
                  int width, int height, int color,
                  unsigned long userId) const;
    virtual unsigned long FlushBuffer() const;
    virtual int FillThisBuffer(unsigned long userId) const;
    virtual int FillBufferDone(unsigned long & userId, uint64_t & TS, bool & eos) const;

private:
    void state(stm::IDvbDevice::EState st) const;

protected:
    bool mIsLoggingEnabled;

private:
    int adev_id;
    int m_nEncoding;
    bool enableSync;
    char m_deviceName[DEVICE_NAME_LENGTH];
    /** Internal state to know if device started or not */
    mutable stm::IDvbDevice::EState m_nState;
    /** Serialize access to dvb commands */
    mutable stm::Mutex      m_mutexCmd;
    mutable stm::Mutex      m_mutexCmd2;
    /** Condition variable signaled when state has changed */
    mutable stm::Condition  m_condState;

    mutable bool             m_bClocked;
    mutable bool             m_bClearWrite;
    mutable bool             m_bClearWrite2;
    mutable bool             m_bFlushWrite;
};

#endif
