/*
 * Copyright 2013 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file   VideoEncoderPort.hpp
 * @author STMicroelectronics
 */

#ifndef _STM_VIDEOENCODERPORT_HPP_
#define _STM_VIDEOENCODERPORT_HPP_

#include "StmOmxPort.hpp"
#include "StmOmxComponent.hpp"
#include "VideoPortDefinition.hpp"
#include <stdint.h>
#include <string.h>
#include <unistd.h>

#include "Mutex.hpp"
#include <pthread.h>
#ifdef ANDROID
#include <hardware/gralloc.h>
#endif
#include "encoder.hpp"

#define DEFAULT_PARAM_HEADER(param,port)    \
    sizeof(param),                          \
    {{0,0,0,0}},                            \
    port


// save and restore nSize, nVersion and nPortIndex from IL client
#define SET_DEFAULT_PARAM(current_param,default_param)      \
do  {                                                       \
        OMX_U32 size            = current_param->nSize;     \
        OMX_VERSIONTYPE omx_ver = current_param->nVersion;  \
        OMX_U32 index           = current_param->nPortIndex;\
        *current_param = default_param;                     \
        current_param->nSize        = size;                 \
        current_param->nVersion     = omx_ver;              \
        current_param->nPortIndex   = index;                \
    }                                                       \
while(0)


namespace stm {

// Forward declaration
class VidEncPort;
class VidEncOutputPort;
class VidEncInputPort;
class VENCComponent;

class VidEncPort: public OmxPort
{
public:
    /** Constructor */
    VidEncPort(unsigned int nIdx, OMX_DIRTYPE aDir,
               const VENCComponent& owner);

    /** Destructor */
    virtual ~VidEncPort();

    virtual const OmxComponent& component() const;
    virtual const VideoPortDefinition& definition() const;

protected:
    virtual VideoPortDefinition& definition();

    const VENCComponent& VidEnc() const;

private:
    const VENCComponent&         m_owner;
    VideoPortDefinition          m_PortDefinition;
};

inline const VENCComponent& VidEncPort::VidEnc() const
{ return m_owner; }


//==========================================================================
// VidEncOutputPort declaration
//==========================================================================
class VidEncOutputPort: public VidEncPort
{
public:
        struct _nbProfileAndLevelSupported {
                enum OMX_VIDEO_AVCPROFILETYPE AVCprofile;
                enum OMX_VIDEO_AVCLEVELTYPE AVClevel;
        };

    /** Constructor */
    VidEncOutputPort(unsigned int nIdx, OMX_DIRTYPE aDir,
                     const VENCComponent& owner);

    /** Destructor */
    virtual ~VidEncOutputPort();

    virtual OMX_ERRORTYPE create();
    virtual void destroy();
    int start();
    int stop();

public:
    virtual OMX_ERRORTYPE
    getParameter(OMX_INDEXTYPE nIndex, OMX_PTR pParamStruct);

    virtual OMX_ERRORTYPE
    setParameter(OMX_INDEXTYPE nIndex, OMX_PTR pParamStruct);

    virtual OMX_ERRORTYPE
    useBuffer(OMX_BUFFERHEADERTYPE** ppBufferHdr,
              OMX_PTR pAppPrivate,
              OMX_U32 nSizeBytes,
              OMX_U8* pBuffer);

    virtual OMX_ERRORTYPE
    allocateBuffer(OMX_BUFFERHEADERTYPE** ppBufferHdr,
                   OMX_PTR pAppPrivate,
                   OMX_U32 nSizeBytes);

    virtual OMX_ERRORTYPE
    freeBuffer(OMX_BUFFERHEADERTYPE* pBufferHdr);

    virtual OMX_ERRORTYPE
    emptyThisBuffer(OMX_BUFFERHEADERTYPE* pBuffer);

    virtual OMX_ERRORTYPE
    fillThisBuffer(OMX_BUFFERHEADERTYPE* pBuffer);

    virtual OMX_ERRORTYPE flush();

    OMX_VIDEO_CODINGTYPE
    supportedCodingFormat(unsigned int nIdx) const;

    unsigned int
    supportedCodingFormatNb() const;

    bool isSupportedCodingFormat(OMX_VIDEO_CODINGTYPE aFmt) const;

    /** Change current port compression format in definition */
    void codingFormat(OMX_VIDEO_CODINGTYPE aFmt);

    bool nativeWindowsEnabled() const;
    unsigned int receivedFrame() const;

    bool  isAfterEosFlag() const;
    void resetAfterEosFlag();
    IDvbDevice& dvb();
    OMX_ERRORTYPE setDvbDevice(IDvbDevice **m_dvb);
    void setInternalPointers(HVAHEncoder_t **params, EncoderBufferMapping **buffermapping);

    void setDefaultParameters();
    int disconnectAssociation(void *Inputbuffer);

    OMX_ERRORTYPE updateAVCParams(OMX_VIDEO_PARAM_AVCTYPE *aParamVideoAvc);

protected:
    /**
     * This function serves as the entry point to the thread.
     * @param pthis the instance
     */
    static void* ThreadEntryPoint(void* pThis);

    /** The thread main function loop */
    void threadFunction();

    int findAssociation(void *Inputbuffer, int type, void **retBuffer);

private:
#ifdef ANDROID
    const gralloc_module_t* grallocModule() const;
    struct alloc_device_t*  grallocDevice() const;
#endif
    OMX_ERRORTYPE doOutputPortExtraProcessing(OMX_BUFFERHEADERTYPE**  ppBufferHdr,OMX_U32 nSizeBytes);
    OMX_U32 getVirtualAddress(OMX_BUFFERHEADERTYPE**  ppBufferHdr);

    IDvbDevice*               m_dvb;

    /** Thread Id of the port thread */
    pthread_t puller_thread_id;

    /*
     * TRUE: Native windows enabled by Android and buffer allocated by
     *       itself through gralloc
     *      => pBuffer contains buffer_handle_t
     * FALSE: Buffer allocated by OMX_AllocateBuffer through gralloc
     *      => pBuffer contains vaddr
     *      => pPlatformPrivate contains buffer_handle_t
     */
    bool m_bNativeWindowsEnabled;

    // Count number of Frame, integer is enough since it allow
    // to play more than 1 year !!!
    unsigned int m_nReceivedFrame;

    bool     m_bAfterEosFlag;

#ifdef ANDROID
    const gralloc_module_t* m_grallocModule;
    struct alloc_device_t* m_grallocDevice;
#endif

    HVAHEncoder_t *mPortHVAHEncoder_Params;
    EncoderBufferMapping *mapping;

    OMX_BOOL m_outputEncoderStarted;

    static const OMX_VIDEO_CODINGTYPE  m_codingFormats[];
    static const struct _nbProfileAndLevelSupported m_profilelevel[];
    static const OMX_VIDEO_PARAM_AVCTYPE mParamVideoAvcDefault;
    static const OMX_VIDEO_PARAM_BITRATETYPE mParamVideoBitrateDefault;

    h264_level_profile profile_level;

    OMX_BOOL m_isAllocateBuffer;

    OMX_BOOL isSPSPPSSent;
    OMX_BUFFERHEADERTYPE *tempOutputBuffer;

};

inline bool VidEncOutputPort::nativeWindowsEnabled() const
{ return m_bNativeWindowsEnabled; }

inline unsigned int VidEncOutputPort::receivedFrame() const
{ return m_nReceivedFrame; }

inline void VidEncOutputPort::resetAfterEosFlag()
{ m_bAfterEosFlag = false; }

inline bool VidEncOutputPort::isAfterEosFlag() const
{ return m_bAfterEosFlag; }

inline IDvbDevice& VidEncOutputPort::dvb()
{ return *m_dvb; }

#ifdef ANDROID
inline const gralloc_module_t* VidEncOutputPort::grallocModule() const
{ return m_grallocModule; }

inline struct alloc_device_t*  VidEncOutputPort::grallocDevice() const
{ return m_grallocDevice; }
#endif

//==========================================================================
// VidEncInputPort declaration
//==========================================================================

class VidEncInputPort: public VidEncPort
{
public:
    typedef struct {
        OMX_COLOR_FORMATTYPE OMXType;
        uint32_t androidHALType;
        uint32_t v4lType;
        uint8_t bpp;
        uint8_t bppWidth;
    } ColorInfo;

    typedef struct {
        OMX_BUFFERHEADERTYPE *omxBuffer;
        OMX_U8 *pBuffer;
        OMX_BOOL isFree;
    }SwapBuffer;


public:
    /** Constructor */
    VidEncInputPort(unsigned int nIdx, OMX_DIRTYPE aDir,
                    const VENCComponent& owner);

    /** Destructor */
    virtual ~VidEncInputPort();

    virtual OMX_ERRORTYPE create();
    virtual void destroy();
    int start();
    int stop();

public:
    virtual OMX_ERRORTYPE
    getParameter(OMX_INDEXTYPE nIndex, OMX_PTR pParamStruct);

    virtual OMX_ERRORTYPE
    setParameter(OMX_INDEXTYPE nIndex, OMX_PTR pParamStruct);

    virtual OMX_ERRORTYPE
    useBuffer(OMX_BUFFERHEADERTYPE** ppBufferHdr,
              OMX_PTR pAppPrivate,
              OMX_U32 nSizeBytes,
              OMX_U8* pBuffer);

    virtual OMX_ERRORTYPE
    allocateBuffer(OMX_BUFFERHEADERTYPE** ppBufferHdr,
                   OMX_PTR pAppPrivate,
                   OMX_U32 nSizeBytes);

    virtual OMX_ERRORTYPE
    freeBuffer(OMX_BUFFERHEADERTYPE* pBufferHdr);

    virtual OMX_ERRORTYPE
    emptyThisBuffer(OMX_BUFFERHEADERTYPE* pBuffer);

    virtual OMX_ERRORTYPE
    fillThisBuffer(OMX_BUFFERHEADERTYPE* pBuffer);

    virtual OMX_ERRORTYPE flush();
    virtual OMX_ERRORTYPE pause();
    virtual OMX_ERRORTYPE resume();


    /** Change current port compression format in definition */
    void codingFormat(OMX_VIDEO_CODINGTYPE aFmt);

    unsigned int sendedFrame() const;

    OMX_COLOR_FORMATTYPE
    supportedColorFormat(unsigned int nIdx) const;

    unsigned int
    supportedColorFormatNb() const;

    bool isSupportedColorFormat(OMX_COLOR_FORMATTYPE aFmt) const;

    /** Change current port color format definition */
    void colorFormat(OMX_COLOR_FORMATTYPE aFmt);

    /** Return information about current color format */
    // TODO: implement static method with color format as param
    const ColorInfo& colorInfo() const;

    IDvbDevice& dvb();
    OMX_ERRORTYPE setDvbDevice(IDvbDevice **m_dvb);
    void setInternalPointers(HVAHEncoder_t **params, EncoderBufferMapping **buffermapping);
    void compute_framerate_num_den(OMX_U32 framerate, OMX_U32& num, OMX_U32& den);
    void bufferSize(unsigned int buff_size);

    int disconnectAssociation(void *Inputbuffer);

    int h264BlitToNV12(OMX_BUFFERHEADERTYPE*   pBufferHdr);

    OMX_ERRORTYPE doInputPortExtraProcessing(OMX_BUFFERHEADERTYPE**  ppBufferHdr,OMX_U32 nSizeBytes);
    OMX_U32 getVirtualAddress(OMX_BUFFERHEADERTYPE**  ppBufferHdr);
#ifdef ANDROID
    const gralloc_module_t* grallocModule() const;
    struct alloc_device_t*  grallocDevice() const;
#endif

protected:
    /**
     * This function serves as the entry point to the thread.
     * @param pthis the instance
     */
    static void* ThreadEntryPoint(void* pThis);

    /** The thread main function loop */
    void threadFunction();

    int findAssociation(void *Inputbuffer, int type, void **retBuffer);

private:
    typedef enum  {
        EThreadState_Stopped  = 0,
        EThreadState_Running,
        EThreadState_Paused,
    } EThreadState;

    EThreadState threadState() const;
    void threadState(EThreadState st);


    int encodeFrame(unsigned int nOmxFlags,
                    uint8_t* pBuf, unsigned int nLen,
                    uint64_t nTimeStamp);

private:
    static const ColorInfo  m_colorInfos[];
    static OMX_BUFFERHEADERTYPE* EOSBUF;
    // Thread Id of the port thread
    pthread_t pusher_thread_id;
    EThreadState    m_nThreadState;
    Mutex           m_mutexThread;
    Condition       m_condState;

    unsigned int    m_nSendedFrame;
    IDvbDevice*               m_dvb;

    bool     params_done;
    bool     initial_header;
#ifdef ANDROID
    const gralloc_module_t* m_grallocModule;
    struct alloc_device_t* m_grallocDevice;
#endif

    HVAHEncoder_t *mPortHVAHEncoder_Params;
    EncoderBufferMapping *mapping;
    OMX_BOOL m_inputEncoderStarted;
    OMX_BOOL m_bAfterEosFlag;
    OMX_BOOL m_isAllocateBuffer;
    SwapBuffer *swp_buffers;
};

inline unsigned int VidEncInputPort::sendedFrame() const
{ return m_nSendedFrame; }

inline IDvbDevice& VidEncInputPort::dvb()
{ return *m_dvb; }

#ifdef ANDROID
inline const gralloc_module_t* VidEncInputPort::grallocModule() const
{ return m_grallocModule; }

inline struct alloc_device_t*  VidEncInputPort::grallocDevice() const
{ return m_grallocDevice; }
#endif

} // eof namespace stm

#endif  // _STM_VIDEOENCODERPORT_HPP_
