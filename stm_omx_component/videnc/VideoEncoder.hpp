/*
 * Copyright 2013 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file   VideoEncoder.cpp
 * @author Jean-Philippe FASSINO(jean-philippe.fassino@st.com)
 */

#ifndef _STM_VIDEOENCODER_HPP_
#define _STM_VIDEOENCODER_HPP_

#include "StmOmxComponent.hpp"
#include "VideoEncoderPort.hpp"
#include "Role.hpp"
#include "dvbvideo_sf.hpp"
#ifdef ANDROID
#include <hardware/gralloc.h>
#endif
extern "C" {
#define OMX_enableAndroidNativeBuffers                        (OMX_IndexVendorStartUnused + 1)
#define OMX_getAndroidNativeBufferUsage                       (OMX_IndexVendorStartUnused + 2)
#define OMX_useAndroidNativeBuffer2                           (OMX_IndexVendorStartUnused + 3)
#define OMX_google_android_index_prependSPSPPSToIDRFrames     (OMX_IndexVendorStartUnused + 4)
#define OMX_google_android_index_storeMetaDataInBuffers       (OMX_IndexVendorStartUnused + 5)
}

#define OMX_StmIndexprependSPSPPSToIDRFramesExt   "OMX.google.android.index.prependSPSPPSToIDRFrames"
#define OMX_StmIndexstoreMetaDataInBuffersExt     "OMX.google.android.index.storeMetaDataInBuffers"

typedef struct {
    OMX_U32 nSize;
    OMX_VERSIONTYPE nVersion;
    OMX_BOOL bEnable;
} OMX_PrependSPSPPSToIDRFramesParams;


namespace stm {


class VENCComponent: public OmxComponent
{
public:
    VENCComponent(OMX_HANDLETYPE hComponent);
    virtual ~VENCComponent();

    virtual OMX_ERRORTYPE create();
    virtual void destroy();
    virtual const VidEncPort& getPort(unsigned int nIdx) const;

    friend class VidEncInputPort;
    friend class VidEncOutputPort;

    virtual OMX_ERRORTYPE getParameter(
                  OMX_INDEXTYPE       nParamIndex,
                  OMX_PTR             ComponentParameterStructure);
    virtual OMX_ERRORTYPE setParameter(
                  OMX_INDEXTYPE       nParamIndex,
                  OMX_PTR             ComponentParameterStructure);
    virtual
    OMX_ERRORTYPE getConfig(OMX_INDEXTYPE nIndex, OMX_PTR pConfStruct);

    virtual
    OMX_ERRORTYPE setConfig(OMX_INDEXTYPE nIndex, OMX_PTR pConfStruct);

    virtual OMX_ERRORTYPE getExtensionIndex(
                  OMX_STRING          cParameterName,
                  OMX_INDEXTYPE *     pIndexType);
    virtual OMX_ERRORTYPE emptyThisBuffer(OMX_BUFFERHEADERTYPE*   pBufferHdr);
    virtual OMX_ERRORTYPE fillThisBuffer(OMX_BUFFERHEADERTYPE*   pBufferHdr);

    const VidEncInputPort& inputPort() const;
    const VidEncOutputPort& outputPort() const;
    bool IsConfigureDone;
    mutable Mutex m_mutexCmd;
    mutable Condition   m_condConfig;

protected:
    virtual VidEncPort& getPort(unsigned int nIdx);

    virtual OMX_ERRORTYPE configure();
    virtual OMX_ERRORTYPE start();
    virtual void stop();
    virtual void execute();
    virtual OMX_ERRORTYPE flush(unsigned int nPortIdx);

    OMX_ERRORTYPE setRole(const char* pNewRole);
public:
    unsigned int getWidth(unsigned int nIdx) const;
    unsigned int getHeight(unsigned int nIdx) const;
    OMX_VIDEO_CODINGTYPE getCompressionFormat(unsigned int nIdx) const;
    OMX_BOOL isPrePendSPSPPS() const;
    HVAHEncoder_t **getEncoderParam();

    OMX_BOOL isForceIntraEnabled() const;
    OMX_BOOL isMetaDataSet() const;
    void resetForceIntraEnabled() const;

private:
    VidEncInputPort& inputPort_();
    VidEncOutputPort& outputPort_();


    Role& role();


private:
    OMX_BOOL mPrependSPSPPS;
    mutable OMX_BOOL m_isForceIntraEnabled;

    VidEncInputPort      m_inputPort;
    VidEncOutputPort     m_outputPort;

    Role                 m_componentRole;
    HVAHEncoder_t *m_HVAHEncoder_Params;
    EncoderBufferMapping *m_nbBufferMapping;

    OMX_BOOL isMetaDataEnabled;
};

inline OMX_BOOL VENCComponent::isMetaDataSet() const
{ return isMetaDataEnabled; }

inline HVAHEncoder_t **VENCComponent::getEncoderParam()
{ return &m_HVAHEncoder_Params; }

inline OMX_BOOL VENCComponent::isForceIntraEnabled() const
{ return m_isForceIntraEnabled; }

inline void VENCComponent::resetForceIntraEnabled() const
{ m_isForceIntraEnabled = OMX_FALSE; }

inline OMX_BOOL VENCComponent::isPrePendSPSPPS() const
{ return mPrependSPSPPS; }

inline const VidEncInputPort& VENCComponent::inputPort() const
{ return m_inputPort; }

inline const VidEncOutputPort& VENCComponent::outputPort() const
{ return m_outputPort; }

inline VidEncInputPort& VENCComponent::inputPort_()
{ return m_inputPort; }

inline VidEncOutputPort& VENCComponent::outputPort_()
{ return m_outputPort; }


inline Role&  VENCComponent::role()
{ return m_componentRole; }


#define OMX_ST_VIDEO_INPUT_PORT 0
#define OMX_ST_VIDEO_OUTPUT_PORT 1
#define OMX_ST_VIDEO_PORT_NUMBER 2



} // eof namespace stm

#endif  // _STM_VIDEOENCODER_HPP_
