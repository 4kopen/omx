/*
 * Copyright 2013 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file   VideoEncoderPort.cpp
 * @author STMicroelectronics
 */

#include <linux/dvb/stm_dvb.h>
#include <linux/dvb/stm_video.h>
#include <linux/videodev2.h>
#include <linux/dvb/dvb_v4l2_export.h>
#include <IDvbDevice.hpp>
#include "VideoEncoderPort.hpp"
#include "VideoEncoder.hpp"

#include "OMX_Video.h"
#include "OMX_VideoExt.h"
#include "OMX_debug.h"

#include <linux/videodev2.h>
#include <IDvbDevice.hpp>
#include <omxse.h>
#include  <fcntl.h>
#include <sys/ioctl.h>

#if HAVE_ASSERT_H
#include <assert.h>
#endif

#if HAVE_DBGTASSERT_H
// Re-direct assert on DBGT_ASSERT macro
#define assert(expr)  OMX_DBGT_ASSERT(expr)
#endif

#define DBGT_PREFIX "VENCP"
#define DBGT_LAYER 0
#include <linux/dbgt.h>

#define V4L2_CARD "STMicroelectronics"
#define ENCODER_DEVICE_NAME "Encoder"


namespace stm {

//==========================================================================
// VidEncPort::VidEncPort
// port constructor of OMX component
//==========================================================================

VidEncPort::VidEncPort(unsigned int nIdx, OMX_DIRTYPE aDir,
                       const VENCComponent& owner)
    :OmxPort(),
     m_owner(owner),
     m_PortDefinition(nIdx, aDir)
{
    //check the string length before copy
    assert(strlen(this->VidEnc().name()) <= sizeof(m_strName));
    strcpy(m_strName,this->VidEnc().name());
    OMXDEBUG_UPDATE_AV_TRACE_SETTINGS(this->VidEnc().name());
}

VidEncPort::~VidEncPort()
{}

const VideoPortDefinition& VidEncPort::definition() const
{ return m_PortDefinition; }

VideoPortDefinition& VidEncPort::definition()
{ return m_PortDefinition; }

const OmxComponent& VidEncPort::component() const
{ return m_owner; }

//==========================================================================
// VidEncOutputPort implementation
//==========================================================================

const OMX_VIDEO_CODINGTYPE  VidEncOutputPort::m_codingFormats[] = {
    OMX_VIDEO_CodingAVC
};

// BGRA8888 is the prefered pixel format returned in OMX output formats
const VidEncOutputPort::_nbProfileAndLevelSupported VidEncOutputPort::m_profilelevel[] = {
    {OMX_VIDEO_AVCProfileBaseline,OMX_VIDEO_AVCLevel31},
    {OMX_VIDEO_AVCProfileMain,OMX_VIDEO_AVCLevel31},
    {OMX_VIDEO_AVCProfileHigh,OMX_VIDEO_AVCLevel31},
};


VidEncOutputPort::VidEncOutputPort(unsigned int nIdx, OMX_DIRTYPE aDir,
                                 const VENCComponent& owner)
    :VidEncPort(nIdx, aDir, owner),
     m_dvb(0),
     puller_thread_id(0),
     m_bNativeWindowsEnabled(false),
     m_nReceivedFrame(0),
     m_bAfterEosFlag(false),
#ifdef ANDROID
     m_grallocModule(0),
     m_grallocDevice(0),
#endif
     mPortHVAHEncoder_Params(0),
     mapping(0),
     m_outputEncoderStarted(OMX_FALSE),
     m_isAllocateBuffer(OMX_FALSE),
     isSPSPPSSent(OMX_FALSE),
     tempOutputBuffer(0)

{
    this->definition().bufferCountMin(NUM_OUT_BUFFERS);
    this->definition().bufferCountActual(NUM_OUT_BUFFERS);
    this->definition().xFrameRate(15<<16);
    this->definition().colorFormat(OMX_COLOR_FormatUnused);
    this->definition().mimeType("video/H264");
    memset(&profile_level, 0, sizeof(h264_level_profile));

    OMX_DBGT_PINFO("%s Default color format is %s (eColorFormat=%d(0x%x)) "
               "(STOMXComponentAsync::create)",
               component().name(),
               this->definition().contentType(),
               this->definition().colorFormat(),
               this->definition().colorFormat());
}

VidEncOutputPort::~VidEncOutputPort()
{
    if (m_dvb) {
        delete m_dvb;
        m_dvb = 0;
    }
}

OMX_VIDEO_CODINGTYPE
VidEncOutputPort::supportedCodingFormat(unsigned int nIdx) const
{
    assert(nIdx < this->supportedCodingFormatNb());
    return m_codingFormats[nIdx];
}

unsigned int
VidEncOutputPort::supportedCodingFormatNb() const
{ return (sizeof(m_codingFormats) / sizeof(m_codingFormats[0])); }

bool VidEncOutputPort::isSupportedCodingFormat(OMX_VIDEO_CODINGTYPE aFmt) const
{
    bool ret = false;
    for (unsigned int i = 0; i < this->supportedCodingFormatNb(); i++) {
        if (this->supportedCodingFormat(i) == aFmt) {
            ret = true;
            break;
        }
    }
    return ret;
}

void VidEncOutputPort::setInternalPointers(HVAHEncoder_t **params, EncoderBufferMapping **buffermapping)
{
    OMX_DBGT_PROLOG();
    mPortHVAHEncoder_Params = *params;
    mapping = *buffermapping;
    OMX_DBGT_PTRACE("m_HVAHEncoder_Params : 0x%x m_nbBufferMapping : 0x%x",(unsigned int)mPortHVAHEncoder_Params,(unsigned int)mapping);
    setDefaultParameters();
    OMX_DBGT_EPILOG();
}


OMX_ERRORTYPE 
VidEncOutputPort::setDvbDevice(IDvbDevice **temp_m_dvb)
{
    int retVal = -1;
    OMX_DBGT_PROLOG();
    m_dvb = *temp_m_dvb;

    retVal = static_cast<DVBVideo_SF*>(m_dvb)->create(ENCODER_DEVICE_NAME, V4L2_CARD,0);
    if (retVal != 0)
    {
        OMX_DBGT_ERROR("Error create, return value %d",retVal);
        OMX_DBGT_EPILOG();
        return OMX_ErrorHardware;
    }

    retVal = static_cast<DVBVideo_SF*>(m_dvb)->SpecificationCheck();
    if (retVal != 0)
    {
        OMX_DBGT_ERROR("Error SpecificationCheck, return value %d",retVal);
        OMX_DBGT_EPILOG();
        return OMX_ErrorHardware;
    }

    retVal = static_cast<DVBVideo_SF*>(m_dvb)->ConfigureEncoder();
    if (retVal != 0)
    {
        OMX_DBGT_ERROR("Error ConfigureEncoder, return value %d",retVal);
        OMX_DBGT_EPILOG();
        return OMX_ErrorHardware;
    }


    static_cast<DVBVideo_SF*>(m_dvb)->configureEncoderStructure((void **)&mPortHVAHEncoder_Params,ENCODER_TYPE_H264ENC);

    retVal = static_cast<DVBVideo_SF*>(m_dvb)->FormatCheck();
    if (retVal != 0)
    {
        OMX_DBGT_ERROR("Error desired format NOT support for output, return value %d",retVal);
        OMX_DBGT_EPILOG();
        return OMX_ErrorHardware;
    }

    (*mPortHVAHEncoder_Params).width = this->definition().frameWidth();
    (*mPortHVAHEncoder_Params).height = this->definition().frameHeight();

    
    retVal = static_cast<DVBVideo_SF*>(m_dvb)->Configure(this->definition().frameWidth(),this->definition().frameHeight(),V4L2_PIX_FMT_H264,12);

    /* setting the buffer count values*/
    (*mPortHVAHEncoder_Params).num_output_buffers = this->definition().bufferCountActual();

    retVal = static_cast<DVBVideo_SF*>(m_dvb)->setParam();
    if (retVal != 0)
    {
        DBGT_ERROR("Error while setupBuffer to v4L for output, return value %d",retVal);
        OMX_DBGT_EPILOG();
        return OMX_ErrorHardware;
    }
  
    static_cast<DVBVideo_SF*>(m_dvb)->setLevelandProfile(profile_level);

    /* this API is to be called encder ourput only */
    retVal = static_cast<DVBVideo_SF*>(m_dvb)->setControls();
    if (retVal != 0)
    {
        OMX_DBGT_ERROR("Error while sending controls to v4L for output, return value %d",retVal);
        OMX_DBGT_EPILOG();
        return OMX_ErrorHardware;
    }

#ifdef ANDROID
    OMX_DBGT_CHECK_RETURN(! hw_get_module(GRALLOC_HARDWARE_MODULE_ID, (const hw_module_t **)&m_grallocModule),
                      OMX_ErrorInsufficientResources);

    OMX_DBGT_CHECK_RETURN(! gralloc_open((const hw_module_t *)m_grallocModule, &m_grallocDevice),
                      OMX_ErrorHardware);

    OMX_DBGT_PINFO("At output value of m_grallocModule : 0x%x m_grallocDevice 0x%x",(unsigned int)m_grallocModule,(unsigned int)m_grallocDevice);
#endif

    OMX_DBGT_EPILOG();
    return OMX_ErrorNone;
}

OMX_ERRORTYPE VidEncOutputPort::create()
{ return OMX_ErrorNone; }

int VidEncOutputPort::start()
{
    // Nothing to do on output port if tunneled
    if (this->isTunneled()) {
        puller_thread_id = 0;
    } else {
        OMX_DBGT_CHECK_RETURN(! pthread_create(&puller_thread_id, NULL, VidEncOutputPort::ThreadEntryPoint, this), OMX_ErrorInsufficientResources);
#ifdef ANDROID
        OMX_DBGT_CHECK_RETURN(! pthread_setname_np(puller_thread_id, "OMXVideoPull"), OMX_ErrorInsufficientResources);
#endif
    }
        return 0;
}

int VidEncOutputPort::stop()
{
    int retVal = -1;
    OMX_DBGT_PROLOG();

    retVal = static_cast<DVBVideo_SF*>(m_dvb)->stop();
    if (retVal < 0) 
    {
        OMX_DBGT_ERROR("Encoder Output Port Stop failed (%s)", strerror (errno));
    }
    OMX_DBGT_EPILOG("retVal : %d",retVal);
    return retVal;
}

void VidEncOutputPort::destroy()
{
    OMX_DBGT_PROLOG();

    int retVal = -1;

    if (m_dvb)
    {
        retVal = static_cast<DVBVideo_SF*>(m_dvb)->destroy();
        if (retVal < 0) {
            OMX_DBGT_ERROR("DVB  device for input port not closed");
        }
    }

    if (puller_thread_id != 0) {
        pthread_join(puller_thread_id, 0);
    }
    if(this->nbBuffers() > 0) {
        OMX_DBGT_ERROR("Allocated buffer on output port not freed");
    }
    OMX_DBGT_EPILOG();
}

void* VidEncOutputPort::ThreadEntryPoint(void* a_pThis)
{
    VidEncOutputPort* pThis = static_cast<VidEncOutputPort*>(a_pThis);
    pThis->threadFunction();
    return 0;
}

const OMX_VIDEO_PARAM_AVCTYPE VidEncOutputPort::mParamVideoAvcDefault =
{
    DEFAULT_PARAM_HEADER(OMX_VIDEO_PARAM_AVCTYPE,1),
    0,                              // nSliceHeaderSpacing
    1,                              // nPFrames
    0,                              // nBFrames
    OMX_TRUE,                       // bUseHadamard
    1,                              // nRefFrames
    0,                              // nRefIdx10ActiveMinus1
    0,                              // nRefIdx11ActiveMinus1
    OMX_FALSE,                      // bEnableUEP
    OMX_FALSE,                      // bEnableFMO
    OMX_FALSE,                      // bEnableASO
    OMX_FALSE,                      // bEnableRS
    OMX_VIDEO_AVCProfileBaseline,   // eProfile
    OMX_VIDEO_AVCLevel31,            // eLevel
    0,                              // nAllowedPictureTypes
    OMX_TRUE,                       // bFrameMBsOnly
    OMX_FALSE,                      // bMBAFF
    OMX_FALSE,                      // bEntropyCodingCABAC
    OMX_FALSE,                      // bWeightedPPrediction
    0,                              // nWeightedBipredicitonMode
    OMX_TRUE,                       // bconstIpred
    OMX_FALSE,                      // bDirect8x8Inference
    OMX_FALSE,                      // bDirectSpatialTemporal
    0,                              // nCabacInitIdc
    OMX_VIDEO_AVCLoopFilterDisable  // eLoopFilterMode
};

const OMX_VIDEO_PARAM_BITRATETYPE VidEncOutputPort::mParamVideoBitrateDefault =
{
    DEFAULT_PARAM_HEADER(OMX_VIDEO_PARAM_BITRATETYPE,1),
    OMX_Video_ControlRateVariable,   // eControlRate;
    64000                            // nTargetBitrate;
};


OMX_ERRORTYPE
VidEncOutputPort::getParameter(OMX_INDEXTYPE nParamIndex, OMX_PTR pParamStruct)
{
    OMX_DBGT_PROLOG("on Video encoder output port");
    switch ((int)nParamIndex)
    {
        case OMX_IndexParamPortDefinition: {
            // Get port definition
            ASSERT_STRUCT_TYPE(pParamStruct, OMX_PARAM_PORTDEFINITIONTYPE);
            OMX_PARAM_PORTDEFINITIONTYPE* param;
            param = (OMX_PARAM_PORTDEFINITIONTYPE*)pParamStruct;
            assert(param->nPortIndex == this->definition().index());
            
            // Provide current content of port and computes nBufferSize on top of it
            OMX_DBGT_PDEBUG("GetParameter(OMX_IndexParamPortDefinition) nPortIndex: %u"
                            " eDir: %d nBufferCountActual: %u nBufferCountMin: %u"
                            " nBufferSize: %u\n",
                            this->definition().index(),
                            this->definition().direction(),
                            this->definition().bufferCountActual(),
                            this->definition().bufferCountMin(),
                            this->definition().bufferSize());
            if ( this->definition().bufferSize() == 0 ) 
            {
                this->definition().bufferSize( (1920*1088*3/2)/2 );
                OMX_DBGT_PINFO("%s set nBufferSize to %uKB "
                       "(GetParameter(OMX_IndexParamPortDefinition input))",
                       component().name(), param->nBufferSize/1024);
            }

            *param = this->definition().getParamPortDef();
            OMX_DBGT_EPILOG();
            return OMX_ErrorNone;
        }

        case OMX_IndexParamVideoAvc: {
            ASSERT_STRUCT_TYPE(pParamStruct, OMX_VIDEO_PARAM_AVCTYPE);
            OMX_VIDEO_PARAM_AVCTYPE *param = (OMX_VIDEO_PARAM_AVCTYPE *)pParamStruct;
            SET_DEFAULT_PARAM(param,mParamVideoAvcDefault);
            param->bEntropyCodingCABAC = (OMX_BOOL)(*mPortHVAHEncoder_Params).EntropyMode;
            param->nBFrames = (*mPortHVAHEncoder_Params).nBFrameInterval;
            param->nPFrames = (*mPortHVAHEncoder_Params).IntraPeriod;
            switch (profile_level.LevelIDC)
            {
                case V4L2_MPEG_VIDEO_H264_LEVEL_3_1:
                    param->eLevel = OMX_VIDEO_AVCLevel31; break;
                case V4L2_MPEG_VIDEO_H264_LEVEL_3_2:
                    param->eLevel = OMX_VIDEO_AVCLevel32; break;
                case V4L2_MPEG_VIDEO_H264_LEVEL_4_0:
                    param->eLevel = OMX_VIDEO_AVCLevel4; break;
                case V4L2_MPEG_VIDEO_H264_LEVEL_4_1:
                    param->eLevel = OMX_VIDEO_AVCLevel41; break;
                case V4L2_MPEG_VIDEO_H264_LEVEL_4_2:
                    param->eLevel = OMX_VIDEO_AVCLevel42; break;
                case V4L2_MPEG_VIDEO_H264_LEVEL_5_1:
                    param->eLevel = OMX_VIDEO_AVCLevel51; break;
                default:
                    DBGT_ERROR("Unknown/Unhandled Level from v4L2 : 0x%x",(unsigned int)profile_level.LevelIDC); break;
            }
                
            switch (profile_level.ProfileIDC)
            {
                case V4L2_MPEG_VIDEO_H264_PROFILE_BASELINE:
                    param->eProfile = OMX_VIDEO_AVCProfileBaseline; break;
                case V4L2_MPEG_VIDEO_H264_PROFILE_MAIN:
                    param->eProfile = OMX_VIDEO_AVCProfileMain; break;
                case V4L2_MPEG_VIDEO_H264_PROFILE_HIGH:
                    param->eProfile = OMX_VIDEO_AVCProfileHigh; break;
                default:
                    DBGT_ERROR("Unknown/Unhandled Profile from v4L2 : 0x%x",(unsigned int)profile_level.ProfileIDC); break;
            }
            OMX_DBGT_EPILOG();
            return OMX_ErrorNone;
        }

        case OMX_IndexParamVideoBitrate: {
            ASSERT_STRUCT_TYPE(pParamStruct, OMX_VIDEO_PARAM_BITRATETYPE);
            OMX_VIDEO_PARAM_BITRATETYPE *aParamVideoBitrate = (OMX_VIDEO_PARAM_BITRATETYPE *)pParamStruct; 
            SET_DEFAULT_PARAM(aParamVideoBitrate,mParamVideoBitrateDefault);
            aParamVideoBitrate->nTargetBitrate = (*mPortHVAHEncoder_Params).BitRate;
            switch ((*mPortHVAHEncoder_Params).BitRateControlType)
            {
            case V4L2_MPEG_VIDEO_BITRATE_MODE_CBR: 
                    aParamVideoBitrate->eControlRate = OMX_Video_ControlRateConstant; break;
                    
            case V4L2_MPEG_VIDEO_BITRATE_MODE_VBR:
                    aParamVideoBitrate->eControlRate = OMX_Video_ControlRateVariable; break;
            }
            OMX_DBGT_EPILOG();
            return OMX_ErrorNone;
        }

        case OMX_IndexParamVideoPortFormat: {
            // Supported video port format
            ASSERT_STRUCT_TYPE(pParamStruct, OMX_VIDEO_PARAM_PORTFORMATTYPE);
            OMX_VIDEO_PARAM_PORTFORMATTYPE* param;
            param = (OMX_VIDEO_PARAM_PORTFORMATTYPE*)pParamStruct;
            assert(param->nPortIndex == this->definition().index());
            if (param->nIndex < this->supportedCodingFormatNb()) {
                param->eCompressionFormat = this->supportedCodingFormat(param->nIndex);
                param->eColorFormat = OMX_COLOR_FormatUnused;
                OMX_DBGT_EPILOG();
                return OMX_ErrorNone;
            } else {
                OMX_DBGT_EPILOG("No more supported coding format");
                return OMX_ErrorNoMore;
            }
        }

        case OMX_IndexParamVideoProfileLevelQuerySupported:{
            ASSERT_STRUCT_TYPE(pParamStruct, OMX_VIDEO_PARAM_PROFILELEVELTYPE);
            OMX_VIDEO_PARAM_PROFILELEVELTYPE *pt = (OMX_VIDEO_PARAM_PROFILELEVELTYPE *)pParamStruct;
            unsigned int nbProfileLevelSupported = sizeof(m_profilelevel) / sizeof(m_profilelevel[0]);

            if (pt->nProfileIndex >= nbProfileLevelSupported) {
                DBGT_WARNING("All supported profile/level have been supplied");
                OMX_DBGT_EPILOG();
                return OMX_ErrorNoMore;
            }
            pt->eProfile = m_profilelevel[pt->nProfileIndex].AVCprofile;
            pt->eLevel = m_profilelevel[pt->nProfileIndex].AVClevel;
            OMX_DBGT_EPILOG();
            return OMX_ErrorNone;
        }; break;

        case OMX_getAndroidNativeBufferUsage: {
            ASSERT_STRUCT_TYPE(pParamStruct, OMX_GetAndroidNativeBufferUsageParams);
            OMX_GetAndroidNativeBufferUsageParams* param;
            param = (OMX_GetAndroidNativeBufferUsageParams*)pParamStruct;
            OMX_DBGT_PDEBUG("VENCComponent::input port get parameter OMX_getAndroidNativeBufferUsage");
            OMX_DBGT_ERROR("::OMX_getAndroidNativeBufferUsage %s not set on output (index=%u)",
                           component().name(), param->nPortIndex);
            OMX_DBGT_EPILOG();
            return OMX_ErrorBadPortIndex;
        }; break;
        
        case OMX_IndexParamOtherPortFormat:
        case OMX_IndexParamAudioPortFormat:
        case OMX_IndexParamImagePortFormat:
        default: {
            OMX_DBGT_ERROR("Unsupported index");
            OMX_DBGT_EPILOG("Unsupported index");
            return OMX_ErrorUnsupportedIndex;
        }; break;
    }
}

void VidEncOutputPort::codingFormat(OMX_VIDEO_CODINGTYPE aFmt)
{
    assert(isSupportedCodingFormat(aFmt));
    this->definition().compressionFormat(aFmt);
}

//==========================================================
// VENCComponent::setDefaultParameters
//==========================================================
void VidEncOutputPort::setDefaultParameters()
{
    profile_level.LevelIDC = V4L2_MPEG_VIDEO_H264_LEVEL_3_1;
    profile_level.ProfileIDC = V4L2_MPEG_VIDEO_H264_PROFILE_BASELINE;
    (*mPortHVAHEncoder_Params).IntraPeriod = 15;
    (*mPortHVAHEncoder_Params).GOPSize = 15;
    (*mPortHVAHEncoder_Params).FrameRate_Num = 30;
    (*mPortHVAHEncoder_Params).FrameRate_Den = 1;
    (*mPortHVAHEncoder_Params).width = 176;
    (*mPortHVAHEncoder_Params).height = 144;
    (*mPortHVAHEncoder_Params).BitRateControlType = V4L2_MPEG_VIDEO_BITRATE_MODE_VBR;
    (*mPortHVAHEncoder_Params).BitRate = 64000;
    (*mPortHVAHEncoder_Params).CpbBufferSize = 64000;
    (*mPortHVAHEncoder_Params).CbrDelay = 1;
    (*mPortHVAHEncoder_Params).qpmin = 6;
    (*mPortHVAHEncoder_Params).qpmax = 45;
//    (*mPortHVAHEncoder_Params).AspectRatio = 
    (*mPortHVAHEncoder_Params).nBFrameInterval=10;
    (*mPortHVAHEncoder_Params).EntropyMode = 0;//0 CAVLC , 1 CABAC
    (*mPortHVAHEncoder_Params).inputBufferSize = 0;
    (*mPortHVAHEncoder_Params).outputBufferSize = 0;
    (*mPortHVAHEncoder_Params).outputBufferType = 0;
    (*mPortHVAHEncoder_Params).forceIntra = 0;
    (*mPortHVAHEncoder_Params).isOutputPortBufferAllocated = OMX_FALSE;
    (*mPortHVAHEncoder_Params).isInputPortBufferAllocated = OMX_FALSE;
    (*mPortHVAHEncoder_Params).input_buffers = NULL;
    (*mPortHVAHEncoder_Params).output_buffers = NULL;
}

//==========================================================
// VidEncOutputPort::updateAVCParams
// checks and updates AVC param values
//==========================================================
OMX_ERRORTYPE
VidEncOutputPort::updateAVCParams(OMX_VIDEO_PARAM_AVCTYPE *aParamVideoAvc)
{
    OMX_DBGT_PROLOG();
    OMX_ERRORTYPE eError = OMX_ErrorNone;
    OMX_DBGT_CHECK_EXIT(aParamVideoAvc->nBFrames                    == 0,                               OMX_ErrorBadParameter);
    OMX_DBGT_CHECK_EXIT(aParamVideoAvc->bUseHadamard                == OMX_TRUE,                        OMX_ErrorBadParameter);
    OMX_DBGT_CHECK_EXIT(aParamVideoAvc->nRefFrames                  == 1,                               OMX_ErrorBadParameter);
    OMX_DBGT_CHECK_EXIT(aParamVideoAvc->nRefIdx10ActiveMinus1       == 0,                               OMX_ErrorBadParameter);
    OMX_DBGT_CHECK_EXIT(aParamVideoAvc->nRefIdx11ActiveMinus1       == 0,                               OMX_ErrorBadParameter);
    OMX_DBGT_CHECK_EXIT(aParamVideoAvc->bEnableUEP                  == OMX_FALSE,                       OMX_ErrorBadParameter);
    OMX_DBGT_CHECK_EXIT(aParamVideoAvc->bEnableFMO                  == OMX_FALSE,                       OMX_ErrorBadParameter);
    OMX_DBGT_CHECK_EXIT(aParamVideoAvc->bEnableASO                  == OMX_FALSE,                       OMX_ErrorBadParameter);
    OMX_DBGT_CHECK_EXIT(aParamVideoAvc->bEnableRS                   == OMX_FALSE,                       OMX_ErrorBadParameter);
    OMX_DBGT_CHECK_EXIT( (aParamVideoAvc->eProfile == OMX_VIDEO_AVCProfileBaseline ) ||
                         (aParamVideoAvc->eProfile == OMX_VIDEO_AVCProfileMain ) ||
                         (aParamVideoAvc->eProfile == OMX_VIDEO_AVCProfileHigh ),                       OMX_ErrorBadParameter);
    OMX_DBGT_CHECK_EXIT(aParamVideoAvc->bFrameMBsOnly               == OMX_TRUE,                        OMX_ErrorBadParameter);
    OMX_DBGT_CHECK_EXIT(aParamVideoAvc->bMBAFF                      == OMX_FALSE,                       OMX_ErrorBadParameter);
    OMX_DBGT_CHECK_EXIT(aParamVideoAvc->bEntropyCodingCABAC         == OMX_FALSE,                       OMX_ErrorBadParameter);
    OMX_DBGT_CHECK_EXIT(aParamVideoAvc->bWeightedPPrediction        == OMX_FALSE,                       OMX_ErrorBadParameter);
    OMX_DBGT_CHECK_EXIT(aParamVideoAvc->nWeightedBipredicitonMode   == 0,                               OMX_ErrorBadParameter);
    OMX_DBGT_CHECK_EXIT(aParamVideoAvc->bDirect8x8Inference         == OMX_FALSE,                       OMX_ErrorBadParameter);
    OMX_DBGT_CHECK_EXIT(aParamVideoAvc->bDirectSpatialTemporal      == OMX_FALSE,                       OMX_ErrorBadParameter);
    OMX_DBGT_CHECK_EXIT(aParamVideoAvc->nCabacInitIdc               == 0,                               OMX_ErrorBadParameter);
    OMX_DBGT_CHECK_EXIT( (aParamVideoAvc->eLoopFilterMode == OMX_VIDEO_AVCLoopFilterEnable)  ||
                         (aParamVideoAvc->eLoopFilterMode == OMX_VIDEO_AVCLoopFilterDisable) ||
                         (aParamVideoAvc->eLoopFilterMode == OMX_VIDEO_AVCLoopFilterDisableSliceBoundary), OMX_ErrorBadParameter);

    /* after validation of parameter, updating the structure */
    (*mPortHVAHEncoder_Params).EntropyMode = (OMX_U16)aParamVideoAvc->bEntropyCodingCABAC;
    (*mPortHVAHEncoder_Params).nBFrameInterval = aParamVideoAvc->nBFrames;
    (*mPortHVAHEncoder_Params).IntraPeriod = aParamVideoAvc->nPFrames;
    switch (aParamVideoAvc->eLevel)
    {
        case OMX_VIDEO_AVCLevel31:
            profile_level.LevelIDC = V4L2_MPEG_VIDEO_H264_LEVEL_3_1; break;
        case OMX_VIDEO_AVCLevel32:
            profile_level.LevelIDC = V4L2_MPEG_VIDEO_H264_LEVEL_3_2; break;
        case OMX_VIDEO_AVCLevel4:
            profile_level.LevelIDC = V4L2_MPEG_VIDEO_H264_LEVEL_4_0; break;
        case OMX_VIDEO_AVCLevel41:
            profile_level.LevelIDC = V4L2_MPEG_VIDEO_H264_LEVEL_4_1; break;
        case OMX_VIDEO_AVCLevel42:
            profile_level.LevelIDC = V4L2_MPEG_VIDEO_H264_LEVEL_4_2; break;
        case OMX_VIDEO_AVCLevel51:
            profile_level.LevelIDC = V4L2_MPEG_VIDEO_H264_LEVEL_5_1; break;
        default:
            DBGT_ERROR("Unknown/Unhandled Level : 0x%x",aParamVideoAvc->eLevel); break;
    }

    switch(aParamVideoAvc->eProfile)
    {
        case OMX_VIDEO_AVCProfileBaseline:
            profile_level.ProfileIDC = V4L2_MPEG_VIDEO_H264_PROFILE_BASELINE; break;
        case OMX_VIDEO_AVCProfileMain:
            profile_level.ProfileIDC = V4L2_MPEG_VIDEO_H264_PROFILE_MAIN; break;
        case OMX_VIDEO_AVCProfileHigh:
            profile_level.ProfileIDC = V4L2_MPEG_VIDEO_H264_PROFILE_HIGH; break;
        default:
            DBGT_ERROR("Unknown/Unhandled Profile : 0x%x",aParamVideoAvc->eProfile); break;

    }

EXIT:
    OMX_DBGT_EPILOG();
    return eError;
}



OMX_ERRORTYPE
VidEncOutputPort::setParameter(OMX_INDEXTYPE nParamIndex, OMX_PTR pParamStruct)
{
    OMX_DBGT_PROLOG("on Video encoder ouptut port");
    switch ((int)nParamIndex)
    {
        case OMX_IndexParamVideoPortFormat: {
            // Set video port format
            ASSERT_STRUCT_TYPE(pParamStruct, OMX_VIDEO_PARAM_PORTFORMATTYPE);
            OMX_VIDEO_PARAM_PORTFORMATTYPE* param;
            param = (OMX_VIDEO_PARAM_PORTFORMATTYPE*)pParamStruct;
            assert(param->nPortIndex == this->definition().index());
            
            if (!isSupportedCodingFormat(param->eCompressionFormat)) {
                // Unsupported compression format
                OMX_DBGT_ERROR("%s eCompressionFormat not supported (%d)",
                               component().name(), param->eCompressionFormat);
                OMX_DBGT_EPILOG("Unsupported video compression format");
                return OMX_ErrorUnsupportedSetting;
            }
            
            // Apply format on port
            this->codingFormat(param->eCompressionFormat);
            this->definition().frameRate(param->xFramerate);
            OMX_DBGT_PINFO("%s compression format changed to %s (eCompressionFormat=%d(0x%x))",
                           component().name(),
                           this->definition().contentType(),
                           this->definition().compressionFormat(),
                           this->definition().compressionFormat());

            OMX_DBGT_EPILOG("Set port compression format %u", param->eColorFormat);
            return OMX_ErrorNone;
        };break;

        case OMX_IndexParamPortDefinition: {
            // Set port definition
            ASSERT_STRUCT_TYPE(pParamStruct, OMX_PARAM_PORTDEFINITIONTYPE);
            OMX_PARAM_PORTDEFINITIONTYPE* param;
            param = (OMX_PARAM_PORTDEFINITIONTYPE*)pParamStruct;
            assert(param->nPortIndex == this->definition().index());
            
            if (this->definition().bufferSize() != param->nBufferSize) {
                OMX_DBGT_PINFO("%s changing nBufferSize from %uKB to %uKB "
                               "(SetParameter(OMX_IndexParamPortDefinition input))",
                               component().name(),
                               this->definition().bufferSize()/1024,
                               param->nBufferSize/1024);
            }
            this->definition().setParamPortDef(param);
            OMX_DBGT_EPILOG();
            return OMX_ErrorNone;
        }; break;

        case OMX_IndexParamVideoIntraRefresh: {
            ASSERT_STRUCT_TYPE(pParamStruct, OMX_VIDEO_PARAM_INTRAREFRESHTYPE);
            OMX_VIDEO_PARAM_INTRAREFRESHTYPE *param = (OMX_VIDEO_PARAM_INTRAREFRESHTYPE *)pParamStruct;
			switch (param->eRefreshMode)
			{
				case OMX_VIDEO_IntraRefreshCyclic:
				    (*mPortHVAHEncoder_Params).intrarefreshmode = INTRAREFRESH_TYPE_CIR;
				    (*mPortHVAHEncoder_Params).IrParamOption = param->nCirMBs;
				    break;
				case OMX_VIDEO_IntraRefreshAdaptive:
				    (*mPortHVAHEncoder_Params).intrarefreshmode = INTRAREFRESH_TYPE_AIR;
				    (*mPortHVAHEncoder_Params).IrParamOption = param->nAirMBs;
				    break;
				default: 
                    DBGT_ERROR("Other IntraRefresh modes are not supported");
                    break;
			}
            OMX_DBGT_EPILOG();
            return OMX_ErrorNone;
		}; break;

        case OMX_IndexParamVideoAvc: {
            ASSERT_STRUCT_TYPE(pParamStruct, OMX_VIDEO_PARAM_AVCTYPE);
            OMX_VIDEO_PARAM_AVCTYPE *aParamVideoAvc = (OMX_VIDEO_PARAM_AVCTYPE *)pParamStruct; 
            updateAVCParams(aParamVideoAvc);
            OMX_DBGT_EPILOG();
            return OMX_ErrorNone;
        }; break;

        case OMX_IndexParamVideoBitrate: {
            ASSERT_STRUCT_TYPE(pParamStruct, OMX_VIDEO_PARAM_BITRATETYPE);
            OMX_VIDEO_PARAM_BITRATETYPE *aParamVideoBitrate = (OMX_VIDEO_PARAM_BITRATETYPE *)pParamStruct; 
            if (aParamVideoBitrate->nPortIndex != OMX_ST_VIDEO_OUTPUT_PORT) {
                DBGT_ERROR("Should only be called on Output Port");
                return OMX_ErrorBadPortIndex;
            }
            (*mPortHVAHEncoder_Params).BitRate = aParamVideoBitrate->nTargetBitrate;
            switch (aParamVideoBitrate->eControlRate)
            {
            case OMX_Video_ControlRateVariable: 
            case OMX_Video_ControlRateVariableSkipFrames:
                (*mPortHVAHEncoder_Params).BitRateControlType = V4L2_MPEG_VIDEO_BITRATE_MODE_VBR;
                break;

            case OMX_Video_ControlRateConstant:
            case OMX_Video_ControlRateConstantSkipFrames:
                (*mPortHVAHEncoder_Params).BitRateControlType = V4L2_MPEG_VIDEO_BITRATE_MODE_CBR;
                break;
            default:
                DBGT_ERROR("Other Bitrate Modes 0x%x Not SUPPORTED",aParamVideoBitrate->eControlRate);
                break;
            }
            OMX_DBGT_EPILOG();
            return OMX_ErrorNone;
        }; break;


        case OMX_enableAndroidNativeBuffers: {
            OMX_DBGT_EPILOG();
            return OMX_ErrorNone;
        }; break;

        case OMX_IndexParamOtherPortFormat:
        case OMX_IndexParamAudioPortFormat:
        case OMX_IndexParamImagePortFormat:
        default: {
            OMX_DBGT_ERROR("Unsupported index");
            OMX_DBGT_EPILOG("Unsupported index");
            return OMX_ErrorUnsupportedIndex;
        }; break;
    }
}

OMX_U32 
VidEncOutputPort::getVirtualAddress(OMX_BUFFERHEADERTYPE**  ppBufferHdr)
{
	OMX_U32 tempValue = 0;
	OMX_DBGT_PROLOG();

    OMX_DBGT_PTRACE("ppBufferHdr : 0x%x ppBufferHdr->pBuffer : 0x%x   ",(unsigned int)*ppBufferHdr,(unsigned int)(*ppBufferHdr)->pBuffer);

#ifdef ANDROID
    if(this->grallocModule()->lock(grallocModule(),
                (buffer_handle_t)((*ppBufferHdr)->pBuffer),
                GRALLOC_USAGE_HW_2D | GRALLOC_USAGE_EXTERNAL_DISP | GRALLOC_USAGE_SW_WRITE_MASK,
                0, 0,
                this->definition().frameWidth(),
                this->definition().frameHeight(),
                &((*ppBufferHdr)->pPlatformPrivate)) != 0)
    {
        OMX_DBGT_ERROR("grallocModule->lock failed");
        goto EXIT_ERROR;
	}

    if(this->grallocModule()->unlock(grallocModule(), (buffer_handle_t)((*ppBufferHdr)->pBuffer)) != 0)
    {
        OMX_DBGT_ERROR("grallocModule->unlock failed");
        goto EXIT_ERROR;
	}
#endif

	tempValue = (OMX_U32)(*ppBufferHdr)->pPlatformPrivate;

EXIT_ERROR:
    OMX_DBGT_EPILOG("tempValue : 0x%x",(unsigned int)tempValue);
	return tempValue;
}


OMX_ERRORTYPE
VidEncOutputPort::doOutputPortExtraProcessing(OMX_BUFFERHEADERTYPE**  ppBufferHdr,OMX_U32 nSizeBytes)
{
    int retVal = -1;
    int i;
    unsigned int bufferCount;
    OMX_ERRORTYPE eError = OMX_ErrorNone;
    OMX_DBGT_PROLOG();
    OMX_U32 tempVirtualaddr = 0;

    if (!m_isAllocateBuffer) {
        tempVirtualaddr = getVirtualAddress(ppBufferHdr);
    }

    if ((tempVirtualaddr == 0) || (m_isAllocateBuffer))
    {
        (*mPortHVAHEncoder_Params).isOutputPortBufferAllocated = OMX_TRUE;
        OMX_DBGT_PTRACE("Virtual address is : 0x%x and m_isAllocateBuffer : %d",(unsigned int)tempVirtualaddr,m_isAllocateBuffer);
    }

    if (!mPortHVAHEncoder_Params->output_buf_allocated)
    {
         retVal = static_cast<DVBVideo_SF*>(m_dvb)->setupBufferCount();

         if (retVal != 0)
         {
            OMX_DBGT_ERROR("Error while setupBuffer to v4L for output, return value %d",retVal);
            return OMX_ErrorHardware;
         }
             
         if ((*mPortHVAHEncoder_Params).isOutputPortBufferAllocated)
         {             
             /* getting the input buffers and associating them to omx buffers */
             retVal = static_cast<DVBVideo_SF*>(m_dvb)->setUpBufferPointer();
             if (retVal != 0)
             {
                 OMX_DBGT_ERROR("Error while setUpBufferPointer to v4L for output, return value %d",retVal);
                 return OMX_ErrorHardware;
             }                 
         }
         mPortHVAHEncoder_Params->output_buf_allocated = true;
    }

    for (i=0;i<MAX_INPUT_OUTPUT_BUFFERS;i++)
    {
        if (mapping[i].isAssociated == false)
            break;
    }

    if (i == MAX_INPUT_OUTPUT_BUFFERS)
    {
        OMX_DBGT_CRITICAL("No free buffer available for use - input");
        return OMX_ErrorInsufficientResources;
    }
    for (bufferCount = 0;bufferCount<(*mPortHVAHEncoder_Params).num_output_buffers ; bufferCount++)
    {
        if ((*mPortHVAHEncoder_Params).output_buffers[bufferCount].isUsed == 0)
            break;
    }

    // forming an assoication between input buffer and encoder internally allocated buffer
    mapping[i].isAssociated = (OMX_BOOL)true;
    (*mPortHVAHEncoder_Params).output_buffers[bufferCount].isUsed = (OMX_BOOL)true;
    mapping[i].buffProperty = 1;
    mapping[i].omxBuffer = *ppBufferHdr;
    mapping[i].internalBuffer =&((*mPortHVAHEncoder_Params).output_buffers[bufferCount]);

    if (tempVirtualaddr)
    {
        (*mPortHVAHEncoder_Params).output_buffers[bufferCount].start = (void *)tempVirtualaddr;
        OMX_DBGT_PTRACE("Assigning virtual address to start pointer");
    }

    if (m_isAllocateBuffer) 
    {
        (*ppBufferHdr)->pBuffer = (OMX_U8 *)(*mPortHVAHEncoder_Params).output_buffers[bufferCount].start;
    }
    OMX_DBGT_PINFO("Input buffer count : %d virtual address 0x%x omx buffer : 0x%x location : %d",bufferCount,(unsigned int)(*mPortHVAHEncoder_Params).output_buffers[bufferCount].start,(unsigned int)*ppBufferHdr,i);

    OMX_DBGT_EPILOG();
    return eError;   	
}

OMX_ERRORTYPE
VidEncOutputPort::useBuffer(OMX_BUFFERHEADERTYPE** ppBufferHdr,
                            OMX_PTR pAppPrivate,
                            OMX_U32 nSizeBytes,
                            OMX_U8* pBuffer)
{
    OMX_ERRORTYPE eError = OMX_ErrorNone;
    OMX_DBGT_PROLOG("Using buffer %p on video encoder ouput port", pBuffer);

    // Invalid case in proprietary tunneling
    assert(!this->isTunneled());

    // Allocate a buffer header
    *ppBufferHdr = 0;
    OMX_BUFFERHEADERTYPE* pBufferHdr = allocateBufferHeader(pAppPrivate,
                                                            nSizeBytes);
    if (!pBufferHdr) {
        eError = OMX_ErrorInsufficientResources;
        OMX_DBGT_ERROR("Can not allocate buffer header");
        OMX_DBGT_EPILOG("Return error %s", OMX_TYPE_TO_STR(OMX_ERRORTYPE, eError));
        return eError;
    }

    pBufferHdr->pBuffer = pBuffer;
    *ppBufferHdr = pBufferHdr;
    (*ppBufferHdr)->pPlatformPrivate = 0;

    // Increment internal buffer counter.
    this->incrementNbBuffers();

    if (VidEnc().IsConfigureDone == false) {
        OMX_DBGT_PINFO("Blocking allocateBuffer before venc configure");
        VidEnc().m_condConfig.wait(VidEnc().m_mutexCmd);
        OMX_DBGT_PINFO("Unblocking allocateBuffer");
    }

    eError = doOutputPortExtraProcessing(ppBufferHdr,nSizeBytes);
    OMX_DBGT_EPILOG();
    return eError;
}

OMX_ERRORTYPE
VidEncOutputPort::allocateBuffer(OMX_BUFFERHEADERTYPE** ppBufferHdr,
                                 OMX_PTR pAppPrivate,
                                 OMX_U32 nSizeBytes)
{
    // Allows to allocate and to return a frame buffer to the player
    OMX_ERRORTYPE eError = OMX_ErrorNone;
#ifdef ANDROID
    buffer_handle_t pPlatformPrivate;
#else
    void * pPlatformPrivate = NULL;
#endif
    OMX_DBGT_PROLOG("Allocating buffer on video encoder ouput port");

    m_isAllocateBuffer = OMX_TRUE;

    // Invalid case in proprietary tunneling
    assert(!this->isTunneled());

    // Allocate a buffer header
    *ppBufferHdr = 0;
    OMX_BUFFERHEADERTYPE* pBufferHdr = allocateBufferHeader(pAppPrivate,
                                                            nSizeBytes);
    if (!pBufferHdr) {
        eError = OMX_ErrorInsufficientResources;
        OMX_DBGT_ERROR("Can not allocate buffer header");
        OMX_DBGT_EPILOG("Return error %s", OMX_TYPE_TO_STR(OMX_ERRORTYPE, eError));
        return eError;
    }

    *ppBufferHdr = pBufferHdr;

    if (VidEnc().IsConfigureDone == false) {
        OMX_DBGT_PINFO("Blocking allocateBuffer before venc configure");
        VidEnc().m_condConfig.wait(VidEnc().m_mutexCmd);
        OMX_DBGT_PINFO("Unblocking allocateBuffer");
    }

    // Increment internal buffer counter.
    this->incrementNbBuffers();
    eError = doOutputPortExtraProcessing(ppBufferHdr,nSizeBytes);

    OMX_DBGT_EPILOG();
    return eError;
}

OMX_ERRORTYPE
VidEncOutputPort::freeBuffer(OMX_BUFFERHEADERTYPE* pBufferHdr)
{
    OMX_DBGT_PROLOG();
    assert(!isTunneled());
    int retVal = -1;
    retVal = static_cast<DVBVideo_SF*>(m_dvb)->freeBufferPointer();
    if (retVal < 0)
    {
            OMX_DBGT_ERROR("Encoder Output Port v4l unmap failed (%s)", strerror (errno));
    }

    freeBufferHeader(pBufferHdr);
    this->decrementNbBuffers();

    OMX_DBGT_EPILOG();
    return OMX_ErrorNone;
}

OMX_ERRORTYPE
VidEncOutputPort::emptyThisBuffer(OMX_BUFFERHEADERTYPE* pBufferHdr)
{
    assert(0 && "Invalid call on output port");
    return OMX_ErrorNone;
}

OMX_ERRORTYPE
VidEncOutputPort::fillThisBuffer(OMX_BUFFERHEADERTYPE* pBufferHdr)
{
    assert(this->isTunneled() == false);

     // Called by player to send an empty buffer to be filled with a
     // encoded video frame
     // This buffer is send to omxse
     OMX_DBGT_PROLOG();
     if (isAfterEosFlag())
     {
         // The last encoded video frame has already been returned to the player.
         // The buffer is send back empty to the player
         pBufferHdr->nFlags |= OMX_BUFFERFLAG_EOS;
         pBufferHdr->nTimeStamp = 0;
         pBufferHdr->nFilledLen = 0;
         this->VidEnc().notifyFillBufferDone(pBufferHdr);
     }
     else
     {
        if (!isSPSPPSSent)
        {
            OMX_DBGT_PINFO("Locking buffer for SPS/PPS Data buffer : 0x%x pBuffer 0x%x",(unsigned int)pBufferHdr,(unsigned int)pBufferHdr->pBuffer);
            tempOutputBuffer = pBufferHdr;
            isSPSPPSSent = OMX_TRUE;
        }
        else
        {
            this->push(pBufferHdr);
        }   
     }
     OMX_DBGT_EPILOG();
    return OMX_ErrorNone;
}

int VidEncOutputPort::disconnectAssociation(void *Inputbuffer)
{
    int i;
    int retVal = -1;
    for (i=0;i<MAX_INPUT_OUTPUT_BUFFERS;i++)
    {
        if ((OMX_BUFFERHEADERTYPE *)Inputbuffer == mapping[i].omxBuffer)
        {
            mapping[i].isAssociated = (OMX_BOOL)false;
            retVal = 0;
            OMX_DBGT_PTRACE("Buffer : 0x%x at location : %d disassociated",(unsigned int)Inputbuffer,i);
            break;
        }
    }
    return retVal;
}


OMX_ERRORTYPE VidEncOutputPort::flush()
{
    OMX_DBGT_PROLOG("Flush video encoder output port");
    if (isTunneled()) {
        // Proprietary tunneling. Nothing to flush
        OMX_DBGT_EPILOG("Port is tunneled.");
        return OMX_ErrorNone;
    }

    this->getRawFifos()->lock();

     OMX_BUFFERHEADERTYPE *buf  = 0;
     while(this->getRawFifos()->tryPop(buf) == true) {
         buf->nFilledLen = 0;
         this->VidEnc().notifyFillBufferDone(buf);
     }
     this->getRawFifos()->unlock();
     buf  = 0;
     buf = this->relockCurrent();
     if(buf != NULL)
     {
         this->VidEnc().notifyFillBufferDone(buf);
     }
     this->freeAndUnlockCurrent();

    OMX_DBGT_EPILOG();
    return OMX_ErrorNone;
}

//=============================================================================
//  Output port core function of Pulling thread
//  Infinite loop: Wait for encoded frame from SE and send them to Player
//=============================================================================
void VidEncOutputPort::threadFunction()
{
    OMX_DBGT_PROLOG();
    struct buffer_t *internalBufferPtr;
    int retVal = -1;

    while(true)
    {
        if (m_bAfterEosFlag)
        {
            goto EXIT;
        }
        else
        {
            // Wait for a 'FillThisBuffer' from player
            OMX_BUFFERHEADERTYPE *pOutBufHdr = this->popAndLockCurrent();
            if (pOutBufHdr)
            {
                OMX_DBGT_PINFO("Output buffer : 0x%x actual 0x%x length : %d timestamp : 0x%x flags : %d",(unsigned int)pOutBufHdr,(unsigned int)pOutBufHdr->pBuffer,(int)pOutBufHdr->nFilledLen,(int)pOutBufHdr->nTimeStamp,(int)pOutBufHdr->nFlags);
                retVal = findAssociation((void *)pOutBufHdr, 0,(void **)&internalBufferPtr);
                if (retVal < 0)
                {
                    OMX_DBGT_CRITICAL("OMX to Internal Buffer Association NOT found");
                    goto EXIT;
                }
                OMX_DBGT_PINFO("Status of internal buffer isUsed (should be false) : %d",(int)internalBufferPtr->isUsed);
                OMX_DBGT_PINFO("To check v4l2 type : %d, memory : %d index : %d start addr 0x%x len : %d",
                    (int)internalBufferPtr->v4l2_buf.type,
                    (int)internalBufferPtr->v4l2_buf.memory,
                    (int)internalBufferPtr->v4l2_buf.index,
                    (unsigned int)internalBufferPtr->start,
                    (int)internalBufferPtr->length);
                uint64_t temp=0;
                if(static_cast<DVBVideo_SF*>(m_dvb)->QueueBuffer((void *)internalBufferPtr,(int &)(pOutBufHdr->nFilledLen),temp) != 0)
                {
                    OMX_DBGT_ERROR("Encoder Output Port initial QBUF failed (%s)", strerror (errno));
                    goto EXIT;
                }
                OMX_DBGT_PINFO("Status of internal buffer isUsed (should be TRUE) : %d",(int)internalBufferPtr->isUsed);
    
                if (!m_outputEncoderStarted)
                {
                    OMX_DBGT_PINFO("Abt to call start for output");
                    if(static_cast<DVBVideo_SF*>(m_dvb)->start() != 0)
                    {
                        OMX_DBGT_ERROR("Encoder Output Port Start failed (%s)", strerror (errno));
                        goto EXIT;
                    }
                    m_outputEncoderStarted = OMX_TRUE;
                    OMX_DBGT_PINFO("Encoder output buffer started");
                }
                
                //Now dequeue the buffer
                retVal = static_cast<DVBVideo_SF*>(m_dvb)->DequeueEncoderBuffer(internalBufferPtr);
                if (retVal < 0)
                {
                    OMX_DBGT_ERROR("!!FATAL!! Error while DequeueBuffer for output buffer");
                }
                pOutBufHdr->nTimeStamp = (internalBufferPtr->v4l2_buf.timestamp.tv_sec * 1000000) + internalBufferPtr->v4l2_buf.timestamp.tv_usec;
                pOutBufHdr->nFilledLen = internalBufferPtr->v4l2_buf.bytesused;
                pOutBufHdr->nOffset = 0;
                
                if (((*mPortHVAHEncoder_Params).isOutputPortBufferAllocated) && (!m_isAllocateBuffer))
                {
					OMX_DBGT_PINFO("Performing memcpy for output buffer");
                    memcpy(pOutBufHdr->pBuffer,internalBufferPtr->start,pOutBufHdr->nFilledLen);
                }
                
                if ((*mPortHVAHEncoder_Params).outputBufferType & V4L2_BUF_FLAG_KEYFRAME)
                {
                    OMX_DBGT_PINFO("Output picture type is I");
                }
                if ((*mPortHVAHEncoder_Params).outputBufferType & V4L2_BUF_FLAG_PFRAME)
                {
                    OMX_DBGT_PINFO("Output picture type is P");
                }
                
                if ((*mPortHVAHEncoder_Params).outputBufferType & V4L2_BUF_FLAG_BFRAME)
                {
                    OMX_DBGT_PINFO("Output picture type is B");
                }

                pOutBufHdr->nFlags |= OMX_BUFFERFLAG_ENDOFFRAME;                 
                OMX_DBGT_PINFO("Output buffer filled len : %d timestamp : %d buffType : 0x%x",(int)pOutBufHdr->nFilledLen,(int)pOutBufHdr->nTimeStamp,(unsigned int)(*mPortHVAHEncoder_Params).outputBufferType);
                //ZERO length marks the last frame
                if (pOutBufHdr->nFilledLen == 0)
                {
                    pOutBufHdr->nFlags |= OMX_BUFFERFLAG_EOS;
                    m_bAfterEosFlag = OMX_TRUE;
                    OMX_DBGT_PINFO("Abt to send event buffer flag");
                    this->VidEnc().notifyEventHandler(OMX_EventBufferFlag, OMX_ST_VIDEO_OUTPUT_PORT, OMX_BUFFERFLAG_EOS, NULL);
                }
#ifdef ENABLE_FILE_DUMPING
                dump_to_file(mydump3,  (uint8_t *)(pOutBufHdr->pBuffer+pOutBufHdr->nOffset), pOutBufHdr->nFilledLen);
#endif
                if (!(*mPortHVAHEncoder_Params).isOutputPortBufferAllocated)
                {
                    disconnectAssociation((void *)pOutBufHdr);
                }
                internalBufferPtr->isUsed = false;
                if (tempOutputBuffer)
                {
                    OMX_DBGT_PINFO("Sending SPS/PPS first output : 0x%x pBuffer 0x%x",(unsigned int)tempOutputBuffer,(unsigned int)tempOutputBuffer->pBuffer);
                    memcpy(tempOutputBuffer->pBuffer,pOutBufHdr->pBuffer,pOutBufHdr->nFilledLen);
                    isSPSPPSSent = OMX_TRUE;
                    tempOutputBuffer->nFilledLen = pOutBufHdr->nFilledLen;
                    tempOutputBuffer->nFlags = OMX_BUFFERFLAG_CODECCONFIG;
                    this->VidEnc().notifyFillBufferDone(tempOutputBuffer);
                    tempOutputBuffer = NULL;

                }

                this->VidEnc().notifyFillBufferDone(pOutBufHdr);
                internalBufferPtr->isUsed = false;
                OMX_DBGT_PINFO("Output buffer rel : 0x%x and isUsed : %d",(unsigned int)pOutBufHdr,(int)internalBufferPtr->isUsed);
            }
            this->freeAndUnlockCurrent();
        }
    }
EXIT:
    //flushPort(this);
    OMX_DBGT_EPILOG();
}

int
VidEncOutputPort::findAssociation(void *Inputbuffer, int type, void **retBuffer)
{
    int i;
    int retVal = -1;
    OMX_DBGT_PROLOG();    
    for (i=0;i<MAX_INPUT_OUTPUT_BUFFERS;i++)
    {
        if (type)
        {
            if (Inputbuffer == mapping[i].internalBuffer)
            {
                *retBuffer = (void *)mapping[i].omxBuffer;
                retVal = 0;
                break;
            }
        }
        else
        {
            if ((OMX_BUFFERHEADERTYPE *)Inputbuffer == mapping[i].omxBuffer)
            {
                *retBuffer = (void *)mapping[i].internalBuffer;
                retVal = 0;
                break;
            }
        }
    }
    OMX_DBGT_PDEBUG("Inputbuffer : 0x%x type : %d retBuffer : 0x%x retVal : %d",(unsigned int)Inputbuffer,type,(unsigned int)*retBuffer,retVal);
    OMX_DBGT_EPILOG();
    return retVal;
}




//==========================================================================
// VidEncInputPort implementation
//==========================================================================

// BGRA8888 is the prefered pixel format returned in OMX output formats
const VidEncInputPort::ColorInfo VidEncInputPort::m_colorInfos[] = {
#ifdef ANDROID
    {OMX_COLOR_FormatYUV420SemiPlanar, HAL_PIXEL_FORMAT_YCrCb_420_SP, V4L2_PIX_FMT_NV12, 12, 8},
    {OMX_COLOR_FormatAndroidOpaque, HAL_PIXEL_FORMAT_YCrCb_420_SP, V4L2_PIX_FMT_NV12, 12, 8},
#else
    {OMX_COLOR_FormatYUV420SemiPlanar, OMX_COLOR_FormatYUV420SemiPlanar, V4L2_PIX_FMT_NV12, 12, 8},
    {OMX_COLOR_FormatAndroidOpaque, OMX_COLOR_FormatAndroidOpaque, V4L2_PIX_FMT_NV12, 12, 8},
#endif
};

OMX_BUFFERHEADERTYPE* VidEncInputPort::EOSBUF =
    (OMX_BUFFERHEADERTYPE*)0xFFFFFFFF;

VidEncInputPort::VidEncInputPort(unsigned int nIdx, OMX_DIRTYPE aDir,
                                 const VENCComponent& owner)
    :VidEncPort(nIdx, aDir, owner),
     pusher_thread_id(0),
     m_nThreadState(EThreadState_Stopped),
     m_mutexThread(),
     m_condState(),
     m_nSendedFrame(0),
     params_done(false),
     initial_header(false),
#ifdef ANDROID
     m_grallocModule(0),
     m_grallocDevice(0),
#endif
     mPortHVAHEncoder_Params(0),
     mapping(0),
     m_inputEncoderStarted(OMX_FALSE),
     m_bAfterEosFlag(OMX_FALSE),
     m_isAllocateBuffer(OMX_FALSE),
     swp_buffers(NULL)
{
    this->definition().bufferCountMin(NUM_IN_BUFFERS);
    this->definition().bufferCountActual(NUM_IN_BUFFERS);
    const OMX_COLOR_FORMATTYPE kDefaultColor = OMX_COLOR_FormatYUV420SemiPlanar;
    this->definition().colorFormat(kDefaultColor);
    this->definition().mimeType("video/H264");
    m_dvb = 0;
}

VidEncInputPort::~VidEncInputPort()
{
    if (m_dvb) {
        delete m_dvb;
        m_dvb = 0;
    }
}

void 
VidEncInputPort::colorFormat(OMX_COLOR_FORMATTYPE aFmt)
{
    this->definition().colorFormat(aFmt);
}


int
VidEncInputPort::findAssociation(void *Inputbuffer, int type, void **retBuffer)
{
    int i;
    int retVal = -1;
    OMX_DBGT_PROLOG();    
    for (i=0;i<MAX_INPUT_OUTPUT_BUFFERS;i++)
    {
        if (type)
        {
            if (Inputbuffer == mapping[i].internalBuffer)
            {
                *retBuffer = (void *)mapping[i].omxBuffer;
                retVal = 0;
                break;
            }
        }
        else
        {
            if ((OMX_BUFFERHEADERTYPE *)Inputbuffer == mapping[i].omxBuffer)
            {
                *retBuffer = (void *)mapping[i].internalBuffer;
                retVal = 0;
                break;
            }
        }
    }
    OMX_DBGT_PDEBUG("Inputbuffer : 0x%x type : %d retBuffer : 0x%x retVal : %d",(unsigned int)Inputbuffer,type,(unsigned int)*retBuffer,retVal);
    OMX_DBGT_EPILOG();
    return retVal;
}

int VidEncInputPort::disconnectAssociation(void *Inputbuffer)
{
    int i;
    int retVal = -1;
    for (i=0;i<MAX_INPUT_OUTPUT_BUFFERS;i++)
    {
        if ((OMX_BUFFERHEADERTYPE *)Inputbuffer == mapping[i].omxBuffer)
        {
            mapping[i].isAssociated = (OMX_BOOL)false;
            retVal = 0;
            OMX_DBGT_PTRACE("Buffer : 0x%x at location : %d disassociated",(unsigned int)Inputbuffer,i);
            break;
        }
    }
    return retVal;
}


void
VidEncInputPort::bufferSize(unsigned int buff_size)
{
    assert(buff_size);
    this->definition().bufferSize(buff_size);
}

VidEncInputPort::EThreadState VidEncInputPort::threadState() const
{ return m_nThreadState; }

void VidEncInputPort::threadState(EThreadState st)
{
    m_mutexThread.lock();
    m_nThreadState = st;
    m_mutexThread.release();
    m_condState.signal();
}

void VidEncInputPort::setInternalPointers(HVAHEncoder_t **params, EncoderBufferMapping **buffermapping)
{
    OMX_DBGT_PROLOG();
    mPortHVAHEncoder_Params = *params;
    mapping = *buffermapping;
    OMX_DBGT_PTRACE("m_HVAHEncoder_Params : 0x%x m_nbBufferMapping : 0x%x",(unsigned int)mPortHVAHEncoder_Params,(unsigned int)mapping);
    OMX_DBGT_EPILOG();
}

void VidEncInputPort::compute_framerate_num_den(OMX_U32 framerate, OMX_U32& num, OMX_U32& den)
{
    OMX_DBGT_PROLOG();
    OMX_U16 iter = 0;
    den = 1;
    double ret;
    double in = framerate;
    in = in / 65536;
    while (iter < 4)
    {
        ret = in - (OMX_U32)in;
        if (ret==0)
            break;
        else
        {
            in *= 10;
        }
        iter++;
        den *= 10;
    }
    num = (OMX_U32) in;
    OMX_DBGT_PINFO("Framerate = (Numerator:%d) / (Denominatory:%d)", (int)num, (int)den);
    OMX_DBGT_EPILOG();
}


OMX_ERRORTYPE
VidEncInputPort::setDvbDevice(IDvbDevice **temp_m_dvb)
{
    uint32_t pixelformat;
    int retVal = -1;
    OMX_DBGT_PROLOG();
    m_dvb = *temp_m_dvb;

    retVal = static_cast<DVBVideo_SF*>(m_dvb)->create(ENCODER_DEVICE_NAME, V4L2_CARD,0);
    if (retVal != 0)
    {
        OMX_DBGT_ERROR("Error create, return value %d",retVal);
        return OMX_ErrorHardware;
    }

    static_cast<DVBVideo_SF*>(m_dvb)->SpecificationCheck();
    retVal = static_cast<DVBVideo_SF*>(m_dvb)->ConfigureEncoder();
    if (retVal != 0)
    {
        OMX_DBGT_ERROR("Error createConfigureEncoder, return value %d",retVal);
        return OMX_ErrorHardware;
    }

    static_cast<DVBVideo_SF*>(m_dvb)->configureEncoderStructure((void **)&mPortHVAHEncoder_Params,ENCODER_TYPE_H264ENC);

    retVal = static_cast<DVBVideo_SF*>(m_dvb)->FormatCheck();
    if (retVal != 0)
    {
        OMX_DBGT_ERROR("Error desired format NOT support for input, return value %d",retVal);
        return OMX_ErrorHardware;
    }

    OMX_DBGT_PDEBUG("Input port format : %d",(int)this->definition().colorFormat());
    switch (this->definition().colorFormat())
    {
        case OMX_COLOR_FormatYUV420SemiPlanar:
            pixelformat = V4L2_PIX_FMT_NV12; break;

        case OMX_COLOR_FormatYUV420Planar:
            pixelformat = V4L2_PIX_FMT_YUV420; break;

        case OMX_COLOR_FormatAndroidOpaque:
            pixelformat = V4L2_PIX_FMT_NV12; break;//V4L2_PIX_FMT_RGB32; break;

        default:
            OMX_DBGT_ERROR("Other formats : %d currently not supported",(int)this->definition().colorFormat());
            return OMX_ErrorHardware;
    }

    OMX_DBGT_PINFO("For input port frameRate : %d num : %d den : %d",(int)this->definition().frameRate(),(int)(*mPortHVAHEncoder_Params).FrameRate_Num,(int)(*mPortHVAHEncoder_Params).FrameRate_Den);

    OMX_DBGT_CHECK_RETURN(! static_cast<DVBVideo_SF*>(m_dvb)->Configure(this->definition().frameWidth(),this->definition().frameHeight(),pixelformat,12), OMX_ErrorHardware);

    compute_framerate_num_den(this->definition().frameRate(),(*mPortHVAHEncoder_Params).FrameRate_Num, (*mPortHVAHEncoder_Params).FrameRate_Den);
    /* setting the buffer count values*/
    (*mPortHVAHEncoder_Params).num_input_buffers = this->definition().bufferCountActual();

    retVal = static_cast<DVBVideo_SF*>(m_dvb)->setParam();
    if (retVal != 0)
    {
        OMX_DBGT_ERROR("Error while setupBuffer to v4L for input, return value %d",retVal);
        return OMX_ErrorHardware;
    }
#ifdef ANDROID
    OMX_DBGT_CHECK_RETURN(! hw_get_module(GRALLOC_HARDWARE_MODULE_ID, (const hw_module_t **)&m_grallocModule),
                      OMX_ErrorInsufficientResources);

    OMX_DBGT_CHECK_RETURN(! gralloc_open((const hw_module_t *)m_grallocModule, &m_grallocDevice),
                      OMX_ErrorHardware);

    OMX_DBGT_PINFO("At input value of m_grallocModule : 0x%x m_grallocDevice 0x%x",(unsigned int)m_grallocModule,(unsigned int)m_grallocDevice);
#endif
    if (VidEnc().isMetaDataSet())
    {
        swp_buffers = (SwapBuffer *)calloc(MAX_INPUT_OUTPUT_BUFFERS,sizeof(SwapBuffer));
        if (swp_buffers == NULL)
        {
            OMX_DBGT_CRITICAL("Not enough memory.... KILL");
            return OMX_ErrorInsufficientResources;
        }
        for (int i=0;i<MAX_INPUT_OUTPUT_BUFFERS;i++)
        {
            swp_buffers[i].isFree = OMX_TRUE;
            swp_buffers[i].pBuffer = 0;
            swp_buffers[i].omxBuffer = 0;
        }
    }

    return OMX_ErrorNone;
    OMX_DBGT_EPILOG();
}

OMX_ERRORTYPE VidEncInputPort::create()
{
    OMX_DBGT_PROLOG();
    OmxPort::create();
    OMX_DBGT_EPILOG();

    return OMX_ErrorNone;
}

int VidEncInputPort::start()
{
    OMX_DBGT_PROLOG();

    OMX_DBGT_CHECK_RETURN(! pthread_create(&pusher_thread_id, NULL, VidEncInputPort::ThreadEntryPoint, this), OMX_ErrorInsufficientResources);
#ifdef ANDROID
    OMX_DBGT_CHECK_RETURN(! pthread_setname_np(pusher_thread_id, "OMXVideoPush"), OMX_ErrorInsufficientResources);
#endif
    this->threadState(EThreadState_Running);
    OMX_DBGT_EPILOG();
    return OMX_ErrorNone;
}

int VidEncInputPort::stop()
{
    int retVal = -1;
    OMX_DBGT_PROLOG();

    retVal = static_cast<DVBVideo_SF*>(m_dvb)->stop();
    if (retVal < 0) 
    {
        OMX_DBGT_ERROR("Encoder Input Port Stop failed (%s)", strerror (errno));
    }
    OMX_DBGT_EPILOG("retVal : %d",retVal);
    return retVal;
}

void VidEncInputPort::destroy()
{
    OMX_DBGT_PROLOG();
    int retVal = -1;

    if (swp_buffers)
    {
        free(swp_buffers);
        swp_buffers = NULL;
    }

    if (m_dvb)
    {
        retVal = static_cast<DVBVideo_SF*>(m_dvb)->destroy();
        if (retVal < 0) {
            OMX_DBGT_ERROR("DVB  device for input port not closed");
        }
    }

    if (pusher_thread_id != 0) {
        // Resume if was previously paused
        this->resume();
        this->push(EOSBUF);
        pthread_join(pusher_thread_id, 0);
    }

    if (this->nbBuffers() > 0) {
        OMX_DBGT_ERROR("Allocated buffer on input port not freed");
    }
    OMX_DBGT_EPILOG();
}

void* VidEncInputPort::ThreadEntryPoint(void* a_pThis)
{
    VidEncInputPort* pThis = static_cast<VidEncInputPort*>(a_pThis);
    pThis->threadFunction();
    return 0;
}



OMX_ERRORTYPE
VidEncInputPort::getParameter(OMX_INDEXTYPE nParamIndex, OMX_PTR pParamStruct)
{
    OMX_DBGT_PROLOG("on Video encoder input port");
    switch ((int)nParamIndex)
    {
    case OMX_IndexParamPortDefinition: {
        // Port definition
        ASSERT_STRUCT_TYPE(pParamStruct, OMX_PARAM_PORTDEFINITIONTYPE);
        OMX_PARAM_PORTDEFINITIONTYPE* param;
        param = (OMX_PARAM_PORTDEFINITIONTYPE*)pParamStruct;
        assert(param->nPortIndex == this->definition().index());

        // Provide current content of sPorts and computes nBufferSize on top of it
        OMX_DBGT_PDEBUG("GetParameter(OMX_IndexParamPortDefinition) nPortIndex: "
                    "%u eDir: %d nBufferCountActual: %u nBufferCountMin: %u nBufferSize: %u\n",
                    this->definition().index(),
                    this->definition().direction(),
                    this->definition().bufferCountActual(),
                    this->definition().bufferCountMin(),
                    this->definition().bufferSize());

        unsigned int nBufferSz = 0;

        unsigned int alignedWidth = ((this->definition().frameWidth() + 0xf) >> 4) << 4;
        unsigned int tempStride =  alignedWidth * this->colorInfo().bpp / 8;
        OMX_DBGT_PDEBUG("Actual width : %d Aligned width : %d Height : %d Stride : %d bpp : %d",this->definition().frameWidth(),alignedWidth,this->definition().frameHeight(),tempStride,this->colorInfo().bpp);

        if ((this->definition().frameWidth() != 0)
            && (this->definition().frameHeight() != 0)
            && (this->definition().colorFormat() != OMX_COLOR_FormatUnused))
        {
            nBufferSz = tempStride
                * this->definition().frameHeight();
            OMX_DBGT_PDEBUG("nBufferSz : %d",nBufferSz);
        }
        if (this->definition().bufferSize() == 0) {
            // Update internal structure
            this->definition().stride(tempStride);
            this->definition().bufferSize(nBufferSz);
            OMX_DBGT_PINFO("%s set nBufferSize to %uKB (wxh=%ux%u, bpp=%u  "
                       "(GetParameter(OMX_IndexParamPortDefinition output)))",
                       component().name(), nBufferSz/1024,
                       this->definition().frameWidth(),
                       this->definition().frameHeight(),
                       this->colorInfo().bpp);
        } else {
            if (param->nBufferSize != nBufferSz)
            {
                OMX_DBGT_PINFO("%s nBufferSize from client %u does not match "
                           "computed %u (GetParameter(OMX_IndexParamPortDefinition output)) ",
                           component().name(),
                           param->nBufferSize,
                           this->definition().bufferSize());
            }
        }
        *param = this->definition().getParamPortDef();
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }; break;

    case OMX_IndexParamVideoPortFormat: {
        // Supported video port format
        ASSERT_STRUCT_TYPE(pParamStruct, OMX_VIDEO_PARAM_PORTFORMATTYPE);
        OMX_VIDEO_PARAM_PORTFORMATTYPE* param;
        param = (OMX_VIDEO_PARAM_PORTFORMATTYPE*)pParamStruct;
        assert(param->nPortIndex == this->definition().index());
        if (param->nIndex < this->supportedColorFormatNb()) {
            param->eCompressionFormat = OMX_VIDEO_CodingUnused;
            param->eColorFormat = supportedColorFormat(param->nIndex);
            OMX_DBGT_EPILOG();
            return OMX_ErrorNone;
        } else {
            OMX_DBGT_EPILOG("No more supported format");
            return OMX_ErrorNoMore;
        }
    }; break;

    case OMX_getAndroidNativeBufferUsage: {
        OMX_DBGT_PDEBUG("VENCComponent::output port get parameter OMX_getAndroidNativeBufferUsage");
        OMX_GetAndroidNativeBufferUsageParams *param = (OMX_GetAndroidNativeBufferUsageParams *)pParamStruct;
#ifdef ANDROID
        param->nUsage = GRALLOC_USAGE_HW_2D;
        param->nUsage |= GRALLOC_USAGE_SW_READ_RARELY;
        param->nUsage |= GRALLOC_USAGE_EXTERNAL_DISP;
        param->nUsage |= GRALLOC_USAGE_SW_WRITE_MASK ;
#endif
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }; break;


    case OMX_IndexParamOtherPortFormat:
    case OMX_IndexParamAudioPortFormat:
    case OMX_IndexParamImagePortFormat:
    default: {
        OMX_DBGT_ERROR("Unsupported index");
        OMX_DBGT_EPILOG("Unsupported index");
        return OMX_ErrorUnsupportedIndex;
    }; break;
    }
}

OMX_ERRORTYPE
VidEncInputPort::setParameter(OMX_INDEXTYPE nParamIndex, OMX_PTR pParamStruct)
{
    OMX_DBGT_PROLOG("on Video encoder input port");
    switch ((int)nParamIndex)
    {
        case OMX_enableAndroidNativeBuffers: {
            // Nothing to do on input port
            OMX_DBGT_EPILOG();
            return OMX_ErrorNone;
        }; break;
        
        case OMX_IndexParamVideoPortFormat: {
            OMX_DBGT_EPILOG();
            return OMX_ErrorNone;
        }; break;
        
        case OMX_IndexParamPortDefinition: {
            // Set port definition
            ASSERT_STRUCT_TYPE(pParamStruct, OMX_PARAM_PORTDEFINITIONTYPE);
            OMX_PARAM_PORTDEFINITIONTYPE* param;
            param = (OMX_PARAM_PORTDEFINITIONTYPE*)pParamStruct;
            assert(param->nPortIndex == this->definition().index());
           
            if (this->definition().bufferSize() != param->nBufferSize) {
                OMX_DBGT_PINFO("%s changing nBufferSize from %uKB to %uKB "
                               "(SetParameter(OMX_IndexParamPortDefinition input))",
                               component().name(),
                               this->definition().bufferSize()/1024,
                               param->nBufferSize/1024);
            }
            this->definition().setParamPortDef(param);
            OMX_DBGT_EPILOG();
            return OMX_ErrorNone;
        }; break;

        case OMX_IndexParamOtherPortFormat:
        case OMX_IndexParamAudioPortFormat:
        case OMX_IndexParamImagePortFormat:
        default: {
            OMX_DBGT_ERROR("Unsupported index");
            OMX_DBGT_EPILOG("Unsupported index");
            return OMX_ErrorUnsupportedIndex;
        };break;
    }
}

OMX_COLOR_FORMATTYPE
VidEncInputPort::supportedColorFormat(unsigned int nIdx) const
{
    assert(nIdx < this->supportedColorFormatNb());
    return m_colorInfos[nIdx].OMXType;
}

bool VidEncInputPort::isSupportedColorFormat(OMX_COLOR_FORMATTYPE aFmt) const
{
    bool ret = false;
    for (unsigned int i = 0; i < this->supportedColorFormatNb(); i++)
    {
        if (m_colorInfos[i].OMXType == aFmt) {
            ret = true;
            break;
        }
    }
    return ret;
}


unsigned int
VidEncInputPort::supportedColorFormatNb() const
{ return sizeof(m_colorInfos) / sizeof(m_colorInfos[0]); }

const VidEncInputPort::ColorInfo& VidEncInputPort::colorInfo() const
{
    OMX_COLOR_FORMATTYPE cFmt = this->definition().colorFormat();
    assert(isSupportedColorFormat(cFmt));
    unsigned int i = 0;
    for (i = 0; i < this->supportedColorFormatNb(); i++) {
        if (m_colorInfos[i].OMXType == cFmt)
            break;
    }
    return m_colorInfos[i];
}

OMX_U32 
VidEncInputPort::getVirtualAddress(OMX_BUFFERHEADERTYPE**  ppBufferHdr)
{
	OMX_U32 tempValue = 0;
	OMX_DBGT_PROLOG();

    OMX_DBGT_PTRACE("ppBufferHdr : 0x%x ppBufferHdr->pBuffer : 0x%x   ",(unsigned int)*ppBufferHdr,(unsigned int)(*ppBufferHdr)->pBuffer);
#ifdef ANDROID
    if(this->grallocModule()->lock(grallocModule(),
                (buffer_handle_t)((*ppBufferHdr)->pBuffer),
                GRALLOC_USAGE_HW_2D | GRALLOC_USAGE_EXTERNAL_DISP | GRALLOC_USAGE_SW_WRITE_MASK,
                0, 0,
                this->definition().frameWidth(),
                this->definition().frameHeight(),
                &((*ppBufferHdr)->pPlatformPrivate)) != 0)
    {
        OMX_DBGT_ERROR("grallocModule->lock failed");
        goto EXIT_ERROR;
	}

    if(this->grallocModule()->unlock(grallocModule(), (buffer_handle_t)((*ppBufferHdr)->pBuffer)) != 0)
    {
        OMX_DBGT_ERROR("grallocModule->unlock failed");
        goto EXIT_ERROR;
	}
#endif
	tempValue = (OMX_U32)(*ppBufferHdr)->pPlatformPrivate;

EXIT_ERROR:
    OMX_DBGT_EPILOG("tempValue : 0x%x",(unsigned int)tempValue);
	return tempValue;
}

OMX_ERRORTYPE
VidEncInputPort::doInputPortExtraProcessing(OMX_BUFFERHEADERTYPE**  ppBufferHdr,OMX_U32 nSizeBytes)
{
    int retVal = -1;
    int i;
    unsigned int bufferCount;
    OMX_ERRORTYPE eError = OMX_ErrorNone;
    OMX_DBGT_PROLOG();
    OMX_U32 tempVirtualaddr = 0;

    if (!m_isAllocateBuffer) {
        tempVirtualaddr = getVirtualAddress(ppBufferHdr);
    }

    if ((tempVirtualaddr == 0) || (m_isAllocateBuffer))
    {
        (*mPortHVAHEncoder_Params).isInputPortBufferAllocated = OMX_TRUE;
        OMX_DBGT_PTRACE("Virtual address is : 0x%x and m_isAllocateBuffer : %d",(unsigned int)tempVirtualaddr,m_isAllocateBuffer);
    }


    if (!mPortHVAHEncoder_Params->input_buf_allocated)
    {
         retVal = static_cast<DVBVideo_SF*>(m_dvb)->setupBufferCount();

         if (retVal != 0)
         {
            OMX_DBGT_ERROR("Error while setupBuffer to v4L for output, return value %d",retVal);
            return OMX_ErrorHardware;
         }
         bufferCount = 0;
             
         if ((*mPortHVAHEncoder_Params).isInputPortBufferAllocated)
         {             
             /* getting the input buffers and associating them to omx buffers */
             retVal = static_cast<DVBVideo_SF*>(m_dvb)->setUpBufferPointer();
             if (retVal != 0)
             {
                 OMX_DBGT_ERROR("Error while setUpBufferPointer to v4L for output, return value %d",retVal);
                 return OMX_ErrorHardware;
             }                 
         }
         mPortHVAHEncoder_Params->input_buf_allocated = true;
    }

    for (i=0;i<MAX_INPUT_OUTPUT_BUFFERS;i++)
    {
        if (mapping[i].isAssociated == false)
            break;
    }

    if (i == MAX_INPUT_OUTPUT_BUFFERS)
    {
        OMX_DBGT_CRITICAL("No free buffer available for use - input");
        return OMX_ErrorInsufficientResources;
    }
    for (bufferCount = 0;bufferCount<(*mPortHVAHEncoder_Params).num_input_buffers ; bufferCount++)
    {
        if ((*mPortHVAHEncoder_Params).input_buffers[bufferCount].isUsed == 0)
            break;
    }

    // forming an assoication between input buffer and encoder internally allocated buffer
    mapping[i].isAssociated = (OMX_BOOL)true;
    (*mPortHVAHEncoder_Params).input_buffers[bufferCount].isUsed = (OMX_BOOL)true;
    mapping[i].buffProperty = 0;
    mapping[i].omxBuffer = *ppBufferHdr;
    OMX_DBGT_PTRACE("Output buffer count : %d buff 0x%x",bufferCount,(unsigned int)(*mPortHVAHEncoder_Params).input_buffers[bufferCount].start);
    mapping[i].internalBuffer =&((*mPortHVAHEncoder_Params).input_buffers[bufferCount]);

    if (tempVirtualaddr)
    {
        (*mPortHVAHEncoder_Params).input_buffers[bufferCount].start = (void *)tempVirtualaddr;
        OMX_DBGT_PTRACE("Assigning virtual address to start pointer");
    }
    if (m_isAllocateBuffer) 
    {
        (*ppBufferHdr)->pBuffer = (OMX_U8 *)(*mPortHVAHEncoder_Params).input_buffers[bufferCount].start;
    }

    OMX_DBGT_PINFO("Input buffer count : %d virtual address 0x%x omx buffer : 0x%x location : %d",bufferCount,(unsigned int)(*mPortHVAHEncoder_Params).input_buffers[bufferCount].start,(unsigned int)*ppBufferHdr,i);

    OMX_DBGT_EPILOG();
    return eError;   	
}


OMX_ERRORTYPE
VidEncInputPort::useBuffer(OMX_BUFFERHEADERTYPE** ppBufferHdr,
                           OMX_PTR pAppPrivate,
                           OMX_U32 nSizeBytes,
                           OMX_U8* pBuffer)
{
    OMX_ERRORTYPE eError = OMX_ErrorNone;
    OMX_DBGT_PROLOG();
    // Allocate a buffer header
    *ppBufferHdr = 0;

    OMX_BUFFERHEADERTYPE* pBufferHdr = allocateBufferHeader(pAppPrivate,
                                                            nSizeBytes);
    if (!pBufferHdr) {
        eError = OMX_ErrorInsufficientResources;
        OMX_DBGT_ERROR("Can not allocate buffer header");
        OMX_DBGT_EPILOG("Return error %s", OMX_TYPE_TO_STR(OMX_ERRORTYPE, eError));
        return eError;
    }

    /* doing pointer assignments here */
    pBufferHdr->pBuffer = pBuffer;
    *ppBufferHdr = pBufferHdr;

    // Increment internal buffer counter.
    this->incrementNbBuffers();

    if (VidEnc().IsConfigureDone == false) {
        OMX_DBGT_PINFO("Blocking usebuffer before venc configure");
        VidEnc().m_condConfig.wait(VidEnc().m_mutexCmd);
        OMX_DBGT_PINFO("Unblocking allocateBuffer");
    }

    if (VidEnc().isMetaDataSet()) 
    {
        OMX_DBGT_PTRACE("Metadata set, nothing to be done here");
    }
    else
    {
        OMX_DBGT_PTRACE("Metadata Not set, to do doInputPortExtraProcessing");
        eError = doInputPortExtraProcessing(ppBufferHdr,nSizeBytes);
    }
    OMX_DBGT_EPILOG("Error : 0x%x",(unsigned int)eError);
    return eError;
}

OMX_ERRORTYPE
VidEncInputPort::allocateBuffer(OMX_BUFFERHEADERTYPE** ppBufferHdr,
                                OMX_PTR pAppPrivate,
                                OMX_U32 nSizeBytes)
{
    OMX_ERRORTYPE eError = OMX_ErrorNone;
    OMX_DBGT_PROLOG();

    m_isAllocateBuffer = OMX_TRUE;

    // Allocate a buffer header
    *ppBufferHdr = 0;
    OMX_BUFFERHEADERTYPE* pBufferHdr = allocateBufferHeader(pAppPrivate,
                                                            nSizeBytes);
    if (!pBufferHdr) {
        eError = OMX_ErrorInsufficientResources;
        OMX_DBGT_ERROR("Can not allocate buffer header");
        OMX_DBGT_EPILOG("Return error %s", OMX_TYPE_TO_STR(OMX_ERRORTYPE, eError));
        return eError;
    }

    *ppBufferHdr = pBufferHdr;

    // Increment internal buffer counter.
    this->incrementNbBuffers();

    if (VidEnc().IsConfigureDone == false) {
        OMX_DBGT_PINFO("Blocking allocateBuffer before venc configure");
        VidEnc().m_condConfig.wait(VidEnc().m_mutexCmd);
        OMX_DBGT_PINFO("Unblocking allocateBuffer");
    }
    OMX_DBGT_PINFO("In allocate buffer for input omx buffer header : 0x%x",(unsigned int)*ppBufferHdr);
    if (VidEnc().isMetaDataSet()) 
    {
        (*ppBufferHdr)->pBuffer = (OMX_U8 *)malloc(this->definition().bufferSize() * sizeof(OMX_U8));
    }
    else
    {
        OMX_DBGT_PTRACE("Metadata Not set, to do doInputPortExtraProcessing");
        eError = doInputPortExtraProcessing(ppBufferHdr,nSizeBytes);
    }
    OMX_DBGT_EPILOG("Error : 0x%x",(unsigned int)eError);
    return eError;
}

OMX_ERRORTYPE
VidEncInputPort::freeBuffer(OMX_BUFFERHEADERTYPE* pBufferHdr)
{
    OMX_DBGT_PROLOG();
    assert(!isTunneled());

    int retVal = -1;
    retVal = static_cast<DVBVideo_SF*>(m_dvb)->freeBufferPointer();
    if (retVal < 0)
    {
            OMX_DBGT_ERROR("Encoder Input Port v4l unmap failed (%s)", strerror (errno));
    }

    freeBufferHeader(pBufferHdr);
    this->decrementNbBuffers();

    OMX_DBGT_EPILOG();
    return OMX_ErrorNone;
}

// Temp Code for Blit HACK code
int VidEncInputPort::h264BlitToNV12(OMX_BUFFERHEADERTYPE*   pBufferHdr){

    //hack code to make blit to work
    if(memcmp((uint8_t*)pBufferHdr->pPlatformPrivate+(this->definition().frameWidth() * this->definition().frameHeight() * 3/2),"Manish",6) == 0) {
        return 0;
    }
    omxse_raw_blit raw_blit;
    int fd = -1;
    int ret;

    //ALOGE(" Entry of Blitting pBuufer->pBuffer %p ",pBufferHdr->pBuffer);
    fd = open("/dev/omxse.blitter", O_RDWR | O_NONBLOCK);

    if (fd < 0) {
        return -EINVAL;
    }
    memset(&raw_blit, 0x0, sizeof(raw_blit));
#ifdef ANDROID
    buffer_handle_t handle ;
    int stride =0;
#endif
    void *memory_gralloc= NULL;

#ifdef ANDROID
    ret = m_grallocDevice->alloc(m_grallocDevice,1280,720,HAL_PIXEL_FORMAT_YCrCb_420_SP,GRALLOC_USAGE_HW_2D | GRALLOC_USAGE_SW_READ_RARELY | GRALLOC_USAGE_SW_READ_OFTEN | GRALLOC_USAGE_SW_WRITE_NEVER, &handle, &stride);


    ret = m_grallocModule->lock(m_grallocModule, handle,GRALLOC_USAGE_HW_2D | GRALLOC_USAGE_SW_READ_RARELY | GRALLOC_USAGE_SW_READ_OFTEN | GRALLOC_USAGE_SW_WRITE_NEVER, 0, 0, 1280, 720, &memory_gralloc);
#endif

  /*  STM_MSG("src-> size: %d, w: %d, h: %d",
                       srcFormat->fmt.pix.sizeimage, srcFormat->fmt.pix.width, fmt.pix.height);
                       STM_MSG("dst-> size: %d, w: %d, h: %d", dstBufferSize, finalWidth, finalHeight);
*/
 //   ALOGE(" Manish: Bufferhdr: %p Buffer: %p Width: %d height: %d ",pBufferHdr,pBufferHdr->pPlatformPrivate,this->getWidth(),this->getHeight());
    raw_blit.src.virtualAddr = (long unsigned int)pBufferHdr->pPlatformPrivate;
    raw_blit.src.bufferSize = 1280*720*4; //pBufferHdr->nFilledLen;
    raw_blit.src.Color =  OMXSE_COLOR_ARGB8888;
    raw_blit.src.width = 1280;
    raw_blit.src.height = 720;
    raw_blit.cropWidth = 1280;
    raw_blit.cropHeight = 720;

    //if(mTargetHeight == mSrcFormat.fmt.pix.height) {
    //raw_blit.cropHeight = mSrcFormat.fmt.pix.height;
    //}else {
    //raw_blit.cropHeight = srcFormat->fmt.pix.height;
    //}


    raw_blit.dst.virtualAddr = (long unsigned int)memory_gralloc;
    raw_blit.dst.bufferSize = 1280*720*3/2;
    raw_blit.dst.Color = OMXSE_COLOR_NV12;
    raw_blit.dst.width = 1280;
    raw_blit.dst.height = 720;

 /*   STM_MSG("rawblit src -> virtualAddr: %p, bufferSize: %d, Color: %d,
                           width: %d, height: %d",
                           (void*)raw_blit.src.virtualAddr, raw_blit.src.bufferSize, raw_blit.src.Color, raw_blit.src.width, raw_blit.src.height);

    STM_MSG("raw_blit cropWidth: %ld, cropHeight: %ld", raw_blit.cropWidth, raw_blit.cropHeight);
    STM_MSG("rawblit dst -> virtualAddr: %p, bufferSize: %d, Color: %d,
                           width: %d, height: %d",
                           (void*)raw_blit.dst.virtualAddr, raw_blit.dst.bufferSize, raw_blit.dst.Color, raw_blit.dst.width, raw_blit.dst.height);
*/

    ret = ioctl(fd, OMXSE_PERFORM_BLITTERING_RAW, &raw_blit);
    //ALOGE(" Swinder blit 1 ret: %d",ret);


    // Copy to pBufferHdr->pBuffer
    raw_blit.src.virtualAddr = (long unsigned int)memory_gralloc;
    raw_blit.src.bufferSize = 1280*720*3/2;
    raw_blit.src.Color = OMXSE_COLOR_NV12;
    raw_blit.src.width = 1280;
    raw_blit.src.height = 720;
    raw_blit.cropWidth = 1280;
    raw_blit.cropHeight = 720;


    raw_blit.dst.virtualAddr = (long unsigned int)pBufferHdr->pPlatformPrivate;
    raw_blit.dst.bufferSize = 1280*720*3/2;
    raw_blit.dst.Color = OMXSE_COLOR_NV12;
    raw_blit.dst.width = 1280;
    raw_blit.dst.height = 720;


    ret = ioctl(fd, OMXSE_PERFORM_BLITTERING_RAW, &raw_blit);
#if 0
    static int dumpFrameCounter;
    if(dumpFrameCounter < 20 ){
        FILE *fp = fopen("/data/dumpafterBlit.rgb","ab+");
        if( fp){
            dumpFrameCounter++;
            int written = fwrite(pBufferHdr->pPlatformPrivate,1, 1280*720*3/2,fp);
            fclose(fp);
        }
    }
#endif


    memcpy((uint8_t*)pBufferHdr->pPlatformPrivate+(1280*720*3/2),"Manish",6);

#ifdef ANDROID
    m_grallocModule->unlock(m_grallocModule, handle);
    m_grallocDevice->free(m_grallocDevice,handle);
#endif

    close(fd);
    //ALOGE(" OUT of Blitting pBuufer->pBuffer %p ",pBufferHdr->pBuffer);
    return ret;
}


OMX_ERRORTYPE
VidEncInputPort::emptyThisBuffer(OMX_BUFFERHEADERTYPE* pBufferHdr)
{
    // Called by player to send a video encoded frame buffer
    // This buffer is pushed via a queue to the PusherThread
    // in order to be sent to SE
    OMX_DBGT_PROLOG("pBufferHdr = 0x%x pBufferHdr->pBuffer : 0x%x nFlags : 0x%x is MetaDataSet : %d",(unsigned int)pBufferHdr,(unsigned int)pBufferHdr->pBuffer,(unsigned int)pBufferHdr->nFlags,VidEnc().isMetaDataSet());
    if (VidEnc().isMetaDataSet())
    {
        int i;
        OMX_BOOL isFound = OMX_FALSE;
        for (i=0;i<MAX_INPUT_OUTPUT_BUFFERS;i++)
        {
            if (swp_buffers[i].omxBuffer == pBufferHdr)
            {
                isFound = OMX_TRUE;
                if (swp_buffers[i].isFree == OMX_TRUE)
                {
                    swp_buffers[i].isFree = OMX_FALSE;
                    swp_buffers[i].pBuffer = pBufferHdr->pBuffer;
                    OMX_DBGT_PTRACE("Entry exists in table, entry changed");
                    break;
                }
                else
                {
                    OMX_DBGT_CRITICAL("Entry for swp_buffers[i]->omxBuffer : 0x%x swp_buffers[i]->pBuffer : 0x%x with TRUE status",(unsigned int)swp_buffers[i].omxBuffer,(unsigned int)swp_buffers[i].pBuffer);
                }
                break;
            }
        }

        if (!isFound)
        {
            for (i=0;i<MAX_INPUT_OUTPUT_BUFFERS;i++)
            {
                if (swp_buffers[i].isFree == OMX_TRUE && swp_buffers[i].omxBuffer == NULL)
                {
                    swp_buffers[i].isFree = OMX_FALSE;
                    swp_buffers[i].pBuffer = pBufferHdr->pBuffer;
                    swp_buffers[i].omxBuffer = pBufferHdr;
                    OMX_DBGT_PTRACE("New Entry taken in table");
                    break;
                }
            }
        }

        void *data = pBufferHdr->pBuffer;
        OMX_U32 type = *(OMX_U32*)data;
        if (type != 1) {
            OMX_DBGT_ERROR("Data passed in with metadata mode does not have type "
                           "kMetadataBufferTypeGrallocSource (%d), has type %u instead",
                           1, type);
        }
#ifdef ANDROID
        buffer_handle_t imgBuffer = *(buffer_handle_t*)((uint8_t*)data + 4);
#else
        void * imgBuffer = *(void **)((uint8_t*)data + 4);
#endif
        (pBufferHdr)->pBuffer = (OMX_U8 *)imgBuffer;
        OMX_DBGT_PTRACE("ppBufferHdr : 0x%x ppBufferHdr->pBuffer : 0x%x typr: %d  imgBuffer: %p ",(unsigned int)pBufferHdr,(unsigned int)(pBufferHdr)->pBuffer,type,imgBuffer);
        OMX_DBGT_PTRACE("Calling associateInternalBuffers from ETB, isMetaDataEnabled : %d",VidEnc().isMetaDataSet());
        OMX_ERRORTYPE eError = doInputPortExtraProcessing(&pBufferHdr,this->definition().bufferSize());
        h264BlitToNV12(pBufferHdr);
    }

    this->push(pBufferHdr);

    OMX_DBGT_EPILOG();
    return OMX_ErrorNone;
}

OMX_ERRORTYPE
VidEncInputPort::fillThisBuffer(OMX_BUFFERHEADERTYPE* pBufferHdr)
{
    assert(0 && "Invalid call on input port");
    return OMX_ErrorNone;
}

OMX_ERRORTYPE VidEncInputPort::flush()
{
    OMX_DBGT_PROLOG("Flush video encoder input port");
    if (isTunneled()) {
        // Proprietary tunneling. Nothing to flush
        OMX_DBGT_EPILOG("Port is tunneled.");
        return OMX_ErrorNone;
    }
   this->getRawFifos()->lock();

    OMX_BUFFERHEADERTYPE *buf  = 0;
    while(this->getRawFifos()->tryPop(buf) == true) {
        this->VidEnc().notifyEmptyBufferDone(buf);
    }
    this->getRawFifos()->unlock();
    buf  = 0;
    buf = this->relockCurrent();
    if(buf != NULL)
    {
        this->VidEnc().notifyEmptyBufferDone(buf);
    }
    this->freeAndUnlockCurrent();


    OMX_DBGT_EPILOG();
    return OMX_ErrorNone;
}

//=============================================================================
//  nothing specific to be done for encoder
//=============================================================================
OMX_ERRORTYPE VidEncInputPort::pause()
{
    OMX_DBGT_PROLOG("Pausing video input port");
    if (this->threadState() == EThreadState_Paused) {
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }

    // Pausing thread
    this->threadState(EThreadState_Paused);

    // Waiting thread is paused

    OMX_DBGT_EPILOG();
    return OMX_ErrorNone;
}

//=============================================================================
//  nothing specific to be done for encoder
//=============================================================================
OMX_ERRORTYPE VidEncInputPort::resume()
{
    if (this->threadState() == EThreadState_Running) {
        return OMX_ErrorNone;
    }

    // Resuming thread
    this->threadState(EThreadState_Running);
    return OMX_ErrorNone;
}


int VidEncInputPort::encodeFrame(unsigned int nOmxFlags,
                                 uint8_t* pBuf, unsigned int nLen,
                                 uint64_t nTimeStamp)
{
    int retVal = -1;
    return retVal;
}


//=============================================================================
//  Input port core function of pusher thread
//  Infinite loop: Wait for encoded frame from player and send them SE to encode it
//=============================================================================
void VidEncInputPort::threadFunction()
{
    OMX_DBGT_PROLOG();
    struct buffer_t *internalBufferPtr;
    int retVal = -1;
    static int counter = 0;   

    while(true)
    {
        if (m_bAfterEosFlag)
        {
            goto EXIT;
        }
        // Wait for a 'EmptyThisBuffer' from player
        OMX_BUFFERHEADERTYPE *pInBufHdr = this->popAndLockCurrent();
        OMX_DBGT_PINFO("input buffer : 0x%x actual 0x%x length : %d timestamp : 0x%x flags : %d",(unsigned int)pInBufHdr,(unsigned int)pInBufHdr->pBuffer,(int)pInBufHdr->nFilledLen,(int)pInBufHdr->nTimeStamp,(int)pInBufHdr->nFlags);
        if (pInBufHdr != NULL)
        {
            retVal = findAssociation((void *)pInBufHdr, 0,(void **)&internalBufferPtr);
            if (retVal < 0)
            {
                OMX_DBGT_ERROR("OMX to Internal Buffer Association NOT found");
                goto EXIT;
            }

            //if (isMetaDataEnabled)
            {
                int buf_width  = ((this->definition().frameWidth() + 31) >> 5) << 5;
                int buf_height = ((this->definition().frameHeight() + 31) >> 5) << 5;
                switch (this->definition().colorFormat())
                {
                    case OMX_COLOR_FormatYUV420SemiPlanar:
                        pInBufHdr->nFilledLen = buf_width * buf_height * 2;
                        OMX_DBGT_PTRACE("For Planar/SemiPlanar new width : %d new height : %d filledlen : %d",buf_width,buf_height,(int)pInBufHdr->nFilledLen);
                        break;

                    case OMX_COLOR_FormatAndroidOpaque:
                        pInBufHdr->nFilledLen = buf_width * buf_height * 2;
                        OMX_DBGT_PTRACE("For OMX_COLOR_FormatAndroidOpaque new width : %d new height : %d filledlen : %d",buf_width,buf_height,(int)pInBufHdr->nFilledLen);
                        break;

                    default:
                        OMX_DBGT_CRITICAL("Other formats : 0x%x currently not supported",(unsigned int)this->definition().colorFormat());
                        break;

                }
            }

            internalBufferPtr->v4l2_buf.bytesused = pInBufHdr->nFilledLen;
            OMX_DBGT_PINFO("Status of internal buffer isUsed (should be false) : %d",(int)internalBufferPtr->isUsed);
            OMX_DBGT_PINFO("To check v4l2 type input : %d, memory : %d index : %d start addr 0x%x len : %d filledlen : %d",
                (int)internalBufferPtr->v4l2_buf.type,
                (int)internalBufferPtr->v4l2_buf.memory,
                (int)internalBufferPtr->v4l2_buf.index,
                (unsigned int)internalBufferPtr->start,
                (int)internalBufferPtr->length,
                (unsigned int)internalBufferPtr->v4l2_buf.bytesused);

            internalBufferPtr->v4l2_buf.timestamp.tv_sec = (pInBufHdr->nTimeStamp / 1000000);
            internalBufferPtr->v4l2_buf.timestamp.tv_usec = (pInBufHdr->nTimeStamp % 1000000);

            //currently assuming last empty buffer with EOS
            if (pInBufHdr->nFlags & OMX_BUFFERFLAG_EOS)
            {
                pInBufHdr->nFilledLen = 0;
                m_bAfterEosFlag = OMX_TRUE;
            }

            if (this->VidEnc().isForceIntraEnabled())
            {
                (*mPortHVAHEncoder_Params).forceIntra = 1;
                internalBufferPtr->v4l2_buf.flags = V4L2_BUF_FLAG_STM_ENCODE_CLOSED_GOP_REQUEST;
                OMX_DBGT_PINFO("Forcing an I frame here");
                unsigned int tempValue = (*mPortHVAHEncoder_Params).IntraPeriod - (counter % (*mPortHVAHEncoder_Params).IntraPeriod);
                OMX_DBGT_PINFO("Inside forceintra counter : %d initial intra : %d new intra : %d",counter,(int)(*mPortHVAHEncoder_Params).IntraPeriod,(int)tempValue);
                static_cast<DVBVideo_SF*>(m_dvb)->setNewIntraPeriod(tempValue);
                this->VidEnc().resetForceIntraEnabled();
                (*mPortHVAHEncoder_Params).forceIntra = 0;
            }               
            else
            {
                internalBufferPtr->v4l2_buf.flags = 0;
            }
            
            if ((counter % (*mPortHVAHEncoder_Params).IntraPeriod) == 0)
            {
                OMX_DBGT_PINFO("Resetting counter value : %d",counter);
                static_cast<DVBVideo_SF*>(m_dvb)->setNewIntraPeriod((*mPortHVAHEncoder_Params).IntraPeriod); 
            }
            
            if (((*mPortHVAHEncoder_Params).isInputPortBufferAllocated) && (!m_isAllocateBuffer))
            {
				OMX_DBGT_PINFO("Performing memcpy for input buffer");
                memcpy(internalBufferPtr->start,pInBufHdr->pBuffer,pInBufHdr->nFilledLen);
            }

#ifdef ENABLE_FILE_DUMPING
            //dump_to_file(mydump3,  (uint8_t *)(pInBufHdr->pBuffer+pInBufHdr->nOffset), pInBufHdr->nFilledLen);
#endif
            uint64_t temp=0;
            if(static_cast<DVBVideo_SF*>(m_dvb)->QueueBuffer((void *)internalBufferPtr,(int &)(pInBufHdr->nFilledLen),temp) != 0)
            {
                OMX_DBGT_ERROR("Encoder Input Port initial QBUF failed (%s)", strerror (errno));
                goto EXIT;
            }
            counter++;

            OMX_DBGT_PINFO("Status if internal buffer isUsed (should be true NOW) : %d",(int)internalBufferPtr->isUsed);
            
            if (!m_inputEncoderStarted)
            {
                OMX_DBGT_PINFO("Abt to call start for input");
                if(static_cast<DVBVideo_SF*>(m_dvb)->start() != 0)
                {
                    OMX_DBGT_ERROR("Encoder Input Port Start failed (%s)", strerror (errno));
                    goto EXIT;
                }
                m_inputEncoderStarted = OMX_TRUE;
                OMX_DBGT_PINFO("Encoder input buffer started");
            }
            
            //Now dequeue the buffer
            retVal = static_cast<DVBVideo_SF*>(m_dvb)->DequeueEncoderBuffer(internalBufferPtr);
            if (retVal < 0)
            {
                OMX_DBGT_ERROR("!!FATAL!! Error while DequeueBuffer for input buffer");
                goto EXIT;
            }

            if (VidEnc().isMetaDataSet())
            {
                int i;
                for (i=0;i<MAX_INPUT_OUTPUT_BUFFERS;i++)
                {
                     if (swp_buffers[i].omxBuffer == pInBufHdr)
                     {
                          swp_buffers[i].isFree = OMX_TRUE;
                          pInBufHdr->pBuffer = swp_buffers[i].pBuffer;
                          OMX_DBGT_PTRACE("Entry exists, changin isFree to OMX_TRUE in emptybufferdone");
                          break;
                     }
                }
                disconnectAssociation((void *)pInBufHdr);    
            }
            internalBufferPtr->isUsed = false;
            OMX_DBGT_PINFO("Abt to call emptybuffer done pInBufHdr : 0x%x pInBufHdr->pBuffer : 0x%x isMetaData : %d",(unsigned int)pInBufHdr,(unsigned int)pInBufHdr->pBuffer,VidEnc().isMetaDataSet());
            this->VidEnc().notifyEmptyBufferDone(pInBufHdr);

            OMX_DBGT_PINFO("Input buffer rel : 0x%x and isUsed : %d",(unsigned int)pInBufHdr,(int)internalBufferPtr->isUsed);
        }
        this->freeAndUnlockCurrent();
    }
EXIT:
    OMX_DBGT_PINFO("Pusher thread exit (VENCComponent::PusherRunner)");
    //flushPort(this);
    OMX_DBGT_EPILOG();
}


} // eof namespace stm
