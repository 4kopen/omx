/*
 * Copyright 2011 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *
 * File   ::   encoder.hpp
 * Author ::   Puneet Gulati(puneet.gulati@st.com)
 *
 */

#ifndef ST_OMX_V4L_ENCODER_H
#define ST_OMX_V4L_ENCODER_H

#define NUM_IN_BUFFERS 4
#define NUM_OUT_BUFFERS 5
#define MAX_INPUT_OUTPUT_BUFFERS 20


struct StoreMetaDataInBuffersParams {
    OMX_U32 nSize;
    OMX_VERSIONTYPE nVersion;
    OMX_U32 nPortIndex;
    OMX_BOOL bStoreMetaData;
};

struct buffer_t {
    struct v4l2_buffer v4l2_buf;
    void *start;
    void *start_phys;
    bool isUsed;
    unsigned int length;
};

typedef enum
{
	INTRAREFRESH_TYPE_NONE = 0,
	INTRAREFRESH_TYPE_CIR,
	INTRAREFRESH_TYPE_AIR,
	INTRAREFRESH_TYPE_BOTH
} IntraRefreshType;


typedef struct {
    struct buffer_t *internalBuffer;
    OMX_BUFFERHEADERTYPE *omxBuffer;
    OMX_BOOL isAssociated;
    OMX_U8 buffProperty;
} EncoderBufferMapping;


//Here when other encoders are added, corresponding structures can be added

/* structure to store internal H264 parameters */
typedef struct {
    enum v4l2_mpeg_video_h264_level LevelIDC;
    enum v4l2_mpeg_video_h264_profile ProfileIDC;
} h264_level_profile;

typedef struct {
    OMX_U32 IntraPeriod;    /* Random Acces Point periodicity */
    OMX_U32 GOPSize;        /* Intra-coded picture periodicity */
    OMX_BOOL IDRIntraEnable;    /* all Intra pictures are random acess points (SPS+PPS+IDR) */
    OMX_U32 FrameRate_Num;
    OMX_U32 FrameRate_Den;
    OMX_U16 width;      /* frame width in pixel, should be a multiple of 16. It shouldn't change within one sequence. (if it changes, a new sequence should be triggered) */
    OMX_U16 height;     /* frame width in pixel, should be a multiple of 16 */
    enum v4l2_mpeg_video_bitrate_mode BitRateControlType;
    OMX_U32 BitRate;        /* target bitrate for BCR  */
    OMX_U32 CpbBufferSize;  /* target cpb size (constrained by level selected) */
    OMX_U32 CbrDelay;       /* delay for CBR. */
    OMX_U16 qpmin;
    OMX_U16 qpmax;
    OMX_U32 AspectRatio;
    OMX_U32 nBFrameInterval;
    OMX_U16 EntropyMode;
    OMX_U16 num_input_buffers;
    OMX_U16 num_output_buffers;
    struct buffer_t *input_buffers;
    bool input_buf_allocated;    
    struct buffer_t *output_buffers;
    bool output_buf_allocated;
    OMX_U32 inputBufferSize;
    OMX_U32 outputBufferSize;
    OMX_U32 outputBufferType;
    OMX_U8 forceIntra;
    OMX_BOOL isOutputPortBufferAllocated;
    OMX_BOOL isInputPortBufferAllocated;
    IntraRefreshType intrarefreshmode;
    OMX_U32 IrParamOption;
} HVAHEncoder_t;
/* HVA encoding parameters - end*/

typedef enum
{
	ENCODER_TYPE_H264ENC = 0,
    ENCODER_TYPE_MAX = 0xff
} OMXEncoderType;

#endif /* ST_OMX_V4L_ENCODER_H */
