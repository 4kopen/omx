/*
 * Copyright 2013 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file   dvbvideo_sf.hpp
 * @author STMicroelectronics
 */


#ifndef ST_OMX_DVBVIDEO_SF_H
#define ST_OMX_DVBVIDEO_SF_H

#include "IDvbDevice.hpp"
#include "Mutex.hpp"
#include <linux/videodev2.h>
#include <linux/dvb/video.h>
#include "encoder.hpp"

#include <OMX_Video.h>

#define V4L_VIDEO_NAME_LENGTH 20

class DVBVideo_SF :public stm::IDvbDevice
{
public:
    virtual int id() const;
    virtual const char* name() const;

    DVBVideo_SF(const char* aDeviceName, const char *name, int aEncoderPort);
    virtual ~DVBVideo_SF();

    virtual int create(){return -1;};
    virtual int destroy();
    virtual bool isOpen() const;

    virtual int create(const char *const driver, const char *const card,int flags);
    int ConfigureEncoder();
    void configureEncoderStructure(void **encoderParam, int EncoderType);
    int setControls();
    int setupBufferCount();
    int setParam();
    int setUpBufferPointer();
    int freeBufferPointer();
    int QueueInitialBuffer(struct buffer_t *ptr);
    int DequeueEncoderBuffer(void* BufferData);
    int SpecificationCheck();
    int FormatCheck();
    int Open (int grabIdx);
    int Configure(int width, int height, uint32_t pixelformat, uint32_t bpp);
    virtual int start() const;
    virtual int stop() const;
    virtual int pause(bool hw) const;
    virtual int resume(bool hw) const;
    virtual void lock() const;
    virtual void unlock() const;
    virtual void resetflags() const;
    int QueueBuffer(void* BufferData, int &nBytesSize, uint64_t &ts) const;
    int setNewIntraPeriod(unsigned int intraPeriod);
    void setLevelandProfile(h264_level_profile l_p);
    char * V4lToString(int32_t v);

    virtual int UseBuffer(unsigned long virtualAddr, unsigned long bufferSize,
                  int width, int height, int color,
                  unsigned long userId) const{return -1;};
    virtual int FillThisBuffer(unsigned long userId) const {return -1;};
    virtual int FillBufferDone(unsigned long & userId, uint64_t & TS, bool & eos) const {return -1;};
    /** Return the current media time */
    virtual uint64_t getTime() const {return 0;};
    virtual uint32_t getSamplingRate() const {return 0;};
    /** Writes data to this device. */
    virtual int write(const void* data_ptr, unsigned int len) const {return -1;};

    virtual int flush(bool bEoS = false) const;
    virtual int clear() const;

    virtual stm::IDvbDevice::EState state() const;

    virtual unsigned long FlushBuffer() const;

private:
    void state(stm::IDvbDevice::EState st) const;

    int v4l_fd;
    char m_deviceName[V4L_VIDEO_NAME_LENGTH];
    //unique device name is not available till 'open'
    bool mIsLoggingEnabled;
    int device_id;
    int EncoderPort;
    struct v4l2_fmtdesc *formats;
    int nbFormats;
    HVAHEncoder_t *encoderHandle;
    h264_level_profile mH264ProfileLevel;
    OMXEncoderType encodeType;

    mutable stm::IDvbDevice::EState m_nState;
    /** Condition variable signaled when state has changed */
    mutable stm::Condition   m_condState;
};

#endif
