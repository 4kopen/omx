/*
 * Copyright 2013 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file   dvbvideo_sf.cpp
 * @author STMicroelectronics
 */

#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <stdio.h>
#include <fcntl.h>
#include <errno.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <OMX_debug.h>
#include "dvbvideo_sf.hpp"

#if HAVE_ASSERT_H
#include <assert.h>
#endif

#if HAVE_DBGTASSERT_H
// Re-direct assert on DBGT_ASSERT macro
#define assert(expr)  OMX_DBGT_ASSERT(expr)
#endif

#undef  DBGT_TAG
#define DBGT_TAG "DVBVS"
#define DBGT_PREFIX "DVBVS"
#define DBGT_LAYER  1
#define DBGT_VAR mDbgtVarDVBVS
#define DBGT_DECLARE_AUTOVAR
#include <linux/dbgt.h>

#define VID_ENCODE_MAX_CTRL             30
static  char cc[5];

#define   CHECK_DEVICE(v4l_fd)                                                                    \
        if (v4l_fd == 0) {                                                                        \
        OMX_DBGT_CRITICAL("Device Id is NULL for function : %s file : %s line : %d",__PRETTY_FUNCTION__, __FILE__, __LINE__);     \
        }

//=============================================================================
// DVBVideo_SF::DVBVideo_SF
//  Constructor
//=============================================================================
DVBVideo_SF::DVBVideo_SF(const char* aDeviceName, const char *cName, int aEncoderPort)
    :IDvbDevice(),
     v4l_fd(-1),
     mIsLoggingEnabled(false),
     device_id(-1),
     EncoderPort(aEncoderPort),
     formats(NULL),
     nbFormats(-1),
     encoderHandle(NULL),
     encodeType(ENCODER_TYPE_MAX),
     m_nState(EState_Stop),
     m_condState()
{
    DBGT_TRACE_INIT(venc);// Reuse VENC trace property
    memset((void *)cc,0x0,sizeof(cc));
    if (aDeviceName) 
    {
        //check the string length before copy
        assert(strlen(aDeviceName) <= sizeof(m_deviceName));
        strcpy(m_deviceName, aDeviceName);
    }
    else
    {
       memset((void *)m_deviceName,0,sizeof(m_deviceName));
    }
    mH264ProfileLevel.LevelIDC = V4L2_MPEG_VIDEO_H264_LEVEL_3_1;
    mH264ProfileLevel.ProfileIDC = V4L2_MPEG_VIDEO_H264_PROFILE_BASELINE;
    OMXDEBUG_UPDATE_AV_TRACE_SETTINGS(cName); //name of the omx component is passed here
}

//=====================================================================================
// DVBVideo_SF::create
//  To open the endoder device, this API is used for both
// encoder input and output
//=====================================================================================
int DVBVideo_SF::create(const char *const driver, const char *const card,int flags)
{
    struct v4l2_capability cap;
    int ret = -1;
    unsigned int i = 0;
    OMX_DBGT_PROLOG();

    if (!flags) {
        OMX_DBGT_PINFO("File mode not registered, would open in O_RDWR \n");
        flags = O_RDWR;
    }

    if (v4l_fd > 0) {
        OMX_DBGT_ERROR("Device dev id : %d, already been registered \n",v4l_fd);
        return ret;
    }

    do {
        sprintf(m_deviceName,"/dev/video%d", i);
        OMX_DBGT_PINFO("Searching for device : %s port : %d",m_deviceName,EncoderPort);
        v4l_fd = open(m_deviceName, flags);
        if (v4l_fd < 0) {
            OMX_DBGT_ERROR("Failed to find device for %s:%s\n", driver,card);
            return v4l_fd;
        }
        OMX_DBGT_PINFO("TRying to open for device : %s port : %d v4l_fd :%d ",m_deviceName,EncoderPort,v4l_fd);

        ret = ioctl(v4l_fd, VIDIOC_QUERYCAP, &cap);
        if (ret < 0) {
            OMX_DBGT_ERROR("Driver BUG: device %s doesn't answer to VIDIOC_QUERYCAP - ignoring for port : %d ",m_deviceName,EncoderPort);
            close(v4l_fd);
            continue;
        }

        if ((strcasecmp((char *)cap.driver, driver) == 0) &&
            (strcasecmp((char *)cap.card, card) == 0)) {
            device_id = i;
            OMX_DBGT_PINFO("Found device file: %s for %s:%s", m_deviceName,
                   driver, card);
            break;
        }

        close(v4l_fd);
        i++;
    } while (i);

    OMX_DBGT_EPILOG("Value of v4l_fd : %d device name : %s device id : %d",v4l_fd,m_deviceName,device_id);
    return ret;
}

//=====================================================================================
// V4LVideo_SF::configureEncoderStructure
//=====================================================================================
void DVBVideo_SF::configureEncoderStructure(void **encoderParam, int EncoderType)
{
    CHECK_DEVICE(v4l_fd);
    encodeType = (OMXEncoderType) EncoderType;
    switch (encodeType)
    {
        case ENCODER_TYPE_H264ENC : OMX_DBGT_PINFO("OMX encoderHandle type registered");
            encoderHandle = (HVAHEncoder_t *)*encoderParam;
            break;
        default: OMX_DBGT_CRITICAL("EncodeType : %d not supported",encodeType);
            break;
    }
    OMX_DBGT_PINFO("Encoder param structure configured : 0x%x encodeType : %d",(unsigned int)encoderHandle,encodeType);
}

//=====================================================================================
// V4LVideo_SF::SpecificationCheck
//=====================================================================================
int DVBVideo_SF::SpecificationCheck()
{
    struct v4l2_capability cap;
    int ret = -1;
    OMX_DBGT_PROLOG();
    CHECK_DEVICE(v4l_fd);
    memset (&cap, 0, sizeof (struct v4l2_capability));

    /* Get device capabilites */
    ret = ioctl(v4l_fd, VIDIOC_QUERYCAP, &cap);
    if (ret< 0)
    {
        OMX_DBGT_ERROR("Unable to open V4L Device (%s) for VIDIOC_QUERYCAP", strerror(errno));
        goto EXIT;
    }
    if (!(cap.capabilities & V4L2_CAP_VIDEO_CAPTURE)) {
        OMX_DBGT_ERROR("V4L2_CAP_VIDEO_CAPTURE should be supported");
        return -1;
    } else {
        OMX_DBGT_PINFO("V4L2_CAP_VIDEO_CAPTURE supported!");
    }
    if (!(cap.capabilities & V4L2_CAP_VIDEO_OUTPUT)) {
        OMX_DBGT_ERROR("V4L2_CAP_VIDEO_OUTPUT should be supported");
        return -1;
    } else {
        OMX_DBGT_PINFO("V4L2_CAP_VIDEO_OUTPUT supported!");
    }
    if (!(cap.capabilities & V4L2_CAP_STREAMING)) {
        OMX_DBGT_ERROR("V4L2_CAP_STREAMING should be supported");
        return -1;
    } else {
        OMX_DBGT_PINFO("V4L2_CAP_STREAMING supported!");
    }
    if (!(cap.capabilities & V4L2_CAP_READWRITE)) {
        OMX_DBGT_ERROR("V4L2_CAP_READWRITE should be supported");
        return -1;
    } else {
        OMX_DBGT_PINFO("V4L2_CAP_READWRITE supported!");
    }
    if (!(cap.capabilities & V4L2_CAP_ASYNCIO)) {
        OMX_DBGT_ERROR("V4L2_CAP_ASYNCIO should be supported");
        return -1;
    } else {
        OMX_DBGT_PINFO("V4L2_CAP_ASYNCIO supported!");
    }

EXIT:
    OMX_DBGT_EPILOG();
    return ret;
}

char * DVBVideo_SF::V4lToString(int32_t v)
{
    //	v = ((__u32)(a) | ((__u32)(b) << 8) | ((__u32)(c) << 16) | ((__u32)(d) << 24))
    cc[0] = v       & 0xff;
    cc[1] = (v>>8)  & 0xff;
    cc[2] = (v>>16) & 0xff;
    cc[3] = (v>>24) & 0xff;
    cc[4] = 0;
    return cc;
}

//=====================================================================================
// V4LVideo_SF::FormatCheck
//=====================================================================================
int DVBVideo_SF::FormatCheck()
{
    int nfmts = 0;
    int i;
    struct v4l2_fmtdesc fmt;
    OMX_DBGT_PROLOG();
    CHECK_DEVICE(v4l_fd);
    fmt.index = 0;

    if (EncoderPort == 0)
    {
        fmt.type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
    }
    else
    {
        fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    }

    while (ioctl(v4l_fd, VIDIOC_ENUM_FMT, &fmt) >= 0)
        fmt.index = ++nfmts;

    if (nfmts == 0)
    {
        OMX_DBGT_ERROR("Not able to retrieve formats");
        return -1;
    }

    //allocating memory for format structure, would be free when VIDIOC_S_FMT is done
    formats = (v4l2_fmtdesc *)malloc(nfmts * sizeof(v4l2_fmtdesc));
    if (!formats){
        OMX_DBGT_ERROR("Failed to allocate memory for V4L2 formats\n");
        return -1;
    }

    //printing all video formats supported
    for (i = 0; i < nfmts; i++) 
    {
        formats[i].index = i;
        formats[i].type = fmt.type;
        if (ioctl(v4l_fd, VIDIOC_ENUM_FMT, &formats[i]) < 0)
        {
            OMX_DBGT_ERROR("cannot get format description for index %u",formats[i].index);
            free(formats);
            formats = NULL;
            return -1;
        }
        OMX_DBGT_PDEBUG("For Port : %d Description %s format : V4L2_PIX_FMT_%s",EncoderPort, formats[i].description,V4lToString(formats[i].pixelformat));
    }
    nbFormats = nfmts;
    OMX_DBGT_EPILOG();
    return 0;
}

bool DVBVideo_SF::isOpen() const
{ return (id() != -1); }

//=============================================================================
// DVBVideo_SF::destroy
//=============================================================================
int DVBVideo_SF::destroy()
{
    OMX_DBGT_PROLOG();
    if(close(id()) != 0) {
        OMX_DBGT_ERROR("Video close failed (%s)", strerror (errno));
        OMX_DBGT_EPILOG("Unable to close device");
        return -1;
    }

    v4l_fd = -1;
    OMX_DBGT_EPILOG();
    return 0;
}

//=============================================================================
// DVBVideo_SF::pause (empty function)
//=============================================================================
int DVBVideo_SF::pause(bool hw) const
{
    assert(0);
    return 0;
}

//=============================================================================
// DVBVideo_SF::resume (empty function)
//=============================================================================
int DVBVideo_SF::resume(bool hw) const
{
    assert(0);
    return 0;
}


//=============================================================================
// DVBVideo_SF::lock (empty function)
//=============================================================================
void DVBVideo_SF::lock() const
{
}

//=============================================================================
// DVBVideo_SF::unlock (empty function)
//=============================================================================
void DVBVideo_SF::unlock() const
{
}

//=============================================================================
// DVBVideo_SF::resetflags (empty function)
//=============================================================================
void DVBVideo_SF::resetflags() const
{
}

//=====================================================================================
// DVBVideo_SF::ConfigureEncoder
//=====================================================================================
int DVBVideo_SF::ConfigureEncoder()
{
    int ret = -1;
    struct v4l2_output outputtype;
    struct v4l2_input inputtype;
    int index;
    OMX_DBGT_PROLOG();
    device_id = 0;
    CHECK_DEVICE(v4l_fd);

    if (EncoderPort < 0) 
    {
        OMX_DBGT_ERROR("Invalid deviceId : %d OR EncoderPort : %d",device_id,EncoderPort);
    }
    else
    {
        if (EncoderPort == 0)
        {
            ret = ioctl(v4l_fd, VIDIOC_S_OUTPUT, &device_id);
            if (ret < 0)
            {
                OMX_DBGT_ERROR("Unable to open V4L Device (%s) for VIDIOC_S_OUTPUT", strerror(errno));
                return -1;
            }

            ret = ioctl (v4l_fd, VIDIOC_G_OUTPUT, &index);
            if (ret == 0)
            {
                OMX_DBGT_PINFO("Value of index returned by VIDIOC_G_OUTPUT : %d",index);
            }
            else
            {
                OMX_DBGT_ERROR("Unable to open V4L Device (%s) for VIDIOC_G_OUTPUT", strerror(errno));
                return -1;
            }

            memset(&outputtype,0,sizeof(outputtype));
            outputtype.index = index;
            ret = ioctl(v4l_fd, VIDIOC_ENUMOUTPUT, &outputtype);
            if (ret == 0)
            {
                OMX_DBGT_PINFO("Supported Output : %s type : %d index : %d",outputtype.name,outputtype.type,outputtype.index);
            }
            else
            {
                OMX_DBGT_ERROR("Unable to open V4L Device (%s) for VIDIOC_ENUMOUTPUT", strerror(errno));
                return -1;
            }
        }
        else if (EncoderPort == 1)
        {
            ret = ioctl(v4l_fd, VIDIOC_S_INPUT, &device_id);
            if (ret < 0)
            {
                OMX_DBGT_ERROR("Unable to open V4L Device (%s) for VIDIOC_S_INPUT", strerror(errno));
                return -1;
            }

            ret = ioctl (v4l_fd, VIDIOC_G_INPUT, &index);
            if (ret == 0)
            {
                OMX_DBGT_PINFO("Value of index returned by VIDIOC_G_INPUT : %d",index);
            }
            else
                return -1;

            memset(&inputtype,0,sizeof(inputtype));
            if (ret == 0)
            {
                inputtype.index = index;
                ret = ioctl(v4l_fd, VIDIOC_ENUMINPUT, &inputtype);
                if (ret ==0)
                {
                    OMX_DBGT_PINFO("Supported Input : %s type : %d index : %d",inputtype.name,inputtype.type,inputtype.index);
                }
            }
        }
        else
        {
            OMX_DBGT_CRITICAL("Invalid EncoderPort : %u for ConfigureEncoder",EncoderPort);
        }
    }

    OMX_DBGT_EPILOG();
    return ret;
}

//=====================================================================================
// DVBVideo_SF::setupBuffer
//=====================================================================================
int DVBVideo_SF::setupBufferCount()
{
    int ret=-1;
    unsigned int nb_buffer = 0;
    struct v4l2_requestbuffers v4l2reqbuf;
    OMX_DBGT_PROLOG();

    /* Buffers allocation */
    /* Request input buffers from v4l2 */
    memset (&v4l2reqbuf, 0, sizeof (struct v4l2_requestbuffers));
    v4l2reqbuf.memory = V4L2_MEMORY_MMAP;

    if (EncoderPort == 0)
    {
        v4l2reqbuf.count = encoderHandle->num_input_buffers;
        nb_buffer = encoderHandle->num_input_buffers;
        v4l2reqbuf.type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
        if (!encoderHandle->isInputPortBufferAllocated)
	    {
            v4l2reqbuf.memory = V4L2_MEMORY_USERPTR;
		}
		OMX_DBGT_PTRACE("Allocating memory for internal input structure, isInputPortBufferAllocated : %d",encoderHandle->isInputPortBufferAllocated);
            
        encoderHandle->input_buffers = (buffer_t *)calloc (encoderHandle->num_input_buffers,sizeof (*encoderHandle->input_buffers));
    }
    else
    {
        v4l2reqbuf.count = encoderHandle->num_output_buffers;
        v4l2reqbuf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        nb_buffer = encoderHandle->num_output_buffers;
        if (!encoderHandle->isOutputPortBufferAllocated)
        {
            v4l2reqbuf.memory = V4L2_MEMORY_USERPTR;
		}
        OMX_DBGT_PTRACE("Allocating memory for internal output structure, isOutputPortBufferAllocated : %d",encoderHandle->isOutputPortBufferAllocated);;
            
        encoderHandle->output_buffers = (buffer_t *)calloc (encoderHandle->num_output_buffers,sizeof (*encoderHandle->output_buffers));     
    }
    OMX_DBGT_PTRACE("Abt to call VIDIOC_REQBUFS for port %d buff cnt : %d memory type : 0x%x",EncoderPort,v4l2reqbuf.count,(unsigned int)v4l2reqbuf.memory);
    ret = ioctl(v4l_fd, VIDIOC_REQBUFS, &v4l2reqbuf);
    if (ret < 0)
    {
        OMX_DBGT_ERROR("Cannot do VIDIOC_REQBUFS for EncoderPort : %u and return Value : %d Msg (%s)",EncoderPort,ret,strerror (errno));
        return ret;
    }
    OMX_DBGT_PTRACE("After VIDIOC_REQBUFS, buffercount from SE :%d , nBufferCountActual : %d for port : %d",v4l2reqbuf.count,nb_buffer,EncoderPort);
    if (v4l2reqbuf.count != nb_buffer) 
    {
        OMX_DBGT_ERROR("Error in VIDIOC_REQBUFS, buffers in Use (%d) and Required (%d) for Port : %d",nb_buffer,v4l2reqbuf.count,EncoderPort);
        return -1;
    }

    OMX_DBGT_EPILOG();
    return ret;
}

//=====================================================================================
// DVBVideo_SF::setParam
//=====================================================================================
int DVBVideo_SF::setUpBufferPointer()
{
    int ret = -1;
    int i=0;
    OMX_DBGT_PROLOG();
    if (EncoderPort == 0)
    {

        /* Query input buffers and map buffers */
        for (i = 0; i < encoderHandle->num_input_buffers; i++)
        {
            encoderHandle->input_buffers[i].start = (void *) -1;
            memset (&encoderHandle->input_buffers[i].v4l2_buf, 0,sizeof (struct v4l2_buffer));
            encoderHandle->input_buffers[i].v4l2_buf.type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
            encoderHandle->input_buffers[i].v4l2_buf.index = i;
            encoderHandle->input_buffers[i].v4l2_buf.memory = V4L2_MEMORY_MMAP;

            /* query buffer */
            ret = ioctl(v4l_fd, VIDIOC_QUERYBUF, &encoderHandle->input_buffers[i].v4l2_buf);
            if (ret < 0)
            {
                OMX_DBGT_ERROR("query input buffers failed - %s", strerror (errno));
                return -1;
            }
            OMX_DBGT_PINFO("Sucess for VIDIOC_QUERYBUF for input length : %d",(int)encoderHandle->input_buffers[i].v4l2_buf.length);
            encoderHandle->input_buffers[i].length = encoderHandle->input_buffers[i].v4l2_buf.length;
            encoderHandle->input_buffers[i].isUsed = false;
            encoderHandle->input_buffers[i].start =
                                          (unsigned char *) mmap (0, encoderHandle->input_buffers[i].v4l2_buf.length,
                                            PROT_READ | PROT_WRITE, MAP_SHARED, v4l_fd,
                                          encoderHandle->input_buffers[i].v4l2_buf.m.offset);

            OMX_DBGT_PINFO( "Buffer for input start address : 0x%x ",(unsigned int)encoderHandle->input_buffers[i].start);
            if (encoderHandle->input_buffers[i].start == ((void *) -1)) 
            {
                OMX_DBGT_ERROR("mmap buffer failed for input");
                return -1;
            }
        }
    }
    else
    {

        /* Query output buffers and map buffers */
        for (i = 0; i < encoderHandle->num_output_buffers; i++) 
        {
            encoderHandle->output_buffers[i].start = (void *) -1;
            memset (&encoderHandle->output_buffers[i].v4l2_buf, 0,sizeof (struct v4l2_buffer));
            encoderHandle->output_buffers[i].v4l2_buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
            encoderHandle->output_buffers[i].v4l2_buf.index = i;
            encoderHandle->output_buffers[i].v4l2_buf.memory = V4L2_MEMORY_MMAP;

            /* query buffer */
            ret = ioctl(v4l_fd, VIDIOC_QUERYBUF,&encoderHandle->output_buffers[i].v4l2_buf);
            if (ret < 0)
            {
                OMX_DBGT_ERROR("query output buffers failed - %s",         strerror (errno));
                return -1;
            }

            OMX_DBGT_PINFO("sucess for output VIDIOC_QUERYBUF length : %d",(int)encoderHandle->output_buffers[i].v4l2_buf.length);
            encoderHandle->output_buffers[i].length = encoderHandle->output_buffers[i].v4l2_buf.length;
            encoderHandle->output_buffers[i].isUsed = false;
            encoderHandle->output_buffers[i].start =
                (unsigned char *) mmap (0, encoderHandle->output_buffers[i].v4l2_buf.length,
                                 PROT_READ | PROT_WRITE, MAP_SHARED, v4l_fd,
                                 encoderHandle->output_buffers[i].v4l2_buf.m.offset);

            OMX_DBGT_PINFO( "Buffer for output : 0x%x ",(unsigned int)encoderHandle->output_buffers[i].start);
            
            if (encoderHandle->output_buffers[i].start == ((void *) -1)) 
            {
                OMX_DBGT_ERROR("mmap buffer failed for output");
                return -1;
            }            
        }
    }
    OMX_DBGT_EPILOG();
    return ret;
}

//=====================================================================================
// DVBVideo_SF::freeBufferPointer
//=====================================================================================
int DVBVideo_SF::freeBufferPointer()
{
    int ret = -1;
    int i=0;
    OMX_DBGT_PROLOG("For Port : %d",EncoderPort);
    if (EncoderPort == 0)
    {
		if (encoderHandle->isInputPortBufferAllocated && encoderHandle->input_buffers)
		{
            /* Query input buffers and map buffers */
            for (i = 0; i < encoderHandle->num_input_buffers; i++)
            {
                ret = -1;
                OMX_DBGT_PTRACE("encoderHandle->input_buffers[i].start : 0x%x length : %d",(unsigned int)encoderHandle->input_buffers[i].start,encoderHandle->input_buffers[i].v4l2_buf.length);
                if (encoderHandle->input_buffers[i].start != ((void *) -1))
                {
                    OMX_DBGT_PINFO("abt todo unmap for encoderHandle->input_buffers[i].start : 0x%x",(unsigned int)encoderHandle->input_buffers[i].start);
                    munmap (encoderHandle->input_buffers[i].start, encoderHandle->input_buffers[i].v4l2_buf.length);
                    encoderHandle->input_buffers[i].start = ((void *) -1);
                    ret = 0;
                }           
            }
		}
		if (encoderHandle->input_buffers)
		{
			OMX_DBGT_PDEBUG("Freeing input buffer memory");
            free(encoderHandle->input_buffers);
            encoderHandle->input_buffers = NULL;
		}
    }
    else
    {
		if (encoderHandle->isOutputPortBufferAllocated && encoderHandle->output_buffers)
		{
            /* Query input buffers and map buffers */
            for (i = 0; i < encoderHandle->num_output_buffers; i++)
            {
                ret = -1;
                OMX_DBGT_PTRACE("encoderHandle->output_buffers[i].start : 0x%x length : %d",(unsigned int)encoderHandle->output_buffers[i].start,encoderHandle->output_buffers[i].v4l2_buf.length);
                if (encoderHandle->output_buffers[i].start != ((void *) -1))
                {
                    OMX_DBGT_PINFO("abt todo unmap for encoderHandle->output_buffers[i].start : 0x%x",(unsigned int)encoderHandle->output_buffers[i].start);
                    munmap (encoderHandle->output_buffers[i].start, encoderHandle->output_buffers[i].v4l2_buf.length);
                    encoderHandle->output_buffers[i].start = ((void *) -1); 
                    ret = 0;              
                }
            }
		}
		if (encoderHandle->output_buffers)
		{
			OMX_DBGT_PDEBUG("Freeing output buffer memory");			
            free(encoderHandle->output_buffers);
            encoderHandle->output_buffers = NULL;
		}
    }
    OMX_DBGT_EPILOG();
    return ret;
}

//=====================================================================================
// DVBVideo_SF::setParam
//=====================================================================================
int DVBVideo_SF::setParam()
{
    OMX_DBGT_PROLOG("For Port : %d ",EncoderPort);
    int ret = -1;
    struct v4l2_streamparm v4l2sparam;

    memset(&v4l2sparam, 0, sizeof(struct v4l2_streamparm));

    //intentionally swapping the numerator and denominator (it is CORRECT)
    v4l2sparam.parm.capture.timeperframe.numerator = encoderHandle->FrameRate_Den;
    v4l2sparam.parm.capture.timeperframe.denominator = encoderHandle->FrameRate_Num;
    OMX_DBGT_PINFO("Frame Numerator : %d Denominator : %d",v4l2sparam.parm.capture.timeperframe.numerator,v4l2sparam.parm.capture.timeperframe.denominator);

    if (EncoderPort == 0)
    {
        v4l2sparam.type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
    }
    else
    {
        v4l2sparam.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    }
    ret = ioctl(v4l_fd, VIDIOC_S_PARM, &v4l2sparam);
    if (ret < 0)
    {
        OMX_DBGT_ERROR("Cannot to VIDIOC_S_PARM for EncoderPort : %u and return Value : %d",EncoderPort,ret);
    }

    OMX_DBGT_EPILOG();
    return ret;
}

void DVBVideo_SF::setLevelandProfile(h264_level_profile l_p)
{
    OMX_DBGT_PROLOG();
    mH264ProfileLevel.LevelIDC    = l_p.LevelIDC;
    mH264ProfileLevel.ProfileIDC = l_p.ProfileIDC;
    OMX_DBGT_EPILOG();
}
//=====================================================================================
// DVBVideo_SF::setNewIntraPeriod
//=====================================================================================
int DVBVideo_SF::setNewIntraPeriod(unsigned int intraPeriod)
{
    int ret = -1;
    unsigned int i=0;
    OMX_DBGT_PROLOG();
    struct v4l2_ext_controls v4l2controls;
    struct v4l2_ext_control v4l2control[VID_ENCODE_MAX_CTRL];

    memset(&v4l2controls, 0, sizeof(struct v4l2_ext_controls));
    v4l2controls.ctrl_class = V4L2_CTRL_CLASS_MPEG;
    v4l2controls.count = 0;
    v4l2controls.controls = v4l2control;
        
    i = 0;
    v4l2control[i].id = V4L2_CID_MPEG_VIDEO_GOP_SIZE;
    v4l2control[i].value = intraPeriod;
    i++;    

    v4l2controls.count = i;
    ret = ioctl(v4l_fd, VIDIOC_S_EXT_CTRLS, &v4l2controls);
    if (ret < 0)
    {
        OMX_DBGT_ERROR("Cannot to VIDIOC_S_EXT_CTRLS for for setting new IntraPeriod, return Value : %d",ret);
    }
    OMX_DBGT_EPILOG();
    return ret;
}

int DVBVideo_SF::setControls()
{
    int ret = -1;
    unsigned int i=0;
    struct v4l2_ext_controls v4l2controls;
    struct v4l2_ext_control v4l2control[VID_ENCODE_MAX_CTRL];
    OMX_DBGT_PROLOG();

    if (EncoderPort != 1)
    {
        OMX_DBGT_ERROR("setControls called for wrong encoder Port : %d",EncoderPort);
        return ret;
    }

    memset(&v4l2controls, 0, sizeof(struct v4l2_ext_controls));
    v4l2controls.ctrl_class = V4L2_CTRL_CLASS_MPEG;
    v4l2controls.count = 0;
    v4l2controls.controls = v4l2control;
        
    i = 0;
    v4l2control[i].id = V4L2_CID_MPEG_VIDEO_H264_LEVEL;
    v4l2control[i].value = mH264ProfileLevel.LevelIDC;
    i++;
    v4l2control[i].id = V4L2_CID_MPEG_VIDEO_H264_PROFILE;
    v4l2control[i].value = mH264ProfileLevel.ProfileIDC;
    i++;
    v4l2control[i].id = V4L2_CID_MPEG_VIDEO_BITRATE_MODE;
    v4l2control[i].value = encoderHandle->BitRateControlType;//V4L2_MPEG_VIDEO_BITRATE_MODE_VBR;
    i++;
    v4l2control[i].id = V4L2_CID_MPEG_VIDEO_BITRATE;
    v4l2control[i].value = 1000000;//encoderHandle->BitRate;
    i++;
//    v4l2control[i].id = V4L2_CID_MPEG_VIDEO_H264_CPB_SIZE;
//    v4l2control[i].value = encoderHandle->CpbBufferSize;
//    i++;
    v4l2control[i].id = V4L2_CID_MPEG_VIDEO_GOP_SIZE;
    v4l2control[i].value = encoderHandle->IntraPeriod;
    i++;    
//        v4l2control[i].id = V4L2_CID_MPEG_VIDEO_H264_I_FRAME_QP;
//        v4l2control[i].value = 20;
//        i++;
//        v4l2control[i].id = V4L2_CID_MPEG_VIDEO_H264_P_FRAME_QP;
//        v4l2control[i].value = 20;
//        i++;
//        v4l2control[i].id = V4L2_CID_MPEG_VIDEO_H264_B_FRAME_QP;
//        v4l2control[i].value = 20;
//        i++;
//        v4l2control[i].id = V4L2_CID_MPEG_VIDEO_H264_MIN_QP;
//        v4l2control[i].value = 10;
//        i++;
//        v4l2control[i].id = V4L2_CID_MPEG_VIDEO_H264_MAX_QP;
//        v4l2control[i].value = 46;
//        i++;
//        v4l2control[i].id = V4L2_CID_MPEG_VIDEO_H264_ENTROPY_MODE;
//        v4l2control[i].value = V4L2_MPEG_VIDEO_H264_ENTROPY_MODE_CAVLC;
//        i++;
//        v4l2control[i].id = V4L2_CID_MPEG_VIDEO_H264_I_PERIOD;
//        v4l2control[i].value = 10;
//        i++;
//        v4l2control[i].id = V4L2_CID_MPEG_VIDEO_B_FRAMES;
//        v4l2control[i].value = 0;
//        i++;
//        v4l2control[i].id = V4L2_CID_MPEG_VIDEO_ASPECT;
//        v4l2control[i].value = V4L2_MPEG_VIDEO_ASPECT_4x3;
//        i++;

    OMX_DBGT_PINFO("Settings sent to Encoder profile : 0x%x level : 0x%x bitrate : %d CBRMode : 0x%x IntraPeriod : %d",
         (unsigned int)mH264ProfileLevel.ProfileIDC,(unsigned int)mH264ProfileLevel.LevelIDC,
         (int)encoderHandle->BitRate,(unsigned int)encoderHandle->BitRateControlType,(int)encoderHandle->IntraPeriod);
    v4l2controls.count = i;
    ret = ioctl(v4l_fd, VIDIOC_S_EXT_CTRLS, &v4l2controls);
    if (ret < 0)
    {
        OMX_DBGT_ERROR("Cannot to VIDIOC_S_EXT_CTRLS for EncoderPort : %u and return Value : %d",EncoderPort,ret);
    }
    OMX_DBGT_EPILOG();
    return ret;
}

//=====================================================================================
// DVBVideo_SF::Configure
//=====================================================================================
int DVBVideo_SF::Configure(int width, int height, uint32_t pixelformat, uint32_t bpp)
{
    struct v4l2_format          Format;
    int i;
    int index=0;
    const char *format_name[] = {"V4L2_PIX_FMT_YUV420","V4L2_PIX_FMT_NV12","V4L2_PIX_FMT_H264",NULL};

    OMX_DBGT_PROLOG();

    memset (&Format, 0, sizeof (struct v4l2_format));
    Format.fmt.pix.field                = V4L2_FIELD_NONE;
    Format.fmt.pix.pixelformat          = pixelformat;
    Format.type                         = V4L2_BUF_TYPE_VIDEO_CAPTURE;

    switch (Format.fmt.pix.pixelformat)
    {
        case V4L2_PIX_FMT_YUV420: index = 0;  break;
        case V4L2_PIX_FMT_NV12: index = 1;    break;
        case V4L2_PIX_FMT_H264: index = 2;    break;
    }

    if (EncoderPort == 0)
    {
        Format.type                         = V4L2_BUF_TYPE_VIDEO_OUTPUT;
        Format.fmt.pix.pixelformat =  V4L2_PIX_FMT_NV12;
    }

    if (EncoderPort == 1)
    {
        Format.type                         = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    }
    
    Format.fmt.pix.width                = width;
    Format.fmt.pix.height               = height;
    Format.fmt.pix.bytesperline         = width * bpp / 8;

    /* Check whether the desired format is available */
    i = 0;
    while ((i < nbFormats) && (formats[i].pixelformat != Format.fmt.pix.pixelformat)) 
    {
        i++;
    }

    /* freeing the memory here */
    if (formats) 
    {
        free(formats);
        formats = NULL;
    }

    if (i == nbFormats) {
        OMX_DBGT_ERROR(" desired format %s for capture queue is not supported at Port : %d",format_name[index],EncoderPort);
        return -1;
    }

    if (ioctl (v4l_fd, VIDIOC_S_FMT, &Format) < 0)
    {
        OMX_DBGT_ERROR("Set Format (VIDIOC_S_FMT) failed (%s)", strerror (errno));
        OMX_DBGT_EPILOG();
        return -1;
    }

    OMX_DBGT_EPILOG();
    return 0;
}

//=====================================================================================
// DVBVideo_SF::Start
//=====================================================================================
int DVBVideo_SF::start() const
{
    int     buftype;

    OMX_DBGT_PROLOG("Value of v4l_fd : %d for Port : %d",v4l_fd,EncoderPort);

    if (EncoderPort == 0)
    {    
        buftype                         = (int)V4L2_BUF_TYPE_VIDEO_OUTPUT;
    }
    else
    {
        buftype                         = (int)V4L2_BUF_TYPE_VIDEO_CAPTURE;
    }
    OMX_DBGT_PINFO("To call ioctl value of v4l_fd : %d for Port : %d",v4l_fd,EncoderPort);
    if (ioctl (v4l_fd, VIDIOC_STREAMON, &buftype) < 0)
    {
        OMX_DBGT_ERROR("Switching stream on (VIDIOC_STREAMON) failed (%s)", strerror (errno));
        OMX_DBGT_EPILOG();
        return -1;
    }
    this->state(stm::IDvbDevice::EState_Start);

    OMX_DBGT_EPILOG();
    return 0;
}

//=====================================================================================
// DVBVideo_SF::Stop
//=====================================================================================
int DVBVideo_SF::stop() const
{
    struct v4l2_buffer          Buffer;

    OMX_DBGT_PROLOG();

    memset (&Buffer, 0, sizeof (struct v4l2_buffer));
    if (EncoderPort == 0)
    {    
        Buffer.type                         = V4L2_BUF_TYPE_VIDEO_OUTPUT;
    }
    else
    {
        Buffer.type                         = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    }
    if (ioctl (v4l_fd, VIDIOC_STREAMOFF, &Buffer.type) < 0)
    {
        OMX_DBGT_ERROR("Switching stream on (VIDIOC_STREAMOFF) failed (%s)", strerror (errno));
        OMX_DBGT_EPILOG();
        return -1;
    }
    this->state(stm::IDvbDevice::EState_Stop);

    OMX_DBGT_EPILOG();
    return 0;
}

//==============================================================================
// DVBVideo_SF::flush
//==============================================================================
int DVBVideo_SF::flush(bool bEoS) const
{
    OMX_DBGT_PROLOG();
/*
    assert(isOpen());
    if (this->isStopped()) {
        OMX_DBGT_EPILOG("Flushing a stopped device");
        return 0;
    }
    if (bEoS) {
        // Inject EOS discontinuity.
        if (ioctl(id(), VIDEO_DISCONTINUITY, (void*)DVB_DISCONTINUITY_EOS) != 0)
        {
            OMX_DBGT_ERROR("VIDEO_DISCONTINUITY ioctl failed (%s)",
                       strerror (errno));
            OMX_DBGT_EPILOG();
            return -1;
        }
    }
    // Wait till pipeline is flushed
    if (ioctl(id(), VIDEO_FLUSH, NULL) != 0) {
        OMX_DBGT_ERROR("VIDEO_FLUSH ioctl failed (%s)", strerror (errno));
        OMX_DBGT_EPILOG();
        return -1;
    }

    this->state(this->state());
*/
    OMX_DBGT_EPILOG();
    return 0;
}

DVBVideo_SF::~DVBVideo_SF()
{}

//==============================================================================
// DVBVideo_SF::FlushBuffer (empty function)
//==============================================================================
unsigned long DVBVideo_SF::FlushBuffer() const
{
    assert(0);
    return 0;
}

//=====================================================================================
// DVBVideo_SF::QueueBuffer
//=====================================================================================
int DVBVideo_SF::QueueBuffer(void* BufferData, int &nBytesSize, uint64_t &ts) const
{
    struct v4l2_buffer  Buffer;
    struct buffer_t *internalBuffer;
    int retVal = -1;

    OMX_DBGT_PROLOG();
    
    if (EncoderPort  == 0 || EncoderPort ==1)
    {
        internalBuffer = (struct buffer_t *)BufferData;
        internalBuffer->v4l2_buf.bytesused = nBytesSize;        
        if (EncoderPort == 0)
        {
            internalBuffer->v4l2_buf.length = encoderHandle->inputBufferSize;
            if (!encoderHandle->isInputPortBufferAllocated)
            {
                internalBuffer->v4l2_buf.m.userptr = (long unsigned int)internalBuffer->start;
                internalBuffer->v4l2_buf.memory = V4L2_MEMORY_USERPTR;
                internalBuffer->v4l2_buf.type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
                internalBuffer->v4l2_buf.length = nBytesSize;
			}
        }
        else
        {
            internalBuffer->v4l2_buf.length = encoderHandle->outputBufferSize;
            internalBuffer->v4l2_buf.bytesused = 0;
            if (!encoderHandle->isOutputPortBufferAllocated)
            {
                internalBuffer->v4l2_buf.m.userptr = (long unsigned int)internalBuffer->start;
                internalBuffer->v4l2_buf.memory = V4L2_MEMORY_USERPTR;
                internalBuffer->v4l2_buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
			}            
        }
        internalBuffer->v4l2_buf.field = V4L2_FIELD_ANY;

        OMX_DBGT_PINFO("queue buffer v4l2 for port : %d type  : %d, memory : %d index : %d start addr 0x%x len : %d filledlen : %d",
                    EncoderPort,
                    (int)internalBuffer->v4l2_buf.type,
                    (int)internalBuffer->v4l2_buf.memory,
                    (int)internalBuffer->v4l2_buf.index,
                    (unsigned int)internalBuffer->start,
                    (int)internalBuffer->length,
                    (unsigned int)internalBuffer->v4l2_buf.bytesused);          
        
        retVal = ioctl(v4l_fd, VIDIOC_QBUF, &internalBuffer->v4l2_buf);
        if (retVal < 0)
        {
            OMX_DBGT_ERROR("VIDIOC_QBUF failed for output- %s",strerror (errno));
        }
        internalBuffer->isUsed = true;
    }
    else
    {
        OMX_DBGT_CRITICAL("Wrong encoder Port : %d",EncoderPort);
    }
    OMX_DBGT_EPILOG();
    return retVal;
}

//=====================================================================================
// DVBVideo_SF::DequeueEncoderBuffer
//=====================================================================================
int DVBVideo_SF::DequeueEncoderBuffer(void* BufferData)
{
    struct buffer_t *internalBuffer;
    OMX_DBGT_PROLOG();

    internalBuffer = (struct buffer_t *)BufferData;
    if (ioctl (v4l_fd, VIDIOC_DQBUF, &internalBuffer->v4l2_buf) < 0)
    {
        OMX_DBGT_ERROR("Failed to dequeue buffer (VIDIOC_DQBUF) (%s)", strerror (errno));
        OMX_DBGT_EPILOG();
        return -1;
    }

    OMX_DBGT_PINFO("dequeue buffer v4l2 for port : %d type  : %d, memory : %d index : %d start addr 0x%x len : %d filledlen : %d",
                    EncoderPort,
                    (int)internalBuffer->v4l2_buf.type,
                    (int)internalBuffer->v4l2_buf.memory,
                    (int)internalBuffer->v4l2_buf.index,
                    (unsigned int)internalBuffer->start,
                    (int)internalBuffer->length,
                    (unsigned int)internalBuffer->v4l2_buf.bytesused);          
    
    encoderHandle->outputBufferType = internalBuffer->v4l2_buf.flags;
    OMX_DBGT_EPILOG();
    return 0;
}

const char* DVBVideo_SF::name() const
{ return m_deviceName; }

int DVBVideo_SF::id() const
{ return v4l_fd; }

void DVBVideo_SF::state(stm::IDvbDevice::EState st) const
{
    m_nState = st;
    m_condState.signal();
}

stm::IDvbDevice::EState DVBVideo_SF::state() const
{ return m_nState; }

//==============================================================================
// DVBVideo_MOO::clear
//==============================================================================
int DVBVideo_SF::clear() const
{
    OMX_DBGT_PROLOG();
    assert(isOpen());
    if (this->isStopped()) {
        OMX_DBGT_EPILOG("Clear on a stopped device");
        return 0;
    }
    // Flush the current stream from the pipeline.  This is accomplished
    // by draining the stream with discard and indicating that a
    // discontinuity will occur
    if (ioctl(id(), VIDEO_CLEAR_BUFFER, NULL) != 0) {
        OMX_DBGT_ERROR("VIDEO_CLEAR_BUFFER ioctl failed (%s)", strerror (errno));
        OMX_DBGT_EPILOG();
        return -1;
    }

    this->state(this->state());
    OMX_DBGT_EPILOG();
    return 0;
}
