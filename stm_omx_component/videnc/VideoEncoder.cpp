/*
 * Copyright 2011 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *
 * File          ::    VideoEncoder.cpp
 * Author ::   Jean-Philippe FASSINO(jean-philippe.fassino@st.com)
 *
 *
 */

#include <linux/videodev2.h>
#include <IDvbDevice.hpp>
#include "VideoEncoder.hpp"
#include "StmOmxVendorExt.hpp"
#include "OMX_debug.h"


#include <linux/dvb/stm_dvb.h>
#include <linux/dvb/stm_video.h>

#if HAVE_ASSERT_H
#include <assert.h>
#endif

#if HAVE_DBGTASSERT_H
// Re-direct assert on DBGT_ASSERT macro
#define assert(expr)  OMX_DBGT_ASSERT(expr)
#endif

#define DBGT_DECLARE_AUTOVAR
#define DBGT_PREFIX "VENC"
#define DBGT_LAYER 0
#include <linux/dbgt.h>


namespace stm {

//==========================================
// VENCComponent implementation
//==========================================

static const char kRoleDefault[] = "default";
static const char kRoleAVC[] = "video_encoder.avc";

VENCComponent::VENCComponent(OMX_HANDLETYPE hComponent):
    OmxComponent("OMX.STM.Video.Encoder", hComponent, OMX_ST_VIDEO_PORT_NUMBER),
    IsConfigureDone(false),
    m_mutexCmd(),
    m_condConfig(),
    mPrependSPSPPS(OMX_FALSE),
    m_isForceIntraEnabled(OMX_FALSE),
    m_inputPort(OMX_ST_VIDEO_INPUT_PORT, OMX_DirInput, *this),
    m_outputPort(OMX_ST_VIDEO_OUTPUT_PORT, OMX_DirOutput, *this),
    m_componentRole(),
    m_HVAHEncoder_Params(NULL),
    m_nbBufferMapping(NULL),
    isMetaDataEnabled(OMX_FALSE)
{
    DBGT_TRACE_INIT(venc); //expands debug.venc.trace
}

VENCComponent::~VENCComponent()
{
}

//==========================================================
// VENCComponent::flush
// flush OMX port at the end of playback when a seek occurs
//==========================================================
OMX_ERRORTYPE VENCComponent::flush(unsigned int nPortIdx)
{
    OMX_DBGT_PROLOG();
    OMX_DBGT_PDEBUG("VENCComponent::flush on port %d  %s", nPortIdx, name());
    int result = 1;


    OMX_ERRORTYPE eError = this->getPort(nPortIdx).flush();
    OMX_DBGT_EPILOG();
    return eError;
}

//==========================================================
// VENCComponent::getParameter
// allows to return current parameters to player
//==========================================================
OMX_ERRORTYPE VENCComponent::getParameter(OMX_INDEXTYPE       nParamIndex,
                                          OMX_PTR             pParamStruct)
{
    OMX_DBGT_PROLOG("%s getParameter 0x%08x", name(), nParamIndex);
    OMX_DBGT_PROLOG();
    switch ((int)nParamIndex)
    {
    case OMX_IndexParamVideoInit: {
        ASSERT_STRUCT_TYPE(pParamStruct, OMX_PORT_PARAM_TYPE);
        OMX_PORT_PARAM_TYPE *param = (OMX_PORT_PARAM_TYPE*)pParamStruct;
        assert(param);
        param->nPorts = OMX_ST_VIDEO_PORT_NUMBER;
        param->nStartPortNumber = 0;
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }; break;

    case OMX_google_android_index_prependSPSPPSToIDRFrames: {
        ASSERT_STRUCT_TYPE(pParamStruct, OMX_PrependSPSPPSToIDRFramesParams);
		OMX_PrependSPSPPSToIDRFramesParams *param = (OMX_PrependSPSPPSToIDRFramesParams *)pParamStruct;
		param->bEnable = mPrependSPSPPS;
	}; break;

    case OMX_google_android_index_storeMetaDataInBuffers: {
        ASSERT_STRUCT_TYPE(pParamStruct, StoreMetaDataInBuffersParams);
	    StoreMetaDataInBuffersParams *param = (StoreMetaDataInBuffersParams *)pParamStruct;
		param->bStoreMetaData = isMetaDataEnabled;
	 }; break;

    }

    // If the component does not support it, we fall back to generic logic.
    OMX_ERRORTYPE eError = OmxComponent::getParameter(nParamIndex,
                                                      pParamStruct);

    OMX_DBGT_EPILOG();
    return eError;
}

//==========================================================
// VENCComponent::setParameter
// allows player to set parameters associated to OMX component
//==========================================================
OMX_ERRORTYPE VENCComponent::setParameter(OMX_INDEXTYPE       nParamIndex,
                                          OMX_PTR             pParamStruct)
{
    OMX_DBGT_PROLOG("%s setParameter 0x%08x", name(), nParamIndex);
    OMX_ERRORTYPE eError = OMX_ErrorNone;

    switch ((int)nParamIndex)
    {
    case OMX_IndexParamStandardComponentRole: {
        ASSERT_STRUCT_TYPE(pParamStruct, OMX_PARAM_COMPONENTROLETYPE);
        OMX_PARAM_COMPONENTROLETYPE *param = (OMX_PARAM_COMPONENTROLETYPE*)pParamStruct;
        assert(param);
        eError = this->setRole((const char*)param->cRole);
        OMX_DBGT_EPILOG();
        return eError;
    }; break;

    case OMX_google_android_index_prependSPSPPSToIDRFrames: {
        ASSERT_STRUCT_TYPE(pParamStruct, OMX_PrependSPSPPSToIDRFramesParams);
		OMX_PrependSPSPPSToIDRFramesParams *param = (OMX_PrependSPSPPSToIDRFramesParams *)pParamStruct;
		mPrependSPSPPS = param->bEnable;
        if (mPrependSPSPPS) {
            OMX_DBGT_PTRACE("Setting color format to opaque");
            this->inputPort_().colorFormat(OMX_COLOR_FormatAndroidOpaque);
        }
        OMX_DBGT_EPILOG();
        return eError;
	 }; break;

    case OMX_google_android_index_storeMetaDataInBuffers: {
	    StoreMetaDataInBuffersParams *param = (StoreMetaDataInBuffersParams *)pParamStruct;
		isMetaDataEnabled = param->bStoreMetaData;
        if (isMetaDataEnabled) {
            OMX_DBGT_PTRACE("Setting correct buffer size");
#ifdef ANDROID
            this->inputPort_().bufferSize(sizeof(buffer_handle_t)+4); //4 bytes more for buffer type field
#else
            this->inputPort_().bufferSize(sizeof(void *)+4); //4 bytes more for buffer type field
#endif
        }
        OMX_DBGT_EPILOG();
        return eError;
	 }; break;

    default:
        OMX_DBGT_PDEBUG("VENCComponent::get undef parameter (0x%08x)",
                    (unsigned int)nParamIndex);
        break;
    }

    // If the component does not support it, we fall back to generic logic.
    eError = OmxComponent::setParameter(nParamIndex, pParamStruct);

    OMX_DBGT_EPILOG();
    return eError;
}

OMX_ERRORTYPE
VENCComponent::getConfig(OMX_INDEXTYPE nIndex,
                          OMX_PTR pConfStruct)
{
    OMX_DBGT_PROLOG();
    switch ((int)nIndex)
    {
        case OMX_StmDvbVideoDeviceInfo: {
            ASSERT_STRUCT_TYPE(pConfStruct, OMX_GetStmLinuxDvbDeviceInfoParams);
            OMX_GetStmLinuxDvbDeviceInfoParams *param = (OMX_GetStmLinuxDvbDeviceInfoParams*)pConfStruct;
            assert(param);
            //param->ppDevice = (stm::IDvbDevice**)&m_dvb;
            OMX_DBGT_EPILOG();
            return OMX_ErrorNone;
        }; break;

        case OMX_IndexConfigVideoIntraVOPRefresh: {
            ASSERT_STRUCT_TYPE(pConfStruct, OMX_CONFIG_INTRAREFRESHVOPTYPE);
		    OMX_CONFIG_INTRAREFRESHVOPTYPE *param = (OMX_CONFIG_INTRAREFRESHVOPTYPE *)pConfStruct;
		    param->IntraRefreshVOP = m_isForceIntraEnabled;
            OMX_DBGT_EPILOG();
            return OMX_ErrorNone;
	    }; break;
    }

    // If the component does not support it, we fall back to generic logic.
    OMX_ERRORTYPE eError = OmxComponent::getConfig(nIndex,
                                                   pConfStruct);
    OMX_DBGT_EPILOG();
    return eError;
}

OMX_ERRORTYPE
VENCComponent::setConfig(OMX_INDEXTYPE nIndex,
                          OMX_PTR pConfStruct)
{
    OMX_DBGT_PROLOG();
    switch ((int)nIndex)
    {
        case OMX_IndexConfigVideoIntraVOPRefresh: {
            ASSERT_STRUCT_TYPE(pConfStruct, OMX_CONFIG_INTRAREFRESHVOPTYPE);
		    OMX_CONFIG_INTRAREFRESHVOPTYPE *param = (OMX_CONFIG_INTRAREFRESHVOPTYPE *)pConfStruct;
		    m_isForceIntraEnabled = param->IntraRefreshVOP;
            OMX_DBGT_EPILOG();
            return OMX_ErrorNone;
	    }; break;
    }

    // If the component does not support it, we fall back to generic logic.
    OMX_ERRORTYPE eError = OmxComponent::getConfig(nIndex,
                                                   pConfStruct);
    OMX_DBGT_EPILOG();
    return eError;
}


//==========================================================
// VENCComponent::getExtensionIndex
// returns extension index
//==========================================================
OMX_ERRORTYPE VENCComponent::getExtensionIndex(
        OMX_STRING          cParameterName,
        OMX_INDEXTYPE *     pIndexType)
{
    OMX_DBGT_PROLOG();
    OMX_ERRORTYPE eError;
    if (strcmp(cParameterName,
               OMX_StmDvbVideoDeviceInfoExt) == 0)
    {
        *pIndexType = (OMX_INDEXTYPE)OMX_StmDvbVideoDeviceInfo;
        eError = OMX_ErrorNone;
    }
    else if (strcmp(cParameterName,
             OMX_StmIndexprependSPSPPSToIDRFramesExt) == 0)
   {
        //This is call used by JB WiFi to force an I-frame
        *pIndexType = (OMX_INDEXTYPE)OMX_google_android_index_prependSPSPPSToIDRFrames;
        eError = OMX_ErrorNone;
    }
    else if (strcmp(cParameterName,
             OMX_StmIndexstoreMetaDataInBuffersExt) == 0)
    {
        *pIndexType = (OMX_INDEXTYPE)OMX_google_android_index_storeMetaDataInBuffers;
         eError = OMX_ErrorNone;
    }
    else
    {
        eError = OmxComponent::getExtensionIndex(cParameterName,pIndexType);
    }

    OMX_DBGT_EPILOG();
    return eError;
}

OMX_ERRORTYPE VENCComponent::emptyThisBuffer(OMX_BUFFERHEADERTYPE* pBufferHdr)
{
    // Reset EOS flag
    this->outputPort_().resetAfterEosFlag();
    OMXdebug_flagAfterEos = 0;

    return OmxComponent::emptyThisBuffer(pBufferHdr);
}

OMX_ERRORTYPE VENCComponent::fillThisBuffer(OMX_BUFFERHEADERTYPE* pBufferHdr)
{
    OMX_DBGT_PROLOG();
    OMX_ERRORTYPE eError = OmxComponent::fillThisBuffer(pBufferHdr);
    OMX_DBGT_EPILOG();
    return eError;
}

//==========================================================
// VENCComponent::destroy
// Allows free resources associated to the OMX component
//==========================================================
void VENCComponent::destroy()
{
    OMX_ERRORTYPE eError;
    OMX_DBGT_PROLOG();

    // stop, cleanup and delete video ports
    m_inputPort.destroy();
    m_outputPort.destroy();

    free(m_HVAHEncoder_Params);
    m_HVAHEncoder_Params = NULL;

    free(m_nbBufferMapping);
    m_nbBufferMapping = NULL;

    OmxComponent::destroy();

    IsConfigureDone = false;
    OMX_DBGT_EPILOG();
}

//==========================================================
// VENCComponent::create
// Allows create resources associated to a new OMX component
//==========================================================
OMX_ERRORTYPE VENCComponent::create()
{
    OMX_ERRORTYPE eError = OMX_ErrorNone;

    OMX_DBGT_PROLOG();

    if ((eError = OmxComponent::create()) != OMX_ErrorNone) {
        OMX_DBGT_ERROR();
        OMX_DBGT_EPILOG();
        return eError;
    }

    // Initialize encoder roles
    this->role().add(kRoleAVC);

    // Initialize the video parameters for input port
    if ((eError = m_inputPort.create()) != OMX_ErrorNone) {
        OMX_DBGT_ERROR("Error when creating video encoder input port");
        OMX_DBGT_EPILOG();
        return eError;
    }

    // Initialize the video parameters for output port
    if ((eError = m_outputPort.create()) != OMX_ErrorNone) {
        OMX_DBGT_ERROR("Error when creating video encoder output port");
        OMX_DBGT_EPILOG();
        return eError;
    }

    // Set default role: currently AVC
    this->setRole(kRoleDefault);

    OMXdebug_flagAfterEos    = 0;

    m_HVAHEncoder_Params = (HVAHEncoder_t *)malloc(sizeof(HVAHEncoder_t));
    if (m_HVAHEncoder_Params == NULL)
    {
        OMX_DBGT_CRITICAL("Not enough memory to allocate memory for m_HVAHEncoder_Params.... KILL");
        return OMX_ErrorInsufficientResources;
    }

    memset((void *)m_HVAHEncoder_Params,0,sizeof(HVAHEncoder_t));

    m_nbBufferMapping = (EncoderBufferMapping *)calloc(MAX_INPUT_OUTPUT_BUFFERS,sizeof(EncoderBufferMapping));
    if (m_nbBufferMapping == NULL)
    {
        OMX_DBGT_CRITICAL("Not enough memory to allocate memory for m_nbBufferMapping.... KILL");
        return OMX_ErrorInsufficientResources;
    }

    for (int i=0;i<MAX_INPUT_OUTPUT_BUFFERS;i++)
    {
        memset((void *)&m_nbBufferMapping[i],0,sizeof(EncoderBufferMapping));
    }

    OMX_DBGT_PTRACE("m_HVAHEncoder_Params : 0x%x m_nbBufferMapping : 0x%x",(unsigned int)m_HVAHEncoder_Params,(unsigned int)m_nbBufferMapping);

    m_inputPort.setInternalPointers(&m_HVAHEncoder_Params,&m_nbBufferMapping);
    m_outputPort.setInternalPointers(&m_HVAHEncoder_Params,&m_nbBufferMapping);


    OMX_DBGT_EPILOG();
    return OMX_ErrorNone;
}

OMX_ERRORTYPE VENCComponent::setRole(const char* pNewRole)
{
    int nRoleIdx = -1;
    if (strcmp(pNewRole, kRoleDefault) == 0) {
        nRoleIdx = 0;
        OMX_DBGT_PDEBUG("Setting role to default %s", this->role().at(nRoleIdx));
    } else {
        // check if proposed role is supported
        for (unsigned int i = 0; i < this->role().size(); i++) {
            if (strcmp(pNewRole, this->role().at(i)) == 0) {
                nRoleIdx = i;
                break;
            }
        }
    }

    if (nRoleIdx == -1) {
        OMX_DBGT_ERROR("Unsupported role %s", pNewRole);
        return OMX_ErrorUnsupportedSetting;
    }

    return OMX_ErrorNone;
}

//=================================================================================
// VENCComponent::getWidth
// Returns Width of Video set at Input Port
//=================================================================================
unsigned int VENCComponent::getWidth(unsigned int nIdx) const
{
    OMX_DBGT_PROLOG();
    unsigned int width = this->getPort(nIdx).definition().frameWidth();
    OMX_DBGT_EPILOG("Width at video input port : %u",width);
    return width;
}

//=================================================================================
// VENCComponent::getHeight
// Returns Width of Video set at Input Port
//=================================================================================
unsigned int VENCComponent::getHeight(unsigned int nIdx) const
{
    OMX_DBGT_PROLOG();
    unsigned int height = this->getPort(nIdx).definition().frameHeight();
    OMX_DBGT_EPILOG("Height at video input port : %u",height);
    return height;
}

//=================================================================================
// VENCComponent::getCompressionFormat
// Returns Video Compression format set at Input Port
//=================================================================================
OMX_VIDEO_CODINGTYPE VENCComponent::getCompressionFormat(unsigned int nIdx) const
{
    OMX_DBGT_PROLOG();
    OMX_VIDEO_CODINGTYPE compressionFormat = this->getPort(nIdx).definition().compressionFormat();
    OMX_DBGT_EPILOG("Video Format set at input port : %u",(unsigned int)compressionFormat);
    return compressionFormat;
}

const VidEncPort& VENCComponent::getPort(unsigned int nIdx) const
{
    assert(nIdx < OMX_ST_VIDEO_PORT_NUMBER);
    if (nIdx == 0)
        return m_inputPort;
    else
        return m_outputPort;
}

VidEncPort& VENCComponent::getPort(unsigned int nIdx)
{
    assert(nIdx < OMX_ST_VIDEO_PORT_NUMBER);
    if (nIdx == 0)
        return m_inputPort;
    else
        return m_outputPort;
}

//==========================================================
// VENCComponent::configure
//  encoder configuration according to codec
//==========================================================
OMX_ERRORTYPE VENCComponent::configure()
{
    OMX_DBGT_PROLOG();


    IDvbDevice * m_dvb;
    OMX_DBGT_PTRACE("Abt creating input port dvb device");
    m_dvb = new DVBVideo_SF(0x0, this->name(),OMX_ST_VIDEO_INPUT_PORT);
    OMX_DBGT_PTRACE("After creating input port dvb device");
    if (!m_dvb)
    {
        OMX_DBGT_ERROR("Can not allocate new Video device for Input Port");
        OMX_DBGT_EPILOG();
        return OMX_ErrorInsufficientResources;
    }

    this->inputPort_().setDvbDevice(&m_dvb);
    OMX_DBGT_PTRACE("Abt creating output port dvb device");
    m_dvb = new DVBVideo_SF(0x0, this->name(),OMX_ST_VIDEO_OUTPUT_PORT);
    OMX_DBGT_PTRACE("After creating output port dvb device");
    if (!m_dvb) {
        OMX_DBGT_ERROR("Can not allocate new Video device for Output Port");
        OMX_DBGT_EPILOG();
        return OMX_ErrorInsufficientResources;
    }

    this->outputPort_().setDvbDevice(&m_dvb);//&m_HVAHEncoder_Params,&m_nbBufferMapping);

    OMX_DBGT_PINFO("Device name for input : %s output : %s",this->inputPort_().dvb().name(),this->outputPort_().dvb().name());

    m_mutexCmd.lock();
    IsConfigureDone = true;
    m_mutexCmd.release();
    m_condConfig.signal();
    OMX_DBGT_EPILOG();
    return OMX_ErrorNone;
}

//==========================================================
// VENCComponent::start
//  start encoding
//==========================================================
OMX_ERRORTYPE VENCComponent::start()
{
    OMX_DBGT_PROLOG();

/*    if (dvb().isStarted()) {
        OMX_DBGT_ERROR("::dvb.Start %s failed (already started)", name());
        OMX_DBGT_EPILOG();
        return OMX_ErrorIncorrectStateTransition;
    }

    if(dvb().start() != 0)
    {
        OMX_DBGT_ERROR("::dvb.Start %s failed (%s)", name(), strerror (errno));
        OMX_DBGT_EPILOG();
        return OMX_ErrorInsufficientResources;
    }
*/
    // Create the port threads
    if (this->inputPort_().start() != 0) {
        return OMX_ErrorInsufficientResources;
    }
    if (this->outputPort_().start() != 0) {
        return OMX_ErrorInsufficientResources;
    }

    OMX_DBGT_EPILOG();

    return OMX_ErrorNone;
}

//==========================================================
// VENCComponent::stop
//  stop encoding
//==========================================================
void VENCComponent::stop()
{
    OMX_DBGT_PROLOG();

    if (this->inputPort_().stop() != 0) {
        OMX_DBGT_ERROR("%s::stop failed for input port", name());
    }
    if (this->outputPort_().stop() != 0) {
        OMX_DBGT_ERROR("%s::stop failed for output port", name());
    }

    OMX_DBGT_EPILOG();

/*
    if (!this->dvb().isStarted()) {
        // Nothing to do
        OMX_DBGT_EPILOG();
        return;
    }

    int result = this->dvb().clear();
    if (result) {
       OMX_DBGT_ERROR("%s::clear failed", name());
       OMX_DBGT_EPILOG();
       return;
    }

    if (this->dvb().stop() != 0)
        OMX_DBGT_ERROR("::dvb.Stop %s failed (%s)", name(), strerror (errno));
*/
    OMX_DBGT_EPILOG();
}


//==============================================================================
// VENCComponent::execute
//  OMX command already processed by CmdThread: nothing to do
//==============================================================================
void VENCComponent::execute()
{}

} // eof namespace stm

//==========================================================
// OMX_ComponentInit
// "C" interface to initialize a new OMX component: creation of the associated object
//==========================================================
extern "C" OMX_ERRORTYPE OMX_ComponentInit (OMX_HANDLETYPE hComponent)
{
    return stm::OmxComponent::ComponentInit(new stm::VENCComponent(hComponent));
}

