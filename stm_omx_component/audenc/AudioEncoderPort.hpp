/*
 * Copyright 2013 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file   AudioEncoderPort.hpp
 * @author STMicroelectronics
 */

#ifndef _STM_AUDIOENCODERPORT_HPP_
#define _STM_AUDIOENCODERPORT_HPP_

#include "StmOmxPort.hpp"
#include "StmOmxComponent.hpp"
#include "AudioPortDefinition.hpp"

#include "Mutex.hpp"
#include <pthread.h>
#include "audencoder.hpp"


#define SET_PARAM_HEADER(param,port)    \
    sizeof(param),                          \
    {{0,0,0,0}},                            \
    port
#define SET_PORT(_s_, index) \
    (_s_)->nPortIndex = index;

namespace stm {

// Forward declaration
class AudEncPort;
class AudEncOutputPort;
class AudEncInputPort;
class AENCComponent;

class AudEncPort: public OmxPort
{
public:
    /** Constructor */
    AudEncPort(unsigned int nIdx, OMX_DIRTYPE aDir,
               const AENCComponent& owner);

    /** Destructor */
    virtual ~AudEncPort();

    virtual const OmxComponent& component() const;
    virtual const AudioPortDefinition& definition() const;

protected:
    virtual AudioPortDefinition& definition();

    const AENCComponent& AudEnc() const;

private:
    const AENCComponent&         m_owner;
    AudioPortDefinition          m_PortDefinition;
};

inline const AENCComponent& AudEncPort::AudEnc() const
{ return m_owner; }


//==========================================================================
// AudEncOutputPort declaration
//==========================================================================
class AudEncOutputPort: public AudEncPort
{
public:

    /** Constructor */
    AudEncOutputPort(unsigned int nIdx, OMX_DIRTYPE aDir,
                     const AENCComponent& owner);

    /** Destructor */
    virtual ~AudEncOutputPort();

    virtual OMX_ERRORTYPE create();
    virtual void destroy();
    int start();
    int stop();

public:
    virtual OMX_ERRORTYPE
    getParameter(OMX_INDEXTYPE nIndex, OMX_PTR pParamStruct);

    virtual OMX_ERRORTYPE
    setParameter(OMX_INDEXTYPE nIndex, OMX_PTR pParamStruct);

    virtual OMX_ERRORTYPE
    useBuffer(OMX_BUFFERHEADERTYPE** ppBufferHdr,
              OMX_PTR pAppPrivate,
              OMX_U32 nSizeBytes,
              OMX_U8* pBuffer);

    virtual OMX_ERRORTYPE
    allocateBuffer(OMX_BUFFERHEADERTYPE** ppBufferHdr,
                   OMX_PTR pAppPrivate,
                   OMX_U32 nSizeBytes);

    virtual OMX_ERRORTYPE
    freeBuffer(OMX_BUFFERHEADERTYPE* pBufferHdr);

    virtual OMX_ERRORTYPE
    emptyThisBuffer(OMX_BUFFERHEADERTYPE* pBuffer);

    virtual OMX_ERRORTYPE
    fillThisBuffer(OMX_BUFFERHEADERTYPE* pBuffer);

    virtual OMX_ERRORTYPE flush();

    OMX_AUDIO_CODINGTYPE
    supportedCodingFormat(unsigned int nIdx) const;

    unsigned int
    supportedCodingFormatNb() const;

    bool isSupportedCodingFormat(OMX_AUDIO_CODINGTYPE aFmt) const;

    /** Change current port compression format in definition */
    void codingFormat(OMX_AUDIO_CODINGTYPE aFmt);

    unsigned int receivedFrame() const;

    bool  isAfterEosFlag() const;
    void resetAfterEosFlag();
    IDvbDevice& dvb();
    OMX_ERRORTYPE setDvbDevice(IDvbDevice **m_dvb);
    void setInternalPointers(AUDEncoderParams_t **params, EncoderBufferMapping **buffermapping);

    void setDefaultParameters();
    int disconnectAssociation(void *Inputbuffer);

    OMX_ERRORTYPE updateEncParams(OMX_AUDIO_PARAM_AACPROFILETYPE *aParamAudioaac);

    unsigned int channelNb() const;

protected:
    /**
     * This function serves as the entry point to the thread.
     * @param pthis the instance
     */
    static void* ThreadEntryPoint(void* pThis);

    /** The thread main function loop */
    void threadFunction();

    int findAssociation(void *Inputbuffer, int type, void **retBuffer);

private:
	static OMX_BUFFERHEADERTYPE* EOSBUF;
    OMX_ERRORTYPE doPortExtraProcessing(OMX_BUFFERHEADERTYPE**  ppBufferHdr,OMX_U32 nSizeBytes);
    uint32_t getV4lSamplingFrequency(uint32_t samplingFrequency);

    IDvbDevice*               m_dvb;

    /** Thread Id of the port thread */
    pthread_t puller_thread_id;

    // Count number of Frame, integer is enough since it allow
    // to play more than 1 year !!!
    unsigned int m_nReceivedFrame;

    bool     m_bAfterEosFlag;

    AUDEncoderParams_t *mPortAUDEncoderParams;
    EncoderBufferMapping *mapping;
    OMX_BOOL m_outputEncoderStarted;
    static const OMX_AUDIO_CODINGTYPE  m_codingFormats[];
    OMX_BOOL m_isAllocateBuffer;
    OMX_AUDIO_PARAM_AACPROFILETYPE m_aacParams;
};

inline unsigned int AudEncOutputPort::receivedFrame() const
{ return m_nReceivedFrame; }

inline void AudEncOutputPort::resetAfterEosFlag()
{ m_bAfterEosFlag = false; }

inline bool AudEncOutputPort::isAfterEosFlag() const
{ return m_bAfterEosFlag; }

inline IDvbDevice& AudEncOutputPort::dvb()
{ return *m_dvb; }

//==========================================================================
// AudEncInputPort declaration
//==========================================================================

class AudEncInputPort: public AudEncPort
{
public:
    /** Constructor */
    AudEncInputPort(unsigned int nIdx, OMX_DIRTYPE aDir,
                    const AENCComponent& owner);

    /** Destructor */
    virtual ~AudEncInputPort();

    virtual OMX_ERRORTYPE create();
    virtual void destroy();
    int start();
    int stop();
public:
    virtual OMX_ERRORTYPE
    getParameter(OMX_INDEXTYPE nIndex, OMX_PTR pParamStruct);

    virtual OMX_ERRORTYPE
    setParameter(OMX_INDEXTYPE nIndex, OMX_PTR pParamStruct);

    virtual OMX_ERRORTYPE
    useBuffer(OMX_BUFFERHEADERTYPE** ppBufferHdr,
              OMX_PTR pAppPrivate,
              OMX_U32 nSizeBytes,
              OMX_U8* pBuffer);

    virtual OMX_ERRORTYPE
    allocateBuffer(OMX_BUFFERHEADERTYPE** ppBufferHdr,
                   OMX_PTR pAppPrivate,
                   OMX_U32 nSizeBytes);

    virtual OMX_ERRORTYPE
    freeBuffer(OMX_BUFFERHEADERTYPE* pBufferHdr);

    virtual OMX_ERRORTYPE
    emptyThisBuffer(OMX_BUFFERHEADERTYPE* pBuffer);

    virtual OMX_ERRORTYPE
    fillThisBuffer(OMX_BUFFERHEADERTYPE* pBuffer);

    virtual OMX_ERRORTYPE flush();
    virtual OMX_ERRORTYPE pause();
    virtual OMX_ERRORTYPE resume();


    /** Change current port compression format in definition */
    void codingFormat(OMX_AUDIO_CODINGTYPE aFmt);

    bool isSupportedCodingFormat(OMX_AUDIO_CODINGTYPE aFmt) const;

    unsigned int sendedFrame() const;

    const OMX_AUDIO_PARAM_PCMMODETYPE& pcmParams() const;

    IDvbDevice& dvb();
    OMX_ERRORTYPE setDvbDevice(IDvbDevice **m_dvb);
    void setInternalPointers(AUDEncoderParams_t **params, EncoderBufferMapping **buffermapping);
    void bufferSize(unsigned int buff_size);

    int disconnectAssociation(void *Inputbuffer);

    OMX_ERRORTYPE doPortExtraProcessing(OMX_BUFFERHEADERTYPE**  ppBufferHdr,OMX_U32 nSizeBytes);

protected:
    /**
     * This function serves as the entry point to the thread.
     * @param pthis the instance
     */
    static void* ThreadEntryPoint(void* pThis);

    /** The thread main function loop */
    void threadFunction();

    int findAssociation(void *Inputbuffer, int type, void **retBuffer);

private:
    typedef enum  {
        EThreadState_Stopped  = 0,
        EThreadState_Running,
        EThreadState_Paused,
    } EThreadState;

    EThreadState threadState() const;
    void threadState(EThreadState st);


    int encodeFrame(unsigned int nOmxFlags,
                    uint8_t* pBuf, unsigned int nLen,
                    uint64_t nTimeStamp);
    uint32_t getV4lSampleFormat(OMX_AUDIO_PARAM_PCMMODETYPE* pcmParam);

private:
    static OMX_BUFFERHEADERTYPE* EOSBUF;
    // Thread Id of the port thread
    pthread_t pusher_thread_id;
    EThreadState    m_nThreadState;
    Mutex           m_mutexThread;
    Condition       m_condState;

    unsigned int    m_nSentFrame;
    IDvbDevice*               m_dvb;

    OMX_AUDIO_PARAM_PCMMODETYPE m_pcmParams;

    AUDEncoderParams_t *mPortAUDEncoderParams;
    EncoderBufferMapping *mapping;
    OMX_BOOL m_inputEncoderStarted;
    OMX_BOOL m_bAfterEosFlag;
    OMX_BOOL m_isAllocateBuffer;
};

inline unsigned int AudEncInputPort::sendedFrame() const
{ return m_nSentFrame; }

inline IDvbDevice& AudEncInputPort::dvb()
{ return *m_dvb; }

inline const OMX_AUDIO_PARAM_PCMMODETYPE& AudEncInputPort::pcmParams() const
{ return m_pcmParams; }


} // eof namespace stm

#endif  // _STM_AUDIOENCODERPORT_HPP_
