/*
 * Copyright 2011 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *
 * File   ::    v4laudioenc.hpp
 * Author ::    Kausik Maiti (kausik.maiti@st.com)
 *
 *
 */


#ifndef ST_OMX_V4L_AUDIO_ENCODE_H
#define ST_OMX_V4L_AUDIO_ENCODE_H
#include "IDvbDevice.hpp"
#include "Mutex.hpp"
#include <stdio.h>
#include <string.h>
#include <linux/videodev2.h>
#include <linux/dvb/video.h>
#include <linux/dvb/dvb_v4l2_export.h>
#include <audencoder.hpp>

#define V4L_AUDIO_LOGGING_NAME                "COM.ST.AUDIO.ENC.V4L[%d]"
#define V4L_AUDIO_LOGGING_NAME_LENGTH         sizeof(V4L_AUDIO_LOGGING_NAME)+5

class V4LAudioEncode :public stm::IDvbDevice{
private:
    void state(stm::IDvbDevice::EState st) const;

    int32_t v4l_fd;
    char m_deviceName[V4L_AUDIO_LOGGING_NAME_LENGTH];
    //unique device name is not available till 'open'
    bool mIsLoggingEnabled;
    int32_t device_id;
    int32_t EncoderPort;
    struct v4l2_fmtdesc *formats;
    int32_t nbFormats;
    AUDEncoderParams_t *encoderParams;
    OMX_AUDIO_CODINGTYPE m_nEncoding;
    mutable stm::IDvbDevice::EState m_nState;
    /** Condition variable signaled when state has changed */
    mutable stm::Condition   m_condState;

public:
    virtual int id() const;
    virtual const char* name() const;

    V4LAudioEncode(const char* aDeviceName, const char *name, int aEncoderPort);
    virtual ~V4LAudioEncode();

    virtual int create(){return -1;};
    virtual int destroy();
    virtual bool isOpen() const;

    virtual int create(const char *const driver, const char *const card,int flags);
    int ConfigureEncoder();
    void configureEncoderStructure(void **encoderParam, int EncoderType);
    int setControls();
    int setupBufferCount();
    int setParam();
    int setUpBufferPointer();
    int freeBufferPointer();
    int QueueInitialBuffer(struct buffer_t *ptr);
    int DequeueEncoderBuffer(void* BufferData);
    int SpecificationCheck();
    int QueryFormat();
    int Open (int grabIdx);
    int Configure();
    virtual int start() const;
    virtual int stop() const;
    virtual int pause(bool hw) const;
    virtual int resume(bool hw) const;
    virtual void lock() const;
    virtual void unlock() const;
    virtual void resetflags() const;
    int QueueBuffer(void* BufferData, int &nBytesSize, uint64_t &ts) const;
    char * V4lToString(int32_t v);

    virtual int UseBuffer(unsigned long virtualAddr, unsigned long bufferSize,
                  int width, int height, int color,
                  unsigned long userId) const{return -1;};
    virtual int FillThisBuffer(unsigned long userId) const {return -1;};
    virtual int FillBufferDone(unsigned long & userId, uint64_t & TS, bool & eos) const {return -1;};
    /** Return the current media time */
    virtual uint64_t getTime() const {return 0;};
    virtual uint32_t getSamplingRate() const {return 0;};
    /** Writes data to this device. */
    virtual int write(const void* data_ptr, unsigned int len) const {return -1;};

    virtual int flush(bool bEoS = false) const;
    virtual int clear() const;

    virtual stm::IDvbDevice::EState state() const;

    virtual unsigned long FlushBuffer() const;

};

#endif
