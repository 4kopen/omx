LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)

LOCAL_SRC_FILES:= \
	v4laudioenc.cpp              \
	AudioEncoderPort.cpp         \
	AudioEncoder.cpp

# Includes for ICS
LOCAL_C_INCLUDES+= \
	$(TOP)/frameworks/base/include/media/stagefright/openmax

# Includes for JB
LOCAL_C_INCLUDES+= \
	$(TOP)/frameworks/native/include/media/openmax

# Common includes
LOCAL_C_INCLUDES+= \
	$(TOP)/vendor/stm/module/omxse \
	$(TOP)/vendor/stm/hardware/omx/include/ \
	$(TOP)/vendor/stm/hardware/omx/stm_omx_component/common/ \
	$(TOP)/vendor/stm/hardware/omx/stm_omx_component/videnc/ \
	$(TOP)/vendor/stm/hardware/omx/stm_omx_component/vid/ \
	$(TOP)/vendor/stm/hardware/header/usr/include \
	$(TOP)/vendor/stm/hardware/header/usr/include/linux \
	$(TOP)/vendor/stm/hardware/header/usr/include/linux/dvb \
	$(TOP)/vendor/stm/module/stlinuxtv/linux/include \
	$(TOP)/vendor/stm/module/stlinuxtv/linux/include/linux/dvb

LOCAL_SHARED_LIBRARIES := \
        libdl \
        libcutils \
        liblog \
        libhardware \
        libSTMOMXCommon


LOCAL_CPPFLAGS += -DANDROID
LOCAL_CFLAGS   += -DDBGT_CONFIG_DEBUG -DDBGT_CONFIG_AUTOVAR -DDBGT_TAG=\"AENC\" -Werror
LOCAL_CFLAGS   += -Wno-error=switch
LOCAL_CFLAGS   += -Wno-unused-parameter
LOCAL_CFLAGS   += -DHAVE_DBGTASSERT_H
LOCAL_MODULE:= libOMX.STM.Audio.Encoder
LOCAL_MODULE_TAGS := optional

include $(BUILD_SHARED_LIBRARY)
