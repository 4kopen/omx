/*
 * Copyright 2013 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file   AudioEncoderPort.cpp
 * @author Kausik Maiti(kausik.maiti@st.com)
 */

#include <linux/dvb/stm_dvb.h>
#include <linux/dvb/stm_video.h>
#include <linux/videodev2.h>
#include <linux/dvb/dvb_v4l2_export.h>
#include <IDvbDevice.hpp>
#include "AudioEncoderPort.hpp"
#include "AudioEncoder.hpp"
#include "OMX_debug.h"
#include <stdio.h>

#if HAVE_ASSERT_H
#include <assert.h>
#endif

#if HAVE_DBGTASSERT_H
// Re-direct assert on DBGT_ASSERT macro
#define assert(expr)  OMX_DBGT_ASSERT(expr)
#endif

#define DBGT_PREFIX "AENCP"
#define DBGT_LAYER 0
#include <linux/dbgt.h>

//#define AUD_ENCODE_INPUTBUFFER_DUMP
//#define AUD_ENCODE_OUTPUTBUFFER_DUMP

#define V4L2_ENCODER_CARD_NAME "STMicroelectronics"
#define V4L2_ENCODER_DRIVER_NAME "Encoder"

namespace stm {

//==========================================================================
// AudEncPort::AudEncPort
// port constructor of OMX component
//==========================================================================

AudEncPort::AudEncPort(unsigned int nIdx, OMX_DIRTYPE aDir,
                       const AENCComponent& owner)
    :OmxPort(),
     m_owner(owner),
     m_PortDefinition(nIdx, aDir)
{
    //check the string length before copy
    assert(strlen(this->AudEnc().name()) <= sizeof(m_strName));
    strcpy(m_strName,this->AudEnc().name());
    DBGT_TRACE_INIT(aenc);
    OMXDEBUG_UPDATE_AV_TRACE_SETTINGS(this->AudEnc().name());
}

AudEncPort::~AudEncPort()
{}

const AudioPortDefinition& AudEncPort::definition() const
{ return m_PortDefinition; }

AudioPortDefinition& AudEncPort::definition()
{ return m_PortDefinition; }

const OmxComponent& AudEncPort::component() const
{ return m_owner; }

//==========================================================================
// AudEncOutputPort implementation
//==========================================================================

const OMX_AUDIO_CODINGTYPE  AudEncOutputPort::m_codingFormats[] = {
    OMX_AUDIO_CodingAAC
};

OMX_BUFFERHEADERTYPE* AudEncOutputPort::EOSBUF =
    (OMX_BUFFERHEADERTYPE*)0xFFFFFFFF;

AudEncOutputPort::AudEncOutputPort(unsigned int nIdx, OMX_DIRTYPE aDir,
                                 const AENCComponent& owner)
    :AudEncPort(nIdx, aDir, owner),
     m_dvb(0),
     puller_thread_id(0),
     m_nReceivedFrame(0),
     m_bAfterEosFlag(false),
     mPortAUDEncoderParams(0),
     mapping(0),
     m_outputEncoderStarted(OMX_FALSE),
     m_isAllocateBuffer(OMX_FALSE)
{
    this->definition().bufferCountMin(NUM_OUT_BUFFERS);
    this->definition().bufferCountActual(NUM_OUT_BUFFERS);
    this->definition().bufferSize(64*1024);
    //this->definition().mimeType("audio/aac");
    this->codingFormat(OMX_AUDIO_CodingAAC);

    OMX_CONF_INIT_STRUCT_PTR(&m_aacParams,    OMX_AUDIO_PARAM_AACPROFILETYPE );
    SET_PORT(&m_aacParams, nIdx);

    OMX_DBGT_PINFO("%s Default color format is %s "
               "(STOMXComponentAsync::create)",
               component().name(),
               this->definition().contentType());
}

AudEncOutputPort::~AudEncOutputPort()
{
    if (m_dvb) {
        delete m_dvb;
        m_dvb = 0;
    }
}

OMX_AUDIO_CODINGTYPE
AudEncOutputPort::supportedCodingFormat(unsigned int nIdx) const
{
    assert(nIdx < this->supportedCodingFormatNb());
    return m_codingFormats[nIdx];
}

unsigned int
AudEncOutputPort::supportedCodingFormatNb() const
{ return (sizeof(m_codingFormats) / sizeof(m_codingFormats[0])); }

bool AudEncOutputPort::isSupportedCodingFormat(OMX_AUDIO_CODINGTYPE aFmt) const
{
    bool ret = false;
    for (unsigned int i = 0; i < this->supportedCodingFormatNb(); i++) {
        if (this->supportedCodingFormat(i) == aFmt) {
            ret = true;
            break;
        }
    }
    return ret;
}

unsigned int
AudEncOutputPort::channelNb() const
{
    switch((int)this->definition().compressionFormat())
    {
    case OMX_AUDIO_CodingAAC:
        return m_aacParams.nChannels; break;
    default:
        return 0;
    }
}

void AudEncOutputPort::setInternalPointers(AUDEncoderParams_t **params, EncoderBufferMapping **buffermapping)
{
    OMX_DBGT_PROLOG();
    mPortAUDEncoderParams = *params;
    mapping = *buffermapping;
    OMX_DBGT_PTRACE("m_HVAHEncoder_Params : 0x%x m_nbBufferMapping : 0x%x",(unsigned int)mPortAUDEncoderParams,(unsigned int)mapping);
    setDefaultParameters();
    OMX_DBGT_EPILOG();
}


OMX_ERRORTYPE 
AudEncOutputPort::setDvbDevice(IDvbDevice **temp_m_dvb)
{
    int retVal = -1;
    OMX_DBGT_PROLOG();
    m_dvb = *temp_m_dvb;

    retVal = static_cast<V4LAudioEncode *>(m_dvb)->create(V4L2_ENCODER_DRIVER_NAME, V4L2_ENCODER_CARD_NAME,0);
    if (retVal != 0)
    {
        OMX_DBGT_ERROR("Error create, return value %d",retVal);
        OMX_DBGT_EPILOG();
        return OMX_ErrorHardware;
    }

    retVal = static_cast<V4LAudioEncode*>(m_dvb)->SpecificationCheck();
    if (retVal != 0)
    {
        OMX_DBGT_ERROR("Error SpecificationCheck, return value %d",retVal);
        OMX_DBGT_EPILOG();
        return OMX_ErrorHardware;
    }

    retVal = static_cast<V4LAudioEncode*>(m_dvb)->ConfigureEncoder();
    if (retVal != 0)
    {
        OMX_DBGT_ERROR("Error ConfigureEncoder, return value %d",retVal);
        OMX_DBGT_EPILOG();
        return OMX_ErrorHardware;
    }
    OMX_DBGT_PINFO("compression format : %d", this->definition().compressionFormat());
    static_cast<V4LAudioEncode*>(m_dvb)->configureEncoderStructure((void **)&mPortAUDEncoderParams,this->definition().compressionFormat());

    retVal = static_cast<V4LAudioEncode*>(m_dvb)->QueryFormat();
    if (retVal != 0)
    {
        OMX_DBGT_ERROR("Error desired format NOT support for output, return value %d",retVal);
        OMX_DBGT_EPILOG();
        return OMX_ErrorHardware;
    }

	retVal = static_cast<V4LAudioEncode*>(m_dvb)->Configure();

    /* setting the buffer count values*/
    mPortAUDEncoderParams->num_output_buffers = this->definition().bufferCountActual();

    retVal = static_cast<V4LAudioEncode*>(m_dvb)->setParam();
    if (retVal != 0)
    {
        DBGT_ERROR("Error while setupBuffer to v4L for output, return value %d",retVal);
        OMX_DBGT_EPILOG();
        return OMX_ErrorHardware;
    }

    /* this API is to be called encoder output only */
    retVal = static_cast<V4LAudioEncode*>(m_dvb)->setControls();
    if (retVal != 0)
    {
        OMX_DBGT_ERROR("Error while sending controls to v4L for output, return value %d",retVal);
        OMX_DBGT_EPILOG();
        return OMX_ErrorHardware;
    }

    OMX_DBGT_EPILOG();
    return OMX_ErrorNone;
}

OMX_ERRORTYPE AudEncOutputPort::create()
{
    OMX_DBGT_PROLOG();
    OmxPort::create();
    OMX_DBGT_EPILOG();
    return OMX_ErrorNone;
}

int AudEncOutputPort::start()
{
    // Nothing to do on output port if tunneled
    if (this->isTunneled()) {
        puller_thread_id = 0;
    } else {
        if (!m_outputEncoderStarted)
        {
            OMX_DBGT_PINFO("Abt to call start dvb for output");
            if(static_cast<V4LAudioEncode*>(m_dvb)->start() != 0)
            {
                OMX_DBGT_ERROR("Encoder Output Port dvb Start failed (%s)", strerror (errno));
                return -1;
            }
            m_outputEncoderStarted = OMX_TRUE;
            OMX_DBGT_PINFO("Encoder output buffer started");
        }
        OMX_DBGT_CHECK_RETURN(! pthread_create(&puller_thread_id, NULL, AudEncOutputPort::ThreadEntryPoint, this), OMX_ErrorInsufficientResources);
#if ANDROID
        OMX_DBGT_CHECK_RETURN(! pthread_setname_np(puller_thread_id, "OMXAunEncPull"), OMX_ErrorInsufficientResources);
#endif
    }
    return 0;
}

int AudEncOutputPort::stop()
{
    OMX_DBGT_PROLOG();
    if (m_outputEncoderStarted)
    {
        OMX_DBGT_PINFO("Abt to call stop dvb for output");
        if(static_cast<V4LAudioEncode*>(m_dvb)->stop() != 0)
        {
            OMX_DBGT_ERROR("Encoder Output Port dvb Stop failed (%s)", strerror (errno));
            OMX_DBGT_EPILOG();
            return -1;
        }
        m_outputEncoderStarted = OMX_FALSE;
        OMX_DBGT_PINFO("Encoder output port stopped");
    }
    OMX_DBGT_EPILOG();
    return OMX_ErrorNone;
}

void AudEncOutputPort::destroy()
{
    OMX_DBGT_PROLOG();

    int retVal = -1;

    if (puller_thread_id != 0) {
        OMX_DBGT_PINFO(" AudEncOutputPort EOSBF sent");
        this->push(EOSBUF);
        pthread_join(puller_thread_id, 0);
    }

    retVal = static_cast<V4LAudioEncode*>(m_dvb)->destroy();
    if (retVal < 0) {
        OMX_DBGT_ERROR("DVB  device for input port not closed");
    }


    if(this->nbBuffers() > 0) {
        OMX_DBGT_ERROR("Allocated buffer on output port not freed");
    }
    OMX_DBGT_EPILOG();
}

void* AudEncOutputPort::ThreadEntryPoint(void* a_pThis)
{
    AudEncOutputPort* pThis = static_cast<AudEncOutputPort*>(a_pThis);
    pThis->threadFunction();
    return 0;
}

void AudEncOutputPort::codingFormat(OMX_AUDIO_CODINGTYPE aFmt)
{
    assert(isSupportedCodingFormat(aFmt));
    this->definition().compressionFormat(aFmt);
}

//==========================================================
// AudEncOutputPort::setDefaultParameters
//==========================================================
void AudEncOutputPort::setDefaultParameters()
{
    mPortAUDEncoderParams->inputBufferSize = 0;
    mPortAUDEncoderParams->outputBufferSize = 64 * 1024;
    mPortAUDEncoderParams->isOutputPortBufferAllocated = OMX_FALSE;
    mPortAUDEncoderParams->isInputPortBufferAllocated = OMX_FALSE;
    mPortAUDEncoderParams->input_buffers = NULL;
    mPortAUDEncoderParams->output_buffers = NULL;
}

//==========================================================
// AudEncOutputPort::updateAVCParams
// checks and updates AVC param values
//==========================================================
OMX_ERRORTYPE
AudEncOutputPort::updateEncParams(OMX_AUDIO_PARAM_AACPROFILETYPE *aParamAudioaac)
{
    OMX_DBGT_PROLOG();
    OMX_ERRORTYPE eError = OMX_ErrorNone;
   //TBD
EXIT:
    OMX_DBGT_EPILOG();
    return eError;
}

OMX_ERRORTYPE
AudEncOutputPort::getParameter(OMX_INDEXTYPE nParamIndex, OMX_PTR pParamStruct)
{
    OMX_DBGT_PROLOG("on Audio encoder output port");
    switch ((int)nParamIndex)
    {
        case OMX_IndexParamPortDefinition: {
            // Get port definition
            ASSERT_STRUCT_TYPE(pParamStruct, OMX_PARAM_PORTDEFINITIONTYPE);
            OMX_PARAM_PORTDEFINITIONTYPE* param;
            param = (OMX_PARAM_PORTDEFINITIONTYPE*)pParamStruct;
            assert(param->nPortIndex == this->definition().index());

            // Provide current content of port and computes nBufferSize on top of it
            OMX_DBGT_PDEBUG("GetParameter(OMX_IndexParamPortDefinition) nPortIndex: %u"
                            " eDir: %d nBufferCountActual: %u nBufferCountMin: %u"
                            " nBufferSize: %u\n",
                            this->definition().index(),
                            this->definition().direction(),
                            this->definition().bufferCountActual(),
                            this->definition().bufferCountMin(),
                            this->definition().bufferSize());
            if ( this->definition().bufferSize() == 0 )
            {
                this->definition().bufferSize( (64*1024) );
                OMX_DBGT_PINFO("%s set nBufferSize to %uKB "
                       "(GetParameter(OMX_IndexParamPortDefinition input))",
                       component().name(), param->nBufferSize/1024);
            }

            *param = this->definition().getParamPortDef();
            OMX_DBGT_EPILOG();
            return OMX_ErrorNone;
        }


		case OMX_IndexParamAudioAac: {
			ASSERT_STRUCT_TYPE(pParamStruct, OMX_AUDIO_PARAM_AACPROFILETYPE);
			OMX_AUDIO_PARAM_AACPROFILETYPE* param;
			param = (OMX_AUDIO_PARAM_AACPROFILETYPE*)pParamStruct;
			assert(param->nPortIndex == this->definition().index());
			*param = m_aacParams;
			OMX_DBGT_EPILOG();
			return OMX_ErrorNone;
		}; break;

		case OMX_IndexParamAudioPortFormat:{
		    // Supported audio port format
		    ASSERT_STRUCT_TYPE(pParamStruct, OMX_AUDIO_PARAM_PORTFORMATTYPE);
		    OMX_AUDIO_PARAM_PORTFORMATTYPE* param;
		    param = (OMX_AUDIO_PARAM_PORTFORMATTYPE*)pParamStruct;
		    assert(param->nPortIndex == this->definition().index());
		    if (param->nIndex < this->supportedCodingFormatNb()) {
		        param->eEncoding = this->supportedCodingFormat(param->nIndex);
		        OMX_DBGT_EPILOG();
		        return OMX_ErrorNone;
		    } else {
		        OMX_DBGT_EPILOG("No more supported format");
		        return OMX_ErrorNoMore;
		    }
        }

        case OMX_IndexParamOtherPortFormat:
        default: {
            OMX_DBGT_ERROR("Unsupported index 0x%x", nParamIndex);
            OMX_DBGT_EPILOG("Unsupported index");
            return OMX_ErrorUnsupportedIndex;
        }; break;
    }
}

uint32_t AudEncOutputPort::getV4lSamplingFrequency(uint32_t samplingFrequency)
{
    uint32_t samplingRate = V4L2_MPEG_AUDIO_STM_SAMPLING_FREQ_48000;
    switch(samplingFrequency)
    {
        case 4000 :samplingRate = V4L2_MPEG_AUDIO_STM_SAMPLING_FREQ_4000;break;
        case 5000 :samplingRate = V4L2_MPEG_AUDIO_STM_SAMPLING_FREQ_5000;break;
        case 6000 :samplingRate = V4L2_MPEG_AUDIO_STM_SAMPLING_FREQ_6000;break;
        case 8000 :samplingRate = V4L2_MPEG_AUDIO_STM_SAMPLING_FREQ_8000;break;
        case 11000:samplingRate = V4L2_MPEG_AUDIO_STM_SAMPLING_FREQ_11025;break;
        case 12000:samplingRate = V4L2_MPEG_AUDIO_STM_SAMPLING_FREQ_12000;break;
        case 16000:samplingRate = V4L2_MPEG_AUDIO_STM_SAMPLING_FREQ_16000;break;
        case 22000:samplingRate = V4L2_MPEG_AUDIO_STM_SAMPLING_FREQ_22050;break;
        case 24000:samplingRate = V4L2_MPEG_AUDIO_STM_SAMPLING_FREQ_24000;break;
        case 32000:samplingRate = V4L2_MPEG_AUDIO_STM_SAMPLING_FREQ_32000;break;
        case 48000:default:samplingRate = V4L2_MPEG_AUDIO_STM_SAMPLING_FREQ_48000;break;
    }
    return samplingRate;
}

OMX_ERRORTYPE
AudEncOutputPort::setParameter(OMX_INDEXTYPE nParamIndex, OMX_PTR pParamStruct)
{
    OMX_DBGT_PROLOG("on Audio encoder ouptut port");
    switch ((int)nParamIndex)
    {
    case OMX_IndexParamAudioPortFormat: {
        // Set audio port format
        ASSERT_STRUCT_TYPE(pParamStruct, OMX_AUDIO_PARAM_PORTFORMATTYPE);
        OMX_AUDIO_PARAM_PORTFORMATTYPE* param;
        param = (OMX_AUDIO_PARAM_PORTFORMATTYPE*)pParamStruct;
        assert(param->nPortIndex == this->definition().index());

        if (!isSupportedCodingFormat(param->eEncoding)) {
            // Unsupported coding format
            OMX_DBGT_ERROR("%s eEncoding not supported (%d)",
            component().name(), param->eEncoding);
            OMX_DBGT_EPILOG("Unsupported audio compression format");
            return OMX_ErrorUnsupportedSetting;
        }

        // Apply format on port
        this->codingFormat(param->eEncoding);
        OMX_DBGT_PINFO("%s coding format changed to %s (eEncoding=%d(0x%x))",
        component().name(),
        this->definition().contentType(),
        this->definition().compressionFormat(),
        this->definition().compressionFormat());

        OMX_DBGT_EPILOG("Set port coding format %u", param->eEncoding);
        return OMX_ErrorNone;
        }; break;

    case OMX_IndexParamAudioAac: {
        //uint32_t samplingRate = V4L2_MPEG_AUDIO_STM_SAMPLING_FREQ_48000;
        ASSERT_STRUCT_TYPE(pParamStruct, OMX_AUDIO_PARAM_AACPROFILETYPE);
        OMX_AUDIO_PARAM_AACPROFILETYPE* param;
        param = (OMX_AUDIO_PARAM_AACPROFILETYPE*)pParamStruct;
        assert(param->nPortIndex == this->definition().index());
        m_aacParams = *param;
        //capture the mPortAUDEncoderParams
        mPortAUDEncoderParams->codec        = V4L2_MPEG_AUDIO_STM_ENCODING_AAC;
        mPortAUDEncoderParams->max_channels = mPortAUDEncoderParams->src_max_channels = 8;// should this be m_aacParams.nChannels
        mPortAUDEncoderParams->bitrate_mode = 0;
        mPortAUDEncoderParams->bitrate      = m_aacParams.nBitRate;
        mPortAUDEncoderParams->vbr_qfactor  = 100;
        mPortAUDEncoderParams->bitrate_cap  = 0;

        mPortAUDEncoderParams->sampling_freq = getV4lSamplingFrequency(m_aacParams.nSampleRate);
        mPortAUDEncoderParams->channel_count = m_aacParams.nChannels;
        mPortAUDEncoderParams->prog_level = 0;
        memset(&mPortAUDEncoderParams->dst_metadata, 0, sizeof(mPortAUDEncoderParams->dst_metadata));
        OMX_DBGT_PINFO("AAC freq : %d, channel_count : %d", (int)mPortAUDEncoderParams->sampling_freq, (int)mPortAUDEncoderParams->channel_count);
        OMX_DBGT_PINFO("AAC bitrate : %d", (int)m_aacParams.nBitRate);

        // channel 0..31
        for(uint32_t i=0; i<V4L2_STM_AUDENC_MAX_CHANNELS; i++)
        {
            if(i< m_aacParams.nChannels)
                mPortAUDEncoderParams->channel[i] = i;
            else
                mPortAUDEncoderParams->channel[i] = 254;
        }

        this->codingFormat(OMX_AUDIO_CodingAAC);
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
        }; break;

    case OMX_IndexParamPortDefinition: {
        // Port definition
        ASSERT_STRUCT_TYPE(pParamStruct, OMX_PARAM_PORTDEFINITIONTYPE);
        OMX_PARAM_PORTDEFINITIONTYPE* param;
        param = (OMX_PARAM_PORTDEFINITIONTYPE*)pParamStruct;
        assert(param->nPortIndex == this->definition().index());

        if (this->definition().bufferSize() != param->nBufferSize) {
            DBGT_PINFO("%s changing nBufferSize from %uKB to %uKB "
            "(SetParameter(OMX_IndexParamPortDefinition output))",
            component().name(),
            this->definition().bufferSize()/1024,
            param->nBufferSize/1024);
        }
        mPortAUDEncoderParams->outputBufferSize = param->nBufferSize;
        this->definition().setParamPortDef(param);
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
        }; break;

        default: {
            OMX_DBGT_ERROR("Unsupported index");
            OMX_DBGT_EPILOG("Unsupported index");
            return OMX_ErrorUnsupportedIndex;
        }; break;
    }
}

OMX_ERRORTYPE
AudEncOutputPort::doPortExtraProcessing(OMX_BUFFERHEADERTYPE**  ppBufferHdr,OMX_U32 nSizeBytes)
{
    int retVal = -1;
    int i;
    unsigned int bufferCount;
    OMX_ERRORTYPE eError = OMX_ErrorNone;
    OMX_DBGT_PROLOG();

    // we will allways use mmpapped buffers as we do not know if the buffers are contiguous
    mPortAUDEncoderParams->isOutputPortBufferAllocated = OMX_TRUE;
    OMX_DBGT_PTRACE(" OutputPort m_isAllocateBuffer : %d", m_isAllocateBuffer);

    if (!mPortAUDEncoderParams->output_buf_allocated)
    {
         retVal = static_cast<V4LAudioEncode*>(m_dvb)->setupBufferCount();

         if (retVal != 0)
         {
            OMX_DBGT_ERROR("Error while setupBuffer to v4L for output, return value %d",retVal);
            return OMX_ErrorHardware;
         }

         if (mPortAUDEncoderParams->isOutputPortBufferAllocated)
         {
             /* getting the input buffers and associating them to omx buffers */
             retVal = static_cast<V4LAudioEncode*>(m_dvb)->setUpBufferPointer();
             if (retVal != 0)
             {
                 OMX_DBGT_ERROR("Error while setUpBufferPointer to v4L for output, return value %d",retVal);
                 return OMX_ErrorHardware;
             }
         }
         mPortAUDEncoderParams->output_buf_allocated = true;
    }

    for (i=0;i<MAX_INPUT_OUTPUT_BUFFERS;i++)
    {
        if (mapping[i].isAssociated == false)
            break;
    }

    if (i == MAX_INPUT_OUTPUT_BUFFERS)
    {
        OMX_DBGT_CRITICAL("No free buffer available for use - input");
        return OMX_ErrorInsufficientResources;
    }
    for (bufferCount = 0;bufferCount<mPortAUDEncoderParams->num_output_buffers ; bufferCount++)
    {
        if (mPortAUDEncoderParams->output_buffers[bufferCount].isUsed == 0)
            break;
    }

    // forming an assoication between input buffer and encoder internally allocated buffer
    mapping[i].isAssociated = (OMX_BOOL)true;
    mPortAUDEncoderParams->output_buffers[bufferCount].isUsed = (OMX_BOOL)true;
    mapping[i].buffProperty = 1;
    mapping[i].omxBuffer = *ppBufferHdr;
    mapping[i].internalBuffer =&(mPortAUDEncoderParams->output_buffers[bufferCount]);

    if (m_isAllocateBuffer)
    {
        (*ppBufferHdr)->pBuffer = (OMX_U8 *)mPortAUDEncoderParams->output_buffers[bufferCount].start;
    }
    OMX_DBGT_PINFO("Input buffer count : %d virtual address 0x%x omx buffer : 0x%x location : %d",bufferCount,(unsigned int)mPortAUDEncoderParams->output_buffers[bufferCount].start,(unsigned int)*ppBufferHdr,i);

    OMX_DBGT_EPILOG();
    return eError;
}

OMX_ERRORTYPE
AudEncOutputPort::useBuffer(OMX_BUFFERHEADERTYPE** ppBufferHdr,
                            OMX_PTR pAppPrivate,
                            OMX_U32 nSizeBytes,
                            OMX_U8* pBuffer)
{
    OMX_ERRORTYPE eError = OMX_ErrorNone;
    OMX_DBGT_PROLOG("Using buffer %p on audio encoder ouput port", pBuffer);

    // Invalid case in proprietary tunneling
    assert(!this->isTunneled());

    // Allocate a buffer header
    *ppBufferHdr = 0;
    OMX_BUFFERHEADERTYPE* pBufferHdr = allocateBufferHeader(pAppPrivate,
                                                            nSizeBytes);
    if (!pBufferHdr) {
        eError = OMX_ErrorInsufficientResources;
        OMX_DBGT_ERROR("Can not allocate buffer header");
        OMX_DBGT_EPILOG("Return error %s", OMX_TYPE_TO_STR(OMX_ERRORTYPE, eError));
        return eError;
    }

    pBufferHdr->pBuffer = pBuffer;
    *ppBufferHdr = pBufferHdr;
    (*ppBufferHdr)->pPlatformPrivate = 0;

    // Increment internal buffer counter.
    this->incrementNbBuffers();

    if (AudEnc().IsConfigureDone == false) {
        OMX_DBGT_PINFO("Blocking allocateBuffer before venc configure");
        AudEnc().m_condConfig.wait(AudEnc().m_mutexCmd);
        OMX_DBGT_PINFO("Unblocking allocateBuffer");
    }

    eError = doPortExtraProcessing(ppBufferHdr,nSizeBytes);
    OMX_DBGT_EPILOG();
    return eError;
}

OMX_ERRORTYPE
AudEncOutputPort::allocateBuffer(OMX_BUFFERHEADERTYPE** ppBufferHdr,
                                 OMX_PTR pAppPrivate,
                                 OMX_U32 nSizeBytes)
{
    // Allows to allocate and to return a frame buffer to the player
    OMX_ERRORTYPE eError = OMX_ErrorNone;

    OMX_DBGT_PROLOG("Allocating buffer on audio encoder ouput port");

    m_isAllocateBuffer = OMX_TRUE;

    // Invalid case in proprietary tunneling
    assert(!this->isTunneled());

    // Allocate a buffer header
    *ppBufferHdr = 0;
    OMX_BUFFERHEADERTYPE* pBufferHdr = allocateBufferHeader(pAppPrivate,
                                                            nSizeBytes);
    if (!pBufferHdr) {
        eError = OMX_ErrorInsufficientResources;
        OMX_DBGT_ERROR("Can not allocate buffer header");
        OMX_DBGT_EPILOG("Return error %s", OMX_TYPE_TO_STR(OMX_ERRORTYPE, eError));
        return eError;
    }

    *ppBufferHdr = pBufferHdr;

    if (AudEnc().IsConfigureDone == false) {
        OMX_DBGT_PINFO("Blocking allocateBuffer before venc configure");
        AudEnc().m_condConfig.wait(AudEnc().m_mutexCmd);
        OMX_DBGT_PINFO("Unblocking allocateBuffer");
    }

    // Increment internal buffer counter.
    this->incrementNbBuffers();
    eError = doPortExtraProcessing(ppBufferHdr,nSizeBytes);

    OMX_DBGT_EPILOG();
    return eError;
}

OMX_ERRORTYPE
AudEncOutputPort::freeBuffer(OMX_BUFFERHEADERTYPE* pBufferHdr)
{
    OMX_DBGT_PROLOG();
    assert(!isTunneled());
    int retVal = -1;
    retVal = static_cast<V4LAudioEncode*>(m_dvb)->freeBufferPointer();
    if (retVal < 0)
    {
            OMX_DBGT_ERROR("Encoder Output Port v4l unmap failed (%s)", strerror (errno));
    }

    freeBufferHeader(pBufferHdr);
    this->decrementNbBuffers();

    OMX_DBGT_EPILOG();
    return OMX_ErrorNone;
}

OMX_ERRORTYPE
AudEncOutputPort::emptyThisBuffer(OMX_BUFFERHEADERTYPE* pBufferHdr)
{
    assert(0 && "Invalid call on output port");
    return OMX_ErrorNone;
}

OMX_ERRORTYPE
AudEncOutputPort::fillThisBuffer(OMX_BUFFERHEADERTYPE* pBufferHdr)
{
    assert(this->isTunneled() == false);

     // Called by player to send an empty buffer to be filled with a
     // encoded audio frame
     // This buffer is send to omxse
     OMX_DBGT_PROLOG();
     if (isAfterEosFlag())
     {
         // The last encoded audio frame has already been returned to the player.
         // The buffer is send back empty to the player
         pBufferHdr->nFlags |= OMX_BUFFERFLAG_EOS;
         pBufferHdr->nTimeStamp = 0;
         pBufferHdr->nFilledLen = 0;
         this->AudEnc().notifyFillBufferDone(pBufferHdr);
     }
     else
     {
        this->push(pBufferHdr);
     }
     OMX_DBGT_EPILOG();
    return OMX_ErrorNone;
}

int AudEncOutputPort::disconnectAssociation(void *Inputbuffer)
{
    int i;
    int retVal = -1;
    for (i=0;i<MAX_INPUT_OUTPUT_BUFFERS;i++)
    {
        if ((OMX_BUFFERHEADERTYPE *)Inputbuffer == mapping[i].omxBuffer)
        {
            mapping[i].isAssociated = (OMX_BOOL)false;
            retVal = 0;
            OMX_DBGT_PTRACE("Buffer : 0x%x at location : %d disassociated",(unsigned int)Inputbuffer,i);
            break;
        }
    }
    return retVal;
}


OMX_ERRORTYPE AudEncOutputPort::flush()
{
    OMX_DBGT_PROLOG("Flush audio encoder output port");
    if (isTunneled()) {
        // Proprietary tunneling. Nothing to flush
        OMX_DBGT_EPILOG("Port is tunneled.");
        return OMX_ErrorNone;
    }

    this->getRawFifos()->lock();

     OMX_BUFFERHEADERTYPE *buf  = 0;
     while(this->getRawFifos()->tryPop(buf) == true) {
         buf->nFilledLen = 0;
         this->AudEnc().notifyFillBufferDone(buf);
     }
     this->getRawFifos()->unlock();
     buf  = 0;
     buf = this->relockCurrent();
     if(buf != NULL)
     {
         this->AudEnc().notifyFillBufferDone(buf);
     }
     this->freeAndUnlockCurrent();

    OMX_DBGT_EPILOG();
    return OMX_ErrorNone;
}

//=============================================================================
//  Output port core function of Pulling thread
//  Infinite loop: Wait for encoded frame from SE and send them to Player
//=============================================================================
void AudEncOutputPort::threadFunction()
{
    OMX_DBGT_PROLOG();
    struct buffer_t *internalBufferPtr;
    int retVal = -1;

    while(true)
    {
        if (m_bAfterEosFlag)
        {
            goto EXIT;
        }
        else
        {
            // Wait for a 'FillThisBuffer' from player
            OMX_BUFFERHEADERTYPE *pOutBufHdr = this->popAndLockCurrent();
            if (pOutBufHdr && pOutBufHdr != this->EOSBUF)
            {
                OMX_DBGT_PINFO("Output buffer : 0x%x actual 0x%x length : %d timestamp : 0x%x flags : %d",
                    (unsigned int)pOutBufHdr,
                    (unsigned int)pOutBufHdr->pBuffer,
                    (int)pOutBufHdr->nFilledLen,
                    (int)pOutBufHdr->nTimeStamp,
                    (int)pOutBufHdr->nFlags);
                retVal = findAssociation((void *)pOutBufHdr, 0,(void **)&internalBufferPtr);
                if (retVal < 0)
                {
                    OMX_DBGT_CRITICAL("OMX to Internal Buffer Association NOT found");
                    goto EXIT;
                }
                OMX_DBGT_PINFO("Status of internal buffer isUsed (should be false) : %d",(int)internalBufferPtr->isUsed);
                OMX_DBGT_PINFO("To check v4l2 type : %d, memory : %d index : %d start addr 0x%x len : %d",
                    (int)internalBufferPtr->v4l2_buf.type,
                    (int)internalBufferPtr->v4l2_buf.memory,
                    (int)internalBufferPtr->v4l2_buf.index,
                    (unsigned int)internalBufferPtr->start,
                    (int)internalBufferPtr->length);
                uint64_t temp=0;
                if(static_cast<V4LAudioEncode*>(m_dvb)->QueueBuffer((void *)internalBufferPtr,(int &)(pOutBufHdr->nFilledLen),temp) != 0)
                {
                    OMX_DBGT_ERROR("Encoder Output Port initial QBUF failed (%s)", strerror (errno));
                    this->AudEnc().notifyFillBufferDone(pOutBufHdr);
                    internalBufferPtr->isUsed = false;
                    this->freeAndUnlockCurrent();
                    goto EXIT;
                }
                m_nReceivedFrame++;
                OMX_DBGT_PINFO("OutputPort : Status of internal buffer isUsed (should be TRUE) : %d, counter : %d",
                    (int)internalBufferPtr->isUsed,
                    m_nReceivedFrame);

                //Now dequeue the buffer
                retVal = static_cast<V4LAudioEncode*>(m_dvb)->DequeueEncoderBuffer(internalBufferPtr);
                if (retVal < 0)
                {
                    OMX_DBGT_ERROR("!!FATAL!! Error while DequeueBuffer for output buffer");
                    this->AudEnc().notifyFillBufferDone(pOutBufHdr);
                    internalBufferPtr->isUsed = false;
                    this->freeAndUnlockCurrent();
                    goto EXIT;
                }
                pOutBufHdr->nTimeStamp = (internalBufferPtr->v4l2_buf.timestamp.tv_sec * 1000000) +
                                            internalBufferPtr->v4l2_buf.timestamp.tv_usec;
                pOutBufHdr->nFilledLen = internalBufferPtr->v4l2_buf.bytesused;
                pOutBufHdr->nOffset = 0;
#ifdef AUD_ENCODE_OUTPUTBUFFER_DUMP
		        {
		            FILE *fd;
		            fd = fopen("/data/encode_output.aac","ab+");
		            if (!fd) {
		                OMX_DBGT_ERROR(": Unable to open %s file.Data Dump discarded","encode_output.aac");
		            }
		            else{
		                fwrite(internalBufferPtr->start,1, pOutBufHdr->nFilledLen,fd);
		                fclose(fd);
		            }
		        }
#endif
                if ((mPortAUDEncoderParams->isOutputPortBufferAllocated) && (!m_isAllocateBuffer))
                {
                    OMX_DBGT_PINFO("Performing memcpy for output buffer");
                    memcpy(pOutBufHdr->pBuffer,internalBufferPtr->start,pOutBufHdr->nFilledLen);
                }

                pOutBufHdr->nFlags |= OMX_BUFFERFLAG_ENDOFFRAME;
                OMX_DBGT_PINFO("Output buffer filled len : %d timestamp : %d",
                    (int)pOutBufHdr->nFilledLen,
                    (int)pOutBufHdr->nTimeStamp);
                //ZERO length marks the last frame
                if (pOutBufHdr->nFilledLen == 0)
                {
                    pOutBufHdr->nFlags |= OMX_BUFFERFLAG_EOS;
                    m_bAfterEosFlag = OMX_TRUE;
                    OMX_DBGT_PINFO("Abt to send event buffer flag");
                    this->AudEnc().notifyEventHandler(OMX_EventBufferFlag, OMX_ST_AUDIO_OUTPUT_PORT, OMX_BUFFERFLAG_EOS, NULL);
                }

                if (!mPortAUDEncoderParams->isOutputPortBufferAllocated)
                {
                    disconnectAssociation((void *)pOutBufHdr);
                }
                internalBufferPtr->isUsed = false;

                this->AudEnc().notifyFillBufferDone(pOutBufHdr);
                internalBufferPtr->isUsed = false;
                OMX_DBGT_PINFO("Output buffer rel : 0x%x and isUsed : %d",(unsigned int)pOutBufHdr,(int)internalBufferPtr->isUsed);
                this->freeAndUnlockCurrent();
            }
            else
            {
                OMX_DBGT_PINFO("EOSBUF detected on PullerRunner");
                this->freeAndUnlockCurrent();
                goto EXIT;
            }
        }
    }
EXIT:
    flush();
    OMX_DBGT_PINFO("Puller thread exit (AENCComponent::PullerRunner)");
    OMX_DBGT_EPILOG();
}

int
AudEncOutputPort::findAssociation(void *Inputbuffer, int type, void **retBuffer)
{
    int i;
    int retVal = -1;
    OMX_DBGT_PROLOG();
    for (i=0;i<MAX_INPUT_OUTPUT_BUFFERS;i++)
    {
        if (type)
        {
            if (Inputbuffer == mapping[i].internalBuffer)
            {
                *retBuffer = (void *)mapping[i].omxBuffer;
                retVal = 0;
                break;
            }
        }
        else
        {
            if ((OMX_BUFFERHEADERTYPE *)Inputbuffer == mapping[i].omxBuffer)
            {
                *retBuffer = (void *)mapping[i].internalBuffer;
                retVal = 0;
                break;
            }
        }
    }
    OMX_DBGT_PDEBUG("Inputbuffer : 0x%x type : %d retBuffer : 0x%x retVal : %d",
        (unsigned int)Inputbuffer,type,
        (unsigned int)*retBuffer,retVal);
    OMX_DBGT_EPILOG();
    return retVal;
}

//==========================================================================
// AudEncInputPort implementation
//==========================================================================

OMX_BUFFERHEADERTYPE* AudEncInputPort::EOSBUF =
    (OMX_BUFFERHEADERTYPE*)0xFFFFFFFF;

AudEncInputPort::AudEncInputPort(unsigned int nIdx, OMX_DIRTYPE aDir,
                                 const AENCComponent& owner)
    :AudEncPort(nIdx, aDir, owner),
     pusher_thread_id(0),
     m_nThreadState(EThreadState_Stopped),
     m_mutexThread(),
     m_condState(),
     m_nSentFrame(0),
     mPortAUDEncoderParams(0),
     mapping(0),
     m_inputEncoderStarted(OMX_FALSE),
     m_bAfterEosFlag(OMX_FALSE),
     m_isAllocateBuffer(OMX_FALSE)
{
    this->definition().bufferCountMin(NUM_IN_BUFFERS);
    this->definition().bufferCountActual(NUM_IN_BUFFERS);
    //this->definition().mimeType("audio/pcm");
    this->definition().bufferSize(4*1024);
    this->codingFormat(OMX_AUDIO_CodingPCM);
}

AudEncInputPort::~AudEncInputPort()
{
    if (m_dvb) {
        delete m_dvb;
        m_dvb = 0;
    }
}

int
AudEncInputPort::findAssociation(void *Inputbuffer, int type, void **retBuffer)
{
    int i;
    int retVal = -1;
    OMX_DBGT_PROLOG();
    for (i=0;i<MAX_INPUT_OUTPUT_BUFFERS;i++)
    {
        if (type)
        {
            if (Inputbuffer == mapping[i].internalBuffer)
            {
                *retBuffer = (void *)mapping[i].omxBuffer;
                retVal = 0;
                break;
            }
        }
        else
        {
            if ((OMX_BUFFERHEADERTYPE *)Inputbuffer == mapping[i].omxBuffer)
            {
                *retBuffer = (void *)mapping[i].internalBuffer;
                retVal = 0;
                break;
            }
        }
    }
    OMX_DBGT_PDEBUG("Inputbuffer : 0x%x type : %d retBuffer : 0x%x retVal : %d",
        (unsigned int)Inputbuffer,type,
        (unsigned int)*retBuffer,retVal);
    OMX_DBGT_EPILOG();
    return retVal;
}

int AudEncInputPort::disconnectAssociation(void *Inputbuffer)
{
    int i;
    int retVal = -1;
    for (i=0;i<MAX_INPUT_OUTPUT_BUFFERS;i++)
    {
        if ((OMX_BUFFERHEADERTYPE *)Inputbuffer == mapping[i].omxBuffer)
        {
            mapping[i].isAssociated = (OMX_BOOL)false;
            retVal = 0;
            OMX_DBGT_PTRACE("Buffer : 0x%x at location : %d disassociated",(unsigned int)Inputbuffer,i);
            break;
        }
    }
    return retVal;
}


void
AudEncInputPort::bufferSize(unsigned int buff_size)
{
    assert(buff_size);
    this->definition().bufferSize(buff_size);
}

AudEncInputPort::EThreadState AudEncInputPort::threadState() const
{ return m_nThreadState; }

void AudEncInputPort::threadState(EThreadState st)
{
    m_mutexThread.lock();
    m_nThreadState = st;
    m_mutexThread.release();
    m_condState.signal();
}

void AudEncInputPort::setInternalPointers(AUDEncoderParams_t **params, EncoderBufferMapping **buffermapping)
{
    OMX_DBGT_PROLOG();
    mPortAUDEncoderParams = *params;
    mapping = *buffermapping;
    OMX_DBGT_PTRACE("m_HVAHEncoder_Params : 0x%x m_nbBufferMapping : 0x%x",(unsigned int)mPortAUDEncoderParams,(unsigned int)mapping);
    OMX_DBGT_EPILOG();
}

OMX_ERRORTYPE
AudEncInputPort::setDvbDevice(IDvbDevice **temp_m_dvb)
{
    uint32_t pixelformat;
    int retVal = -1;
    OMX_DBGT_PROLOG();
    m_dvb = *temp_m_dvb;

    retVal = static_cast<V4LAudioEncode*>(m_dvb)->create(V4L2_ENCODER_DRIVER_NAME, V4L2_ENCODER_CARD_NAME,0);
    if (retVal != 0)
    {
        OMX_DBGT_ERROR("Error create, return value %d",retVal);
        return OMX_ErrorHardware;
    }

    static_cast<V4LAudioEncode*>(m_dvb)->SpecificationCheck();
    retVal = static_cast<V4LAudioEncode*>(m_dvb)->ConfigureEncoder();
    if (retVal != 0)
    {
        OMX_DBGT_ERROR("Error ConfigureEncoder, return value %d",retVal);
        return OMX_ErrorHardware;
    }

    static_cast<V4LAudioEncode*>(m_dvb)->configureEncoderStructure((void **)&mPortAUDEncoderParams,this->definition().compressionFormat());

    retVal = static_cast<V4LAudioEncode*>(m_dvb)->QueryFormat();
    if (retVal != 0)
    {
        OMX_DBGT_ERROR("Error desired format NOT support for input, return value %d",retVal);
        return OMX_ErrorHardware;
    }

    retVal = static_cast<V4LAudioEncode*>(m_dvb)->Configure();

    /* setting the buffer count values*/
    mPortAUDEncoderParams->num_input_buffers = this->definition().bufferCountActual();

    retVal = static_cast<V4LAudioEncode*>(m_dvb)->setParam();
    if (retVal != 0)
    {
        OMX_DBGT_ERROR("Error while setupBuffer to v4L for input, return value %d",retVal);
        return OMX_ErrorHardware;
    }

    return OMX_ErrorNone;
    OMX_DBGT_EPILOG();
}

OMX_ERRORTYPE AudEncInputPort::create()
{
    OMX_DBGT_PROLOG();
    static const unsigned int kDefaultFreqHz = 44100;
    // Set the default parameters
    OMX_CONF_INIT_STRUCT_PTR(&m_pcmParams, OMX_AUDIO_PARAM_PCMMODETYPE );
    m_pcmParams.eNumData      = OMX_NumericalDataSigned;
    m_pcmParams.nBitPerSample = 16;
    m_pcmParams.nChannels     = 2;
    m_pcmParams.ePCMMode      = OMX_AUDIO_PCMModeLinear;
    m_pcmParams.nSamplingRate = kDefaultFreqHz;
    m_pcmParams.bInterleaved  = OMX_TRUE;
    m_pcmParams.nPortIndex    = OMX_ST_AUDIO_INPUT_PORT;

    OmxPort::create();
    OMX_DBGT_EPILOG();

    return OMX_ErrorNone;
}

int AudEncInputPort::start()
{
    OMX_DBGT_PROLOG();

	if (!m_inputEncoderStarted)
    {
        OMX_DBGT_PINFO("Abt to call start dvb for output");
        if(static_cast<V4LAudioEncode*>(m_dvb)->start() != 0)
        {
            OMX_DBGT_ERROR("Encoder Input Port dvb Start failed (%s)", strerror (errno));
            return -1;
        }
        m_inputEncoderStarted = OMX_TRUE;
        OMX_DBGT_PINFO("Encoder Input buffer started");
    }

    OMX_DBGT_CHECK_RETURN(! pthread_create(&pusher_thread_id, NULL, AudEncInputPort::ThreadEntryPoint, this), OMX_ErrorInsufficientResources);
#ifdef ANDROID
    OMX_DBGT_CHECK_RETURN(! pthread_setname_np(pusher_thread_id, "OMXAudEncPush"), OMX_ErrorInsufficientResources);
#endif
    this->threadState(EThreadState_Running);
    OMX_DBGT_EPILOG();
    return OMX_ErrorNone;
}

int AudEncInputPort::stop()
{
    OMX_DBGT_PROLOG();

	if (m_inputEncoderStarted)
    {
        OMX_DBGT_PINFO("Abt to call stop dvb for Input");
        if(static_cast<V4LAudioEncode*>(m_dvb)->stop() != 0)
        {
            OMX_DBGT_ERROR("Encoder Input Port dvb Stop failed (%s)", strerror (errno));
            OMX_DBGT_EPILOG();
            return -1;
        }
        m_inputEncoderStarted = OMX_FALSE;
        OMX_DBGT_PINFO("Encoder Input port stopped");
    }

    OMX_DBGT_EPILOG();
    return OMX_ErrorNone;
}

void AudEncInputPort::destroy()
{
    OMX_DBGT_PROLOG(" AudEncInputPort ");
    int retVal = -1;

    if (pusher_thread_id != 0) {
        // Resume if was previously paused
        this->resume();
        OMX_DBGT_PINFO(" AudEncInputPort EOSBF sent");
        this->push(EOSBUF);
        pthread_join(pusher_thread_id, 0);
    }

    retVal = static_cast<V4LAudioEncode*>(m_dvb)->destroy();
    if (retVal < 0) {
        OMX_DBGT_ERROR("DVB  device for input port not closed");
    }

    if (this->nbBuffers() > 0) {
        OMX_DBGT_ERROR("Allocated buffer on input port not freed");
    }
    OMX_DBGT_EPILOG();
}

void* AudEncInputPort::ThreadEntryPoint(void* a_pThis)
{
    AudEncInputPort* pThis = static_cast<AudEncInputPort*>(a_pThis);
    pThis->threadFunction();
    return 0;
}



OMX_ERRORTYPE
AudEncInputPort::getParameter(OMX_INDEXTYPE nParamIndex, OMX_PTR pParamStruct)
{
    OMX_DBGT_PROLOG("on Audio encoder input port");
    switch ((int)nParamIndex)
    {
    case OMX_IndexParamPortDefinition: {
        // Port definition
        ASSERT_STRUCT_TYPE(pParamStruct, OMX_PARAM_PORTDEFINITIONTYPE);
        OMX_PARAM_PORTDEFINITIONTYPE* param;
        param = (OMX_PARAM_PORTDEFINITIONTYPE*)pParamStruct;
        assert(param->nPortIndex == this->definition().index());

        // Provide current content of sPorts and computes nBufferSize on top of it
        OMX_DBGT_PDEBUG("GetParameter(OMX_IndexParamPortDefinition) nPortIndex: "
                    "%u eDir: %d nBufferCountActual: %u nBufferCountMin: %u nBufferSize: %u\n",
                    this->definition().index(),
                    this->definition().direction(),
                    this->definition().bufferCountActual(),
                    this->definition().bufferCountMin(),
                    this->definition().bufferSize());

        *param = this->definition().getParamPortDef();
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }; break;

   case OMX_IndexParamAudioPcm: {
        ASSERT_STRUCT_TYPE(pParamStruct, OMX_AUDIO_PARAM_PCMMODETYPE);
        OMX_AUDIO_PARAM_PCMMODETYPE* param;
        param = (OMX_AUDIO_PARAM_PCMMODETYPE*)pParamStruct;
        assert(param->nPortIndex == this->definition().index());
        *param = m_pcmParams;
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }; break;

    case OMX_IndexParamAudioPortFormat:{
        OMX_DBGT_ERROR("Unsupported IndexParamAudioPortFormat");
        OMX_DBGT_EPILOG("Unsupported IndexParamAudioPortFormat");
        return OMX_ErrorUnsupportedIndex;
    }; break;
    case OMX_IndexParamOtherPortFormat:
    default: {
        OMX_DBGT_ERROR("Unsupported index");
        OMX_DBGT_EPILOG("Unsupported index");
        return OMX_ErrorUnsupportedIndex;
    }; break;
    }
}

uint32_t AudEncInputPort::getV4lSampleFormat(OMX_AUDIO_PARAM_PCMMODETYPE* pcmParam)
{
    uint32_t v4lsampleFormat = V4L2_MPEG_AUDIO_STM_PCM_FMT_32_S32LE;
    if(pcmParam->eNumData == OMX_NumericalDataSigned)
    {
        if(pcmParam->eEndian == OMX_EndianBig)
        {
            switch(pcmParam->nBitPerSample)
            {
            case 32: v4lsampleFormat = V4L2_MPEG_AUDIO_STM_PCM_FMT_32_S32BE; break;
            case 24: v4lsampleFormat = V4L2_MPEG_AUDIO_STM_PCM_FMT_24_S24BE; break;
            case 16: v4lsampleFormat = V4L2_MPEG_AUDIO_STM_PCM_FMT_16_S16BE; break;
            case 8:default: v4lsampleFormat = V4L2_MPEG_AUDIO_STM_PCM_FMT_8_S8; break;
            }
        }
        else
        {
            // OMX_EndianLittle
            switch(pcmParam->nBitPerSample)
            {
            case 32: v4lsampleFormat = V4L2_MPEG_AUDIO_STM_PCM_FMT_32_S32LE; break;
            case 24: v4lsampleFormat = V4L2_MPEG_AUDIO_STM_PCM_FMT_24_S24LE; break;
            case 16: v4lsampleFormat = V4L2_MPEG_AUDIO_STM_PCM_FMT_16_S16LE; break;
            case 8:default: v4lsampleFormat = V4L2_MPEG_AUDIO_STM_PCM_FMT_8_S8; break;
            }
        }
    }
    else
    {
        // assume OMX_NumericalDataUnsigned
        if(pcmParam->eEndian == OMX_EndianBig)
        {
            switch(pcmParam->nBitPerSample)
            {
            case 32: v4lsampleFormat = V4L2_MPEG_AUDIO_STM_PCM_FMT_32_S32BE; break;
            case 24: v4lsampleFormat = V4L2_MPEG_AUDIO_STM_PCM_FMT_24_S24BE; break;
            case 16: v4lsampleFormat = V4L2_MPEG_AUDIO_STM_PCM_FMT_16_U16BE; break;
            case 8:default: v4lsampleFormat = V4L2_MPEG_AUDIO_STM_PCM_FMT_8_U8; break;
            }
        }
        else
        {
            // OMX_EndianLittle
            switch(pcmParam->nBitPerSample)
            {
            case 32: v4lsampleFormat = V4L2_MPEG_AUDIO_STM_PCM_FMT_32_S32LE; break;
            case 24: v4lsampleFormat = V4L2_MPEG_AUDIO_STM_PCM_FMT_24_S24LE; break;
            case 16: v4lsampleFormat = V4L2_MPEG_AUDIO_STM_PCM_FMT_16_U16LE; break;
            case 8:default: v4lsampleFormat = V4L2_MPEG_AUDIO_STM_PCM_FMT_8_U8; break;
            }
        }
    }
    return v4lsampleFormat;
}

OMX_ERRORTYPE
AudEncInputPort::setParameter(OMX_INDEXTYPE nParamIndex, OMX_PTR pParamStruct)
{
    OMX_DBGT_PROLOG("on Audio encoder input port");
    switch ((int)nParamIndex)
    {
        case OMX_IndexParamAudioPortFormat:{
            OMX_DBGT_EPILOG();
            return OMX_ErrorNone;
        }; break;

        case OMX_IndexParamPortDefinition: {
            // Set port definition
            ASSERT_STRUCT_TYPE(pParamStruct, OMX_PARAM_PORTDEFINITIONTYPE);
            OMX_PARAM_PORTDEFINITIONTYPE* param;
            param = (OMX_PARAM_PORTDEFINITIONTYPE*)pParamStruct;
            assert(param->nPortIndex == this->definition().index());

            if (this->definition().bufferSize() != param->nBufferSize) {
                OMX_DBGT_PINFO("%s changing nBufferSize from %uKB to %uKB "
                               "(SetParameter(OMX_IndexParamPortDefinition input))",
                               component().name(),
                               this->definition().bufferSize()/1024,
                               param->nBufferSize/1024);
				this->definition().bufferSize(param->nBufferSize);
            }
            this->definition().setParamPortDef(param);
            OMX_DBGT_EPILOG();
            return OMX_ErrorNone;
        }; break;

        case OMX_IndexParamAudioPcm: {
            ASSERT_STRUCT_TYPE(pParamStruct, OMX_AUDIO_PARAM_PCMMODETYPE);
            OMX_AUDIO_PARAM_PCMMODETYPE* param;
            param = (OMX_AUDIO_PARAM_PCMMODETYPE*)pParamStruct;
            assert(param->nPortIndex == this->definition().index());
            if ((*param).nBitPerSample == 0) {
            // Take default value in this case
            OMX_DBGT_WARNING("%s::SetParameter, nBitPerSample wrongly set to 0, "
                "default it to %u for output nBufferSize computation",
                component().name(),
                m_pcmParams.nBitPerSample);
                (*param).nBitPerSample = m_pcmParams.nBitPerSample;
            }

            if ((this->AudEnc().outputPort().channelNb() > 0) &&
                (((*param).nChannels) != m_pcmParams.nChannels)) {
                // OMX_IndexParamAudioXX previously set value is different
                // from default PCM one, nb channels are so not consistent.
                // Keep default PCM value to not break previous nBufferSize
                // computation
                OMX_DBGT_WARNING("%s::SetParameter, PCM nChannels(%u) inconsistent "
                    "with default one (%u) "
                    "force it to %u for output nBufferSize computation",
                    component().name(),
                    (*param).nChannels,
                    m_pcmParams.nChannels,
                    m_pcmParams.nChannels);
                    (*param).nChannels = m_pcmParams.nChannels;
            }

            m_pcmParams = *param;
            /*polulate mPortAUDEncoderParams*/
            mPortAUDEncoderParams->byte_per_sample = m_pcmParams.nBitPerSample/8;
            mPortAUDEncoderParams->src_max_channels = 8;
            uint32_t sampleFormat = getV4lSampleFormat(&m_pcmParams);
            mPortAUDEncoderParams->src_metadata.sample_format = sampleFormat;
            mPortAUDEncoderParams->src_metadata.program_level = 0;
            mPortAUDEncoderParams->src_metadata.emphasis      = 0;
            mPortAUDEncoderParams->src_metadata.channel_count = m_pcmParams.nChannels;
            mPortAUDEncoderParams->src_metadata.sample_rate   = m_pcmParams.nSamplingRate?:44100;
            mPortAUDEncoderParams->src_metadata.emphasis      = 0;
            // channel 0..31
            OMX_DBGT_PINFO("PCM nChannels : %d, nSamplingRate : %d, sampleFormat : %d",
                (int)m_pcmParams.nChannels,
                (int)m_pcmParams.nSamplingRate,
                (int)sampleFormat);
            for(int32_t i=0; i < mPortAUDEncoderParams->src_max_channels; i++)
            {
                if(i < (int32_t)mPortAUDEncoderParams->src_metadata.channel_count)
                    mPortAUDEncoderParams->src_metadata.channel[i] = i;
                else
                    mPortAUDEncoderParams->src_metadata.channel[i] = 254;
            }

            OMX_DBGT_EPILOG();
            return OMX_ErrorNone;
        }; break;

        case OMX_IndexParamOtherPortFormat:
        default: {
            OMX_DBGT_ERROR("Unsupported index");
            OMX_DBGT_EPILOG("Unsupported index");
            return OMX_ErrorUnsupportedIndex;
        };break;
    }
}

bool AudEncInputPort::isSupportedCodingFormat(OMX_AUDIO_CODINGTYPE aFmt) const
{
    if (aFmt == OMX_AUDIO_CodingPCM)
        return true;
    else
        return false;
}

void AudEncInputPort::codingFormat(OMX_AUDIO_CODINGTYPE aFmt)
{
    assert(isSupportedCodingFormat(aFmt));
    this->definition().compressionFormat(aFmt);
}

OMX_ERRORTYPE
AudEncInputPort::doPortExtraProcessing(OMX_BUFFERHEADERTYPE**  ppBufferHdr,OMX_U32 nSizeBytes)
{
    int retVal = -1;
    int i;
    unsigned int bufferCount;
    OMX_ERRORTYPE eError = OMX_ErrorNone;
    OMX_DBGT_PROLOG();

    // we will allways use mmpapped buffers as we do not know if the buffers are contiguous
    mPortAUDEncoderParams->isInputPortBufferAllocated = OMX_TRUE;
    OMX_DBGT_PTRACE(" InputPort m_isAllocateBuffer : %d", m_isAllocateBuffer);

    if (!mPortAUDEncoderParams->input_buf_allocated)
    {
         retVal = static_cast<V4LAudioEncode*>(m_dvb)->setupBufferCount();

         if (retVal != 0)
         {
            OMX_DBGT_ERROR("Error while setupBuffer to v4L for output, return value %d",retVal);
            return OMX_ErrorHardware;
         }
         bufferCount = 0;

         if (mPortAUDEncoderParams->isInputPortBufferAllocated)
         {
             /* getting the input buffers and associating them to omx buffers */
             retVal = static_cast<V4LAudioEncode*>(m_dvb)->setUpBufferPointer();
             if (retVal != 0)
             {
                 OMX_DBGT_ERROR("Error while setUpBufferPointer to v4L for output, return value %d",retVal);
                 return OMX_ErrorHardware;
             }   
         }
         mPortAUDEncoderParams->input_buf_allocated = true;
    }

    for (i=0;i<MAX_INPUT_OUTPUT_BUFFERS;i++)
    {
        if (mapping[i].isAssociated == false)
            break;
    }

    if (i == MAX_INPUT_OUTPUT_BUFFERS)
    {
        OMX_DBGT_CRITICAL("No free buffer available for use - input");
        return OMX_ErrorInsufficientResources;
    }
    for (bufferCount = 0; bufferCount < mPortAUDEncoderParams->num_input_buffers ; bufferCount++)
    {
        if (mPortAUDEncoderParams->input_buffers[bufferCount].isUsed == 0)
            break;
    }

    // forming an assoication between input buffer and encoder internally allocated buffer
    mapping[i].isAssociated = (OMX_BOOL)true;
    mPortAUDEncoderParams->input_buffers[bufferCount].isUsed = (OMX_BOOL)true;
    mapping[i].buffProperty = 0;
    mapping[i].omxBuffer = *ppBufferHdr;
    OMX_DBGT_PTRACE("Input buffer count : %d buff 0x%x",bufferCount,(unsigned int)mPortAUDEncoderParams->input_buffers[bufferCount].start);
    mapping[i].internalBuffer = &(mPortAUDEncoderParams->input_buffers[bufferCount]);

    if (m_isAllocateBuffer)
    {
        (*ppBufferHdr)->pBuffer = (OMX_U8 *)mPortAUDEncoderParams->input_buffers[bufferCount].start;
    }

    OMX_DBGT_PINFO("Input buffer count : %d virtual address 0x%x omx buffer : 0x%x location : %d",
        bufferCount,
        (unsigned int)mPortAUDEncoderParams->input_buffers[bufferCount].start,
        (unsigned int)*ppBufferHdr,
        i);

    OMX_DBGT_EPILOG();
    return eError;
}

OMX_ERRORTYPE
AudEncInputPort::useBuffer(OMX_BUFFERHEADERTYPE** ppBufferHdr,
                           OMX_PTR pAppPrivate,
                           OMX_U32 nSizeBytes,
                           OMX_U8* pBuffer)
{
    OMX_ERRORTYPE eError = OMX_ErrorNone;
    OMX_DBGT_PROLOG();
    // Allocate a buffer header
    *ppBufferHdr = 0;

    OMX_BUFFERHEADERTYPE* pBufferHdr = allocateBufferHeader(pAppPrivate,
                                                            nSizeBytes);
    if (!pBufferHdr) {
        eError = OMX_ErrorInsufficientResources;
        OMX_DBGT_ERROR("Can not allocate buffer header");
        OMX_DBGT_EPILOG("Return error %s", OMX_TYPE_TO_STR(OMX_ERRORTYPE, eError));
        return eError;
    }

    /* doing pointer assignments here */
    pBufferHdr->pBuffer = pBuffer;
    *ppBufferHdr = pBufferHdr;

    // Increment internal buffer counter.
    this->incrementNbBuffers();

    if (AudEnc().IsConfigureDone == false) {
        OMX_DBGT_PINFO("Blocking usebuffer before aenc configure");
        AudEnc().m_condConfig.wait(AudEnc().m_mutexCmd);
        OMX_DBGT_PINFO("Unblocking allocateBuffer");
    }

    OMX_DBGT_PTRACE("To do doPortExtraProcessing");
    eError = doPortExtraProcessing(ppBufferHdr,nSizeBytes);

    OMX_DBGT_EPILOG("Error : 0x%x",(unsigned int)eError);
    return eError;
}

OMX_ERRORTYPE
AudEncInputPort::allocateBuffer(OMX_BUFFERHEADERTYPE** ppBufferHdr,
                                OMX_PTR pAppPrivate,
                                OMX_U32 nSizeBytes)
{
    OMX_ERRORTYPE eError = OMX_ErrorNone;
    OMX_DBGT_PROLOG();

    m_isAllocateBuffer = OMX_TRUE;

    // Allocate a buffer header
    *ppBufferHdr = 0;
    OMX_BUFFERHEADERTYPE* pBufferHdr = allocateBufferHeader(pAppPrivate,
                                                            nSizeBytes);
    if (!pBufferHdr) {
        eError = OMX_ErrorInsufficientResources;
        OMX_DBGT_ERROR("Can not allocate buffer header");
        OMX_DBGT_EPILOG("Return error %s", OMX_TYPE_TO_STR(OMX_ERRORTYPE, eError));
        return eError;
    }

    *ppBufferHdr = pBufferHdr;

    // Increment internal buffer counter.
    this->incrementNbBuffers();

    if (AudEnc().IsConfigureDone == false) {
        OMX_DBGT_PINFO("Blocking allocateBuffer before venc configure");
        AudEnc().m_condConfig.wait(AudEnc().m_mutexCmd);
        OMX_DBGT_PINFO("Unblocking allocateBuffer");
    }

    OMX_DBGT_PTRACE("To do doPortExtraProcessing");
    eError = doPortExtraProcessing(ppBufferHdr,nSizeBytes);

    OMX_DBGT_EPILOG("Error : 0x%x",(unsigned int)eError);
    return eError;
}

OMX_ERRORTYPE
AudEncInputPort::freeBuffer(OMX_BUFFERHEADERTYPE* pBufferHdr)
{
    OMX_DBGT_PROLOG();
    assert(!isTunneled());

    int retVal = -1;
    retVal = static_cast<V4LAudioEncode*>(m_dvb)->freeBufferPointer();
    if (retVal < 0)
    {
            OMX_DBGT_ERROR("Encoder Input Port v4l unmap failed (%s)", strerror (errno));
    }

    freeBufferHeader(pBufferHdr);
    this->decrementNbBuffers();

    OMX_DBGT_EPILOG();
    return OMX_ErrorNone;
}

OMX_ERRORTYPE
AudEncInputPort::emptyThisBuffer(OMX_BUFFERHEADERTYPE* pBufferHdr)
{
    // Called by player to send a audio encoded frame buffer
    // This buffer is pushed via a queue to the PusherThread
    // in order to be sent to SE
    OMX_DBGT_PROLOG("pBufferHdr = 0x%x pBufferHdr->pBuffer : 0x%x nFlags : 0x%x",
        (unsigned int)pBufferHdr,
        (unsigned int)pBufferHdr->pBuffer,
        (unsigned int)pBufferHdr->nFlags);

    this->push(pBufferHdr);

    OMX_DBGT_EPILOG();
    return OMX_ErrorNone;
}

OMX_ERRORTYPE
AudEncInputPort::fillThisBuffer(OMX_BUFFERHEADERTYPE* pBufferHdr)
{
    assert(0 && "Invalid call on input port");
    return OMX_ErrorNone;
}

OMX_ERRORTYPE AudEncInputPort::flush()
{
    OMX_DBGT_PROLOG("Flush audio encoder input port");
    if (isTunneled()) {
        // Proprietary tunneling. Nothing to flush
        OMX_DBGT_EPILOG("Port is tunneled.");
        return OMX_ErrorNone;
    }
   this->getRawFifos()->lock();

    OMX_BUFFERHEADERTYPE *buf  = 0;
    while(this->getRawFifos()->tryPop(buf) == true) {
        this->AudEnc().notifyEmptyBufferDone(buf);
    }
    this->getRawFifos()->unlock();
    buf  = 0;
    buf = this->relockCurrent();
    if(buf != NULL)
    {
        this->AudEnc().notifyEmptyBufferDone(buf);
    }
    this->freeAndUnlockCurrent();


    OMX_DBGT_EPILOG();
    return OMX_ErrorNone;
}

//=============================================================================
//  nothing specific to be done for encoder
//=============================================================================
OMX_ERRORTYPE AudEncInputPort::pause()
{
    OMX_DBGT_PROLOG("Pausing audio input port");
    if (this->threadState() == EThreadState_Paused) {
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }

    // Pausing thread
    this->threadState(EThreadState_Paused);

    // Waiting thread is paused

    OMX_DBGT_EPILOG();
    return OMX_ErrorNone;
}

//=============================================================================
//  nothing specific to be done for encoder
//=============================================================================
OMX_ERRORTYPE AudEncInputPort::resume()
{
    if (this->threadState() == EThreadState_Running) {
        return OMX_ErrorNone;
    }

    // Resuming thread
    this->threadState(EThreadState_Running);
    return OMX_ErrorNone;
}


int AudEncInputPort::encodeFrame(unsigned int nOmxFlags,
                                 uint8_t* pBuf, unsigned int nLen,
                                 uint64_t nTimeStamp)
{
    int retVal = -1;
    return retVal;
}


//=============================================================================
//  Input port core function of pusher thread
//  Infinite loop: Wait for encoded frame from player and send them SE to encode it
//=============================================================================
void AudEncInputPort::threadFunction()
{
    OMX_DBGT_PROLOG();
    struct buffer_t *internalBufferPtr;
    int retVal = -1;

    while(true)
    {
        if (m_bAfterEosFlag)
        {
            goto EXIT;
        }
        OMX_BUFFERHEADERTYPE *pInBufHdr = this->popAndLockCurrent();

        if (pInBufHdr != NULL && pInBufHdr != this->EOSBUF)
        {
            OMX_DBGT_PINFO("input buffer : 0x%x actual 0x%x length : %d timestamp : 0x%x flags : %d",
                (unsigned int)pInBufHdr,
                (unsigned int)pInBufHdr->pBuffer,
                (int)pInBufHdr->nFilledLen,
                (int)pInBufHdr->nTimeStamp,
                (int)pInBufHdr->nFlags);
            retVal = findAssociation((void *)pInBufHdr, 0,(void **)&internalBufferPtr);
            if (retVal < 0)
            {
                OMX_DBGT_ERROR("OMX to Internal Buffer Association NOT found");
                goto EXIT;
            }

            internalBufferPtr->v4l2_buf.bytesused = pInBufHdr->nFilledLen;
            OMX_DBGT_PINFO("Status of internal buffer isUsed (should be false) : %d",(int)internalBufferPtr->isUsed);
            OMX_DBGT_PINFO("To check v4l2 type input : %d, memory : %d index : %d start addr 0x%x len : %d filledlen : %d",
                (int)internalBufferPtr->v4l2_buf.type,
                (int)internalBufferPtr->v4l2_buf.memory,
                (int)internalBufferPtr->v4l2_buf.index,
                (unsigned int)internalBufferPtr->start,
                (int)internalBufferPtr->length,
                (unsigned int)internalBufferPtr->v4l2_buf.bytesused);

            internalBufferPtr->v4l2_buf.timestamp.tv_sec = (pInBufHdr->nTimeStamp / 1000000);
            internalBufferPtr->v4l2_buf.timestamp.tv_usec = (pInBufHdr->nTimeStamp % 1000000);

            //currently assuming last empty buffer with EOS
            if (pInBufHdr->nFlags & OMX_BUFFERFLAG_EOS)
            {
                pInBufHdr->nFilledLen = 0;
                m_bAfterEosFlag = OMX_TRUE;
                OMX_DBGT_PINFO("Execute EOS");
            }

            if ((mPortAUDEncoderParams->isInputPortBufferAllocated) && (!m_isAllocateBuffer))
            {
				OMX_DBGT_PINFO("Performing memcpy for input buffer");
                memcpy(internalBufferPtr->start,pInBufHdr->pBuffer,pInBufHdr->nFilledLen);
            }

#ifdef AUD_ENCODE_INPUTBUFFER_DUMP
		    {
		        FILE *fd;
		        fd = fopen("/data/encode_input.pcm","ab+");
		        if (!fd) {
		            OMX_DBGT_ERROR(": Unable to open %s file.Data Dump discarded","encode_input.pcm");
		        }
		        else{
		            fwrite(internalBufferPtr->start,1, pInBufHdr->nFilledLen,fd);
		            fclose(fd);
		        }
		    }
#endif
            uint64_t temp=0;
            if(static_cast<V4LAudioEncode*>(m_dvb)->QueueBuffer((void *)internalBufferPtr,(int &)(pInBufHdr->nFilledLen),temp) != 0)
            {
                OMX_DBGT_ERROR("Encoder Input Port initial QBUF failed (%s)", strerror (errno));
                internalBufferPtr->isUsed = false;
                this->AudEnc().notifyEmptyBufferDone(pInBufHdr);
                this->freeAndUnlockCurrent();
                goto EXIT;
            }

            m_nSentFrame++;
            OMX_DBGT_PINFO("InputPort : Status of internal buffer isUsed (should be true NOW) : %d, Counter : %d",
                (int)internalBufferPtr->isUsed,
                m_nSentFrame);

            //Now dequeue the buffer
            retVal = static_cast<V4LAudioEncode*>(m_dvb)->DequeueEncoderBuffer(internalBufferPtr);
            if (retVal < 0)
            {
                OMX_DBGT_ERROR("!!FATAL!! Error while DequeueBuffer for input buffer");
                internalBufferPtr->isUsed = false;
                this->AudEnc().notifyEmptyBufferDone(pInBufHdr);
                this->freeAndUnlockCurrent();
                goto EXIT;
            }

            internalBufferPtr->isUsed = false;
            OMX_DBGT_PINFO("Abt to call emptybuffer done pInBufHdr : 0x%x pInBufHdr->pBuffer : 0x%x",
                (unsigned int)pInBufHdr,
                (unsigned int)pInBufHdr->pBuffer);
            this->AudEnc().notifyEmptyBufferDone(pInBufHdr);

            OMX_DBGT_PINFO("Input buffer rel : 0x%x and isUsed : %d",(unsigned int)pInBufHdr,(int)internalBufferPtr->isUsed);
            this->freeAndUnlockCurrent();
        }
        else
        {
            OMX_DBGT_PINFO("EOSBUF detected on PusherRunner");
            this->freeAndUnlockCurrent();
            goto EXIT;
        }
    }
EXIT:
    flush();
    OMX_DBGT_PINFO("Pusher thread exit (AENCComponent::PusherRunner)");
    OMX_DBGT_EPILOG();
}


} // eof namespace stm
