/*
 * Copyright 2011 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *
 * File   ::    AudioEncoder.cpp
 * Author ::    Kausik Maiti(kausik.maiti@st.com)
 *
 *
 */

#include <linux/dvb/stm_dvb.h>
#include <linux/dvb/stm_video.h>
#include <linux/videodev2.h>
#include <linux/dvb/dvb_v4l2_export.h>
#include <IDvbDevice.hpp>
#include "AudioEncoderPort.hpp"
#include "AudioEncoder.hpp"
#include "OMX_debug.h"

#if HAVE_ASSERT_H
#include <assert.h>
#endif

#if HAVE_DBGTASSERT_H
// Re-direct assert on DBGT_ASSERT macro
#define assert(expr)  OMX_DBGT_ASSERT(expr)
#endif

#define DBGT_DECLARE_AUTOVAR
#define DBGT_PREFIX "AENC"
#define DBGT_LAYER 0
#include <linux/dbgt.h>


namespace stm {

//==========================================
// AENCComponent implementation
//==========================================

static const char kRoleDefault[] = "default";
static const char kRoleAAC[] = "audio_encoder.aac";

AENCComponent::AENCComponent(OMX_HANDLETYPE hComponent):
    OmxComponent("OMX.STM.Audio.Encoder", hComponent, OMX_ST_AUDIO_PORT_NUMBER),
    IsConfigureDone(false),
    m_mutexCmd(),
    m_condConfig(),
    m_inputPort(OMX_ST_AUDIO_INPUT_PORT, OMX_DirInput, *this),
    m_outputPort(OMX_ST_AUDIO_OUTPUT_PORT, OMX_DirOutput, *this),
    m_componentRole(),
    m_AUDEncoderParams(NULL),
    m_nbBufferMapping(NULL)
{
    DBGT_TRACE_INIT(aenc); //expands debug.aenc.trace
}

AENCComponent::~AENCComponent()
{
}

//==========================================================
// AENCComponent::flush
// flush OMX port at the end of playback when a seek occurs
//==========================================================
OMX_ERRORTYPE AENCComponent::flush(unsigned int nPortIdx)
{
    OMX_DBGT_PROLOG();
    OMX_DBGT_PDEBUG("AENCComponent::flush on port %d  %s", nPortIdx, name());
    int result = 1;


    OMX_ERRORTYPE eError = this->getPort(nPortIdx).flush();
    OMX_DBGT_EPILOG();
    return eError;
}

//==========================================================
// AENCComponent::getParameter
// allows to return current parameters to player
//==========================================================
OMX_ERRORTYPE AENCComponent::getParameter(OMX_INDEXTYPE       nParamIndex,
                                          OMX_PTR             pParamStruct)
{
    OMX_DBGT_PDEBUG("%s getParameter 0x%08x", name(), nParamIndex);
    OMX_DBGT_PROLOG();
    switch ((int)nParamIndex)
    {
    case OMX_IndexParamAudioInit: {
        ASSERT_STRUCT_TYPE(pParamStruct, OMX_PORT_PARAM_TYPE);
        OMX_PORT_PARAM_TYPE *param = (OMX_PORT_PARAM_TYPE*)pParamStruct;
        assert(param);
        param->nPorts = OMX_ST_AUDIO_PORT_NUMBER;
        param->nStartPortNumber = 0;
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }; break;


    }

    // If the component does not support it, we fall back to generic logic.
    OMX_ERRORTYPE eError = OmxComponent::getParameter(nParamIndex,
                                                      pParamStruct);

    OMX_DBGT_EPILOG();
    return eError;
}

//==========================================================
// AENCComponent::setParameter
// allows player to set parameters associated to OMX component
//==========================================================
OMX_ERRORTYPE AENCComponent::setParameter(OMX_INDEXTYPE       nParamIndex,
                                          OMX_PTR             pParamStruct)
{
    OMX_DBGT_PROLOG("%s setParameter 0x%08x", name(), nParamIndex);
    OMX_ERRORTYPE eError = OMX_ErrorNone;

    switch ((int)nParamIndex)
    {
    case OMX_IndexParamStandardComponentRole: {
        ASSERT_STRUCT_TYPE(pParamStruct, OMX_PARAM_COMPONENTROLETYPE);
        OMX_PARAM_COMPONENTROLETYPE *param = (OMX_PARAM_COMPONENTROLETYPE*)pParamStruct;
        assert(param);
        eError = this->setRole((const char*)param->cRole);
        OMX_DBGT_EPILOG();
        return eError;
    }; break;

    default:
        OMX_DBGT_PDEBUG("AENCComponent::set undef parameter (0x%08x)",
                    (unsigned int)nParamIndex);
        break;
    }

    // If the component does not support it, we fall back to generic logic.
    eError = OmxComponent::setParameter(nParamIndex, pParamStruct);

    OMX_DBGT_EPILOG();
    return eError;
}

OMX_ERRORTYPE
AENCComponent::getConfig(OMX_INDEXTYPE nIndex,
                          OMX_PTR pConfStruct)
{
    OMX_DBGT_PROLOG();
    switch ((int)nIndex)
    {

    default:
        OMX_DBGT_PDEBUG("AENCComponent::get undef Config (0x%08x)",
                    (unsigned int)nIndex);
        break;
    }

    // If the component does not support it, we fall back to generic logic.
    OMX_ERRORTYPE eError = OmxComponent::getConfig(nIndex,
                                                   pConfStruct);
    OMX_DBGT_EPILOG();
    return eError;
}

OMX_ERRORTYPE
AENCComponent::setConfig(OMX_INDEXTYPE nIndex,
                          OMX_PTR pConfStruct)
{
    OMX_DBGT_PROLOG();
    switch ((int)nIndex)
    {

    default:
        OMX_DBGT_PDEBUG("AENCComponent::set undef Config (0x%08x)",
                    (unsigned int)nIndex);
        break;
    }

    // If the component does not support it, we fall back to generic logic.
    OMX_ERRORTYPE eError = OmxComponent::getConfig(nIndex,
                                                   pConfStruct);
    OMX_DBGT_EPILOG();
    return eError;
}

OMX_ERRORTYPE AENCComponent::emptyThisBuffer(OMX_BUFFERHEADERTYPE* pBufferHdr)
{
    // Reset EOS flag
    this->outputPort_().resetAfterEosFlag();
    OMXdebug_flagAfterEos = 0;

    return OmxComponent::emptyThisBuffer(pBufferHdr);
}

OMX_ERRORTYPE AENCComponent::fillThisBuffer(OMX_BUFFERHEADERTYPE* pBufferHdr)
{
    OMX_DBGT_PROLOG();
    OMX_ERRORTYPE eError = OmxComponent::fillThisBuffer(pBufferHdr);
    OMX_DBGT_EPILOG();
    return eError;
}

//==========================================================
// AENCComponent::destroy
// Allows free resources associated to the OMX component
//==========================================================
void AENCComponent::destroy()
{
    OMX_DBGT_PROLOG();

    // stop, cleanup and delete audio ports
    m_outputPort.destroy();
    m_inputPort.destroy();

    free(m_AUDEncoderParams);
    m_AUDEncoderParams = NULL;

    free(m_nbBufferMapping);
    m_nbBufferMapping = NULL;

    OmxComponent::destroy();

    IsConfigureDone = false;
    OMX_DBGT_EPILOG();
}

//==========================================================
// AENCComponent::create
// Allows create resources associated to a new OMX component
//==========================================================
OMX_ERRORTYPE AENCComponent::create()
{
    OMX_ERRORTYPE eError = OMX_ErrorNone;

    OMX_DBGT_PROLOG();

    if ((eError = OmxComponent::create()) != OMX_ErrorNone) {
        OMX_DBGT_ERROR();
        OMX_DBGT_EPILOG();
        return eError;
    }

    // Initialize encoder roles
    this->role().add(kRoleAAC);

    // Initialize the audio parameters for input port
    if ((eError = m_inputPort.create()) != OMX_ErrorNone) {
        OMX_DBGT_ERROR("Error when creating audio encoder input port");
        OMX_DBGT_EPILOG();
        return eError;
    }

    // Initialize the audio parameters for output port
    if ((eError = m_outputPort.create()) != OMX_ErrorNone) {
        OMX_DBGT_ERROR("Error when creating audio encoder output port");
        OMX_DBGT_EPILOG();
        return eError;
    }

    // Set default role: currently AAC
    this->setRole(kRoleDefault);

    OMXdebug_flagAfterEos    = 0;

    m_AUDEncoderParams = (AUDEncoderParams_t *)malloc(sizeof(AUDEncoderParams_t));
    if (m_AUDEncoderParams == NULL)
    {
        OMX_DBGT_CRITICAL("Not enough memory to allocate memory for m_AUDEncoderParams.... KILL");
        return OMX_ErrorInsufficientResources;
    }

    memset((void *)m_AUDEncoderParams,0,sizeof(AUDEncoderParams_t));

    m_nbBufferMapping = (EncoderBufferMapping *)calloc(MAX_INPUT_OUTPUT_BUFFERS,sizeof(EncoderBufferMapping));
    if (m_nbBufferMapping == NULL)
    {
        OMX_DBGT_CRITICAL("Not enough memory to allocate memory for m_nbBufferMapping.... KILL");
        return OMX_ErrorInsufficientResources;
    }

    for (int i=0;i<MAX_INPUT_OUTPUT_BUFFERS;i++)
    {
        memset((void *)&m_nbBufferMapping[i],0,sizeof(EncoderBufferMapping));
    }

    OMX_DBGT_PTRACE("m_AUDEncoderParams : 0x%x m_nbBufferMapping : 0x%x",(unsigned int)m_AUDEncoderParams,(unsigned int)m_nbBufferMapping);

    m_inputPort.setInternalPointers(&m_AUDEncoderParams,&m_nbBufferMapping);
    m_outputPort.setInternalPointers(&m_AUDEncoderParams,&m_nbBufferMapping);


    OMX_DBGT_EPILOG();
    return OMX_ErrorNone;
}

OMX_ERRORTYPE AENCComponent::setRole(const char* pNewRole)
{
    int nRoleIdx = -1;
    if (strcmp(pNewRole, kRoleDefault) == 0) {
        nRoleIdx = 0;
        OMX_DBGT_PDEBUG("Setting role to default %s", this->role().at(nRoleIdx));
    } else {
        // check if proposed role is supported
        for (unsigned int i = 0; i < this->role().size(); i++) {
            if (strcmp(pNewRole, this->role().at(i)) == 0) {
                nRoleIdx = i;
                break;
            }
        }
    }

    if (nRoleIdx == -1) {
        OMX_DBGT_ERROR("Unsupported role %s", pNewRole);
        return OMX_ErrorUnsupportedSetting;
    }

    const char* pActivRole = this->role().active(nRoleIdx);
    if (strcmp(pActivRole, kRoleAAC) == 0) {
        this->outputPort_().codingFormat(OMX_AUDIO_CodingAAC);
        this->inputPort_().codingFormat(OMX_AUDIO_CodingPCM);
    }else {
        // TODO don't know which role to use in default case...
        assert(0 && "Role not supported");
    }

    return OMX_ErrorNone;
}

const AudEncPort& AENCComponent::getPort(unsigned int nIdx) const
{
    assert(nIdx < OMX_ST_AUDIO_PORT_NUMBER);
    if (nIdx == 0)
        return m_inputPort;
    else
        return m_outputPort;
}

AudEncPort& AENCComponent::getPort(unsigned int nIdx)
{
    assert(nIdx < OMX_ST_AUDIO_PORT_NUMBER);
    if (nIdx == 0)
        return m_inputPort;
    else
        return m_outputPort;
}

//==========================================================
// AENCComponent::configure
//  encoder configuration according to codec
//==========================================================
OMX_ERRORTYPE AENCComponent::configure()
{
	OMX_DBGT_PROLOG();

	IDvbDevice * m_dvb;
	OMX_DBGT_PTRACE("Abt creating output port dvb device");
	m_dvb = new V4LAudioEncode(0x0, this->name(),OMX_ST_AUDIO_OUTPUT_PORT);
	OMX_DBGT_PTRACE("After creating output port dvb device");
    if (!m_dvb) {
        OMX_DBGT_ERROR("Can not allocate new Audio device for Output Port");
        OMX_DBGT_EPILOG();
        return OMX_ErrorInsufficientResources;
    }

    this->outputPort_().setDvbDevice(&m_dvb);;

    OMX_DBGT_PTRACE("Abt creating input port dvb device");
    m_dvb = new V4LAudioEncode(0x0, this->name(),OMX_ST_AUDIO_INPUT_PORT);
    OMX_DBGT_PTRACE("After creating input port dvb device");
    if (!m_dvb)
    {
        OMX_DBGT_ERROR("Can not allocate new Audio device for Input Port");
        OMX_DBGT_EPILOG();
        return OMX_ErrorInsufficientResources;
    }

    this->inputPort_().setDvbDevice(&m_dvb);

    OMX_DBGT_PINFO("Device name for input : %s output : %s",this->inputPort_().dvb().name(),this->outputPort_().dvb().name());

    m_mutexCmd.lock();
    IsConfigureDone = true;
    m_mutexCmd.release();
    m_condConfig.signal();
    OMX_DBGT_EPILOG();
    return OMX_ErrorNone;
}

//==========================================================
// AENCComponent::start
//  start encoding
//==========================================================
OMX_ERRORTYPE AENCComponent::start()
{
    OMX_DBGT_PROLOG();

	// Create the port threads
    if (this->outputPort_().start() != 0) {
        return OMX_ErrorInsufficientResources;
    }

    if (this->inputPort_().start() != 0) {
        return OMX_ErrorInsufficientResources;
    }

    OMX_DBGT_EPILOG();

    return OMX_ErrorNone;
}

//==========================================================
// AENCComponent::stop
//  stop encoding
//==========================================================
void AENCComponent::stop()
{
    OMX_DBGT_PROLOG();
    if (this->inputPort_().stop() != 0)
        OMX_DBGT_ERROR("inputPort.Stop %s failed (%s)", name(), strerror (errno));

    if (this->outputPort_().stop() != 0)
        OMX_DBGT_ERROR("outputPort.Stop %s failed (%s)", name(), strerror (errno));
        
    OMX_DBGT_EPILOG();
}


//==============================================================================
// AENCComponent::execute
//  OMX command already processed by CmdThread: nothing to do
//==============================================================================
void AENCComponent::execute()
{}

} // eof namespace stm

//==========================================================
// OMX_ComponentInit
// "C" interface to initialize a new OMX component: creation of the associated object
//==========================================================
extern "C" OMX_ERRORTYPE OMX_ComponentInit (OMX_HANDLETYPE hComponent)
{
    return stm::OmxComponent::ComponentInit(new stm::AENCComponent(hComponent));
}

