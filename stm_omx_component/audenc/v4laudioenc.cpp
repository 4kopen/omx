/*
 * Copyright 2013 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file   v4laudioenc.cpp
 * @author Kausik Maiti (kausik.maiti@st.com)
 */

#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <stdio.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <OMX_debug.h>
#include <v4laudioenc.hpp>

#if HAVE_ASSERT_H
#include <assert.h>
#endif

#if HAVE_DBGTASSERT_H
// Re-direct assert on DBGT_ASSERT macro
#define assert(expr)  OMX_DBGT_ASSERT(expr)
#endif

#undef  DBGT_TAG
#define DBGT_TAG "V4LAE"
#define DBGT_PREFIX "V4LAE"
#define DBGT_LAYER  1
#define DBGT_VAR mDbgtVarV4LAE
#define DBGT_DECLARE_AUTOVAR
#include <linux/dbgt.h>

#define AUD_ENCODE_MAX_CTRL             35
static  char cc[5];

#define   CHECK_DEVICE(v4l_fd)                                                                    \
        if (v4l_fd == 0) {                                                                        \
        OMX_DBGT_CRITICAL("Device Id is NULL for function : %s file : %s line : %d",__PRETTY_FUNCTION__, __FILE__, __LINE__);     \
        }

//=============================================================================
// V4LAudioEncode::DVBVideo_SF
//  Constructor
//=============================================================================
V4LAudioEncode::V4LAudioEncode(const char* aDeviceName, const char *cName, int aEncoderPort)
    :IDvbDevice(),
     v4l_fd(-1),
     mIsLoggingEnabled(false),
     device_id(-1),
     EncoderPort(aEncoderPort),
     formats(NULL),
     nbFormats(-1),
     encoderParams(NULL),
     m_nEncoding(OMX_AUDIO_CodingUnused),
     m_nState(EState_Stop),
     m_condState()
{
    DBGT_TRACE_INIT(aenc);// Reuse AENC trace property
    memset((void *)cc,0x0,sizeof(cc));
    if (aDeviceName) 
    {
        //check the string length before copy
        assert(strlen(aDeviceName) <= sizeof(m_deviceName));
        strcpy(m_deviceName, aDeviceName);
    }
    else
    {
       memset((void *)m_deviceName,0,sizeof(m_deviceName));
    }

    OMXDEBUG_UPDATE_AV_TRACE_SETTINGS(cName); //name of the omx component is passed here
}

//=====================================================================================
// V4LAudioEncode::create
//  To open the endoder device, this API is used for both
// encoder input and output
//=====================================================================================
int V4LAudioEncode::create(const char *const driver, const char *const card,int flags)
{
    struct v4l2_capability cap;
    int ret = -1;
    unsigned int i = 0;
    OMX_DBGT_PROLOG();

    if (!flags) {
        OMX_DBGT_PINFO("File mode not registered, would open in O_RDWR \n");
        flags = O_RDWR;
    }

    if (v4l_fd > 0) {
        OMX_DBGT_ERROR("Device dev id : %d, already been registered \n",v4l_fd);
        return ret;
    }

    do {
        sprintf(m_deviceName,"/dev/video%d", i);
        OMX_DBGT_PINFO("Searching for device : %s port : %d \n",m_deviceName,EncoderPort);
        v4l_fd = open(m_deviceName, flags);
        if (v4l_fd < 0) {
            OMX_DBGT_ERROR("Failed to find device for %s:%s\n", driver,card);
            return v4l_fd;
        }
        OMX_DBGT_PINFO("Trying to open for device : %s port : %d v4l_fd :%d \n",m_deviceName,EncoderPort,v4l_fd);

        ret = ioctl(v4l_fd, VIDIOC_QUERYCAP, &cap);
        if (ret < 0) {
            OMX_DBGT_ERROR("Driver BUG: device %s doesn't answer to VIDIOC_QUERYCAP - ignoring for port : %d \n",
                m_deviceName,
                EncoderPort);
            close(v4l_fd);
            continue;
        }

        if ((strcasecmp((char *)cap.driver, driver) == 0) &&
            (strcasecmp((char *)cap.card, card) == 0)) {
            device_id = i;
            OMX_DBGT_PINFO("Found device file: %s for %s:%s\n", m_deviceName,
                   driver, card);
            break;
        }

        close(v4l_fd);
        i++;
    } while (i);

    OMX_DBGT_EPILOG("Value of v4l_fd : %d device name : %s device id : %d\n",v4l_fd,m_deviceName,device_id);
    return ret;
}

//=====================================================================================
// V4LAudioEncode::configureEncoderStructure
//=====================================================================================
void V4LAudioEncode::configureEncoderStructure(void **encoderParam, int EncoderType)
{
    CHECK_DEVICE(v4l_fd);
    m_nEncoding = (OMX_AUDIO_CODINGTYPE) EncoderType;
    switch (m_nEncoding)
    {
        case OMX_AUDIO_CodingPCM :
        case OMX_AUDIO_CodingAAC : OMX_DBGT_PINFO("OMX encoderHandle type AAC/PCM registered");
            encoderParams = *(AUDEncoderParams_t **)encoderParam;
            break;
        default: OMX_DBGT_CRITICAL("EncodeType : %d not supported",m_nEncoding);
            break;
    }
    OMX_DBGT_PINFO("Encoder param structure configured : 0x%x encodeType : %d",
        (unsigned int)encoderParams,
        m_nEncoding);
}

//=====================================================================================
// V4LAudioEncode::SpecificationCheck
//=====================================================================================
int V4LAudioEncode::SpecificationCheck()
{
    struct v4l2_capability cap;
    int ret = -1;
    OMX_DBGT_PROLOG();
    CHECK_DEVICE(v4l_fd);
    memset (&cap, 0, sizeof (struct v4l2_capability));

    /* Get device capabilites */
    ret = ioctl(v4l_fd, VIDIOC_QUERYCAP, &cap);
    if (ret< 0)
    {
        OMX_DBGT_ERROR("Unable to open V4L Device (%s) for VIDIOC_QUERYCAP", strerror(errno));
        goto EXIT;
    }
    if (!(cap.capabilities & V4L2_CAP_VIDEO_CAPTURE)) {
        OMX_DBGT_ERROR("V4L2_CAP_VIDEO_CAPTURE should be supported");
        return -1;
    } else {
        OMX_DBGT_PINFO("V4L2_CAP_VIDEO_CAPTURE supported!");
    }
    if (!(cap.capabilities & V4L2_CAP_VIDEO_OUTPUT)) {
        OMX_DBGT_ERROR("V4L2_CAP_VIDEO_OUTPUT should be supported");
        return -1;
    } else {
        OMX_DBGT_PINFO("V4L2_CAP_VIDEO_OUTPUT supported!");
    }
    if (!(cap.capabilities & V4L2_CAP_STREAMING)) {
        OMX_DBGT_ERROR("V4L2_CAP_STREAMING should be supported");
        return -1;
    } else {
        OMX_DBGT_PINFO("V4L2_CAP_STREAMING supported!");
    }
    if (!(cap.capabilities & V4L2_CAP_READWRITE)) {
        OMX_DBGT_ERROR("V4L2_CAP_READWRITE should be supported");
        return -1;
    } else {
        OMX_DBGT_PINFO("V4L2_CAP_READWRITE supported!");
    }

EXIT:
    OMX_DBGT_EPILOG();
    return ret;
}

char * V4LAudioEncode::V4lToString(int32_t v)
{
    //	v = ((__u32)(a) | ((__u32)(b) << 8) | ((__u32)(c) << 16) | ((__u32)(d) << 24))
    cc[0] = v       & 0xff;
    cc[1] = (v>>8)  & 0xff;
    cc[2] = (v>>16) & 0xff;
    cc[3] = (v>>24) & 0xff;
    cc[4] = 0;
    return cc;
}

//=====================================================================================
// V4LAudioEncode::QueryFormat
//=====================================================================================
int V4LAudioEncode::QueryFormat()
{
    uint32_t nfmts = 0;
    uint32_t i;
    struct v4l2_fmtdesc fmt;
    OMX_DBGT_PROLOG();
    CHECK_DEVICE(v4l_fd);
    fmt.index = 0;

    if (EncoderPort == 0)
    {
        fmt.type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
    }
    else
    {
        fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    }

    while (ioctl(v4l_fd, VIDIOC_ENUM_FMT, &fmt) >= 0)
        fmt.index = ++nfmts;

    if (nfmts == 0)
    {
        OMX_DBGT_ERROR("Not able to retrieve formats");
        return -1;
    }

    OMX_DBGT_PDEBUG("For Port : %d nfmts available : %d ",EncoderPort, nfmts);

    //allocating memory for format structure, would be free when VIDIOC_S_FMT is done
    formats = (v4l2_fmtdesc *)malloc(nfmts * sizeof(v4l2_fmtdesc));
    if (!formats){
        OMX_DBGT_ERROR("Failed to allocate memory for V4L2 formats\n");
        return -1;
    }

    //printing all audio formats supported
    for (i = 0; i < nfmts; i++)
    {
        formats[i].index = i;
        formats[i].type = fmt.type;
        if (ioctl(v4l_fd, VIDIOC_ENUM_FMT, &formats[i]) < 0)
        {
            OMX_DBGT_ERROR("cannot get format description for index %u",formats[i].index);
            free(formats);
            formats = NULL;
            return -1;
        }
        OMX_DBGT_PDEBUG("For Port : %d Description %s ",EncoderPort, formats[i].description);
    }
    nbFormats = nfmts;
    OMX_DBGT_EPILOG();
    return 0;
}

bool V4LAudioEncode::isOpen() const
{ return (id() != -1); }

//=============================================================================
// V4LAudioEncode::destroy
//=============================================================================
int V4LAudioEncode::destroy()
{
    OMX_DBGT_PROLOG();
    if(close(id()) != 0) {
        OMX_DBGT_ERROR("Audio close failed (%s)", strerror (errno));
        OMX_DBGT_EPILOG("Unable to close device");
        return -1;
    }

    v4l_fd = -1;
    OMX_DBGT_EPILOG();
    return 0;
}

//=============================================================================
// V4LAudioEncode::pause (empty function)
//=============================================================================
int V4LAudioEncode::pause(bool hw) const
{
    assert(0);
    return 0;
}

//=============================================================================
// V4LAudioEncode::lock (empty function)
//=============================================================================
void V4LAudioEncode::lock() const
{

}


//=============================================================================
// V4LAudioEncode::unlock (empty function)
//=============================================================================
void V4LAudioEncode::unlock() const
{

}

//=============================================================================
// V4LAudioEncode::resetflags (empty function)
//=============================================================================
void V4LAudioEncode::resetflags() const
{

}

//=============================================================================
// V4LAudioEncode::resume (empty function)
//=============================================================================
int V4LAudioEncode::resume(bool hw) const
{
    assert(0);
    return 0;
}

//=====================================================================================
// V4LAudioEncode::ConfigureEncoder
//=====================================================================================
int V4LAudioEncode::ConfigureEncoder()
{
    int ret = -1;
    uint32_t ioIndex = 0;
    OMX_DBGT_PROLOG("EncoderPort : %d",EncoderPort);
    CHECK_DEVICE(v4l_fd);

    if (EncoderPort < 0)
    {
        OMX_DBGT_ERROR("Invalid deviceId : %d OR EncoderPort : %d",device_id,EncoderPort);
    }
    else
    {
        if (EncoderPort == 0)
        {
            /* Select audio output */
            ret = ioctl(v4l_fd, VIDIOC_S_AUDOUT, &ioIndex);
	        if ( ret < 0) {
	            OMX_DBGT_ERROR("Cannot set encoder context %u\n", ioIndex);
	            OMX_DBGT_EPILOG();
	            return -1;
	        }
        }
        else if (EncoderPort == 1)
        {
            /* Select Audio input */
            ret = ioctl(v4l_fd, VIDIOC_S_AUDIO, &ioIndex);
            if ( ret < 0) {
	            OMX_DBGT_ERROR("Cannot set input %u\n", ioIndex);
	            return -1;
            }
        }
        else
        {
            OMX_DBGT_CRITICAL("Invalid EncoderPort : %u for ConfigureEncoder",EncoderPort);
        }
    }

    OMX_DBGT_EPILOG();
    return ret;
}

//=====================================================================================
// V4LAudioEncode::setupBuffer
//=====================================================================================
int V4LAudioEncode::setupBufferCount()
{
    int ret=-1;
    unsigned int nb_buffer = 0;
    struct v4l2_requestbuffers v4l2reqbuf;
    OMX_DBGT_PROLOG();

    /* Buffers allocation */
    /* Request input buffers from v4l2 */
    memset (&v4l2reqbuf, 0, sizeof (struct v4l2_requestbuffers));
    v4l2reqbuf.memory = V4L2_MEMORY_MMAP;

    if (EncoderPort == 0)
    {
        v4l2reqbuf.count = encoderParams->num_input_buffers;
        nb_buffer = encoderParams->num_input_buffers;
        v4l2reqbuf.type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
        if (!encoderParams->isInputPortBufferAllocated)
        {
            v4l2reqbuf.memory = V4L2_MEMORY_USERPTR;
        }
        OMX_DBGT_PTRACE("Allocating memory for internal input structure, isInputPortBufferAllocated : %d",
            encoderParams->isInputPortBufferAllocated);

        encoderParams->input_buffers = (buffer_t *)calloc (encoderParams->num_input_buffers,sizeof (*encoderParams->input_buffers));
    }
    else
    {
        v4l2reqbuf.count = encoderParams->num_output_buffers;
        v4l2reqbuf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        nb_buffer = encoderParams->num_output_buffers;
        if (!encoderParams->isOutputPortBufferAllocated)
        {
            v4l2reqbuf.memory = V4L2_MEMORY_USERPTR;
        }
        OMX_DBGT_PTRACE("Allocating memory for internal output structure, isOutputPortBufferAllocated : %d",
            encoderParams->isOutputPortBufferAllocated);;

        encoderParams->output_buffers = (buffer_t *)calloc (encoderParams->num_output_buffers,sizeof (*encoderParams->output_buffers));     
    }
    OMX_DBGT_PTRACE("Abt to call VIDIOC_REQBUFS for port %d buff cnt : %d memory type : 0x%x",
        EncoderPort,
        v4l2reqbuf.count,
        (unsigned int)v4l2reqbuf.memory);

    ret = ioctl(v4l_fd, VIDIOC_REQBUFS, &v4l2reqbuf);
    if (ret < 0)
    {
        OMX_DBGT_ERROR("Cannot do VIDIOC_REQBUFS for EncoderPort : %u and return Value : %d Msg (%s)",
            EncoderPort, ret, strerror (errno));
        return ret;
    }
    OMX_DBGT_PTRACE("After VIDIOC_REQBUFS, buffercount from SE :%d , nBufferCountActual : %d for port : %d",
        v4l2reqbuf.count,
        nb_buffer,
        EncoderPort);
    if (v4l2reqbuf.count != nb_buffer)
    {
        OMX_DBGT_ERROR("Error in VIDIOC_REQBUFS, buffers in Use (%d) and Required (%d) for Port : %d",
            nb_buffer, v4l2reqbuf.count, EncoderPort);
        return -1;
    }

    OMX_DBGT_EPILOG();
    return ret;
}

//=====================================================================================
// V4LAudioEncode::setUpBufferPointer
//=====================================================================================
int V4LAudioEncode::setUpBufferPointer()
{
    int ret = -1;
    uint32_t i=0;
    OMX_DBGT_PROLOG();
    if (EncoderPort == 0)
    {

        /* Query input buffers and map buffers */
        for (i = 0; i < encoderParams->num_input_buffers; i++)
        {
            struct buffer_t *inputBuffers_p = &(encoderParams->input_buffers[i]);
            inputBuffers_p->start = (void *) -1;
            memset (&inputBuffers_p->v4l2_buf, 0,sizeof (struct v4l2_buffer));
            inputBuffers_p->v4l2_buf.type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
            inputBuffers_p->v4l2_buf.index = i;
            inputBuffers_p->v4l2_buf.memory = V4L2_MEMORY_MMAP;

            /* query buffer */
            ret = ioctl(v4l_fd, VIDIOC_QUERYBUF, &inputBuffers_p->v4l2_buf);
            if (ret < 0)
            {
                OMX_DBGT_ERROR("query input buffers failed - %s", strerror (errno));
                return -1;
            }
            OMX_DBGT_PINFO("Sucess for VIDIOC_QUERYBUF for input length : %d",(int)inputBuffers_p->v4l2_buf.length);
            inputBuffers_p->length = inputBuffers_p->v4l2_buf.length;
            inputBuffers_p->isUsed = false;
            inputBuffers_p->start  = (unsigned char *)mmap(0, inputBuffers_p->v4l2_buf.length,
                                                        PROT_READ | PROT_WRITE, MAP_SHARED, v4l_fd,
                                                        inputBuffers_p->v4l2_buf.m.offset);

            OMX_DBGT_PINFO( "Buffer for input start address : 0x%x , %d",
                            (unsigned int)inputBuffers_p->start,
                            inputBuffers_p->v4l2_buf.m.offset);

            if (inputBuffers_p->start == ((void *) -1))
            {
                OMX_DBGT_ERROR("mmap buffer failed for input");
                return -1;
            }
        }
    }
    else
    {
        /* Query output buffers and map buffers */
        for (i = 0; i < encoderParams->num_output_buffers; i++)
        {
            struct buffer_t *outputBuffers_p = &(encoderParams->output_buffers[i]);
            outputBuffers_p->start = (void *) -1;
            memset (&outputBuffers_p->v4l2_buf, 0,sizeof (struct v4l2_buffer));
            outputBuffers_p->v4l2_buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
            outputBuffers_p->v4l2_buf.index = i;
            outputBuffers_p->v4l2_buf.memory = V4L2_MEMORY_MMAP;

            /* query buffer */
            ret = ioctl(v4l_fd, VIDIOC_QUERYBUF,&outputBuffers_p->v4l2_buf);
            if (ret < 0)
            {
                OMX_DBGT_ERROR("query output buffers failed - %s", strerror (errno));
                return -1;
            }

            OMX_DBGT_PINFO("sucess for output VIDIOC_QUERYBUF length : %d",
                (int)outputBuffers_p->v4l2_buf.length);

            outputBuffers_p->length = outputBuffers_p->v4l2_buf.length;
            outputBuffers_p->isUsed = false;
            outputBuffers_p->start  = (unsigned char *)mmap(0, outputBuffers_p->v4l2_buf.length,
                                PROT_READ | PROT_WRITE, MAP_SHARED, v4l_fd,
                                outputBuffers_p->v4l2_buf.m.offset);

            OMX_DBGT_PINFO( "Buffer for output : 0x%x , offset : %d",
                            (unsigned int)outputBuffers_p->start,
                            outputBuffers_p->v4l2_buf.m.offset);

            if (outputBuffers_p->start == ((void *) -1))
            {
                OMX_DBGT_ERROR("mmap buffer failed for output");
                return -1;
            }
        }
    }
    OMX_DBGT_EPILOG();
    return ret;
}

//=====================================================================================
// V4LAudioEncode::freeBufferPointer
//=====================================================================================
int V4LAudioEncode::freeBufferPointer()
{
    int ret = -1;
    uint32_t i=0;
    OMX_DBGT_PROLOG("For Port : %d",EncoderPort);
    if (EncoderPort == 0)
    {
        if (encoderParams->isInputPortBufferAllocated && encoderParams->input_buffers)
        {
            /* Query input buffers and map buffers */
            for (i = 0; i < encoderParams->num_input_buffers; i++)
            {
                struct buffer_t *inputBuffers_p = &(encoderParams->input_buffers[i]);
                ret = -1;
                OMX_DBGT_PTRACE("input_buffers[%d].start : 0x%x length : %d", i,
                                (unsigned int)inputBuffers_p->start,
                                inputBuffers_p->v4l2_buf.length);
                if (inputBuffers_p->start != ((void *) -1))
                {
                    OMX_DBGT_PINFO("abt todo unmap for input_buffers[%d].start : 0x%x", i,
                                (unsigned int)inputBuffers_p->start);
                    munmap (inputBuffers_p->start, inputBuffers_p->v4l2_buf.length);
                    inputBuffers_p->start = ((void *) -1);
                    ret = 0;
                }
            }
        }
		if (encoderParams->input_buffers)
		{
			OMX_DBGT_PDEBUG("Freeing input buffer memory");
            free(encoderParams->input_buffers);
            encoderParams->input_buffers = NULL;
		}
		else
		{
			OMX_DBGT_PDEBUG("No input buffer memory");
			ret = 0;
		}
    }
    else
    {
        if (encoderParams->isOutputPortBufferAllocated && encoderParams->output_buffers)
        {
            /* Query input buffers and map buffers */
            for (i = 0; i < encoderParams->num_output_buffers; i++)
            {
                struct buffer_t *outputBuffers_p = &(encoderParams->output_buffers[i]);
                ret = -1;
                OMX_DBGT_PTRACE("output_buffers[%d].start : 0x%x length : %d", i,
                                (unsigned int)outputBuffers_p->start,
                outputBuffers_p->v4l2_buf.length);
                if (outputBuffers_p->start != ((void *) -1))
                {
                    OMX_DBGT_PINFO("abt todo unmap for output_buffers[%d].start : 0x%x", i,
                                    (unsigned int)outputBuffers_p->start);
                    munmap (outputBuffers_p->start, outputBuffers_p->v4l2_buf.length);
                    outputBuffers_p->start = ((void *) -1);
                    ret = 0;
                }
            }
        }

        if (encoderParams->output_buffers)
        {
            OMX_DBGT_PDEBUG("Freeing output buffer memory");
            free(encoderParams->output_buffers);
            encoderParams->output_buffers = NULL;
        }
        else
        {
            OMX_DBGT_PDEBUG("No output buffer memory");
            ret = 0;
        }
    }
    OMX_DBGT_EPILOG();
    return ret;
}

//=====================================================================================
// V4LAudioEncode::setParam
//=====================================================================================
int V4LAudioEncode::setParam()
{
    OMX_DBGT_PROLOG("For Port : %d ",EncoderPort);
    int ret = 0;
    //todo
    OMX_DBGT_EPILOG();
    return ret;
}

int V4LAudioEncode::setControls()
{
    uint32_t index=0, i;
    struct v4l2_ext_controls ext_ctrls;
    struct v4l2_ext_control  ext_ctrl[AUD_ENCODE_MAX_CTRL];
    OMX_DBGT_PROLOG();

    if (EncoderPort != 1)
    {
        OMX_DBGT_ERROR("setControls called for wrong encoder Port : %d",EncoderPort);
        return -1;
    }

	/* manually initialize the control list */
	/* all the controls to set are extended controls from the MPEG class */
    memset(&ext_ctrls, 0, sizeof(struct v4l2_ext_controls));
    ext_ctrls.ctrl_class = V4L2_CTRL_CLASS_MPEG;
    ext_ctrls.count      = 0;
    ext_ctrls.controls   = ext_ctrl;

	// core_format
	// sampling freq
    ext_ctrl[index].id = V4L2_CID_MPEG_AUDIO_SAMPLING_FREQ;
    ext_ctrl[index].value = encoderParams->sampling_freq;
    index++;

    // channel count
    ext_ctrl[index].id = V4L2_CID_MPEG_STM_AUDIO_CHANNEL_COUNT;
    ext_ctrl[index].value = encoderParams->channel_count;
    OMX_DBGT_PINFO("Port : %d , V4L2_CID_MPEG_STM_AUDIO_CHANNEL_COUNT : %d",EncoderPort, encoderParams->channel_count);
    index++;

	// channel 0..31
	for(i = 0; i<encoderParams->channel_count; i++)
	{
		ext_ctrl[index].id    = V4L2_CID_MPEG_STM_AUDIO_CHANNEL_MAP;
		ext_ctrl[index].value = (i << 8) | encoderParams->channel[i];
		OMX_DBGT_PINFO("Port : %d , V4L2_CID_MPEG_STM_AUDIO_CHANNEL_MAP[%d] : %d",EncoderPort, i, ext_ctrl[index].value);
		index++;
	}

	// apply core_format
	ext_ctrl[index].id    = V4L2_CID_MPEG_STM_AUDIO_CORE_FORMAT;
	ext_ctrl[index].value = 1;
	index++;

	ext_ctrls.count = index;

	if (ioctl(v4l_fd, VIDIOC_S_EXT_CTRLS, &ext_ctrls) < 0) {
		OMX_DBGT_ERROR("Error in extended controls not Supported 1\n");
		return -1;
	}

	// bitrate control
	index = 0;

	ext_ctrl[index].id    = V4L2_CID_MPEG_STM_AUDIO_BITRATE_MODE;
	ext_ctrl[index].value = encoderParams->bitrate_mode; // CBR, VBR
	OMX_DBGT_PINFO("Port : %d , V4L2_CID_MPEG_STM_AUDIO_BITRATE_MODE : %d",EncoderPort, ext_ctrl[index].value);
	index++;

    // Only AAC Encoder supported for now
	if(encoderParams->codec == V4L2_MPEG_AUDIO_STM_ENCODING_AAC)
		ext_ctrl[index].id    = V4L2_CID_MPEG_AUDIO_AAC_BITRATE;

	ext_ctrl[index].value = encoderParams->bitrate;
	OMX_DBGT_PINFO("Port : %d , V4L2_CID_MPEG_STM_AUDIO_AAC_BITRATE : %d",EncoderPort, ext_ctrl[index].value);
	index++;

	ext_ctrl[index].id    = V4L2_CID_MPEG_STM_AUDIO_VBR_QUALITY_FACTOR;
	ext_ctrl[index].value = encoderParams->vbr_qfactor; //100;
	OMX_DBGT_PINFO("Port : %d , V4L2_CID_MPEG_STM_AUDIO_VBR_QUALITY_FACTOR : %d",EncoderPort, ext_ctrl[index].value);
	index++;

	ext_ctrl[index].id    = V4L2_CID_MPEG_STM_AUDIO_BITRATE_CAP;
	ext_ctrl[index].value = encoderParams->bitrate_cap;; // 0
	OMX_DBGT_PINFO("Port : %d , V4L2_CID_MPEG_STM_AUDIO_BITRATE_CAP : %d",EncoderPort, ext_ctrl[index].value);
	index++;

	ext_ctrl[index].id    = V4L2_CID_MPEG_STM_AUDIO_BITRATE_CONTROL;
	ext_ctrl[index].value = 1;
	index++;

	ext_ctrls.count = index;

	if (ioctl(v4l_fd, VIDIOC_S_EXT_CTRLS, &ext_ctrls) < 0) {
		OMX_DBGT_ERROR("Error in extended controls not Supported 2\n");
		return -1;
	}

	// other control
	index = 0;

	ext_ctrl[index].id    = V4L2_CID_MPEG_STM_AUDIO_PROGRAM_LEVEL;
	ext_ctrl[index].value = encoderParams->prog_level; //
	OMX_DBGT_PINFO("Port : %d , V4L2_CID_MPEG_STM_AUDIO_PROGRAM_LEVEL : %d",EncoderPort, ext_ctrl[index].value);
	index++;

	ext_ctrl[index].id    = V4L2_CID_MPEG_STM_AUDIO_SERIAL_CONTROL;
	ext_ctrl[index].value = V4L2_MPEG_AUDIO_NO_COPYRIGHT;
	index++;

	ext_ctrls.count = index;

	if (ioctl(v4l_fd, VIDIOC_S_EXT_CTRLS, &ext_ctrls) < 0) {
		OMX_DBGT_ERROR("Error in extended controls not Supported 3\n");
		return -1;
	}

    OMX_DBGT_EPILOG();
    return 0;
}

//=====================================================================================
// V4LAudioEncode::Configure
//=====================================================================================
int V4LAudioEncode::Configure()
{
    struct v4l2_format          format;
    struct v4l2_audenc_format *aud_fmt;
    uint32_t index=0;
    int32_t i = 0;
    OMX_DBGT_PROLOG(" port : %d", EncoderPort);

    memset (&format, 0, sizeof (struct v4l2_format));
    aud_fmt = (struct v4l2_audenc_format *)format.fmt.raw_data;

    if (EncoderPort == 0)
    {
        /* Set input format */
        format.type  = V4L2_BUF_TYPE_VIDEO_OUTPUT;
        assert(m_nEncoding ==  OMX_AUDIO_CodingPCM);
        aud_fmt->codec = V4L2_MPEG_AUDIO_STM_ENCODING_PCM;
        aud_fmt->max_channels = encoderParams->src_max_channels;
        OMX_DBGT_PINFO(" codec : %x : max_channels : %d", aud_fmt->codec, aud_fmt->max_channels);
    } else if (EncoderPort == 1)
    {
        /* Set output format */
        format.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        assert(m_nEncoding !=  OMX_AUDIO_CodingPCM);
        aud_fmt->codec = encoderParams->codec;
        aud_fmt->max_channels = encoderParams->max_channels;

        /* Check whether the desired format is available */
        while ((i < nbFormats) && (formats[i].pixelformat != encoderParams->codec)) 
        {
            i++;
        }
    }
    
    /* freeing the memory here */
    if (formats) 
    {
        free(formats);
        formats = NULL;
    }

    if(EncoderPort == 1)
    {
        if (i == nbFormats) {
            OMX_DBGT_ERROR(" desired format %x for capture queue is not supported at Port : %d",
                encoderParams->codec, EncoderPort);
            return -1;
        }
        OMX_DBGT_PINFO(" desired format %x for capture queue is supported at Port : %d",
            encoderParams->codec, EncoderPort);
    }

    if (ioctl (v4l_fd, VIDIOC_S_FMT, &format) < 0)
    {
        OMX_DBGT_ERROR("Set Format (VIDIOC_S_FMT) failed (%s)", strerror (errno));
        OMX_DBGT_EPILOG();
        return -1;
    }
    else
    {
        OMX_DBGT_PINFO(" Set Format (VIDIOC_S_FMT) success at Port : %d", EncoderPort);
    }

    OMX_DBGT_EPILOG();
    return 0;
}

//=====================================================================================
// V4LAudioEncode::Start
//=====================================================================================
int V4LAudioEncode::start() const
{
    int     buftype;

    OMX_DBGT_PROLOG("Value of v4l_fd : %d for Port : %d",v4l_fd,EncoderPort);

    if (EncoderPort == 0)
    {
        buftype                         = (int)V4L2_BUF_TYPE_VIDEO_OUTPUT;
    }
    else
    {
        buftype                         = (int)V4L2_BUF_TYPE_VIDEO_CAPTURE;
    }
    OMX_DBGT_PINFO("To call ioctl value of v4l_fd : %d for Port : %d",v4l_fd,EncoderPort);
    if (ioctl (v4l_fd, VIDIOC_STREAMON, &buftype) < 0)
    {
        OMX_DBGT_ERROR("Switching stream on (VIDIOC_STREAMON) failed (%s)", strerror (errno));
        OMX_DBGT_EPILOG();
        return -1;
    }
    this->state(stm::IDvbDevice::EState_Start);

    OMX_DBGT_EPILOG();
    return 0;
}

//=====================================================================================
// V4LAudioEncode::Stop
//=====================================================================================
int V4LAudioEncode::stop() const
{
    struct v4l2_buffer          Buffer;

    OMX_DBGT_PROLOG();

    memset (&Buffer, 0, sizeof (struct v4l2_buffer));
    if (EncoderPort == 0)
    {
        Buffer.type                         = V4L2_BUF_TYPE_VIDEO_OUTPUT;
    }
    else
    {
        Buffer.type                         = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    }
    if (ioctl (v4l_fd, VIDIOC_STREAMOFF, &Buffer.type) < 0)
    {
        OMX_DBGT_ERROR("Switching stream on (VIDIOC_STREAMOFF) failed (%s)", strerror (errno));
        OMX_DBGT_EPILOG();
        return -1;
    }
    this->state(stm::IDvbDevice::EState_Stop);

    OMX_DBGT_EPILOG();
    return 0;
}

//==============================================================================
// V4LAudioEncode::flush
//==============================================================================
int V4LAudioEncode::flush(bool bEoS) const
{
    OMX_DBGT_PROLOG();

    OMX_DBGT_EPILOG();
    return 0;
}

V4LAudioEncode::~V4LAudioEncode()
{}

//==============================================================================
// V4LAudioEncode::FlushBuffer (empty function)
//==============================================================================
unsigned long V4LAudioEncode::FlushBuffer() const
{
    assert(0);
    return 0;
}

//=====================================================================================
// V4LAudioEncode::QueueBuffer
//=====================================================================================
int V4LAudioEncode::QueueBuffer(void* BufferData, int &nBytesSize, uint64_t &ts) const
{
    struct buffer_t *internalBuffer;
    int retVal = -1;

    OMX_DBGT_PROLOG();

    if (EncoderPort  == 0 || EncoderPort ==1)
    {
        internalBuffer = (struct buffer_t *)BufferData;
        internalBuffer->v4l2_buf.memory = V4L2_MEMORY_MMAP;

        if (EncoderPort == 0)
        {
            internalBuffer->v4l2_buf.bytesused = nBytesSize;
            internalBuffer->v4l2_buf.type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
            internalBuffer->v4l2_buf.reserved  = (uint32_t)&encoderParams->src_metadata;
            OMX_DBGT_PDEBUG("src_metadata freq : %d, channel_count : %d, format : %d, ch[0]: %x, ch[1] : %x",
                encoderParams->src_metadata.sample_rate,
                encoderParams->src_metadata.channel_count,
                encoderParams->src_metadata.sample_format,
                encoderParams->src_metadata.channel[0],
                encoderParams->src_metadata.channel[1]);
        }
        else
        {
            internalBuffer->v4l2_buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
            memset(&encoderParams->dst_metadata, 0, sizeof(encoderParams->dst_metadata));
            internalBuffer->v4l2_buf.reserved  = (uint32_t)&encoderParams->dst_metadata;
            internalBuffer->v4l2_buf.bytesused = 0;
        }
        internalBuffer->v4l2_buf.field = V4L2_FIELD_ANY;

        OMX_DBGT_PINFO("queue buffer v4l2 for port:%d type:%d, memory:%d index:%d start_addr:0x%x len:%d filledlen:%d offset:%d",
                    EncoderPort,
                    (int)internalBuffer->v4l2_buf.type,
                    (int)internalBuffer->v4l2_buf.memory,
                    (int)internalBuffer->v4l2_buf.index,
                    (unsigned int)internalBuffer->start,
                    (int)internalBuffer->v4l2_buf.length,
                    (unsigned int)internalBuffer->v4l2_buf.bytesused,
                    (int)internalBuffer->v4l2_buf.m.offset);

        retVal = ioctl(v4l_fd, VIDIOC_QBUF, &internalBuffer->v4l2_buf);
        if (retVal < 0)
        {
            OMX_DBGT_ERROR("VIDIOC_QBUF failed for output- %s on port %d",strerror (errno), EncoderPort);
        }
        internalBuffer->isUsed = true;
    }
    else
    {
        OMX_DBGT_CRITICAL("Wrong encoder Port : %d",EncoderPort);
    }
    OMX_DBGT_EPILOG();
    return retVal;
}

//=====================================================================================
// V4LAudioEncode::DequeueEncoderBuffer
//=====================================================================================
int V4LAudioEncode::DequeueEncoderBuffer(void* BufferData)
{
    struct buffer_t *internalBuffer;
    OMX_DBGT_PROLOG();

    internalBuffer = (struct buffer_t *)BufferData;
    if (ioctl (v4l_fd, VIDIOC_DQBUF, &internalBuffer->v4l2_buf) < 0)
    {
        OMX_DBGT_ERROR("Failed to dequeue buffer (VIDIOC_DQBUF) (%s) on Port : %d", strerror (errno), EncoderPort);
        OMX_DBGT_EPILOG();
        return -1;
    }

    OMX_DBGT_PINFO("dequeue buffer v4l2 for port : %d type  : %d, memory : %d index : %d start addr 0x%x len : %d filledlen : %d",
                    EncoderPort,
                    (int)internalBuffer->v4l2_buf.type,
                    (int)internalBuffer->v4l2_buf.memory,
                    (int)internalBuffer->v4l2_buf.index,
                    (unsigned int)internalBuffer->start,
                    (int)internalBuffer->length,
                    (unsigned int)internalBuffer->v4l2_buf.bytesused);

    OMX_DBGT_EPILOG();
    return 0;
}

const char* V4LAudioEncode::name() const
{ return m_deviceName; }

int V4LAudioEncode::id() const
{ return v4l_fd; }

void V4LAudioEncode::state(stm::IDvbDevice::EState st) const
{
    m_nState = st;
    m_condState.signal();
}

stm::IDvbDevice::EState V4LAudioEncode::state() const
{ return m_nState; }

//==============================================================================
// V4LAudioEncode::clear
//==============================================================================
int V4LAudioEncode::clear() const
{
    OMX_DBGT_PROLOG();
    assert(isOpen());
    if (this->isStopped()) {
        OMX_DBGT_EPILOG("Clear on a stopped device");
        return 0;
    }
    // Flush the current stream from the pipeline.  This is accomplished
    // by draining the stream with discard and indicating that a
    // discontinuity will occur
    if (ioctl(id(), VIDEO_CLEAR_BUFFER, NULL) != 0) {
        OMX_DBGT_ERROR("VIDEO_CLEAR_BUFFER ioctl failed (%s)", strerror (errno));
        OMX_DBGT_EPILOG();
        return -1;
    }

    this->state(this->state());
    OMX_DBGT_EPILOG();
    return 0;
}
