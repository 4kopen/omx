/*
 * Copyright 2011 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *
 * File   ::   audencoder.hpp
 * Author ::   Kausik Maiti(kausik.maiti@st.com)
 *
 */

#ifndef ST_OMX_AUDIO_ENCODER_H
#define ST_OMX_AUDIO_ENCODER_H

#define NUM_IN_BUFFERS 2
#define NUM_OUT_BUFFERS 3
#define MAX_INPUT_OUTPUT_BUFFERS 20

struct buffer_t {
    struct v4l2_buffer v4l2_buf;
    void *start;
    void *start_phys;
    bool isUsed;
    unsigned int length;
};

typedef struct {
    struct buffer_t *internalBuffer;
    OMX_BUFFERHEADERTYPE *omxBuffer;
    OMX_BOOL isAssociated;
    OMX_U8 buffProperty;
} EncoderBufferMapping;

typedef struct{
    int audio_init;

    /* input stream config */
    int byte_per_sample;

    /* src meta data */
    int src_max_channels;
    struct v4l2_audenc_src_metadata src_metadata;
    struct v4l2_audenc_dst_metadata dst_metadata;

    /* audio controls */
    uint32_t codec;
    int max_channels;

    int bitrate_mode;
    int bitrate;
    int vbr_qfactor;
    int bitrate_cap;

    int sampling_freq;
    uint32_t channel_count;
    int channel[V4L2_STM_AUDENC_MAX_CHANNELS];

    int prog_level;
    struct buffer_t *input_buffers;
    bool input_buf_allocated;
    struct buffer_t *output_buffers;
    bool output_buf_allocated;
    uint32_t inputBufferSize;
    uint32_t outputBufferSize;
    uint32_t num_output_buffers;
    uint32_t num_input_buffers;
    OMX_BOOL isOutputPortBufferAllocated;
    OMX_BOOL isInputPortBufferAllocated;
} AUDEncoderParams_t;

typedef  enum  {
    VLAudio_CodingAAC = 0,
    VLAudio_CodingOTHER
} VLAudio_CODINGTYPE;

#endif /* ST_OMX_AUDIO_ENCODER_H */
