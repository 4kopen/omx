/*
 * Copyright 2013 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file   AudioEncoder.hpp
 * @author Kausik Maiti(kausik.maiti@st.com)
 */

#ifndef _STM_AUDIOENCODER_HPP_
#define _STM_AUDIOENCODER_HPP_

#include "StmOmxComponent.hpp"
#include "AudioEncoderPort.hpp"
#include "Role.hpp"
#include "v4laudioenc.hpp"

namespace stm {


class AENCComponent: public OmxComponent
{
public:
    AENCComponent(OMX_HANDLETYPE hComponent);
    virtual ~AENCComponent();

    virtual OMX_ERRORTYPE create();
    virtual void destroy();
    virtual const AudEncPort& getPort(unsigned int nIdx) const;

    friend class AudEncInputPort;
    friend class AudEncOutputPort;

    virtual OMX_ERRORTYPE getParameter(
                  OMX_INDEXTYPE       nParamIndex,
                  OMX_PTR             ComponentParameterStructure);
    virtual OMX_ERRORTYPE setParameter(
                  OMX_INDEXTYPE       nParamIndex,
                  OMX_PTR             ComponentParameterStructure);
    virtual
    OMX_ERRORTYPE getConfig(OMX_INDEXTYPE nIndex, OMX_PTR pConfStruct);

    virtual
    OMX_ERRORTYPE setConfig(OMX_INDEXTYPE nIndex, OMX_PTR pConfStruct);
#if 0
    virtual OMX_ERRORTYPE getExtensionIndex(
                  OMX_STRING          cParameterName,
                  OMX_INDEXTYPE *     pIndexType);
#endif
    virtual OMX_ERRORTYPE emptyThisBuffer(OMX_BUFFERHEADERTYPE*   pBufferHdr);
    virtual OMX_ERRORTYPE fillThisBuffer(OMX_BUFFERHEADERTYPE*   pBufferHdr);

    const AudEncInputPort& inputPort() const;
    const AudEncOutputPort& outputPort() const;
    bool IsConfigureDone;
    mutable Mutex m_mutexCmd;
    mutable Condition   m_condConfig;

protected:
    virtual AudEncPort& getPort(unsigned int nIdx);

    virtual OMX_ERRORTYPE configure();
    virtual OMX_ERRORTYPE start();
    virtual void stop();
    virtual void execute();
    virtual OMX_ERRORTYPE flush(unsigned int nPortIdx);

    OMX_ERRORTYPE setRole(const char* pNewRole);
public:
    OMX_AUDIO_CODINGTYPE getCompressionFormat(unsigned int nIdx) const;
    AUDEncoderParams_t **getEncoderParam();

//    OMX_BOOL isMetaDataSet() const;

private:
    AudEncInputPort& inputPort_();
    AudEncOutputPort& outputPort_();

    Role& role();

private:

    AudEncInputPort      m_inputPort;
    AudEncOutputPort     m_outputPort;

    Role                 m_componentRole;
    AUDEncoderParams_t *m_AUDEncoderParams;
    EncoderBufferMapping *m_nbBufferMapping;
};

inline AUDEncoderParams_t **AENCComponent::getEncoderParam()
{ return &m_AUDEncoderParams; }

inline const AudEncInputPort& AENCComponent::inputPort() const
{ return m_inputPort; }

inline const AudEncOutputPort& AENCComponent::outputPort() const
{ return m_outputPort; }

inline AudEncInputPort& AENCComponent::inputPort_()
{ return m_inputPort; }

inline AudEncOutputPort& AENCComponent::outputPort_()
{ return m_outputPort; }

inline Role&  AENCComponent::role()
{ return m_componentRole; }


#define OMX_ST_AUDIO_INPUT_PORT 0
#define OMX_ST_AUDIO_OUTPUT_PORT 1
#define OMX_ST_AUDIO_PORT_NUMBER 2



} // eof namespace stm

#endif  // _STM_VIDEOENCODER_HPP_
