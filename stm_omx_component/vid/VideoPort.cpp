/*
 * Copyright 2013 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file   VideoPort.cpp
 * @author STMicroelectronics
 */

#include "StmOmxIComponent.hpp"
#include "VideoPort.hpp"
#include "OMX_debug.h"

#if HAVE_ASSERT_H
#include <assert.h>
#endif

#if HAVE_DBGTASSERT_H
// Re-direct assert on DBGT_ASSERT macro
#define assert(expr)  OMX_DBGT_ASSERT(expr)
#endif
#include <linux/dbgt.h>


#include <string.h>


namespace stm {

//=============================================================================
// VideoPort implementation
//=============================================================================

OmxVideoPort::OmxVideoPort(unsigned int nIdx, OMX_DIRTYPE eDir,
                           const OmxComponent& owner)
    :OmxPort(),
     m_owner(owner),
     m_PortDefinition(nIdx, eDir)
{
    // NV 12
    m_ColorFormat[0] = OMX_COLOR_FormatYUV420SemiPlanar;
    // BGRA8888
    m_ColorFormat[1] = OMX_COLOR_Format32bitBGRA8888;
    // RGB565
    m_ColorFormat[2] = OMX_COLOR_Format16bitRGB565;

    // Set default as NV12
    this->definition().colorFormat(OMX_COLOR_FormatYUV420SemiPlanar);
    //check the string length before copy
    assert(strlen(this->component().name()) <= sizeof(m_strName));
    strcpy(m_strName,this->component().name());
    OMXDEBUG_UPDATE_AV_TRACE_SETTINGS(this->component().name());
}

OmxVideoPort::~OmxVideoPort()
{}

OMX_ERRORTYPE OmxVideoPort::create()
{ return OMX_ErrorNone; }

void OmxVideoPort::destroy()
{}

const VideoPortDefinition& OmxVideoPort::definition() const
{ return m_PortDefinition; }

VideoPortDefinition& OmxVideoPort::definition()
{ return m_PortDefinition; }

const OmxComponent& OmxVideoPort::component() const
{ return m_owner; }


OMX_COLOR_FORMATTYPE
OmxVideoPort::supportedColorFormat(unsigned int nIdx) const
{
    assert(nIdx < this->supportedColorFormatNb());
    return m_ColorFormat[nIdx];
}

bool OmxVideoPort::isSupportedColorFormat(OMX_COLOR_FORMATTYPE aFmt) const
{
    bool ret = false;
    for (unsigned int i = 0; i < this->supportedColorFormatNb(); i++)
    {
        if (this->supportedColorFormat(i) == aFmt) {
            ret = true;
            break;
        }
    }
    return ret;
}

OMX_ERRORTYPE
OmxVideoPort::getParameter(OMX_INDEXTYPE nIndex, OMX_PTR pParamStruct)
{
    OMX_DBGT_PROLOG();
    switch (nIndex)
    {
    case OMX_IndexParamPortDefinition: {
        // Port definition
        ASSERT_STRUCT_TYPE(pParamStruct, OMX_PARAM_PORTDEFINITIONTYPE);
        OMX_PARAM_PORTDEFINITIONTYPE* param;
        param = (OMX_PARAM_PORTDEFINITIONTYPE*)pParamStruct;
        assert(param->nPortIndex == this->definition().index());
        *param = this->definition().getParamPortDef();
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }

    case OMX_IndexParamVideoPortFormat: {
        // Supported video port format
        ASSERT_STRUCT_TYPE(pParamStruct, OMX_VIDEO_PARAM_PORTFORMATTYPE);
        OMX_VIDEO_PARAM_PORTFORMATTYPE* param;
        param = (OMX_VIDEO_PARAM_PORTFORMATTYPE*)pParamStruct;
        assert(param->nPortIndex == this->definition().index());
        if (param->nIndex < supportedColorFormatNb()) {
            param->eCompressionFormat = OMX_VIDEO_CodingUnused;
            param->eColorFormat = supportedColorFormat(param->nIndex);
            OMX_DBGT_EPILOG();
            return OMX_ErrorNone;
        } else {
            DBGT_EPILOG("No more supported format");
            return OMX_ErrorNoMore;
        }
    }

    case OMX_IndexParamOtherPortFormat:
    case OMX_IndexParamAudioPortFormat:
    case OMX_IndexParamImagePortFormat: {
        // Unsupported port format
        OMX_DBGT_EPILOG("Unsupported index");
        return OMX_ErrorUnsupportedIndex;
    }

    default: {
        OMX_DBGT_EPILOG("Unsupported index");
        return OMX_ErrorUnsupportedIndex;
    }
    }
}

OMX_ERRORTYPE
OmxVideoPort::setParameter(OMX_INDEXTYPE nIndex, OMX_PTR pParamStruct)
{
    OMX_DBGT_PROLOG();
    switch (nIndex)
    {
    case OMX_IndexParamPortDefinition: {
        // Port definition
        ASSERT_STRUCT_TYPE(pParamStruct, OMX_PARAM_PORTDEFINITIONTYPE);
        OMX_PARAM_PORTDEFINITIONTYPE* param;
        param = (OMX_PARAM_PORTDEFINITIONTYPE*)pParamStruct;
        assert(param->nPortIndex == this->definition().index());
        this->definition().setParamPortDef(param);
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }

    case OMX_IndexParamVideoPortFormat: {
        // Set the a default video port format
        ASSERT_STRUCT_TYPE(pParamStruct, OMX_VIDEO_PARAM_PORTFORMATTYPE);
        OMX_VIDEO_PARAM_PORTFORMATTYPE* param;
        param = (OMX_VIDEO_PARAM_PORTFORMATTYPE*)pParamStruct;
        assert(param->nPortIndex == this->definition().index());
        // Check color format
        if (!isSupportedColorFormat(param->eColorFormat)) {
            OMX_DBGT_ERROR("eColorFormat %u not supported", param->eColorFormat);
            OMX_DBGT_EPILOG("Unsupported video color format");
            return OMX_ErrorUnsupportedSetting;
        }
        // Color format is supported
        this->definition().colorFormat(param->eColorFormat);
        OMX_DBGT_EPILOG("Set port color format %u", param->eColorFormat);
        return OMX_ErrorNone;
    }

    case OMX_IndexParamOtherPortFormat:
    case OMX_IndexParamAudioPortFormat:
    case OMX_IndexParamImagePortFormat: {
        // Unsupported port format
        OMX_DBGT_EPILOG("Unsupported index");
        return OMX_ErrorUnsupportedIndex;
    }

    default: {
        OMX_DBGT_EPILOG("Unsupported index");
        return OMX_ErrorUnsupportedIndex;
    }
    }
}

OMX_ERRORTYPE
OmxVideoPort::emptyThisBuffer(OMX_BUFFERHEADERTYPE* pBuffer)
{
    assert(this->isTunneled() == false);
    return OMX_ErrorNone;
}

OMX_ERRORTYPE
OmxVideoPort::fillThisBuffer(OMX_BUFFERHEADERTYPE* pBuffer)
{
    assert(this->isTunneled() == false);
    return OMX_ErrorNone;
}

} // eof namespace stm
