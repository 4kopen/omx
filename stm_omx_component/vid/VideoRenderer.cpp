/*
 * Copyright 2013 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file   VideoRenderer.cpp
 * @author STMicroelectronics
 */

#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <linux/videodev2.h>
#include <linux/dvb/dvb_v4l2_export.h>
#include <utils/v4l2_helper.h>
#include <linux/stmvout.h>
#include "VideoRenderer.hpp"
#include <OMX_debug.h>

#if HAVE_ASSERT_H
#include <assert.h>
#endif

#if HAVE_DBGTASSERT_H
// Re-direct assert on DBGT_ASSERT macro
#define assert(expr)  OMX_DBGT_ASSERT(expr)
#endif

#define DBGT_DECLARE_AUTOVAR
#define DBGT_PREFIX "OMX "
#define DBGT_LAYER 0
#include <linux/dbgt.h>

#define V4L2_OVERLAY_DRIVER_NAME    "AV Decoder"
#define V4L2_OVERLAY_CARD_NAME      "STMicroelectronics"

#define V4L2_DISPLAY_DRIVER_NAME    "Planes"
#define V4L2_DISPLAY_CARD_NAME      "STMicroelectronics"
#define VIDIO_OUTPUT_NAME           "Main-VID"

// Define Statistics file path for QOS
#define PATH_FRAME_COUNT_TO_CODEC "/sys/class/streaming_engine/dvb0.video0/frame_count_to_codec"
#define PATH_FRAME_COUNT_MANIFESTED "/sys/class/streaming_engine/dvb0.video0/frame_count_manifested"
#define PATH_DROPPED_BEFORE_MANIFESTATION_TOTAL "/sys/class/streaming_engine/dvb0.video0/dropped_before_manifestation_total"
#define PATH_DROPPED_BEFORE_OUTPUT_TIMING_TOTAL "/sys/class/streaming_engine/dvb0.video0/dropped_before_output_timing_total"
#define PATH_DROPPED_BEFORE_DECODE_TOTAL "/sys/class/streaming_engine/dvb0.video0/dropped_before_decode_total"
#define PATH_DROPPED_BEFORE_DECODE_WINDOW_TOTAL "/sys/class/streaming_engine/dvb0.video0/dropped_before_decode_window_total"

#include "OMX_CoreExt.h"

namespace stm {
#ifdef ANDROID
const char VideoRenderer::kRole[] = "iv_renderer.yuv.overlay";
#else
const char VideoRenderer::kRole[] = "video_renderer.main";
#endif
VideoRenderer::VideoRenderer(OMX_HANDLETYPE hComponent)
    :OmxComponent("OMX.STM.Video.Renderer", hComponent, VIDEO_RENDERER_PORTNB),
     m_inputPort(VIDEO_RENDERER_INPUT, OMX_DirInput, *this),
     m_clockPort(CLOCK_RENDERER_INPUT, OMX_DirInput, *this),
     m_frameCountToCodec(0),
     m_frameCountManifested(0),
     m_droppedBeforeManifestationTotal(0),
     m_droppedBeforeOutputTimingTotal(0),
     m_droppedBeforeDecodeTotal(0),
     m_droppedBeforeDecodeWindowTotal(0),
     m_qosTimerId(0)
{
    INIT_DVB_DEVICE_INFO(&m_dvbInfo);
    DBGT_TRACE_INIT(vdec); //expands debug.vdec.trace
}

VideoRenderer::~VideoRenderer()
{}

void qosEventHandler(sigval_t v)
{
    uint32_t fps_data1 = 0;
    uint32_t fps_data2 = 0;
    VideoRenderer* p_vrend = (VideoRenderer*)v.sival_ptr;
    p_vrend->getFPSInfo(&fps_data1, &fps_data2);
    p_vrend->notifyEventHandler(OMX_EventRendererFpsInfo, fps_data1,fps_data2, NULL);
    return;
}

int VideoRenderer::qosCreateTimer()
{
    timer_t tid;
    struct sigevent se;
    struct itimerspec ts, ots;

    memset(&se, 0, sizeof (se));
    se.sigev_notify = SIGEV_THREAD;
    se.sigev_notify_function = qosEventHandler;
    se.sigev_value.sival_int = 0;
    se.sigev_value.sival_ptr = (void*) this;

    if (timer_create (CLOCK_REALTIME, &se, &tid) < 0)
    {
        perror ("timer_create");
        return -1;
    }
    puts ("timer_create successfully.");

    memset(&ts, 0, sizeof(ts));
    memset(&ots, 0, sizeof(ots));
    ts.it_value.tv_sec = 2;
    ts.it_value.tv_nsec = 0;
    ts.it_interval.tv_sec = 1;
    ts.it_interval.tv_nsec = 0;
    if (timer_settime (tid, TIMER_ABSTIME, &ts, &ots) < 0)
    {
        perror ("timer_settime");
        return -1;
    }
    return (int)tid;
}

void VideoRenderer::qosDeleteTimer (timer_t tid)
{
    if (tid) {
		timer_delete(tid);
	}
}

OMX_ERRORTYPE
VideoRenderer::getFPSInfo(uint32_t *fps_data1, uint32_t *fps_data2)
{
    int eError;
    int frame_count_to_codec_ps = 0;
    int frame_count_manifested_ps = 0;
    int dropped_before_manifestation_total_ps = 0;
    int dropped_before_output_timing_total_ps = 0;
    int dropped_before_decode_total_ps = 0;
    int dropped_before_decode_window_total_ps = 0;

    int frame_count_to_codec_current = 0;
    int frame_count_manifested_current = 0;
    int dropped_before_manifestation_total_current = 0;
    int dropped_before_output_timing_total_current = 0;
    int dropped_before_decode_total_current = 0;
    int dropped_before_decode_window_total_current = 0;

    FILE * pFile_frame_count_to_codec;
    FILE * pFile_frame_count_manifested;
    FILE * pFile_dropped_before_manifestation_total;
    FILE * pFile_dropped_before_output_timing_total;
    FILE * pFile_dropped_before_decode_total;
    FILE * pFile_dropped_before_decode_window_total;

    pFile_frame_count_to_codec = fopen (PATH_FRAME_COUNT_TO_CODEC,"r");
    pFile_frame_count_manifested = fopen (PATH_FRAME_COUNT_MANIFESTED,"r");
    pFile_dropped_before_manifestation_total = fopen (PATH_DROPPED_BEFORE_MANIFESTATION_TOTAL,"r");
    pFile_dropped_before_output_timing_total = fopen (PATH_DROPPED_BEFORE_OUTPUT_TIMING_TOTAL,"r");
    pFile_dropped_before_decode_total = fopen (PATH_DROPPED_BEFORE_DECODE_TOTAL,"r");
    pFile_dropped_before_decode_window_total = fopen (PATH_DROPPED_BEFORE_DECODE_WINDOW_TOTAL,"r");

    if (pFile_frame_count_to_codec != NULL) {
        eError = fscanf(pFile_frame_count_to_codec, "%d", &frame_count_to_codec_current);
    }
    else
    {
        puts("pFile_frame_count_to_codec failed.");
    }
    if (pFile_frame_count_manifested != NULL) {
        eError = fscanf(pFile_frame_count_manifested, "%d", &frame_count_manifested_current);
    }
    else
    {
        puts("pFile_frame_count_to_manifested failed.");
    }
    if (pFile_dropped_before_manifestation_total != NULL) {
        eError = fscanf(pFile_dropped_before_manifestation_total, "%d", &dropped_before_manifestation_total_current);
    }
    else
    {
        puts("pFile_dropped_before_manifestation_total failed.");
    }
    if (pFile_dropped_before_output_timing_total != NULL) {
        eError = fscanf(pFile_dropped_before_output_timing_total, "%d", &dropped_before_output_timing_total_current);
    }
    else
    {
        puts("pFile_dropped_before_output_timing_total failed.");
    }
    if (pFile_dropped_before_decode_total != NULL) {
        eError = fscanf(pFile_dropped_before_decode_total, "%d", &dropped_before_decode_total_current);
    }
    else
    {
        puts("pFile_dropped_before_decode_total failed.");
    }
    if (pFile_dropped_before_decode_window_total != NULL) {
        eError = fscanf(pFile_dropped_before_decode_window_total, "%d", &dropped_before_decode_window_total_current);
    }
    else
    {
        puts("pFile_dropped_before_decode_window_total failed.");
    }

    frame_count_to_codec_ps = frame_count_to_codec_current - m_frameCountToCodec;
    frame_count_manifested_ps = frame_count_manifested_current - m_frameCountManifested;
    dropped_before_manifestation_total_ps = dropped_before_manifestation_total_current - m_droppedBeforeManifestationTotal;
    dropped_before_output_timing_total_ps = dropped_before_output_timing_total_current - m_droppedBeforeOutputTimingTotal;
    dropped_before_decode_total_ps = dropped_before_decode_total_current - m_droppedBeforeDecodeTotal;
    dropped_before_decode_window_total_ps = dropped_before_decode_window_total_current - m_droppedBeforeDecodeWindowTotal;

    m_frameCountToCodec = frame_count_to_codec_current;
    m_frameCountManifested = frame_count_manifested_current;
    m_droppedBeforeManifestationTotal = dropped_before_manifestation_total_current;
    m_droppedBeforeOutputTimingTotal = dropped_before_output_timing_total_current;
    m_droppedBeforeDecodeTotal = dropped_before_decode_total_current;
    m_droppedBeforeDecodeWindowTotal =  dropped_before_decode_window_total_current;

    *fps_data1 = (frame_count_to_codec_ps & 0xFFFF) << 16 | (frame_count_manifested_ps & 0xFFFF);
    *fps_data2 = ((dropped_before_manifestation_total_ps + dropped_before_output_timing_total_ps) << 16)
        | ((dropped_before_decode_total_ps + dropped_before_decode_window_total_ps) & 0xFFFF);

   OMX_DBGT_PDEBUG("\nInput fps = %d\nDisplayed fps = %d\nDropped fps = %d\nLate fps = %d\n",
           frame_count_to_codec_ps,
           frame_count_manifested_ps,
           dropped_before_manifestation_total_ps + dropped_before_output_timing_total_ps,
           dropped_before_decode_total_ps + dropped_before_decode_window_total_ps);


    if(pFile_frame_count_to_codec)
        fclose (pFile_frame_count_to_codec);
    if(pFile_frame_count_manifested)
        fclose (pFile_frame_count_manifested);
    if(pFile_dropped_before_manifestation_total)
        fclose (pFile_dropped_before_manifestation_total);
    if(pFile_dropped_before_output_timing_total)
        fclose (pFile_dropped_before_output_timing_total);
    if(pFile_dropped_before_decode_total)
        fclose (pFile_dropped_before_decode_total);
    if(pFile_dropped_before_decode_window_total)
        fclose (pFile_dropped_before_decode_window_total);

    return OMX_ErrorNone;
}

OMX_ERRORTYPE
VideoRenderer::getParameter(OMX_INDEXTYPE       nParamIndex,
                            OMX_PTR             pParamStruct)
{
    OMX_DBGT_PROLOG();
    switch ((int)nParamIndex)
    {
    case OMX_IndexParamVideoInit: {
        ASSERT_STRUCT_TYPE(pParamStruct, OMX_PORT_PARAM_TYPE);
        OMX_PORT_PARAM_TYPE *param = (OMX_PORT_PARAM_TYPE*)pParamStruct;
        assert(param);
        param->nPorts = VIDEO_RENDERER_PORTNB;
        param->nStartPortNumber = VIDEO_RENDERER_INPUT;
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }; break;
    case OMX_IndexParamOtherInit: {
        ASSERT_STRUCT_TYPE(pParamStruct, OMX_PORT_PARAM_TYPE);
        OMX_PORT_PARAM_TYPE *param = (OMX_PORT_PARAM_TYPE*)pParamStruct;
        assert(param);
        param->nPorts = VIDEO_RENDERER_PORTNB;
        param->nStartPortNumber = CLOCK_RENDERER_INPUT;
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }; break;

    }

    // If the component does not support it, we fall back to generic logic.
    OMX_ERRORTYPE eError = OmxComponent::getParameter(nParamIndex,
                                                      pParamStruct);

    OMX_DBGT_EPILOG();
    return eError;
}

OMX_ERRORTYPE
VideoRenderer::setConfig(OMX_INDEXTYPE       nParamIndex,
                            OMX_PTR             pParamStruct)
{
    OMX_DBGT_PROLOG();
    switch ((int)nParamIndex)
    {
    case OMX_IndexConfigCommonOutputCrop:{
        int result;
        ASSERT_STRUCT_TYPE(pParamStruct, OMX_CONFIG_RECTTYPE);
        OMX_CONFIG_RECTTYPE *param = (OMX_CONFIG_RECTTYPE *)pParamStruct;

        OMX_DBGT_PDEBUG("Output Image Dimensions (%d,%d) (%d x %d)\n",
                            param->nLeft, param->nTop,
                            param->nWidth, param->nHeight);
        result = setCrop(param->nLeft, param->nTop,
                         param->nWidth, param->nHeight);
        if (result < 0) {
            OMX_DBGT_ERROR("setting crop failed:\n");
            OMX_DBGT_EPILOG();
            return (OMX_ERRORTYPE)-1;
        }
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }; break;
    case OMX_IndexConfigCommonPlaneBlend:{
        int result;
        ASSERT_STRUCT_TYPE(pParamStruct, OMX_CONFIG_PLANEBLENDTYPE);
        OMX_CONFIG_PLANEBLENDTYPE *param = (OMX_CONFIG_PLANEBLENDTYPE *)pParamStruct;
        OMX_DBGT_PDEBUG("Output z order %d\n", param->nDepth);

       /* set output zorder */
        result = setZOrder(param->nDepth);
        if (result < 0) {
            OMX_DBGT_ERROR("setting z order failed:\n");
            OMX_DBGT_EPILOG();
            return (OMX_ERRORTYPE) -1;
        }

        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }; break;
    }

    // If the component does not support it, we fall back to generic logic.
    OMX_ERRORTYPE eError = OmxComponent::setConfig(nParamIndex,
                                                      pParamStruct);

    OMX_DBGT_EPILOG();
    return eError;
}

OMX_ERRORTYPE
VideoRenderer::setParameter(OMX_INDEXTYPE       nParamIndex,
                            OMX_PTR             pParamStruct)
{
    OMX_DBGT_PROLOG();
    switch ((int)nParamIndex)
    {
    case OMX_IndexParamStandardComponentRole: {
        ASSERT_STRUCT_TYPE(pParamStruct, OMX_PARAM_COMPONENTROLETYPE);
        OMX_PARAM_COMPONENTROLETYPE *param = (OMX_PARAM_COMPONENTROLETYPE*)pParamStruct;
        assert(param);
        if (strcmp((char*)param->cRole, "default") == 0) {
            OMX_DBGT_EPILOG("Setting role to default");
            return OMX_ErrorNone;
        }
        if (strcmp((char*)param->cRole, kRole)) {
            OMX_DBGT_ERROR("Unsupported role %s", param->cRole);
            return OMX_ErrorUnsupportedSetting;
        }
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }; break;

    }

    // If the component does not support it, we fall back to generic logic.
    OMX_ERRORTYPE eError = OmxComponent::setParameter(nParamIndex,
                                                      pParamStruct);
    OMX_DBGT_EPILOG();
    return eError;
}

OMX_ERRORTYPE
VideoRenderer::getExtensionIndex(OMX_STRING          cParameterName,
                                 OMX_INDEXTYPE *     pIndexType)
{
    OMX_DBGT_PROLOG();
    if (strcmp(cParameterName,
               OMX_StmDvbVideoDeviceInfoExt) == 0) {
        *pIndexType = (OMX_INDEXTYPE)OMX_StmDvbVideoDeviceInfo;
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }
    OMX_ERRORTYPE eError = OmxComponent::getExtensionIndex(cParameterName,
                                                           pIndexType);
    OMX_DBGT_EPILOG();
    return eError;
}

OMX_ERRORTYPE
VideoRenderer::componentTunnelRequest(OMX_U32 nPort,
                                      OMX_HANDLETYPE hTunneledComp,
                                      OMX_U32 nTunneledPort,
                                      OMX_TUNNELSETUPTYPE* pSetup)
{
    // First call base class function
    OMX_ERRORTYPE eError = OmxComponent::componentTunnelRequest(nPort,
                                                                hTunneledComp,
                                                                nTunneledPort,
                                                                pSetup);
    if (eError != OMX_ErrorNone) {
        return eError;
    }

    // Tunneled with ST component supporting DvbInfo extension.
    // So retrieve dvb info
    if (nPort== VIDEO_RENDERER_INPUT) {
        assert(this->getPort(nPort).isInput() == true);
        OMX_INDEXTYPE nExtIndex;
        eError = OMX_GetExtensionIndex(hTunneledComp,
                                   (OMX_STRING)OMX_StmDvbVideoDeviceInfoExt,
                                   &nExtIndex);
        assert(eError == OMX_ErrorNone);
        OMX_GetStmLinuxDvbDeviceInfoParams cConfig;
        INIT_DVB_DEVICE_INFO(&cConfig);
        cConfig.hTunnelComp = OmxComponent::handle();
        eError = OMX_GetConfig(hTunneledComp, nExtIndex, &cConfig);
        if (eError != OMX_ErrorNone) {
            OMX_DBGT_ERROR("Can not retrieve dvb info during tunnel request");
        } else {
            copyDeviceInfo(m_dvbInfo, cConfig);
        }
    } else if (nPort== CLOCK_RENDERER_INPUT) {
        assert(this->getPort(nPort).isInput() == true);
        OMX_INDEXTYPE nExtIndex;
        eError = OMX_GetExtensionIndex(hTunneledComp,
                                   (OMX_STRING)OMX_StmDvbVideoDeviceInfoExt,
                                   &nExtIndex);
        assert(eError == OMX_ErrorNone);
        OMX_GetStmLinuxDvbDeviceInfoParams cConfig;
        INIT_DVB_DEVICE_INFO(&cConfig);
        copyDeviceInfo(cConfig,m_dvbInfo);
        eError = OMX_SetConfig(hTunneledComp, nExtIndex, &cConfig);
        if (eError != OMX_ErrorNone) {
            OMX_DBGT_ERROR("Can not retrieve dvb info during tunnel request");
        }
    }
    return eError;
}

OMX_ERRORTYPE
VideoRenderer::emptyThisBuffer(OMX_BUFFERHEADERTYPE* pBufferHdr)
{
#ifdef ANDROID
    assert(0 && "Invalid case in proprietary tunneling");
#else
    // We only handle EOF for now
    if(pBufferHdr->nFlags & OMX_BUFFERFLAG_EOS) {
        this->notifyEventHandler(OMX_EventBufferFlag, 0, pBufferHdr->nFlags, 0);
    }
#endif
    return OMX_ErrorNone;
}

OMX_ERRORTYPE
VideoRenderer::fillThisBuffer(OMX_BUFFERHEADERTYPE* pBufferHdr)
{
    assert(0 && "Invalid case in proprietary tunneling");
    return OMX_ErrorNone;
}

OMX_ERRORTYPE
VideoRenderer::useBuffer(OMX_BUFFERHEADERTYPE**  ppBufferHdr,
                         OMX_U32                 nPortIndex,
                         OMX_PTR                 pAppPrivate,
                         OMX_U32                 nSizeBytes,
                         OMX_U8*                 pBuffer)
{
    assert(0 && "Invalid case in proprietary tunneling");
    return OMX_ErrorNone;

}

OMX_ERRORTYPE
VideoRenderer::allocateBuffer(OMX_BUFFERHEADERTYPE**  ppBufferHdr,
                              OMX_U32                 nPortIndex,
                              OMX_PTR                 pAppPrivate,
                              OMX_U32                 nSizeBytes)
{
    assert(0 && "Invalid case in proprietary tunneling");
    return OMX_ErrorNone;
}

OMX_ERRORTYPE
VideoRenderer::freeBuffer(OMX_U32                 nPortIndex,
                          OMX_BUFFERHEADERTYPE*   pBufferHdr)
{
    assert(0 && "Invalid case in proprietary tunneling");
    return OMX_ErrorNone;
}

int VideoRenderer::setCrop (OMX_U32 nLeft,
                            OMX_U32 nTop,
                            OMX_U32 nWidth,
                            OMX_U32 nHeight)
{
    int v4l_fd;
    int result;
    struct v4l2_format format;
    char DeviceName[30];
    unsigned int viddevice = 0;
    OMX_DBGT_PROLOG();

    v4l_fd = LTV_CHECK(v4l2_open_by_name(V4L2_OVERLAY_DRIVER_NAME, V4L2_OVERLAY_CARD_NAME, O_RDWR));
    if (v4l_fd < 0) {
        return -1;
    }

    /* Choose the LinuxDVB decoder input to use according to the dev-id property */
    sprintf(DeviceName, "dvb0.video%01d", viddevice);

    result = v4l2_set_input_by_name(v4l_fd, DeviceName);
    if (result < 0) {
        OMX_DBGT_ERROR("VIDIOC_S_INPUT failed:\n");
        close(v4l_fd);
        return result;
    }

    // set to mannualmode
    {
        /*unsigned int ControlId[] = { V4L2_CID_STM_INPUT_WINDOW_MODE,
                                     V4L2_CID_STM_OUTPUT_WINDOW_MODE };*/
        struct v4l2_control Control;

        Control.id =  V4L2_CID_STM_OUTPUT_WINDOW_MODE;
        Control.value = VCSPLANE_MANUAL_MODE;
        if (ioctl (v4l_fd, VIDIOC_S_CTRL, &Control) != 0) {
            OMX_DBGT_ERROR("VIDIOC_S_CTRL failed");
            close(v4l_fd);
            return -1;
        }
    }

    memset(&format, 0, sizeof(format));
    format.type             = V4L2_BUF_TYPE_VIDEO_OVERLAY;
    format.fmt.win.w.left   = nLeft;
    format.fmt.win.w.top    = nTop;
    format.fmt.win.w.width  = nWidth;
    format.fmt.win.w.height = nHeight;

    result = ioctl(v4l_fd, VIDIOC_S_FMT, &format);
    close(v4l_fd);
    if (result < 0) {
        OMX_DBGT_ERROR("VIDIOC_S_FMT failed:\n");
        return result;
    }

    OMX_DBGT_EPILOG();
    return v4l_fd;
}

int VideoRenderer::setZOrder (OMX_U32 nDepth)
{
    int v4l_fd;
    struct v4l2_control cntrl;
    int result;
    char outputName[30];
    OMX_DBGT_PROLOG();

    /*
     * Open the requested V4L2 device
     */
    v4l_fd = LTV_CHECK(v4l2_open_by_name(V4L2_DISPLAY_DRIVER_NAME, V4L2_DISPLAY_CARD_NAME, O_RDWR));
    if (v4l_fd < 0) {
        return -1;
    }

    sprintf(outputName, VIDIO_OUTPUT_NAME);

    v4l2_set_output_by_name(v4l_fd, outputName);

    cntrl.id = V4L2_CID_STM_Z_ORDER;
    result = ioctl(v4l_fd, VIDIOC_G_CTRL, &cntrl);
    if(result >= 0)
    {
        OMX_DBGT_PDEBUG("zorder for %s: %d\n", outputName, cntrl.value);
    }

    /* set output zorder */
    cntrl.id = V4L2_CID_STM_Z_ORDER;
    cntrl.value = nDepth;
    result = ioctl(v4l_fd, VIDIOC_S_CTRL, &cntrl);
    close(v4l_fd);
    if (result < 0) {
        OMX_DBGT_ERROR("VIDIOC_S_CTRL failed:\n");
        return result;
    }

    OMX_DBGT_EPILOG();
    return 0;
}

OMX_ERRORTYPE VideoRenderer::create ()
{
    OMX_ERRORTYPE eError = OMX_ErrorNone;

    OMX_DBGT_PROLOG();

    if ((eError = OmxComponent::create()) != OMX_ErrorNone) {
        OMX_DBGT_ERROR();
        OMX_DBGT_EPILOG();
        return eError;
    }

    // Initialize video renderer port
    if ((eError = m_inputPort.create()) != OMX_ErrorNone) {
        OMX_DBGT_ERROR("Error when creating video renderer input port");
        OMX_DBGT_EPILOG();
        return eError;
    }

    // Initialize clock renderer port
    if ((eError = m_clockPort.create()) != OMX_ErrorNone) {
        OMX_DBGT_ERROR("Error when creating audio renderer clock port");
        OMX_DBGT_EPILOG();
        return eError;
    }

    OMX_DBGT_EPILOG();
    return OMX_ErrorNone;
}

void VideoRenderer::destroy()
{
    OMX_DBGT_PROLOG();

    // cleanup the input port
    m_inputPort.destroy();

    // cleanup the clock port
    m_clockPort.destroy();

    // cleanup component
    OmxComponent::destroy();
    OMX_DBGT_EPILOG();
}

const OmxPort& VideoRenderer::getPort(unsigned int nIdx) const
{
    assert(nIdx < VIDEO_RENDERER_PORTNB);
    if (nIdx == VIDEO_RENDERER_INPUT)
        return m_inputPort;
    else
        return m_clockPort;
}

OmxPort& VideoRenderer::getPort(unsigned int nIdx)
{
    assert(nIdx < VIDEO_RENDERER_PORTNB);
    if (nIdx == VIDEO_RENDERER_INPUT)
        return m_inputPort;
    else
        return m_clockPort;
}


OMX_ERRORTYPE VideoRenderer::configure()
{ return OMX_ErrorNone; }

OMX_ERRORTYPE VideoRenderer::start()
{
    if (this->m_inputPort.isTunneled()) {
        m_qosTimerId = (timer_t)this->qosCreateTimer();
    }

    return OMX_ErrorNone;
}

void VideoRenderer::stop()
{
    if (this->m_inputPort.isTunneled()) {
        this->qosDeleteTimer(m_qosTimerId);
    }
}

void VideoRenderer::execute()
{}

OMX_ERRORTYPE VideoRenderer::pause()
{ return OMX_ErrorNone; }

OMX_ERRORTYPE VideoRenderer::resume()
{ return OMX_ErrorNone; }

OMX_ERRORTYPE VideoRenderer::flush(unsigned int nPortIdx)
{ return OMX_ErrorNone; }

}; // eof namespace stm

//=====================================================================================
// OMX_ComponentInit
// "C" interface to initialize a new OMX component: creation of the associated object
//======================================================================================
extern "C" OMX_ERRORTYPE OMX_ComponentInit (OMX_HANDLETYPE hComponent)
{
    return stm::OmxComponent::ComponentInit(new stm::VideoRenderer(hComponent));
}

