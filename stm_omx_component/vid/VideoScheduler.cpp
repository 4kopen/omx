/*
 * Copyright 2013 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file   VideoScheduler.cpp
 * @author STMicroelectronics
 */

#include "VideoScheduler.hpp"
#include <OMX_debug.h>

#if HAVE_ASSERT_H
#include <assert.h>
#endif

#if HAVE_DBGTASSERT_H
// Re-direct assert on DBGT_ASSERT macro
#define assert(expr)  OMX_DBGT_ASSERT(expr)
#endif

#define DBGT_DECLARE_AUTOVAR
#define DBGT_PREFIX "OMX "
#define DBGT_LAYER 0
#include <linux/dbgt.h>

namespace stm {

const char VideoScheduler::kRole[] = "video_scheduler.binary";

VideoScheduler::VideoScheduler(OMX_HANDLETYPE hComponent)
    :OmxComponent("OMX.STM.Video.Scheduler", hComponent, VIDEO_SCHED_PORTNB),
     m_inputPort(VIDEO_SCHED_INPUT, OMX_DirInput, *this),
     m_outputPort(VIDEO_SCHED_OUTPUT, OMX_DirOutput, *this),
     m_clockPort(CLOCK_SCHED_PORT, OMX_DirInput, *this)
{
    INIT_DVB_DEVICE_INFO(&m_dvbInfo);
    DBGT_TRACE_INIT(vdec); //expands debug.vdec.trace
}

VideoScheduler::~VideoScheduler()
{}

OMX_ERRORTYPE
VideoScheduler::getParameter(OMX_INDEXTYPE       nParamIndex,
                             OMX_PTR             pParamStruct)
{
    OMX_DBGT_PROLOG();
    switch ((int)nParamIndex)
    {
    case OMX_IndexParamVideoInit: {
        ASSERT_STRUCT_TYPE(pParamStruct, OMX_PORT_PARAM_TYPE);
        OMX_PORT_PARAM_TYPE *param = (OMX_PORT_PARAM_TYPE*)pParamStruct;
        assert(param);
        param->nPorts = 2;
        param->nStartPortNumber = 0;
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }; break;

    case OMX_IndexParamOtherInit: {
        ASSERT_STRUCT_TYPE(pParamStruct, OMX_PORT_PARAM_TYPE);
        OMX_PORT_PARAM_TYPE *param = (OMX_PORT_PARAM_TYPE*)pParamStruct;
        assert(param);
        param->nPorts = 1;
        param->nStartPortNumber = CLOCK_SCHED_PORT;
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }; break;

    }

    // If the component does not support it, we fall back to generic logic.
    OMX_ERRORTYPE eError = OmxComponent::getParameter(nParamIndex,
                                                      pParamStruct);

    OMX_DBGT_EPILOG();
    return eError;
}

OMX_ERRORTYPE
VideoScheduler::setParameter(OMX_INDEXTYPE       nParamIndex,
                             OMX_PTR             pParamStruct)
{
    OMX_DBGT_PROLOG();
    switch ((int)nParamIndex)
    {
    case OMX_IndexParamStandardComponentRole: {
        ASSERT_STRUCT_TYPE(pParamStruct, OMX_PARAM_COMPONENTROLETYPE);
        OMX_PARAM_COMPONENTROLETYPE *param = (OMX_PARAM_COMPONENTROLETYPE*)pParamStruct;
        assert(param);
        if (strcmp((char*)param->cRole, "default") == 0) {
            OMX_DBGT_EPILOG("Setting role to default");
            return OMX_ErrorNone;
        }
        if (strcmp((char*)param->cRole, kRole)) {
            OMX_DBGT_ERROR("Unsupported role %s", param->cRole);
            return OMX_ErrorUnsupportedSetting;
        }
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }; break;

    }

    // If the component does not support it, we fall back to generic logic.
    OMX_ERRORTYPE eError = OmxComponent::setParameter(nParamIndex,
                                                      pParamStruct);
    OMX_DBGT_EPILOG();
    return eError;
}

OMX_ERRORTYPE
VideoScheduler::getConfig(OMX_INDEXTYPE nIndex,
                          OMX_PTR pConfStruct)
{
    OMX_DBGT_PROLOG();
    switch ((int)nIndex)
    {
    case OMX_StmDvbVideoDeviceInfo: {
        ASSERT_STRUCT_TYPE(pConfStruct, OMX_GetStmLinuxDvbDeviceInfoParams);
        OMX_GetStmLinuxDvbDeviceInfoParams *param = (OMX_GetStmLinuxDvbDeviceInfoParams*)pConfStruct;
        assert(param);
        copyDeviceInfo(*param, m_dvbInfo);
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }; break;

    }

    // If the component does not support it, we fall back to generic logic.
    OMX_ERRORTYPE eError = OmxComponent::getConfig(nIndex,
                                                   pConfStruct);
    OMX_DBGT_EPILOG();
    return eError;
}

OMX_ERRORTYPE
VideoScheduler::getExtensionIndex(OMX_STRING          cParameterName,
                                 OMX_INDEXTYPE *     pIndexType)
{
    OMX_DBGT_PROLOG();
    if (strcmp(cParameterName,
               OMX_StmDvbVideoDeviceInfoExt) == 0) {
        *pIndexType = (OMX_INDEXTYPE)OMX_StmDvbVideoDeviceInfo;
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }

    // We fall back to generic logic.
    OMX_ERRORTYPE eError = OmxComponent::getExtensionIndex(cParameterName,
                                                           pIndexType);
    OMX_DBGT_EPILOG();
    return eError;
}

OMX_ERRORTYPE 
VideoScheduler::componentTunnelRequest(OMX_U32 nPort,
                                       OMX_HANDLETYPE hTunneledComp,
                                       OMX_U32 nTunneledPort,
                                       OMX_TUNNELSETUPTYPE* pSetup)
{
    // First call base class function
    OMX_ERRORTYPE eError = OmxComponent::componentTunnelRequest(nPort,
                                                                hTunneledComp,
                                                                nTunneledPort,
                                                                pSetup);
    if (eError != OMX_ErrorNone) {
        return eError;
    }

    // Tunneled with ST component supporting DvbInfo extension.
    // So retrieve dvb info
    if (nPort == VIDEO_SCHED_INPUT) {
        assert(this->getPort(nPort).isInput() == true);
        OMX_INDEXTYPE nExtIndex;
        eError = OMX_GetExtensionIndex(hTunneledComp,
                                       (OMX_STRING)OMX_StmDvbVideoDeviceInfoExt,
                                       &nExtIndex);
        assert(eError == OMX_ErrorNone);
        OMX_GetStmLinuxDvbDeviceInfoParams cConfig;
        INIT_DVB_DEVICE_INFO(&cConfig);
        eError = OMX_GetConfig(hTunneledComp, nExtIndex,
                               &cConfig);
        if (eError != OMX_ErrorNone) {
            OMX_DBGT_ERROR("Can not retrieve dvb info during tunnel request");
        } else {
            copyDeviceInfo(m_dvbInfo, cConfig);
        }
    } else if (nPort == CLOCK_SCHED_PORT) {
        assert(this->getPort(nPort).isInput() == true);
        OMX_INDEXTYPE nExtIndex;
        eError = OMX_GetExtensionIndex(hTunneledComp,
                                       (OMX_STRING)OMX_StmDvbVideoDeviceInfoExt,
                                       &nExtIndex);
        assert(eError == OMX_ErrorNone);
        OMX_GetStmLinuxDvbDeviceInfoParams cConfig;
        INIT_DVB_DEVICE_INFO(&cConfig);
        copyDeviceInfo(cConfig, m_dvbInfo);
        eError = OMX_SetConfig(hTunneledComp, nExtIndex,
                               &cConfig);
        if (eError != OMX_ErrorNone) {
            OMX_DBGT_ERROR("Can not set dvb info during tunnel request");
        }
    }
    return eError;
}

OMX_ERRORTYPE
VideoScheduler::emptyThisBuffer(OMX_BUFFERHEADERTYPE* pBufferHdr)
{
    assert(0 && "Invalid case in proprietary tunneling");
    return OMX_ErrorNone;
}

OMX_ERRORTYPE
VideoScheduler::fillThisBuffer(OMX_BUFFERHEADERTYPE* pBufferHdr)
{
    assert(0 && "Invalid case in proprietary tunneling");
    return OMX_ErrorNone;
}

OMX_ERRORTYPE
VideoScheduler::useBuffer(OMX_BUFFERHEADERTYPE**  ppBufferHdr,
                         OMX_U32                 nPortIndex,
                         OMX_PTR                 pAppPrivate,
                         OMX_U32                 nSizeBytes,
                         OMX_U8*                 pBuffer)
{
    assert(0 && "Invalid case in proprietary tunneling");
    return OMX_ErrorNone;
}

OMX_ERRORTYPE
VideoScheduler::allocateBuffer(OMX_BUFFERHEADERTYPE**  ppBufferHdr,
                              OMX_U32                 nPortIndex,
                              OMX_PTR                 pAppPrivate,
                              OMX_U32                 nSizeBytes)
{
    assert(0 && "Invalid case in proprietary tunneling");
    return OMX_ErrorNone;
}

OMX_ERRORTYPE
VideoScheduler::freeBuffer(OMX_U32                 nPortIndex,
                          OMX_BUFFERHEADERTYPE*   pBufferHdr)
{
    assert(0 && "Invalid case in proprietary tunneling");
    return OMX_ErrorNone;
}

OMX_ERRORTYPE VideoScheduler::create ()
{
    OMX_ERRORTYPE eError = OMX_ErrorNone;

    OMX_DBGT_PROLOG();

    if ((eError = OmxComponent::create()) != OMX_ErrorNone) {
        DBGT_ERROR();
        DBGT_EPILOG();
        return eError;
    }

    // Initialize video scheduler ports
    if ((eError = m_inputPort.create()) != OMX_ErrorNone) {
        OMX_DBGT_ERROR("Error when creating video scheduler input port");
        OMX_DBGT_EPILOG();
        return eError;
    }

    if ((eError = m_outputPort.create()) != OMX_ErrorNone) {
        OMX_DBGT_ERROR("Error when creating video scheduler output port");
        OMX_DBGT_EPILOG();
        return eError;
    }

    // Initialize clock scheduler port
    if ((eError = m_clockPort.create()) != OMX_ErrorNone) {
        OMX_DBGT_ERROR("Error when creating video scheduler clock port");
        OMX_DBGT_EPILOG();
        return eError;
    }

    OMX_DBGT_EPILOG();
    return OMX_ErrorNone;
}

void VideoScheduler::destroy()
{
    OMX_DBGT_PROLOG();

    // cleanup video ports
    m_inputPort.destroy();
    m_outputPort.destroy();

    // cleanup clock port
    m_clockPort.destroy();

    // cleanup component
    OmxComponent::destroy();
    OMX_DBGT_EPILOG();
}

const OmxPort& VideoScheduler::getPort(unsigned int nIdx) const
{
    assert(nIdx < VIDEO_SCHED_PORTNB);
    if (nIdx == VIDEO_SCHED_INPUT)
        return m_inputPort;
    if (nIdx == VIDEO_SCHED_OUTPUT)
        return m_outputPort;
    return m_clockPort;
}

OmxPort& VideoScheduler::getPort(unsigned int nIdx)
{
    assert(nIdx < VIDEO_SCHED_PORTNB);
    if (nIdx == VIDEO_SCHED_INPUT)
        return m_inputPort;
    if (nIdx == VIDEO_SCHED_OUTPUT)
        return m_outputPort;
    return m_clockPort;
}

OMX_ERRORTYPE VideoScheduler::configure()
{ return OMX_ErrorNone; }

OMX_ERRORTYPE VideoScheduler::start()
{ return OMX_ErrorNone; }

void VideoScheduler::stop()
{}

void VideoScheduler::execute()
{}

OMX_ERRORTYPE VideoScheduler::pause()
{ return OMX_ErrorNone; }

OMX_ERRORTYPE VideoScheduler::resume()
{ return OMX_ErrorNone; }

OMX_ERRORTYPE VideoScheduler::flush(unsigned int nPortIdx)
{ return OMX_ErrorNone; }

} // eof namespace stm

//=====================================================================================
// OMX_ComponentInit
// "C" interface to initialize a new OMX component: creation of the associated object
//======================================================================================
extern "C" OMX_ERRORTYPE OMX_ComponentInit (OMX_HANDLETYPE hComponent)
{
    return stm::OmxComponent::ComponentInit(new stm::VideoScheduler(hComponent));
}

