/*
 * Copyright 2013 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file   VideoRenderer.hpp
 * @author STMicroelectronics
 */

#ifndef _STM_OMXVIDEORENDERER_HPP_
#define _STM_OMXVIDEORENDERER_HPP_

#include "StmOmxComponent.hpp"
#include "StmOmxVendorExt.hpp"
#include "VideoPort.hpp"
#include "ClockPort.hpp"

#include <OMX_Video.h>
#include <stdio.h>
#include <time.h>
#include <signal.h>

namespace stm {

class VideoRenderer: public OmxComponent
{
public:
    /** Constructor */
    VideoRenderer(OMX_HANDLETYPE hComponent);

    /** Destructor */
    virtual ~VideoRenderer();

    virtual OMX_ERRORTYPE create();
    virtual void destroy();
    virtual const OmxPort& getPort(unsigned int nIdx) const;
    virtual OMX_ERRORTYPE setConfig(OMX_INDEXTYPE       nParamIndex,
                                    OMX_PTR             pParamStruct);
    virtual OMX_ERRORTYPE getParameter(OMX_INDEXTYPE       nParamIndex,
                                       OMX_PTR             pParamStruct);
    virtual OMX_ERRORTYPE setParameter(OMX_INDEXTYPE       nParamIndex,
                                       OMX_PTR             pParamStruct);
    virtual OMX_ERRORTYPE getExtensionIndex(OMX_STRING     cParameterName,
                                            OMX_INDEXTYPE* pIndexType);
    virtual
    OMX_ERRORTYPE componentTunnelRequest(OMX_U32 nPort,
                                         OMX_HANDLETYPE hTunneledComp,
                                         OMX_U32 nTunneledPort,
                                         OMX_TUNNELSETUPTYPE* pSetup);
    virtual OMX_ERRORTYPE emptyThisBuffer(OMX_BUFFERHEADERTYPE* pBufferHdr);
    virtual OMX_ERRORTYPE fillThisBuffer(OMX_BUFFERHEADERTYPE* pBufferHdr);

    virtual OMX_ERRORTYPE useBuffer(OMX_BUFFERHEADERTYPE**  ppBufferHdr,
                                    OMX_U32                 nPortIndex,
                                    OMX_PTR                 pAppPrivate,
                                    OMX_U32                 nSizeBytes,
                                    OMX_U8*                 pBuffer);
    virtual OMX_ERRORTYPE allocateBuffer(OMX_BUFFERHEADERTYPE**  ppBufferHdr,
                                         OMX_U32                 nPortIndex,
                                         OMX_PTR                 pAppPrivate,
                                         OMX_U32                 nSizeBytes);
    virtual OMX_ERRORTYPE freeBuffer(OMX_U32                 nPortIndex,
                                     OMX_BUFFERHEADERTYPE*   pBufferHdr);
    OMX_ERRORTYPE getFPSInfo(uint32_t *fps_data1, uint32_t *fps_data2);


protected:
    virtual OmxPort& getPort(unsigned int nIdx);
    virtual OMX_ERRORTYPE configure();
    virtual OMX_ERRORTYPE start();
    virtual void stop();
    virtual void execute();
    virtual OMX_ERRORTYPE pause();
    virtual OMX_ERRORTYPE resume();
    virtual OMX_ERRORTYPE flush(unsigned int nPortIdx);

private:
    static const char kRole[];
    static const unsigned int VIDEO_RENDERER_PORTNB = 2;
    static const unsigned int VIDEO_RENDERER_INPUT = 0;
    static const unsigned int CLOCK_RENDERER_INPUT = 1;

    OmxVideoPort      m_inputPort;
    OmxClockPort      m_clockPort;

    OMX_GetStmLinuxDvbDeviceInfoParams  m_dvbInfo;

    int m_frameCountToCodec;
    int m_frameCountManifested;
    int m_droppedBeforeManifestationTotal;
    int m_droppedBeforeOutputTimingTotal;
    int m_droppedBeforeDecodeTotal;
    int m_droppedBeforeDecodeWindowTotal;

    friend void qosEventHandler(sigval_t v);
    int qosCreateTimer();
    void qosDeleteTimer (timer_t tid);
    timer_t m_qosTimerId;
    int setCrop(OMX_U32 nLeft,
                OMX_U32 nTop,
                OMX_U32 nWidth,
                OMX_U32 nHeight);
    int setZOrder(OMX_U32 nDepth);
};

} // eof namespace stm

#endif // _STM_OMXVIDEORENDERER_HPP_
