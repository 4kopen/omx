/*
 * Copyright 2013 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file   VideoPort.hpp
 * @author STMicroelectronics
 */

#ifndef _STM_VIDEOPORT_HPP_
#define _STM_VIDEOPORT_HPP_

#include "StmOmxPort.hpp"
#include "StmOmxComponent.hpp"

#include "VideoPortDefinition.hpp"

namespace stm {

/**
 * A video port object.
 * Default implementation is an uncompressed video port, supporting default
 * color format.
 */
class OmxVideoPort: public OmxPort
{
public:
    OmxVideoPort(unsigned int nIdx, OMX_DIRTYPE eDir,
                 const OmxComponent& owner);
    virtual ~OmxVideoPort();

    virtual OMX_ERRORTYPE create();
    virtual void destroy();

    virtual const OmxComponent& component() const;
    virtual const VideoPortDefinition& definition() const;

    virtual OMX_ERRORTYPE
    getParameter(OMX_INDEXTYPE nIndex, OMX_PTR pParamStruct);

    virtual OMX_ERRORTYPE
    setParameter(OMX_INDEXTYPE nIndex, OMX_PTR pParamStruct);

    virtual OMX_ERRORTYPE
    emptyThisBuffer(OMX_BUFFERHEADERTYPE* pBuffer);

    virtual OMX_ERRORTYPE
    fillThisBuffer(OMX_BUFFERHEADERTYPE* pBuffer);

    OMX_COLOR_FORMATTYPE
    supportedColorFormat(unsigned int nIdx) const;

    unsigned int
    supportedColorFormatNb() const;

    bool isSupportedColorFormat(OMX_COLOR_FORMATTYPE aFmt) const;

protected:
    virtual VideoPortDefinition& definition();

private:
    const OmxComponent&         m_owner;
    VideoPortDefinition         m_PortDefinition;

    static const unsigned int   SUPPORTED_FORMAT_SZ = 3;
    OMX_COLOR_FORMATTYPE        m_ColorFormat[SUPPORTED_FORMAT_SZ];
};

inline unsigned int
OmxVideoPort::supportedColorFormatNb() const
{ return SUPPORTED_FORMAT_SZ; }

} // eof namespace stm

#endif // _STM_VIDEOPORT_HPP_
