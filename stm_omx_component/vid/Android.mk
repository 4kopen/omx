
LOCAL_PATH:= $(call my-dir)

#=============================================================================
# OMX Video Renderer
#=============================================================================

include $(CLEAR_VARS)

LOCAL_SRC_FILES:= \
	VideoPort.cpp                \
	VideoRenderer.cpp

# Includes for ICS
LOCAL_C_INCLUDES+= \
	$(TOP)/frameworks/base/include/media/stagefright/openmax
# Includes for JB
LOCAL_C_INCLUDES+= \
	$(TOP)/frameworks/native/include/media/openmax

# Common includes
LOCAL_C_INCLUDES+= \
	$(TOP)/vendor/stm/hardware/omx/include/ \
	$(TOP)/vendor/stm/hardware/omx/stm_omx_component/common/ \
	$(TOP)/vendor/stm/hardware/omx/stm_omx_component/vid/ \
	$(TOP)/vendor/stm/hardware/header/usr/include \
	$(TOP)/vendor/stm/module/stlinuxtv/linux/include \
	$(TOP)/vendor/stm/module/stlinuxtv

LOCAL_SHARED_LIBRARIES := \
        libdl \
        libcutils \
        liblog \
        libhardware \
        libSTMOMXCommon


LOCAL_CPPFLAGS += -DANDROID
LOCAL_CFLAGS   += -DDBGT_CONFIG_DEBUG -DDBGT_CONFIG_AUTOVAR -DDBGT_TAG=\"VREND\ \" -Werror
LOCAL_CFLAGS   += -DHAVE_DBGTASSERT_H
LOCAL_CFLAGS   += -Wno-error=switch
LOCAL_CFLAGS   += -Wno-unused-parameter
LOCAL_MODULE:= libOMX.STM.Video.Renderer
LOCAL_MODULE_TAGS := optional

include $(BUILD_SHARED_LIBRARY)

#=============================================================================
# OMX Video Processor
#=============================================================================

include $(CLEAR_VARS)

LOCAL_SRC_FILES:= \
	VideoPort.cpp                \
	VideoProcessor.cpp

# Includes for ICS
LOCAL_C_INCLUDES+= \
	$(TOP)/frameworks/base/include/media/stagefright/openmax
# Includes for JB
LOCAL_C_INCLUDES+= \
	$(TOP)/frameworks/native/include/media/openmax

# Common includes
LOCAL_C_INCLUDES+= \
	$(TOP)/vendor/stm/hardware/omx/include/ \
	$(TOP)/vendor/stm/hardware/omx/stm_omx_component/common/ \
	$(TOP)/vendor/stm/hardware/omx/stm_omx_component/vid/ \
	$(TOP)/vendor/stm/hardware/header/usr/include \

LOCAL_SHARED_LIBRARIES := \
        libdl \
        libcutils \
        liblog \
        libhardware \
        libSTMOMXCommon


LOCAL_CPPFLAGS += -DANDROID
LOCAL_CFLAGS   += -DDBGT_CONFIG_DEBUG -DDBGT_CONFIG_AUTOVAR -DDBGT_TAG=\"VPROC\ \" -Werror
LOCAL_CFLAGS   += -DHAVE_DBGTASSERT_H
LOCAL_CFLAGS   += -Wno-error=switch
LOCAL_CFLAGS   += -Wno-unused-parameter
LOCAL_MODULE:= libOMX.STM.Video.Processor
LOCAL_MODULE_TAGS := optional

include $(BUILD_SHARED_LIBRARY)


#=============================================================================
# OMX Video Scheduler
#=============================================================================

include $(CLEAR_VARS)

LOCAL_SRC_FILES:= \
	VideoPort.cpp                \
	VideoScheduler.cpp

# Includes for ICS
LOCAL_C_INCLUDES+= \
	$(TOP)/frameworks/base/include/media/stagefright/openmax
# Includes for JB
LOCAL_C_INCLUDES+= \
	$(TOP)/frameworks/native/include/media/openmax

# Common includes
LOCAL_C_INCLUDES+= \
	$(TOP)/vendor/stm/hardware/omx/include/ \
	$(TOP)/vendor/stm/hardware/omx/stm_omx_component/common/ \
	$(TOP)/vendor/stm/hardware/omx/stm_omx_component/vid/ \
	$(TOP)/vendor/stm/hardware/header/usr/include \

LOCAL_SHARED_LIBRARIES := \
        libdl \
        libcutils \
        liblog \
        libhardware \
        libSTMOMXCommon


LOCAL_CPPFLAGS += -DANDROID
LOCAL_CFLAGS   += -DDBGT_CONFIG_DEBUG -DDBGT_CONFIG_AUTOVAR -DDBGT_TAG=\"VSCHED\" -Werror
LOCAL_CFLAGS   += -DHAVE_DBGTASSERT_H
LOCAL_CFLAGS   += -Wno-error=switch
LOCAL_CFLAGS   += -Wno-unused-parameter
LOCAL_MODULE:= libOMX.STM.Video.Scheduler
LOCAL_MODULE_TAGS := optional

include $(BUILD_SHARED_LIBRARY)
