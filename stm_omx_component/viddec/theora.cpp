/*
 * Copyright 2011 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *
 * File   ::    theora.cpp
 * Author ::    Swinder Pal SINGH (swinder.singh@st.com)
 *
 *
 */
#include "OMX_debug.h"
#include "VideoPesAdapter.hpp"
#include "VideoDecoder.hpp"

#include "pes.hpp"

#define DBGT_PREFIX "THEORA"
#define DBGT_LAYER  1
#include <linux/dbgt.h>

namespace stm {

#define PTS_CORRECTION(x) (((x) * 90000)/1000000)
#define THEORA_HEADER_IDENTIFICATION 0x80
#define THEORA_HEADER_COMMENT        0x81
#define THEORA_HEADER_SETUP          0x82

struct TheoraHeader {
    uint8_t* pHeader;
    int nSize;
};

THEORAPesAdapter::THEORAPesAdapter(const VDECComponent& dec)
    :VideoPesAdapter(dec)
{}

THEORAPesAdapter::~THEORAPesAdapter()
{}


int THEORAPesAdapter::emitInitialHeader() const
{
    OMX_DBGT_PROLOG();
    OMX_DBGT_EPILOG();
    return 0;
}

int THEORAPesAdapter::findTheoraHeader(int headertype, uint8_t* buffer, unsigned int& maxsize, uint8_t*& header) const
{
    OMX_DBGT_PROLOG("headertype: %#x",headertype);
    int size = 0;

    unsigned int pos = 0;
    while (pos < maxsize) {
        if(*(buffer+pos) == headertype) {
            break;
        }
        pos++;
    }

    DBGT_PDEBUG("pos: %d",pos);

    if(pos == maxsize) {
        DBGT_ERROR("pos == maxsize");
        DBGT_EPILOG();
        return -1;
    }

    header = buffer + pos;

    if (memcmp (&header[1], "theora", 6) != 0) {
        DBGT_ERROR("theora not found in header");
        DBGT_EPILOG();
        return -1;
    }

    for(unsigned int i = 0; i<pos; i++) {
        if(i > 0)
            size = size<<8;
        size += buffer[i];
    }

    maxsize = maxsize - pos - size;
    OMX_DBGT_EPILOG("maxsize: %d, size: %d",maxsize,size);
    return size;
}

int THEORAPesAdapter::emitCodecConfig(uint8_t* pBuffer, unsigned int nFilledLen) const
{
    int err = 0;
    int PesHeaderLength = 0;
    int PesLength;
    int PrivateHeaderLength = 0;
    TheoraHeader theoraHeaders[3];

    OMX_DBGT_PROLOG ("pBuf=0x%p, len=%d", pBuffer, nFilledLen);

    theoraHeaders[0].nSize = findTheoraHeader((uint8_t)THEORA_HEADER_IDENTIFICATION, pBuffer, nFilledLen, theoraHeaders[0].pHeader);
    if(theoraHeaders[0].nSize < 0) {
        OMX_DBGT_EPILOG ();
        return -1;
    }

    theoraHeaders[1].nSize = findTheoraHeader(THEORA_HEADER_COMMENT, theoraHeaders[0].pHeader + theoraHeaders[0].nSize, nFilledLen, theoraHeaders[1].pHeader);
    if(theoraHeaders[1].nSize < 0) {
        OMX_DBGT_EPILOG ();
        return -1;
    }

    theoraHeaders[2].nSize = findTheoraHeader(THEORA_HEADER_SETUP, theoraHeaders[1].pHeader + theoraHeaders[1].nSize, nFilledLen, theoraHeaders[2].pHeader);
    if(theoraHeaders[2].nSize < 0) {
        OMX_DBGT_EPILOG ();
        return -1;
    }

    for(int i = 0; i < 3; i++) {
        OMX_DBGT_PDEBUG("theoraHeaders[%d].nSize: %d",i,theoraHeaders[i].nSize);

        memset (PesHeader, 0, PES_MAX_HEADER_SIZE);
        PesHeaderLength = InsertPesHeader (PesHeader, theoraHeaders[i].nSize, MPEG_VIDEO_PES_START_CODE, INVALID_PTS_VALUE, 0);
        PrivateHeaderLength = InsertVideoPrivateDataHeaderForAudio (&PesHeader[PesHeaderLength], theoraHeaders[i].nSize);
        OMX_DBGT_PTRACE("EmitFrame pesheader : %d private : %d",PesHeaderLength,PrivateHeaderLength);
        PesLength                               = PesHeader[PES_LENGTH_BYTE_0] + (PesHeader[PES_LENGTH_BYTE_1] << 8) + PrivateHeaderLength;
        PesHeader[PES_LENGTH_BYTE_0]            = PesLength & 0xff;
        PesHeader[PES_LENGTH_BYTE_1]            = (PesLength >> 8) & 0xff;
        PesHeader[PES_HEADER_DATA_LENGTH_BYTE] += PrivateHeaderLength;
        PesHeader[PES_FLAGS_BYTE]              |= PES_EXTENSION_DATA_PRESENT;
        PesHeaderLength                        += PrivateHeaderLength;
        OMX_DBGT_PDEBUG("PesHeaderLength: %d",PesHeaderLength);
        err = this->viddec().dvb().write (PesHeader, PesHeaderLength);
        if (err) {
            OMX_DBGT_ERROR("%s::THEORAEmitCodecConfig->dvb.Write PesHeader failed", name());
            goto ERROR;
        }
        err = this->viddec().dvb().write (theoraHeaders[i].pHeader, theoraHeaders[i].nSize);
        if (err) {
            OMX_DBGT_ERROR("%s::THEORAEmitCodecConfig->dvb.Write theoraHeaders[i] failed", name());
            goto ERROR;
        }
    }

ERROR:
    OMX_DBGT_EPILOG ();
    return err;
}

int THEORAPesAdapter::emitFrame(uint8_t* pBuffer, unsigned int nFilledLen, uint64_t nTimeStamp) const
{
    int err = 0;
    int PesHeaderLength = 0;
    int PesLength;
    int PrivateHeaderLength = 0;
    uint64_t nPTS = PTS_CORRECTION(nTimeStamp);

    OMX_DBGT_PROLOG();
    OMX_DBGT_PTRACE("pBuf=%p, len=%lu, nTimeStamp=%llu", pBuffer, (unsigned long)nFilledLen, nTimeStamp);

    if(nPTS > INVALID_PTS_VALUE)
    {
        //We will anyway overflow. So send no PTS
        nPTS = INVALID_PTS_VALUE;
    }

    // Write PES header
    memset (PesHeader, 0, PES_MAX_HEADER_SIZE);
    PesHeaderLength = InsertPesHeader (PesHeader, nFilledLen, MPEG_VIDEO_PES_START_CODE, nPTS, 0);

    // Update PES length
    PrivateHeaderLength = InsertVideoPrivateDataHeaderForAudio (&PesHeader[PesHeaderLength], nFilledLen);
    OMX_DBGT_PTRACE("EmitFrame pesheader : %d private : %d",PesHeaderLength,PrivateHeaderLength);
    PesLength                               = PesHeader[PES_LENGTH_BYTE_0] + (PesHeader[PES_LENGTH_BYTE_1] << 8) + PrivateHeaderLength;
    PesHeader[PES_LENGTH_BYTE_0]            = PesLength & 0xff;
    PesHeader[PES_LENGTH_BYTE_1]            = (PesLength >> 8) & 0xff;
    PesHeader[PES_HEADER_DATA_LENGTH_BYTE] += PrivateHeaderLength;
    PesHeader[PES_FLAGS_BYTE]              |= PES_EXTENSION_DATA_PRESENT;
    PesHeaderLength                        += PrivateHeaderLength;

    OMX_DBGT_PTRACE("EmitFrame header size : %d nFilledLen : %d",PesHeaderLength,nFilledLen);

    err = this->viddec().dvb().write (PesHeader, PesHeaderLength);
    if(err)
    {
        OMX_DBGT_ERROR("%s::THEORAEmitFrame->dvb.Write PesHeader failed", name());
        goto ERROR;
    }

    // Write data
    if (nFilledLen > 0)
        err = this->viddec().dvb().write(pBuffer, nFilledLen);
    if(err)
    {
        OMX_DBGT_ERROR("%s::THEORAEmitFrame->dvb.Write pBuffer failed", name());
        goto ERROR;
    }

ERROR:
    OMX_DBGT_EPILOG ();
    return err;
}

} // eof namespace stm

