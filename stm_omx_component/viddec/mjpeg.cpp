/*
 * Copyright 2011 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *
 * File   ::    mjpeg.cpp
 * Author ::    Priyank SAXSENA (priyank.saxena@st.com)
 *
 *
 */

#include "OMX_debug.h"
#include "VideoPesAdapter.hpp"
#include "VideoDecoder.hpp"

#include "pes.hpp"

#define DBGT_PREFIX "MJPEG "
#define DBGT_LAYER  1
#include <linux/dbgt.h>

#define PTS_CORRECTION(x) (((x) * 90000)/1000000)
#define MJPEG_APPLICATION_SPECIFIC_START_CODE   0xef

namespace stm {

MjpegPesAdapter::MjpegPesAdapter(const VDECComponent& dec)
    :VideoPesAdapter(dec)
{}

MjpegPesAdapter::~MjpegPesAdapter()
{}

int MjpegPesAdapter::emitInitialHeader() const
{
    /* in mjpeg it is required to set framerate in metadata  - reference taken from gst-apps */
    static const unsigned char  SequenceLayerStartCode[]        = {0xff,   MJPEG_APPLICATION_SPECIFIC_START_CODE};
    char vendor_id[] = "STMicroelectronics";
    unsigned int metadata[4],fps_num,fps_den;
    unsigned int metadata_length;
    int PesHeaderLength;
    int err = 0;

    fps_num = 30000;
    fps_den = 1001;

    metadata[0] = (((unsigned int) fps_num & 0x00FF) << 24) |
                  (((unsigned int) fps_num & 0xFF00) << 8) |
                  (((unsigned int) fps_num >> 8) & 0xFF00) |
                  (((unsigned int) fps_num >> 24) & 0x00FF);
    metadata[1] = (((unsigned int) fps_den & 0x00FF) << 24) |
                  (((unsigned int) fps_den & 0xFF00) << 8) |
                  (((unsigned int) fps_den >> 8) & 0xFF00) |
                  (((unsigned int) fps_den >> 24) & 0x00FF);

    memset (PesHeader, 0, PES_MAX_HEADER_SIZE);
    metadata_length = PES_MIN_HEADER_SIZE;
    memcpy (&PesHeader[metadata_length], SequenceLayerStartCode, sizeof(SequenceLayerStartCode));
    metadata_length += sizeof(SequenceLayerStartCode);
    memcpy (&PesHeader[metadata_length], vendor_id, sizeof (vendor_id));
    metadata_length += sizeof (vendor_id);
    memcpy (&PesHeader[metadata_length], metadata, sizeof (metadata));
    metadata_length += sizeof (metadata);

    PesHeaderLength = InsertPesHeader (PesHeader, metadata_length, MPEG_VIDEO_PES_START_CODE,INVALID_PTS_VALUE, 0);
    err = this->viddec().dvb().write(PesHeader, PesHeaderLength + metadata_length);
    return err;
}

int MjpegPesAdapter::emitCodecConfig(uint8_t* pBuffer,
                                     unsigned int nFilledLen) const
{
    int err = 0;
    int PesHeaderLength;

    OMX_DBGT_PROLOG();
    OMX_DBGT_PTRACE("pBuf=%p, len=%lu", pBuffer, (unsigned long)nFilledLen);

    // Write PES header
    memset (PesHeader, 0, PES_MAX_HEADER_SIZE);
    PesHeaderLength            = InsertPesHeader (PesHeader, nFilledLen, MPEG_VIDEO_PES_START_CODE, INVALID_PTS_VALUE, 0);
    err = this->viddec().dvb().write(PesHeader, PesHeaderLength);
    if(err)
    {
       OMX_DBGT_ERROR("%s::MJPEGEmitCodecConfig->dvb().write failed", name());
       goto error;
    }

    // Write data
    err = this->viddec().dvb().write(pBuffer, nFilledLen);
    if(err)
    {
       OMX_DBGT_ERROR("%s::MJPEGEmitCodecConfig->dvb().write failed", name());
    }

error:
    OMX_DBGT_EPILOG();
    return err;
}

int MjpegPesAdapter::emitFrame(uint8_t* pBuffer, unsigned int nFilledLen,
                               uint64_t nTimeStamp) const
{
    int err = 0;
    int PesHeaderLength;
    uint64_t nPTS = PTS_CORRECTION(nTimeStamp);

    OMX_DBGT_PROLOG();
    OMX_DBGT_PTRACE("pBuf=%p, len=%lu, nTimeStamp=%llu", pBuffer, (unsigned long)nFilledLen, nTimeStamp);

    if(nPTS > INVALID_PTS_VALUE)
    {
        //We will anyway overflow. So send no PTS
        nPTS = INVALID_PTS_VALUE;
    }

    // Write Pes header
    memset (PesHeader, 0, PES_MAX_HEADER_SIZE);

    /* if pes packet is more than 64k then length should be made as zero */
    if(nFilledLen < 65536)
    {
        PesHeaderLength = InsertPesHeader (PesHeader, nFilledLen, MPEG_VIDEO_PES_START_CODE, nPTS, 0);
    }
    else
    {
        PesHeaderLength = InsertPesHeader (PesHeader, 0, MPEG_VIDEO_PES_START_CODE, nPTS, 0);
    }

    err = this->viddec().dvb().write(PesHeader, PesHeaderLength);
    if(err)
    {
       OMX_DBGT_ERROR("%s::MJPEGEmitFrame->dvb().write failed", name());
       goto error;
    }

    // Write data
    if (nFilledLen > 0)
        err = this->viddec().dvb().write(pBuffer, nFilledLen);
    if(err)
    {
       OMX_DBGT_ERROR("%s::MJPEGEmitFrame->dvb().write failed", name());
    }

error:
    OMX_DBGT_EPILOG();
    return err;
}

} // eof namespace stm
