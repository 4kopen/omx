/*
 * Copyright 2013 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file   VideoDecoderPort.hpp
 * @author STMicroelectronics
 */

#ifndef _STM_VIDEODECODERPORT_HPP_
#define _STM_VIDEODECODERPORT_HPP_

#include "StmOmxPort.hpp"
#include "StmOmxComponent.hpp"
#include "VideoPortDefinition.hpp"

#include "Mutex.hpp"
#include <pthread.h>
#include <stdint.h>

namespace stm {

// Forward declaration
class VidDecPort;
class VidDecOutputPort;
class VidDecInputPort;
class VDECComponent;

class VidDecPort: public OmxPort
{
public:
    /** Constructor */
    VidDecPort(unsigned int nIdx, OMX_DIRTYPE aDir,
               const VDECComponent& owner);

    /** Destructor */
    virtual ~VidDecPort();

    virtual const OmxComponent& component() const;
    virtual const VideoPortDefinition& definition() const;

protected:
    virtual VideoPortDefinition& definition();

    const VDECComponent& viddec() const;
    typedef enum  {
        EThreadState_Stopped  = 0,
        EThreadState_Running,
        EThreadState_Paused,
    } EThreadState;

    EThreadState threadState() const;
    void threadState(EThreadState st);

    EThreadState    m_nThreadState;
    Mutex           m_mutexThread;
    Condition       m_condState;
private:
    const VDECComponent&         m_owner;
    VideoPortDefinition          m_PortDefinition;
};

inline const VDECComponent& VidDecPort::viddec() const
{ return m_owner; }


//==========================================================================
// VidDecOutputPort declaration
//==========================================================================

class VidDecOutputPort: public VidDecPort
{
public:
    typedef struct {
        OMX_COLOR_FORMATTYPE OMXType;
        uint32_t androidHALType;
        uint32_t v4lType;
        uint32_t omxseType;
        uint8_t bpp;
        uint8_t bppWidth;
    } ColorInfo;

public:
    /** Constructor */
    VidDecOutputPort(unsigned int nIdx, OMX_DIRTYPE aDir,
                     const VDECComponent& owner);

    /** Destructor */
    virtual ~VidDecOutputPort();

    virtual OMX_ERRORTYPE create();
    virtual void destroy();
    int start();

public:
    virtual OMX_ERRORTYPE
    getParameter(OMX_INDEXTYPE nIndex, OMX_PTR pParamStruct);

    virtual OMX_ERRORTYPE
    setParameter(OMX_INDEXTYPE nIndex, OMX_PTR pParamStruct);

    virtual OMX_ERRORTYPE
    useBuffer(OMX_BUFFERHEADERTYPE** ppBufferHdr,
              OMX_PTR pAppPrivate,
              OMX_U32 nSizeBytes,
              OMX_U8* pBuffer);

    virtual OMX_ERRORTYPE
    allocateBuffer(OMX_BUFFERHEADERTYPE** ppBufferHdr,
                   OMX_PTR pAppPrivate,
                   OMX_U32 nSizeBytes);

    virtual OMX_ERRORTYPE
    freeBuffer(OMX_BUFFERHEADERTYPE* pBufferHdr);

    virtual OMX_ERRORTYPE
    emptyThisBuffer(OMX_BUFFERHEADERTYPE* pBuffer);

    virtual OMX_ERRORTYPE
    fillThisBuffer(OMX_BUFFERHEADERTYPE* pBuffer);

    virtual OMX_ERRORTYPE flush();

    OMX_COLOR_FORMATTYPE
    supportedColorFormat(unsigned int nIdx) const;

    unsigned int
    supportedColorFormatNb() const;

    bool isSupportedColorFormat(OMX_COLOR_FORMATTYPE aFmt) const;

    /** Change current port color format definition */
    void colorFormat(OMX_COLOR_FORMATTYPE aFmt);

    /** Return information about current color format */
    // TODO: implement static method with color format as param
    const ColorInfo& colorInfo() const;
    bool nativeWindowsEnabled() const;
    unsigned int receivedFrame() const;

    bool  isAfterEosFlag() const;
    void resetAfterEosFlag();

protected:
    /**
     * This function serves as the entry point to the thread.
     * @param pthis the instance
     */
    static void* ThreadEntryPoint(void* pThis);

    /** The thread main function loop */
    void threadFunction();

    /**
     * This function serves as the entry point to the ud thread.
     * @param pthis the instance
     */
    static void* udThreadEntryPoint(void* pThis);

    /** The thread main function loop */
    void udThreadFunction();

    OMX_ERRORTYPE udEnableCapture(bool bEnable);
private:
    static const ColorInfo  m_colorInfos[];

    /** Thread Id of the port thread */
    pthread_t puller_thread_id;

    /** Thread Id of the port thread for userdata*/
    pthread_t userdata_thread_id;
    bool m_bUDEnabled;
    /*
     * TRUE: Native windows enabled by Android and buffer allocated by
     *       itself through gralloc
     *      => pBuffer contains buffer_handle_t
     * FALSE: Buffer allocated by OMX_AllocateBuffer through gralloc
     *      => pBuffer contains vaddr
     *      => pPlatformPrivate contains buffer_handle_t
     */
    bool m_bNativeWindowsEnabled;

    // Count number of Frame, integer is enough since it allow
    // to play more than 1 year !!!
    unsigned int m_nReceivedFrame;

    bool     m_bAfterEosFlag;

    bool m_isEOSnFlagsSet;
    // Mutex to syncronize methods called prom diff threads
    Mutex    m_mutexSync;

    OMX_VIDEO_PARAM_PREPARE_FOR_ADAPTIVE_PLAYBACK m_prepareForAdaptivePlayback;

};

inline bool VidDecOutputPort::nativeWindowsEnabled() const
{ return m_bNativeWindowsEnabled; }

inline unsigned int VidDecOutputPort::receivedFrame() const
{ return m_nReceivedFrame; }

inline void VidDecOutputPort::resetAfterEosFlag()
{ m_bAfterEosFlag = false; }

inline bool VidDecOutputPort::isAfterEosFlag() const
{ return m_bAfterEosFlag; }


//==========================================================================
// VidDecInputPort declaration
//==========================================================================

class VidDecInputPort: public VidDecPort
{
public:
    /** Constructor */
    VidDecInputPort(unsigned int nIdx, OMX_DIRTYPE aDir,
                    const VDECComponent& owner);

    /** Destructor */
    virtual ~VidDecInputPort();

    virtual OMX_ERRORTYPE create();
    virtual void destroy();
    int start();

    struct ProfileAndLevelSupportedAVC_s {
        enum OMX_VIDEO_AVCPROFILETYPE AVCprofile;
        enum OMX_VIDEO_AVCLEVELTYPE AVClevel;
    };

    struct ProfileAndLevelSupportedMPEG4_s {
        enum OMX_VIDEO_MPEG4PROFILETYPE MPEG4profile;
        enum OMX_VIDEO_MPEG4LEVELTYPE MPEG4level;
    };


public:
    virtual OMX_ERRORTYPE
    getParameter(OMX_INDEXTYPE nIndex, OMX_PTR pParamStruct);

    virtual OMX_ERRORTYPE
    setParameter(OMX_INDEXTYPE nIndex, OMX_PTR pParamStruct);

    virtual OMX_ERRORTYPE
    useBuffer(OMX_BUFFERHEADERTYPE** ppBufferHdr,
              OMX_PTR pAppPrivate,
              OMX_U32 nSizeBytes,
              OMX_U8* pBuffer);

    virtual OMX_ERRORTYPE
    allocateBuffer(OMX_BUFFERHEADERTYPE** ppBufferHdr,
                   OMX_PTR pAppPrivate,
                   OMX_U32 nSizeBytes);

    virtual OMX_ERRORTYPE
    freeBuffer(OMX_BUFFERHEADERTYPE* pBufferHdr);

    virtual OMX_ERRORTYPE
    emptyThisBuffer(OMX_BUFFERHEADERTYPE* pBuffer);

    virtual OMX_ERRORTYPE
    fillThisBuffer(OMX_BUFFERHEADERTYPE* pBuffer);

    virtual OMX_ERRORTYPE flush();
    virtual OMX_ERRORTYPE pause();
    virtual OMX_ERRORTYPE resume();

    OMX_VIDEO_CODINGTYPE
    supportedCodingFormat(unsigned int nIdx) const;

    unsigned int
    supportedCodingFormatNb() const;

    bool isSupportedCodingFormat(OMX_VIDEO_CODINGTYPE aFmt) const;

    /** Change current port compression format in definition */
    void codingFormat(OMX_VIDEO_CODINGTYPE aFmt);

    unsigned int sendedFrame() const;

    bool isFlushIssued() const;
    bool is10BitEnabled() const;

protected:
    /**
     * This function serves as the entry point to the thread.
     * @param pthis the instance
     */
    static void* ThreadEntryPoint(void* pThis);

    /** The thread main function loop */
    void threadFunction();

private:

    int decodeFrame(unsigned int nOmxFlags,
                    uint8_t* pBuf, unsigned int nLen,
                    uint64_t nTimeStamp);

private:
    static OMX_BUFFERHEADERTYPE* EOSBUF;
    static const OMX_VIDEO_CODINGTYPE  m_codingFormats[];
    // Thread Id of the port thread
    pthread_t pusher_thread_id;

    unsigned int    m_nSendedFrame;

    char * tmp_buff;
    // TODO: check usage of tmp_length. no reset, only incrmented ?
    unsigned int tmp_length;

    bool     params_done;
    bool     initial_header;
    bool     isFlushCommandIssued;
    // 10Bit Enabled/Disabled
    bool     m_b10BitEnabled;

    static const struct ProfileAndLevelSupportedAVC_s m_profilelevelAVC[];
    static const struct ProfileAndLevelSupportedMPEG4_s m_profilelevelMPEG4[];
};

inline unsigned int VidDecInputPort::sendedFrame() const
{ return m_nSendedFrame; }

inline bool VidDecInputPort::isFlushIssued() const
{
    return isFlushCommandIssued;
}

inline bool VidDecInputPort::is10BitEnabled() const
{
    return m_b10BitEnabled;
}

} // eof namespace stm

#endif  // _STM_VIDEODECODERPORT_HPP_
