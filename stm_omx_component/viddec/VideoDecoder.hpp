/*
 * Copyright 2013 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file   VideoDecoder.cpp
 * @author Jean-Philippe FASSINO(jean-philippe.fassino@st.com)
 */

#ifndef _STM_VIDEODECODER_HPP_
#define _STM_VIDEODECODER_HPP_

#include "StmOmxComponent.hpp"
#include "VideoDecoderPort.hpp"
#include "Role.hpp"
#include "dvbvideo_moo.hpp"
#include "dvbvideo_sf.hpp"
#include <OMX_IndexExt.h>
#include <OMX_VideoExt.h>
#include "link.hpp"

#ifdef ANDROID
#include <hardware/gralloc.h>
#endif

#define MAX_SUPPORTED_WIDTH_HD 1920
#define MAX_SUPPORTED_HEIGHT_HD 1080
#define MAX_SUPPORTED_WIDTH_UHD 3840
#define MAX_SUPPORTED_HEIGHT_UHD 2160

namespace stm {

//Forward declaration
class VideoPesAdapter;

class VDECComponent: public OmxComponent
{
public:
    VDECComponent(OMX_HANDLETYPE hComponent);
    virtual ~VDECComponent();

    virtual OMX_ERRORTYPE create();
    virtual void destroy();
    virtual const VidDecPort& getPort(unsigned int nIdx) const;

    friend class VidDecInputPort;
    friend class VidDecOutputPort;

    virtual OMX_ERRORTYPE getParameter(
                  OMX_INDEXTYPE       nParamIndex,
                  OMX_PTR             ComponentParameterStructure);
    virtual OMX_ERRORTYPE setParameter(
                  OMX_INDEXTYPE       nParamIndex,
                  OMX_PTR             ComponentParameterStructure);
    virtual
    OMX_ERRORTYPE getConfig(OMX_INDEXTYPE nIndex, OMX_PTR pConfStruct);
    virtual
    OMX_ERRORTYPE setConfig(OMX_INDEXTYPE nIndex, OMX_PTR pConfStruct);
    virtual OMX_ERRORTYPE getExtensionIndex(
                  OMX_STRING          cParameterName,
                  OMX_INDEXTYPE *     pIndexType);
    virtual OMX_ERRORTYPE emptyThisBuffer(OMX_BUFFERHEADERTYPE*   pBufferHdr);
    virtual OMX_ERRORTYPE fillThisBuffer(OMX_BUFFERHEADERTYPE*   pBufferHdr);

    const VidDecInputPort& inputPort() const;
    const VidDecOutputPort& outputPort() const;
    const IDvbDevice& dvb() const;
    bool IsConfigureDone;
    mutable Mutex m_mutexCmd;
    mutable Condition   m_condConfig;

protected:
    virtual VidDecPort& getPort(unsigned int nIdx);

    virtual OMX_ERRORTYPE configure();
    virtual OMX_ERRORTYPE deConfigure();
    virtual OMX_ERRORTYPE start();
    virtual void stop();
    virtual void execute();
    virtual OMX_ERRORTYPE flush(unsigned int nPortIdx);

    OMX_ERRORTYPE setRole(const char* pNewRole);
public:
    unsigned int getWidth(unsigned int nIdx) const;
    unsigned int getHeight(unsigned int nIdx) const;
    OMX_VIDEO_CODINGTYPE getCompressionFormat(unsigned int nIdx) const;

private:
    VidDecInputPort& inputPort_();
    VidDecOutputPort& outputPort_();

    Role& role();
    VideoPesAdapter* pesAdapter() const;
#ifdef ANDROID
    const gralloc_module_t* grallocModule() const;
    struct alloc_device_t*  grallocDevice() const;
#endif
    IDvbDevice& dvb();

private:
    IDvbDevice*               m_dvb;
    uint32_t                  nDecodeMode;

#ifdef ANDROID
    const gralloc_module_t* m_grallocModule;
    struct alloc_device_t* m_grallocDevice;
#endif
    VidDecInputPort      m_inputPort;
    VidDecOutputPort     m_outputPort;

    Role                 m_componentRole;
    VideoPesAdapter*     m_pPesAdapter;
public:
   OMX_HANDLETYPE         hTunnelComp;
   mutable struct timeMapping_s   *timeMapping_p;
   mutable struct timeMapping_s   *overflowTimeMapping_p;
   mutable Mutex m_mutexCtrl;
};

inline const IDvbDevice& VDECComponent::dvb() const
{ return *m_dvb; }

inline IDvbDevice& VDECComponent::dvb()
{ return *m_dvb; }

inline const VidDecInputPort& VDECComponent::inputPort() const
{ return m_inputPort; }

inline const VidDecOutputPort& VDECComponent::outputPort() const
{ return m_outputPort; }

inline VidDecInputPort& VDECComponent::inputPort_()
{ return m_inputPort; }

inline VidDecOutputPort& VDECComponent::outputPort_()
{ return m_outputPort; }

#ifdef ANDROID
inline const gralloc_module_t* VDECComponent::grallocModule() const
{ return m_grallocModule; }

inline struct alloc_device_t*  VDECComponent::grallocDevice() const
{ return m_grallocDevice; }
#endif

inline Role&  VDECComponent::role()
{ return m_componentRole; }

inline VideoPesAdapter* VDECComponent::pesAdapter() const
{ return m_pPesAdapter; }

#define OMX_ST_VIDEO_INPUT_PORT 0
#define OMX_ST_VIDEO_OUTPUT_PORT 1
#define OMX_ST_VIDEO_PORT_NUMBER 2



} // eof namespace stm

#endif  // _STM_VIDEODECODER_HPP_
