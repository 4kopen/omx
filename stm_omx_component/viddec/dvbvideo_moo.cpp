/*
 * Copyright 2013 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file   dvbvideo_moo.cpp
 * @author STMicroelectronics
 */

#include "OMX_debug.h"
#include "VideoDecoder.hpp"
#include "dvbvideo_moo.hpp"

#include <sys/ioctl.h>
#include <sys/types.h>
#include <fcntl.h>
#include <errno.h>

#include <linux/videodev2.h>
#include <linux/dvb/stm_dvb.h>
#include <linux/dvb/video.h>
#include <linux/dvb/stm_video.h>
#include <linux/dvb/stm_audio.h>
#include <linux/stm/stmedia_export.h>
#include <utils/v4l2_helper.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#if HAVE_ASSERT_H
#include <assert.h>
#endif

#if HAVE_DBGTASSERT_H
// Re-direct assert on DBGT_ASSERT macro
#define assert(expr)  OMX_DBGT_ASSERT(expr)
#endif

#define V4L2_CAPTURE_DRIVER_NAME    "AV Decoder"
#define V4L2_CAPTURE_CARD_NAME      "STMicroelectronics"

#undef  DBGT_TAG
#define DBGT_TAG "DVBVM"
#define DBGT_PREFIX "DVBVM"
#define DBGT_LAYER  1
#define DBGT_VAR mDbgtVarDVBVM
#define DBGT_DECLARE_AUTOVAR
#include <linux/dbgt.h>

#define THREAD_GUARD(name)  stm::ScopedLock name(m_mutexCmd)

//==============================================================================
// DVBVideo_MOO::DVBVideo_MOO
//  Constructor
//==============================================================================
DVBVideo_MOO::DVBVideo_MOO(const char* aDeviceName, int nEncoding, const char *cName)
    :IDvbDevice(),
     mIsLoggingEnabled(false),
     vdev_id(-1),
     v4l2fd_ud(-1),
     m_nEncoding(nEncoding),
     m_nWidth(0),
     m_nHeight(0),
     enableSync(true),
     m_b10BitEnable(false),
     eFrameDecodeMode(OMX_VIDEO_DECODE_ALL_FRAMES),
     m_nState(EState_Stop),
     m_mutexCmd(),
     m_mutexCmd2(),
     m_condState(),
     m_bClearWrite(false),
     m_bClearWrite2(false),
     m_bFlushWrite(false)

{
    // Reuse VDEC trace property
    DBGT_TRACE_INIT(vdec);
    //check the string length before copy
    assert(strlen(aDeviceName) <= sizeof(m_deviceName));
    strcpy(m_deviceName, aDeviceName);
    OMXDEBUG_UPDATE_AV_TRACE_SETTINGS(cName);
}

DVBVideo_MOO::~DVBVideo_MOO()
{}

int DVBVideo_MOO::id() const
{ return vdev_id; }

const char* DVBVideo_MOO::name() const
{ return m_deviceName; }

stm::IDvbDevice::EState DVBVideo_MOO::state() const
{ return m_nState; }

void DVBVideo_MOO::state(stm::IDvbDevice::EState st) const
{
    m_nState = st;
    m_condState.signal();
}

//==============================================================================
// DVBVideo_MOO::setEncoding
//==============================================================================
void DVBVideo_MOO::setEncoding(int nEncoding)
{
    m_nEncoding = nEncoding;
}

//==============================================================================
//// DVBVideo_MOO::setResolution
////==============================================================================
void DVBVideo_MOO::setResolution(int nWidth, int nHeight)
{
    m_nWidth= nWidth;
    m_nHeight= nHeight;
}
//==============================================================================
// DVBVideo_MOO::setSync
//==============================================================================

void DVBVideo_MOO::setSync(bool bSyncEnable)
{
    enableSync = bSyncEnable;
    if(isOpen())
    {
        struct video_command videoCommand;
        memset(&videoCommand, 0, sizeof(struct video_command));
        videoCommand.cmd            = VIDEO_CMD_SET_OPTION;
        videoCommand.option.option  = DVB_OPTION_AV_SYNC;
        videoCommand.option.value   = (bSyncEnable)?DVB_OPTION_VALUE_ENABLE:DVB_OPTION_VALUE_DISABLE;
        if (ioctl (id(), VIDEO_COMMAND, (void*)&videoCommand) != 0)
        {
            OMX_DBGT_ERROR("VIDEO_COMMAND ioctl failed (%s)", strerror (errno));
        }
    }
}

//==============================================================================
// DVBVideo_MOO::setDecodeMode
//==============================================================================
void DVBVideo_MOO::setDecodeMode(uint32_t nDecodeMode)
{
    eFrameDecodeMode = (OMX_VIDEO_FRAME_DECODE_MODE)nDecodeMode;
    if(isOpen())
    {
        struct video_command videoCommand;
        memset(&videoCommand, 0, sizeof(struct video_command));
        videoCommand.cmd            = VIDEO_CMD_SET_OPTION;
        videoCommand.option.option  = DVB_OPTION_TRICK_MODE_DOMAIN;
        videoCommand.option.value   = (eFrameDecodeMode == OMX_VIDEO_DECODE_IFRAMES_ONLY)?
                                      DVB_OPTION_VALUE_TRICK_MODE_DECODE_KEY_FRAMES:
                                      DVB_OPTION_VALUE_TRICK_MODE_DECODE_ALL;
        if (ioctl (id(), VIDEO_COMMAND, (void*)&videoCommand) != 0)
        {
            OMX_DBGT_ERROR("VIDEO_COMMAND ioctl failed (%s)", strerror (errno));
        }
    }
}


//==============================================================================
// DVBVideo_MOO::v4lopen
//==============================================================================
int DVBVideo_MOO::v4lopen(const char *const driver, const char *const card, int flags)
{
    int v4l_fd = -1;
    int result = -1;
    char DeviceName[30];
    unsigned int viddevice = 0;
    v4l_fd = v4l2_open_by_name(driver, card, flags);
    if (v4l_fd < 0) {
        OMX_DBGT_ERROR("VIDIOC_S_INPUT failed:\n");
        return -1;
    }

    /* Choose the LinuxDVB decoder input to use according to the dev-id property */
    sprintf(DeviceName, "dvb0.video%01d", viddevice);

    result = v4l2_set_input_by_name(v4l_fd, DeviceName);
    if (result < 0)
    {
        OMX_DBGT_ERROR("VIDIOC_S_INPUT failed:\n");
        close(v4l_fd);
        return result;
    }

    return v4l_fd;
}

uint32_t DVBVideo_MOO::getMemProfile()
{
    uint32_t ctrlValue = DVB_OPTION_CTRL_VALUE_HD_PROFILE;

    if(m_nWidth > MAX_SUPPORTED_WIDTH_UHD || m_nHeight > MAX_SUPPORTED_HEIGHT_UHD)
    {
        ctrlValue = DVB_OPTION_CTRL_VALUE_4K2K_PROFILE;
        if(m_b10BitEnable && m_nEncoding == VIDEO_ENCODING_HEVC)
        {
            ctrlValue = DVB_OPTION_CTRL_VALUE_4K2K_10BITS_PROFILE;
        }
    } else if(m_nWidth > MAX_SUPPORTED_WIDTH_HD || m_nHeight > MAX_SUPPORTED_HEIGHT_HD)
    {
        ctrlValue = DVB_OPTION_CTRL_VALUE_UHD_PROFILE;
        if(m_b10BitEnable && m_nEncoding == VIDEO_ENCODING_HEVC)
        {
            ctrlValue = DVB_OPTION_CTRL_VALUE_UHD_10BITS_PROFILE;
        }
    }
    else
    {
        if(m_b10BitEnable && m_nEncoding == VIDEO_ENCODING_HEVC)
        {
            ctrlValue = DVB_OPTION_CTRL_VALUE_HD_10BITS_PROFILE;
        }
    }
    return ctrlValue;
}

//==============================================================================
// DVBVideo_MOO::create
//==============================================================================
int DVBVideo_MOO::create()
{
    struct video_command videoCommand;
    THREAD_GUARD(guard);
    OMX_DBGT_PROLOG("Creating DVB video %s device", name());
    if(!isOpen())
        vdev_id = open(name(), O_RDWR);

    if(vdev_id < 0)
    {
        OMX_DBGT_ERROR("%s::No able to open device (%s)", name(), strerror (errno));
        OMX_DBGT_EPILOG("Unable to open device");
        return -1;
    }

    OMX_DBGT_PINFO("'%s'  Configure DVB video ", name());

    // Setting the video device number to 0 for now
    if (ioctl (id(), VIDEO_SET_SYNC_GROUP, (void*)(AUDIO_SYNC_GROUP_VIDEO + 0)) != 0)
    {
        OMX_DBGT_ERROR("VIDEO_SET_SYNC_GROUP ioctl failed (%s)", strerror (errno));
        OMX_DBGT_EPILOG("Unable to configure device");
        return -1;
    }

    if (ioctl (id(), VIDEO_SET_FORMAT, (void*)(VIDEO_FORMAT_16_9)) != 0)
    {
        OMX_DBGT_WARNING("VIDEO_SET_FORMAT ioctl failed (%s)", strerror (errno));
        // The display may be disabled
    }

    if (ioctl (id(), VIDEO_SELECT_SOURCE, (void*)VIDEO_SOURCE_MEMORY) != 0)
    {
        OMX_DBGT_ERROR("VIDEO_SELECT_SOURCE ioctl failed (%s)", strerror (errno));
        OMX_DBGT_EPILOG("Unable to configure device");
        return -1;
    }

    if (ioctl (id(), VIDEO_SET_STREAMTYPE ,(void*)STREAM_TYPE_PES) != 0)
    {
        OMX_DBGT_ERROR("VIDEO_SET_STREAMTYPE ioctl failed (%s)", strerror (errno));
        OMX_DBGT_EPILOG("Unable to configure device");
        return -1;
    }

    // AVC and HEVC streams can be of 4K2K size and this requires more memory allocation
    // in the decoders. We also put in the size check

    if(m_nEncoding == VIDEO_ENCODING_H264 || m_nEncoding == VIDEO_ENCODING_HEVC)
    {
        memset(&videoCommand, 0, sizeof(struct video_command));
        videoCommand.cmd = VIDEO_CMD_SET_OPTION;
        videoCommand.option.option = DVB_OPTION_CTRL_VIDEO_MEMORY_PROFILE;
        videoCommand.option.value  = getMemProfile();

        if (ioctl (id(), VIDEO_COMMAND, (void*)&videoCommand) != 0)
        {
            OMX_DBGT_ERROR("VIDEO_COMMAND ioctl failed (%s)", strerror (errno));
            OMX_DBGT_EPILOG("Unable to configure device");
            return -1;
        }
    }

    if (ioctl (id(), VIDEO_SET_ENCODING, (void*)m_nEncoding) != 0)
    {
        OMX_DBGT_ERROR("VIDEO_SET_ENCODING ioctl failed (%s)", strerror (errno));
        OMX_DBGT_EPILOG("Unable to configure device");
        return -1;
    }

    setSync(enableSync);
    setDecodeMode(eFrameDecodeMode);

    /*Open the userdata capture device*/
    if(v4l2fd_ud == -1) {
        v4l2fd_ud = v4lopen(V4L2_CAPTURE_DRIVER_NAME, V4L2_CAPTURE_CARD_NAME, O_RDWR | O_NONBLOCK);
        if(v4l2fd_ud < 0)
        {
            OMX_DBGT_ERROR("Not able to open v4l2 capture device ");
            OMX_DBGT_EPILOG("Unable to open device");
            return -1;
        }
    }

    OMX_DBGT_EPILOG();
    return 0;
}

//==============================================================================
// DVBVideo_MOO::destroy
//==============================================================================
int DVBVideo_MOO::destroy()
{
    THREAD_GUARD(guard);
    OMX_DBGT_PROLOG();
    if (close(id()) != 0) {
        OMX_DBGT_ERROR("Video close failed (%s)", strerror (errno));
        OMX_DBGT_EPILOG("Unable to close device");
        return -1;
    }

    vdev_id = -1;

    if (close(v4l2fd_ud) != 0) {
        OMX_DBGT_ERROR("v4l2fd_ud close failed (%s)", strerror (errno));
        OMX_DBGT_EPILOG("Unable to close device");
        return -1;
    }

    v4l2fd_ud = -1;
    OMX_DBGT_EPILOG();
    return 0;
}

bool DVBVideo_MOO::isOpen() const
{ return (id() != -1); }


//==============================================================================
// DVBVideo_MOO::UseBuffer (empty function)
//==============================================================================
int DVBVideo_MOO::UseBuffer(unsigned long virtualAddr, unsigned long bufferSize,
                        int width, int height, int color,
                        unsigned long userId) const
{
#ifdef ANDROID
    assert(0);
#endif
    return 0;
}

//==============================================================================
// DVBVideo_MOO::start
//==============================================================================
int DVBVideo_MOO::start() const
{
    struct audio_command command;
    THREAD_GUARD(guard);
    OMX_DBGT_PROLOG("Starting device Video");
    assert(isOpen());
    if (this->isStarted()) {
        OMX_DBGT_EPILOG("Already started");
        return 0;
    }

    memset(&command, 0, sizeof(command));
    command.cmd      = VIDEO_CMD_SET_OPTION;
    command.u.option.option = DVB_OPTION_CTRL_REDUCE_COLLATED_DATA;
    command.u.option.value  = (enableSync)?DVB_OPTION_VALUE_DISABLE:DVB_OPTION_VALUE_ENABLE;

    if (ioctl (id(), VIDEO_COMMAND ,&command) != 0)
    {
        OMX_DBGT_ERROR("VIDEO_COMMAND REDUCE_COLLATED_DATA (%s)", strerror (errno));
        OMX_DBGT_EPILOG();
        return -1;
    }

    if (ioctl (id(), VIDEO_PLAY, NULL) != 0) {
        OMX_DBGT_ERROR("VIDEO_PLAY ioctl failed (%s)", strerror (errno));
        OMX_DBGT_EPILOG("Unable to start device");
        return -1;
    }
    this->state(stm::IDvbDevice::EState_Start);
    OMX_DBGT_EPILOG();
    return 0;
}

//==============================================================================
// DVBVideo_MOO::stop
//==============================================================================
int DVBVideo_MOO::stop() const
{
    THREAD_GUARD(guard);
    OMX_DBGT_PROLOG();
    assert(isOpen());
    if (this->isStopped()) {
        OMX_DBGT_EPILOG("Already stopped");
        return 0;
    }
    if (ioctl (id(), VIDEO_STOP, NULL) != 0) {
        OMX_DBGT_ERROR("VIDEO_STOP ioctl failed (%s)", strerror (errno));
        OMX_DBGT_EPILOG("Unable to stop device");
        return -1;
    }
    this->state(stm::IDvbDevice::EState_Stop);
    OMX_DBGT_EPILOG();
    return 0;
}

//==============================================================================
// DVBVideo_MOO::pause
//==============================================================================
int DVBVideo_MOO::pause(bool hw) const
{
    THREAD_GUARD(guard);
    OMX_DBGT_PROLOG();
    assert(isOpen());
    if (this->isStopped()) {
        OMX_DBGT_WARNING("Invalid state change");
        OMX_DBGT_EPILOG("Can not pause a stopped device");
        return 0;
    }
    if (this->isPaused()) {
        OMX_DBGT_EPILOG("Already paused");
        return 0;
    }
    if (hw)
    if (ioctl (id(), VIDEO_FREEZE, NULL) != 0) {
        OMX_DBGT_ERROR("VIDEO_FREEZE ioctl failed (%s)", strerror (errno));
        OMX_DBGT_EPILOG("Unable to pause device");
        return -1;
    }
    this->state(stm::IDvbDevice::EState_Pause);
    OMX_DBGT_EPILOG();
    return 0;
}

//==============================================================================
// DVBVideo_MOO::resume
//==============================================================================
int DVBVideo_MOO::resume(bool hw) const
{
    THREAD_GUARD(guard);
    OMX_DBGT_PROLOG();
    assert(isOpen());
    if (this->isStopped()) {
        OMX_DBGT_WARNING("Invalid state change");
        OMX_DBGT_EPILOG("Can not resume a stopped device");
        return 0;
    }
    if (!this->isPaused()) {
        OMX_DBGT_EPILOG("Already resumed");
        return 0;
    }
    if (hw)
    if (ioctl (id(), VIDEO_CONTINUE, NULL) != 0) {
        OMX_DBGT_ERROR("VIDEO_CONTINUE ioctl failed (%s)", strerror (errno));
        OMX_DBGT_EPILOG("Unable to resume device");
        return -1;
    }
    this->state(stm::IDvbDevice::EState_Start);
    OMX_DBGT_EPILOG();
    return 0;
}

//=====================================================================================
// DVBVideo_MOO::lock
//=====================================================================================
void DVBVideo_MOO::lock() const
{
    OMX_DBGT_PROLOG();
    m_mutexCmd2.lock();
}

//=====================================================================================
// DVBVideo_MOO::unlock
//=====================================================================================
void DVBVideo_MOO::unlock() const
{
    OMX_DBGT_PROLOG();
    m_mutexCmd2.release();
}

//=====================================================================================
// DVBVideo_MOO::unlock
//=====================================================================================
void DVBVideo_MOO::resetflags() const
{
    if (m_bClearWrite || m_bFlushWrite) {
        OMX_DBGT_PROLOG("step 1");

        m_bClearWrite = false;
        m_bClearWrite2 = true;
        m_bFlushWrite = false;

        OMX_DBGT_EPILOG();
    } else if (m_bClearWrite2) {
        OMX_DBGT_PROLOG("step 2");

        m_bClearWrite2 = false;

        OMX_DBGT_EPILOG();
    }
}

//==============================================================================
// DVBVideo_MOO::write
// send frame to decode
//==============================================================================
int DVBVideo_MOO::write(const void* data_ptr, unsigned int len) const
{
    THREAD_GUARD(guard);
    OMX_DBGT_PROLOG("Injecting data in DVB Video");
    assert(isOpen());

    // Block request if in pause state
    if (m_bClearWrite2 == false)
    while (this->state() == EState_Pause) {
        OMX_DBGT_PINFO("Blocking write on a paused device");
        // Check if device was cleared in the meantime
        if (m_bClearWrite == true || m_bFlushWrite == true) {
            OMX_DBGT_EPILOG("Data cleared");
            return -ECANCELED;
        }
        m_condState.wait(m_mutexCmd);
        // Check if device was cleared in the meantime
        if (m_bClearWrite == true || m_bFlushWrite == true) {
            OMX_DBGT_EPILOG("Data cleared");
            return -ECANCELED;
        }
    }

    if (this->isStopped()) {
        OMX_DBGT_WARNING("Writing on a stopped device");
        OMX_DBGT_EPILOG("Data skipped");
        return -EAGAIN;
    }

    int retval = ::write(id(), data_ptr, (size_t)len);
    if (retval < 0) {
        OMX_DBGT_ERROR("Error when writing in video device (%s)", strerror(errno));
        OMX_DBGT_EPILOG("Write error");
        return -errno;
    }
    assert(retval >= 0);
    if (len != (unsigned int)retval) {
        OMX_DBGT_ERROR("Error writing Video content write=%lu != written=%d",
                   (unsigned long)len, retval);
        OMX_DBGT_EPILOG("Write error");
        return -errno;
    }
    OMX_DBGT_EPILOG();
    return 0;
}

//==============================================================================
// DVBVideo_MOO::flush
//==============================================================================
int DVBVideo_MOO::flush(bool bEoS) const
{
    THREAD_GUARD(guard);
    OMX_DBGT_PROLOG();
    assert(isOpen());
    if (this->isStopped()) {
        OMX_DBGT_EPILOG("Flushing a stopped device");
        return 0;
    }
    if (bEoS) {
        // Inject EOS discontinuity.
        if (ioctl(id(), VIDEO_DISCONTINUITY, (void*)DVB_DISCONTINUITY_EOS) != 0)
        {
            OMX_DBGT_ERROR("VIDEO_DISCONTINUITY ioctl failed (%s)",
                       strerror (errno));
            OMX_DBGT_EPILOG();
            return -1;
        }
    }
    // Wait till pipeline is flushed
    if (ioctl(id(), VIDEO_FLUSH, NULL) != 0) {
        OMX_DBGT_ERROR("VIDEO_FLUSH ioctl failed (%s)", strerror (errno));
        OMX_DBGT_EPILOG();
        return -1;
    }
    // Trigger state with same value to unblock any write
    m_bFlushWrite = true;
    this->state(this->state());
    OMX_DBGT_EPILOG();
    return 0;
}

//==============================================================================
// DVBVideo_MOO::clear
//==============================================================================
int DVBVideo_MOO::clear() const
{
    THREAD_GUARD(guard);
    OMX_DBGT_PROLOG();
    assert(isOpen());
    if (this->isStopped()) {
        OMX_DBGT_EPILOG("Clear on a stopped device");
        return 0;
    }
    // Flush the current stream from the pipeline.  This is accomplished
    // by draining the stream with discard and indicating that a
    // discontinuity will occur
    if (ioctl(id(), VIDEO_CLEAR_BUFFER, NULL) != 0) {
        OMX_DBGT_ERROR("VIDEO_CLEAR_BUFFER ioctl failed (%s)", strerror (errno));
        OMX_DBGT_EPILOG();
        return -1;
    }

    // Trigger state with same value to unblock any write
    m_bClearWrite = true;
    this->state(this->state());
    OMX_DBGT_EPILOG();
    return 0;
}

// Todo shall be exported in dvb headers
#define INVALID_TIME    0xfeedfacedeadbeefull

//==============================================================================
// DVBVideo_MOO::getTime
//==============================================================================
uint64_t DVBVideo_MOO::getTime() const
{
    OMX_DBGT_PROLOG();
    static uint64_t LastInvalidTime = (uint64_t)-1ll;
    video_play_info_t VideoPlayInfo;
    assert(isOpen());
    if (ioctl(id(), VIDEO_GET_PLAY_INFO, (void*)&VideoPlayInfo) != 0)
    {
        OMX_DBGT_ERROR("VIDEO_GET_PLAY_INFO ioctl failed (%s)", strerror (errno));
        return 0;
    }
    OMX_DBGT_PDEBUG("Get time info on video device "
                "sys_time=%llu pres_time=%llu pts=%llu" ,
                VideoPlayInfo.system_time,
                VideoPlayInfo.presentation_time,
                VideoPlayInfo.pts);

    if ((VideoPlayInfo.pts == INVALID_TIME) || (VideoPlayInfo.pts == LastInvalidTime)) {
        OMX_DBGT_EPILOG("Invalid video media time");
        return 0;
    }

    if (VideoPlayInfo.presentation_time == INVALID_TIME) {
        LastInvalidTime = VideoPlayInfo.pts;
        OMX_DBGT_EPILOG("Invalid video media time");
        return 0;
    }

    uint64_t vidtime = (VideoPlayInfo.pts * 100) / 9; // pts unit is 90 kHz ticks
    OMX_DBGT_EPILOG();
    return vidtime;
}


//==============================================================================
// DVBVideo_MOO::getSamplingRate (empty function)
//==============================================================================
uint32_t DVBVideo_MOO::getSamplingRate() const
{
    assert(0);
    return 0;
}
//==============================================================================
// DVBVideo_MOO::FillThisBuffer (empty function)
//==============================================================================
int DVBVideo_MOO::FillThisBuffer(unsigned long userId) const
{
    assert(0);
    return 0;
}

//=============================================================================
// DVBVideo_MOO::FillBufferDone (empty function)
//=============================================================================
int DVBVideo_MOO::FillBufferDone(unsigned long &userId, uint64_t &TS,
                             bool & eos) const
{
    assert(0);
    return 0;
}

//==============================================================================
// DVBVideo_MOO::FlushBuffer (empty function)
//==============================================================================
unsigned long DVBVideo_MOO::FlushBuffer() const
{
    assert(0);
    return 0;
}

//=============================================================================
// DVBVideo_MOO::QueueBuffer
//=============================================================================
int DVBVideo_MOO::QueueBuffer(void* BufferData, int &nBytesSize, uint64_t &ts) const
{
    struct v4l2_buffer buf;
    memset(&buf, 0, sizeof(buf));

    buf.type                 = V4L2_BUF_TYPE_USER_DATA_CAPTURE;
    buf.memory               = V4L2_MEMORY_USERPTR;
    buf.m.userptr            = (unsigned long)BufferData;
    buf.field                = V4L2_FIELD_ANY;
    buf.length               = nBytesSize;

    if (ioctl (v4l2fd_ud, VIDIOC_QBUF, &buf) < 0)
    {
        OMX_DBGT_ERROR("Buffer queue (VIDIOC_QBUF) for userdata failed (%s)", strerror (errno));
        OMX_DBGT_EPILOG();
        return -1;
    }

    memset(&buf, 0, sizeof(buf));
    buf.type     = V4L2_BUF_TYPE_USER_DATA_CAPTURE;
    buf.memory   = V4L2_MEMORY_USERPTR;
 
    int ret = ioctl (v4l2fd_ud, VIDIOC_DQBUF, &buf);
    int counter = 0;

    // remove counter (300) to exit of the loop for the following case : 
    // end of playback (no more decoded buffer received)
    // but sometime in this case the pulling thread is not blocked on
    // on DQBUF until the stop closed the fd or a seek backward inject
    // new buffer to decode

    while ((ret == -1) && (errno == EAGAIN))
    {
        ret = ioctl (v4l2fd_ud, VIDIOC_DQBUF, &buf);
        usleep(10000);
        counter++;
    }

    if (ret < 0)
    {
        if(errno == 19)
        {
            OMX_DBGT_PDEBUG("DVBVideo_moo::v4lQueueBuffer - EOS marker received");
            buf.bytesused = 0;
            nBytesSize = buf.bytesused;
            ts = (uint64_t)buf.timestamp.tv_sec * 1000000 + (uint64_t)buf.timestamp.tv_usec;
            OMX_DBGT_EPILOG();
            return 0;
        }
        else
        {
            OMX_DBGT_ERROR("Failed (VIDIOC_DQBUF) %d %d (%s)",ret, errno, strerror (errno));
            OMX_DBGT_EPILOG();
            return -1;
        }
    }

    nBytesSize = buf.bytesused;

    ts = (uint64_t)buf.timestamp.tv_sec * 1000000 + (uint64_t)buf.timestamp.tv_usec;

    OMX_DBGT_EPILOG();
    return 0;
}

//=============================================================================
// DVBVideo_MOO::v4l2Start
//=============================================================================
int DVBVideo_MOO::v4l2Start() const
{
    struct v4l2_buffer buf;
    struct v4l2_format fmt;
    struct v4l2_requestbuffers reqbuf;
    memset(&fmt, 0, sizeof(fmt));
    fmt.type = V4L2_BUF_TYPE_USER_DATA_CAPTURE;
    if(ioctl(v4l2fd_ud, VIDIOC_S_FMT,&fmt) < 0)
    {
        OMX_DBGT_ERROR("VIDEO_S_FMT ioctl failed (%s)", strerror (errno));
        OMX_DBGT_EPILOG("Unable to configure device");
        return -1;
    }

    memset(&reqbuf, 0, sizeof(reqbuf));
    reqbuf.memory = V4L2_MEMORY_USERPTR;
    reqbuf.type = V4L2_BUF_TYPE_USER_DATA_CAPTURE;
    reqbuf.count = 1;
    if(ioctl(v4l2fd_ud, VIDIOC_REQBUFS, &reqbuf) < 0)
    {
        OMX_DBGT_ERROR("VIDEO_REQBUFS ioctl failed (%s)", strerror (errno));
        OMX_DBGT_EPILOG("Unable to configure device");
        return -1;
    }

    memset(&buf, 0, sizeof(buf));

    buf.type = V4L2_BUF_TYPE_USER_DATA_CAPTURE;
    if(ioctl(v4l2fd_ud, VIDIOC_STREAMON, &buf.type) < 0)
    {
        OMX_DBGT_ERROR("Stream %u (VIDIOC_STREAMON) failed (%s)",
                        v4l2fd_ud, strerror (errno));
        OMX_DBGT_EPILOG();
        return -1;
    }

    return 0;
}

//=============================================================================
// DVBVideo_MOO::v4l2Stop
//=============================================================================
int DVBVideo_MOO::v4l2Stop() const
{
    struct v4l2_buffer buf;
    memset(&buf, 0, sizeof(buf));
    buf.type = V4L2_BUF_TYPE_USER_DATA_CAPTURE;
    if(ioctl(v4l2fd_ud, VIDIOC_STREAMOFF, &buf.type) < 0)
    {
        OMX_DBGT_ERROR("Stream %u (VIDIOC_STREAMOFF) failed (%s)",
                        v4l2fd_ud, strerror (errno));
        OMX_DBGT_EPILOG();
        return -1;
    }

    return 0;
}
