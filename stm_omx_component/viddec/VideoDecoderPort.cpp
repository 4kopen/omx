/*
 * Copyright 2013 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file   VideoDecoderPort.cpp
 * @author STMicroelectronics
 */

#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include "VideoDecoderPort.hpp"
#include "VideoDecoder.hpp"
#include "VideoPesAdapter.hpp"

#include "OMX_Video.h"
#include "OMX_VideoExt.h"
#include "OMX_CoreExt.h"
#include "OMX_debug.h"
#include "videocc.hpp"
#include <linux/videodev2.h>
#include <IDvbDevice.hpp>
#include <omxse.h>
#include "link.hpp"
#include "pes.hpp"

#if HAVE_ASSERT_H
#include <assert.h>
#endif

#if HAVE_DBGTASSERT_H
// Re-direct assert on DBGT_ASSERT macro
#define assert(expr)  OMX_DBGT_ASSERT(expr)
#endif

#define DBGT_PREFIX "VDECP"
#define DBGT_LAYER 0
#include <linux/dbgt.h>

#define CONFIG_BUF_SIZE (128 * 1024)

#define PTS_CORRECTION(x) (((x) * 90)/1000)
#define NUM_IN_BUFFERS 2
#define NUM_OUT_BUFFERS 4
#define NUM_OUT_BUFFERS_FULL_PIPE 3

#define VGA_WIDTH  640
#define VGA_HEIGHT 480

#define MAX_SUPPORTED_RESOLUTION_WIDTH_4K 4096
#define MAX_SUPPORTED_RESOLUTION_HEIGHT_2K 2048
#define MAX_SUPPORTED_RESOLUTION_WIDTH 1920
#define MAX_SUPPORTED_RESOLUTION_HEIGHT 1080


namespace stm {

//==========================================================================
// VidDecPort::VidDecPort
// port constructor of OMX component
//==========================================================================

VidDecPort::VidDecPort(unsigned int nIdx, OMX_DIRTYPE aDir,
                       const VDECComponent& owner)
    :OmxPort(),
     m_nThreadState(EThreadState_Stopped),
     m_mutexThread(),
     m_condState(),
     m_owner(owner),
     m_PortDefinition(nIdx, aDir)
{
    //check the string length before copy
    assert(strlen(this->viddec().name()) <= sizeof(m_strName));
    strcpy(m_strName,this->viddec().name());
    OMXDEBUG_UPDATE_AV_TRACE_SETTINGS(this->viddec().name());
}

VidDecPort::~VidDecPort()
{}

const VideoPortDefinition& VidDecPort::definition() const
{ return m_PortDefinition; }

VideoPortDefinition& VidDecPort::definition()
{ return m_PortDefinition; }

const OmxComponent& VidDecPort::component() const
{ return m_owner; }

VidDecPort::EThreadState VidDecPort::threadState() const
{ return m_nThreadState; }

void VidDecPort::threadState(EThreadState st)
{
    m_mutexThread.lock();
    m_nThreadState = st;
    m_mutexThread.release();
    m_condState.signal();
}

//==========================================================================
// VidDecOutputPort implementation
//==========================================================================

// BGRA8888 is the prefered pixel format returned in OMX output formats
const VidDecOutputPort::ColorInfo VidDecOutputPort::m_colorInfos[] = {
#ifdef ANDROID
    {OMX_STM_COLOR_FormatYVU420SemiPlanarNV21, HAL_PIXEL_FORMAT_YCrCb_420_SP, V4L2_PIX_FMT_NV21, OMXSE_COLOR_NV21, 12, 8},
    {OMX_COLOR_Format32bitBGRA8888, HAL_PIXEL_FORMAT_BGRA_8888, V4L2_PIX_FMT_BGR32, OMXSE_COLOR_BGRA8888, 32, 32},
    {OMX_COLOR_FormatYUV420SemiPlanar, HAL_PIXEL_FORMAT_YCrCb_420_SP, V4L2_PIX_FMT_NV12, OMXSE_COLOR_NV12, 12, 8},
    {OMX_COLOR_Format16bitRGB565,  HAL_PIXEL_FORMAT_RGB_565, V4L2_PIX_FMT_RGB565, OMXSE_COLOR_RGB565, 16, 8},
#else
    {OMX_STM_COLOR_FormatYVU420SemiPlanarNV21, OMX_STM_COLOR_FormatYVU420SemiPlanarNV21, V4L2_PIX_FMT_NV21, OMXSE_COLOR_NV21, 12, 8},
    {OMX_COLOR_Format32bitBGRA8888, OMX_COLOR_Format32bitBGRA8888, V4L2_PIX_FMT_BGR32, OMXSE_COLOR_BGRA8888, 32, 32},
    {OMX_COLOR_FormatYUV420SemiPlanar, OMX_COLOR_FormatYUV420SemiPlanar, V4L2_PIX_FMT_NV12, OMXSE_COLOR_NV12, 12, 8},
    {OMX_COLOR_Format16bitRGB565,  OMX_COLOR_Format16bitRGB565, V4L2_PIX_FMT_RGB565, OMXSE_COLOR_RGB565, 16, 8},
#endif
};

VidDecOutputPort::VidDecOutputPort(unsigned int nIdx, OMX_DIRTYPE aDir,
                                 const VDECComponent& owner)
    :VidDecPort(nIdx, aDir, owner),
     puller_thread_id(0),
     userdata_thread_id(0),
     m_bUDEnabled(false),
     m_bNativeWindowsEnabled(false),
     m_nReceivedFrame(0),
     m_bAfterEosFlag(false),
     m_isEOSnFlagsSet(false),
     m_mutexSync()
{
    this->definition().bufferCountMin(NUM_OUT_BUFFERS);
    this->definition().bufferCountActual(NUM_OUT_BUFFERS);

    // Force NV12 here as a workaround for VLC:
    // None of VLC nor Stagefright negotiate codec output pixel format
    // - VLC will only do some getParam(PortDef), so
    //   format returned to VLC will be the default one (NV12 forced here).
    // - Stagefright/OMXCodec.cpp will do first getParam(PortFormat)
    //   & setParam(PortFormat) to have the codec supported format, then
    //   will do getParam(portDef) to have the codec supported pixel format.
    //   In this file, first format given in format list is BGRA8888, see (1),
    //   so getParam(PortFormat) returns BGRA8888, then SF calls
    //   setParam(PortFormat) with BGRA8888 which is so initializing
    //   internal PortDef & internalColorIndex to BGRA8888, see (2) and (3)
    // Final fix should be to create 2 components one for each format, TBC
    const OMX_COLOR_FORMATTYPE kDefaultColor = OMX_COLOR_FormatYUV420SemiPlanar;
    this->colorFormat(kDefaultColor);
    OMX_DBGT_PINFO("%s Default color format is %s (eColorFormat=%d(0x%x)) "
               "(STOMXComponentAsync::create)",
               component().name(),
               this->definition().contentType(),
               this->definition().colorFormat(),
               this->definition().colorFormat());

   OMX_CONF_INIT_STRUCT_PTR(&m_prepareForAdaptivePlayback, OMX_VIDEO_PARAM_PREPARE_FOR_ADAPTIVE_PLAYBACK);

}

VidDecOutputPort::~VidDecOutputPort()
{}

OMX_ERRORTYPE VidDecOutputPort::create()
{ return OMX_ErrorNone; }

int VidDecOutputPort::start()
{
    // Nothing to do on output port if tunneled
    if (this->isTunneled()) {
        puller_thread_id = 0;
    } else {
        OMX_DBGT_CHECK_RETURN(! pthread_create(&puller_thread_id, NULL, VidDecOutputPort::ThreadEntryPoint, this), OMX_ErrorInsufficientResources);
#ifdef ANDROID
        OMX_DBGT_CHECK_RETURN(! pthread_setname_np(puller_thread_id, "OMXVideoPull"), OMX_ErrorInsufficientResources);
#endif
    }
    if(m_bUDEnabled && (userdata_thread_id == 0))
    {
        // User data acapture enabled but not started yet
        // So start it.
        udEnableCapture(m_bUDEnabled);
    }
    this->threadState(EThreadState_Running);
    return 0;
}

void VidDecOutputPort::destroy()
{
    OMX_DBGT_PROLOG();
    if (puller_thread_id != 0) {
        pthread_join(puller_thread_id, 0);
        puller_thread_id = 0;
    }

    if (userdata_thread_id != 0) {
        this->viddec().dvb().v4l2Stop();
        pthread_join(userdata_thread_id, 0);
        userdata_thread_id = 0;
    }
    if(this->nbBuffers() > 0) {
        OMX_DBGT_ERROR("Allocated buffer on output port not freed");
    }

    m_nReceivedFrame = 0;
    m_bAfterEosFlag  = false;
    m_isEOSnFlagsSet  = false;

    OMX_DBGT_EPILOG();
}

void* VidDecOutputPort::ThreadEntryPoint(void* a_pThis)
{
    VidDecOutputPort* pThis = static_cast<VidDecOutputPort*>(a_pThis);
    pThis->threadFunction();
    return 0;
}

void* VidDecOutputPort::udThreadEntryPoint(void* a_pThis)
{
    VidDecOutputPort* pThis = static_cast<VidDecOutputPort*>(a_pThis);
    pThis->udThreadFunction();
    return 0;
}

OMX_COLOR_FORMATTYPE
VidDecOutputPort::supportedColorFormat(unsigned int nIdx) const
{
    assert(nIdx < this->supportedColorFormatNb());
    return m_colorInfos[nIdx].OMXType;
}

unsigned int
VidDecOutputPort::supportedColorFormatNb() const
{ return sizeof(m_colorInfos) / sizeof(m_colorInfos[0]); }

const VidDecOutputPort::ColorInfo& VidDecOutputPort::colorInfo() const
{
    OMX_COLOR_FORMATTYPE cFmt = this->definition().colorFormat();
    assert(cFmt != OMX_COLOR_FormatUnused);
    assert(isSupportedColorFormat(cFmt));
    unsigned int i = 0;
    for (i = 0; i < this->supportedColorFormatNb(); i++) {
        if (m_colorInfos[i].OMXType == cFmt)
            break;
    }
    return m_colorInfos[i];
}

bool VidDecOutputPort::isSupportedColorFormat(OMX_COLOR_FORMATTYPE aFmt) const
{
    bool ret = false;
    for (unsigned int i = 0; i < this->supportedColorFormatNb(); i++)
    {
        if (m_colorInfos[i].OMXType == aFmt) {
            ret = true;
            break;
        }
    }
    return ret;
}

void VidDecOutputPort::colorFormat(OMX_COLOR_FORMATTYPE aFmt)
{
    assert(isSupportedColorFormat(aFmt));
    this->definition().colorFormat(aFmt);
}

OMX_ERRORTYPE
VidDecOutputPort::getParameter(OMX_INDEXTYPE nParamIndex, OMX_PTR pParamStruct)
{
    OMX_DBGT_PROLOG("on Video decoder output port");
    switch ((int)nParamIndex)
    {
    case OMX_IndexParamPortDefinition: {
        // Port definition
        ASSERT_STRUCT_TYPE(pParamStruct, OMX_PARAM_PORTDEFINITIONTYPE);
        OMX_PARAM_PORTDEFINITIONTYPE* param;
        param = (OMX_PARAM_PORTDEFINITIONTYPE*)pParamStruct;
        assert(param->nPortIndex == this->definition().index());

        // Provide current content of sPorts and computes nBufferSize on top of it
        OMX_DBGT_PDEBUG("GetParameter(OMX_IndexParamPortDefinition) nPortIndex: "
                    "%u eDir: %d nBufferCountActual: %u nBufferCountMin: %u nBufferSize: %u\n",
                    this->definition().index(),
                    this->definition().direction(),
                    this->definition().bufferCountActual(),
                    this->definition().bufferCountMin(),
                    this->definition().bufferSize());

        unsigned int nBufferSz = 0;

        unsigned int alignedWidth = ((this->definition().frameWidth() + 0xf) >> 4) << 4;
        unsigned int tempStride =  alignedWidth *  (this->colorInfo().bpp / 8);
        OMX_DBGT_PDEBUG("Actual width : %d Aligned width : %d Height : %d Stride : %d",this->definition().frameWidth(),alignedWidth,this->definition().frameHeight(),tempStride);

        if ((this->definition().frameWidth() != 0)
            && (this->definition().frameHeight() != 0)
            && (this->definition().colorFormat() != OMX_COLOR_FormatUnused))
        {
            nBufferSz = (alignedWidth *  this->colorInfo().bpp) / 8
                * this->definition().frameHeight();
            OMX_DBGT_PDEBUG("nBufferSz : %d",nBufferSz);
        }
        if (this->definition().bufferSize() == 0) {
            // Update internal structure
            this->definition().stride(tempStride);
            this->definition().bufferSize(nBufferSz);
            OMX_DBGT_PINFO("%s set nBufferSize to %uKB (wxh=%ux%u, bpp=%u  "
                       "(GetParameter(OMX_IndexParamPortDefinition output)))",
                       component().name(), nBufferSz/1024,
                       this->definition().frameWidth(),
                       this->definition().frameHeight(),
                       this->colorInfo().bpp);
        } else {
            if (param->nBufferSize != nBufferSz)
            {
                OMX_DBGT_PINFO("%s nBufferSize from client %u does not match "
                           "computed %u (GetParameter(OMX_IndexParamPortDefinition output)) ",
                           component().name(),
                           param->nBufferSize,
                           this->definition().bufferSize());
            }
        }
        *param = this->definition().getParamPortDef();
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }; break;

    case OMX_IndexParamVideoPortFormat: {
        // Supported video port format
        ASSERT_STRUCT_TYPE(pParamStruct, OMX_VIDEO_PARAM_PORTFORMATTYPE);
        OMX_VIDEO_PARAM_PORTFORMATTYPE* param;
        param = (OMX_VIDEO_PARAM_PORTFORMATTYPE*)pParamStruct;
        assert(param->nPortIndex == this->definition().index());
        OMX_DBGT_PDEBUG("::OMX_IndexParamVideoPortFormat:");
        if (param->nIndex < this->supportedColorFormatNb()) {
            param->eCompressionFormat = this->definition().compressionFormat();
            param->xFramerate = this->definition().frameRate();
            param->eColorFormat = supportedColorFormat(param->nIndex);
            OMX_DBGT_EPILOG();
            return OMX_ErrorNone;
        } else {
            OMX_DBGT_EPILOG("No more supported format");
            return OMX_ErrorNoMore;
        }
    }; break;

#ifdef ANDROID
    case OMX_getAndroidNativeBufferUsage: {
        OMX_DBGT_PDEBUG("VDECComponent::output port get parameter OMX_getAndroidNativeBufferUsage");
        OMX_GetAndroidNativeBufferUsageParams *param = (OMX_GetAndroidNativeBufferUsageParams *)pParamStruct;
        param->nUsage = GRALLOC_USAGE_HW_2D;
        param->nUsage |= GRALLOC_USAGE_EXTERNAL_DISP;
        param->nUsage |= GRALLOC_USAGE_SW_READ_RARELY;
        param->nUsage |= GRALLOC_USAGE_SW_WRITE_MASK ;
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }; break;
#endif

    case OMX_IndexConfigCommonOutputCrop: {
        ASSERT_STRUCT_TYPE(pParamStruct, OMX_CONFIG_RECTTYPE);
        OMX_CONFIG_RECTTYPE* config;
        config = (OMX_CONFIG_RECTTYPE*)pParamStruct;
        assert(config->nPortIndex == this->definition().index());
        config->nLeft = 0;
        config->nTop = 0;
        config->nWidth = this->definition().frameWidth();
        config->nHeight = this->definition().frameHeight();
        OMX_DBGT_PDEBUG("OMX_IndexConfigCommonOutputCrop -> w:%u h:%u)",
                    (unsigned int)config->nWidth,
                    (unsigned int)config->nHeight);
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }; break;

    case OMX_IndexExtParamPrepareForAdaptivePlayback: {
        ASSERT_STRUCT_TYPE(pParamStruct, OMX_VIDEO_PARAM_PREPARE_FOR_ADAPTIVE_PLAYBACK);
        OMX_VIDEO_PARAM_PREPARE_FOR_ADAPTIVE_PLAYBACK* param;
        param = (OMX_VIDEO_PARAM_PREPARE_FOR_ADAPTIVE_PLAYBACK*)pParamStruct;
        assert(param->nPortIndex == this->definition().index());
        memcpy(param, &m_prepareForAdaptivePlayback, sizeof(m_prepareForAdaptivePlayback));
        OMX_DBGT_PDEBUG("OMX_IndexExtParamPrepareForAdaptivePlayback -> h:%u w:%u)",
                    (unsigned int)param->nMaxFrameHeight,
                    (unsigned int)param->nMaxFrameWidth);
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }; break;

    case OMX_IndexParamOtherPortFormat:
    case OMX_IndexParamAudioPortFormat:
    case OMX_IndexParamImagePortFormat:
    default: {
        OMX_DBGT_ERROR("Unsupported index(%#x)",nParamIndex);
        OMX_DBGT_EPILOG("Unsupported index");
        return OMX_ErrorUnsupportedIndex;
    }
    }
}

OMX_ERRORTYPE
VidDecOutputPort::udEnableCapture(bool bEnable)
{
    if(bEnable) {
        if(this->viddec().dvb().v4l2Start() == 0)
        {
            OMX_DBGT_CHECK_RETURN(! pthread_create(&userdata_thread_id, NULL,
                VidDecOutputPort::udThreadEntryPoint, this), OMX_ErrorInsufficientResources);
        }
    } else {
        if(userdata_thread_id){
            // destroy  userdata puller thread
            this->viddec().dvb().v4l2Stop();
            pthread_join(userdata_thread_id,0);
            userdata_thread_id = 0;
        }
    }
    return OMX_ErrorNone;
}

OMX_ERRORTYPE
VidDecOutputPort::setParameter(OMX_INDEXTYPE nParamIndex, OMX_PTR pParamStruct)
{
    OMX_DBGT_PROLOG("on Video decoder ouptut port");
    switch ((int)nParamIndex)
    {
    case OMX_IndexParamVideoPortFormat: {
        // Set video port format
        ASSERT_STRUCT_TYPE(pParamStruct, OMX_VIDEO_PARAM_PORTFORMATTYPE);
        OMX_VIDEO_PARAM_PORTFORMATTYPE* param;
        param = (OMX_VIDEO_PARAM_PORTFORMATTYPE*)pParamStruct;
        assert(param->nPortIndex == this->definition().index());

        if (!isSupportedColorFormat(param->eColorFormat)) {
            OMX_DBGT_ERROR("%s eColorFormat not supported (%d)",
                       component().name(), param->eColorFormat);
            OMX_DBGT_EPILOG("Unsupported video color format");
            return OMX_ErrorUnsupportedSetting;
        }

        OMX_DBGT_PDEBUG("OMX_IndexParamVideoPortFormat:"
                    "compression format %d->%d ,"
                    "color format %s (eColorFormat=%d(0x%x)) -> (eColorFormat=%d(0x%x)) ,"
                    "frame rate %d -> %d",
                    this->definition().compressionFormat(), param->eCompressionFormat,
                    this->definition().contentType(),
                    this->definition().colorFormat(),
                    this->definition().colorFormat(),
                    param->eColorFormat, param->eColorFormat,
                    this->definition().frameRate(), param->xFramerate);

        // Apply format on port
        this->colorFormat(param->eColorFormat);
        this->definition().frameRate(param->xFramerate);

        OMX_DBGT_EPILOG("Set port color format %u", param->eColorFormat);
        return OMX_ErrorNone;
    }; break;

    case OMX_IndexParamPortDefinition: {
        unsigned int MaxWidthSupported,MaxHeightSupported;

        // Port definition
        ASSERT_STRUCT_TYPE(pParamStruct, OMX_PARAM_PORTDEFINITIONTYPE);
        OMX_PARAM_PORTDEFINITIONTYPE* param;
        param = (OMX_PARAM_PORTDEFINITIONTYPE*)pParamStruct;
        assert(param->nPortIndex == this->definition().index());

        if (param->nPortIndex == 0)
        {

        if ((viddec().inputPort().definition().compressionFormat() == OMX_VIDEO_CodingAVC) ||
            (viddec().inputPort().definition().compressionFormat() == (OMX_VIDEO_CODINGTYPE)OMX_VIDEO_CodingHEVC))
        {
            MaxWidthSupported = MAX_SUPPORTED_RESOLUTION_WIDTH_4K;
            MaxHeightSupported = MAX_SUPPORTED_RESOLUTION_HEIGHT_2K;
        }
        else
        {
            MaxWidthSupported = MAX_SUPPORTED_RESOLUTION_WIDTH;
            MaxHeightSupported = MAX_SUPPORTED_RESOLUTION_HEIGHT;
        }

        // formats above 1920 * 1080 not supported
        if (param->format.video.nFrameWidth > MaxWidthSupported || param->format.video.nFrameHeight > MaxHeightSupported)
        {
            OMX_DBGT_ERROR(" %s  Wrong SIZE %u %u \n", component().name(), (unsigned int)param->format.video.nFrameWidth,(unsigned int)param->format.video.nFrameHeight);
            OMX_DBGT_EPILOG();
            return OMX_ErrorUnsupportedSetting;
        }
        }

        if (this->definition().bufferSize() != param->nBufferSize) {
            OMX_DBGT_PINFO("%s changing nBufferSize from %uKB to %uKB "
                       "(SetParameter(OMX_IndexParamPortDefinition output))",
                       component().name(),
                       this->definition().bufferSize()/1024,
                       param->nBufferSize/1024);
            this->definition().bufferSize(param->nBufferSize);
        }
        this->definition().setParamPortDef(param);
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }; break;
    case OMX_IndexExtVideoUserData: {
        ASSERT_STRUCT_TYPE(pParamStruct, OMX_VIDEO_CONFIG_USERDATA);
        OMX_VIDEO_CONFIG_USERDATA *param = (OMX_VIDEO_CONFIG_USERDATA*)pParamStruct;
        if(param->bEnable)
        {
            m_bUDEnabled = true;
            if(userdata_thread_id == 0)
            {
                // we can start the capture device
                // only if the input is running else log the request
                if(this->threadState() == EThreadState_Stopped)
                {
                    //we are in stopped state yet.
                    //so return
                    OMX_DBGT_EPILOG();
                    return OMX_ErrorNone;
                }
                udEnableCapture(param->bEnable);
            }
        } else
        {
            udEnableCapture(param->bEnable);
        }
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }; break;

#ifdef ANDROID
    case OMX_enableAndroidNativeBuffers: {
        if (viddec().grallocModule() == NULL ||
            viddec().grallocDevice() == NULL) {
            OMX_DBGT_ERROR("%s NativeWindows asked, but gralloc module not present",
                       component().name());
            OMX_DBGT_EPILOG();
            return OMX_ErrorUnsupportedSetting;
        }

        m_bNativeWindowsEnabled = true;
        // Do not saturate memory for thumbnails
        this->definition().bufferCountMin(NUM_OUT_BUFFERS_FULL_PIPE);
        this->definition().bufferCountActual(NUM_OUT_BUFFERS_FULL_PIPE);
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }; break;
#endif

    case OMX_IndexExtParamPrepareForAdaptivePlayback: {
        ASSERT_STRUCT_TYPE(pParamStruct, OMX_VIDEO_PARAM_PREPARE_FOR_ADAPTIVE_PLAYBACK);
        OMX_VIDEO_PARAM_PREPARE_FOR_ADAPTIVE_PLAYBACK* param = (OMX_VIDEO_PARAM_PREPARE_FOR_ADAPTIVE_PLAYBACK*)pParamStruct;
        memcpy(&m_prepareForAdaptivePlayback, param, sizeof(m_prepareForAdaptivePlayback));
        OMX_DBGT_PDEBUG("OMX_IndexExtParamPrepareForAdaptivePlayback -> h:%u w:%u)",
                    (unsigned int)m_prepareForAdaptivePlayback.nMaxFrameHeight,
                    (unsigned int)m_prepareForAdaptivePlayback.nMaxFrameWidth);
        if(m_prepareForAdaptivePlayback.bEnable &&
            (m_prepareForAdaptivePlayback.nMaxFrameWidth != 0) &&
            (m_prepareForAdaptivePlayback.nMaxFrameHeight != 0)){
                this->definition().frameWidth(m_prepareForAdaptivePlayback.nMaxFrameWidth);
                this->definition().frameHeight(m_prepareForAdaptivePlayback.nMaxFrameHeight);
        }
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }; break;

    case OMX_IndexParamOtherPortFormat:
    case OMX_IndexParamAudioPortFormat:
    case OMX_IndexParamImagePortFormat:
    default: {
        OMX_DBGT_ERROR("Unsupported index(%#x)",nParamIndex);
        OMX_DBGT_EPILOG("Unsupported index");
        return OMX_ErrorUnsupportedIndex;
    }; break;
    }
}

OMX_ERRORTYPE
VidDecOutputPort::useBuffer(OMX_BUFFERHEADERTYPE** ppBufferHdr,
                            OMX_PTR pAppPrivate,
                            OMX_U32 nSizeBytes,
                            OMX_U8* pBuffer)
{
    OMX_ERRORTYPE eError = OMX_ErrorNone;
    OMX_DBGT_PROLOG("Using buffer %p on video decoder ouput port", pBuffer);

    // Invalid case in proprietary tunneling
    assert(!this->isTunneled());

    // Allocate a buffer header
    *ppBufferHdr = 0;
    OMX_BUFFERHEADERTYPE* pBufferHdr = allocateBufferHeader(pAppPrivate,
                                                            nSizeBytes);
    if (!pBufferHdr) {
        eError = OMX_ErrorInsufficientResources;
        OMX_DBGT_ERROR("Can not allocate buffer header");
        OMX_DBGT_EPILOG("Return error %s", OMX_TYPE_TO_STR(OMX_ERRORTYPE, eError));
        return eError;
    }

    pBufferHdr->pBuffer = pBuffer;
    *ppBufferHdr = pBufferHdr;

    // Increment internal buffer counter.
    this->incrementNbBuffers();

#ifdef ANDROID
    if (this->nativeWindowsEnabled()) {
        // Get Virtual addr in pPlatformPrivate
        // pBuffer contains buffer_handle_t
        assert(viddec().grallocModule());
        if (viddec().grallocModule()->lock(viddec().grallocModule(),
                                           (buffer_handle_t)pBuffer,
                                           GRALLOC_USAGE_HW_2D | GRALLOC_USAGE_EXTERNAL_DISP | GRALLOC_USAGE_SW_WRITE_MASK,
                                           0, 0,
                                           this->definition().frameWidth(),
                                           this->definition().frameHeight(),
                                           &((*ppBufferHdr)->pPlatformPrivate)) != 0) {
            eError = OMX_ErrorInsufficientResources;
            OMX_DBGT_ERROR("::grallocModule->lock %s failed",
                       component().name());
        }
        if (viddec().grallocModule()->unlock(viddec().grallocModule(),
                                             (buffer_handle_t)pBuffer) != 0) {
            eError = OMX_ErrorInsufficientResources;
            OMX_DBGT_ERROR("::grallocModule->lock %s failed",
                       component().name());
        }

        viddec().m_mutexCmd.lock();
        while (viddec().IsConfigureDone == false) {
            OMX_DBGT_PINFO("Blocking useBuffer before vdec configure");
            viddec().m_condConfig.wait(viddec().m_mutexCmd);
            OMX_DBGT_PINFO("Unblocking useBuffer");
        }
        viddec().m_mutexCmd.release();
        if (viddec().dvb().UseBuffer((unsigned long)(*ppBufferHdr)->pPlatformPrivate,
                                     (unsigned long)nSizeBytes,
                                     (((this->definition().frameWidth() + 0xf) >> 4) << 4),
                                     this->definition().frameHeight(),
                                     (int)this->colorInfo().omxseType,
                                     (unsigned long)*ppBufferHdr) != 0) {
            eError = OMX_ErrorInsufficientResources;
            OMX_DBGT_ERROR("::dvb.UseBuffer %s failed",
                       component().name());
        }
    }
#endif
    OMX_DBGT_EPILOG();
    return eError;
}

OMX_ERRORTYPE
VidDecOutputPort::allocateBuffer(OMX_BUFFERHEADERTYPE** ppBufferHdr,
                                 OMX_PTR pAppPrivate,
                                 OMX_U32 nSizeBytes)
{
    // Allows to allocate and to return a frame buffer to the player
    OMX_ERRORTYPE eError = OMX_ErrorNone;
#ifdef ANDROID
    buffer_handle_t pPlatformPrivate;
    int Stride;
#else
    void * pPlatformPrivate = NULL;
#endif
    void* pBuffer;

    OMX_DBGT_PROLOG("Allocating buffer on video decoder ouput port");

    // Invalid case in proprietary tunneling
    assert(!this->isTunneled());

#ifdef ANDROID
    assert(viddec().grallocDevice());
    assert(viddec().grallocModule());
#endif
    // Allocate a buffer header
    *ppBufferHdr = 0;
    OMX_BUFFERHEADERTYPE* pBufferHdr = allocateBufferHeader(pAppPrivate,
                                                            nSizeBytes);
    if (!pBufferHdr) {
        eError = OMX_ErrorInsufficientResources;
        OMX_DBGT_ERROR("Can not allocate buffer header");
        OMX_DBGT_EPILOG("Return error %s", OMX_TYPE_TO_STR(OMX_ERRORTYPE, eError));
        return eError;
    }

#ifdef ANDROID
    if (viddec().grallocDevice()->alloc(viddec().grallocDevice(),
                                        this->definition().frameWidth(),
                                        this->definition().frameHeight(),
                                        this->colorInfo().androidHALType,
                                        GRALLOC_USAGE_HW_2D | GRALLOC_USAGE_EXTERNAL_DISP | GRALLOC_USAGE_SW_READ_RARELY,
                                        &pPlatformPrivate, &Stride) != 0)
    {
        OMX_DBGT_ERROR("Call grallocDevice->alloc %s failed (%ux%u fmt=0x%x)",
                   component().name(),
                   this->definition().frameWidth(),
                   this->definition().frameHeight(),
                   this->colorInfo().androidHALType);
        freeBufferHeader(pBufferHdr);
        OMX_DBGT_EPILOG();
        return OMX_ErrorInsufficientResources;
    }

    // Get Virtual addr in pBuffer
    // pPlatformPrivate contains buffer_handle_t
    viddec().grallocModule()->lock(viddec().grallocModule(),
                                   pPlatformPrivate,
                                   GRALLOC_USAGE_HW_2D | GRALLOC_USAGE_EXTERNAL_DISP | GRALLOC_USAGE_SW_READ_RARELY,
                                   0, 0,
                                   this->definition().frameWidth(),
                                   this->definition().frameHeight(),
                                   (void**)&pBuffer);
    viddec().grallocModule()->unlock(viddec().grallocModule(),
                                     pPlatformPrivate);
#else
    pBuffer = (void *)malloc(sizeof(char) * nSizeBytes);
#endif
    pBufferHdr->pBuffer = (OMX_U8*)pBuffer;
    *ppBufferHdr = pBufferHdr;

    viddec().m_mutexCmd.lock();
    while (viddec().IsConfigureDone == false) {
        OMX_DBGT_PINFO("Blocking allocateBuffer before vdec configure");
        viddec().m_condConfig.wait(viddec().m_mutexCmd);
        OMX_DBGT_PINFO("Unblocking allocateBuffer");
    }
    viddec().m_mutexCmd.release();

    if (viddec().dvb().UseBuffer((unsigned long)pBuffer,
                                 (unsigned long)nSizeBytes,
                                 this->definition().frameWidth(),
                                 this->definition().frameHeight(),
                                 (int)this->colorInfo().omxseType,
                                 (unsigned long)*ppBufferHdr) != 0) {
        OMX_DBGT_ERROR("::dvb.UseBuffer %s failed (%s)",
                   component().name(), strerror(errno));
    }

    (*ppBufferHdr)->pPlatformPrivate = (void*)pPlatformPrivate;

    // Increment internal buffer counter.
    this->incrementNbBuffers();

    OMX_DBGT_EPILOG();
    return eError;
}

OMX_ERRORTYPE
VidDecOutputPort::freeBuffer(OMX_BUFFERHEADERTYPE* pBufferHdr)
{
    assert(!isTunneled());

    if (pBufferHdr->pPlatformPrivate != NULL &&
        !this->nativeWindowsEnabled()) {
#ifdef ANDROID
        assert(viddec().grallocDevice());
        // Allocated through AllocateBuffer, free specific part
        viddec().grallocDevice()->free(viddec().grallocDevice(),
                                       (buffer_handle_t)pBufferHdr->pPlatformPrivate);
#else
        free(pBufferHdr->pBuffer);
#endif
        pBufferHdr->pBuffer = 0;
    }

    freeBufferHeader(pBufferHdr);
    this->decrementNbBuffers();
    return OMX_ErrorNone;
}

OMX_ERRORTYPE
VidDecOutputPort::emptyThisBuffer(OMX_BUFFERHEADERTYPE* pBufferHdr)
{
    assert(0 && "Invalid call on output port");
    return OMX_ErrorNone;
}

OMX_ERRORTYPE
VidDecOutputPort::fillThisBuffer(OMX_BUFFERHEADERTYPE* pBufferHdr)
{
    assert(this->isTunneled() == false);

     // Called by player to send an empty buffer to be filled with a
     // decoded video frame
     // This buffer is send to omxse
     OMX_DBGT_PROLOG();
     if (isAfterEosFlag())
     {
         // The last decoded video frame has already been returned to the player.
         // The buffer is send back empty to the player
         //pBufferHdr->nFlags |= OMX_BUFFERFLAG_EOS;
	 if (!m_isEOSnFlagsSet)
	 {
             pBufferHdr->nFlags |= OMX_BUFFERFLAG_EOS;
	     m_isEOSnFlagsSet = true;
	 }
         pBufferHdr->nTimeStamp = 0;
         pBufferHdr->nFilledLen = 0;
         this->viddec().notifyFillBufferDone(pBufferHdr);
     }
     else
     {
         if (viddec().dvb().FillThisBuffer((unsigned long)pBufferHdr) != 0)
         {
             OMX_DBGT_ERROR("dvb.FillThisBuffer %s error", component().name());
             OMX_DBGT_EPILOG();
             return OMX_ErrorHardware;
         }
     }
     OMX_DBGT_EPILOG();
    return OMX_ErrorNone;
}

OMX_ERRORTYPE VidDecOutputPort::flush()
{
    OMX_DBGT_PROLOG("Flush video decoder output port");
    if (isTunneled()) {
        // Proprietary tunneling. Nothing to flush
        OMX_DBGT_EPILOG("Port is tunneled.");
        return OMX_ErrorNone;
    }

    OMX_BUFFERHEADERTYPE *buf = 0;
    // synchronize the dvb().FlushBuffer() as ::flush() can be called
    // from the puller as well as the OmxComponent
    m_mutexSync.lock();
    while((buf = (OMX_BUFFERHEADERTYPE *)this->viddec().dvb().FlushBuffer()) != 0)
    {
        OMX_DBGT_PDEBUG(" %s Flush buffer(0x%08x)",
                    component().name(), (int)buf->pBuffer);
        this->viddec().notifyFillBufferDone(buf);
    }
    m_mutexSync.release();
    OMX_DBGT_EPILOG();
    return OMX_ErrorNone;
}

//=============================================================================
//  Output port core function of Pulling thread
//  Infinite loop: Wait for decoded frame from SE and send them to Player
//=============================================================================
void VidDecOutputPort::threadFunction()
{
     OMX_DBGT_PROLOG();
     OMX_DBGT_PINFO("%s Puller thread running on output port",
                component().name());

     while (true) {
         OMX_BUFFERHEADERTYPE *pOutBufHdr;
         uint64_t TS;
         bool eos;

         //  Wait for decoded frame from SE
         if (this->viddec().dvb().FillBufferDone((unsigned long&)pOutBufHdr,
                                                 TS, eos) != 0)
         {
             OMX_DBGT_ERROR("::dvb.FillBufferDone %s failed (%s)",
                        component().name(), strerror(errno));
             goto EXIT;
         }

         if (pOutBufHdr == NULL) {
             // End Of Stream maker received
             OMX_DBGT_PDEBUG("%s PullingRunner EventHandler OMX_BUFFERFLAG_EOS",
                         component().name());
             if (!(this->viddec().inputPort().isFlushIssued()))
             {
                 this->viddec().notifyEventHandler(OMX_EventBufferFlag,
                                                   OMX_ST_VIDEO_OUTPUT_PORT,
                                                   OMX_BUFFERFLAG_EOS, NULL);
                 m_bAfterEosFlag = true;
                 OMXdebug_flagAfterEos    = 1;

                 // loop to send back to player all buffer received after EOS
                 while( (pOutBufHdr = (OMX_BUFFERHEADERTYPE*)this->viddec().dvb().FlushBuffer()) != 0)
                 {
                     pOutBufHdr->nFlags |= OMX_BUFFERFLAG_EOS;
		     m_isEOSnFlagsSet = true;
                     pOutBufHdr->nTimeStamp = 0;
                     pOutBufHdr->nFilledLen = 0;
                     this->viddec().notifyFillBufferDone(pOutBufHdr);
                 }
             }
         } else {
             // decoded buffer to send to player

             // Do we have the potential correction
             struct timeMapping_s *node;
             struct timeMapping_s *overflowNode;
             this->viddec().m_mutexCtrl.lock();
             node = this->viddec().timeMapping_p;
             overflowNode = this->viddec().overflowTimeMapping_p;
             while(node)
             {
                 uint64_t timeDiff = (node->nTS > TS)?(node->nTS - TS): (TS - node->nTS);
                 if(timeDiff <= 100)
                 {
                     OMX_DBGT_PDEBUG("Potential timemapping: stored: %llu, received: %llu", node->nTS, TS);
                     TS = node->nTS;
                     this->viddec().timeMapping_p = deleteNode(this->viddec().timeMapping_p, node->nTS);
                     break;
                 }
                 node = node->next_p;
             }

             while(overflowNode)
             {
                 if(TS ==  overflowNode->nNewTS)
                 {
                     OMX_DBGT_PDEBUG("timemapping for overflow: stored: %llu, received: %llu", overflowNode->nTS, TS);
                     TS = overflowNode->nTS;
                     this->viddec().overflowTimeMapping_p = deleteNode(this->viddec().overflowTimeMapping_p, overflowNode->nTS);
                     break;
                 }
                 overflowNode = overflowNode->next_p;
             }

             this->viddec().m_mutexCtrl.release();

             pOutBufHdr->nTimeStamp = TS;
             pOutBufHdr->nFilledLen = pOutBufHdr->nAllocLen;
             pOutBufHdr->nOffset = 0;

             m_nReceivedFrame++;
             OMX_DBGT_PDEBUG("::FillBufferDone %s 0x%08x (len=%lu, pts=%llu, flags=%lu sendidFrame=%d receivedFrame=%d\n",
                         component().name(),
                         (unsigned int)pOutBufHdr->pBuffer,
                         (unsigned long)pOutBufHdr->nFilledLen,
                         pOutBufHdr->nTimeStamp,
                         (unsigned long)pOutBufHdr->nFlags,
                         this->viddec().inputPort().sendedFrame(),
                         this->receivedFrame());
             this->viddec().notifyFillBufferDone(pOutBufHdr);
         }
     }

     // end of playback: pulling thread destroy
 EXIT:
     OMX_DBGT_PINFO("%s Puller thread exit (VDECComponent::PullingRunner)",
                component().name());
     this->flush();
     OMX_DBGT_EPILOG();
}

//=============================================================================
//  Output port core function of Pulling thread
//  Infinite loop: Wait for decoded frame from SE and send them to Player
//=============================================================================
void VidDecOutputPort::udThreadFunction()
{
    int nBytesSize = USER_DATA_SIZE;
    char * data =  (char *)malloc(nBytesSize);
    uint64_t TS;
    uint32_t ret;
    OMX_CCUserData captureddata;
    OMX_CCUserdataGeneric *ccUsrDataGeneric_p;
    OMX_DBGT_PROLOG();
    OMX_DBGT_PINFO("%s Puller thread running on output port for userdata",
               component().name());
    while(true)
    {
        char *p;
        nBytesSize = USER_DATA_SIZE;

        ret = this->viddec().dvb().QueueBuffer(data, nBytesSize, TS);
        if(ret != 0)
        {
            goto EXIT;
        }
        for(p = data; p < (data + nBytesSize); p+= ((OMX_CCUserdataGeneric *)p)->block_length)
        {
            OMX_EVENT_CLOSEDCAPTIONDATA ccEvtData;
            char * usrDataBuff_p = NULL;

            memset(&ccEvtData, 0, sizeof(ccEvtData));
            ccUsrDataGeneric_p = (OMX_CCUserdataGeneric *)p;
            usrDataBuff_p = (char *)p + sizeof(OMX_CCUserdataGeneric); // fixed header
            memset(&captureddata, 0, sizeof(captureddata));
            if(ccUsrDataGeneric_p->is_there_a_pts || ccUsrDataGeneric_p->is_pts_interpolated)
            {
                uint64_t PTS = ((uint64_t)ccUsrDataGeneric_p->pts_msb << 32) | ccUsrDataGeneric_p->pts;
                ccEvtData.nTimeStamp = (PTS *1000)/90;
                captureddata.pts = ccUsrDataGeneric_p->pts;
                OMX_DBGT_PINFO("pts: %llu, %llu\n", PTS, ccEvtData.nTimeStamp);
            }

            if(ccUsrDataGeneric_p->codec_id == OMX_CC_CODEC_TYPE_H264)
            {
                OMX_CCUserDataH264 * ccDataH264 = (OMX_CCUserDataH264 *) usrDataBuff_p;
                captureddata.is_registered = ccDataH264->is_registered;
                captureddata.itu_t_t35_country_code =
                    ccDataH264->itu_t_t35_country_code;
                captureddata.itu_t_t35_country_code_extension_byte =
                    ccDataH264->itu_t_t35_country_code_extension_byte;
                captureddata.itu_t_t35_provider_code =
                    ccDataH264->itu_t_t35_provider_code;
                memcpy (captureddata.uuid_iso_iec_11578,
                    ccDataH264->uuid_iso_iec_11578, 16);

                usrDataBuff_p +=sizeof(OMX_CCUserDataH264);
            }
            if(ccUsrDataGeneric_p->codec_id == OMX_CC_CODEC_TYPE_MPEG2)
            {
                usrDataBuff_p+=sizeof(OMX_CCUserDataMpeg2);
            }

            usrDataBuff_p += ccUsrDataGeneric_p->padding_bytes;
            {
                uint32_t datasize = (ccUsrDataGeneric_p->block_length -
                     (ccUsrDataGeneric_p->header_length +
                      ccUsrDataGeneric_p->padding_bytes));
                OMX_DBGT_PINFO("codec_id: %d, datasize: %d\n",ccUsrDataGeneric_p->codec_id, datasize);
                memcpy(captureddata.data, usrDataBuff_p, datasize);
            }

            OMXCCFormatMode detectMode = omx_cc_detect_SCTE128(&captureddata);

            if(detectMode == OMX_CC_FORMAT_SCTE128)
            {
                OMXCCParserSCT128(&captureddata, &ccEvtData);
            }else
            {
                detectMode = omx_cc_detect_SCTE21(&captureddata);

                if(detectMode == OMX_CC_FORMAT_SCTE21)
                {
                    OMXCCParserSCT21(&captureddata, &ccEvtData);
                }
            }

            this->viddec().notifyEventHandler(OMX_EventVideoUserDataAvailable,
                OMX_ST_VIDEO_OUTPUT_PORT,
                0, &ccEvtData);
        }
    }

    // end of playback: pulling thread destroy
EXIT:
    free(data);
    OMX_DBGT_PINFO("%s userdata Puller thread exit (VDECComponent::userdata)",
               component().name());
    OMX_DBGT_EPILOG();
}

//==========================================================================
// VidDecInputPort implementation
//==========================================================================

const OMX_VIDEO_CODINGTYPE  VidDecInputPort::m_codingFormats[] = {
    OMX_VIDEO_CodingAVC,
    OMX_VIDEO_CodingMPEG4,
    OMX_VIDEO_CodingMPEG2,
    OMX_VIDEO_CodingH263,
    OMX_VIDEO_CodingVC1,
    OMX_VIDEO_CodingWMV,
    (OMX_VIDEO_CODINGTYPE)OMX_VIDEO_CodingFLV1,
    OMX_VIDEO_CodingMJPEG,
    OMX_VIDEO_CodingVP8,
    OMX_VIDEO_CodingVP9,
    (OMX_VIDEO_CODINGTYPE)OMX_VIDEO_CodingVPX,
    (OMX_VIDEO_CODINGTYPE)OMX_VIDEO_CodingTHEORA,
    (OMX_VIDEO_CODINGTYPE)OMX_VIDEO_CodingSorenson,
    (OMX_VIDEO_CODINGTYPE)OMX_VIDEO_CodingCAVS,
    OMX_VIDEO_CodingHEVC,
};

const VidDecInputPort::ProfileAndLevelSupportedAVC_s VidDecInputPort::m_profilelevelAVC[] = {
    {OMX_VIDEO_AVCProfileBaseline,OMX_VIDEO_AVCLevel42},
    {OMX_VIDEO_AVCProfileMain,OMX_VIDEO_AVCLevel42},
    {OMX_VIDEO_AVCProfileHigh,OMX_VIDEO_AVCLevel42},
};

const VidDecInputPort::ProfileAndLevelSupportedMPEG4_s VidDecInputPort::m_profilelevelMPEG4[] = {
    {OMX_VIDEO_MPEG4ProfileSimple,OMX_VIDEO_MPEG4Level5},
    {OMX_VIDEO_MPEG4ProfileAdvancedSimple,OMX_VIDEO_MPEG4Level5},
};

OMX_BUFFERHEADERTYPE* VidDecInputPort::EOSBUF =
    (OMX_BUFFERHEADERTYPE*)0xFFFFFFFF;

VidDecInputPort::VidDecInputPort(unsigned int nIdx, OMX_DIRTYPE aDir,
                                 const VDECComponent& owner)
    :VidDecPort(nIdx, aDir, owner),
     pusher_thread_id(0),
     m_nSendedFrame(0),
     tmp_buff(0),
     tmp_length(0),
     params_done(false),
     initial_header(false),
     isFlushCommandIssued(false)
     , m_b10BitEnabled(false)
{
    this->definition().bufferCountMin(NUM_IN_BUFFERS);
    this->definition().bufferCountActual(NUM_IN_BUFFERS);
    OmxPort::create();
    tmp_buff = (char *)malloc(CONFIG_BUF_SIZE);
    if (tmp_buff == NULL) {
        OMX_DBGT_ERROR("Allocation of tmp_buff failed");
    }
}

VidDecInputPort::~VidDecInputPort()
{
    if (tmp_buff != NULL)
        free(tmp_buff);
    tmp_buff = NULL;
}

OMX_ERRORTYPE VidDecInputPort::create()
{
    return OMX_ErrorNone;
}

int VidDecInputPort::start()
{
    OMX_DBGT_PROLOG();

    OMX_DBGT_CHECK_RETURN(! pthread_create(&pusher_thread_id, NULL, VidDecInputPort::ThreadEntryPoint, this), OMX_ErrorInsufficientResources);
#ifdef ANDROID
    OMX_DBGT_CHECK_RETURN(! pthread_setname_np(pusher_thread_id, "OMXVideoPush"), OMX_ErrorInsufficientResources);
#endif
    this->threadState(EThreadState_Running);
    OMX_DBGT_EPILOG();
    return OMX_ErrorNone;
}

void VidDecInputPort::destroy()
{
    OMX_DBGT_PROLOG();

    if (pusher_thread_id != 0) {
        // Resume if was previously paused
        this->resume();
        this->push(EOSBUF);
        pthread_join(pusher_thread_id, 0);
    }

    if (this->nbBuffers() > 0) {
        OMX_DBGT_ERROR("Allocated buffer on input port not freed");
    }

    initial_header       = false;
    params_done          = false;
    m_nSendedFrame       = 0;
    isFlushCommandIssued = false;

    // TODO: we may need to reset 'tmp_length' in case of encoding type
    //       and dvb has not been destroyed.

    OMX_DBGT_EPILOG();
}

void* VidDecInputPort::ThreadEntryPoint(void* a_pThis)
{
    VidDecInputPort* pThis = static_cast<VidDecInputPort*>(a_pThis);
    pThis->threadFunction();
    return 0;
}

OMX_VIDEO_CODINGTYPE
VidDecInputPort::supportedCodingFormat(unsigned int nIdx) const
{
    assert(nIdx < this->supportedCodingFormatNb());
    return m_codingFormats[nIdx];
}

unsigned int
VidDecInputPort::supportedCodingFormatNb() const
{ return (sizeof(m_codingFormats) / sizeof(m_codingFormats[0])); }

bool VidDecInputPort::isSupportedCodingFormat(OMX_VIDEO_CODINGTYPE aFmt) const
{
    bool ret = false;
    for (unsigned int i = 0; i < this->supportedCodingFormatNb(); i++) {
        if (this->supportedCodingFormat(i) == aFmt) {
            ret = true;
            break;
        }
    }
    return ret;
}

void VidDecInputPort::codingFormat(OMX_VIDEO_CODINGTYPE aFmt)
{
    assert(isSupportedCodingFormat(aFmt));
    this->definition().compressionFormat(aFmt);
}

OMX_ERRORTYPE
VidDecInputPort::getParameter(OMX_INDEXTYPE nParamIndex, OMX_PTR pParamStruct)
{
    OMX_DBGT_PROLOG("on Video decoder input port");
    switch ((int)nParamIndex)
    {

    case OMX_IndexParamVideoAvc: {
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }; break;

    case OMX_IndexParamPortDefinition: {
        // Get port definition
        ASSERT_STRUCT_TYPE(pParamStruct, OMX_PARAM_PORTDEFINITIONTYPE);
        OMX_PARAM_PORTDEFINITIONTYPE* param;
        param = (OMX_PARAM_PORTDEFINITIONTYPE*)pParamStruct;
        assert(param->nPortIndex == this->definition().index());

        // Provide current content of port and computes nBufferSize on top of it
        OMX_DBGT_PDEBUG("GetParameter(OMX_IndexParamPortDefinition) nPortIndex: %u"
                    " eDir: %d nBufferCountActual: %u nBufferCountMin: %u"
                    " nBufferSize: %u\n",
                    this->definition().index(),
                    this->definition().direction(),
                    this->definition().bufferCountActual(),
                    this->definition().bufferCountMin(),
                    this->definition().bufferSize());
        if ( this->definition().bufferSize() == 0 ) {
            // default input buffer size not computed yet:
            // We have 2 cases: Either the size will be provided by the media
            // extractor either or it will never be provided
            // The computation consists in giving a large enough default
            // size in case no size will be provided at all
            // The principle is to give a default size that is half of
            // the maximum output size (using yuv 420) considering
            //  that encoder will compress at least twice the IFrame:
            // This is the case for MPEG4 and be confirmed for others
            this->definition().bufferSize( (4096*2048*3/2)/2 );
            OMX_DBGT_PINFO("%s set nBufferSize to %uKB "
                       "(GetParameter(OMX_IndexParamPortDefinition input))",
                       component().name(), param->nBufferSize/1024);
        }

        *param = this->definition().getParamPortDef();
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }

    case OMX_IndexParamVideoPortFormat: {
        // Supported video port format
        ASSERT_STRUCT_TYPE(pParamStruct, OMX_VIDEO_PARAM_PORTFORMATTYPE);
        OMX_VIDEO_PARAM_PORTFORMATTYPE* param;
        param = (OMX_VIDEO_PARAM_PORTFORMATTYPE*)pParamStruct;
        assert(param->nPortIndex == this->definition().index());
        OMX_DBGT_PDEBUG("::OMX_IndexParamVideoPortFormat:");
        if (param->nIndex < this->supportedCodingFormatNb()) {
            param->eCompressionFormat = this->supportedCodingFormat(param->nIndex);
            param->eColorFormat = OMX_COLOR_FormatUnused;
            OMX_DBGT_EPILOG();
            return OMX_ErrorNone;
        } else {
            OMX_DBGT_EPILOG("No more supported format");
            return OMX_ErrorNoMore;
        }
    }

    case OMX_getAndroidNativeBufferUsage: {
        ASSERT_STRUCT_TYPE(pParamStruct, OMX_GetAndroidNativeBufferUsageParams);
        OMX_GetAndroidNativeBufferUsageParams* param;
        param = (OMX_GetAndroidNativeBufferUsageParams*)pParamStruct;
        OMX_DBGT_PDEBUG("VDECComponent::input port get parameter OMX_getAndroidNativeBufferUsage");
        OMX_DBGT_ERROR("::OMX_getAndroidNativeBufferUsage %s not set on output (index=%u)",
                   component().name(), param->nPortIndex);
        OMX_DBGT_EPILOG();
        return OMX_ErrorBadPortIndex;
    }; break;

    case OMX_IndexParam10BitProfile: {
        ASSERT_STRUCT_TYPE(pParamStruct, OMX_VIDEO_PARAM_10BITPROFILETYPE);
        OMX_VIDEO_PARAM_10BITPROFILETYPE* param;
        param = (OMX_VIDEO_PARAM_10BITPROFILETYPE*)pParamStruct;
        assert(param->nPortIndex == this->definition().index());

        param->bEnable = (OMX_BOOL)m_b10BitEnabled;

        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }; break;


    case OMX_IndexParamVideoProfileLevelQuerySupported: {
       ASSERT_STRUCT_TYPE(pParamStruct, OMX_VIDEO_PARAM_PROFILELEVELTYPE);
       OMX_VIDEO_PARAM_PROFILELEVELTYPE* param = (OMX_VIDEO_PARAM_PROFILELEVELTYPE *)pParamStruct;
       unsigned int nbProfileLevelSupportedAVC = sizeof(m_profilelevelAVC) / sizeof(m_profilelevelAVC[0]);
       unsigned int nbProfileLevelSupportedMPEG4 = sizeof(m_profilelevelMPEG4) / sizeof(m_profilelevelMPEG4[0]);

       if (param->nProfileIndex > (nbProfileLevelSupportedAVC + nbProfileLevelSupportedMPEG4 -1)) {
           OMX_DBGT_PDEBUG(" All supported profile/level have been supplied, param->nProfileIndex=%d \
                             nbProfileLevelSupportedAVC=%d, nbProfileLevelSupportedMPEG4=%d",
                             param->nProfileIndex , nbProfileLevelSupportedAVC, nbProfileLevelSupportedMPEG4);
           OMX_DBGT_EPILOG();
           return OMX_ErrorNoMore;
       }
       int index = 0;
       if(param->nProfileIndex < nbProfileLevelSupportedAVC) {
           index = param->nProfileIndex;
           param->eProfile = m_profilelevelAVC[index].AVCprofile;
           param->eLevel = m_profilelevelAVC[index].AVClevel;
       }
       else {
           index = param->nProfileIndex - nbProfileLevelSupportedAVC;
           param->eProfile = m_profilelevelMPEG4[index].MPEG4profile;
           param->eLevel = m_profilelevelMPEG4[index].MPEG4level;
       }
       OMX_DBGT_EPILOG();
       return OMX_ErrorNone;
    }; break;

    case OMX_IndexParamOtherPortFormat:
    case OMX_IndexParamAudioPortFormat:
    case OMX_IndexParamImagePortFormat:
    default: {
        OMX_DBGT_ERROR("Unsupported index(%#x)",nParamIndex);
        OMX_DBGT_EPILOG("Unsupported index");
        return OMX_ErrorUnsupportedIndex;
    }; break;
    }
}

OMX_ERRORTYPE
VidDecInputPort::setParameter(OMX_INDEXTYPE nParamIndex, OMX_PTR pParamStruct)
{
    OMX_DBGT_PROLOG("on Video decoder input port");
    switch ((int)nParamIndex)
    {

    case OMX_IndexParamVideoAvc: {
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }; break;

    case OMX_enableAndroidNativeBuffers: {
        // Nothing to do on input port
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }; break;

    case OMX_IndexParamVideoPortFormat: {
        // Set video port format
        ASSERT_STRUCT_TYPE(pParamStruct, OMX_VIDEO_PARAM_PORTFORMATTYPE);
        OMX_VIDEO_PARAM_PORTFORMATTYPE* param;
        param = (OMX_VIDEO_PARAM_PORTFORMATTYPE*)pParamStruct;
        assert(param->nPortIndex == this->definition().index());

        if (!isSupportedCodingFormat(param->eCompressionFormat)) {
            // Unsupported compression format
            OMX_DBGT_ERROR("%s eCompressionFormat not supported (%d)",
                       component().name(), param->eCompressionFormat);
            OMX_DBGT_EPILOG("Unsupported video compression format");
            return OMX_ErrorUnsupportedSetting;
        }

        // Workaround to limit FLV size to VGA format (640x480)
        // TODO (waiting for a SE fix)
        if ((param->eCompressionFormat == (OMX_VIDEO_CODINGTYPE)OMX_VIDEO_CodingFLV1)
            || (param->eCompressionFormat == (OMX_VIDEO_CODINGTYPE)OMX_VIDEO_CodingSorenson)) {
            if ((this->definition().frameWidth() != 0) &&
                (this->definition().frameHeight() != 0) &&
                ( (this->definition().frameWidth() > VGA_WIDTH) ||
                  (this->definition().frameHeight() > VGA_HEIGHT))) {
                OMX_DBGT_ERROR(" %s FLV1 : This Resolution is not fully supported %d %d \n",
                           component().name(),
                           this->definition().frameWidth(),
                           this->definition().frameHeight());
            }
        }

        OMX_DBGT_PDEBUG("OMX_IndexParamVideoPortFormat:"
                    "compression format %u->%u ,"
                    "color format %s (eColorFormat=%d(0x%x)) -> (eColorFormat=%d(0x%x)) ,"
                    "frame rate %u -> %u",
                    this->definition().compressionFormat(), param->eCompressionFormat,
                    this->definition().contentType(),
                    this->definition().colorFormat(),
                    this->definition().colorFormat(),
                    param->eColorFormat, param->eColorFormat,
                    this->definition().frameRate(), param->xFramerate);

        // Apply format on port
        this->codingFormat(param->eCompressionFormat);
        this->definition().frameRate(param->xFramerate);
        OMX_DBGT_PINFO("%s compression format changed to %s (eCompressionFormat=%d(0x%x))",
                   component().name(),
                   this->definition().contentType(),
                   this->definition().compressionFormat(),
                   this->definition().compressionFormat());

        OMX_DBGT_EPILOG("Set port compression format %u", param->eColorFormat);
        return OMX_ErrorNone;
    }

    case OMX_IndexParamPortDefinition: {
        // Set port definition
        ASSERT_STRUCT_TYPE(pParamStruct, OMX_PARAM_PORTDEFINITIONTYPE);
        OMX_PARAM_PORTDEFINITIONTYPE* param;
        param = (OMX_PARAM_PORTDEFINITIONTYPE*)pParamStruct;
        assert(param->nPortIndex == this->definition().index());

        // Workaround to limit FLV size to VGA format (640x480) (waiting for a SE fix)
        if( ((this->definition().compressionFormat() ==  (OMX_VIDEO_CODINGTYPE)OMX_VIDEO_CodingFLV1)
          || (this->definition().compressionFormat() == (OMX_VIDEO_CODINGTYPE)OMX_VIDEO_CodingSorenson))
            &&
            (     (param->format.video.nFrameWidth  > VGA_WIDTH)
                  || (param->format.video.nFrameHeight > VGA_HEIGHT)
                  )
            )
        {
            OMX_DBGT_ERROR(" %s FLV1 : This resolution is not fully supported %u %u \n",
                       component().name(),
                       (unsigned int)param->format.video.nFrameWidth,
                       (unsigned int)param->format.video.nFrameHeight);
        }

        if (this->definition().bufferSize() != param->nBufferSize) {
            OMX_DBGT_PINFO("%s changing nBufferSize from %uKB to %uKB "
                       "(SetParameter(OMX_IndexParamPortDefinition input))",
                       component().name(),
                       this->definition().bufferSize()/1024,
                       param->nBufferSize/1024);
            this->definition().bufferSize(param->nBufferSize);
        }
        this->definition().setParamPortDef(param);
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }; break;

    case OMX_IndexParam10BitProfile: {
        // Configure 10 Bit
        ASSERT_STRUCT_TYPE(pParamStruct, OMX_VIDEO_PARAM_10BITPROFILETYPE);
        OMX_VIDEO_PARAM_10BITPROFILETYPE* param;
        param = (OMX_VIDEO_PARAM_10BITPROFILETYPE*)pParamStruct;
        assert(param->nPortIndex == this->definition().index());

        m_b10BitEnabled = param->bEnable;

        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }; break;

    case OMX_IndexParamOtherPortFormat:
    case OMX_IndexParamAudioPortFormat:
    case OMX_IndexParamImagePortFormat:
    default: {
        OMX_DBGT_ERROR("Unsupported index(%#x)",nParamIndex);
        OMX_DBGT_EPILOG("Unsupported index");
        return OMX_ErrorUnsupportedIndex;
    }
    }
}

OMX_ERRORTYPE
VidDecInputPort::useBuffer(OMX_BUFFERHEADERTYPE** ppBufferHdr,
                           OMX_PTR pAppPrivate,
                           OMX_U32 nSizeBytes,
                           OMX_U8* pBuffer)
{
    return OmxPort::useBuffer(ppBufferHdr, pAppPrivate,
                              nSizeBytes, pBuffer);
}

OMX_ERRORTYPE
VidDecInputPort::allocateBuffer(OMX_BUFFERHEADERTYPE** ppBufferHdr,
                                OMX_PTR pAppPrivate,
                                OMX_U32 nSizeBytes)
{
    return OmxPort::allocateBuffer(ppBufferHdr, pAppPrivate, nSizeBytes);
}

OMX_ERRORTYPE
VidDecInputPort::freeBuffer(OMX_BUFFERHEADERTYPE* pBufferHdr)
{
    return OmxPort::freeBuffer(pBufferHdr);
}

OMX_ERRORTYPE
VidDecInputPort::emptyThisBuffer(OMX_BUFFERHEADERTYPE* pBufferHdr)
{
    // Called by player to send a video encoded frame buffer
    // This buffer is pushed via a queue to the PusherThread
    // in order to be sent to SE
    OMX_DBGT_PROLOG();
    isFlushCommandIssued = false;
    if ((pBufferHdr->nFilledLen == 0) &&  ((pBufferHdr->nFlags & OMX_BUFFERFLAG_EOS) == 0)) {
        this->viddec().notifyEmptyBufferDone(pBufferHdr);
    }
    else
    {
        this->push(pBufferHdr);
    }

    OMX_DBGT_EPILOG();
    return OMX_ErrorNone;
}

OMX_ERRORTYPE
VidDecInputPort::fillThisBuffer(OMX_BUFFERHEADERTYPE* pBufferHdr)
{
    assert(0 && "Invalid call on input port");
    return OMX_ErrorNone;
}

OMX_ERRORTYPE VidDecInputPort::flush()
{
    OMX_DBGT_PROLOG("Flush video decoder input port");
    if (isTunneled()) {
        // Proprietary tunneling. Nothing to flush
        OMX_DBGT_EPILOG("Port is tunneled.");
        return OMX_ErrorNone;
    }

   isFlushCommandIssued = true;
   this->getRawFifos()->lock();

    OMX_BUFFERHEADERTYPE *buf  = 0;
    while(this->getRawFifos()->tryPop(buf) == true) {
        this->viddec().notifyEmptyBufferDone(buf);
    }
    this->getRawFifos()->unlock();
    buf  = 0;
    buf = this->relockCurrent();
    if(buf != NULL)
    {
        this->viddec().notifyEmptyBufferDone(buf);
    }
    this->freeAndUnlockCurrent();
    this->viddec().dvb().resetflags();

    OMX_DBGT_EPILOG();
    return OMX_ErrorNone;
}

OMX_ERRORTYPE VidDecInputPort::pause()
{
    OMX_DBGT_PROLOG("Pausing video input port");
    if (this->threadState() == EThreadState_Paused) {
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }

    // Pausing thread
    this->threadState(EThreadState_Paused);

    // Waiting thread is paused

    OMX_DBGT_EPILOG();
    return OMX_ErrorNone;
}

OMX_ERRORTYPE VidDecInputPort::resume()
{
    if (this->threadState() == EThreadState_Running) {
        return OMX_ErrorNone;
    }

    // Resuming thread
    this->threadState(EThreadState_Running);
    return OMX_ErrorNone;
}

int VidDecInputPort::decodeFrame(unsigned int nOmxFlags,
                                 uint8_t* pBuf, unsigned int nLen,
                                 uint64_t nTimeStamp)
{
    if (nOmxFlags & OMX_BUFFERFLAG_CODECCONFIG) {
        OMX_DBGT_PDEBUG("%s ::Codec configuration. Bytes Recvd in Buffer = %u",
                    component().name(), nLen);
        params_done = false;

        assert(tmp_buff != NULL);
        // Check for potential overflow
        assert((tmp_length + nLen) < CONFIG_BUF_SIZE);
        // Save data
        memcpy(tmp_buff + tmp_length, pBuf, nLen);
        tmp_length += nLen;
        return 0;
    }

    // to protect dvbvideo::write done in emitInitialHeader, emitCodecConfig and emitFrame from dvbaudio::pause
    this->viddec().dvb().lock();

    int dvbErr = 0;
    if (initial_header == false) {
        OMX_DBGT_PDEBUG("%s::Emit Initial PES header", component().name());
        dvbErr = this->viddec().pesAdapter()->emitInitialHeader();
        initial_header = true;
    }
    if (dvbErr) {
        OMX_DBGT_ERROR("Error (%d) when writing in video dvb", dvbErr);
        this->viddec().dvb().unlock();
        return dvbErr;
    }

    if (params_done == false) {
        OMX_DBGT_PDEBUG("%s::Codec Params done. Bytes Recvd = %d",
                    component().name(), tmp_length);

        // Send previously saved config frame
        if ((tmp_length > 0)
                || (this->definition().compressionFormat() == (OMX_VIDEO_CODINGTYPE) OMX_VIDEO_CodingVPX)
                || (this->definition().compressionFormat() == OMX_VIDEO_CodingVP8)
                || (this->definition().compressionFormat() == OMX_VIDEO_CodingVP9)) {
            OMX_DBGT_PDEBUG("%s::Emit PES Codec config", component().name());
            dvbErr = this->viddec().pesAdapter()->emitCodecConfig((uint8_t*)tmp_buff, tmp_length);
            tmp_length = 0;
        }
        params_done = true;
    }
    if (dvbErr) {
        OMX_DBGT_ERROR("Error (%d) when writing in video dvb", dvbErr);
        this->viddec().dvb().unlock();
        return dvbErr;
    }

    if(!this->viddec().outputPort().isTunneled() && (nTimeStamp != (uint64_t)OMX_INVALID_TIMESTAMP))
    {
        // We may require a mapping due to overflow of time beyond 2^34-1
        if(nTimeStamp >  MAX_TS_US)
        {
            uint64_t  nTime = nTimeStamp % MAX_TS_US;
            OMX_DBGT_PDEBUG("TS overflow : actual: %llu, truncated: %llu", nTimeStamp, nTime);
            this->viddec().m_mutexCtrl.lock();
            this->viddec().overflowTimeMapping_p = insertNode(this->viddec().overflowTimeMapping_p, nTimeStamp, nTime);
            this->viddec().m_mutexCtrl.release();
            nTimeStamp = nTime;
        }

        // We may require a mapping due to quantization loss
        {
            uint64_t  nTime = PTS_CORRECTION(nTimeStamp);
            nTime  = PTS_TO_US(nTime);
            if(nTime != nTimeStamp)
            {
                OMX_DBGT_PDEBUG("Potential TS mod : actual: %llu, returned: %llu", nTimeStamp, nTime);
                this->viddec().m_mutexCtrl.lock();
                this->viddec().timeMapping_p = insertNode(this->viddec().timeMapping_p, nTimeStamp, 0);
                this->viddec().m_mutexCtrl.release();
            }
        }
    }

    // Send video frame to decode
    OMX_DBGT_PDEBUG("%s::Emit PES Frame with %u bytes",
                component().name(), nLen);
    dvbErr = this->viddec().pesAdapter()->emitFrame(pBuf, nLen, nTimeStamp);
    this->viddec().dvb().unlock();
    if (dvbErr) {
        OMX_DBGT_ERROR("Error (%d) when writing in video dvb", dvbErr);
        return dvbErr;
    }

    m_nSendedFrame++;
    OMX_DBGT_PDEBUG (" %s::EmitFrame %p (len=%u, pts=%llu, flags=%x) "
                 "sendedFrame+bufferinPipe=(%d+%d) receiveFrame=%d",
                 component().name(),
                 pBuf,
                 nLen,
                 nTimeStamp,
                 nOmxFlags,
                 this->sendedFrame(),
                 this->getRawFifos()->size(),
                 this->viddec().outputPort().receivedFrame());
    return 0;
}


//=============================================================================
//  Input port core function of pusher thread
//  Infinite loop: Wait for encoded frame from player and send them SE to decode it
//=============================================================================
void VidDecInputPort::threadFunction()
{
    OMX_DBGT_PROLOG("%s Pusher thread running on input port",
            component().name());

    OMX_BUFFERHEADERTYPE *pInBufHdr = 0;
    OMX_U32 nFlags = 0;

    while(true) {
        m_mutexThread.lock();
        while (this->threadState() == EThreadState_Paused) {
            OMX_DBGT_PINFO("Pause video input port thread");
            m_condState.wait(m_mutexThread);
            OMX_DBGT_PINFO("Resuming video input port thread");
        }
        m_mutexThread.release();

        // Wait for a 'EmptyThisBuffer' from player
        pInBufHdr = this->popAndLockCurrent();
        assert(pInBufHdr != 0);

        if(pInBufHdr != NULL && pInBufHdr != this->EOSBUF)
        {
            nFlags = pInBufHdr->nFlags;
            OMX_DBGT_PDEBUG("%s video buffer header %p flag=(EOS:%d EOF:%d DecodeOnly:%d CodecCfg:%d) timestamp: %lld",
                    component().name(),
                    pInBufHdr,
                    ((nFlags & OMX_BUFFERFLAG_EOS) != 0),
                    ((nFlags & OMX_BUFFERFLAG_ENDOFFRAME) != 0),
                    ((nFlags & OMX_BUFFERFLAG_DECODEONLY) != 0),
                    ((nFlags & OMX_BUFFERFLAG_CODECCONFIG) != 0),
                    (int64_t)pInBufHdr->nTimeStamp);

            // Frame to decode
            if (this->decodeFrame(nFlags,
                        pInBufHdr->pBuffer + pInBufHdr->nOffset,
                        pInBufHdr->nFilledLen,
                        pInBufHdr->nTimeStamp))
            {
                OMX_DBGT_ERROR("Error when decoding video frame");
            }
            else
            {
                this->viddec().dvb().resetflags();
            }

            this->viddec().notifyEmptyBufferDone(pInBufHdr);

            if (nFlags & OMX_BUFFERFLAG_EOS)
            {
                // End Of Stream detected: last frame to decode
                OMX_DBGT_PDEBUG("%s OMX_BUFFERFLAG_EOS detected (VDECComponent::PusherRunner)",component().name());
                this->viddec().dvb().flush(true);

                // End Of Stream detected: last frame to decode
                if (this->viddec().outputPort().isTunneled())
                {
                    this->viddec().notifyEventHandler(OMX_EventBufferFlag,
                            this->viddec().outputPort().definition().index(),
                            OMX_BUFFERFLAG_EOS, 0);
#ifndef ANDROID
                    {
                        OMX_BUFFERHEADERTYPE BufferHdr;
                        OMX_CONF_INIT_STRUCT_PTR(&BufferHdr, OMX_BUFFERHEADERTYPE);
                        BufferHdr.nFlags = OMX_BUFFERFLAG_EOS;
                        if(this->viddec().hTunnelComp)
                        {
                            OMX_EmptyThisBuffer(this->viddec().hTunnelComp, &BufferHdr);
                        }
                    }
#endif
                }
            }
            this->freeAndUnlockCurrent();
        }
        else
        {
            this->freeAndUnlockCurrent();
            goto EXIT;
        }
    }
EXIT:
    OMX_DBGT_PINFO("Pusher thread exit (VDECComponent::PusherRunner)");
    flush();
    OMX_DBGT_EPILOG();
}


} // eof namespace stm
