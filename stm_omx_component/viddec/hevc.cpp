/*
 * Copyright 2011 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *
 * File   ::    hevc.cpp
 * Author ::   Puneet Gulati (puneet.gulati@st.com) 
 *
 *
 */

#include <stdio.h>

#include "VideoPesAdapter.hpp"
#include "VideoDecoder.hpp"
#include "OMX_debug.h"
#include "Mutex.hpp"

#include "pes.hpp"

#define DBGT_PREFIX "HEVC "
#define DBGT_LAYER  1
#include <linux/dbgt.h>

#define MAX_DATA_LENGTH 20

#define PTS_CORRECTION(x) (((x) * 90000)/1000000)

namespace stm {

const char kHEVC_SC[] = {0x00, 0x00, 0x01};
#define HEVC_HEADER_1 0x60
#define HEVC_HEADER_2 0x61
#define HEVC_HEADER_3 0x62

struct HEVCHeader {
    uint8_t* pHeader;
    int nSize;
};

#ifdef DUMP_FRAMES
int xx = 0;
#endif

static Mutex mLock;

HEVCPesAdapter::HEVCPesAdapter(const VDECComponent& dec)
    :VideoPesAdapter(dec),nNALLengthSize(0)
{
#ifdef DUMP_FRAMES
    xx = 0;
#endif
    mLock.lock();
}

HEVCPesAdapter::~HEVCPesAdapter()
{
    mLock.release();
}

int HEVCPesAdapter::emitInitialHeader() const
{
    return 0;
}

int HEVCPesAdapter::findHEVCHeader(int headertype, uint8_t* buffer, unsigned int& maxsize, uint8_t*& header) const
{
    OMX_DBGT_PROLOG("headertype: %#x, buffer: %p, maxsize: %u",headertype,buffer,maxsize);
    int size = 0;

    unsigned int pos = 0;
    pos +=3;
    header = buffer + pos;
    DBGT_PDEBUG("header: %p",header);

    for(unsigned int i = 0; i<2; i++) {
        DBGT_PDEBUG("header[%d]: %#x",i,header[i]);
        if(i > 0)
            size = size<<8;
        size += header[i];
    }

    header += 2;
    DBGT_PDEBUG("header: %p",header);

    maxsize = maxsize - pos - size;
    OMX_DBGT_EPILOG("maxsize: %d, size: %d",maxsize,size);
    return size;
}

int HEVCPesAdapter::emitCodecConfig(uint8_t* pBuffer, unsigned int nFilledLen) const
{
    int err = 0;
    int PesHeaderLength;

    OMX_DBGT_PROLOG();
    OMX_DBGT_PTRACE("pBuf=%p, len=%lu", pBuffer, (unsigned long)nFilledLen);

    if((pBuffer[0] == 0 && pBuffer[1] == 0 && pBuffer[2] == 1) || (pBuffer[1] == 0 && pBuffer[2] == 0 && pBuffer[3] == 1)) {
        OMX_DBGT_PTRACE("HEVC SC Found");
    }
    else
    {
        if(nNALLengthSize == 0) {
            nNALLengthSize = 1 + (pBuffer[22] & 3);
            OMX_DBGT_PTRACE("pBuffer[22]: %#x, nNALLengthSize: %d", pBuffer[22],nNALLengthSize);
        }

        HEVCHeader hevcHeaders[3];
        nFilledLen = nFilledLen-23;

        hevcHeaders[0].nSize = findHEVCHeader(HEVC_HEADER_1, pBuffer+23, nFilledLen, hevcHeaders[0].pHeader);
        if(hevcHeaders[0].nSize < 0) {
            OMX_DBGT_EPILOG ();
            return -1;
        }

        hevcHeaders[1].nSize = findHEVCHeader(HEVC_HEADER_2, hevcHeaders[0].pHeader + hevcHeaders[0].nSize, nFilledLen, hevcHeaders[1].pHeader);
        if(hevcHeaders[1].nSize < 0) {
            OMX_DBGT_EPILOG ();
            return -1;
        }

        hevcHeaders[2].nSize = findHEVCHeader(HEVC_HEADER_3, hevcHeaders[1].pHeader + hevcHeaders[1].nSize, nFilledLen, hevcHeaders[2].pHeader);
        if(hevcHeaders[2].nSize < 0) {
            OMX_DBGT_EPILOG ();
            return -1;
        }

        nFilledLen = hevcHeaders[0].nSize + hevcHeaders[1].nSize + hevcHeaders[2].nSize + 3*4;

        int skip = 0;
        for(int i = 0; i < 3; i++) {
            const uint8_t scBuf[4] = {0x00,0x00,0x00,0x01};
            memcpy(pBuffer+skip, scBuf, 4);
            skip += 4;
            memcpy(pBuffer+skip, hevcHeaders[i].pHeader, hevcHeaders[i].nSize);
            skip += hevcHeaders[i].nSize;
        }

        char* debug_buffer = new char[nFilledLen*3 + 3];
        for(uint i = 0; i < nFilledLen; i++) {
            sprintf(&debug_buffer[i*3], "%2x ",pBuffer[i]);
        }
        OMX_DBGT_PTRACE("debug_buffer: %s",debug_buffer);
        delete[] debug_buffer;
    }

    // Write PES header
    memset (PesHeader, 0, PES_MAX_HEADER_SIZE);
    PesHeaderLength            = InsertPesHeader (PesHeader, nFilledLen, H264_VIDEO_PES_START_CODE, INVALID_PTS_VALUE, 0);
    err = this->viddec().dvb().write(PesHeader, PesHeaderLength);
    if(err)
    {
       OMX_DBGT_ERROR("%s::HEVCEmitCodecConfig->dvb().write failed", name());
       goto error;
    }

    // Write data
    err = this->viddec().dvb().write(pBuffer, nFilledLen);
    if(err)
    {
       OMX_DBGT_ERROR("%s::HEVCEmitCodecConfig->dvb().write failed", name());
    }
error:
    OMX_DBGT_EPILOG();
    return err;
}

int HEVCPesAdapter::emitFrame(uint8_t* pBuffer, unsigned int nFilledLen,
                             uint64_t nTimeStamp) const
{
    int err = 0;
    int PesHeaderLength;
    uint64_t nPTS = PTS_CORRECTION(nTimeStamp);

    OMX_DBGT_PROLOG();
    OMX_DBGT_PTRACE("pBuf=%p, len=%lu, nTimeStamp=%llu", pBuffer, (unsigned long)nFilledLen, nTimeStamp);

    if (nFilledLen == 0) {
        OMX_DBGT_EPILOG("Empty frame");
        return 0;
    }

    char sha[10*3 +1];

    for(uint i = 0; i < 10; i++) {
        sprintf(&sha[i*3], "%2x ",pBuffer[i]);
    }
    OMX_DBGT_PTRACE("sha: %s",sha);

    if( ( (pBuffer[0] == 0 && pBuffer[1] == 0 && pBuffer[2] == 1) || (pBuffer[1] == 0 && pBuffer[2] == 0 && pBuffer[3] == 1) )
        && (nNALLengthSize == 0) ){
        OMX_DBGT_PTRACE("HEVC SC Found");
    }
    else
    {
        OMX_DBGT_PTRACE("nNALLengthSize: %d", nNALLengthSize);

        if(nNALLengthSize > 0) {
            uint8_t* buffer = pBuffer;
            if((nNALLengthSize == 3) || (nNALLengthSize == 4)) {
                while(buffer < pBuffer + nFilledLen) {
                    OMX_DBGT_PTRACE("buffer in: %p",buffer);
                    int size = 0;
                    for(int i = 0; i<nNALLengthSize; i++) {
                        if(i > 0)
                            size = size<<8;
                        size += buffer[i];
                        buffer[i] = 0x0;
                    }
                    OMX_DBGT_PTRACE("size: %d",size);
                    buffer[nNALLengthSize - 1] = 0x1;
                    buffer = buffer + nNALLengthSize +size;
                    OMX_DBGT_PTRACE("buffer out: %p",buffer);
                }
            } else {
                OMX_DBGT_EPILOG("unsupported nNALLengthSize: %d",nNALLengthSize);
                return 0;
            }
        }
    }

#ifdef DUMP_FRAMES
    FILE* fp = NULL;
    char fname[256];
    sprintf(fname, "/data/dump/frame_%d.bin",xx);
    fp = fopen(fname,"wb");
    if(fp) {
        int writtenn = fwrite(pBuffer,1,nFilledLen,fp);
        ALOGE("frame writtenn: %d",writtenn);
        fclose(fp);
        fp = NULL;
    }
    if(xx == 0) {
        fp = fopen("/data/dump/frame_full.hevc","wb");
    } else {
        fp = fopen("/data/dump/frame_full.hevc","ab");
    }
    if(fp) {
        int writtenn = fwrite(pBuffer,1,nFilledLen,fp);
        ALOGE("frame full writtenn: %d",writtenn);
        fclose(fp);
        fp = NULL;
    }
    xx++;
#endif

    if(nPTS > INVALID_PTS_VALUE)
    {
        //We will anyway overflow. So send no PTS
        nPTS = INVALID_PTS_VALUE;
    }

    // Write PES header
    memset (PesHeader, 0, PES_MAX_HEADER_SIZE);
    PesHeaderLength = InsertPesHeader (PesHeader, nFilledLen, H264_VIDEO_PES_START_CODE, nPTS, 0);
    err = this->viddec().dvb().write(PesHeader, PesHeaderLength);
    if (err) {
       OMX_DBGT_ERROR("%s::HEVCEmitFrame->dvb().write failed", name());
       goto error;
    }

    // Write data
    err = this->viddec().dvb().write(pBuffer, nFilledLen);
    if (err) {
       OMX_DBGT_ERROR("%s::HEVCEmitFrame->dvb().write failed", name());
    }
error:
    OMX_DBGT_EPILOG();
    return err;
}

} // eof namespace stm
