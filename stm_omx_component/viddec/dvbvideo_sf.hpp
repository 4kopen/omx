/*
 * Copyright 2013 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file   dvbvideo_sf.hpp
 * @author STMicroelectronics
 */


#ifndef ST_OMX_DVBVIDEO_SF_H
#define ST_OMX_DVBVIDEO_SF_H

#include <omxse.h>
#include "IDvbDevice.hpp"
#include "Mutex.hpp"

class DVBVideo_SF :public stm::IDvbDevice
{
public:
    virtual int id() const;
    virtual const char* name() const;

    DVBVideo_SF(const char* strName, int nEncoding, int width, int height, bool thumbnail, const char *name);
    virtual ~DVBVideo_SF();

    virtual int create();
    virtual int destroy();
    virtual bool isOpen() const;

    virtual int start() const;
    virtual int stop() const;
    virtual int pause(bool hw) const;
    virtual int resume(bool hw) const;
    virtual void lock() const;
    virtual void unlock() const;
    virtual void resetflags() const;

    virtual uint64_t getTime() const;
    virtual uint32_t getSamplingRate() const{return 0;};
    virtual void set10Bit(bool bEnable10Bit) {m_b10BitEnable = bEnable10Bit;};

    virtual int write(const void* data_ptr, unsigned int len) const ;
    virtual int flush(bool bEoS = false) const;
    virtual int clear() const;

    virtual stm::IDvbDevice::EState state() const;

    virtual int UseBuffer(unsigned long virtualAddr, unsigned long bufferSize,
                  int width, int height, int color,
                  unsigned long userId) const;
    virtual unsigned long FlushBuffer() const;

    virtual int FillThisBuffer(unsigned long userId) const;
    virtual int FillBufferDone(unsigned long & userId, uint64_t & TS, bool & eos) const;
    virtual int QueueBuffer (void* BufferData, int &nBytesSize, uint64_t &ts) const;

protected:
    bool mIsLoggingEnabled;

private:
    void state(stm::IDvbDevice::EState st) const;

    int vdev_id;
    int m_nEncoding;
    int m_width;
    int m_height;
    bool m_b10BitEnable;
    bool m_thumbnail;
    char m_deviceName[32];
    /** Internal state to know if device started or not */
    mutable stm::IDvbDevice::EState m_nState;
    /** Serialize access to dvb commands */
    mutable stm::Mutex       m_mutexCmd;
    /** Condition variable signaled when state has changed */
    mutable stm::Condition   m_condState;
};

#endif
