/*
 * Copyright 2011 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *
 * File   ::    vc1.cpp
 * Author ::    Damien TOURNUS (damien.tournus@st.com)
 *
 *
 */

#include "OMX_debug.h"
#include "VideoPesAdapter.hpp"
#include "VideoDecoder.hpp"

#include "pes.hpp"

#define DBGT_PREFIX "VC1 "
#define DBGT_LAYER  1
#include <linux/dbgt.h>

// #define VC1_DEBUG

#ifndef EINVAL
#define EINVAL	22
#endif

#define PTS_CORRECTION(x) (((x) * 90000)/ 1000000)
//
// WMV codec (VC-1 Simple and Main profiles)
//

namespace stm {

WmvPesAdapter::WmvPesAdapter(const VDECComponent& dec)
    :VideoPesAdapter(dec)
{}

WmvPesAdapter::~WmvPesAdapter()
{}

int WmvPesAdapter::emitInitialHeader() const
{
    return 0;
}

// pBuffer points to data that are STRUCT_C(Sc) and STRUCT_A(Sa) information packed as follows
// Sc0 Sc1 Sc2 Sc3 Sa0 Sa1 Sa2 Sa3
// Sa4 Sa5 Sa6 Sa7
int WmvPesAdapter::emitCodecConfig(uint8_t* pBuffer,
                                   unsigned int nFilledLen) const
{
    int err = 0;
    int PesHeaderLength = 0;
    unsigned char* PesPtr = NULL;
    unsigned char Metadata[] = { 0x00,   0x00,   0x00,   0xc5,
                                 0x04,   0x00,   0x00,   0x00,
                                 0xc0,   0x00,   0x00,   0x00,   /* Struct C */
                                 0x00,   0x00,   0x00,   0x00,   /* Struct A */
                                 0x00,   0x00,   0x00,   0x00,
                                 0x0c,   0x00,   0x00,   0x00,
                                 0x60,   0x00,   0x00,   0x00,   /* Struct B */
                                 0x00,   0x00,   0x00,   0x00,
                                 0x00,   0x00,   0x00,   0x00};
    int MetadataLength = sizeof(Metadata);

    OMX_DBGT_PROLOG();

    PesPtr = &PesHeader[PES_MIN_HEADER_SIZE];

    /* if the buffer length is 12 then we insert metadata and then copy struct A/C information in the header */
    /* VLC player passes 36 byte header which is the metadata header itself with all required information, so in
       this case we copy the header as it is without doing any manupilation */
    if (nFilledLen == 12) {
        // Copy the header skeleton
        memcpy (PesPtr, Metadata, MetadataLength);

        //  Add STRUCT_C and STRUCT_A information
        PesPtr += 8;
        memcpy(PesPtr, pBuffer, 12);
        PesPtr += 12;

        // Skip flag word and STRUCT_B first 8 bytes */
        PesPtr += 12;

        // Add the frame rate field of STRUCT_B
        // FRAMERATE is set to 0xFFFFFFFF to specify that it's not known,
        // unspecified or non-constant
        *PesPtr++ = 0xFF;
        *PesPtr++ = 0xFF;
        *PesPtr++ = 0xFF;
        *PesPtr++ = 0xFF;
    } else if (nFilledLen == 36) {
        memcpy (PesPtr, pBuffer, nFilledLen);
    } else {
        return -EINVAL;
    }

    // Write PES header and data
    PesHeaderLength = InsertPesHeader (PesHeader, MetadataLength, VC1_VIDEO_PES_START_CODE, INVALID_PTS_VALUE, 0);
    err = this->viddec().dvb().write (PesHeader, PesHeaderLength + MetadataLength);
    if(err)
    {
       OMX_DBGT_ERROR("%s::WMVEmitCodecConfig->dvb().write failed", name());
       goto error;
    }

#ifdef VC1_DEBUG
    {
        int i = 0;
        char* LogPtr = (char *)malloc ((PesHeaderLength + MetadataLength) * 3 + 1);

        OMX_DBGT_PINFO("%s WMV codec specific data length %lu (VDECComponent::WMVEmitCodecConfig)", name(), (unsigned long)PesHeaderLength + (unsigned long)MetadataLength);
        for(i = 0; i < PesHeaderLength + MetadataLength; i++)
        {
            sprintf (LogPtr+3*i, "%.2x ", PesHeader[i]);
        }
        OMX_DBGT_PINFO("%s %s (VDECComponent::WMVEmitCodecConfig)", name(), LogPtr);
        free (LogPtr);
    }
#endif
error:
    OMX_DBGT_EPILOG();
    return err;
}

int WmvPesAdapter::emitFrame(uint8_t* pBuffer, unsigned int nFilledLen,
                             uint64_t nTimeStamp) const
{
    int err = 0;
    int PesHeaderLength = 0;
    int PesLength;
    int PrivateHeaderLength = 0;
    uint64_t nPTS = PTS_CORRECTION(nTimeStamp);

    OMX_DBGT_PROLOG();
    OMX_DBGT_PTRACE("WMV pBuf=%p, len=%lu, ntimestamp=%llu", pBuffer, (unsigned long)nFilledLen, nTimeStamp);

    if(nPTS > INVALID_PTS_VALUE)
    {
        //We will anyway overflow. So send no PTS
        nPTS = INVALID_PTS_VALUE;
    }

    // Write PES header
    memset (PesHeader, 0, PES_MAX_HEADER_SIZE);
    PesHeaderLength = InsertPesHeader (PesHeader, nFilledLen, VC1_VIDEO_PES_START_CODE, nPTS, 0);

    // Update PES length
    PrivateHeaderLength = InsertVideoPrivateDataHeaderForAudio (&PesHeader[PesHeaderLength], nFilledLen);

    PesLength                               = PesHeader[PES_LENGTH_BYTE_0] + (PesHeader[PES_LENGTH_BYTE_1] << 8) + PrivateHeaderLength;
    PesHeader[PES_LENGTH_BYTE_0]            = PesLength & 0xff;
    PesHeader[PES_LENGTH_BYTE_1]            = (PesLength >> 8) & 0xff;
    PesHeader[PES_HEADER_DATA_LENGTH_BYTE] += PrivateHeaderLength;
    PesHeader[PES_FLAGS_BYTE]              |= PES_EXTENSION_DATA_PRESENT;
    PesHeaderLength                        += PrivateHeaderLength;

    err = this->viddec().dvb().write (PesHeader, PesHeaderLength);
    if(err)
    {
       OMX_DBGT_ERROR("%s::WMVEmitFrame->dvb().write failed", name());
       goto error;
    }

#ifdef VC1_DEBUG
    {
        int i = 0;
        char* LogPtr = (char *)malloc (PesHeaderLength * 3 + 1);

        OMX_DBGT_PINFO("%s WMV data header length %lu (VDECComponent::WMVEmitFrame)", name(), (unsigned long)PesHeaderLength);
        for(i = 0; i < PesHeaderLength; i++)
        {
            sprintf (LogPtr+3*i, "%.2x ", PesHeader[i]);
        }
        OMX_DBGT_PINFO("%s %s (VDECComponent::WMVEmitFrame)", name(), LogPtr);
        free (LogPtr);
    }
#endif

    // Write data
    if (nFilledLen > 0)
        err = this->viddec().dvb().write(pBuffer, nFilledLen);
    if(err)
    {
       OMX_DBGT_ERROR("%s::WMVEmitFrame->dvb().write failed", name());
       goto error;
    }

#ifdef VC1_DEBUG
    {
        int i = 0;
        char* LogPtr = (char *)malloc (40 * 3 + 1);

        OMX_DBGT_PINFO("%s WMV data length %lu (VDECComponent::WMVEmitFrame)", name(), (unsigned long)nFilledLen);
        for(i = 0; i < 40; i++)
        {
            sprintf (LogPtr+3*i, "%.2x", pBuffer[i]);
        }
        OMX_DBGT_PINFO("%s %s (VDECComponent::WMVEmitFrame)", name(), LogPtr);
        free (LogPtr);
    }
#endif

error:
    OMX_DBGT_EPILOG();
    return err;
}

//
// VC-1 codec (VC-1 Advanced Profile
//

Vc1PesAdapter::Vc1PesAdapter(const VDECComponent& dec)
    :VideoPesAdapter(dec)
{}

Vc1PesAdapter::~Vc1PesAdapter()
{}

int Vc1PesAdapter::emitInitialHeader() const
{
    return 0;
}

// pBuffer points to data that are a standard VC-1 Sequence Header (SH)
int Vc1PesAdapter::emitCodecConfig(uint8_t* pBuffer,
                                   unsigned int nFilledLen) const
{
    int err = 0;
    int PesHeaderLength = 0;

    OMX_DBGT_PROLOG();

    // Write PES header and data
    memset (PesHeader, 0, PES_MAX_HEADER_SIZE);
    memcpy (&PesHeader[PES_MIN_HEADER_SIZE], pBuffer, nFilledLen);
    PesHeaderLength = InsertPesHeader (PesHeader, nFilledLen, VC1_VIDEO_PES_START_CODE, INVALID_PTS_VALUE, 0);
    err = this->viddec().dvb().write (PesHeader, PesHeaderLength + nFilledLen);
    if(err)
    {
       OMX_DBGT_ERROR("%s::VC1EmitCodecConfig->dvb().write failed", name());
       goto error;
    }

#ifdef VC1_DEBUG
    {
        int i = 0;
        char* LogPtr = (char *)malloc ((PesHeaderLength + nFilledLen) * 3 + 1);

        OMX_DBGT_PINFO("%s VC-1 codec specific data length %lu (VDECComponent::VC1EmitCodecConfig)", name(), (unsigned long)PesHeaderLength + (unsigned long)nFilledLen);
        for(i = 0; i < PesHeaderLength + (int)nFilledLen; i++)
        {
            sprintf (LogPtr+3*i, "%.2x ", PesHeader[i]);
        }
        OMX_DBGT_PINFO("%s %s (VDECComponent::VC1EmitCodecConfig)", name(), LogPtr);
        free (LogPtr);
    }
#endif

error:
    OMX_DBGT_EPILOG();
    return err;
}

int Vc1PesAdapter::emitFrame(uint8_t* pBuffer, unsigned int nFilledLen,
                             uint64_t pts) const
{
    int err = 0;
    int PesHeaderLength = 0;
    char Vc1FrameStartCode[] = {0x00, 0x00, 0x01, 0x0d};
    char Vc1SequenceHeaderCode[] = {0x00, 0x00, 0x01, 0x0f};

    OMX_DBGT_PROLOG();

    // Write PES header
    memset (PesHeader, 0, PES_MAX_HEADER_SIZE);
    PesHeaderLength = InsertPesHeader (PesHeader, nFilledLen, VC1_VIDEO_PES_START_CODE, pts * 90000 / 1000000, 0);

    // Add a frame header if not present
    if ((nFilledLen <= 3) || ((memcmp (pBuffer, Vc1FrameStartCode, 4) != 0) && (memcmp (pBuffer, Vc1SequenceHeaderCode, 4) != 0)))
    {
        memcpy (&PesHeader[PesHeaderLength], Vc1FrameStartCode, sizeof (Vc1FrameStartCode));
        PesHeaderLength += sizeof (Vc1FrameStartCode);
    }

    err = this->viddec().dvb().write(PesHeader, PesHeaderLength);
    if(err)
    {
       OMX_DBGT_ERROR("%s::VC1EmitFrame->dvb().write failed", name());
       goto error;
    }

#ifdef VC1_DEBUG
    {
        int i = 0;
        char* LogPtr = (char *)malloc (PesHeaderLength * 3);

        OMX_DBGT_PINFO("%s VC-1 data header length %lu (VDECComponent::VC1EmitFrame)", name(), (unsigned long)PesHeaderLength + 1);
        for(i = 0; i < PesHeaderLength; i++)
        {
            sprintf (LogPtr+3*i, "%.2x ", PesHeader[i]);
        }
        OMX_DBGT_PINFO("%s %s (VDECComponent::VC1EmitFrame)", name(), LogPtr);
        free (LogPtr);
    }
#endif

    // Write data
    if (nFilledLen > 0)
        err = this->viddec().dvb().write(pBuffer, nFilledLen);
    if(err)
    {
       OMX_DBGT_ERROR("%s::VC1EmitFrame->dvb().write failed", name());
       goto error;
    }

#ifdef VC1_DEBUG
    {
        int i = 0;
        char* LogPtr = (char *)malloc (40 * 3 + 1);

        OMX_DBGT_PINFO("%s VC-1 data length %lu (VDECComponent::VC1EmitFrame)", name(), (unsigned long)nFilledLen);
        for(i = 0; i < 40; i++)
        {
            sprintf (LogPtr+3*i, "%.2x ", pBuffer[i]);
        }
        OMX_DBGT_PINFO("%s %s (VDECComponent::VC1EmitFrame)", name(), LogPtr);
        free (LogPtr);
    }
#endif

error:
    OMX_DBGT_EPILOG();
    return err;
}

} // eof namespace stm
