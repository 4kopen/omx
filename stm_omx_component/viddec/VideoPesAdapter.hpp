/*
 * Copyright 2013 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file   VideoPesAdapter.hpp
 * @author STMicroelectronics
 */

#ifndef _STM_VIDEOPESADAPTER_HPP_
#define _STM_VIDEOPESADAPTER_HPP_

#include <stdint.h>

namespace stm {

// Forward declaration
class VDECComponent;

class VideoPesAdapter
{
public:
    VideoPesAdapter(const VDECComponent& dec);

    virtual ~VideoPesAdapter(); 

    /**
     * Factory method returning right PesAdapter instance
     * depending on codec type
     */
    static VideoPesAdapter* create(int nCoding,
                                   const VDECComponent& aDec);

public:
    virtual int emitInitialHeader() const = 0;

    virtual int emitCodecConfig(uint8_t* pBuffer,
                                unsigned int nFilledLen) const = 0;
    
    virtual int emitFrame(uint8_t* pBuffer, unsigned int nFilledLen,
                          uint64_t nTimeStamp) const = 0;
    
    const char* name() const;

protected:
    unsigned char*       PesHeader;
    const VDECComponent& viddec() const;

protected:
    bool mIsLoggingEnabled;

private:
    const VDECComponent&  m_viddec;
};

inline const VDECComponent& VideoPesAdapter::viddec() const
{ return m_viddec; }


/**
 * Avc codec
 */
class AvcPesAdapter: public VideoPesAdapter
{
public:
    AvcPesAdapter(const VDECComponent& dec);

    virtual ~AvcPesAdapter(); 

public:
    virtual int emitInitialHeader() const;

    virtual int emitCodecConfig(uint8_t* pBuffer,
                                unsigned int nFilledLen) const;
    
    virtual int emitFrame(uint8_t* pBuffer, unsigned int nFilledLen,
                          uint64_t nTimeStamp) const;
};

/**
 * Flv1 codec
 */
class Flv1PesAdapter: public VideoPesAdapter
{
public:
    Flv1PesAdapter(const VDECComponent& dec);

    virtual ~Flv1PesAdapter(); 

public:
    virtual int emitInitialHeader() const;

    virtual int emitCodecConfig(uint8_t* pBuffer,
                                unsigned int nFilledLen) const;
    
    virtual int emitFrame(uint8_t* pBuffer, unsigned int nFilledLen,
                          uint64_t nTimeStamp) const;
};

/**
 * H263 codec
 */
class H263PesAdapter: public VideoPesAdapter
{
public:
    H263PesAdapter(const VDECComponent& dec);

    virtual ~H263PesAdapter(); 

public:
    virtual int emitInitialHeader() const;

    virtual int emitCodecConfig(uint8_t* pBuffer,
                                unsigned int nFilledLen) const;
    
    virtual int emitFrame(uint8_t* pBuffer, unsigned int nFilledLen,
                          uint64_t nTimeStamp) const;
};

/**
 * Mjpeg codec
 */
class MjpegPesAdapter: public VideoPesAdapter
{
public:
    MjpegPesAdapter(const VDECComponent& dec);

    virtual ~MjpegPesAdapter(); 

public:
    virtual int emitInitialHeader() const;

    virtual int emitCodecConfig(uint8_t* pBuffer,
                                unsigned int nFilledLen) const;
    
    virtual int emitFrame(uint8_t* pBuffer, unsigned int nFilledLen,
                          uint64_t nTimeStamp) const;
};

/**
 * Mpeg2 codec
 */
class Mpeg2PesAdapter: public VideoPesAdapter
{
public:
    Mpeg2PesAdapter(const VDECComponent& dec);

    virtual ~Mpeg2PesAdapter(); 

public:
    virtual int emitInitialHeader() const;

    virtual int emitCodecConfig(uint8_t* pBuffer,
                                unsigned int nFilledLen) const;
    
    virtual int emitFrame(uint8_t* pBuffer, unsigned int nFilledLen,
                          uint64_t nTimeStamp) const;
};

/**
 * Mpeg4 codec
 */
class Mpeg4PesAdapter: public VideoPesAdapter
{
public:
    Mpeg4PesAdapter(const VDECComponent& dec);

    virtual ~Mpeg4PesAdapter(); 

public:
    virtual int emitInitialHeader() const;

    virtual int emitCodecConfig(uint8_t* pBuffer,
                                unsigned int nFilledLen) const;
    
    virtual int emitFrame(uint8_t* pBuffer, unsigned int nFilledLen,
                          uint64_t nTimeStamp) const;
};

/**
 * Vc1 codec
 */
class Vc1PesAdapter: public VideoPesAdapter
{
public:
    Vc1PesAdapter(const VDECComponent& dec);

    virtual ~Vc1PesAdapter(); 

public:
    virtual int emitInitialHeader() const;

    virtual int emitCodecConfig(uint8_t* pBuffer,
                                unsigned int nFilledLen) const;
    
    virtual int emitFrame(uint8_t* pBuffer, unsigned int nFilledLen,
                          uint64_t nTimeStamp) const;
};

/**
 * Wmv codec
 */
class WmvPesAdapter: public VideoPesAdapter
{
public:
    WmvPesAdapter(const VDECComponent& dec);

    virtual ~WmvPesAdapter(); 

public:
    virtual int emitInitialHeader() const;

    virtual int emitCodecConfig(uint8_t* pBuffer,
                                unsigned int nFilledLen) const;
    
    virtual int emitFrame(uint8_t* pBuffer, unsigned int nFilledLen,
                          uint64_t nTimeStamp) const;
};

/**
 * VP8 codec
 */
class VP8PesAdapter: public VideoPesAdapter
{
public:
    VP8PesAdapter(const VDECComponent& dec);

    virtual ~VP8PesAdapter(); 

public:
    virtual int emitInitialHeader() const;

    virtual int emitCodecConfig(uint8_t* pBuffer,
                                unsigned int nFilledLen) const;
    
    virtual int emitFrame(uint8_t* pBuffer, unsigned int nFilledLen,
                          uint64_t nTimeStamp) const;
};


/**
 * THEORA codec
 */
class THEORAPesAdapter: public VideoPesAdapter
{
public:
    THEORAPesAdapter(const VDECComponent& dec);

    virtual ~THEORAPesAdapter();

public:
    virtual int emitInitialHeader() const;

    virtual int emitCodecConfig(uint8_t* pBuffer,
                                unsigned int nFilledLen) const;

    virtual int emitFrame(uint8_t* pBuffer, unsigned int nFilledLen,
                          uint64_t nTimeStamp) const;

    int findTheoraHeader(int headertype, uint8_t* buffer, unsigned int& maxsize, uint8_t*& header) const;
};

/**
 * CAVS codec
 */
class CAVSPesAdapter: public VideoPesAdapter
{
public:
    CAVSPesAdapter(const VDECComponent& dec);

    virtual ~CAVSPesAdapter();

public:
    virtual int emitInitialHeader() const;

    virtual int emitCodecConfig(uint8_t* pBuffer,
                                unsigned int nFilledLen) const;

    virtual int emitFrame(uint8_t* pBuffer, unsigned int nFilledLen,
                          uint64_t nTimeStamp) const;

};

/**
 * HEVC codec
 */
class HEVCPesAdapter: public VideoPesAdapter
{
public:
    HEVCPesAdapter(const VDECComponent& dec);

    virtual ~HEVCPesAdapter();

public:
    virtual int emitInitialHeader() const;

    virtual int emitCodecConfig(uint8_t* pBuffer,
                                unsigned int nFilledLen) const;

    virtual int emitFrame(uint8_t* pBuffer, unsigned int nFilledLen,
                          uint64_t nTimeStamp) const;

    mutable int nNALLengthSize;

private:
    int findHEVCHeader(int headertype, uint8_t* buffer, unsigned int& maxsize, uint8_t*& header) const;

};

} // eof nm stm

#endif
