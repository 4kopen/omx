/*
 * Copyright 2013 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file   dvbvideo_sf.cpp
 * @author STMicroelectronics
 */

#include <sys/ioctl.h>
#include <sys/types.h>
#include <fcntl.h>
#include <errno.h>
#include <OMX_debug.h>
#include <dvbvideo_sf.hpp>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#if HAVE_ASSERT_H
#include <assert.h>
#endif

#if HAVE_DBGTASSERT_H
// Re-direct assert on DBGT_ASSERT macro
#define assert(expr)  OMX_DBGT_ASSERT(expr)
#endif

#undef  DBGT_TAG
#define DBGT_TAG "DVBVS"
#define DBGT_PREFIX "DVBVS"
#define DBGT_LAYER  1
#define DBGT_VAR mDbgtVarDVBVS
#define DBGT_DECLARE_AUTOVAR
#include <linux/dbgt.h>

#define THREAD_GUARD(name)  stm::ScopedLock name(m_mutexCmd)

//=============================================================================
// DVBVideo_SF::DVBVideo_SF
//  Constructor
//=============================================================================
DVBVideo_SF::DVBVideo_SF(const char* aDeviceName, int nEncoding, int width, int height, bool thumbnail, const char *cName)
    :IDvbDevice(),
     mIsLoggingEnabled(false),
     vdev_id(-1),
     m_nEncoding(nEncoding),
     m_width(width),
     m_height(height),
     m_b10BitEnable(false),
     m_thumbnail(thumbnail),
     m_nState(EState_Stop),
     m_mutexCmd(),
     m_condState()
{
    // Reuse VDEC trace property
    DBGT_TRACE_INIT(vdec);
    //check the string length before copy
    assert(strlen(aDeviceName) <= sizeof(m_deviceName));
    strcpy(m_deviceName, aDeviceName);
    OMXDEBUG_UPDATE_AV_TRACE_SETTINGS(cName);
}

DVBVideo_SF::~DVBVideo_SF()
{}

int DVBVideo_SF::id() const
{ return vdev_id; }

const char* DVBVideo_SF::name() const
{ return m_deviceName; }

stm::IDvbDevice::EState DVBVideo_SF::state() const
{ return m_nState; }

void DVBVideo_SF::state(stm::IDvbDevice::EState st) const
{
    m_nState = st;
    m_condState.signal();
}

//=============================================================================
// DVBVideo_SF::create
//=============================================================================
int DVBVideo_SF::create()
{
    THREAD_GUARD(guard);
    OMX_DBGT_PROLOG("Creating DVB video %s device", name());
    omxse_setencoding setencoding;

    vdev_id = open(name(), O_RDWR | O_NONBLOCK);
    if(vdev_id < 0)
    {
        OMX_DBGT_ERROR("%s::Not able to open /dev/omxse.grabber device (%s)", name(), strerror (errno));
        return -1;
    }

    setencoding.Encoding = (omxse_encoding) m_nEncoding;
    setencoding.thumbnail = m_thumbnail;
    setencoding.width = m_width;
    setencoding.height = m_height;
    setencoding.enable10Bit = m_b10BitEnable;
    //check the string length before copy
    assert(strlen(name()) <= sizeof(setencoding.componentName));
    strcpy(setencoding.componentName, name());

    if (ioctl(id(), OMXSE_SET_ENCODING, (void*)&setencoding) != 0)
    {
        OMX_DBGT_ERROR("%s::OMXSE_SET_ENCODING ioctl failed (%s)", name(), strerror (errno));
        return -1;
    }

    OMX_DBGT_EPILOG();
    return 0;
}

//=============================================================================
// DVBVideo_SF::destroy
//=============================================================================
int DVBVideo_SF::destroy()
{
    OMX_DBGT_PROLOG();
    if(close(id()) != 0) {
        OMX_DBGT_ERROR("Video close failed (%s)", strerror (errno));
        OMX_DBGT_EPILOG("Unable to close device");
        return -1;
    }

    vdev_id = -1;
    OMX_DBGT_EPILOG();
    return 0;
}

bool DVBVideo_SF::isOpen() const
{ return (id() != -1); }

//=============================================================================
// DVBVideo_SF::UseBuffer
//=============================================================================
int DVBVideo_SF::UseBuffer(unsigned long virtualAddr, unsigned long bufferSize,
                        int width, int height, int color,
                        unsigned long userId) const
{
    THREAD_GUARD(guard);
    OMX_DBGT_PROLOG();
    omxse_usebuffer usebuffer;
    int retval;
    usebuffer.userID = userId;
    usebuffer.virtualAddr = virtualAddr;
    usebuffer.bufferSize = bufferSize;
    usebuffer.width = width;
    usebuffer.height = height;
    usebuffer.Color = (omxse_color) color;

    retval = ioctl(vdev_id, OMXSE_USE_BUFFER, &usebuffer);
    OMX_DBGT_EPILOG();
    return retval;
}

//=============================================================================
// DVBVideo_SF::start
//=============================================================================
int DVBVideo_SF::start() const
{
    OMX_DBGT_PROLOG();
    assert(isOpen());
    if (this->isStarted()) {
        OMX_DBGT_EPILOG("Already started");
        return 0;
    }
    int retval = ioctl(id(), OMXSE_START, NULL);

    this->state(stm::IDvbDevice::EState_Start);
    OMX_DBGT_EPILOG();
    return retval;
}

//=============================================================================
// DVBVideo_SF::stop
//=============================================================================
int DVBVideo_SF::stop() const
{
    OMX_DBGT_PROLOG();
    assert(isOpen());
    if (this->isStopped()) {
        OMX_DBGT_EPILOG("Already stopped");
        return 0;
    }
    if (ioctl(id(), OMXSE_STOP, (void*)0) != 0) {
        OMX_DBGT_ERROR("OMXSE_STOP ioctl failed (%s)", strerror (errno));
        OMX_DBGT_EPILOG("Unable to stop device");
        return -1;
    }
    this->state(stm::IDvbDevice::EState_Stop);
    OMX_DBGT_EPILOG();
    return 0;
}

//=============================================================================
// DVBVideo_SF::pause (empty function)
//=============================================================================
int DVBVideo_SF::pause(bool hw) const
{
    assert(0);
    return 0;
}

//=============================================================================
// DVBVideo_SF::resume (empty function)
//=============================================================================
int DVBVideo_SF::resume(bool hw) const
{
    assert(0);
    return 0;
}

//=============================================================================
// DVBVideo_SF::lock (empty function)
//=============================================================================
void DVBVideo_SF::lock() const
{
}

//=============================================================================
// DVBVideo_SF::unlock (empty function)
//=============================================================================
void DVBVideo_SF::unlock() const
{
}

//=============================================================================
// DVBVideo_SF::resetflags (empty function)
//=============================================================================
void DVBVideo_SF::resetflags() const
{
}

//=============================================================================
// DVBVideo_SF::write
// send frame to decode
//=============================================================================
int DVBVideo_SF::write(const void* data_ptr, unsigned int len) const
{
    OMX_DBGT_PROLOG();
    assert(isOpen());

    if (this->isStopped()) {
        OMX_DBGT_WARNING("Writing on a stopped device");
        OMX_DBGT_EPILOG("Data skipped");
        return -EAGAIN;
    }

    int retval = ::write(id(), data_ptr, (size_t)len);
    if (retval < 0)
    {
        OMX_DBGT_ERROR("Error when writing in video device (%s)", strerror(errno));
        OMX_DBGT_EPILOG("Write error");
        return -errno;
    }
    assert(retval >= 0);
    if (len != (unsigned int)retval) {
        OMX_DBGT_ERROR("Error writing Video content write=%lu != written=%d",
                   (unsigned long)len, retval);
        OMX_DBGT_EPILOG("Write error");
        return -errno;
    }

    OMX_DBGT_EPILOG();
    return 0;
}

//=============================================================================
// DVBVideo_SF::flush
//=============================================================================
int DVBVideo_SF::flush(bool bEoS) const
{
    OMX_DBGT_PROLOG();
    assert(isOpen());
    if (this->isStopped()) {
        OMX_DBGT_EPILOG("Flushing a stopped device");
        return 0;
    }
    if (bEoS) {
        if (ioctl(vdev_id, OMXSE_INJECT_EOS, (void*)0) != 0)
        {
            OMX_DBGT_ERROR("OMXSE_INJECT_EOS ioctl failed (%s)", strerror (errno));
            OMX_DBGT_EPILOG();
            return -1;
        }

    } else {

        if (ioctl(vdev_id, OMXSE_INJECT_DISCONTINUITY, (void*)0) != 0)
        {
            OMX_DBGT_ERROR("OMXSE_INJECT_DISCONTINUITY ioctl failed (%s)", strerror (errno));
            OMX_DBGT_EPILOG();
            return -1;
        }
    }

    OMX_DBGT_EPILOG();
    return 0;
}

//=============================================================================
// DVBVideo_SF::clear
//=============================================================================
int DVBVideo_SF::clear() const
{
    return 0;
}

//=============================================================================
// DVBVideo_SF::getTime (empty function)
//=============================================================================
uint64_t DVBVideo_SF::getTime() const
{
    assert(0);
    return 0;
}

//=============================================================================
// DVBVideo_SF::FillThisBuffer
//=============================================================================
int DVBVideo_SF::FillThisBuffer(unsigned long userId) const
{
    int retval;

    OMX_DBGT_PROLOG();
    assert(isOpen());
    retval = ioctl(vdev_id, OMXSE_FILL_THIS_BUFFER, userId);
    if (retval != 0)
        OMX_DBGT_ERROR("OMXSE_FILL_THIS_BUFFER ioctl failed (%s)", strerror (errno));

    OMX_DBGT_EPILOG();
    return retval;
}

//=============================================================================
// DVBVideo_SF::FillBufferDone
//=============================================================================
int DVBVideo_SF::FillBufferDone(unsigned long &userId, uint64_t &TS,
                             bool & eos) const
{
    omxse_grab grab;
    int retval;

    OMX_DBGT_PROLOG();
    assert(isOpen());
    retval = ioctl(vdev_id, OMXSE_FILL_BUFFER_DONE, &grab);

    userId = grab.userID;
    TS = grab.PTS;
    eos = grab.eos;

    OMX_DBGT_PDEBUG("PTS=%llu, EOS=%lu, userID=%lu", grab.PTS, (long unsigned int)grab.eos, (long unsigned int)grab.userID);

    OMX_DBGT_EPILOG();
    return retval;
}

//=============================================================================
// DVBVideo_SF::FlushBuffer
//=============================================================================
unsigned long DVBVideo_SF::FlushBuffer() const
{
    omxse_grab grab;
    int retval;

    OMX_DBGT_PROLOG();
    assert(isOpen());
    retval = ioctl(vdev_id, OMXSE_FLUSH_BUFFER, &grab);
    if (retval != 0)
    {
        OMX_DBGT_ERROR("OMXSE_FLUSH_BUFFER ioctl failed (%s)", strerror (errno));
        return 0;
    }

    OMX_DBGT_EPILOG();
    return grab.userID;
}

//=============================================================================
// DVBVideo_SF::QueueBuffer (empty function)
//=============================================================================
int DVBVideo_SF::QueueBuffer(void* BufferData, int &nBytesSize, uint64_t &ts) const
{
    assert(0);
    return 0;
}
