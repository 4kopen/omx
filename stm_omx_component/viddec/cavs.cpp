/*
 * Copyright 2011 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *
 * File   ::    cavs.cpp
 * Author ::    Swinder Pal SINGH (swinder.singh@st.com)
 *
 *
 */
#include "OMX_debug.h"
#include "VideoPesAdapter.hpp"
#include "VideoDecoder.hpp"

#include "pes.hpp"

#define DBGT_PREFIX "CAVS"
#define DBGT_LAYER  1
#include <linux/dbgt.h>

namespace stm {

#define PTS_CORRECTION(x) (((x) * 90000)/1000000)

CAVSPesAdapter::CAVSPesAdapter(const VDECComponent& dec)
    :VideoPesAdapter(dec)
{}

CAVSPesAdapter::~CAVSPesAdapter()
{}

int CAVSPesAdapter::emitInitialHeader() const
{
    OMX_DBGT_PROLOG();
    OMX_DBGT_EPILOG();
    return 0;
}

int CAVSPesAdapter::emitCodecConfig(uint8_t* pBuffer, unsigned int nFilledLen) const
{
    int err = 0;
    int PesHeaderLength;

    OMX_DBGT_PROLOG();
    OMX_DBGT_PTRACE("pBuf=%p, len=%lu", pBuffer, (unsigned long)nFilledLen);

    // Write PES header
    memset (PesHeader, 0, PES_MAX_HEADER_SIZE);
    PesHeaderLength            = InsertPesHeader (PesHeader, nFilledLen, MPEG_VIDEO_PES_START_CODE, INVALID_PTS_VALUE, 0);
    err = this->viddec().dvb().write(PesHeader, PesHeaderLength);
    if(err)
    {
        OMX_DBGT_ERROR("CAVSEmitCodecConfig->dvb.Write PesHeader failed");
        goto ERROR;
    }

    // Write data
    err = this->viddec().dvb().write(pBuffer, nFilledLen);
    if(err)
    {
        OMX_DBGT_ERROR("CAVSEmitCodecConfig->dvb.Write pBuffer failed");
        goto ERROR;
    }

ERROR:
    OMX_DBGT_EPILOG();
    return err;
}

int CAVSPesAdapter::emitFrame(uint8_t* pBuffer, unsigned int nFilledLen, uint64_t nTimeStamp) const
{
    int err = 0;
    int PesHeaderLength = 0;
    uint64_t nPTS = PTS_CORRECTION(nTimeStamp);

    OMX_DBGT_PROLOG();
    OMX_DBGT_PTRACE("pBuf=%p, len=%lu, nTimeStamp=%llu", pBuffer, (unsigned long)nFilledLen, nTimeStamp);

    if(nPTS > INVALID_PTS_VALUE)
    {
        //We will anyway overflow. So send no PTS
        nPTS = INVALID_PTS_VALUE;
    }

    // Write PES header
    memset (PesHeader, 0, PES_MAX_HEADER_SIZE);
    PesHeaderLength = InsertPesHeader (PesHeader, nFilledLen, MPEG_VIDEO_PES_START_CODE, nPTS, 0);
    OMX_DBGT_PTRACE("EmitFrame header size : %d nFilledLen : %d",PesHeaderLength,nFilledLen);

    err = this->viddec().dvb().write (PesHeader, PesHeaderLength);
    if(err)
    {
        OMX_DBGT_ERROR("%s::CAVSEmitFrame->dvb.Write PesHeader failed", name());
        goto ERROR;
    }

    // Write data
    if (nFilledLen > 0)
        err = this->viddec().dvb().write(pBuffer, nFilledLen);
    if(err)
    {
        OMX_DBGT_ERROR("%s::CAVSEmitFrame->dvb.Write pBuffer failed", name());
        goto ERROR;
    }

ERROR:
    OMX_DBGT_EPILOG();
    return err;
}

} // eof namespace stm
