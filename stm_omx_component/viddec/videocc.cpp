/*
 * Copyright 2013 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file   videocc.cpp
 * @author STMicroelectronics
 */

#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include "OMX_CoreExt.h"
#include "videocc.hpp"


uint32_t
OMXCCGetBits (uint32_t NbRequestedBits, uint8_t * const DataBuff_p,
    OMXWindowCommand WindowCommand)
{
  uint32_t RequestedBits = 0;    /* Returned value */
  static struct
  {
    uint32_t Value;              /* Current U32 Window value */
    uint32_t NumBits;            /* Current valid Window bits */
    uint8_t *DataPointer_p;      /* Current Window data pointer */
  } BitWindows;                 /* Parsing window */

  uint32_t Value = 0;
  uint8_t *DataPointer_p = NULL;
  uint32_t NumBits = 0;

  if (WindowCommand == OMX_CC_RESET_WINDOW) {
    /* Reset the the bits window */
    BitWindows.Value = 0;
    BitWindows.NumBits = 0;
    BitWindows.DataPointer_p = (uint8_t *) DataBuff_p;
  }

  /* Get parsing window settings */
  Value = BitWindows.Value;
  DataPointer_p = BitWindows.DataPointer_p;
  NumBits = BitWindows.NumBits;

  /* Take bits in the bits window */
  if (NumBits != 0) {
    RequestedBits = Value >> (32 - NbRequestedBits);
  }

  if (NbRequestedBits <= NumBits) {
    /* If enough bits, return the bits */
    Value = Value << (NbRequestedBits);
    /* and update the bits windows */
    NumBits -= NbRequestedBits;
  } else {
    /* If not enough bits, re-fill the bits window */
    NbRequestedBits -= NumBits;
    NumBits = 32;
    Value = (*DataPointer_p++) << 24;
    Value |= (*DataPointer_p++) << 16;
    Value |= (*DataPointer_p++) << 8;
    Value |= (*DataPointer_p++) << 0;
    RequestedBits |= (Value >> (32 - NbRequestedBits));
    Value = Value << (NbRequestedBits);
    NumBits -= NbRequestedBits;

  }

  if (WindowCommand != OMX_CC_BACKUP_WINDOW) {
    /* Store new parsing window settings */
    BitWindows.Value = Value;
    BitWindows.NumBits = NumBits;
    BitWindows.DataPointer_p = DataPointer_p;
  }

  return (RequestedBits);
}

OMXCCFormatMode omx_cc_detect_SCTE21(OMX_CCUserData * ccUserData)
{
    uint32_t identifier;
    identifier = (uint32_t) OMXCCGetBits(32, ccUserData->data,
                            OMX_CC_RESET_WINDOW);

    if(identifier == ATSC_IDENTIFIER){
        return OMX_CC_FORMAT_SCTE21;
    }
    return OMX_CC_FORMAT_DETECT;
}

OMXCCFormatMode omx_cc_detect_SCTE128 (OMX_CCUserData * ccUserData)
{
    uint32_t identifier;
    OMXCCGetBits(24, ccUserData->data, OMX_CC_RESET_WINDOW);
    identifier = (uint32_t) OMXCCGetBits(32, ccUserData->data,
                            OMX_CC_UPDATE_WINDOW);
  if ((ccUserData->is_registered)
      && (ccUserData->itu_t_t35_country_code == USA_COUNTRY_CODE)
#if defined(CC_H264_DETECT_MANUFACTURER_CODE)
      && (ccUserData->itu_t_t35_provider_code == NEW_MANUFACTURER_CODE)
#endif /* CC_H264_DETECT_MANUFACTURER_CODE */
      && (identifier == ATSC_IDENTIFIER)
      ) {
    return (OMX_CC_FORMAT_SCTE128);
  }

  return (OMX_CC_FORMAT_DETECT);
}

void OMXCCParserSCT128(OMX_CCUserData *capturedData_p, OMX_EVENT_CLOSEDCAPTIONDATA * ccEvtData_p)
{
    uint8_t ccType;
    uint8_t *dataBuff_p = capturedData_p->data;
    OMXCCGetBits(24, dataBuff_p, OMX_CC_RESET_WINDOW); //country+provider
    OMXCCGetBits(32, dataBuff_p, OMX_CC_UPDATE_WINDOW); //IDENTIFIER
    ccType = (uint8_t) OMXCCGetBits(8, dataBuff_p, OMX_CC_UPDATE_WINDOW); //user data type code
    //printf("OMX_CC_FORMAT_SCTE128, type : %d\n",ccType);
    if(ccType == OMX_CC_CLOSED_CAPTION_EIA)
    {
        //0x03 : cc_data structure
        uint32_t Index;
        bool process_cc_data_flag = false, cc_valid = false;
        uint8_t cc_count = 0;
        uint8_t cc_type = 0, cc_data1 = 0, cc_data2 = 0;

        if(capturedData_p->itu_t_t35_provider_code == 47)
        {
            // skip userdata length
            OMXCCGetBits(8, dataBuff_p, OMX_CC_UPDATE_WINDOW);
        }
        OMXCCGetBits(1, dataBuff_p, OMX_CC_UPDATE_WINDOW);     /* process em data flag */
        process_cc_data_flag =
            (OMXCCGetBits(1, dataBuff_p, OMX_CC_UPDATE_WINDOW) & 0x01);
        if (process_cc_data_flag)
        {
            /*additional_data_flag / zero bit flag*/
            OMXCCGetBits(1, dataBuff_p, OMX_CC_UPDATE_WINDOW);
            cc_count = (uint8_t) OMXCCGetBits(5, dataBuff_p, OMX_CC_UPDATE_WINDOW);
            /* em_data  : reserved*/
            OMXCCGetBits(8, dataBuff_p, OMX_CC_UPDATE_WINDOW);

            for (Index = 0; Index < cc_count; Index++)
            {
                OMX_DATA_CLOSEDCAPTION *dataCC_p = &ccEvtData_p->ClosedCaptionData[Index];

                /* SCTE128 --> one_bit */
                OMXCCGetBits(1, dataBuff_p, OMX_CC_UPDATE_WINDOW);

                /* Marker_bits (SCTE128 --> Reserved) */
                OMXCCGetBits(4, dataBuff_p, OMX_CC_UPDATE_WINDOW); 
                cc_valid =
                    (OMXCCGetBits(1, dataBuff_p, OMX_CC_UPDATE_WINDOW) & 0x01);
                cc_type =
                    (uint8_t) OMXCCGetBits(2, dataBuff_p, OMX_CC_UPDATE_WINDOW);
                cc_data1 =
                    (uint8_t) OMXCCGetBits(8, dataBuff_p, OMX_CC_UPDATE_WINDOW);
                cc_data2 =
                    (uint8_t) OMXCCGetBits(8, dataBuff_p, OMX_CC_UPDATE_WINDOW);
                dataCC_p->bClosedCaptionValid = cc_valid;
                dataCC_p->uClosedCaptionMask  = cc_type;
                if (cc_valid)
                {
                    //if ((cc_type == 0) || (cc_type == 1))
                    //send all types if valid
                    {
                        ccEvtData_p->uNumValidEntries++;
                        /* NTSC line 21 data field or valid DTV Channel Data */
                        dataCC_p->uClosedCaptionData1  = cc_data1;
                        dataCC_p->uClosedCaptionData2  = cc_data2;
                    }
                }
            }
        }
        else
        {

        }
        /* marker_bits */
        OMXCCGetBits(8, dataBuff_p, OMX_CC_UPDATE_WINDOW);
    }

}

void OMXCCParserSCT21(OMX_CCUserData *capturedData_p, OMX_EVENT_CLOSEDCAPTIONDATA * ccEvtData_p)
{
    uint8_t ccType;
    uint8_t *dataBuff_p = capturedData_p->data;
    OMXCCGetBits(32, dataBuff_p, OMX_CC_RESET_WINDOW); //IDENTIFIER
    ccType = (uint8_t) OMXCCGetBits(8, dataBuff_p, OMX_CC_UPDATE_WINDOW); //user data type code
    //printf("OMX_CC_FORMAT_SCTE21, type : %d\n",ccType);
    if(ccType == OMX_CC_CLOSED_CAPTION_EIA)
    {
        //0x03 : cc_data structure
        uint32_t Index;
        bool process_cc_data_flag = false, cc_valid = false;
        uint8_t cc_count = 0;
        uint8_t cc_type = 0, cc_data1 = 0, cc_data2 = 0;

        OMXCCGetBits(1, dataBuff_p, OMX_CC_UPDATE_WINDOW);     /* process em data flag */
        process_cc_data_flag =
            (OMXCCGetBits(1, dataBuff_p, OMX_CC_UPDATE_WINDOW) & 0x01);
        if (process_cc_data_flag)
        {
            /* This flag shall be set to 0 (SCTE128 --> zero_bit). */
            /*additional_data_flag */
            OMXCCGetBits(1, dataBuff_p, OMX_CC_UPDATE_WINDOW);
            cc_count =
                (uint8_t) OMXCCGetBits(5, dataBuff_p, OMX_CC_UPDATE_WINDOW);
            /* em_data  : reserved*/
            OMXCCGetBits(8, dataBuff_p, OMX_CC_UPDATE_WINDOW);

            for (Index = 0; Index < cc_count; Index++)
            {
                OMX_DATA_CLOSEDCAPTION *dataCC_p = &ccEvtData_p->ClosedCaptionData[Index];

                /* SCTE128 --> one_bit */
                OMXCCGetBits(1, dataBuff_p, OMX_CC_UPDATE_WINDOW);

                /* Marker_bits (SCTE128 --> Reserved) */
                OMXCCGetBits(4, dataBuff_p, OMX_CC_UPDATE_WINDOW); 
                cc_valid =
                    (OMXCCGetBits(1, dataBuff_p, OMX_CC_UPDATE_WINDOW) & 0x01);
                cc_type =
                    (uint8_t) OMXCCGetBits(2, dataBuff_p, OMX_CC_UPDATE_WINDOW);
                cc_data1 =
                    (uint8_t) OMXCCGetBits(8, dataBuff_p, OMX_CC_UPDATE_WINDOW);
                cc_data2 =
                    (uint8_t) OMXCCGetBits(8, dataBuff_p, OMX_CC_UPDATE_WINDOW);
                dataCC_p->bClosedCaptionValid = cc_valid;
                dataCC_p->uClosedCaptionMask  = cc_type;
                if (cc_valid)
                {
                    //if ((cc_type == 0) || (cc_type == 1))
                    // send all type
                    {
                        ccEvtData_p->uNumValidEntries++;
                        /* NTSC line 21 data field or valid DTV Channel Data */
                        dataCC_p->uClosedCaptionData1  = cc_data1;
                        dataCC_p->uClosedCaptionData2  = cc_data2;
                    }
                }
            }
        }
        /* marker_bits */
        OMXCCGetBits(8, dataBuff_p, OMX_CC_UPDATE_WINDOW);
   }
}

