/*
 * Copyright 2011 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *
 * File   ::    avc.c
 * Author ::    Jean-Philippe FASSINO (jean-philippe.fassino@st.com)
 *
 *
 */

#include "VideoPesAdapter.hpp"
#include "VideoDecoder.hpp"
#include "OMX_debug.h"

#include "pes.hpp"

#define DBGT_PREFIX "AVC "
#define DBGT_LAYER  1
#include <linux/dbgt.h>

#define MAX_DATA_LENGTH 20

#define PTS_CORRECTION(x) (((x) * 90000)/1000000)

namespace stm {

AvcPesAdapter::AvcPesAdapter(const VDECComponent& dec)
    :VideoPesAdapter(dec)
{}

AvcPesAdapter::~AvcPesAdapter()
{}

int AvcPesAdapter::emitInitialHeader() const
{
#define NALU_TYPE_PLAYER2_CONTAINER_PARAMETERS          24
#define CONTAINER_PARAMETERS_VERSION                    0x00
    int err = 0;
    char data[MAX_DATA_LENGTH];
    uint32_t ParametersLength                = 0;
    uint32_t TimeScale    =   0x00000018;
    uint32_t TimeDelta    =   0xffffffff;
    uint32_t PesHeaderLength;

    OMX_DBGT_PROLOG();

    data[ParametersLength++]        = 0x00;     // Start code
    data[ParametersLength++]        = 0x00;
    data[ParametersLength++]        = 0x01;
    data[ParametersLength++]        = NALU_TYPE_PLAYER2_CONTAINER_PARAMETERS;
    data[ParametersLength++]        = CONTAINER_PARAMETERS_VERSION;   // Container message version - changes when/if we vary the format of the message
    data[ParametersLength++]        = 0xff;                         // Field separator

    TimeDelta                       = 1;

    data[ParametersLength++]        = (TimeScale >> 24) & 0xff;         // Output the timescale
    data[ParametersLength++]        = (TimeScale >> 16) & 0xff;
    data[ParametersLength++]        = 0xff;
    data[ParametersLength++]        = (TimeScale >> 8) & 0xff;
    data[ParametersLength++]        = TimeScale & 0xff;
    data[ParametersLength++]        = 0xff;

    data[ParametersLength++]        = (TimeDelta >> 24) & 0xff;         // Output frame period
    data[ParametersLength++]        = (TimeDelta >> 16) & 0xff;
    data[ParametersLength++]        = 0xff;
    data[ParametersLength++]        = (TimeDelta >> 8) & 0xff;
    data[ParametersLength++]        = TimeDelta & 0xff;
    data[ParametersLength++]        = 0xff;
    data[ParametersLength++]        = 0x80;                                         // Rsbp trailing bits

    if (ParametersLength>MAX_DATA_LENGTH)
    {
       OMX_DBGT_ERROR("%s::AVCEmitCodecConfig-> data size too small !!! (%d>%d) ", name(),ParametersLength,MAX_DATA_LENGTH);
       err = -1;
       OMX_DBGT_EPILOG();
       return err;
    }

    // Write PES header
    memset (PesHeader, 0, PES_MAX_HEADER_SIZE);
    PesHeaderLength            = InsertPesHeader (PesHeader, ParametersLength, H264_VIDEO_PES_START_CODE, INVALID_PTS_VALUE, 0);
    err = this->viddec().dvb().write(PesHeader, PesHeaderLength);
    if(err)
    {
       OMX_DBGT_ERROR("%s::AVCEmitCodecConfig->dvb().write failed", name());
       goto error;
    }

    // Write data
    err = this->viddec().dvb().write(data, ParametersLength);
    if(err)
    {
       OMX_DBGT_ERROR("%s::AVCEmitCodecConfig->dvb().write failed", name());
    }

error:
    OMX_DBGT_EPILOG();
    return err;
}

int AvcPesAdapter::emitCodecConfig(uint8_t* pBuffer, unsigned int nFilledLen) const
{
    int err = 0;
    int PesHeaderLength;

    OMX_DBGT_PROLOG();
    OMX_DBGT_PTRACE("pBuf=%p, len=%lu", pBuffer, (unsigned long)nFilledLen);

    // Write PES header
    memset (PesHeader, 0, PES_MAX_HEADER_SIZE);
    PesHeaderLength            = InsertPesHeader (PesHeader, nFilledLen, H264_VIDEO_PES_START_CODE, INVALID_PTS_VALUE, 0);
    err = this->viddec().dvb().write(PesHeader, PesHeaderLength);
    if(err)
    {
       OMX_DBGT_ERROR("%s::AVCEmitCodecConfig->dvb().write failed", name());
       goto error;
    }

    // Write data
    err = this->viddec().dvb().write(pBuffer, nFilledLen);
    if(err)
    {
       OMX_DBGT_ERROR("%s::AVCEmitCodecConfig->dvb().write failed", name());
    }

error:
    OMX_DBGT_EPILOG();
    return err;
}

int AvcPesAdapter::emitFrame(uint8_t* pBuffer, unsigned int nFilledLen,
                             uint64_t nTimeStamp) const
{
    int err = 0;
    int PesHeaderLength;
    uint64_t nPTS = PTS_CORRECTION(nTimeStamp);

    OMX_DBGT_PROLOG();
    OMX_DBGT_PTRACE("pBuf=%p, len=%lu, nTimeStamp=%llu", pBuffer, (unsigned long)nFilledLen, nTimeStamp);

    if (nFilledLen == 0) {
        OMX_DBGT_EPILOG("Empty frame");
        return 0;
    }

    if(nPTS > INVALID_PTS_VALUE)
    {
        //We will anyway overflow. So send no PTS
        nPTS = INVALID_PTS_VALUE;
    }

    // Write PES header & data
    memset (PesHeader, 0, PES_MAX_HEADER_SIZE);
    PesHeaderLength = InsertPesHeader (PesHeader, nFilledLen, H264_VIDEO_PES_START_CODE, nPTS, 0);
    err = this->viddec().dvb().write(PesHeader, PesHeaderLength);
    if (err) {
       OMX_DBGT_ERROR("%s::AVCEmitFrame->dvb().write failed", name());
       goto error;
    }

    // Write data
    if (nFilledLen > 0)
        err = this->viddec().dvb().write(pBuffer, nFilledLen);
    if (err) {
       OMX_DBGT_ERROR("%s::AVCEmitFrame->dvb().write failed", name());
    }

error:
    OMX_DBGT_EPILOG();
    return err;
}

} // eof namespace stm
