/*
 * Copyright 2011 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *
 * File   ::    vp8.c
 * Author ::    Puneet Gulati (puneet.gulati@st.com)
 *
 *
 */

#include "OMX_debug.h"
#include "VideoPesAdapter.hpp"
#include "VideoDecoder.hpp"

#include "pes.hpp"

#define DBGT_PREFIX "VP8 "
#define DBGT_LAYER  1
#include <linux/dbgt.h>

//#define VP8_DEBUG

namespace stm {

#define PTS_CORRECTION(x) (((x) * 90000)/1000000)

typedef struct Vp8MetaData_s
{
   unsigned int         Codec;
   unsigned int         Width;
   unsigned int         Height;
   unsigned int         Duration;
   unsigned int         FrameRate;              /* x1000000 */
} Vp8MetaData_t;

VP8PesAdapter::VP8PesAdapter(const VDECComponent& dec)
    :VideoPesAdapter(dec)
{}

VP8PesAdapter::~VP8PesAdapter()
{}


int VP8PesAdapter::emitInitialHeader() const
{
    int err = 0;
    OMX_DBGT_PROLOG();
    OMX_DBGT_EPILOG();
    return err;
}

int VP8PesAdapter::emitCodecConfig(uint8_t* pBuffer, unsigned int nFilledLen) const
{
    int err = 0;
    int PesHeaderLength = 0;
    struct Vp8MetaData_s  MetaData;

    OMX_DBGT_PROLOG ("pBuf=0x%p, len=%lu", pBuffer, (unsigned long)nFilledLen);

    MetaData.Codec = 0;
    MetaData.Width          = this->viddec().getWidth(OMX_ST_VIDEO_INPUT_PORT);
    MetaData.Height         = this->viddec().getHeight(OMX_ST_VIDEO_INPUT_PORT);
    MetaData.Duration       = 0;
    MetaData.FrameRate      = 0;

    //nFilledLen for configuration header for VP8 should be ZERO
    OMX_DBGT_ASSERT(nFilledLen == 0);
    // Write PES header
    memset (PesHeader, 0, PES_MAX_HEADER_SIZE);
    PesHeaderLength = InsertPesHeader (PesHeader, sizeof(MetaData), H263_VIDEO_PES_START_CODE, INVALID_PTS_VALUE, 0);
    OMX_DBGT_ASSERT((PesHeaderLength+sizeof(MetaData)) < PES_MAX_HEADER_SIZE);
    memcpy (PesHeader+PesHeaderLength, &MetaData, sizeof(MetaData));

#ifdef VP8_DEBUG
    OMX_DBGT_PTRACE("EmitCodecConfig header size : %d metaData size : %d",PesHeaderLength,sizeof(MetaData));
    OMX_DBGT_PTRACE("EmitCodecConfig header data : ");
    for (unsigned int i=0;i<(PesHeaderLength+sizeof(MetaData));i++)
	OMX_DBGT_PTRACE(" 0x%x",(unsigned int)PesHeader[i]);
#endif

    err = this->viddec().dvb().write (PesHeader, PesHeaderLength+sizeof(MetaData));

    OMX_DBGT_EPILOG ();
    return err;
}

int VP8PesAdapter::emitFrame(uint8_t* pBuffer, unsigned int nFilledLen, uint64_t nTimeStamp) const
{
    int err = 0;
    int PesHeaderLength = 0;
    int PesLength;
    int PrivateHeaderLength = 0;
    uint64_t nPTS = PTS_CORRECTION(nTimeStamp);

    OMX_DBGT_PROLOG();
    OMX_DBGT_PTRACE("pBuf=%p, len=%lu, nTimeStamp=%llu", pBuffer, (unsigned long)nFilledLen, nTimeStamp);

    if(nPTS > INVALID_PTS_VALUE)
    {
        //We will anyway overflow. So send no PTS
        nPTS = INVALID_PTS_VALUE;
    }

    // Write PES header
    memset (PesHeader, 0, PES_MAX_HEADER_SIZE);
    PesHeaderLength = InsertPesHeader (PesHeader, nFilledLen, H263_VIDEO_PES_START_CODE, nPTS, 0);

    // Update PES length
    PrivateHeaderLength = InsertVideoPrivateDataHeaderForAudio (&PesHeader[PesHeaderLength], nFilledLen);
    OMX_DBGT_PTRACE("EmitFrame pesheader : %d private : %d",PesHeaderLength,PrivateHeaderLength);
    PesLength                               = PesHeader[PES_LENGTH_BYTE_0] + (PesHeader[PES_LENGTH_BYTE_1] << 8) + PrivateHeaderLength;
    PesHeader[PES_LENGTH_BYTE_0]            = PesLength & 0xff;
    PesHeader[PES_LENGTH_BYTE_1]            = (PesLength >> 8) & 0xff;
    PesHeader[PES_HEADER_DATA_LENGTH_BYTE] += PrivateHeaderLength;
    PesHeader[PES_FLAGS_BYTE]              |= PES_EXTENSION_DATA_PRESENT;
    PesHeaderLength                        += PrivateHeaderLength;

    OMX_DBGT_PTRACE("EmitFrame header size : %d nFilledLen : %d",PesHeaderLength,nFilledLen);

#ifdef VP8_DEBUG
    OMX_DBGT_PTRACE("EmitFrame header data : ");
    for (int i=0;i<PesHeaderLength;i++)
	OMX_DBGT_PTRACE(" 0x%x",(unsigned int)PesHeader[i]);
#endif

    err = this->viddec().dvb().write (PesHeader, PesHeaderLength);
    if(err)
    {
        OMX_DBGT_ERROR("%s::VP8EmitFrame->dvb.Write PesHeader failed", name());
        goto ERROR;
    }

    // Write data
    if (nFilledLen > 0)
        err = this->viddec().dvb().write(pBuffer, nFilledLen);
    if(err)
    {
        OMX_DBGT_ERROR("%s::VP8EmitFrame->dvb.Write pBuffer failed", name());
        goto ERROR;
    }

ERROR:
    OMX_DBGT_EPILOG();
    return err;
}

} // eof namespace stm
