/*
 * Copyright 2013 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file   videocc.hpp
 * @author STMicroelectronics
 */


#define USER_DATA_SIZE  2048
#define MAX_CC_BLOCKS   7000
#define COPY_SIZE       300
#define ATSC_IDENTIFIER 0x47413934
#define USA_COUNTRY_CODE        0xB5    /* USA country code, defined by ITU T35 as 0xB5. */
typedef enum
{
  OMX_CC_FORMAT_DETECT,
  OMX_CC_FORMAT_DTVVID21,
  OMX_CC_FORMAT_EIA708,
  OMX_CC_FORMAT_DVS157,
  OMX_CC_FORMAT_UDATA130,
  OMX_CC_FORMAT_EIA608,
  OMX_CC_FORMAT_DVB,
  OMX_CC_FORMAT_ITU_T35,
  OMX_CC_FORMAT_SCTE128,    /* User Data - SCTE 128 */
  OMX_CC_FORMAT_ATSC,       /* User Data - A/53, Part 4 */
  OMX_CC_FORMAT_SCTE20 = OMX_CC_FORMAT_EIA608,       /* User Data - SCTE 20 */
  OMX_CC_FORMAT_SCTE21 = OMX_CC_FORMAT_EIA708,       /* User Data - SCTE 21 */
  OMX_CC_FORMAT_DTV_MPEG2 = OMX_CC_FORMAT_DTVVID21,  /* User Data - DTV MPEG-2 Video */
  OMX_CC_FORMAT_DTV_AVC = OMX_CC_FORMAT_ITU_T35     /* User Data - DTV AVC Video */
} OMXCCFormatMode;

typedef enum
{
  OMX_CC_FORBIDDEN_0,
  OMX_CC_SA_CUE_TRIGGER,
  OMX_CC_REESSERVED_2,
  OMX_CC_CLOSED_CAPTION_EIA,
  OMX_CC_ADDITIONAL_EIA_608,
  OMX_CC_LUMA_PAM,
  OMX_CC_BAR_DATA_EIA
} OMXUserDataTypeCode;

typedef enum
{
  OMX_CC_CODEC_TYPE_MPEG2 = 0,
  OMX_CC_CODEC_TYPE_H264 = 1
} OMXCCVideoCodecType;

typedef enum WindowCommand_e
{
  OMX_CC_RESET_WINDOW,
  OMX_CC_UPDATE_WINDOW,
  OMX_CC_BACKUP_WINDOW
} OMXWindowCommand;

struct OMX_CCUserData
{

  uint32_t used;
  bool is_registered;
  uint8_t itu_t_t35_country_code;
  uint8_t itu_t_t35_country_code_extension_byte;
  uint16_t itu_t_t35_provider_code;
  char uuid_iso_iec_11578[16];
  uint32_t length;
  uint32_t pts;
  uint8_t data[COPY_SIZE];

};

struct OMX_CCUserdataGeneric
{
  char id[4];
  uint32_t block_length:16;
  uint32_t header_length:16;
  uint32_t reserved_1:1;
  uint32_t padding_bytes:5;
  uint32_t stream_abridgement:1;
  uint32_t overflow:1;
  uint32_t codec_id:8;
  uint32_t reserved_2:6;
  uint32_t is_there_a_pts:1;
  uint32_t is_pts_interpolated:1;
  uint32_t reserved_3:7;
  uint32_t pts_msb:1;
  uint32_t pts:32;
};

struct OMX_CCUserDataH264
{
  uint32_t reserved:31;
  uint32_t is_registered:1;
  uint32_t itu_t_t35_country_code:8;
  uint32_t itu_t_t35_country_code_extension_byte:8;
  uint32_t itu_t_t35_provider_code:16;
  char uuid_iso_iec_11578[16];
};

struct OMX_CCUserDataMpeg2
{
  uint32_t reserved:31;
  uint32_t top_field_first:1;
};

uint32_t
OMXCCGetBits (uint32_t NbRequestedBits, uint8_t * const DataBuff_p,
    OMXWindowCommand WindowCommand);
OMXCCFormatMode omx_cc_detect_SCTE21(OMX_CCUserData * ccUserData);
OMXCCFormatMode omx_cc_detect_SCTE128(OMX_CCUserData * ccUserData);
void OMXCCParserSCT21(OMX_CCUserData *captureddata_p, OMX_EVENT_CLOSEDCAPTIONDATA * ccEvtData_p);
void OMXCCParserSCT128(OMX_CCUserData *captureddata_p, OMX_EVENT_CLOSEDCAPTIONDATA * ccEvtData_p);
