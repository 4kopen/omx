/*
 * Copyright 2011 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *
 * File   ::    mpeg2.c
 * Author ::    Jean-Philippe FASSINO (jean-philippe.fassino@st.com)
 *
 *
 */

#include "OMX_debug.h"
#include "VideoPesAdapter.hpp"
#include "VideoDecoder.hpp"

#include "pes.hpp"

#define DBGT_PREFIX "MP2 "
#define DBGT_LAYER  1
#include <linux/dbgt.h>

#define PTS_CORRECTION(x) (((x) * 90000)/1000000)

namespace stm {

Mpeg2PesAdapter::Mpeg2PesAdapter(const VDECComponent& dec)
    :VideoPesAdapter(dec)
{}

Mpeg2PesAdapter::~Mpeg2PesAdapter()
{}

int Mpeg2PesAdapter::emitInitialHeader() const
{
    return 0;
}

int Mpeg2PesAdapter::emitCodecConfig(uint8_t* pBuffer,
                                     unsigned int nFilledLen) const
{
    int err = 0;
    int PesHeaderLength;

    OMX_DBGT_PROLOG();
    OMX_DBGT_PTRACE("pBuf=%p, len=%lu", pBuffer, (unsigned long)nFilledLen);

    // Write PES header
    memset (PesHeader, 0, PES_MAX_HEADER_SIZE);
    PesHeaderLength            = InsertPesHeader (PesHeader, nFilledLen, MPEG_VIDEO_PES_START_CODE, INVALID_PTS_VALUE, 0);
    err = this->viddec().dvb().write(PesHeader, PesHeaderLength);
    if(err)
    {
       OMX_DBGT_ERROR("%s::MPEG2EmitCodecConfig->dvb().write failed", name());
       goto error;
    }

    // Write data
    err = this->viddec().dvb().write(pBuffer, nFilledLen);
    if(err)
    {
       OMX_DBGT_ERROR("%s::MPEG2EmitCodecConfig->dvb().write failed", name());
    }

error:
    OMX_DBGT_EPILOG();
    return err;
}

int Mpeg2PesAdapter::emitFrame(uint8_t* pBuffer, unsigned int nFilledLen,
                               uint64_t nTimeStamp) const
{
    int err = 0;
    int PesHeaderLength;
    uint64_t nPTS = PTS_CORRECTION(nTimeStamp);

    OMX_DBGT_PROLOG();
    OMX_DBGT_PTRACE("pBuf=%p, len=%lu, nTimeStamp=%llu", pBuffer, (unsigned long)nFilledLen, nTimeStamp);

    if(nPTS > INVALID_PTS_VALUE)
    {
        //We will anyway overflow. So send no PTS
        nPTS = INVALID_PTS_VALUE;
    }

    // Write Pes header
    memset (PesHeader, 0, PES_MAX_HEADER_SIZE);
    PesHeaderLength = InsertPesHeader (PesHeader, nFilledLen, MPEG_VIDEO_PES_START_CODE, nPTS, 0);
    err = this->viddec().dvb().write(PesHeader, PesHeaderLength);
    if(err)
    {
       OMX_DBGT_ERROR("%s::MPEG2EmitFrame->dvb().write failed", name());
       goto error;
    }

    // Write data
    if (nFilledLen > 0)
        err = this->viddec().dvb().write(pBuffer, nFilledLen);
    if(err)
    {
       OMX_DBGT_ERROR("%s::MPEG2EmitFrame->dvb().write failed", name());
    }

error:
    OMX_DBGT_EPILOG();
    return err;
}

} // eof namespace stm
