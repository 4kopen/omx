LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)

LOCAL_SRC_FILES:= \
	VideoDecoderPort.cpp         \
	VideoDecoder.cpp             \
	VideoPesAdapter.cpp          \
	videocc.cpp                  \
	dvbvideo_moo.cpp             \
	dvbvideo_sf.cpp              \
	avc.cpp mpeg4.cpp h263.cpp mpeg2.cpp vc1.cpp flv1.cpp mjpeg.cpp vp8.cpp theora.cpp cavs.cpp hevc.cpp

# Includes for ICS
LOCAL_C_INCLUDES+= \
	$(TOP)/frameworks/base/include/media/stagefright/openmax

# Includes for JB
LOCAL_C_INCLUDES+= \
	$(TOP)/frameworks/native/include/media/openmax

# Common includes
LOCAL_C_INCLUDES+= \
	$(TOP)/vendor/stm/module/omxse \
	$(TOP)/vendor/stm/hardware/omx/include/ \
	$(TOP)/vendor/stm/hardware/omx/stm_omx_component/common/ \
	$(TOP)/vendor/stm/hardware/omx/stm_omx_component/viddec/ \
	$(TOP)/vendor/stm/hardware/omx/stm_omx_component/vid/ \
	$(TOP)/vendor/stm/hardware/header/usr/include \
	$(TOP)/vendor/stm/module/stlinuxtv/linux/include \
	$(TOP)/vendor/stm/module/stlinuxtv

LOCAL_LDFLAGS += \
               -Wl,--no-warn-shared-textrel

LOCAL_SHARED_LIBRARIES := \
        libdl \
        libcutils \
        liblog \
        libhardware \
        libSTMOMXCommon


LOCAL_CPPFLAGS += -DANDROID
LOCAL_CFLAGS   += -DDBGT_CONFIG_DEBUG -DDBGT_CONFIG_AUTOVAR -DDBGT_TAG=\"VDEC\" -Werror
LOCAL_CFLAGS   += -Wno-error=switch
LOCAL_CFLAGS   += -Wno-unused-parameter
LOCAL_CFLAGS   += -DHAVE_DBGTASSERT_H
LOCAL_MODULE:= libOMX.STM.Video.Decoder
LOCAL_MODULE_TAGS := optional

include $(BUILD_SHARED_LIBRARY)

include $(CLEAR_VARS)

LOCAL_SRC_FILES:= \
	VideoDecoderPort.cpp         \
	VideoDecoder.cpp             \
	VideoPesAdapter.cpp          \
	videocc.cpp                  \
	dvbvideo_moo.cpp             \
	dvbvideo_sf.cpp              \
	avc.cpp mpeg4.cpp h263.cpp mpeg2.cpp vc1.cpp flv1.cpp mjpeg.cpp vp8.cpp theora.cpp cavs.cpp hevc.cpp

# Includes for ICS
LOCAL_C_INCLUDES+= \
	$(TOP)/frameworks/base/include/media/stagefright/openmax

# Includes for JB
LOCAL_C_INCLUDES+= \
	$(TOP)/frameworks/native/include/media/openmax

# Common includes
LOCAL_C_INCLUDES+= \
	$(TOP)/vendor/stm/module/omxse \
	$(TOP)/vendor/stm/hardware/omx/include/ \
	$(TOP)/vendor/stm/hardware/omx/stm_omx_component/common/ \
	$(TOP)/vendor/stm/hardware/omx/stm_omx_component/viddec/ \
	$(TOP)/vendor/stm/hardware/omx/stm_omx_component/vid/ \
	$(TOP)/vendor/stm/hardware/header/usr/include \
	$(TOP)/vendor/stm/module/stlinuxtv/linux/include \
	$(TOP)/vendor/stm/module/stlinuxtv

LOCAL_SHARED_LIBRARIES := \
        libdl \
        libcutils \
        liblog \
        libhardware \
        libSTMOMXCommon


LOCAL_CPPFLAGS += -DANDROID
LOCAL_CFLAGS   += -DDBGT_CONFIG_DEBUG -DDBGT_CONFIG_AUTOVAR -DDBGT_TAG=\"VDEC\" -Werror
LOCAL_CFLAGS   += -Wno-error=switch
LOCAL_CFLAGS   += -Wno-unused-parameter
LOCAL_CFLAGS   += -DHAVE_DBGTASSERT_H
LOCAL_CFLAGS   += -DSECURE_CONTENT
LOCAL_MODULE:= libOMX.STM.Video.Decoder.secure
LOCAL_MODULE_TAGS := optional

include $(BUILD_SHARED_LIBRARY)
