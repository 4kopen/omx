/*
 * Copyright 2013 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file   VideoPesAdapter.cpp
 * @author STMicroelectronics
 */

#include "OMX_debug.h"
#include "VideoPesAdapter.hpp"
#include "VideoDecoder.hpp"

#include "pes.hpp"

#include <linux/dvb/stm_dvb.h>
#include <linux/dvb/stm_video.h>
#include <malloc.h>

#define DBGT_PREFIX "VDEC"
#define DBGT_LAYER  0
#include <linux/dbgt.h>

namespace stm {

VideoPesAdapter::VideoPesAdapter(const VDECComponent& dec)
    :PesHeader(0),
     mIsLoggingEnabled(false),
     m_viddec(dec)
{
    PesHeader = (unsigned char*)malloc(MAX_PES_PACKET_SIZE);
    OMXDEBUG_UPDATE_AV_TRACE_SETTINGS(this->viddec().name());
}

VideoPesAdapter::~VideoPesAdapter()
{
    if (PesHeader) {
        free(PesHeader);
        PesHeader = 0;
    }
}

const char* VideoPesAdapter::name() const
{ return this->viddec().name(); }

VideoPesAdapter* VideoPesAdapter::create(int nCoding, const VDECComponent& aDec)
{
    VideoPesAdapter* adapt = 0;

    switch(nCoding) {
    case VIDEO_ENCODING_H264: {
        adapt = new AvcPesAdapter(aDec);
    }; break;

    case VIDEO_ENCODING_MPEG4P2: {
        adapt = new Mpeg4PesAdapter(aDec);
    }; break;
  
    case VIDEO_ENCODING_H263: {
        adapt = new H263PesAdapter(aDec);
    }; break;

    case VIDEO_ENCODING_MPEG2: {
        adapt = new Mpeg2PesAdapter(aDec);
    }; break;

    case VIDEO_ENCODING_VC1: {
        adapt = new Vc1PesAdapter(aDec);
    }; break;

    case VIDEO_ENCODING_WMV: {
        adapt = new WmvPesAdapter(aDec);
    }; break;

    case VIDEO_ENCODING_FLV1: {
        adapt = new Flv1PesAdapter(aDec);
    }; break;

    case VIDEO_ENCODING_MJPEG: {
        adapt = new MjpegPesAdapter(aDec);
    }; break;

    case VIDEO_ENCODING_VP8:
    case VIDEO_ENCODING_VP9: {
        adapt = new VP8PesAdapter(aDec);
    }; break;

    case VIDEO_ENCODING_THEORA: {
        adapt = new THEORAPesAdapter(aDec);
    }; break;

    case VIDEO_ENCODING_AVS: {
        adapt = new CAVSPesAdapter(aDec);
    }; break;

    case VIDEO_ENCODING_HEVC: {
        adapt = new HEVCPesAdapter(aDec);
    }; break;

    default: {
        DBGT_ERROR("Unsupported Video format");
        adapt = 0;
    }; break;   
    }
    return adapt;
}

}
