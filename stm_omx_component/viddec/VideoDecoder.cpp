/*
 * Copyright 2011 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *
 * File          ::    VideoDecoder.cpp
 * Author ::   Jean-Philippe FASSINO(jean-philippe.fassino@st.com)
 *
 *
 */

#include "VideoDecoder.hpp"
#include "VideoPesAdapter.hpp"
#include "StmOmxVendorExt.hpp"
#include "OMX_debug.h"
#include "pes.hpp"

#include <linux/dvb/stm_dvb.h>
#include <linux/dvb/stm_video.h>
#include <omxse.h>

#if HAVE_ASSERT_H
#include <assert.h>
#endif

#if HAVE_DBGTASSERT_H
// Re-direct assert on DBGT_ASSERT macro
#define assert(expr)  OMX_DBGT_ASSERT(expr)
#endif

#define DBGT_DECLARE_AUTOVAR
#define DBGT_PREFIX "VDEC"
#define DBGT_LAYER 0
#include <linux/dbgt.h>


namespace stm {

//==========================================
// VDECComponent implementation
//==========================================

static const char kRoleDefault[] = "default";
static const char kRoleAVC[] = "video_decoder.avc";
static const char kRoleMPEG2[] = "video_decoder.mpeg2";
static const char kRoleMPEG4[] = "video_decoder.mpeg4";
static const char kRoleH263[] = "video_decoder.h263";
static const char kRoleVC1[] = "video_decoder.vc1";
static const char kRoleWMV[] = "video_decoder.wmv";
static const char kRoleFLV1[] = "video_decoder.flv1";
static const char kRoleMJPEG[] = "video_decoder.mjpeg";
static const char kRoleVP8[] = "video_decoder.vp8";
static const char kRoleVP9[] = "video_decoder.vp9";
static const char kRoleTHEORA[] = "video_decoder.theora";
static const char kRoleSORENSON[] = "video_decoder.sorenson";
static const char kRoleCAVS[] = "video_decoder.cavs";
static const char kRoleHEVC[] = "video_decoder.hevc";

#ifdef ANDROID
const char DVB_DEVICE_VIDEO_MOO[] = "/dev/dvb0.video0";
#else
const char DVB_DEVICE_VIDEO_MOO[] = "/dev/dvb/adapter0/video0";
#endif
const char DVB_DEVICE_VIDEO_SF[] = "/dev/omxse.grabber";

VDECComponent::VDECComponent(OMX_HANDLETYPE hComponent):
    OmxComponent("OMX.STM.Video.Decoder", hComponent, OMX_ST_VIDEO_PORT_NUMBER),
    IsConfigureDone(false),
    m_mutexCmd(),
    m_condConfig(),
    m_dvb(0),
    nDecodeMode((uint32_t)OMX_VIDEO_DECODE_ALL_FRAMES),
#ifdef ANDROID
    m_grallocModule(0),
    m_grallocDevice(0),
#endif
    m_inputPort(OMX_ST_VIDEO_INPUT_PORT, OMX_DirInput, *this),
    m_outputPort(OMX_ST_VIDEO_OUTPUT_PORT, OMX_DirOutput, *this),
    m_componentRole(),
    m_pPesAdapter(0),
    hTunnelComp(NULL),
    timeMapping_p(NULL)
    , overflowTimeMapping_p(NULL)
    , m_mutexCtrl()
{
    DBGT_TRACE_INIT(vdec); //expands debug.vdec.trace
}

VDECComponent::~VDECComponent()
{
    if (m_pPesAdapter) {
        delete m_pPesAdapter;
        m_pPesAdapter = 0;
    }

    if (m_dvb) {
        delete m_dvb;
        m_dvb = 0;
    }
}

//==========================================================
// VDECComponent::flush
// flush OMX port at the end of playback when a seek occurs
//==========================================================
OMX_ERRORTYPE VDECComponent::flush(unsigned int nPortIdx)
{
    OMX_DBGT_PROLOG();
    OMX_DBGT_PDEBUG("VDECComponent::flush on port %d  %s", nPortIdx, name());
    int result = 0;

    if (!this->outputPort_().isAfterEosFlag()) {
        if (this->outputPort_().isTunneled() == false) {
            result = this->dvb().flush(false);
            m_mutexCtrl.lock();
            destroyList(timeMapping_p);
            timeMapping_p = NULL;
            destroyList(overflowTimeMapping_p);
            overflowTimeMapping_p = NULL;
            m_mutexCtrl.release();
        }
        if (result) {
            OMX_DBGT_ERROR("%s::flush failed", name());
            OMX_DBGT_EPILOG();
            return OMX_ErrorUndefined;
        }
    }

    OMX_ERRORTYPE eError = this->getPort(nPortIdx).flush();
    OMX_DBGT_EPILOG();
    return eError;
}

//==========================================================
// VDECComponent::getParameter
// allows to return current parameters to player
//==========================================================
OMX_ERRORTYPE VDECComponent::getParameter(OMX_INDEXTYPE       nParamIndex,
                                          OMX_PTR             pParamStruct)
{
    OMX_DBGT_PROLOG("%s getParameter 0x%08x", name(), nParamIndex);
    OMX_DBGT_PROLOG();
    switch ((int)nParamIndex)
    {
    case OMX_IndexParamVideoInit: {
        ASSERT_STRUCT_TYPE(pParamStruct, OMX_PORT_PARAM_TYPE);
        OMX_PORT_PARAM_TYPE *param = (OMX_PORT_PARAM_TYPE*)pParamStruct;
        assert(param);
        param->nPorts = OMX_ST_VIDEO_PORT_NUMBER;
        param->nStartPortNumber = 0;
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }; break;

    }

    // If the component does not support it, we fall back to generic logic.
    OMX_ERRORTYPE eError = OmxComponent::getParameter(nParamIndex,
                                                      pParamStruct);

    OMX_DBGT_EPILOG();
    return eError;
}

//==========================================================
// VDECComponent::setParameter
// allows player to set parameters associated to OMX component
//==========================================================
OMX_ERRORTYPE VDECComponent::setParameter(OMX_INDEXTYPE       nParamIndex,
                                          OMX_PTR             pParamStruct)
{
    OMX_DBGT_PROLOG("%s setParameter 0x%08x", name(), nParamIndex);
    OMX_ERRORTYPE eError = OMX_ErrorNone;

    switch ((int)nParamIndex)
    {
    case OMX_IndexParamStandardComponentRole: {
        ASSERT_STRUCT_TYPE(pParamStruct, OMX_PARAM_COMPONENTROLETYPE);
        OMX_PARAM_COMPONENTROLETYPE *param = (OMX_PARAM_COMPONENTROLETYPE*)pParamStruct;
        assert(param);
        eError = this->setRole((const char*)param->cRole);
        OMX_DBGT_EPILOG();
        return eError;
    }; break;

//    default:
//        OMX_DBGT_PDEBUG("VDECComponent::get undef parameter (0x%08x)",
//                    (unsigned int)nParamIndex);
//        break;
    }

    // If the component does not support it, we fall back to generic logic.
    eError = OmxComponent::setParameter(nParamIndex, pParamStruct);

    OMX_DBGT_EPILOG();
    return eError;
}

OMX_ERRORTYPE
VDECComponent::getConfig(OMX_INDEXTYPE nIndex,
                          OMX_PTR pConfStruct)
{
    OMX_DBGT_PROLOG();
    switch ((int)nIndex)
    {
    case OMX_StmDvbVideoDeviceInfo: {
        ASSERT_STRUCT_TYPE(pConfStruct, OMX_GetStmLinuxDvbDeviceInfoParams);
        OMX_GetStmLinuxDvbDeviceInfoParams *param = (OMX_GetStmLinuxDvbDeviceInfoParams*)pConfStruct;
        assert(param);
        param->ppDevice = (stm::IDvbDevice**)&m_dvb;
        hTunnelComp = param->hTunnelComp;
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }; break;
    case OMX_IndexConfigTimeCurrentMediaTime: {
        ASSERT_STRUCT_TYPE(pConfStruct, OMX_TIME_CONFIG_TIMESTAMPTYPE);
        OMX_TIME_CONFIG_TIMESTAMPTYPE *param;
        param = (OMX_TIME_CONFIG_TIMESTAMPTYPE *)pConfStruct;
        assert(param);
        if(m_dvb && m_dvb->isStarted()) {
             param->nTimestamp = m_dvb->getTime();
        }
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    } break;

    }

    // If the component does not support it, we fall back to generic logic.
    OMX_ERRORTYPE eError = OmxComponent::getConfig(nIndex,
                                                   pConfStruct);
    OMX_DBGT_EPILOG();
    return eError;
}

OMX_ERRORTYPE
VDECComponent::setConfig(OMX_INDEXTYPE nIndex,
                          OMX_PTR pConfStruct)
{
    OMX_DBGT_PROLOG();
    switch ((int)nIndex)
    {
    case OMX_IndexConfigTimePosition: {
        ASSERT_STRUCT_TYPE(pConfStruct, OMX_TIME_CONFIG_TIMESTAMPTYPE);
        OMX_TIME_CONFIG_TIMESTAMPTYPE *param = (OMX_TIME_CONFIG_TIMESTAMPTYPE*)pConfStruct;
        assert(param);
        // Do we need to do anything here?
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }; break;
    case OMX_IndexParamLowLatencyMode: {
        ASSERT_STRUCT_TYPE(pConfStruct, OMX_CONFIG_BOOLEANTYPE);
        OMX_CONFIG_BOOLEANTYPE *param = (OMX_CONFIG_BOOLEANTYPE*)pConfStruct;

        if(m_dvb) this->dvb().setSync(!param->bEnabled);

        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }; break;
    case OMX_IndexExtVideoUserData: {
        ASSERT_STRUCT_TYPE(pConfStruct, OMX_VIDEO_CONFIG_USERDATA);
        OMX_VIDEO_CONFIG_USERDATA *param = (OMX_VIDEO_CONFIG_USERDATA*)pConfStruct;
        this->outputPort_().setParameter(nIndex, param);
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }; break;
    case OMX_IndexExtParamFrameDecodeMode: {
        ASSERT_STRUCT_TYPE(pConfStruct, OMX_VIDEO_CONFIG_FRAME_DECODE_MODE);
        OMX_VIDEO_CONFIG_FRAME_DECODE_MODE *param = (OMX_VIDEO_CONFIG_FRAME_DECODE_MODE*)pConfStruct;

        if(this->nDecodeMode != (uint32_t)param->FrameDecodeMode) {
            this->nDecodeMode = (uint32_t)param->FrameDecodeMode;
            if(m_dvb) this->dvb().setDecodeMode(this->nDecodeMode);
        }

        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }; break;
    }

    // If the component does not support it, we fall back to generic logic.
    OMX_ERRORTYPE eError = OmxComponent::setConfig(nIndex,
                                                   pConfStruct);
    OMX_DBGT_EPILOG();
    return eError;
}

//==========================================================
// VDECComponent::getExtensionIndex
// returns extension index
//==========================================================
OMX_ERRORTYPE VDECComponent::getExtensionIndex(
        OMX_STRING          cParameterName,
        OMX_INDEXTYPE *     pIndexType)
{
    OMX_DBGT_PROLOG();
    if (strcmp(cParameterName,
               OMX_StmDvbVideoDeviceInfoExt) == 0) {
        *pIndexType = (OMX_INDEXTYPE)OMX_StmDvbVideoDeviceInfo;
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }
    else if(strcmp(cParameterName,
            RDK_OMX_CONFIG_LOW_LATENCY)==0)
    {
        *pIndexType = (OMX_INDEXTYPE)OMX_IndexParamLowLatencyMode;
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }
    else if(strcmp(cParameterName,
            RDK_OMX_CONFIG_VIDEO_USER_DATA)==0)
    {
        *pIndexType = (OMX_INDEXTYPE)OMX_IndexExtVideoUserData;
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }
    else if(strcmp(cParameterName,
            STM_OMX_PARAM_PREPARE_FOR_ADAPTIVE_PLAYBACK)==0)
    {
        *pIndexType = (OMX_INDEXTYPE)OMX_IndexExtParamPrepareForAdaptivePlayback;
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }
    else if(strcmp(cParameterName,
            RDK_OMX_CONFIG_VIDEO_FRAME_DECODE_MODE)==0)
    {
        *pIndexType = (OMX_INDEXTYPE)OMX_IndexExtParamFrameDecodeMode;
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }

    OMX_ERRORTYPE eError = OmxComponent::getExtensionIndex(cParameterName,
                                                           pIndexType);
    OMX_DBGT_EPILOG();
    return eError;
}

OMX_ERRORTYPE VDECComponent::emptyThisBuffer(OMX_BUFFERHEADERTYPE* pBufferHdr)
{
    // Reset EOS flag
    this->outputPort_().resetAfterEosFlag();
    OMXdebug_flagAfterEos = 0;

    return OmxComponent::emptyThisBuffer(pBufferHdr);
}

OMX_ERRORTYPE VDECComponent::fillThisBuffer(OMX_BUFFERHEADERTYPE* pBufferHdr)
{
    OMX_DBGT_PROLOG();
    OMX_ERRORTYPE eError = OmxComponent::fillThisBuffer(pBufferHdr);
    OMX_DBGT_EPILOG();
    return eError;
}

//==========================================================
// VDECComponent::destroy
// Allows free resources associated to the OMX component
//==========================================================
void VDECComponent::destroy()
{
    OMX_ERRORTYPE eError;
    OMX_DBGT_PROLOG();

    // stop the current playback (if still playing)
    if(m_dvb) stop();

    OmxComponent::destroy();

    // free the video device resources
    if(m_dvb) this->dvb().destroy();
    IsConfigureDone = false;
    OMX_DBGT_EPILOG();
}

//==========================================================
// VDECComponent::create
// Allows create resources associated to a new OMX component
//==========================================================
OMX_ERRORTYPE VDECComponent::create()
{
    OMX_ERRORTYPE eError = OMX_ErrorNone;

    OMX_DBGT_PROLOG();

    if ((eError = OmxComponent::create()) != OMX_ErrorNone) {
        OMX_DBGT_ERROR();
        OMX_DBGT_EPILOG();
        return eError;
    }

    // Initialize decoder roles
    this->role().add(kRoleAVC);
    this->role().add(kRoleMPEG2);
    this->role().add(kRoleMPEG4);
    this->role().add(kRoleH263);
    this->role().add(kRoleVC1);
    this->role().add(kRoleWMV);
    this->role().add(kRoleFLV1);
    this->role().add(kRoleMJPEG);
    this->role().add(kRoleVP8);
    this->role().add(kRoleVP9);
    this->role().add(kRoleTHEORA);
    this->role().add(kRoleSORENSON);
    this->role().add(kRoleCAVS);
    this->role().add(kRoleHEVC);

    // Initialize the video parameters for input port
    if ((eError = m_inputPort.create()) != OMX_ErrorNone) {
        OMX_DBGT_ERROR("Error when creating video decoder input port");
        OMX_DBGT_EPILOG();
        return eError;
    }


    // Initialize the video parameters for output port
    if ((eError = m_outputPort.create()) != OMX_ErrorNone) {
        OMX_DBGT_ERROR("Error when creating video decoder output port");
        OMX_DBGT_EPILOG();
        return eError;
    }

    // Set default role: currently AVC
    this->setRole(kRoleDefault);

    OMXdebug_flagAfterEos    = 0;
#ifdef ANDROID
    OMX_DBGT_CHECK_RETURN(! hw_get_module(GRALLOC_HARDWARE_MODULE_ID, (const hw_module_t **)&m_grallocModule),
                      OMX_ErrorInsufficientResources);

    OMX_DBGT_CHECK_RETURN(! gralloc_open((const hw_module_t *)m_grallocModule, &m_grallocDevice),
                      OMX_ErrorHardware);
#endif
    OMX_DBGT_EPILOG();
    return OMX_ErrorNone;
}

OMX_ERRORTYPE VDECComponent::setRole(const char* pNewRole)
{
    int nRoleIdx = -1;
    if (strcmp(pNewRole, kRoleDefault) == 0) {
        nRoleIdx = 0;
        OMX_DBGT_PDEBUG("Setting role to default %s", this->role().at(nRoleIdx));
    } else {
        // check if proposed role is supported
        for (unsigned int i = 0; i < this->role().size(); i++) {
            if (strcmp(pNewRole, this->role().at(i)) == 0) {
                nRoleIdx = i;
                break;
            }
        }
    }

    if (nRoleIdx == -1) {
        OMX_DBGT_ERROR("Unsupported role %s", pNewRole);
        return OMX_ErrorUnsupportedSetting;
    }

    const char* pActivRole = this->role().active(nRoleIdx);
    if (strcmp(pActivRole, kRoleAVC) == 0) {
        this->inputPort_().codingFormat(OMX_VIDEO_CodingAVC);
        this->outputPort_().colorFormat(OMX_COLOR_FormatYUV420SemiPlanar);
    } else if (strcmp(pActivRole, kRoleMPEG4) == 0) {
        this->inputPort_().codingFormat(OMX_VIDEO_CodingMPEG4);
        this->outputPort_().colorFormat(OMX_COLOR_FormatYUV420SemiPlanar);
    } else if (strcmp(pActivRole, kRoleMPEG2) == 0) {
        this->inputPort_().codingFormat(OMX_VIDEO_CodingMPEG2);
        this->outputPort_().colorFormat(OMX_COLOR_FormatYUV420SemiPlanar);
    } else if (strcmp(pActivRole, kRoleH263) == 0) {
        this->inputPort_().codingFormat(OMX_VIDEO_CodingH263);
        this->outputPort_().colorFormat(OMX_COLOR_FormatYUV420SemiPlanar);
    } else if (strcmp(pActivRole, kRoleVC1) == 0) {
        this->inputPort_().codingFormat(OMX_VIDEO_CodingVC1);
        this->outputPort_().colorFormat(OMX_COLOR_FormatYUV420SemiPlanar);
    } else if (strcmp(pActivRole, kRoleWMV) == 0) {
        this->inputPort_().codingFormat(OMX_VIDEO_CodingWMV);
        this->outputPort_().colorFormat(OMX_COLOR_FormatYUV420SemiPlanar);
    } else if ((strcmp(pActivRole, kRoleFLV1) == 0) ||
               (strcmp(pActivRole, kRoleSORENSON) == 0)) {
        this->inputPort_().codingFormat(OMX_VIDEO_CodingFLV1);
        this->outputPort_().colorFormat(OMX_COLOR_FormatYUV420SemiPlanar);
    } else if (strcmp(pActivRole, kRoleMJPEG) == 0) {
        this->inputPort_().codingFormat(OMX_VIDEO_CodingMJPEG);
        this->outputPort_().colorFormat(OMX_COLOR_FormatYUV420SemiPlanar);
    } else if (strcmp(pActivRole, kRoleVP8) == 0) {
        this->inputPort_().codingFormat(OMX_VIDEO_CodingVP8);
        this->outputPort_().colorFormat(OMX_COLOR_FormatYUV420SemiPlanar);
    } else if (strcmp(pActivRole, kRoleHEVC) == 0) {
       this->inputPort_().codingFormat(OMX_VIDEO_CodingHEVC);
       this->outputPort_().colorFormat(OMX_COLOR_FormatYUV420SemiPlanar);
    } else if (strcmp(pActivRole, kRoleVP9) == 0) {
        this->inputPort_().codingFormat(OMX_VIDEO_CodingVP9);
        this->outputPort_().colorFormat(OMX_COLOR_FormatYUV420SemiPlanar);
    } else if (strcmp(pActivRole, kRoleTHEORA) == 0) {
        this->inputPort_().codingFormat((OMX_VIDEO_CODINGTYPE)OMX_VIDEO_CodingTHEORA);
        this->outputPort_().colorFormat(OMX_COLOR_FormatYUV420SemiPlanar);
    } else if (strcmp(pActivRole, kRoleCAVS) == 0) {
        this->inputPort_().codingFormat((OMX_VIDEO_CODINGTYPE)OMX_VIDEO_CodingCAVS);
        this->outputPort_().colorFormat(OMX_COLOR_FormatYUV420SemiPlanar);
    } else {
        return OMX_ErrorUnsupportedSetting;
    }
    return OMX_ErrorNone;
}

//=================================================================================
// VDECComponent::getWidth
// Returns Width of Video set at Input Port
//=================================================================================
unsigned int VDECComponent::getWidth(unsigned int nIdx) const
{
    OMX_DBGT_PROLOG();
    unsigned int width = this->getPort(nIdx).definition().frameWidth();
    OMX_DBGT_EPILOG("Width at video input port : %u",width);
    return width;
}

//=================================================================================
// VDECComponent::getHeight
// Returns Width of Video set at Input Port
//=================================================================================
unsigned int VDECComponent::getHeight(unsigned int nIdx) const
{
    OMX_DBGT_PROLOG();
    unsigned int height = this->getPort(nIdx).definition().frameHeight();
    OMX_DBGT_EPILOG("Height at video input port : %u",height);
    return height;
    }

//=================================================================================
// VDECComponent::getCompressionFormat
// Returns Video Compression format set at Input Port
//=================================================================================
OMX_VIDEO_CODINGTYPE VDECComponent::getCompressionFormat(unsigned int nIdx) const
{
    OMX_DBGT_PROLOG();
    OMX_VIDEO_CODINGTYPE compressionFormat = this->getPort(nIdx).definition().compressionFormat();
    OMX_DBGT_EPILOG("Video Format set at input port : %u",(unsigned int)compressionFormat);
    return compressionFormat;
}

const VidDecPort& VDECComponent::getPort(unsigned int nIdx) const
{
    assert(nIdx < OMX_ST_VIDEO_PORT_NUMBER);
    if (nIdx == 0)
        return m_inputPort;
    else
        return m_outputPort;
}

VidDecPort& VDECComponent::getPort(unsigned int nIdx)
{
    assert(nIdx < OMX_ST_VIDEO_PORT_NUMBER);
    if (nIdx == 0)
        return m_inputPort;
    else
        return m_outputPort;
}

//==========================================================
// VDECComponent::deConfigure
//  decoder configuration cleared
//==========================================================
OMX_ERRORTYPE VDECComponent::deConfigure()
{
    OMX_DBGT_PROLOG();

    // stop the current playback (if still playing)
    if(m_dvb) stop();

    if (m_pPesAdapter) {
        delete m_pPesAdapter;
        m_pPesAdapter = 0;
    }
    IsConfigureDone = false;

    OMX_DBGT_EPILOG();
    return OMX_ErrorNone;
}
//==========================================================
// VDECComponent::configure
//  decoder configuration according to codec
//==========================================================
OMX_ERRORTYPE VDECComponent::configure()
{
    OMX_DBGT_PROLOG();

    OMX_DBGT_PINFO("%s Stream %s(%d) %d Hz (%d bytes * %d) (VDECComponent::configure)",
               name(),
               this->inputPort().definition().contentType(),
               this->inputPort().definition().compressionFormat(),
               this->inputPort().definition().frameRate(),
               this->inputPort().definition().bufferSize(),
               this->inputPort().definition().bufferCountActual());
    OMX_DBGT_PINFO("%s Image %s(%d) (%d * %d => %d bytes * %d) (VDECComponent::configure)",
               name(),
               this->outputPort().definition().contentType(),
               this->outputPort().definition().colorFormat(),
               this->outputPort().definition().frameWidth(),
               this->outputPort().definition().frameHeight(),
               this->outputPort().definition().bufferSize(),
               this->outputPort().definition().bufferCountActual());
#ifndef ANDROID
    if(!this->outputPort_().isTunneled())
    {
        OMX_DBGT_WARNING("We do not support NON tunneled mode");
    }

    if(m_dvb && this->dvb().isStarted())
    {
        OMX_DBGT_PINFO("Returning from video configure");
        return OMX_ErrorNone;
    }

    OMX_VIDEO_CODINGTYPE cFmt = this->inputPort().definition().compressionFormat();

    int DvbColorFormat = 0;
    int PesColorFormat = 0;
    switch((unsigned int)cFmt)
    {
    case OMX_VIDEO_CodingAVC:
        DvbColorFormat = VIDEO_ENCODING_H264;
        PesColorFormat = VIDEO_ENCODING_H264;
        break;
    case OMX_VIDEO_CodingMPEG4:
        DvbColorFormat =  VIDEO_ENCODING_MPEG4P2;
        PesColorFormat = VIDEO_ENCODING_MPEG4P2;
        break;
    case OMX_VIDEO_CodingH263:
        DvbColorFormat =  VIDEO_ENCODING_H263;
        PesColorFormat = VIDEO_ENCODING_H263;
        break;
    case OMX_VIDEO_CodingMPEG2:
        DvbColorFormat = VIDEO_ENCODING_MPEG2;
        PesColorFormat = VIDEO_ENCODING_MPEG2;
        break;
    case OMX_VIDEO_CodingVC1:
        DvbColorFormat = VIDEO_ENCODING_VC1;
        PesColorFormat = VIDEO_ENCODING_VC1;
        break;
    case OMX_VIDEO_CodingWMV:
        DvbColorFormat = VIDEO_ENCODING_WMV;
        PesColorFormat = VIDEO_ENCODING_WMV;
        break;
    case OMX_VIDEO_CodingSorenson:
    case OMX_VIDEO_CodingFLV1:
        DvbColorFormat = VIDEO_ENCODING_FLV1;
        PesColorFormat = VIDEO_ENCODING_FLV1;
        break;
    case OMX_VIDEO_CodingMJPEG:
        DvbColorFormat = VIDEO_ENCODING_MJPEG;
        PesColorFormat = VIDEO_ENCODING_MJPEG;
        break;
    case OMX_VIDEO_CodingVP8:
    case OMX_VIDEO_CodingVPX:
        DvbColorFormat = VIDEO_ENCODING_VP8;
        PesColorFormat = VIDEO_ENCODING_VP8;
        break;
    case OMX_VIDEO_CodingVP9:
        DvbColorFormat = VIDEO_ENCODING_VP9;
        PesColorFormat = VIDEO_ENCODING_VP9;
        break;
    case OMX_VIDEO_CodingTHEORA:
        DvbColorFormat = VIDEO_ENCODING_THEORA;
        PesColorFormat = VIDEO_ENCODING_THEORA;
        break;

    case OMX_VIDEO_CodingCAVS:
        DvbColorFormat = VIDEO_ENCODING_AVS;
        PesColorFormat = VIDEO_ENCODING_AVS;
        break;

    case OMX_VIDEO_CodingHEVC:
        DvbColorFormat = VIDEO_ENCODING_HEVC;
        PesColorFormat = VIDEO_ENCODING_HEVC;
        break;

    default:
        OMX_DBGT_ERROR("Unsupported Video format : 0x%x",(unsigned int)cFmt);
        OMX_DBGT_EPILOG();
        return OMX_ErrorUnsupportedSetting;
    }
#else
    OMX_VIDEO_CODINGTYPE cFmt = this->inputPort().definition().compressionFormat();
    int DvbColorFormat = 0;
    int PesColorFormat = 0;
    switch((unsigned int)cFmt)
    {
    case OMX_VIDEO_CodingAVC:
        DvbColorFormat = (this->outputPort_().isTunneled())? VIDEO_ENCODING_H264 : (int)OMXSE_ENCODING_H264;
        PesColorFormat = VIDEO_ENCODING_H264;
        break;
    case OMX_VIDEO_CodingMPEG4:
        DvbColorFormat =  (this->outputPort_().isTunneled())? VIDEO_ENCODING_MPEG4P2 : (int)OMXSE_ENCODING_MPEG4P2;
        PesColorFormat = VIDEO_ENCODING_MPEG4P2;
        break;
    case OMX_VIDEO_CodingH263:
        DvbColorFormat =  (this->outputPort_().isTunneled())? VIDEO_ENCODING_H263 : (int)OMXSE_ENCODING_H263;
        PesColorFormat = VIDEO_ENCODING_H263;
        break;
    case OMX_VIDEO_CodingMPEG2:
        DvbColorFormat =  (this->outputPort_().isTunneled())? VIDEO_ENCODING_MPEG2 : (int)OMXSE_ENCODING_MPEG2;
        PesColorFormat = VIDEO_ENCODING_MPEG2;
        break;
    case OMX_VIDEO_CodingVC1:
        DvbColorFormat =  (this->outputPort_().isTunneled())? VIDEO_ENCODING_VC1 : (int)OMXSE_ENCODING_VC1;
        PesColorFormat = VIDEO_ENCODING_VC1;
        break;
    case OMX_VIDEO_CodingWMV:
        DvbColorFormat =  (this->outputPort_().isTunneled())? VIDEO_ENCODING_WMV : (int)OMXSE_ENCODING_WMV;
        PesColorFormat = VIDEO_ENCODING_WMV;
        break;
    case OMX_VIDEO_CodingSorenson:
    case OMX_VIDEO_CodingFLV1:
        DvbColorFormat =  (this->outputPort_().isTunneled())? VIDEO_ENCODING_FLV1 : (int)OMXSE_ENCODING_FLV1;
        PesColorFormat = VIDEO_ENCODING_FLV1;
        break;
    case OMX_VIDEO_CodingMJPEG:
        DvbColorFormat =  (this->outputPort_().isTunneled())? VIDEO_ENCODING_MJPEG : (int)OMXSE_ENCODING_MJPEG;
        PesColorFormat = VIDEO_ENCODING_MJPEG;
        break;
    case OMX_VIDEO_CodingVP8:
    case OMX_VIDEO_CodingVPX:
        DvbColorFormat =  (this->outputPort_().isTunneled())? VIDEO_ENCODING_VP8 : (int)OMXSE_ENCODING_VP8;
        PesColorFormat = VIDEO_ENCODING_VP8;
        break;
    case OMX_VIDEO_CodingVP9:
        DvbColorFormat =  (this->outputPort_().isTunneled())? VIDEO_ENCODING_VP9 : (int)OMXSE_ENCODING_VP9;
        PesColorFormat = VIDEO_ENCODING_VP9;
        break;
    case OMX_VIDEO_CodingTHEORA:
        DvbColorFormat =  (this->outputPort_().isTunneled())? VIDEO_ENCODING_THEORA : (int)OMXSE_ENCODING_THEORA;
        PesColorFormat = VIDEO_ENCODING_THEORA;
        break;

    case OMX_VIDEO_CodingCAVS:
        DvbColorFormat =  (this->outputPort_().isTunneled())? VIDEO_ENCODING_AVS : (int)OMXSE_ENCODING_AVS;
        PesColorFormat = VIDEO_ENCODING_AVS;
        break;

    case OMX_VIDEO_CodingHEVC:
        DvbColorFormat =  (this->outputPort_().isTunneled())? VIDEO_ENCODING_HEVC : (int)OMXSE_ENCODING_HEVC;
        PesColorFormat = VIDEO_ENCODING_HEVC;
        break;

    default:
        OMX_DBGT_ERROR("Unsupported Video format : 0x%x",(unsigned int)cFmt);
        OMX_DBGT_EPILOG();
        return OMX_ErrorUnsupportedSetting;
    }
#endif

    if(m_dvb)
    {
       dvb().destroy();
       delete m_dvb;
       m_dvb = 0;
    }
    assert(m_dvb == 0);

#ifndef ANDROID
        if(!m_dvb) {
            m_dvb = new DVBVideo_MOO(DVB_DEVICE_VIDEO_MOO, DvbColorFormat, this->name());
        }
#else

    if(this->outputPort_().isTunneled())
        m_dvb = new DVBVideo_MOO(DVB_DEVICE_VIDEO_MOO, DvbColorFormat, this->name());
    else
        m_dvb = new DVBVideo_SF(DVB_DEVICE_VIDEO_SF, DvbColorFormat,
                          this->inputPort().definition().frameWidth(),
                          this->inputPort().definition().frameHeight(),
                          !this->outputPort().nativeWindowsEnabled(),
                          this->name());
#endif
    if (!m_dvb) {
        OMX_DBGT_ERROR("Can not allocate new Video device");
        OMX_DBGT_EPILOG();
        return OMX_ErrorInsufficientResources;
    }

    this->dvb().setEncoding(DvbColorFormat);
    this->dvb().setResolution(this->inputPort().definition().frameWidth(),
                              this->inputPort().definition().frameHeight());
    this->dvb().set10Bit(this->inputPort().is10BitEnabled());
    this->dvb().setDecodeMode(this->nDecodeMode);

    if (m_dvb->create()) {
        OMX_DBGT_ERROR("Cannot create new Video device");
        OMX_DBGT_EPILOG();
        return OMX_ErrorHardware;
    }

    m_pPesAdapter = VideoPesAdapter::create(PesColorFormat, *this);
    if (!m_pPesAdapter) {
        OMX_DBGT_ERROR("OMX_ErrorInsufficientResources");
        OMX_DBGT_EPILOG();
        return OMX_ErrorInsufficientResources;
    }

    if(this->outputPort_().isTunneled())
    {
        // We really do not need tolock mutex
        // as O/P is not allocated
        IsConfigureDone = true;
    }
    else
    {
        m_mutexCmd.lock();
        IsConfigureDone = true;
        m_condConfig.signal();
        m_mutexCmd.release();
    }

    OMX_DBGT_EPILOG();
    return OMX_ErrorNone;
}

//==========================================================
// VDECComponent::start
//  start decoding
//==========================================================
OMX_ERRORTYPE VDECComponent::start()
{
    OMX_DBGT_PROLOG();

    if (dvb().isStarted()) {
        OMX_DBGT_ERROR("::dvb.Start %s failed (already started)", name());
        OMX_DBGT_EPILOG();
        return OMX_ErrorIncorrectStateTransition;
    }

    if(dvb().start() != 0)
    {
        OMX_DBGT_ERROR("::dvb.Start %s failed (%s)", name(), strerror (errno));
        OMX_DBGT_EPILOG();
        return OMX_ErrorInsufficientResources;
    }

    // Create the port threads
    if (this->inputPort_().start() != 0) {
        return OMX_ErrorInsufficientResources;
    }
    if (this->outputPort_().start() != 0) {
        return OMX_ErrorInsufficientResources;
    }

    OMX_DBGT_EPILOG();

    return OMX_ErrorNone;
}

//==========================================================
// VDECComponent::stop
//  stop decoding
//==========================================================
void VDECComponent::stop()
{
    OMX_DBGT_PROLOG();

    if (!this->dvb().isStarted()) {
        // Nothing to do
        OMX_DBGT_EPILOG();
        return;
    }

    int result = this->dvb().clear();
    if (result) {
       OMX_DBGT_ERROR("%s::clear failed", name());
       OMX_DBGT_EPILOG();
       return;
    }

    if (this->dvb().stop() != 0)
        OMX_DBGT_ERROR("::dvb.Stop %s failed (%s)", name(), strerror (errno));

    // cleanup and delete video ports
    m_inputPort.destroy();
    m_outputPort.destroy();

    OMX_DBGT_EPILOG();
}


//==============================================================================
// VDECComponent::execute
//  OMX command already processed by CmdThread: nothing to do
//==============================================================================
void VDECComponent::execute()
{}

} // eof namespace stm

//==========================================================
// OMX_ComponentInit
// "C" interface to initialize a new OMX component: creation of the associated object
//==========================================================
extern "C" OMX_ERRORTYPE OMX_ComponentInit (OMX_HANDLETYPE hComponent)
{
    return stm::OmxComponent::ComponentInit(new stm::VDECComponent(hComponent));
}

