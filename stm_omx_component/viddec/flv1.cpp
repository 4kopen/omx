/*
 * Copyright 2011 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *
 * File   ::    flv1.c
 * Author ::    Jean-Christophe TROTIN (jean-christophe.trotin@st.com)
 *
 *
 */

#include "OMX_debug.h"
#include "VideoPesAdapter.hpp"
#include "VideoDecoder.hpp"

#include "pes.hpp"

#define DBGT_PREFIX "FLV1 "
#define DBGT_LAYER  1
#include <linux/dbgt.h>

#define PTS_CORRECTION(x) (((x) * 90000)/1000000)

//#define FLV1_DEBUG

namespace stm {

Flv1PesAdapter::Flv1PesAdapter(const VDECComponent& dec)
    :VideoPesAdapter(dec)
{}

Flv1PesAdapter::~Flv1PesAdapter()
{}

int Flv1PesAdapter::emitInitialHeader() const
{
    return 0;
}

int Flv1PesAdapter::emitCodecConfig(uint8_t* pBuffer,
                                    unsigned int nFilledLen) const
{
    int err = 0;
    int PesHeaderLength = 0;

    OMX_DBGT_PROLOG ("pBuf=0x%p, len=%lu", pBuffer, (unsigned long)nFilledLen);

    // Write PES header
    memset (PesHeader, 0, PES_MAX_HEADER_SIZE);
    PesHeaderLength = InsertPesHeader (PesHeader, nFilledLen, H263_VIDEO_PES_START_CODE, INVALID_PTS_VALUE, 0);
    err = this->viddec().dvb().write (PesHeader, PesHeaderLength);
    if(err)
    {
       OMX_DBGT_ERROR ("%s::FLV1EmitCodecConfig->dvb().write failed", name());
       goto error;
    }

    // Write data
    err = this->viddec().dvb().write (pBuffer, nFilledLen);
    if(err)
    {
       OMX_DBGT_ERROR ("%s::FLV1EmitCodecConfig->dvb().write failed", name());
       goto error;
    }

#ifdef FLV1_DEBUG
    {
        int i = 0;
        char* LogPtr = (char *)malloc ((PesHeaderLength + nFilledLen) * 3 + 1);

        OMX_DBGT_PINFO("%s FLV1 codec specific data length %lu (VDECComponent::FLV1EmitCodecConfig)", name(), (unsigned long)PesHeaderLength + (unsigned long)nFilledLen);
        for(i = 0; i < PesHeaderLength + nFilledLen; i++)
        {
            sprintf (LogPtr+3*i, "%.2x ", PesHeader[i]);
        }
        OMX_DBGT_PINFO("%s %s (VDECComponent::FLV1EmitCodecConfig)", name(), LogPtr);
        free (LogPtr);
    }
#endif

error:
    OMX_DBGT_EPILOG ();
    return err;
}

int Flv1PesAdapter::emitFrame(uint8_t* pBuffer, unsigned int nFilledLen,
                              uint64_t nTimeStamp) const
{
    int err = 0;
    int PesHeaderLength = 0;
    int PesLength = 0;
    int PrivateHeaderLength = 0;
    uint64_t nPTS = PTS_CORRECTION(nTimeStamp);

    OMX_DBGT_PROLOG ("pBuf=0x%p, len=%lu, nTimeStamp=%llu", pBuffer, (unsigned long)nFilledLen, nTimeStamp);

    if(nPTS > INVALID_PTS_VALUE)
    {
        //We will anyway overflow. So send no PTS
        nPTS = INVALID_PTS_VALUE;
    }

    // Write Pes header
    memset (PesHeader, 0, PES_MAX_HEADER_SIZE);
    PesHeaderLength = InsertPesHeader (PesHeader, nFilledLen, H263_VIDEO_PES_START_CODE, nPTS, 0);

    // Update PES length
    PrivateHeaderLength = InsertVideoPrivateDataHeaderForAudio(&PesHeader[PesHeaderLength], nFilledLen);
    PesLength = PesHeader[PES_LENGTH_BYTE_0] + (PesHeader[PES_LENGTH_BYTE_1] << 8) + PrivateHeaderLength;
    PesHeader[PES_LENGTH_BYTE_0] = PesLength & 0xff;
    PesHeader[PES_LENGTH_BYTE_1] = (PesLength >> 8) & 0xff;
    PesHeader[PES_HEADER_DATA_LENGTH_BYTE] += PrivateHeaderLength;
    PesHeader[PES_FLAGS_BYTE] |= PES_EXTENSION_DATA_PRESENT;
    PesHeaderLength += PrivateHeaderLength;

    err = this->viddec().dvb().write (PesHeader, PesHeaderLength);
    if (err)
    {
       OMX_DBGT_ERROR ("%s::FLV1EmitFrame->dvb().write failed", name());
       goto error;
    }

#ifdef FLV1_DEBUG
    {
        int i = 0;
        char* LogPtr = (char *)malloc (PesHeaderLength * 3);

        OMX_DBGT_PINFO("%s FLV1 data header length %lu (VDECComponent::FLV1EmitFrame)", name(), (unsigned long)PesHeaderLength + 1);
        for(i = 0; i < PesHeaderLength; i++)
        {
            sprintf (LogPtr+3*i, "%.2x ", PesHeader[i]);
        }
        OMX_DBGT_PINFO("%s %s (VDECComponent::FLV1EmitFrame)", name(), LogPtr);
        free (LogPtr);
    }
#endif

    // Write data
    if (nFilledLen > 0)
        err = this->viddec().dvb().write (pBuffer, nFilledLen);
    if (err)
    {
       OMX_DBGT_ERROR ("%s::FLV1EmitFrame->dvb().write failed", name());
       goto error;
    }

#ifdef FLV1_DEBUG
    {
        int i = 0;
        char* LogPtr = (char *)malloc (40 * 3 + 1);

        OMX_DBGT_PINFO("%s FLV1 data length %lu (VDECComponent::FLV1EmitFrame)", name(),  (unsigned long)nFilledLen);
        for(i = 0; i < 40; i++)
        {
            sprintf (LogPtr+3*i, "%.2x ", pBuffer[i]);
        }
        OMX_DBGT_PINFO("%s %s (VDECComponent::FLV1EmitFrame)", name(), LogPtr);
        free (LogPtr);
    }
#endif

error:
    OMX_DBGT_EPILOG ();
    return err;
}

} // eof namespace stm
