/*
 * Copyright 2011 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *
 * File   ::    h263.cpp
 * Author ::    Jean-Philippe FASSINO (jean-philippe.fassino@st.com)
 *
 *
 */

#include "OMX_debug.h"
#include "VideoPesAdapter.hpp"
#include "VideoDecoder.hpp"

#include "pes.hpp"

#define DBGT_PREFIX "H263"
#define DBGT_LAYER 1
#include <linux/dbgt.h>

namespace stm {

H263PesAdapter::H263PesAdapter(const VDECComponent& dec)
    :VideoPesAdapter(dec)
{}

H263PesAdapter::~H263PesAdapter()
{}

int H263PesAdapter::emitInitialHeader() const
{
    return 0;
}

int H263PesAdapter::emitCodecConfig(uint8_t* pBuffer,
                                    unsigned int nFilledLen) const
{
    return 0;
}

int H263PesAdapter::emitFrame(uint8_t* pBuffer, unsigned int nFilledLen,
                              uint64_t nTimeStamp) const
{
    return 0;
}

} // eof namespace stm
