/*
 * Copyright 2008-2009 Wind River Systems
 *
 * Copyright 2013 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file   AudioRenderer.cpp
 * @author STMicroelectronics
 */
#include <errno.h>
#include <stdarg.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <dlfcn.h>

#include "AudioRenderer.hpp"
#include <OMX_debug.h>

#if HAVE_ASSERT_H
#include <assert.h>
#endif

#if HAVE_DBGTASSERT_H
// Re-direct assert on DBGT_ASSERT macro
#undef assert
#define assert(expr)  OMX_DBGT_ASSERT(expr)
#endif

#define DBGT_DECLARE_AUTOVAR
#define DBGT_PREFIX "OMX "
#define DBGT_LAYER 0
#include <linux/dbgt.h>

namespace stm {

#define MixerCardNumber 2
#define MixerCardName "hw:MIXER0"
#define VolumeCtrlName "Primary Playback Volume"
#define ChannelIndexAll -1
#define ChannelIndexLeft 0
#define ChannelIndexRight 1
#define Q3_13_0DB (1<<13)
#define Q3_13_MUTED 0

const char AudioRenderer::kRole[] = "audio_renderer.pcm";

AudioRenderer::AudioRenderer(OMX_HANDLETYPE hComponent)
    :OmxComponent("OMX.STM.Audio.Renderer", hComponent, AUDIO_RENDERER_PORTNB),
     m_inputPort(AUDIO_RENDERER_INPUT, OMX_DirInput, *this),
     m_clockPort(CLOCK_RENDERER_PORT, OMX_DirInput, *this)
{
    INIT_DVB_DEVICE_INFO(&m_dvbInfo);
    DBGT_TRACE_INIT(adec); //expands debug.adec.trace
    int err = snd_ctl_open(&mHandle, MixerCardName, MixerCardNumber);
    if (err<0)  {
        mHandle = 0;
        OMX_DBGT_ERROR("impossible to open alsa card");
    } else {
        set_alsa_ctrl(VolumeCtrlName, Q3_13_0DB, ChannelIndexAll);
    }
}

OMX_ERRORTYPE AudioRenderer::set_alsa_ctrl(const char *pname, unsigned int value, int index)
{
    if (!mHandle) {
        OMX_DBGT_ERROR("no access to alsa card");
        return OMX_ErrorUndefined;
    }

    snd_ctl_elem_id_t *id;
    snd_ctl_elem_info_t *info;

    snd_ctl_elem_id_alloca(&id);
    snd_ctl_elem_info_alloca(&info);

    snd_ctl_elem_id_set_interface(id, SND_CTL_ELEM_IFACE_MIXER);
    snd_ctl_elem_id_set_name(id, pname);
    snd_ctl_elem_info_set_id(info, id);

    int ret = snd_ctl_elem_info(mHandle, info);
    if (ret < 0) {
        OMX_DBGT_ERROR("Control '%s' cannot get element info: %d", pname, ret);
        return OMX_ErrorUndefined;
    }

    int count = snd_ctl_elem_info_get_count(info);
    if (index >= count) {
        OMX_DBGT_ERROR("Control '%s' index is out of range (%d >= %d)", pname, index, count);
        return OMX_ErrorBadParameter;
    }

    if (index == ChannelIndexAll)
        index = 0; // Range over all of them
    else
        count = index + 1; // Just do the one specified

    snd_ctl_elem_type_t type = snd_ctl_elem_info_get_type(info);

    snd_ctl_elem_value_t *control;
    snd_ctl_elem_value_alloca(&control);

    snd_ctl_elem_info_get_id(info, id);
    snd_ctl_elem_value_set_id(control, id);

    for (int i = index; i < count; i++)
        switch (type) {
            case SND_CTL_ELEM_TYPE_BOOLEAN:
                snd_ctl_elem_value_set_boolean(control, i, value);
                break;
            case SND_CTL_ELEM_TYPE_INTEGER:
                snd_ctl_elem_value_set_integer(control, i, value);
                break;
            case SND_CTL_ELEM_TYPE_INTEGER64:
                snd_ctl_elem_value_set_integer64(control, i, value);
                break;
            case SND_CTL_ELEM_TYPE_ENUMERATED:
                snd_ctl_elem_value_set_enumerated(control, i, value);
                break;
            case SND_CTL_ELEM_TYPE_BYTES:
                snd_ctl_elem_value_set_byte(control, i, value);
                break;
            default:
                break;
        }

    ret = snd_ctl_elem_write(mHandle, control);
    return (ret < 0) ? OMX_ErrorUndefined : OMX_ErrorNone;
}

AudioRenderer::~AudioRenderer()
{
    if (mHandle) snd_ctl_close(mHandle);
}

OMX_ERRORTYPE
AudioRenderer::getParameter(OMX_INDEXTYPE       nParamIndex,
                            OMX_PTR             pParamStruct)
{
    OMX_DBGT_PROLOG();
    switch ((int)nParamIndex)
    {
    case OMX_IndexParamAudioInit: {
        ASSERT_STRUCT_TYPE(pParamStruct, OMX_PORT_PARAM_TYPE);
        OMX_PORT_PARAM_TYPE *param = (OMX_PORT_PARAM_TYPE*)pParamStruct;
        assert(param);
        param->nPorts = AUDIO_RENDERER_PORTNB;
        param->nStartPortNumber = AUDIO_RENDERER_INPUT;
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }; break;

    case OMX_IndexParamOtherInit: {
        ASSERT_STRUCT_TYPE(pParamStruct, OMX_PORT_PARAM_TYPE);
        OMX_PORT_PARAM_TYPE *param = (OMX_PORT_PARAM_TYPE*)pParamStruct;
        assert(param);
        param->nPorts = AUDIO_RENDERER_PORTNB;
        param->nStartPortNumber = CLOCK_RENDERER_PORT;
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }; break;

    }

    // If the component does not support it, we fall back to generic logic.
    OMX_ERRORTYPE eError = OmxComponent::getParameter(nParamIndex,
                                                      pParamStruct);

    OMX_DBGT_EPILOG();
    return eError;
}

OMX_ERRORTYPE
AudioRenderer::setParameter(OMX_INDEXTYPE       nParamIndex,
                            OMX_PTR             pParamStruct)
{
    OMX_DBGT_PROLOG();
    switch ((int)nParamIndex)
    {
    case OMX_IndexParamStandardComponentRole: {
        ASSERT_STRUCT_TYPE(pParamStruct, OMX_PARAM_COMPONENTROLETYPE);
        OMX_PARAM_COMPONENTROLETYPE *param = (OMX_PARAM_COMPONENTROLETYPE*)pParamStruct;
        assert(param);
        if (strcmp((char*)param->cRole, "default") == 0) {
            OMX_DBGT_EPILOG("Setting role to default");
            return OMX_ErrorNone;
        }
        if (strcmp((char*)param->cRole, kRole)) {
            OMX_DBGT_ERROR("Unsupported role %s", param->cRole);
            return OMX_ErrorUnsupportedSetting;
        }
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }; break;

    }

    // If the component does not support it, we fall back to generic logic.
    OMX_ERRORTYPE eError = OmxComponent::setParameter(nParamIndex,
                                                      pParamStruct);
    OMX_DBGT_EPILOG();
    return eError;
}

OMX_ERRORTYPE AudioRenderer::getConfig(OMX_INDEXTYPE nIndex,
                                       OMX_PTR pConfigStruct)
{
    OMX_DBGT_PROLOG();
    OMX_ERRORTYPE eError = OMX_ErrorNone;
    switch ((int)nIndex)
    {
    case OMX_IndexConfigAudioChannelVolume: {
        ASSERT_STRUCT_TYPE(pConfigStruct, OMX_AUDIO_CONFIG_CHANNELVOLUMETYPE);
        OMX_AUDIO_CONFIG_CHANNELVOLUMETYPE* configVolume;
        configVolume = (OMX_AUDIO_CONFIG_CHANNELVOLUMETYPE*)pConfigStruct;
        // check linear format
        if (configVolume->bLinear != OMX_TRUE) {
            OMX_DBGT_ERROR("only linear volume is supported");
            OMX_DBGT_EPILOG();
            return OMX_ErrorNotImplemented;
        }
        configVolume->sVolume.nMin = 0;
        configVolume->sVolume.nMax = 100;

        OMX_DBGT_EPILOG();
        return eError;
    } break;
    case OMX_IndexConfigAudioVolume: {
        ASSERT_STRUCT_TYPE(pConfigStruct, OMX_AUDIO_CONFIG_CHANNELVOLUMETYPE);
        OMX_AUDIO_CONFIG_VOLUMETYPE* configVolume;
        configVolume = (OMX_AUDIO_CONFIG_VOLUMETYPE*)pConfigStruct;
        // check linear format
        if (configVolume->bLinear != OMX_TRUE) {
            OMX_DBGT_ERROR("only linear volume is supported");
            OMX_DBGT_EPILOG();
            return OMX_ErrorNotImplemented;
        }

        configVolume->sVolume.nMin = 0;
        configVolume->sVolume.nMax = 100;
        OMX_DBGT_EPILOG();
        return eError;
    } break;
    }
    // If the component does not support it, we fall back to generic logic.
    eError = OmxComponent::setConfig(nIndex,
                                     pConfigStruct);

    OMX_DBGT_EPILOG();
    return eError;

}

OMX_ERRORTYPE AudioRenderer::setConfig(OMX_INDEXTYPE nIndex,
                                       OMX_PTR pConfigStruct)
{
    OMX_DBGT_PROLOG();
    OMX_ERRORTYPE eError = OMX_ErrorNone;

    switch ((int)nIndex)
    {
    case OMX_IndexConfigAudioChannelVolume: {
        ASSERT_STRUCT_TYPE(pConfigStruct, OMX_AUDIO_CONFIG_CHANNELVOLUMETYPE);
        OMX_AUDIO_CONFIG_CHANNELVOLUMETYPE* configVolume;
        configVolume = (OMX_AUDIO_CONFIG_CHANNELVOLUMETYPE*)pConfigStruct;
        int channel = ChannelIndexAll;

        // check linear format
        if (configVolume->bLinear != OMX_TRUE) {
            OMX_DBGT_ERROR("only linear volume is supported");
            OMX_DBGT_EPILOG();
            return OMX_ErrorNotImplemented;
        }

        // check channel
        switch (configVolume->nChannel)
        {
        case OMX_AUDIO_ChannelLF:
        case OMX_AUDIO_ChannelLHS:
            channel = ChannelIndexLeft;
        break;
        case OMX_AUDIO_ChannelRF:
        case OMX_AUDIO_ChannelRHS:
            channel = ChannelIndexRight;
        break;
        }

        // check value before to apply volume
        if (configVolume->sVolume.nValue<=0) {
            eError = set_alsa_ctrl(VolumeCtrlName, Q3_13_MUTED, channel);
        } else if (configVolume->sVolume.nValue>=100) {
            eError = set_alsa_ctrl(VolumeCtrlName, Q3_13_0DB, channel);
        } else {
            eError = set_alsa_ctrl(VolumeCtrlName, (configVolume->sVolume.nValue*Q3_13_0DB+50)/100, channel);
        }

        OMX_DBGT_EPILOG();
        return eError;
    }; break;

    case OMX_IndexConfigAudioVolume: {
        ASSERT_STRUCT_TYPE(pConfigStruct, OMX_AUDIO_CONFIG_VOLUMETYPE);
        OMX_AUDIO_CONFIG_VOLUMETYPE* configVolume;
        configVolume = (OMX_AUDIO_CONFIG_VOLUMETYPE*)pConfigStruct;
        int channel = ChannelIndexAll;

        // check linear format
        if (configVolume->bLinear != OMX_TRUE) {
            OMX_DBGT_ERROR("only linear volume is supported");
            OMX_DBGT_EPILOG();
            return OMX_ErrorNotImplemented;
        }

        // check value before to apply volume
        if (configVolume->sVolume.nValue<=0) {
            eError = set_alsa_ctrl(VolumeCtrlName, Q3_13_MUTED, channel);
        } else if (configVolume->sVolume.nValue>=100) {
            eError = set_alsa_ctrl(VolumeCtrlName, Q3_13_0DB, channel);
        } else {
            eError = set_alsa_ctrl(VolumeCtrlName, (configVolume->sVolume.nValue*Q3_13_0DB+50)/100, channel);
        }

        OMX_DBGT_EPILOG();
        return eError;
    }; break;
    }

    // If the component does not support it, we fall back to generic logic.
    eError = OmxComponent::setConfig(nIndex,
                                     pConfigStruct);

    OMX_DBGT_EPILOG();
    return eError;
}


OMX_ERRORTYPE
AudioRenderer::getExtensionIndex(OMX_STRING          cParameterName,
                                 OMX_INDEXTYPE *     pIndexType)
{
    OMX_DBGT_PROLOG();
    if (strcmp(cParameterName,
               OMX_StmDvbAudioDeviceInfoExt) == 0) {
        *pIndexType = (OMX_INDEXTYPE)OMX_StmDvbAudioDeviceInfo;
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }

    OMX_ERRORTYPE eError = OmxComponent::getExtensionIndex(cParameterName,
                                                           pIndexType);
    OMX_DBGT_EPILOG();
    return eError;
}

OMX_ERRORTYPE 
AudioRenderer::componentTunnelRequest(OMX_U32 nPort,
                                      OMX_HANDLETYPE hTunneledComp,
                                      OMX_U32 nTunneledPort,
                                      OMX_TUNNELSETUPTYPE* pSetup)
{
    // First call base class function
    OMX_ERRORTYPE eError = OmxComponent::componentTunnelRequest(nPort,
                                                                hTunneledComp,
                                                                nTunneledPort,
                                                                pSetup);
    if (eError != OMX_ErrorNone) {
        return eError;
    }

    // ST component supporting DvbInfo extension.
    // So retrieve dvb info
    if (nPort == AUDIO_RENDERER_INPUT) {
        assert(this->getPort(nPort).isInput() == true);
        OMX_INDEXTYPE nExtIndex;
        eError = OMX_GetExtensionIndex(hTunneledComp,
                                       (OMX_STRING)OMX_StmDvbAudioDeviceInfoExt,
                                       &nExtIndex);
        assert(eError == OMX_ErrorNone);
        OMX_GetStmLinuxDvbDeviceInfoParams cConfig;
        INIT_DVB_DEVICE_INFO(&cConfig);
        cConfig.hTunnelComp = OmxComponent::handle();
        eError = OMX_GetConfig(hTunneledComp, nExtIndex, &cConfig);
        if (eError != OMX_ErrorNone) {
            OMX_DBGT_ERROR("Can not retrieve dvb info during tunnel request");
        } else {
            copyDeviceInfo(m_dvbInfo, cConfig);
        }
    } else if (nPort == CLOCK_RENDERER_PORT) {
        assert(this->getPort(nPort).isInput() == true);
        OMX_INDEXTYPE nExtIndex;
        eError = OMX_GetExtensionIndex(hTunneledComp,
                                       (OMX_STRING)OMX_StmDvbAudioDeviceInfoExt,
                                       &nExtIndex);
        assert(eError == OMX_ErrorNone);
        OMX_GetStmLinuxDvbDeviceInfoParams cConfig;
        INIT_DVB_DEVICE_INFO(&cConfig);
        copyDeviceInfo(cConfig, m_dvbInfo);
        eError = OMX_SetConfig(hTunneledComp, nExtIndex, &cConfig);
        if (eError != OMX_ErrorNone) {
            OMX_DBGT_ERROR("Can not set dvb info during tunnel request");
        }
    }
    return eError;
}


OMX_ERRORTYPE
AudioRenderer::emptyThisBuffer(OMX_BUFFERHEADERTYPE* pBufferHdr)
{
#ifdef ANDROID
    assert(0 && "Invalid case in proprietary tunneling");
#else
    // We only handle EOF for now
    if(pBufferHdr->nFlags & OMX_BUFFERFLAG_EOS) {
        this->notifyEventHandler(OMX_EventBufferFlag, 0, pBufferHdr->nFlags, 0);
    }
#endif
    return OMX_ErrorNone;
}

OMX_ERRORTYPE
AudioRenderer::fillThisBuffer(OMX_BUFFERHEADERTYPE* pBufferHdr)
{
    assert(0 && "Invalid case in proprietary tunneling");
    return OMX_ErrorNone;
}

OMX_ERRORTYPE
AudioRenderer::useBuffer(OMX_BUFFERHEADERTYPE**  ppBufferHdr,
                         OMX_U32                 nPortIndex,
                         OMX_PTR                 pAppPrivate,
                         OMX_U32                 nSizeBytes,
                         OMX_U8*                 pBuffer)
{
    assert(0 && "Invalid case in proprietary tunneling");
    return OMX_ErrorNone;

}

OMX_ERRORTYPE
AudioRenderer::allocateBuffer(OMX_BUFFERHEADERTYPE**  ppBufferHdr,
                              OMX_U32                 nPortIndex,
                              OMX_PTR                 pAppPrivate,
                              OMX_U32                 nSizeBytes)
{
    assert(0 && "Invalid case in proprietary tunneling");
    return OMX_ErrorNone;
}

OMX_ERRORTYPE
AudioRenderer::freeBuffer(OMX_U32                 nPortIndex,
                          OMX_BUFFERHEADERTYPE*   pBufferHdr)
{
    assert(0 && "Invalid case in proprietary tunneling");
    return OMX_ErrorNone;
}

OMX_ERRORTYPE AudioRenderer::create ()
{
    OMX_ERRORTYPE eError = OMX_ErrorNone;

    OMX_DBGT_PROLOG();

    if ((eError = OmxComponent::create()) != OMX_ErrorNone) {
        OMX_DBGT_ERROR();
        OMX_DBGT_EPILOG();
        return eError;
    }

    // Initialize audio renderer port
    if ((eError = m_inputPort.create()) != OMX_ErrorNone) {
        OMX_DBGT_ERROR("Error when creating audio renderer input port");
        OMX_DBGT_EPILOG();
        return eError;
    }

    // Initialize clock renderer port
    if ((eError = m_clockPort.create()) != OMX_ErrorNone) {
        OMX_DBGT_ERROR("Error when creating audio renderer clock port");
        OMX_DBGT_EPILOG();
        return eError;
    }

    OMX_DBGT_EPILOG();
    return OMX_ErrorNone;
}

void AudioRenderer::destroy()
{
    OMX_DBGT_PROLOG();

    // cleanup the input port
    m_inputPort.destroy();

    // cleanup clock port
    m_clockPort.destroy();

    // cleanup component
    OmxComponent::destroy();
    OMX_DBGT_EPILOG();
}

const OmxPort& AudioRenderer::getPort(unsigned int nIdx) const
{
    assert(nIdx < AUDIO_RENDERER_PORTNB);
    if (nIdx == AUDIO_RENDERER_INPUT)
        return m_inputPort;
    else
        return m_clockPort;
}

OmxPort& AudioRenderer::getPort(unsigned int nIdx)
{
    assert(nIdx < AUDIO_RENDERER_PORTNB);
    if (nIdx == AUDIO_RENDERER_INPUT)
        return m_inputPort;
    else
        return m_clockPort;
}


OMX_ERRORTYPE AudioRenderer::configure()
{ return OMX_ErrorNone; }

OMX_ERRORTYPE AudioRenderer::start()
{ return OMX_ErrorNone; }

void AudioRenderer::stop()
{}

void AudioRenderer::execute()
{}

OMX_ERRORTYPE AudioRenderer::pause()
{ return OMX_ErrorNone; }

OMX_ERRORTYPE AudioRenderer::resume()
{ return OMX_ErrorNone; }

OMX_ERRORTYPE AudioRenderer::flush(unsigned int nPortIdx)
{ return OMX_ErrorNone; }

}; // eof namespace stm

//=====================================================================================
// OMX_ComponentInit
// "C" interface to initialize a new OMX component: creation of the associated object
//======================================================================================
extern "C" OMX_ERRORTYPE OMX_ComponentInit (OMX_HANDLETYPE hComponent)
{
    return stm::OmxComponent::ComponentInit(new stm::AudioRenderer(hComponent));
}

