
LOCAL_PATH:= $(call my-dir)

#=============================================================================
# OMX Audio Renderer
#=============================================================================

include $(CLEAR_VARS)

LOCAL_SRC_FILES:= \
	AudioPort.cpp                \
	AudioRenderer.cpp

# Includes for GTV
LOCAL_C_INCLUDES+= \
        vendor/tv/frameworks/av/include/media/openmax_1.2

# Includes for ICS
LOCAL_C_INCLUDES+= \
	$(TOP)/frameworks/base/include/media/stagefright/openmax
# Includes for JB
LOCAL_C_INCLUDES+= \
	$(TOP)/frameworks/native/include/media/openmax

# Common includes
LOCAL_C_INCLUDES+= \
	external/alsa-lib/include \
	$(TOP)/vendor/stm/hardware/omx/include/ \
	$(TOP)/vendor/stm/hardware/omx/stm_omx_component/common/ \
	$(TOP)/vendor/stm/hardware/omx/stm_omx_component/aud/ \
	$(TOP)/vendor/stm/hardware/header/usr/include \

LOCAL_SHARED_LIBRARIES := \
        libasound \
        libdl \
        libcutils \
        liblog \
        libhardware \
        libSTMOMXCommon


LOCAL_CPPFLAGS += -DANDROID
LOCAL_CFLAGS   += -DDBGT_CONFIG_DEBUG -DDBGT_CONFIG_AUTOVAR -DDBGT_TAG=\"VDEC\" -Werror
LOCAL_CFLAGS   += -DHAVE_DBGTASSERT_H
LOCAL_CFLAGS   += -Wno-error=switch
LOCAL_CFLAGS   += -Wno-unused-parameter
LOCAL_MODULE:= libOMX.STM.Audio.Renderer
LOCAL_MODULE_TAGS := optional
LOCAL_CFLAGS += -D_POSIX_SOURCE
include $(BUILD_SHARED_LIBRARY)
