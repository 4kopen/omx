/*
 * Copyright 2013 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file   AudioPort.cpp
 * @author STMicroelectronics
 */

#include "StmOmxIComponent.hpp"
#include "AudioPort.hpp"
#include "OMX_debug.h"

#if HAVE_ASSERT_H
#include <assert.h>
#endif

#if HAVE_DBGTASSERT_H
// Re-direct assert on DBGT_ASSERT macro
#define assert(expr)  OMX_DBGT_ASSERT(expr)
#endif
#include <linux/dbgt.h>


#include <string.h>


namespace stm {

//=============================================================================
// AudioPort implementation
//=============================================================================

OmxAudioPort::OmxAudioPort(unsigned int nIdx, OMX_DIRTYPE eDir,
                           const OmxComponent& owner)
    :OmxPort(),
     m_owner(owner),
     m_PortDefinition(nIdx, eDir)
{
    // Set default as PCM
    this->definition().compressionFormat(OMX_AUDIO_CodingPCM);
    //check the string length before copy
    assert(strlen(this->component().name()) <= sizeof(m_strName));
    strcpy(m_strName,this->component().name());
    memset(&m_sPcmParam,0,sizeof(m_sPcmParam));
    OMXDEBUG_UPDATE_AV_TRACE_SETTINGS(this->component().name());
}

OmxAudioPort::~OmxAudioPort()
{}

OMX_ERRORTYPE OmxAudioPort::create()
{ return OMX_ErrorNone; }

void OmxAudioPort::destroy()
{}

const AudioPortDefinition& OmxAudioPort::definition() const
{ return m_PortDefinition; }

AudioPortDefinition& OmxAudioPort::definition()
{ return m_PortDefinition; }

const OmxComponent& OmxAudioPort::component() const
{ return m_owner; }


OMX_ERRORTYPE
OmxAudioPort::getParameter(OMX_INDEXTYPE nIndex, OMX_PTR pParamStruct)
{
    OMX_DBGT_PROLOG();
    switch (nIndex)
    {
    case OMX_IndexParamPortDefinition: {
        // Port definition
        ASSERT_STRUCT_TYPE(pParamStruct, OMX_PARAM_PORTDEFINITIONTYPE);
        OMX_PARAM_PORTDEFINITIONTYPE* param;
        param = (OMX_PARAM_PORTDEFINITIONTYPE*)pParamStruct;
        assert(param->nPortIndex == this->definition().index());
        *param = this->definition().getParamPortDef();
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }

    case OMX_IndexParamAudioPortFormat: {
        // Supported audio port format
        ASSERT_STRUCT_TYPE(pParamStruct, OMX_AUDIO_PARAM_PORTFORMATTYPE);
        OMX_AUDIO_PARAM_PORTFORMATTYPE* param;
        param = (OMX_AUDIO_PARAM_PORTFORMATTYPE*)pParamStruct;
        assert(param->nPortIndex == this->definition().index());
        if (param->nIndex == 0) {
            param->eEncoding = OMX_AUDIO_CodingPCM;
            OMX_DBGT_EPILOG();
            return OMX_ErrorNone;
        } else {
            OMX_DBGT_EPILOG("No more supported format");
            return OMX_ErrorNoMore;
        }
    }

    case OMX_IndexParamAudioPcm: {
        ASSERT_STRUCT_TYPE(pParamStruct, OMX_AUDIO_PARAM_PCMMODETYPE);
        OMX_AUDIO_PARAM_PCMMODETYPE* param;
        param = (OMX_AUDIO_PARAM_PCMMODETYPE*)pParamStruct;
        assert(param->nPortIndex == this->definition().index());
        *param = m_sPcmParam;
        return OMX_ErrorNone;
    }

    case OMX_IndexParamOtherPortFormat:
    case OMX_IndexParamVideoPortFormat:
    case OMX_IndexParamImagePortFormat: {
        // Unsupported port format
        OMX_DBGT_EPILOG("Unsupported index");
        return OMX_ErrorUnsupportedIndex;
    }

    default: {
        OMX_DBGT_EPILOG("Unsupported index");
        return OMX_ErrorUnsupportedIndex;
    }
    }
}

OMX_ERRORTYPE
OmxAudioPort::setParameter(OMX_INDEXTYPE nIndex, OMX_PTR pParamStruct)
{
    OMX_DBGT_PROLOG();
    switch (nIndex)
    {
    case OMX_IndexParamPortDefinition: {
        // Port definition
        ASSERT_STRUCT_TYPE(pParamStruct, OMX_PARAM_PORTDEFINITIONTYPE);
        OMX_PARAM_PORTDEFINITIONTYPE* param;
        param = (OMX_PARAM_PORTDEFINITIONTYPE*)pParamStruct;
        assert(param->nPortIndex == this->definition().index());
        this->definition().setParamPortDef(param);
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }

    case OMX_IndexParamAudioPortFormat: {
        // Set the default audio format
        ASSERT_STRUCT_TYPE(pParamStruct, OMX_AUDIO_PARAM_PORTFORMATTYPE);
        OMX_AUDIO_PARAM_PORTFORMATTYPE* param;
        param = (OMX_AUDIO_PARAM_PORTFORMATTYPE*)pParamStruct;
        assert(param->nPortIndex == this->definition().index());
        // Check audio format
        if (param->eEncoding == OMX_AUDIO_CodingPCM) {
            // Audio format is supported
            this->definition().compressionFormat(param->eEncoding);
            OMX_DBGT_EPILOG("Set port audio format %u", param->eEncoding);
            return OMX_ErrorNone;
        }

        // Unsupported audio format
        OMX_DBGT_EPILOG("Unsupported audio format");
        return OMX_ErrorUnsupportedSetting;
    }

    case OMX_IndexParamAudioPcm: {
        ASSERT_STRUCT_TYPE(pParamStruct, OMX_AUDIO_PARAM_PCMMODETYPE);
        OMX_AUDIO_PARAM_PCMMODETYPE* param;
        param = (OMX_AUDIO_PARAM_PCMMODETYPE*)pParamStruct;
        assert(param->nPortIndex == this->definition().index());
        this->definition().compressionFormat(OMX_AUDIO_CodingPCM);
        m_sPcmParam = *param;
        return OMX_ErrorNone;
    }

    case OMX_IndexParamOtherPortFormat:
    case OMX_IndexParamVideoPortFormat:
    case OMX_IndexParamImagePortFormat: {
        // Unsupported port format
        OMX_DBGT_EPILOG("Unsupported index");
        return OMX_ErrorUnsupportedIndex;
    }

    default: {
        OMX_DBGT_EPILOG("Unsupported index");
        return OMX_ErrorUnsupportedIndex;
    }
    }
}

OMX_ERRORTYPE
OmxAudioPort::emptyThisBuffer(OMX_BUFFERHEADERTYPE* pBuffer)
{
    assert(this->isTunneled() == false);
    return OMX_ErrorNone;
}

OMX_ERRORTYPE
OmxAudioPort::fillThisBuffer(OMX_BUFFERHEADERTYPE* pBuffer)
{
    assert(this->isTunneled() == false);
    return OMX_ErrorNone;
}

} // eof namespace stm
