/*
 * Copyright 2013 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file   AudioPort.hpp
 * @author STMicroelectronics
 */

#ifndef _STM_VIDEOPORT_HPP_
#define _STM_VIDEOPORT_HPP_

#include "StmOmxPort.hpp"
#include "StmOmxComponent.hpp"

#include "AudioPortDefinition.hpp"

namespace stm {

/**
 * An audio port object.
 * Default implementation is an pcm audio port, supporting only PCM format
 */
class OmxAudioPort: public OmxPort
{
public:
    OmxAudioPort(unsigned int nIdx, OMX_DIRTYPE eDir,
                 const OmxComponent& owner);
    virtual ~OmxAudioPort();

    virtual OMX_ERRORTYPE create();
    virtual void destroy();

    virtual const OmxComponent& component() const;
    virtual const AudioPortDefinition& definition() const;

    virtual OMX_ERRORTYPE
    getParameter(OMX_INDEXTYPE nIndex, OMX_PTR pParamStruct);

    virtual OMX_ERRORTYPE
    setParameter(OMX_INDEXTYPE nIndex, OMX_PTR pParamStruct);

    virtual OMX_ERRORTYPE
    emptyThisBuffer(OMX_BUFFERHEADERTYPE* pBuffer);

    virtual OMX_ERRORTYPE
    fillThisBuffer(OMX_BUFFERHEADERTYPE* pBuffer);

protected:
    virtual AudioPortDefinition& definition();

private:
    const OmxComponent&         m_owner;
    AudioPortDefinition         m_PortDefinition;

    OMX_AUDIO_PARAM_PCMMODETYPE m_sPcmParam;
};

} // eof namespace stm

#endif // _STM_AUDIOPORT_HPP_
