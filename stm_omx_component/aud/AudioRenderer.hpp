/*
 * Copyright 2013 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file   AudioRenderer.hpp
 * @author STMicroelectronics
 */

#ifndef _STM_OMXAUDIORENDERER_HPP_
#define _STM_OMXAUDIORENDERER_HPP_

#include "StmOmxComponent.hpp"
#include "StmOmxVendorExt.hpp"
#include "AudioPort.hpp"
#include "ClockPort.hpp"


#include <OMX_Audio.h>
#include <alsa/asoundlib.h>
namespace stm {

class AudioRenderer: public OmxComponent
{
public:
    /** Constructor */
    AudioRenderer(OMX_HANDLETYPE hComponent);

    /** Destructor */
    virtual ~AudioRenderer();

    virtual OMX_ERRORTYPE create();
    virtual void destroy();
    virtual const OmxPort& getPort(unsigned int nIdx) const;

    virtual OMX_ERRORTYPE getParameter(OMX_INDEXTYPE       nParamIndex,
                                       OMX_PTR             pParamStruct);
    virtual OMX_ERRORTYPE setParameter(OMX_INDEXTYPE       nParamIndex,
                                       OMX_PTR             pParamStruct);
    virtual OMX_ERRORTYPE getConfig(OMX_INDEXTYPE nIndex,
                                    OMX_PTR pParamStructure);

    virtual OMX_ERRORTYPE setConfig(OMX_INDEXTYPE nIndex,
                                    OMX_PTR pComponentConfigStructure);

    virtual OMX_ERRORTYPE getExtensionIndex(OMX_STRING     cParameterName,
                                            OMX_INDEXTYPE* pIndexType);
    virtual
    OMX_ERRORTYPE componentTunnelRequest(OMX_U32 nPort,
                                         OMX_HANDLETYPE hTunneledComp,
                                         OMX_U32 nTunneledPort,
                                         OMX_TUNNELSETUPTYPE* pSetup);
    virtual OMX_ERRORTYPE emptyThisBuffer(OMX_BUFFERHEADERTYPE* pBufferHdr);
    virtual OMX_ERRORTYPE fillThisBuffer(OMX_BUFFERHEADERTYPE* pBufferHdr);

    virtual OMX_ERRORTYPE useBuffer(OMX_BUFFERHEADERTYPE**  ppBufferHdr,
                                    OMX_U32                 nPortIndex,
                                    OMX_PTR                 pAppPrivate,
                                    OMX_U32                 nSizeBytes,
                                    OMX_U8*                 pBuffer);
    virtual OMX_ERRORTYPE allocateBuffer(OMX_BUFFERHEADERTYPE**  ppBufferHdr,
                                         OMX_U32                 nPortIndex,
                                         OMX_PTR                 pAppPrivate,
                                         OMX_U32                 nSizeBytes);
    virtual OMX_ERRORTYPE freeBuffer(OMX_U32                 nPortIndex,
                                     OMX_BUFFERHEADERTYPE*   pBufferHdr);


protected:
    virtual OmxPort& getPort(unsigned int nIdx);
    virtual OMX_ERRORTYPE configure();
    virtual OMX_ERRORTYPE start();
    virtual void stop();
    virtual void execute();
    virtual OMX_ERRORTYPE pause();
    virtual OMX_ERRORTYPE resume();
    virtual OMX_ERRORTYPE flush(unsigned int nPortIdx);

private:
    static const char kRole[];
    static const unsigned int AUDIO_RENDERER_PORTNB = 2;
    static const unsigned int AUDIO_RENDERER_INPUT = 0;
    static const unsigned int CLOCK_RENDERER_PORT   = 1;
    OMX_ERRORTYPE set_alsa_ctrl(const char *pname, unsigned int value, int index);
    OmxAudioPort      m_inputPort;
    OmxClockPort      m_clockPort;

    OMX_GetStmLinuxDvbDeviceInfoParams  m_dvbInfo;
snd_ctl_t* mHandle;
};

} // eof namespace stm

#endif // _STM_OMXAUDIORENDERER_HPP_
