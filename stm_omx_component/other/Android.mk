
LOCAL_PATH:= $(call my-dir)

#=============================================================================
# OMX Clock
#=============================================================================

include $(CLEAR_VARS)

LOCAL_SRC_FILES:= \
	Clock.cpp

# Includes for ICS
LOCAL_C_INCLUDES+= \
	$(TOP)/frameworks/base/include/media/stagefright/openmax
# Includes for JB
LOCAL_C_INCLUDES+= \
	$(TOP)/frameworks/native/include/media/openmax

# Common includes
LOCAL_C_INCLUDES+= \
	$(TOP)/vendor/stm/hardware/omx/include/ \
	$(TOP)/vendor/stm/hardware/omx/stm_omx_component/common/ \
	$(TOP)/vendor/stm/hardware/omx/stm_omx_component/other/ \
	$(TOP)/vendor/stm/hardware/header/usr/include \
	$(TOP)/vendor/stm/module/stlinuxtv/linux/include \


LOCAL_SHARED_LIBRARIES := \
        libdl \
        libcutils \
        liblog \
        libhardware \
        libSTMOMXCommon


LOCAL_CPPFLAGS += -DANDROID
LOCAL_CFLAGS   += -DDBGT_CONFIG_DEBUG -DDBGT_CONFIG_AUTOVAR -DDBGT_TAG=\"VDEC\" -Werror
LOCAL_CFLAGS   += -DHAVE_DBGTASSERT_H
LOCAL_CFLAGS   += -Wno-error=switch
LOCAL_CFLAGS   += -Wno-unused-parameter
LOCAL_MODULE:= libOMX.STM.Clock
LOCAL_MODULE_TAGS := optional

include $(BUILD_SHARED_LIBRARY)
