/*
 * Copyright 2013 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file   Clock.cpp
 * @author STMicroelectronics
 */
#include "string.h"
#include "Clock.hpp"
#include "OMX_debug.h"
#include <sys/ioctl.h>
#if HAVE_ASSERT_H
#include <assert.h>
#endif

#if HAVE_DBGTASSERT_H
// Re-direct assert on DBGT_ASSERT macro
#define assert(expr)  OMX_DBGT_ASSERT(expr)
#endif

#define DBGT_DECLARE_AUTOVAR
#define DBGT_PREFIX "CLCK"
#define DBGT_LAYER 0
#include <linux/dbgt.h>
#include <linux/dvb/stm_dvb.h>
#include <linux/dvb/stm_video.h>

namespace stm {

const char Clock::kRole[] = "clock.binary";

Clock::Clock(OMX_HANDLETYPE hComponent)
    :OmxComponent("OMX.STM.Clock", hComponent, CLOCK_PORTNB),
     m_audioPort(AUDIO_CLOCK_PORT, OMX_DirOutput, *this),
     m_videoPort(VIDEO_CLOCK_PORT, OMX_DirOutput, *this),
     m_xScale(0x10000),
     m_nLastMediaTime(0)
{
    INIT_DVB_DEVICE_INFO(&m_dvbVideoInfo);
    INIT_DVB_DEVICE_INFO(&m_dvbAudioInfo);
    DBGT_TRACE_INIT(clck); //expands debug.clck.trace
}

Clock::~Clock()
{}

IDvbDevice* Clock::audioDevice() const
{
    if (m_dvbAudioInfo.ppDevice) {
        return (*m_dvbAudioInfo.ppDevice);
    } else {
        OMX_DBGT_PINFO("Play video only file");
        return (0);
    }
}

IDvbDevice* Clock::videoDevice() const
{
    if (m_dvbVideoInfo.ppDevice) {
        return (*m_dvbVideoInfo.ppDevice);
    } else {
        OMX_DBGT_PINFO("Play audio only file");
        return (0);
    }
}

OMX_ERRORTYPE
Clock::getParameter(OMX_INDEXTYPE       nParamIndex,
                    OMX_PTR             pParamStruct)
{
    OMX_DBGT_PROLOG();
    switch ((int)nParamIndex)
    {
    case OMX_IndexParamOtherInit: {
        ASSERT_STRUCT_TYPE(pParamStruct, OMX_PORT_PARAM_TYPE);
        OMX_PORT_PARAM_TYPE *param = (OMX_PORT_PARAM_TYPE*)pParamStruct;
        assert(param);
        param->nPorts = CLOCK_PORTNB;
        param->nStartPortNumber = 0;
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }; break;

    }

    // If the component does not support it, we fall back to generic logic.
    OMX_ERRORTYPE eError = OmxComponent::getParameter(nParamIndex,
                                                      pParamStruct);

    OMX_DBGT_EPILOG();
    return eError;
}

OMX_ERRORTYPE
Clock::setParameter(OMX_INDEXTYPE       nParamIndex,
                    OMX_PTR             pParamStruct)
{
    OMX_DBGT_PROLOG();
    switch ((int)nParamIndex)
    {
    case OMX_IndexParamStandardComponentRole: {
        ASSERT_STRUCT_TYPE(pParamStruct, OMX_PARAM_COMPONENTROLETYPE);
        OMX_PARAM_COMPONENTROLETYPE *param = (OMX_PARAM_COMPONENTROLETYPE*)pParamStruct;
        assert(param);
        if (strcmp((char*)param->cRole, "default") == 0) {
            OMX_DBGT_EPILOG("Setting role to default");
            return OMX_ErrorNone;
        }
        if (strcmp((char*)param->cRole, kRole)) {
            OMX_DBGT_ERROR("Unsupported role %s", param->cRole);
            return OMX_ErrorUnsupportedSetting;
        }
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }; break;
    }

    // If the component does not support it, we fall back to generic logic.
    OMX_ERRORTYPE eError = OmxComponent::setParameter(nParamIndex,
                                                      pParamStruct);

    OMX_DBGT_EPILOG();
    return eError;
}

OMX_ERRORTYPE Clock::getConfig(OMX_INDEXTYPE nIndex,
                               OMX_PTR pConfigStruct)
{
    OMX_DBGT_PROLOG();
    switch ((int)nIndex)
    {
    case OMX_IndexConfigTimeScale: {
        // To pause and resume playback
        return OMX_ErrorNotImplemented;
    }; break;

    case OMX_IndexConfigTimeClockState: {
        // To get current clock state
        return OMX_ErrorNotImplemented;
    }; break;

    case OMX_IndexConfigTimeCurrentMediaTime: {
        // To get current clock state
        ASSERT_STRUCT_TYPE(pConfigStruct, OMX_TIME_CONFIG_TIMESTAMPTYPE);
        OMX_TIME_CONFIG_TIMESTAMPTYPE* config;
        config = (OMX_TIME_CONFIG_TIMESTAMPTYPE*)pConfigStruct;
        config->nTimestamp = this->getMediaTime();
        return OMX_ErrorNone;
    }; break;
    }

    // If the component does not support it, we fall back to generic logic.
    OMX_ERRORTYPE eError = OmxComponent::getConfig(nIndex,
                                                   pConfigStruct);

    OMX_DBGT_EPILOG();
    return eError;
}

OMX_ERRORTYPE Clock::setConfig(OMX_INDEXTYPE nIndex,
                               OMX_PTR pConfigStruct)
{
    OMX_DBGT_PROLOG();
    switch ((int)nIndex)
    {
    case OMX_IndexConfigTimeScale: {
        // To pause and resume playback
        ASSERT_STRUCT_TYPE(pConfigStruct, OMX_TIME_CONFIG_SCALETYPE);
        OMX_TIME_CONFIG_SCALETYPE* config;
        config = (OMX_TIME_CONFIG_SCALETYPE*)pConfigStruct;
        if (m_xScale == config->xScale) {
            OMX_DBGT_EPILOG();
            return OMX_ErrorNone;
        }
        // Value is in Q16 format which is used for scaling the media time
        m_xScale = config->xScale;
        assert(m_xScale == 0 || m_xScale == 0x10000);
        if (m_xScale) {
            // Resume playback
            if (this->audioDevice()) {
                this->audioDevice()->resume(true);
                if (this->videoDevice()) this->videoDevice()->resume(false);
            }
            else if (this->videoDevice()) this->videoDevice()->resume(true);
        } else {
            // Pause playback
            if (this->audioDevice()) {
                if (this->videoDevice()) this->videoDevice()->lock();
                this->audioDevice()->lock();
                this->audioDevice()->pause(true);
                if (this->videoDevice()) this->videoDevice()->pause(false);
                if (this->videoDevice()) this->videoDevice()->unlock();
                this->audioDevice()->unlock();
            }
            else if (this->videoDevice()) this->videoDevice()->pause(true);
        }
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }; break;

    case OMX_IndexConfigTimeClockState: {
        // To start stop playback
        ASSERT_STRUCT_TYPE(pConfigStruct, OMX_TIME_CONFIG_CLOCKSTATETYPE);
        OMX_TIME_CONFIG_CLOCKSTATETYPE* config;
        config = (OMX_TIME_CONFIG_CLOCKSTATETYPE*)pConfigStruct;
        if (config->eState == OMX_TIME_ClockStateStopped) {
            if (this->audioDevice()) this->audioDevice()->clear();
            if (this->videoDevice()) this->videoDevice()->clear();
            OMX_DBGT_EPILOG();
            return OMX_ErrorNone;
        } else if  (config->eState == OMX_TIME_ClockStateRunning) {
            m_nLastMediaTime = config->nStartTime;
            OMX_DBGT_EPILOG();
            return OMX_ErrorNone;
        } else {
            OMX_DBGT_EPILOG();
            return OMX_ErrorNotImplemented;
        }
    }; break;

    case OMX_StmDvbVideoDeviceInfo: {
        ASSERT_STRUCT_TYPE(pConfigStruct, OMX_GetStmLinuxDvbDeviceInfoParams);
        OMX_GetStmLinuxDvbDeviceInfoParams* config;
        config = (OMX_GetStmLinuxDvbDeviceInfoParams*)pConfigStruct;
        assert(config);
        copyDeviceInfo(m_dvbVideoInfo, *config);
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }; break;

    case OMX_StmDvbAudioDeviceInfo: {
        ASSERT_STRUCT_TYPE(pConfigStruct, OMX_GetStmLinuxDvbDeviceInfoParams);
        OMX_GetStmLinuxDvbDeviceInfoParams* config;
        config = (OMX_GetStmLinuxDvbDeviceInfoParams*)pConfigStruct;
        assert(config);
        copyDeviceInfo(m_dvbAudioInfo, *config);
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }; break;
    }

    // If the component does not support it, we fall back to generic logic.
    OMX_ERRORTYPE eError = OmxComponent::setConfig(nIndex,
                                                   pConfigStruct);

    OMX_DBGT_EPILOG();
    return eError;
}

OMX_ERRORTYPE
Clock::getExtensionIndex(OMX_STRING          cParameterName,
                         OMX_INDEXTYPE*      pIndexType)
{
    OMX_DBGT_PROLOG();
    if (strcmp(cParameterName,
               OMX_StmDvbVideoDeviceInfoExt) == 0) {
        *pIndexType = (OMX_INDEXTYPE)OMX_StmDvbVideoDeviceInfo;
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }
    if (strcmp(cParameterName,
               OMX_StmDvbAudioDeviceInfoExt) == 0) {
        *pIndexType = (OMX_INDEXTYPE)OMX_StmDvbAudioDeviceInfo;
        OMX_DBGT_EPILOG();
        return OMX_ErrorNone;
    }
    // We fall back to generic logic.
    OMX_ERRORTYPE eError = OmxComponent::getExtensionIndex(cParameterName,
                                                           pIndexType);
    OMX_DBGT_EPILOG();
    return eError;
}

namespace {

bool getDeviceMediaTime(const OMX_GetStmLinuxDvbDeviceInfoParams& dev,
                        uint64_t* time)
{
    DBGT_ASSERT(time);
    if (dev.ppDevice && *dev.ppDevice) {
        DBGT_ASSERT(*dev.ppDevice);
        if ((*dev.ppDevice)->isStarted()) {
            uint64_t current_pts = (*dev.ppDevice)->getTime();
            if (current_pts) {
                *time =current_pts;
                return true;
            } else {
                return false;
            }
        }
    }
    return false;
}
}

uint64_t Clock::getMediaTime() const
{
    OMX_DBGT_PROLOG();
    uint64_t dvb_time = 0;

    if (m_xScale != 0) {
        if (this->audioDevice()) {
           if (!getDeviceMediaTime(m_dvbAudioInfo, &dvb_time)) {
                OMX_DBGT_WARNING("No valid media time available");
            } else {
                m_nLastMediaTime = dvb_time;
            }
        } else {
            if (!getDeviceMediaTime(m_dvbVideoInfo, &dvb_time)) {
                OMX_DBGT_WARNING("No valid media time available");
            } else {
                m_nLastMediaTime = dvb_time;
            }
        }
    }

    OMX_DBGT_EPILOG();
    return m_nLastMediaTime;
}

OMX_ERRORTYPE
Clock::emptyThisBuffer(OMX_BUFFERHEADERTYPE* pBufferHdr)
{
    assert(0 && "Invalid case in proprietary tunneling");
    return OMX_ErrorNone;
}

OMX_ERRORTYPE
Clock::fillThisBuffer(OMX_BUFFERHEADERTYPE* pBufferHdr)
{
    assert(0 && "Invalid case in proprietary tunneling");
    return OMX_ErrorNone;
}

OMX_ERRORTYPE
Clock::useBuffer(OMX_BUFFERHEADERTYPE**  ppBufferHdr,
                         OMX_U32                 nPortIndex,
                         OMX_PTR                 pAppPrivate,
                         OMX_U32                 nSizeBytes,
                         OMX_U8*                 pBuffer)
{
    assert(0 && "Invalid case in proprietary tunneling");
    return OMX_ErrorNone;
}

OMX_ERRORTYPE
Clock::allocateBuffer(OMX_BUFFERHEADERTYPE**  ppBufferHdr,
                              OMX_U32                 nPortIndex,
                              OMX_PTR                 pAppPrivate,
                              OMX_U32                 nSizeBytes)
{
    assert(0 && "Invalid case in proprietary tunneling");
    return OMX_ErrorNone;
}

OMX_ERRORTYPE
Clock::freeBuffer(OMX_U32                 nPortIndex,
                          OMX_BUFFERHEADERTYPE*   pBufferHdr)
{
    assert(0 && "Invalid case in proprietary tunneling");
    return OMX_ErrorNone;
}

OMX_ERRORTYPE Clock::create ()
{
    OMX_ERRORTYPE eError = OMX_ErrorNone;

    OMX_DBGT_PROLOG();

    if((eError = OmxComponent::create()) != OMX_ErrorNone) {
        OMX_DBGT_ERROR();
        OMX_DBGT_EPILOG();
        return eError;
    }

    // Initialize clock ports
    if ((eError = m_audioPort.create()) != OMX_ErrorNone) {
        OMX_DBGT_ERROR("Error when creating audio clock port");
        OMX_DBGT_EPILOG();
        return eError;
    }
    if ((eError = m_videoPort.create()) != OMX_ErrorNone) {
        OMX_DBGT_ERROR("Error when creating video clock port");
        OMX_DBGT_EPILOG();
        return eError;
    }

    OMX_DBGT_EPILOG();
    return OMX_ErrorNone;
}

void Clock::destroy()
{
    OMX_DBGT_PROLOG();

    // cleanup ports
    m_audioPort.destroy();
    m_videoPort.destroy();

    OmxComponent::destroy();
    OMX_DBGT_EPILOG();
}

const OmxClockPort& Clock::getPort(unsigned int nIdx) const
{
    OMX_DBGT_PROLOG("Getting clock port %u", nIdx);
    assert(nIdx < CLOCK_PORTNB);
    OMX_DBGT_EPILOG();
    return ((nIdx == 0)? m_audioPort: m_videoPort);
}

OmxClockPort& Clock::getPort(unsigned int nIdx)
{
    OMX_DBGT_PROLOG("Getting clock port %u", nIdx);
    assert(nIdx < CLOCK_PORTNB);
    OMX_DBGT_EPILOG();
    return ((nIdx == 0)? m_audioPort: m_videoPort);
}

OMX_ERRORTYPE Clock::configure()
{ return OMX_ErrorNone; }

OMX_ERRORTYPE Clock::start()
{ return OMX_ErrorNone; }

void Clock::stop()
{}

void Clock::execute()
{}

OMX_ERRORTYPE Clock::pause()
{ return OMX_ErrorNone; }

OMX_ERRORTYPE Clock::resume()
{ return OMX_ErrorNone; }

OMX_ERRORTYPE Clock::flush(unsigned int nPortIdx)
{ return OMX_ErrorNone; }

} // eof namespace stm

//=====================================================================================
// OMX_ComponentInit
// "C" interface to initialize a new OMX component: creation of the associated object
//======================================================================================
extern "C" OMX_ERRORTYPE OMX_ComponentInit (OMX_HANDLETYPE hComponent)
{
    return stm::OmxComponent::ComponentInit(new stm::Clock(hComponent));
}

