LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)

LOCAL_PRELINK_MODULE := false

LOCAL_SRC_FILES:= \
	OMX_Core.c

# Includes for ICS
LOCAL_C_INCLUDES += \
	$(TOP)/frameworks/base/include/media/stagefright/openmax

# Includes for JB
LOCAL_C_INCLUDES += \
    $(TOP)/frameworks/native/include/media/openmax

# Common includes
LOCAL_C_INCLUDES += \
	$(TOP)/kernel/stmfb/linux/kernel \
	$(TOP)/vendor/stm/hardware/header/usr/include \
	$(TOP)/vendor/stm/hardware/omx/include/ \
	$(TOP)/vendor/stm/hardware/omx/stm_omx_component/common/inc/
LOCAL_SHARED_LIBRARIES := \
    libcutils \
	libdl \
	liblog

LOCAL_CFLAGS := $(STM_OMX_CFLAGS)
LOCAL_CFLAGS   += -DDBGT_CONFIG_DEBUG -DDBGT_CONFIG_AUTOVAR

LOCAL_MODULE:= libOMX_Core
LOCAL_MODULE_TAGS := optional

include $(BUILD_SHARED_LIBRARY)
