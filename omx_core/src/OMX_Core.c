/*
 * Texas Instruments OMAP(TM) Platform Software
 * (c) Copyright Texas Instruments, Incorporated. All Rights Reserved.
 *
 * Copyright 2011 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *
 * File	  ::	OMX_Core.c	
 * Author ::	Sachin Verma (sachin.verma@st.com)
 *              Jean-Philippe Fassino (jean-philippe.fassino@st.com)
 *
 *
 */


#include <dlfcn.h>   /* For dynamic loading */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <stdint.h>

#include <OMX_Component.h>
#include <OMX_Core.h>

#undef  DBGT_TAG
#define DBGT_TAG "OMXC"
#define DBGT_PREFIX "OMXC"
#define DBGT_LAYER  0
#define DBGT_VAR mDbgtVarOMXC
#define DBGT_DECLARE_AUTOVAR
#include <linux/dbgt.h>


/** size for the array of allocated components.  Sets the maximum
 * number of components that can be allocated at once */
#define MAXCOMP 50
#define MAXNAMESIZE 130
#define MAX_ROLES 20

/* struct definitions */
typedef struct _ComponentTable {
    OMX_STRING name;
    OMX_U16 nRoles;
    OMX_STRING pRoleArray[MAX_ROLES];
} ComponentTable;

/** Determine the number of elements in an array */
#define COUNTOF(x) (sizeof(x)/sizeof(x[0]))

/** Array to hold the DLL pointers for each allocated component */
static void* pModules[MAXCOMP] = {0};

/** Array to hold the component handles for each allocated component */
static OMX_HANDLETYPE pComponents[MAXCOMP] = {0};

/** count will be used as a reference counter for OMX_Init()
    so all changes to count should be mutex protected */
int count = 0;
static pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

ComponentTable componentTable[] = {
    {"OMX.STM.Video.Decoder", 14, {"video_decoder.mpeg2","video_decoder.mpeg4", "video_decoder.avc","video_decoder.h263","video_decoder.vc1","video_decoder.wmv","video_decoder.flv1","video_decoder.mjpeg","video_decoder.vp8","video_decoder.theora","video_decoder.sorenson","video_decoder.cavs","video_decoder.hevc","video_decoder.vp9", }},
    {"OMX.STM.Video.Encoder", 1, {"video_encoder.avc"}},
    {"OMX.STM.Audio.Decoder", 10, {"audio_decoder.mp3", "audio_decoder.vorbis", "audio_decoder.aac", "audio_decoder.wma", "audio_decoder.ac3","audio_decoder.dts", "audio_decoder.amrnb", "audio_decoder.pcm","audio_decoder.adpcm","audio_decoder.mpa",}},
    {"OMX.STM.Audio.Encoder", 1, {"audio_encoder.aac"}},
    {"OMX.STM.Video.Renderer", 2, {"iv_renderer.yuv.overlay", "video_renderer.yuv.overlay" }},
    {"OMX.STM.Video.Processor", 2, {"iv_processor.yuv", "video_processor.yuv" }},
    {"OMX.STM.Video.Scheduler", 1, {"video_scheduler.binary"}},
    {"OMX.STM.Audio.Renderer", 1, {"audio_renderer.pcm" }},
    {"OMX.STM.Clock", 1, {"clock.binary" }},
    {"OMX.STM.Video.Decoder.secure", 4, {"video_decoder.avc","video_decoder.vc1","video_decoder.wmv","video_decoder.vp8", }},
};

/******************************Public*Routine******************************\
* OMX_Init()
*
* Description:This method will initialize the OMX Core.  It is the
* responsibility of the application to call OMX_Init to ensure the proper
* set up of core resources.
*
* Returns:    OMX_NOERROR          Successful
*
* Note
*
\**************************************************************************/
OMX_ERRORTYPE OMX_Init()
{
    DBGT_TRACE_INIT(omx);// Reuse OMX trace property
    pthread_mutex_lock(&mutex);

    count++;
    DBGT_PDEBUG("DT0 2Init count = %d", count);

    pthread_mutex_unlock(&mutex);
    return OMX_ErrorNone;
}
/******************************Public*Routine******************************\
* OMX_GetHandle
*
* Description: This method will create the handle of the COMPONENTTYPE
* If the component is currently loaded, this method will reutrn the
* hadle of existingcomponent or create a new instance of the component.
* It will call the OMX_ComponentInit function and then the setcallback
* method to initialize the callback functions
* Parameters:
* @param[out] pHandle            Handle of the loaded components
* @param[in] cComponentName     Name of the component to load
* @param[in] pAppData           Used to identify the callbacks of component
* @param[in] pCallBacks         Application callbacks
*
* @retval OMX_ErrorUndefined
* @retval OMX_ErrorInvalidComponentName
* @retval OMX_ErrorInvalidComponent
* @retval OMX_ErrorInsufficientResources
* @retval OMX_NOERROR                      Successful
*
* Note
*
\**************************************************************************/

OMX_ERRORTYPE OMX_GetHandle(
        OMX_HANDLETYPE* pHandle,
        OMX_STRING cComponentName,
        OMX_PTR pAppData,
        OMX_CALLBACKTYPE* pCallBacks)
{
    static const char prefix[] = "lib";
    static const char postfix[] = ".so";
    OMX_ERRORTYPE (*pComponentInit)(OMX_HANDLETYPE);
    OMX_COMPONENTTYPE *componentType;
    OMX_ERRORTYPE eError;
    uint32_t i = COUNTOF(pModules);

    DBGT_PROLOG();

    pthread_mutex_lock(&mutex);

    DBGT_CHECK_EXIT(NULL != cComponentName, OMX_ErrorBadParameter);
    DBGT_CHECK_EXIT(NULL != pHandle, OMX_ErrorBadParameter);
    DBGT_CHECK_EXIT(NULL != pCallBacks, OMX_ErrorBadParameter);

    /* Verify that the name is not too long and could cause a crash.  Notice
     * that the comparison is a greater than or equals.  This is to make
     * sure that there is room for the terminating NULL at the end of the
     * name. */
    DBGT_CHECK_EXIT(strlen(cComponentName) < MAXNAMESIZE, OMX_ErrorInvalidComponentName);

    /* Locate the first empty slot for a component.  If no slots
     * are available, error out */
    for(i = 0; i < COUNTOF(pModules); i++)
    {
        if(pModules[i] == NULL)
            break;
    }
    DBGT_CHECK_EXIT(i != COUNTOF(pModules), OMX_ErrorInsufficientResources);

    /* load the component and check for an error.  If filename is not an
     * absolute path (i.e., it does not  begin with a "/"), then the
     * file is searched for in the following locations:
     *
     *     The LD_LIBRARY_PATH environment variable locations
     *     The library cache, /etc/ld.so.cache.
     *     /lib
     *     /usr/lib
     *
     * If there is an error, we can't go on, so set the error code and exit */

    /* the lengths are defined herein or have been
     * checked already, so strcpy and strcat are
     * are safe to use in this context. */
    char buf[sizeof(prefix) + MAXNAMESIZE + sizeof(postfix)];
    strcpy(buf, prefix);
    strcat(buf, cComponentName);
    strcat(buf, postfix);

    pModules[i] = dlopen(buf, RTLD_LAZY | RTLD_GLOBAL | RTLD_NOW);
    DBGT_CHECK_EXIT_MSG(pModules[i] != NULL, OMX_ErrorComponentNotFound, dlerror());

    /* Get a function pointer to the "OMX_ComponentInit" function.  If
     * there is an error, we can't go on, so set the error code and exit */
    pComponentInit = dlsym(pModules[i], "OMX_ComponentInit");
    DBGT_CHECK_EXIT_MSG(pComponentInit != NULL, OMX_ErrorInvalidComponent, dlerror());

    /* We now can access the dll.  So, we need to call the "OMX_ComponentInit"
     * method to load up the "handle" (which is just a list of functions to
     * call) and we should be all set.*/
    *pHandle = pComponents[i] = malloc(sizeof(OMX_COMPONENTTYPE));
    DBGT_CHECK_EXIT(pComponents[i] != NULL, OMX_ErrorInsufficientResources);

    componentType = (OMX_COMPONENTTYPE*) pComponents[i];
    componentType->nSize = sizeof(OMX_COMPONENTTYPE);
    componentType->nVersion.s.nVersionMajor = 1;
    componentType->nVersion.s.nVersionMinor = 1;
    componentType->nVersion.s.nRevision = 0;
    componentType->nVersion.s.nStep = 0;

    eError = (*pComponentInit)(pComponents[i]);
    DBGT_CHECK_EXIT_MSG(eError == OMX_ErrorNone, eError, "ComponentInit failed");

    eError = componentType->SetCallbacks(pComponents[i], pCallBacks, pAppData);
    DBGT_CHECK_EXIT_MSG(eError == OMX_ErrorNone, eError, "SetCallBacks failed");

    pthread_mutex_unlock(&mutex);

    DBGT_PINFO("%s Handle=%p (STOMX_GetHanle)", cComponentName, *pHandle);

    DBGT_EPILOG();
    return OMX_ErrorNone;
EXIT:
    if(i != COUNTOF(pModules))
    {
        if(pComponents[i] != NULL)
        {
            free(pComponents[i]);
            *pHandle = pComponents[i] = NULL;
        }
        if(pModules[i] != NULL)
        {
            dlclose(pModules[i]);
            pModules[i] = NULL;
        }
    }

    pthread_mutex_unlock(&mutex);

    DBGT_EPILOG();
    return eError;
}


/******************************Public*Routine******************************\
* OMX_FreeHandle()
*
* Description:This method will unload the OMX component pointed by 
* OMX_HANDLETYPE. It is the responsibility of the calling method to ensure that
* the Deinit method of the component has been called prior to unloading component
*
* Parameters:
* @param[in] hComponent the component to unload
*
* Returns:    OMX_NOERROR          Successful
*
* Note
*
\**************************************************************************/
OMX_ERRORTYPE OMX_FreeHandle (OMX_HANDLETYPE hComponent)
{
    OMX_COMPONENTTYPE *pHandle = (OMX_COMPONENTTYPE *)hComponent;
    OMX_ERRORTYPE eError;
    uint32_t i = COUNTOF(pModules);

    DBGT_PROLOG();

    DBGT_PINFO("Handle=%p (OMX_FreeHandle)", hComponent);

    pthread_mutex_lock(&mutex);

    /* Locate the component handle in the array of handles */
    for(i=0; i< COUNTOF(pModules); i++) {
        if(pComponents[i] == hComponent) break;
    }
    DBGT_CHECK_EXIT(i != COUNTOF(pModules), OMX_ErrorBadParameter);

    if(pHandle->ComponentDeInit(hComponent) != OMX_ErrorNone)
        DBGT_ERROR("ComponentDeInit failed");

    dlclose(pModules[i]);
    pModules[i] = NULL;
    free(pComponents[i]);
    pComponents[i] = NULL;

    DBGT_EPILOG();
    eError = OMX_ErrorNone;
EXIT:
    pthread_mutex_unlock(&mutex);

    DBGT_EPILOG();
    return eError;
}

/******************************Public*Routine******************************\
* OMX_DeInit()
*
* Description:This method will release the resources of the OMX Core.  It is the 
* responsibility of the application to call OMX_DeInit to ensure the clean up of these
* resources.
*
* Returns:    OMX_NOERROR          Successful
*
* Note
*
\**************************************************************************/
OMX_ERRORTYPE OMX_Deinit()
{
    pthread_mutex_lock(&mutex);

    DBGT_PROLOG();
    if (count)
        count--;

    DBGT_PDEBUG("deinit count = %d\n", count);

    pthread_mutex_unlock(&mutex);

    DBGT_EPILOG();
    return OMX_ErrorNone;
}

/*************************************************************************
* OMX_SetupTunnel()
*
* Description: Setup the specified tunnel the two components
*
* Parameters:
* @param[in] hOutput     Handle of the component to be accessed
* @param[in] nPortOutput Source port used in the tunnel
* @param[in] hInput      Component to setup the tunnel with.
* @param[in] nPortInput  Destination port used in the tunnel
*
* Returns:    OMX_NOERROR          Successful
*
* Note
*
**************************************************************************/
/* OMX_SetupTunnel */
OMX_API OMX_ERRORTYPE OMX_APIENTRY OMX_SetupTunnel(
    OMX_IN  OMX_HANDLETYPE hOutput,
    OMX_IN  OMX_U32 nPortOutput,
    OMX_IN  OMX_HANDLETYPE hInput,
    OMX_IN  OMX_U32 nPortInput)
{
    OMX_ERRORTYPE eError = OMX_ErrorNotImplemented;
    OMX_COMPONENTTYPE *pCompIn, *pCompOut;
    OMX_TUNNELSETUPTYPE oTunnelSetup;

    DBGT_PROLOG();

    if (hOutput == NULL && hInput == NULL)
    {
        DBGT_ERROR("OMX_ErrorBadParameter");
        DBGT_EPILOG();
        return OMX_ErrorBadParameter;
    }

    oTunnelSetup.nTunnelFlags = 0;
    oTunnelSetup.eSupplier = OMX_BufferSupplyUnspecified;

    pCompOut = (OMX_COMPONENTTYPE*)hOutput;

    if (hOutput)
    {
        eError = pCompOut->ComponentTunnelRequest(hOutput, nPortOutput, hInput, nPortInput, &oTunnelSetup);
    }


    if (eError == OMX_ErrorNone && hInput) 
    {  
        pCompIn = (OMX_COMPONENTTYPE*)hInput;
        eError = pCompIn->ComponentTunnelRequest(hInput, nPortInput, hOutput, nPortOutput, &oTunnelSetup);
        if (eError != OMX_ErrorNone && hOutput) 
        {
            /* cancel tunnel request on output port since input port failed */
            pCompOut->ComponentTunnelRequest(hOutput, nPortOutput, NULL, 0, NULL);
        }
    }
    DBGT_EPILOG();
    return eError;
}

/*************************************************************************
* OMX_ComponentNameEnum()
*
* Description: This method will provide the name of the component at the given nIndex
*
*Parameters:
* @param[out] cComponentName       The name of the component at nIndex
* @param[in] nNameLength                The length of the component name
* @param[in] nIndex                         The index number of the component 
*
* Returns:    OMX_NOERROR          Successful
*
* Note
*
**************************************************************************/
OMX_API OMX_ERRORTYPE OMX_APIENTRY OMX_ComponentNameEnum(
    OMX_OUT OMX_STRING cComponentName,
    OMX_IN  OMX_U32 nNameLength,
    OMX_IN  OMX_U32 nIndex)
{
    OMX_ERRORTYPE eError = OMX_ErrorNone;

    DBGT_PROLOG();

    DBGT_CHECK_EXIT(cComponentName != NULL, OMX_ErrorBadParameter);

    if (nIndex >= COUNTOF(componentTable))
    {
        DBGT_EPILOG("OMX_ErrorNoMore");
        return OMX_ErrorNoMore;
     }

    else
        strncpy(cComponentName, componentTable[nIndex].name, nNameLength);
    
EXIT:
   DBGT_EPILOG();
   return eError;
}


/*************************************************************************
* OMX_GetRolesOfComponent()
*
* Description: This method will query the component for its supported roles
*
*Parameters:
* @param[in] cComponentName     The name of the component to query
* @param[in] pNumRoles     The number of roles supported by the component
* @param[in] roles		The roles of the component
*
* Returns:    OMX_NOERROR          Successful
*                 OMX_ErrorBadParameter		Faliure due to a bad input parameter
*
* Note
*
**************************************************************************/
OMX_API OMX_ERRORTYPE OMX_GetRolesOfComponent (
    OMX_IN      OMX_STRING cComponentName,
    OMX_INOUT   OMX_U32 *pNumRoles,
    OMX_OUT     OMX_U8 **roles)
{

    OMX_ERRORTYPE eError = OMX_ErrorNone;
    OMX_U32 i, j;

    DBGT_CHECK_EXIT(cComponentName != NULL, OMX_ErrorBadParameter);
    DBGT_CHECK_EXIT(pNumRoles != NULL, OMX_ErrorBadParameter);
    DBGT_CHECK_EXIT(strlen(cComponentName) < MAXNAMESIZE, OMX_ErrorInvalidComponentName);

    for( i = 0; i < COUNTOF(componentTable); i++)
    {
        if (strcmp(cComponentName, componentTable[i].name) == 0)
        {
            if (roles == NULL)
            {
                *pNumRoles = componentTable[i].nRoles;
            }
            else
            {
                /* must be second of two calls,
                   pNumRoles is input in this context.
                   If pNumRoles is < actual number of roles
                   than we return an error */
                DBGT_CHECK_EXIT(*pNumRoles == componentTable[i].nRoles, OMX_ErrorBadParameter);

                for (j = 0; j < componentTable[i].nRoles; j++)
                {
                    strcpy((OMX_STRING) roles[j], componentTable[i].pRoleArray[j]);
                }
            }
            DBGT_EPILOG();
            return eError;
        }
    }

EXIT:
    DBGT_EPILOG();
    return eError;
}

/*************************************************************************
* OMX_GetComponentsOfRole()
*
* Description: This method will query the component for its supported roles
*
*Parameters:
* @param[in] role     The role name to query for
* @param[in] pNumComps     The number of components supporting the given role
* @param[in] compNames      The names of the components supporting the given role
*
* Returns:    OMX_NOERROR          Successful
*
* Note
*
**************************************************************************/
OMX_API OMX_ERRORTYPE OMX_GetComponentsOfRole ( 
    OMX_IN      OMX_STRING role,
    OMX_INOUT   OMX_U32 *pNumComps,
    OMX_INOUT   OMX_U8  **compNames)
{
    OMX_ERRORTYPE eError;
    OMX_U32 i, j;
    OMX_U32 k = 0;

    DBGT_CHECK_EXIT(role != NULL, OMX_ErrorBadParameter);
    DBGT_CHECK_EXIT(pNumComps != NULL, OMX_ErrorBadParameter);

    DBGT_PROLOG("role=%s", role);
    for( i = 0; i < COUNTOF(componentTable); i++)
    {
        for (j = 0; j < componentTable[i].nRoles; j++)
        {
            if (strcmp(componentTable[i].pRoleArray[j], role) == 0)
            {
                /* the first call to this function should only count the number
                   of roles so that for the second call compNames can be allocated
                   with the proper size for that number of roles */
                if (compNames != NULL)
                {
                    strcpy((OMX_STRING)compNames[k], componentTable[i].name);
                }
                k++;
            }
        }
    }

    *pNumComps = k;
    DBGT_EPILOG();
    return OMX_ErrorNone;
EXIT:
    DBGT_EPILOG();
    return eError;
}

