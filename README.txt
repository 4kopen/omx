omx is a userspace library and is dynamically linked with other libraries.
omx and its contents are licensed as Apache-2.0
omxapp folder in omx is built into an executable which is to be used
internally and will not be delivered to the customers.
Erstwhile test folder has been removed from OMX releases from now on as it's
not used.
