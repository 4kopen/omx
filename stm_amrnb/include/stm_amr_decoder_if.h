/*
 * Copyright 2011 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *
 * File   ::    stm_amr_decoder_if.h
 * Author ::    Pradeep Kumar SAH (pradeep.sah@st.com)
 *
 *
 */

#ifndef	_stm_amr_decoder_if_h_
#define _stm_amr_decoder_if_h_

#include "stm_codec_common_if.h"


enum {
    AMR_ERROR_CONCEALMENT_OFF = 0,
    AMR_ERROR_CONCEALMENT_RX_NO_DATA
};

typedef struct {
    // Config									//  DEFAULT CONF
    unsigned short noHeader;					// depending on AMR support format - STATIC
    unsigned short concealment_on;				// enable error concealment
    unsigned short memory_preset;               		// defines memory allocation zone. Value taken from AMR_ALLOC_TYPE_ENUM_T
    unsigned short Payload_Format;              		// 0 (rfc3267), 1 (IF2), 2(GSM MODEM PAYLOAD), 0x82 (3G MODEM PAYLOAD)
    unsigned short Efr_on;                                          // 1 activates Alternative EFR mode
} AMR_DECODER_CONFIG_STRUCT_T;


typedef struct {
    // Config									//  DEFAULT CONF
    unsigned short data_valid;					// 0 as long as codec info not available, 1 otherwise
    unsigned short mode;
    unsigned short bitrate;						// value in bps/10
} AMR_DECODER_INFO_STRUCT_T;


#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

int 
amr_syncword(int hi, int lo, int *length);

CODEC_INIT_T
amr_init_decode(CODEC_INTERFACE_T *itf_cfg);

CODEC_INIT_T
amr_init_decode_malloc(CODEC_INTERFACE_T *itf_cfg);

void
amr_close_decode_malloc(CODEC_INTERFACE_T *itf_cfg);

RETURN_STATUS_LEVEL_T
amr_decode_frame(CODEC_INTERFACE_T *interface_fe);

void
amr_decode_reset(CODEC_INTERFACE_T *interface_fe);

void
amr_decode_setBuffer(CODEC_INTERFACE_T *interface_fe, void *buf);

#ifdef __cplusplus
}
#endif // __cplusplus

#endif /*_stm_amr_decoder_if_h_ */
