ifneq ($(ARCH),)
MYARCH := $(ARCH)
else
MYARCH := armv7
endif


TREE_ROOT := $(shell pwd)
CROSS_COMPILE ?= $(MYARCH)-linux-
ccflags-y := -g -Wall -fPIC -Wno-unused -Wno-switch
ccflags-y +=-I$(TREE_ROOT)/include
ccflags-y +=-I$(TREE_ROOT)/stm_omx_component/common
ccflags-y +=-I$(TREE_ROOT)/stm_omx_component/audenc
ccflags-y +=-I$(TREE_ROOT)/stm_omx_component/viddec
ccflags-y +=-I$(TREE_ROOT)/stm_amrnb/include
ccflags-y +=-I$(SDK2_SOURCE_OMXHEADERS)
ccflags-y +=-I$(SDK2_SOURCE_STLINUXTV)
ccflags-y +=-I$(SDK2_SOURCE_STLINUXTV)/linux/include
ccflags-y +=-I$(SDK2_SOURCE_OMXSE)


CC := $(CROSS_COMPILE)gcc
CXX := $(CROSS_COMPILE)g++
DESTDIR?=$(TARGET_DIR)
PATH :=$(STM_BASE_PREFIX)/devkit/$(MYARCH)/bin:$(PATH)

export PATH

RM := rm -rf

LIBS := -lpthread -lrt -ldl -L$(TREE_ROOT)
ccflags-y += -DHAVE_STDINT_H -DHAVE_PTHREAD_H -DHAVE_STRING_H -DHAVE_ASSERT_H

OBJ_OMX_CORE := omx_core/src/OMX_Core.o

LIBS_OMX_CORE := $(LIBS)

OBJ_OMX_AUDREND :=  libSTMOMXCommon.so stm_omx_component/aud/AudioPort.d \
	stm_omx_component/aud/AudioRenderer.d

LIBS_OMX_AUDREND := $(LIBS) -lSTMOMXCommon -lasound

OBJ_OMX_AUDDEC :=  libSTMOMXCommon.so stm_omx_component/auddec/AudioDecoderPort.d \
	stm_omx_component/auddec/AudioDecoder.d \
	stm_omx_component/auddec/AudioPesAdapter.d \
	stm_omx_component/auddec/dvbaudio_moo.d \
	stm_omx_component/auddec/dvbaudio_sf.d \
	stm_omx_component/auddec/aac.d \
	stm_omx_component/auddec/pcm.d \
	stm_omx_component/auddec/vorbis.d \
	stm_omx_component/auddec/ac3.d \
	stm_omx_component/auddec/wma.d \
	stm_omx_component/auddec/dts.d \
	stm_omx_component/auddec/mp3.d \
	stm_omx_component/auddec/amr.d \
	stm_omx_component/auddec/adpcm.d

LIBS_OMX_AUDDEC := $(LIBS)  -lSTMOMXCommon
# commented out for now
#LIBS_OMX_AUDDEC += -lAMRNB
CFLAGS_OMX_AUDDEC := $(ccflags-y)
#CFLAGS_OMX_AUDDEC += -L$(TREE_ROOT)/stm_amrnb/static_lib

OBJ_OMX_AUDENC := libSTMOMXCommon.so \
	stm_omx_component/audenc/v4laudioenc.d              \
	stm_omx_component/audenc/AudioEncoderPort.d         \
	stm_omx_component/audenc/AudioEncoder.d

LIBS_OMX_AUDENC := $(LIBS) -lSTMOMXCommon

OBJ_OMX_VIDREND := libSTMOMXCommon.so \
	stm_omx_component/vid/VideoPort.d                \
	stm_omx_component/vid/VideoRenderer.d

LIBS_OMX_VIDREND := $(LIBS) -lSTMOMXCommon

OBJ_OMX_VIDPROC := libSTMOMXCommon.so \
	stm_omx_component/vid/VideoPort.d                \
	stm_omx_component/vid/VideoProcessor.d

LIBS_OMX_VIDPROC := $(LIBS) -lSTMOMXCommon

OBJ_OMX_VIDSCHD := libSTMOMXCommon.so \
	stm_omx_component/vid/VideoPort.d                \
	stm_omx_component/vid/VideoScheduler.d

LIBS_OMX_VIDSCHD := $(LIBS) -lSTMOMXCommon

OMX_VIDDEC_PATH := stm_omx_component/viddec
OBJ_OMX_VIDDEC := libSTMOMXCommon.so \
	$(OMX_VIDDEC_PATH)/VideoDecoderPort.d         \
	$(OMX_VIDDEC_PATH)/VideoDecoder.d             \
	$(OMX_VIDDEC_PATH)/VideoPesAdapter.d          \
	$(OMX_VIDDEC_PATH)/dvbvideo_moo.d             \
	$(OMX_VIDDEC_PATH)/dvbvideo_sf.d              \
	$(OMX_VIDDEC_PATH)/avc.d 					\
	$(OMX_VIDDEC_PATH)/mpeg4.d \
	$(OMX_VIDDEC_PATH)/h263.d \
	$(OMX_VIDDEC_PATH)/mpeg2.d \
	$(OMX_VIDDEC_PATH)/vc1.d \
	$(OMX_VIDDEC_PATH)/flv1.d \
	$(OMX_VIDDEC_PATH)/mjpeg.d \
	$(OMX_VIDDEC_PATH)/vp8.d \
	$(OMX_VIDDEC_PATH)/theora.d \
	$(OMX_VIDDEC_PATH)/cavs.d \
	$(OMX_VIDDEC_PATH)/hevc.d \
	$(OMX_VIDDEC_PATH)/videocc.cpp

LIBS_OMX_VIDDEC := $(LIBS) -lSTMOMXCommon

OBJ_OMX_VIDENC := \
	stm_omx_component/videnc/dvbvideo_sf.d              \
	stm_omx_component/videnc/VideoEncoderPort.d         \
	stm_omx_component/videnc/VideoEncoder.d

OBJ_OMX_CLOCK := libSTMOMXCommon.so \
	stm_omx_component/other/Clock.d

LIBS_OMX_CLOCK := $(LIBS) -lSTMOMXCommon

OMX_COMMON_PATH := stm_omx_component/common
OBJ_OMX_COMMON := \
	$(OMX_COMMON_PATH)/OMXdebug.d \
	$(OMX_COMMON_PATH)/OMXdebug_specific.o \
	$(OMX_COMMON_PATH)/pes.d \
	$(OMX_COMMON_PATH)/Role.d \
	$(OMX_COMMON_PATH)/Mutex.d \
	$(OMX_COMMON_PATH)/StmOmxPortDefinition.d  \
	$(OMX_COMMON_PATH)/VideoPortDefinition.d  \
	$(OMX_COMMON_PATH)/AudioPortDefinition.d  \
	$(OMX_COMMON_PATH)/OtherPortDefinition.d  \
	$(OMX_COMMON_PATH)/StmOmxPort.d  \
	$(OMX_COMMON_PATH)/ClockPort.d  \
	$(OMX_COMMON_PATH)/StmOmxComponent.d  \
	$(OMX_COMMON_PATH)/StmOmxComponentWrapper.d \
	$(OMX_COMMON_PATH)/link.d

CFLAGS_OMX_COMMON := $(ccflags-y)
CFLAGS_OMX_COMMON += -DHAVE_STRING_H

OMX_OMXAPP_PATH=$(TREE_ROOT)/omxapp

ifneq ("$(wildcard $(OMX_OMXAPP_PATH))","")
OBJ_OMX_MEDIAPLAYER := \
	omxapp/src/OMXMediaPlayer.g                  \
	omxapp/src/OMXUtils.g

CFLAGS_OMX_MEDIAPLAYER := $(ccflags-y) -Wno-write-strings
CFLAGS_OMX_MEDIAPLAYER +=-I$(TREE_ROOT)/omxapp/include -DSTM_PLATFORM

LIBS_OMX_MEDIAPLAYER := $(LIBS) -lOMX_Core

OBJ_OMX_PLAYERAPP := \
        omxapp/src/OMXExampleApp.g

LIBS_OMX_PLAYERAPP := $(LIBS) -lOMX_Core -lOMXMediaPlayer

APP_OMX_PLAYERAPP := OMXPlayerApp
endif

ifneq ("$(wildcard $(OMX_OMXAPP_PATH))","")
all : libOMX_Core.so libSTMOMXCommon.so \
	libOMX.STM.Audio.Renderer.so libOMX.STM.Audio.Decoder.so libOMX.STM.Audio.Encoder.so \
	libOMX.STM.Video.Renderer.so libOMX.STM.Video.Processor.so libOMX.STM.Video.Scheduler.so \
	libOMX.STM.Video.Decoder.so libOMX.STM.Video.Encoder.so \
	libOMX.STM.Clock.so \
	libOMXMediaPlayer.so $(APP_OMX_PLAYERAPP)
else
all : libOMX_Core.so libSTMOMXCommon.so \
	libOMX.STM.Audio.Renderer.so libOMX.STM.Audio.Decoder.so libOMX.STM.Audio.Encoder.so \
	libOMX.STM.Video.Renderer.so libOMX.STM.Video.Processor.so libOMX.STM.Video.Scheduler.so \
	libOMX.STM.Video.Decoder.so libOMX.STM.Video.Encoder.so \
	libOMX.STM.Clock.so
endif

%.o: %.c
	$(CC) -c -o $@ $< $(ccflags-y)

%.d: %.cpp
	$(CXX) -c -o $@ $< $(ccflags-y)

ifneq ("$(wildcard $(OMX_OMXAPP_PATH))","")
%.g: %.cpp
	$(CXX) -c -o $@ $< $(CFLAGS_OMX_MEDIAPLAYER)
endif

libOMX_Core.so : $(OBJ_OMX_CORE)
	@echo Include
	$(CC) -shared -o $@ $^ $(ccflags-y) $(LIBS_OMX_CORE)

libOMX.STM.Audio.Renderer.so : $(OBJ_OMX_AUDREND)
	$(CXX) -shared -o $@ $^ $(ccflags-y) $(LIBS_OMX_AUDREND)

libOMX.STM.Audio.Decoder.so : $(OBJ_OMX_AUDDEC)
	$(CXX) -shared -o $@ $^ $(CFLAGS_OMX_AUDDEC) $(LIBS_OMX_AUDDEC)

libOMX.STM.Audio.Encoder.so : $(OBJ_OMX_AUDENC)
	$(CXX) -shared -o $@ $^ $(ccflags-y) $(LIBS_OMX_AUDENC)

libOMX.STM.Video.Renderer.so : $(OBJ_OMX_VIDREND)
	$(CXX) -shared -o $@ $^ $(ccflags-y) $(LIBS_OMX_VIDREND)

libOMX.STM.Video.Processor.so : $(OBJ_OMX_VIDPROC)
	$(CXX) -shared -o $@ $^ $(ccflags-y) $(LIBS_OMX_VIDPROC)

libOMX.STM.Video.Scheduler.so : $(OBJ_OMX_VIDSCHD)
	$(CXX) -shared -o $@ $^ $(ccflags-y) $(LIBS_OMX_VIDSCHD)

libOMX.STM.Video.Decoder.so : $(OBJ_OMX_VIDDEC)
	$(CXX) -shared -o $@ $^ $(ccflags-y) $(LIBS_OMX_VIDDEC)

libOMX.STM.Video.Encoder.so : $(OBJ_OMX_VIDENC)
	$(CXX) -shared -o $@ $^ $(ccflags-y) $(LIBS)

libOMX.STM.Clock.so : $(OBJ_OMX_CLOCK)
	$(CXX) -shared -o $@ $^ $(ccflags-y) $(LIBS_OMX_CLOCK)

libSTMOMXCommon.so :  $(OBJ_OMX_COMMON)
	$(CXX) -shared -o $@ $^ $(CFLAGS_OMX_COMMON) $(LIBS)

ifneq ("$(wildcard $(OMX_OMXAPP_PATH))","")
libOMXMediaPlayer.so :  $(OBJ_OMX_MEDIAPLAYER)
	$(CXX) -shared -o $@ $^ $(CFLAGS_OMX_MEDIAPLAYER) $(LIBS)
endif

ifneq ("$(wildcard $(OMX_OMXAPP_PATH))","")
$(APP_OMX_PLAYERAPP) : $(OBJ_OMX_PLAYERAPP)
	$(CXX) -o  $@ $^ $(CFLAGS_OMX_MEDIAPLAYER) $(LIBS_OMX_PLAYERAPP) -L$(TREE_ROOT)

install:
	cp -Lrf *.so $(DESTDIR)/usr/lib
	cp -Lrf $(APP_OMX_PLAYERAPP) $(DESTDIR)/usr/bin

clean_all:
	rm -rf $(APP_OMX_PLAYERAPP)
	find . -name "*.o" -print0 | xargs -0 rm -rf
	find . -name "*.d" -print0 | xargs -0 rm -rf
	find . -name "*.e" -print0 | xargs -0 rm -rf
	find . -name "*.g" -print0 | xargs -0 rm -rf
	find . -name "*.so" -print0 | xargs -0 rm -rf
else
install:
	cp -Lrf *.so $(DESTDIR)/usr/lib

clean_all:
	find . -name "*.o" -print0 | xargs -0 rm -rf
	find . -name "*.d" -print0 | xargs -0 rm -rf
	find . -name "*.e" -print0 | xargs -0 rm -rf
	find . -name "*.so" -print0 | xargs -0 rm -rf
endif

clean:
	find . -name "*.o" -print0 | xargs -0 rm -rf
.PHONY :
