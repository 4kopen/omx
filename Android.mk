
LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

STM_OMX_TOP := $(LOCAL_PATH)

STM_OMX_CFLAGS := -Wall -fpic -pipe -DSTATIC_TABLE -O0 


STM_OMX_INC := $(STM_OMX_TOP)/include/
STM_OMX_COMPONENT := $(STM_OMX_TOP)/stm_omx_component

include $(STM_OMX_TOP)/omx_core/src/Android.mk
include $(STM_OMX_TOP)/stm_amrnb/static_lib/Android.mk

include $(STM_OMX_COMPONENT)/common/Android.mk
include $(STM_OMX_COMPONENT)/viddec/Android.mk
include $(STM_OMX_COMPONENT)/vid/Android.mk
include $(STM_OMX_COMPONENT)/other/Android.mk
include $(STM_OMX_COMPONENT)/aud/Android.mk
include $(STM_OMX_COMPONENT)/auddec/Android.mk
include $(STM_OMX_COMPONENT)/videnc/Android.mk
include $(STM_OMX_COMPONENT)/audenc/Android.mk
