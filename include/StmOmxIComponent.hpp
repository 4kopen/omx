/*
 * Copyright 2013 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file   StmIOmxComponent.hpp
 * @author STMicroelectronics
 */

#ifndef _STM_IOMXCOMPONENT_HPP_
#define _STM_IOMXCOMPONENT_HPP_

#include "OMX_Core.h"
#include "OMX_Component.h"

namespace stm {

// Forward declaration
class IOmxPort;
class IOmxComponent;

/**
 * Super class IComponent which defines component methods required
 * by omx core and component wrapper.
 */
class IOmxComponent
{
public:
    /** Destructor. */
    virtual ~IOmxComponent()
    {}

public:
    /** Return the name of component instance */
    virtual const char* name() const = 0;

    /** Return the handle of the component */
    virtual OMX_HANDLETYPE handle() const = 0;

    /** Return the number of port of the component */
    virtual unsigned int portNb() const = 0;

    /** Return the given port */
    virtual const IOmxPort& getPort(unsigned int nIdx) const = 0;

    /** Return the component OMX state */
    virtual OMX_STATETYPE omxState() const = 0;

    virtual
    OMX_ERRORTYPE getComponentVersion(OMX_VERSIONTYPE* pComponentVersion,
                                      OMX_VERSIONTYPE* pSpecVersion,
                                      OMX_UUIDTYPE* pComponentUUID) = 0;

    virtual
    OMX_ERRORTYPE sendCommand(OMX_COMMANDTYPE Cmd,
                              OMX_U32 nParam,
                              OMX_PTR pCmdData) = 0;

    virtual
    OMX_ERRORTYPE getParameter(OMX_INDEXTYPE nIndex,
                               OMX_PTR pComponentParameterStructure) = 0;

    virtual
    OMX_ERRORTYPE setParameter(OMX_INDEXTYPE nIndex,
                               OMX_PTR pComponentParameterStructure) = 0;

    virtual
    OMX_ERRORTYPE getConfig(OMX_INDEXTYPE nIndex,
                            OMX_PTR pComponentConfigStructure) = 0;

    virtual
    OMX_ERRORTYPE setConfig(OMX_INDEXTYPE nIndex,
                            OMX_PTR pComponentConfigStructure) = 0;

    virtual
    OMX_ERRORTYPE getExtensionIndex(OMX_STRING cParameterName,
                                    OMX_INDEXTYPE* pIndexType) = 0;

    virtual
    OMX_ERRORTYPE componentTunnelRequest(OMX_U32 nPort,
                                         OMX_HANDLETYPE hTunneledComp,
                                         OMX_U32 nTunneledPort,
                                         OMX_TUNNELSETUPTYPE* pSetup) = 0;

    virtual
    OMX_ERRORTYPE useBuffer(OMX_BUFFERHEADERTYPE** ppBufferHdr,
                            OMX_U32 nPortIndex,
                            OMX_PTR pAppPrivate,
                            OMX_U32 nSizeBytes,
                            OMX_U8* pBuffer) = 0;

    virtual
    OMX_ERRORTYPE allocateBuffer(OMX_BUFFERHEADERTYPE** ppBufferHdr,
                                 OMX_U32 nPortIndex,
                                 OMX_PTR pAppPrivate,
                                 OMX_U32 nSizeBytes) = 0;

    virtual
    OMX_ERRORTYPE freeBuffer(OMX_U32 nPortIndex,
                             OMX_BUFFERHEADERTYPE* pBuffer) = 0;

    virtual
    OMX_ERRORTYPE emptyThisBuffer(OMX_BUFFERHEADERTYPE* pBuffer) = 0;

    virtual
    OMX_ERRORTYPE fillThisBuffer(OMX_BUFFERHEADERTYPE* pBuffer) = 0;

    virtual
    OMX_ERRORTYPE setCallbacks(OMX_CALLBACKTYPE* pCallbacks,
                               OMX_PTR pAppData) = 0;

    virtual
    OMX_ERRORTYPE useEGLImage(OMX_BUFFERHEADERTYPE** ppBufferHdr,
                              OMX_U32 nPortIndex,
                              OMX_PTR pAppPrivate,
                              void* eglImage) = 0;

    virtual
    OMX_ERRORTYPE componentRoleEnum(OMX_U8* cRole,
                                    OMX_U32 nIndex) = 0;

    virtual
    void destroy() = 0;

protected:
    /** Return the given port. Non const accessor */
    virtual IOmxPort& getPort(unsigned int nIdx) = 0;

    /**
     * Hidden default constructor, make protected so cannot ever create
     * one on its own.
     */
    IOmxComponent()
    {}

    /** Copy constructor not implemented */
    IOmxComponent(const IOmxComponent& );
};


/** Define the OMX IL version that corresponds to this set of header files.
 *  We also define a combined version that can be used to write or compare
 *  values of the 32bit nVersion field, assuming a little endian architecture
 */
static const unsigned int VERSION_MAJOR    = 1;
static const unsigned int VERSION_MINOR    = 1;
static const unsigned int VERSION_REVISION = 2;
static const unsigned int VERSION_STEP     = 0;

static const unsigned int VERSION = ((VERSION_STEP << 24)     |
                                     (VERSION_REVISION << 16) |
                                     (VERSION_MINOR << 8)     |
                                     (VERSION_MAJOR));

/** Function used to indicate the Open Max specification version. */
inline void omxIlSpecVersion(OMX_VERSIONTYPE* version)
{
    version->nVersion        = 0;
    version->s.nVersionMajor = VERSION_MAJOR;
    version->s.nVersionMinor = VERSION_MINOR;
    version->s.nRevision     = VERSION_REVISION;
    version->s.nStep         = VERSION_STEP;
}

template<typename T>
bool checkStructVersion(const void* pStruct)
{
    OMX_VERSIONTYPE ver;
    const T* tmpStruct = static_cast<const T*>(pStruct);

    ::stm::omxIlSpecVersion(&ver);

    if ((tmpStruct->nVersion.nVersion & 0xFFFF) == (ver.nVersion & 0xFFFF)) {
        return true;
    } else {
        return false;
    }
}

#define ASSERT_STRUCT_TYPE(ptr, type)                  \
    {                                                  \
        type* _tempstruct = (type*)ptr;                \
        OMX_VERSIONTYPE version;                       \
        version.nVersion = 0;                          \
        ::stm::omxIlSpecVersion(&version);             \
        if ((_tempstruct->nSize != 0) &&               \
            !(_tempstruct->nSize == sizeof(type))) {   \
            DBGT_ASSERT("Bad structure size");         \
        }                                              \
        if ((_tempstruct->nVersion.nVersion != 0) &&   \
            !(((_tempstruct->nVersion.nVersion & 0xFF) \
               == (version.nVersion & 0xFF)))) {       \
            DBGT_ASSERT("Bad openmax version");        \
        }                                              \
    }

} // eof namespace stm

#endif // _STM_IOMXCOMPONENT_HPP_
