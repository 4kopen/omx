/*
 * Copyright 2013 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file   StmIOmxPortDefinition.hpp
 * @author STMicroelectronics
 */

#ifndef _STM_IOMXPORTDEFINITION_HPP_
#define _STM_IOMXPORTDEFINITION_HPP_

#include "OMX_Core.h"
#include "OMX_Component.h"


namespace stm {

/**
 * Interface which defines port definition common to all media
 */
class IOmxPortDefinition
{
public:
    /** Destructor. */
    virtual ~IOmxPortDefinition()
    {}

public:
    /** Port index in the component */
    virtual unsigned int index() const = 0;

    /** Return whether the port is enable or disable */
    virtual bool isEnabled() const = 0;

    /** Return whether the port is populated with all of its buffers or not.
     * A disabled port is always unpopulated
     */
    virtual bool isPopulated() const = 0;

    /** Domain of the port */
    virtual OMX_PORTDOMAINTYPE domain() const = 0;

    /** Direction (input or output) of the port */
    virtual OMX_DIRTYPE direction() const = 0;

    /** The actual number of buffers allocated on the port */
    virtual unsigned int bufferCountActual() const = 0;

    /** The minimum number of buffers this port requires */
    virtual unsigned int bufferCountMin() const = 0;

    /** Size, in bytes, for buffers to be used for the port */
    virtual unsigned int bufferSize() const = 0;

    /** Return whether port mandate buffer with a contiguous chunk
     * in memory */
    virtual bool isBuffersContiguous() const = 0;

    /** Return alignement in bytes mandate by buffer allocated within the port */
    virtual unsigned int  bufferAlignment() const = 0;

    /** Turn internal port state to enable */
    virtual void
    enabled() = 0;

    /** Turn internal port state to disable */
    virtual void
    disabled() = 0;

    /** Turn internal port state to populated */
    virtual void
    populated() = 0;

    /** Turn internal port state to not populated */
    virtual void
    unpopulated() = 0;

protected:
    /** Hidden default constructor, make protected so cannot ever create
	one on its own. */
    IOmxPortDefinition()
    {}

    /** Copy constructor not implemented */
    IOmxPortDefinition(const IOmxPortDefinition& );
};

} // eof namespace stm

#endif // _STM_IOMXPORTDEFINITION_HPP_


