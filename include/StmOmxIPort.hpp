/*
 * Copyright 2013 ST MicroElectronics Pvt LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file   StmIOmxPort.hpp
 * @author STMicroelectronics
 */

#ifndef _STM_IOMXPORT_HPP_
#define _STM_IOMXPORT_HPP_

#include "OMX_Core.h"
#include "OMX_Component.h"


namespace stm {

// Forward declaration
class IOmxPort;
class IOmxComponent;
class IOmxPortDefinition;


/**
 * Super class Port which defines port methods required all media component.
 */
class IOmxPort
{
public:
    /** Destructor. */
    virtual ~IOmxPort()
    {}

public:
    /** Return port definition and buffer requirements */
    virtual const IOmxPortDefinition& definition() const = 0;

    /** Return reference the component owning port */
    virtual const IOmxComponent& component() const = 0;

    virtual OMX_ERRORTYPE
    getParameter(OMX_INDEXTYPE nIndex, OMX_PTR pParamStruct) = 0;

    virtual OMX_ERRORTYPE
    setParameter(OMX_INDEXTYPE nIndex, OMX_PTR pParamStruct) = 0;

    virtual OMX_ERRORTYPE
    tunnelRequest(OMX_HANDLETYPE hTunneledComp,
                  OMX_U32 nTunneledPort,
                  OMX_TUNNELSETUPTYPE* pTunnelSetup) = 0;

    virtual OMX_ERRORTYPE
    useBuffer(OMX_BUFFERHEADERTYPE** ppBufferHdr,
              OMX_PTR pAppPrivate,
              OMX_U32 nSizeBytes,
              OMX_U8* pBuffer) = 0;

    virtual OMX_ERRORTYPE
    allocateBuffer(OMX_BUFFERHEADERTYPE** ppBufferHdr,
                   OMX_PTR pAppPrivate,
                   OMX_U32 nSizeBytes) = 0;

    virtual OMX_ERRORTYPE
    freeBuffer(OMX_BUFFERHEADERTYPE* pBuffer) = 0;

    virtual OMX_ERRORTYPE
    emptyThisBuffer(OMX_BUFFERHEADERTYPE* pBuffer) = 0;

    virtual OMX_ERRORTYPE
    fillThisBuffer(OMX_BUFFERHEADERTYPE* pBuffer) = 0;

    virtual OMX_ERRORTYPE disable() = 0;
    virtual OMX_ERRORTYPE enable() = 0;
    virtual OMX_ERRORTYPE flush() = 0;

protected:
    /** Return port definition and buffer requirements */
    virtual IOmxPortDefinition& definition() = 0;

    /** Hidden default constructor, make protected so cannot ever create
        one on its own. */
    IOmxPort()
    {}

    /** Copy constructor not implemented */
    IOmxPort(const IOmxPort& st);
};

} // eof namespace stm

#endif // _STM_IOMXPORT_HPP_
